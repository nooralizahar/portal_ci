<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Kodrat_model extends CI_Model {

    public function ambil_semua() {
        $data = $this->db->get("klasifikasi")->result();
        return $data;
    }

    public function tambah() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);

        if(isset($post["kode"])) {
            $num = $this->db->insert("klasifikasi",$post);
            return $num;
        } else return false;
    }
    public function edit() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);

        if(isset($post["kode"])) {
            $this->db->where("id_klasifikasi",$post["id_klasifikasi"]);
            unset($post["id_klasifikasi"]);
            $num = $this->db->update("klasifikasi",$post);
            return $num;
        } else return false;
    }

    public function ambil_berdasarkan_id($id) {
        $data = $this->db->where("id_klasifikasi",$id)->get("klasifikasi")->row();
        return $data;
    }


}