<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Otbdata_model extends CI_Model {

    public function jclsim() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["tbua"] = date("Y-m-d H:i:s");
        $post["ubua"] = $this->session->userdata("id_pengguna");
        if(isset($post["jocl"])) {
            $n= $this->db->insert("t_bto_jc",$post);
            return $n;
        } else return false;
    }
    
    public function updnam(){
        $p = $this->input->post();
        unset($p["btnSimpan"]);
        unset($p["nm_otb_old"]);
        $d=$this->db->where('id_otb', $this->input->post('id_otb'))->update('t_bto',$p);
        return $d;
    }

    public function jclhap($i) {
        $d = $this->db->where('idjc',$i)->delete("t_bto_jc");
        return $d;
    }

    public function jclsem() {
        $data = $this->db->order_by('tbua','desc')->get("t_bto_jc")->result();
        return $data;
    }

    public function jcllih($i) {
        $data = $this->db->where('idot',$i)->order_by('tbua','desc')->get("t_bto_jc")->result();
        return $data;
    }

    public function petapil($p) {
        $data = $this->db->where('idot',$p)->get("v_btojc_ba_final")->result();
        return $data;
    }

    public function pilcent($p) {
        $data = $this->db->where('idot',$p)->get("v_btojc_b")->row();
        return $data;
    }

    public function otsem() {
    	$data = $this->db->order_by('tg_cre','asc')->get("v_bto_w_loc")->result();
        return $data;
    }

    public function jcsem() {
        $data = $this->db->order_by('tbua','asc')->get("t_bto_jc")->result();
        return $data;
    }

    public function ottam() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["tg_cre"] = date("Y-m-d H:i:s");
        $post["us_cre"] = $this->session->userdata("username");
        if(isset($post["nm_otb"])) {
            $num = $this->db->insert("t_bto",$post);
            
            $idot=$this->db->select("id_otb")->from("t_bto")->order_by("id_otb","desc")->limit(1)->get()->result();
            foreach ($idot as $k) {
                $idotb=$k->id_otb;
            }
            $i=$this->input->post('jm_por');
            $tgl=date("Y-m-d H:i:s");
            $usr=$this->session->userdata("username");

            for ($j = 1; $j <= $i; $j++) {
            //..code
                $kolom = ['id_otb'=>$idotb,'id_cor'=>$j,'ou_des'=>'','in_des'=>'NOT SPLICE','tg_cre'=>$tgl,'us_cre'=>$usr,'tg_upd'=>$tgl,'us_upd'=>$usr];
                $r = $this->db->insert("t_bto_rel",$kolom);
            }

            return $num;
        } else return false;
    }

    public function otlih($param) {

        $data = $this->db->where("id_otb", $param)->get("v_bto_all")->result();
        return $data;
    }

    public function otpil($param) {

        $d = $this->db->where("id_otb", $param)->get("t_bto")->result();
        return $d;
    }

    public function otbpil($par) {

        $op = $this->db->where("id_otb",$par)->get("t_bto")->row(); 
        return $op;
    }


    public function otlink($param) {

        $d = $this->db->where("id_otb !=", $param)->get("v_bto_core")->result();
        return $d;
    }

    public function otlinx($param) {

        $d = $this->db->where("id_otb !=", $param)->where("in_des","NOT SPLICE")->get("v_bto_core")->result();
        return $d;
    }

    public function otcor($par) {

        $d = $this->db->where("id_otr", $par)->get("v_bto_core")->result(); 
        return $d;
    }



    public function otpus($param) {
    	$hapus=$this->db->where("id_otb",$param)->delete("t_bto_rel");
        $hapus=$this->db->where("id_otb",$param)->delete("t_bto");
        
    return $hapus;

    }

    public function copil($param) {

        $d = $this->db->where("id_otr", $param)->get("t_bto_rel")->result();
        return $d;
    }
    public function pillo($idl) {

        $d = $this->db->where("id_lok", $idl)->get("t_isakol")->result();
        return $d;
    }

    public function simlo($idl) {

        $tgl=date("Y-m-d H:i:s");
        $usr=$this->session->userdata("username");
        $nalo=$this->input->post("nm_lok");
        $allo=$this->input->post("al_lok");
        $tiko=$this->input->post("ti_kor");

        $updlok= ['nm_lok'=>$nalo,'al_lok'=>$allo,'ti_kor'=>$tiko,'tg_upd'=>$tgl,'us_upd'=>$usr];

        $data=$this->db->where('id_lok', $idl)->update('t_isakol',$updlok );
        return $data;
    }

    public function cosim($param) {

        $tgl=date("Y-m-d H:i:s");
        $usr=$this->session->userdata("username");
        $odes=$this->input->post("ou_des");
        $ides=$this->input->post("in_des");

        $datfill= ['ou_des'=>$odes,'in_des'=>$ides,'tg_upd'=>$tgl,'us_upd'=>$usr];

        $data=$this->db->where('id_otr', $param)->update('t_bto_rel',$datfill );
        
        //----- awal pengecualian

        if($ides!="NOT SPLICE" || $ides!="SPLICE IN OTB"){
        $idor=$this->db->where('id_otr', $param)->get('t_bto_rel')->result();
            foreach ($idor as $y) {
                $idnor=$y->in_des;
            }
        $relfill= ['ou_des'=>$odes,'in_des'=>$param,'tg_upd'=>$tgl,'us_upd'=>$usr];
       	$rela=$this->db->where('id_otr', $idnor)->update('t_bto_rel',$relfill );
       	}else{

       	}    
       	//----- akhir pengecualian 

        return $data;
    }


    public function losem() {
        $data = $this->db->order_by('tg_cre','asc')->get("t_isakol")->result();
        return $data;
    }

    public function lotam() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["tg_cre"] = date("Y-m-d H:i:s");
        $post["us_cre"] = $this->session->userdata("username");
        if(isset($post["nm_lok"])) {
            $num = $this->db->insert("t_isakol",$post);
            return $num;
        } else return false;
    }




    public function pilihp($param) {

        $data = $this->db->where("idodpr", $param)->get("t_odp_rel")->result();//->limit(1)
        return $data;

    }

    public function pelanggan($param) {

        $data = $this->db->where("idodp", $param)->order_by("noport","ASC")->get("t_odp_rel")->result();//->limit(1)
        return $data;

    }

    public function pelsemua() {

        $data = $this->db->get("v_t_odp_rel")->result();//->limit(1)
        return $data;

    }

    public function pelodpset() {

        $data = $this->db->where("idodp !=", 1)->get("v_t_odp_rel")->result();//->limit(1)
        return $data;

    }

    public function ubah() {
        
        if(!empty($this->input->post("status"))){$stat=1;}else{$stat=0;}
        $id=$this->input->post("id");
        $tgup=date("Y-m-d H:i:s");
        $usup=$this->session->userdata("username");
        $nmed=$this->input->post("nama");
        $aled=$this->input->post("alamat");
        $sted=$stat;
        $value=$this->input->post("ver");
        //$checked = ($value === FALSE) ? 0 : 1;
        $koed=$this->input->post("kordinat");
        $kaed=$this->input->post("kapasitas");
        $oled=$this->input->post("oltrel");

        $kolom = ['tg_upd'=>$tgup,'us_upd'=>$usup,'nama'=>$nmed,'alamat'=>$aled,'status'=>$sted,'ver'=>$value,'kordinat'=>$koed,'oltrel'=>$oled,'kapasitas'=>$kaed];

        $data=$this->db->where('idodp', $id)->update('t_odp',$kolom );     
        return $data;
    }

        public function ubahp() {
        
        $id=$this->input->post("id");
        $ido=$this->input->post("idodp");
        $tgup=date("Y-m-d H:i:s");
        $usup=$this->session->userdata("username");
        $uied=$this->input->post("userid");
        $pled=$this->input->post("pelanggan");
        $aled=$this->input->post("alamat");
        $nped=$this->input->post("noport");
        $koed=$this->input->post("kordinat");

        $kolom = ['tg_upd'=>$tgup,'us_upd'=>$usup, 'idodp'=>$ido,'userid'=>$uied,'pelanggan'=>$pled, 'alamat'=>$aled,'noport'=>$nped,'kordinat'=>$koed];

        $data=$this->db->where('idodpr', $id)->update('t_odp_rel',$kolom );     
        return $data;
    }


    public function hapusp($idp) {
        return $this->db->where('idodpr', $idp)->delete('t_odp_rel');
    }

    public function updqr($qid,$qhash,$qfile) {
        
        //$id=$this->input->post("id");
        $tgup=date("Y-m-d H:i:s");
        $usup=$this->session->userdata("username");
        //$nmed=$this->input->post("nama");
        //$aled=$this->input->post("alamat");
        //$koed=$this->input->post("kordinat");
        //$kaed=$this->input->post("kapasitas");

        $kol = ['tg_upd'=>$tgup,'us_upd'=>$usup,'fileqr'=>$qfile,'qrhash'=>$qhash];

        $data=$this->db->where('idodp', $qid)->update('t_odp',$kol );     
        return $data;
    }

    public function odppidai($param) {

            $data = $this->db->where("qrhash",$param)->get("v_t_odp")->result();
            return $data;

    }

    public function cekpidai($param) {

            $data = $this->db->where("qrhash",$param)->get("v_t_odp")->num_rows();
            return $data;

    }

    public function odpuse($idodp) {

            $data = $this->db->where("idodp",$idodp)->get("t_odp_rel")->num_rows();
            return $data;

    }

    public function odppel($idodp) {

            $data = $this->db->where("idodp",$idodp)->get("t_odp_rel")->result();
            return $data;

    }

    public function pilodp($pilih,$param) {

            $data = $this->db->like($pilih,$param)->get("v_t_odp")->result();
            return $data;

    }

    public function pilpel($pilih,$param) {

            $data = $this->db->like($pilih,$param)->get("v_t_pelanggan")->result();
            return $data;

    }

    public function selodp($param) {

        $data = $this->db->where("idodp", $param)->get("v_t_odp")->result();//->limit(1)
        return $data;

    }



}