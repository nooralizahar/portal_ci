<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Export_model extends CI_model{

    public function get_countRequester($requester = NULL, $tgl_awal = NULL, $tgl_akhir = NULL) {
        $query = $this->db->query('select count(Requester) as total from LAMA_DOWN_TIME where Requester = "' . $requester . '" and STR_TO_DATE(Created_Time, "%d/%m/%Y") >= "' . $tgl_awal . '" and STR_TO_DATE(Created_Time, "%d/%m/%Y") <= "' . $tgl_akhir . '" order by Requester');
        //echo $tgl_awal;exit;
        if ($query)
            return $query->result_array();
        else
            return NULL;
    }

    public function get_Requester($requester = NULL, $tgl_awal = NULL, $tgl_akhir = NULL) {
        $query = $this->db->query('select Requester, Created_Time, Subject, request_id, request_type, Down_Time, Uptime, DURASI, Root_Cause, Resolution from LAMA_DOWN_TIME where requester = "' . $requester . '" and STR_TO_DATE(Created_Time, "%d/%m/%Y") >= "' . $tgl_awal . '" and STR_TO_DATE(Created_Time, "%d/%m/%Y") <= "' . $tgl_akhir . '" order by requester');
        //echo $hasil;exit;
        if ($query)
            return $query->result_array();
        else
            return NULL;
    }

    public function get_countProblem($problem = NULL, $tgl_awal = NULL, $tgl_akhir = NULL) {
        $query = $this->db->query('select count(Problem_Side) as total from LAMA_DOWN_TIME where Problem_Side = "' . $problem . '" and STR_TO_DATE(Created_Time, "%d/%m/%Y") >= "' . $tgl_awal . '" and STR_TO_DATE(Created_Time, "%d/%m/%Y") <= "' . $tgl_akhir . '" order by Requester');
        //echo $tgl_awal;exit;
        if ($query)
            return $query->result_array();
        else
            return NULL;
    }

    public function get_Problem($problem = NULL, $tgl_awal = NULL, $tgl_akhir = NULL) {
        $query = $this->db->query('select Requester, Created_Time, Subject, request_id, request_type, Down_Time, Uptime, DURASI, Root_Cause, Resolution from LAMA_DOWN_TIME where Problem_Side = "' . $problem . '" and STR_TO_DATE(Created_Time, "%d/%m/%Y") >= "' . $tgl_awal . '" and STR_TO_DATE(Created_Time, "%d/%m/%Y") <= "' . $tgl_akhir . '" order by requester');
        //echo $hasil;exit;
        if ($query)
            return $query->result_array();
        else
            return NULL;
    }

}
