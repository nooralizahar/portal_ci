<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Netdev_model extends CI_model{


    public function simcpo() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["usbua"] = $this->session->userdata('id_pengguna');
        $post["tgbua"] = date('Y-m-d H:i:s');

            $isi = $this->db->insert("app_latrop_aeraak_cpo",$post);
            return $isi;   
    }

    public function hapus($id) {
        return $this->db->where('idrea', $id)->delete("app_latrop_aeraak");
    }

    public function semua() {
        return $this->db->get("v_app_latrop_aeraak")->result();
    }

    public function pilrea($id) {
        return $this->db->where('idrea', $id)->get("v_app_latrop_aeraak")->row();
    }

    public function reahon($id) {
        return $this->db->where('phrea', $id)->get("v_app_latrop_aeraak")->result();
    }

    public function comrea() {
        return $this->db->get("v_app_latrop_aeraak_lok")->result();
    }

    public function iomrea() {
        return $this->db->get("app_latrop_aeraak_iom")->result();
    }

    public function simiom() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        $post["usbua"] = $this->session->userdata('id_pengguna');
        $post["tgbua"] = date('Y-m-d H:i:s');

            $isi = $this->db->insert("app_latrop_aeraak_iom",$post);
            return $isi;   
    }

	public function simrea() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);

        
        if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_filex($_FILES["attach"]);
                if($gambar == false) {

                    redirect(base_url($this->uriku."/kaarea/"));
                    exit();
                }
                $post["piran"] = json_encode($gambar);
            }

        unset($post["rilok_ddl"]); unset($post["rilok_txt"]);
        unset($post["salok_ddl"]); unset($post["salok_txt"]);
        unset($post["talok_ddl"]); unset($post["talok_txt"]);
        $post["phrea"]=$this->session->userdata("id_pengguna");
        $post["tgrea"]=date("Y-m-d H:i:s");

        if(isset($post["delak"])) {
            $num = $this->db->insert("app_latrop_aeraak",$post);
            return $num;
        } else return false;
    }


    private function upload_filex($files,$path = "assets/uploads/netdev/"){
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|jpeg|bmp|gif|doc|docx|xls|xlsx|ppt|pptx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }	



}