<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Pidai_model extends CI_Model {

        public function koptabaru($id,$param) {

        
        $data = array(
            'id' => $id,
			'deskripsi' => $this->input->post('qrcode_text'),
			'kodefile' => $param,
			'tgbuat' => date('Y-m-d H:i'),
			'usbuat' => 1
		);

		$this->db->insert('t_sakreb', $data);

        }

        public function qrgen($id,$param) {

        
        $data = array(
            'id' => $id,
            'deskripsi' => $this->input->post('qrcode_text'),
            'kodefile' => $param,
            'tgbuat' => date('Y-m-d H:i'),
            'usbuat' => 1
        );

        $this->db->insert('t_sakreb', $data);

        }

        public function cekpidai($param) {

            $data = $this->db->where("depta",$param)->get("v_sakreb")->result();
            return $data;

        }


}