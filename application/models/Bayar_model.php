<?php


defined("BASEPATH") OR exit("Akses script tidak diizinkan!");
class Bayar_model extends CI_Model {

	public function namapel() {
		
		$dq="SELECT username,name FROM rad_pay WHERE username like '0011%' GROUP BY username";
		return $this->db->query($dq)->result();

	}

	public function paymdet($id) {
		
		$dq="select substr(`dbjlm`.`rad_pay`.`invcode`,1,4) AS `thn`,`dbjlm`.`rad_pay`.`invcode` as `invcode`,UPPER(DATE_FORMAT(`dbjlm`.`rad_pay`.`pay_month`,'%b-%y')) AS `bln`,`dbjlm`.`rad_pay`.`username` AS `username`,`dbjlm`.`rad_pay`.`name` AS `name`,`dbjlm`.`rad_pay`.`pay_date` AS `pay_date`,`dbjlm`.`rad_pay`.`packet` AS `packet`,sum(`dbjlm`.`rad_pay`.`price`) AS `price`,((`dbjlm`.`rad_pay`.`price_tax` / 100) * sum(`dbjlm`.`rad_pay`.`price`)) AS `ppn`,sum(`dbjlm`.`rad_pay`.`discount`) AS `discount`,sum(`dbjlm`.`rad_pay`.`discount_tagihan`) AS `discount_tagihan`,`dbjlm`.`rad_pay`.`user` AS `kasir`,ifnull(`dbjlm`.`rad_pay`.`vendor`,`dbjlm`.`rad_pay`.`how`) AS `vendor` from `dbjlm`.`rad_pay` where (`dbjlm`.`rad_pay`.`username` = '".$id."') group by substr(`dbjlm`.`rad_pay`.`invcode`,1,4),`dbjlm`.`rad_pay`.`pay_date`";
		return $this->db->query($dq)->result();

	}

	public function sesidet($id) {
		
		$ds="select * from `radacct_desmar` where `username` = '".$id."' order by acctstarttime desc";
		return $this->db->query($ds)->result();

	}

	public function paybln($id) {

		if($id=='202103'){
			$kon="BETWEEN '2021-03-01' AND '2021-03-31' ";
		}else if($id=='202102'){
			$kon="BETWEEN '2021-02-01' AND '2021-02-28' ";
		}else{
			$kon="BETWEEN '2021-01-01' AND '2021-01-31' ";
		}
		
		$dq="select substr(`dbjlm`.`rad_pay`.`invcode`,1,4) AS `thn`,`dbjlm`.`rad_pay`.`invcode` as `invcode`,UPPER(DATE_FORMAT(`dbjlm`.`rad_pay`.`pay_month`,'%b-%y')) AS `bln`,`dbjlm`.`rad_pay`.`username` AS `username`,ifnull(`dbjlm`.`rad_pay`.`name`,CONCAT('R-',`dbjlm`.`rad_pay`.`item`)) AS `name`,`dbjlm`.`rad_pay`.`pay_date` AS `pay_date`,`dbjlm`.`rad_pay`.`packet` AS `packet`,sum(`dbjlm`.`rad_pay`.`price`) AS `price`,((`dbjlm`.`rad_pay`.`price_tax` / 100) * sum(`dbjlm`.`rad_pay`.`price`)) AS `ppn`,sum(`dbjlm`.`rad_pay`.`discount`) AS `discount`,sum(`dbjlm`.`rad_pay`.`discount_tagihan`) AS `discount_tagihan`,`dbjlm`.`rad_pay`.`user` AS `kasir`,ifnull(`dbjlm`.`rad_pay`.`vendor`,`dbjlm`.`rad_pay`.`how`) AS `vendor` from `dbjlm`.`rad_pay` where `dbjlm`.`rad_pay`.`pay_date` ".$kon." group by substr(`dbjlm`.`rad_pay`.`invcode`,1,4),`dbjlm`.`rad_pay`.`pay_date`,`dbjlm`.`rad_pay`.`username` order by username,pay_date asc";
		return $this->db->query($dq)->result();

	}

    public function cekdata($id) {
        
        $dq ="select substr(`dbjlm`.`rad_pay`.`invcode`,1,4) AS `thn`,`dbjlm`.`rad_pay`.`username` AS `username`,ifnull(`dbjlm`.`rad_pay`.`name`,'JUMLAH') AS `nama`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 1,`dbjlm`.`rad_pay`.`price`,0)) AS `JAN`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 2,`dbjlm`.`rad_pay`.`price`,0)) AS `FEB`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 3,`dbjlm`.`rad_pay`.`price`,0)) AS `MAR`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 4,`dbjlm`.`rad_pay`.`price`,0)) AS `APR`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 5,`dbjlm`.`rad_pay`.`price`,0)) AS `MEI`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 6,`dbjlm`.`rad_pay`.`price`,0)) AS `JUN`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 7,`dbjlm`.`rad_pay`.`price`,0)) AS `JUL`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 8,`dbjlm`.`rad_pay`.`price`,0)) AS `AGU`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 9,`dbjlm`.`rad_pay`.`price`,0)) AS `SEP`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 10,`dbjlm`.`rad_pay`.`price`,0)) AS `OKT`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 11,`dbjlm`.`rad_pay`.`price`,0)) AS `NOP`,sum(if(substr(`dbjlm`.`rad_pay`.`invcode`,5,2) = 12,`dbjlm`.`rad_pay`.`price`,0)) AS `DES`,sum(`dbjlm`.`rad_pay`.`price`) AS `TOTAL` from `dbjlm`.`rad_pay` where `dbjlm`.`rad_pay`.`username` = '".$id."' group by substr(`dbjlm`.`rad_pay`.`invcode`,1,4),`dbjlm`.`rad_pay`.`username`";
        return $this->db->query($dq)->result();

    }


}