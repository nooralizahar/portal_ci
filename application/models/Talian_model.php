<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Talian_model extends CI_Model {

    public function simtal() {

        $post = $this->input->post();
        $post["idta"] = $this->input->post("idta");
        $post["tgta"] = $this->input->post("tgta");
        $post["tgbu"] = date("Y-m-d H:i:s");
        $post["usbu"] = $this->session->userdata("id_pengguna");
        if(isset($post["btnSimpan"])) {
            unset($post["btnSimpan"]);
            unset($post["nama"]);
            unset($post["dept"]);
            $sim = $this->db->insert("app_latrop_nailat",$post);

            $insert_id = $this->db->insert_id();

            return  $insert_id;

            return $sim;
        } else return false;
    }

    public function tambar() {

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            unset($post["btnSimpan"]);
            unset($post["jeba_ddl"]);
            unset($post["jeba_txt"]);

            if (file_exists($_FILES['attach']['tmp_name'])) {

                $config['upload_path']          = 'assets/uploads/po/';
                $config['allowed_types']        = 'jpg|png|pdf|gif|xls|xlsx|jpeg';
                $config['overwrite']            = 1;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('attach'))
                {
                    $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      Data gagal ditambahkan </div>');
                }
                else
                {
                    $post['attach'] = $this->upload->data("file_name");

                    $sim = $this->db->insert("app_latrop_nailat_rel", $post);

                    $insert_id = $this->db->insert_id();

                    return  $insert_id;
                }
            } else {
                $sim = $this->db->insert("app_latrop_nailat_rel", $post);

                $insert_id = $this->db->insert_id();

                return  $insert_id;
            }
        } else return false;
    }

    public function pusbar($id){

        $dat = $this->db->where('idre',$id)->delete("app_latrop_nailat_rel");
        return $dat;
    }

    public function dapure(){
        $dat = $this->db->where('status_approve', 1)->order_by('idta', 'DESC')->get("v_app_latrop_nailat")->result();
        return $dat;
    }

    public function pureku(){
        $dat = $this->db->where("usbu",$this->session->userdata("id_pengguna"))->get("v_app_latrop_nailat")->result();
        return $dat;
    }

    public function pureprove(){
        $dat = $this->db->join('pengguna', 'v_app_latrop_nailat.usbu = pengguna.id_pengguna', 'left')
        ->where("pengguna.id_spv", $this->session->userdata("id_pengguna"))->get("v_app_latrop_nailat")->result();
        return $dat;
    }

    public function darang($id){
        $dat = $this->db->where('idta',$id)->get("app_latrop_nailat_rel")->result();
        return $dat;

    }

    public function depure($id){
        $dat = $this->db->where('idta',$id)->get("v_app_latrop_nailat")->row();
        return $dat;

    } 

    public function cbjeba(){
        $dat = $this->db->get("v_nailat_jeba")->result();
        return $dat;

    }

    public function approval($id, $data)
    {
        $this->db->update('app_latrop_nailat', $data, ['idta' => $id]);
        return true;
    }

    public function update_harga($id, $data)
    {
        $this->db->update('app_latrop_nailat_rel', $data, ['idre' => $id]);
        return true;
    }

    public function update_juba($id, $data)
    {
        $this->db->update('app_latrop_nailat_rel', $data, ['idre' => $id]);
        return true;
    }

    public function save_po($data)
    {
        $this->db->insert("app_latrop_nailat_po", $data);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
    }

    public function get_po($id)
    {
        $data = $this->db->where('idta', $id)->get("app_latrop_nailat_po")->row();
        return $data;
    }

    public function get_nailat($id)
    {
        $data = $this->db->where('idta', $id)->get("app_latrop_nailat")->row();
        return $data;
    }

    public function get_pengguna($id)
    {
        $data = $this->db->where('id_pengguna', $id)->get("v_pengguna_login")->row();
        return $data;
    }

    public function get_nailat_rel($id)
    {
        $data = $this->db->where('idta', $id)->get("app_latrop_nailat_rel")->result();
        return $data;
    }

    public function autoApprove($id, $data)
    {
        $this->db->update('app_latrop_nailat', $data, ['idta' => $id]);
        return true;
    }

    public function alamat()
    {
        $dat = $this->db->get("app_latrop_nailat_po")->result();
        return $dat;
    }


    public function update_po($id, $data)
    {
        $this->db->update('app_latrop_nailat', $data, ['idta' => $id]);
        return true;
    }


    public function ignore_po($id, $data)
    {
        $this->db->update('app_latrop_nailat', $data, ['idta' => $id]);
        return true;
    }

    public function delete_po($data)
    {
        $this->db->where($data);
        $this->db->delete('app_latrop_nailat_po');
        return true;
    }
}