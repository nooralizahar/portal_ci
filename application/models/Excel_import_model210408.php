<?php
class Excel_import_model extends CI_Model
{
	function select()
	{
		$this->db->order_by('CustomerID', 'DESC');
		$query = $this->db->get('tbl_customer');
		return $query;
	}

    public function pakpro(){
        $query = $this->db->get('t_omorp_pak')->result();
        return $query;
    }
    
    public function pilbla($id) {
        $pil = $this->db->where("idfile",$id)->get("t_tsalb")->result();

        foreach ($pil as $p) {

            $this->kirimwa($p->hp,$p->nama,$p->username,$p->alamat);
            sleep(3);

            $datupd = array(
            'tgblas' => date('Y-m-d H:i:s'),
            'usblas' => $this->session->userdata('id_pengguna')
            );

            $this->db->where('id',$p->id)->update('t_tsalb',$datupd);


            
        }

            $datfil = array(
            'tgbl' => date('Y-m-d H:i:s'),
            'usbl' => $this->session->userdata('id_pengguna')
            );

            $this->db->where('id',$id)->update('t_tsalb_elif',$datfil);

        return $pil;

    }

    public function pilebl($id) {
        $pil = $this->db->where("idfile",$id)->get("t_tsalb")->result();
        foreach ($pil as $p) {

            $this->kirimem($p->email,$p->hp,$p->nama,$p->username,$p->alamat);
            sleep(3);

            $this->session->set_flashdata('pesan', 'Kirim email ke alamat '.$p->email);

            $datupd = array(
            'tgebla' => date('Y-m-d H:i:s'),
            'usebla' => $this->session->userdata('id_pengguna')
            );

            $this->db->where('id',$p->id)->update('t_tsalb',$datupd);
            
        }
            $datfil = array(
            'tgeb' => date('Y-m-d H:i:s'),
            'useb' => $this->session->userdata('id_pengguna')
            );

            $this->db->where('id',$id)->update('t_tsalb_elif',$datfil);

        return $pil;

    }

    public function setemail(){
            //$this->db->select("*");
            $this->db->from("set_email");
            $sem = $this->db->get()->row();
            return $sem;
    }

    public function simemail(){

        $post = $this->input->post();
        unset($post["btnSubmit"]);
        $sim=$this->db->update("set_email",$post);
        return $sim;
    }

    function kirimem($alx,$hpx,$nama,$usrn,$alam){
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');

        $dat=$this->setemail();
        $alamat=$alx;//str_replace('%10','@',$alx);
        // PHPMailer object
        $mail = $this->phpmailer_lib->load();

        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = $dat->smtp;//'smtp.email.co.id';
        $mail->SMTPAuth = true;
        $mail->Username = $dat->email;//'email@lengkap.co.id';
        $mail->Password = $dat->password;//'password';
        $mail->SMTPSecure = $dat->secure;//'tls';
        $mail->Port     = $dat->port;//587;

        $mail->setFrom($dat->email, $dat->nama);
        //$mail->addReplyTo('toby@jlm.net.id', 'Balas');

        // Add a recipient
        //$mail->addAddress('toby@jlm.net.id');
        $mail->addAddress($alamat);
        // Add cc or bcc 
        //$mail->addCC('droid.jig@gmail.com');
        //$mail->addBCC('droid.jig@gmail.com');

        // Email subject
        $mail->Subject = 'Email Pelanggan Bnetfit #'.$usrn;

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = '<p>Pelanggan Bnetfit yang terhormat,<br>
Kami dengan ini menginformasikan bahwa id Anda termasuk dalam id pergantian sistem yang baru dari Ebilling menjadi IBOSS.dampak dari perpindahan sistem antara lain :<br>

1. Untuk jatuh tempo atau tanggal softblock menjadi tanggal 22.<br>
2. Untuk Virtual account BCA, Danamon , Mandiri statis setiap bulan sama.<br>
3. Terdapat tambahan biaya transfer bank BCA, Mandiri dan Danamon sebesar Rp. 5.000.<br>
4. Pembayaran tunai ke kantor pun berubah untuk 10 Mbps ialah Rp 224.000, untuk 20 Mbps ialah Rp324.000,untuk 30 Mbps ialah Rp489.000, untuk 50 Mbps ialah Rp764.000.<br>
5. Untuk pembayaran Via Alfamart di sistem baru ini sudah tidak bisa.<br>
6. Untuk pembayaran Via Alfamart diubah menjadi pembayaran Via Indomaret.<br>
7. Pembayaran Via Indomaret dikenakan biaya admin sebesar Rp. 7.000 diluar dari invoice.<br>
8. Untuk Pembayaran Via Indomaret dilakukan dengan cara menginformasikan user id customer dan menginformasikan ingin membayar Bnetfit.<br>
9. Untuk Pembayaran hanya bisa via 3 bank diatas dan tunai di kantor kami atau di Indomaret terdekat.<br>
        </p>

<p>Sistem Pembayaran yang baru dapat di akses melalui https://my.bnetfit.id</p>

<p>
Nama Pelanggan : '.$nama.'<br>
Alamat         : '.$alam.'<br>
User        : '.$usrn.'<br>
Password    : '.$hpx.'<br><br>  

Nomor VA Pembayaran JLM :<br><br>

Bank Mandiri : 8891897 + '.$usrn.'<br>
Panduan lengkap klik disini <a href="https://bit.ly/3cNb402" target="_blank">https://bit.ly/3cNb402</a><br>
Bank Danamon : 8880097 + '.$usrn.'<br>
Bank BCA : 1068597 + '.$usrn.'<br>
Panduan lengkap klik disini <a href="https://bit.ly/3ui0q7A" target="_blank">https://bit.ly/3ui0q7A</a><br><br>

Melalui Indomaret sebut Pembayaran Bnetfit, No. Pelanggan '.$usrn.'<br><br></p>

<p>Dapat kami informasikan untuk nominal pembayaran yang harus dilakukan adalah total tagihan + biaya jasa pembayaran dari pihak bank sebagai berikut :<br><br>

Bank BCA : Invoice Amount + Rp 5.000<br>
Bank Mandiri/Danamon : Invoice Amount + Rp 5.000<br>
Biaya Indomaret : Invoice Amount + Rp 7.000<br></p>

<p>
(Dimohon saat sebelum melakukan transfer dicek nominalnya)<br>
Di bantu untuk sesuaikan dengan Bank yang tersedia, apabila berbeda Bank tidak dapat dilakukan transaksi untuk pembayaran<br><br>
Untuk alternatif pembayaran bisa melalui pembayaran langsung ke kantor.<br></p>';
        $mail->Body = $mailContent;

        // Send email
        if(!$mail->send()){
            echo 'Notifikasi gagal di kirim.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Library berjalan dengan baik, notifikasi berhasil dikirim.';
        }
    }


    function kirimwa($hpx,$nama,$usrn,$alam){
            //Awal notif WA

            $apiURL='https://api.chat-api.com/instance236853/';
            $token='bajshal5wgwvg6cf';

            //$message = $_GET['pesan'];
            //$phone = $_GET['phone'];
            if(substr($hpx,0,1)==0){$hp='62'.substr($hpx,1,12);}elseif(substr($hpx,0,2)==62){$hp=$hpx;}else{$hp='62'.$hpx;}
            $phone = $hp;
            $message ='Pelanggan Bnetfit yang terhormat,
Kami dengan ini menginformasikan bahwa id Anda termasuk dalam id pergantian sistem yang baru dari Ebilling menjadi IBOSS.
dampak dari perpindahan sistem antara lain :

1. Untuk jatuh tempo atau tanggal softblock menjadi tanggal 22.
2. Untuk Virtual account BCA, Danamon , Mandiri statis setiap bulan sama.
3. Terdapat tambahan biaya transfer bank BCA, Mandiri dan Danamon sebesar Rp. 5.000.
4. Pembayaran tunai ke kantor pun berubah untuk 10 Mbps ialah Rp 224.000, untuk 20 Mbps ialah Rp324.000,untuk 30 Mbps ialah Rp489.000, untuk 50 Mbps ialah Rp764.000.
5. Untuk pembayaran Via Alfamart di sistem baru ini sudah tidak bisa.
6. Untuk pembayaran Via Alfamart diubah menjadi pembayaran Via Indomaret.
7. Pembayaran Via Indomaret dikenakan biaya admin sebesar Rp. 7.000 diluar dari invoice.
8. Untuk Pembayaran Via Indomaret dilakukan dengan cara menginformasikan user id customer dan menginformasikan ingin membayar Bnetfit.
9. Untuk Pembayaran hanya bisa via 3 bank diatas dan tunai di kantor kami atau di Indomaret terdekat.

Apabila ada pertanyaan lebih lanjut, bisa langsung menghubungi Call Center kami atau di nomor ini.

Sistem Pembayaran yang baru dapat di akses melalui https://my.bnetfit.id

Nama Pelanggan : '.$nama.'
Alamat         : '.$alam.'
User        : '.$usrn.'
Password    : '.$hpx.'  

Nomor VA Pembayaran JLM :

Bank Mandiri : 8891897 + '.$usrn.'
Panduan lengkap klik disini https://bit.ly/3cNb402
Alternatif link panduan Bank Mandiri klik disini https://docs.google.com/viewer?url=https://my.bnetfit.id/panduan/PembayaranInternetBNETFIT_via_Mandiri.pdf&pdf=true
Bank Danamon : 8880097 + '.$usrn.'
Bank BCA : 1068597 + '.$usrn.'
Panduan lengkap klik disini https://bit.ly/3ui0q7A
Alternatif link panduan Bank BCA klik disini https://docs.google.com/viewer?url=https://my.bnetfit.id/panduan/PembayaranInternetBNETFIT_via_BCA.pdf&pdf=true

Melalui Indomaret sebut Pembayaran Bnetfit, No. Pelanggan '.$usrn.'

Dapat kami informasikan untuk nominal pembayaran yang harus dilakukan adalah total tagihan + biaya jasa pembayaran dari pihak bank sebagai berikut :

Bank BCA : Invoice Amount + Rp 5.000
Bank Mandiri/Danamon : Invoice Amount + Rp 5.000
Biaya Indomaret : Invoice Amount + Rp 7.000


(Dimohon saat sebelum melakukan transfer dicek nominalnya)
Di bantu untuk sesuaikan dengan Bank yang tersedia, apabila berbeda Bank tidak dapat dilakukan transaksi untuk pembayaran.

Untuk alternatif pembayaran bisa melalui pembayaran langsung ke kantor.';

            $data = json_encode(
                array(
                    'chatId'=>$phone.'@c.us',
                    'body'=>$message
                )
            );
            $url = $apiURL.'message?token='.$token;
            $options = stream_context_create(
                array('http' =>
                    array(
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/json',
                        'content' => $data
                    )
                )
            );
            $response = file_get_contents($url,false,$options);
            echo $response; 

            // Akhir Notif WA
    }

    public function dfile() {
        $data = $this->db->order_by("id","desc")->get("v_t_tsalb_elif")->result();
        return $data;
    }
    
    public function dfpro() {
        $data = $this->db->order_by("id","desc")->get("t_omorp")->result();
        return $data;
    }
     public function lfile() {
        $ldata = $this->db->order_by("id","desc")->limit(1)->get("t_tsalb_elif")->result();
        return $ldata;
     }


	function insert($data)
	{
		$this->db->insert_batch('t_datame', $data);
	}

	function insertblast($data)
	{
		$this->db->insert_batch('t_tsalb', $data);
	}

    function iblapro()
    {

        $post = $this->input->post();

        if(isset($post["btnImport"])) {

            $post = array(
            'konten' => $this->input->post('konten'),
            'target' => $this->input->post('target'),
            'tgbua' => date('Y-m-d H:i:s'),
            'usbua' => $this->session->userdata('id_pengguna')
            );
            $ibp=$this->db->insert('t_omorp',$post);
        }

        return $ibp;
    }
    function hapbla($data)
    {
        $this->db->where('idfile',$data)->delete('t_tsalb');
        $d=$this->db->where('id',$data)->delete('t_tsalb_elif');
        return $d;
    }
	public function ulobla() {
      $post = $this->input->post();  
      unset($post["btnImport"]);       
      
      if($_FILES["nafile"]["name"][0] != "") {
                $dathsl = $this->upblas($_FILES["nafile"]);
                if($dathsl == false) {
                    redirect(base_url("panbd/blasin/?err"));
                    exit();
                }
                $post["nafi"] = json_encode($dathsl);
            }

        $post["tgup"] = date("Y-m-d H:i:s");
        $post["usup"] = $this->session->userdata("id_pengguna");

        if(isset($post["nafi"])) {
            
            $bla=$this->db->insert("t_tsalb_elif",$post);
            return $bla;
        } else return false;
    }

    private function upblas($files,$path = "assets/uploads/tsalb/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'xls|xlsx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid(). "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }







}
