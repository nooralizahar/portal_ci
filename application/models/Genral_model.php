<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Genral_model extends CI_model{


    public function semua($t){
        return $this->db->get($t)->result();
    }

    public function gentam($t){
        return $this->db->get($t)->result();
    }

    public function gencbo($t,$k){
        return $this->db->select($k)->group_by($k)->get($t)->result();
    }

    public function gensim($t){
        $p=$this->input->post();
        unset($p["btnSimpan"]);
        $p["usbua"]=$this->idaku;
        $p["tgbua"]=date('Y-m-d H:i:s');
        return $this->db->insert($t,$p);
    }

    public function genrui($t,$k,$n,$c,$i){
        $b=array($k => $n);
        return $this->db->where($c,$i)->update($t,$b);
    }

    public function dathir($t,$k){
        return $this->db->order_by($k,"desc")->limit(1)->get($t)->result();
    }

    public function pildat($t,$k,$n){
        return $this->db->where($k,$n)->get($t)->result();
    }

    public function pilpus($t,$k,$n){
        return $this->db->where($k,$n)->delete($t);
    }

    public function hitdat($t){
        return $this->db->get($t)->num_rows();
    }

    public function pilhit($t,$k,$n){
        return $this->db->where($k,$n)->get($t)->num_rows();
    }

    public function tamakt($akt,$usr){
        $tgakt = date('Y-m-d H:i:s');
        
        if(empty($this->session->userdata("id_div"))){
            $iddiv =0;  
        }else{
            $iddiv = $this->session->userdata("id_div");
        }

        $dat = array(
                    "tgakt" => $tgakt,
                    "iddiv" => $iddiv,
                    "nmusr" => $usr,
                    "aktifitas" => $akt,
                    "status" => true
                );

    $sim = $this->db->insert("app_latrop_gol",$dat);
    return $sim;
    }

    public function rimsan($p=null){
        if($p==1){
            $d=$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data berhasil disimpan </div>');
        }elseif($p==2){
            $d=$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data gagal disimpan</div>');
        }elseif($p==3){
            $d=$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data berhasil dihapus </div>');
        }elseif($p==4){
            $d=$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data gagal dihapus </div>');
        }elseif($p==5){
            $d=$this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data detil sudah terisi, silahkan dihapus terlebih dahulu. </div>');

        } 

        return $d;

    }

}