<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Harga_model extends CI_model{

    public function segment_semua() {
        $data = $this->db->get("ts_agrah_seg")->result();
        return $data;
    }
    public function media_semua() {
        $data = $this->db->get("ts_aidem")->result();
        return $data;
    }
    public function request_semua() {
        $data = $this->db->get("ts_tseuqer")->result();
        return $data;
    }
    public function service_semua() {
        $data = $this->db->get("ts_ecivres")->result();
        return $data;
    }
	
    public function semua() {
        //$data = $this->db->get("ts_agrah")->result();
        //return $data;
        $this->db->select("ts_agrah.*,pengguna.nama_lengkap");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
         $this->db->order_by("tg_buat", "desc");
                
        return $this->db->get()->result();
    }  

    public function gpsales() {
        $this->db->select("ts_agrah.*,pengguna.nama_lengkap");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
        $this->db->where("ts_agrah.penerima",$this->session->userdata("username"));
        $this->db->order_by("tg_buat", "desc");
        
        return $this->db->get()->result();
    } 

        public function gpsaleso() {
        $this->db->select("ts_agrah.*,pengguna.nama_lengkap");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
        $this->db->where("ts_agrah.penerima","Presales");
        $this->db->order_by("tg_buat", "desc");
        
        return $this->db->get()->result();
    }

        public function gsalespv() {
        $this->db->select("ts_agrah.*,pengguna.nama_lengkap,pengguna.id_spv");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
        $this->db->where("ts_agrah.sales",$this->session->userdata("username"));
        $this->db->or_where("pengguna.id_spv",$this->session->userdata("id_pengguna"));
        $this->db->order_by("tg_buat", "desc");
        
        return $this->db->get()->result();
    }

    public function gsalesmg() {
        $this->db->select("ts_agrah.*,pengguna.nama_lengkap,pengguna.id_spv,pengguna.id_mgr");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
        $this->db->where("ts_agrah.sales",$this->session->userdata("username"));
        $this->db->or_where("pengguna.id_spv",$this->session->userdata("id_pengguna"));
        $this->db->or_where("pengguna.id_mgr",$this->session->userdata("id_pengguna"));
        $this->db->order_by("tg_buat", "desc");
        return $this->db->get()->result();
    }

        public function gsales() {
        $this->db->select("ts_agrah.*,pengguna.nama_lengkap");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
        $this->db->where("ts_agrah.sales",$this->session->userdata("username"));
        $this->db->order_by("tg_buat", "desc");
        
        return $this->db->get()->result();
    } 

    public function harga_bid($id) {
        $this->db->select("ts_agrah.*,pengguna.nama_lengkap");
        $this->db->from("ts_agrah");
        $this->db->where("ts_agrah.id_rha",$id);
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
               
        return $this->db->get()->row(); //Ok tested 
    } 
    
    public function pilpersal($pilih) {

        $data = $this->db->like("bln", $pilih)->order_by("jmlreq", "desc")->get("v_req_per_sales")->result();
        return $data;
    }

    public function pilperseg($pilih) {

        $data = $this->db->like("bln", $pilih)->order_by("jmlreq", "desc")->get("v_ts_req_segment")->result();
        return $data;
    }

    public function harga_id($id){
        $data = $this->db->where("id_rha", $id)->get("ts_agrah")->row();
        return $data; //Ok tested
    }

	public function minharga() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        
        if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_filex($_FILES["attach"]);
                if($gambar == false) {
                    redirect(base_url("panel/hargatamb/?err"));
                    exit();
                }
                $post["lampiran"] = json_encode($gambar);
            }



        if(isset($post["penerima"])) {
            $num = $this->db->insert("ts_agrah",$post);
            return $num;
        } else return false;
    }



	public function isiharga() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
         /*   $this->db->set($data);
            $this->db->where('id',$id);
            $update = $this->db->update('user'); */

        if(isset($post["id_rha"]) && isset($post["hg_instalasi"]) && isset($post["hg_bulanan"]) && isset($post["hg_assignby"]) && isset($post["tg_jwb"])) {

            $this->db->where("id_rha",$post["id_rha"]);
            $num = $this->db->update("ts_agrah",$post);
            return $num;
        } else return false;
    }

    public function ediharga() {

        $post = $this->input->post();
        unset($post["btnSimpan"]);

        if(isset($post["id_rha"]) && isset($post["hg_instalasi"]) && isset($post["hg_bulanan"]) && isset($post["tg_jwb"]) && isset($post["hg_edit"])) {
            $data = $this->db->where("id_rha",$post["id_rha"])->get("ts_agrah")->row();
            $post["hg_edit"]=$post["hg_edit"]+$data->hg_edit;
            
            $this->db->where("id_rha",$post["id_rha"]);
            $num = $this->db->update("ts_agrah",$post);
            return $num;
        } else return false;
    }
    private function upload_filex($files,$path = "assets/uploads/lampiran/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|jpeg|bmp|gif|doc|docx|xls|xlsx|ppt|pptx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }	



}