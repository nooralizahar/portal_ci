<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Harga_model extends CI_model{

// Awal Ditambah 20220301
    public function lihun(){
    
    $data = $this->db->select("YEAR(tg_buat) as thn")->group_by("YEAR(tg_buat)")->order_by("YEAR(tg_buat)","DESC")->get("ts_agrah")->result();        
    return $data;
    }
// Akhir Ditambah 20220301

// awal model quotation 


    public function uploquo() {
        $post = $this->input->post();
        
        unset($post["btnSubmit"]);       
      $id = $this->input->post('id');
      if($_FILES["attach"]["name"][0] != "") {
                $dathsl = $this->ulo_quo($_FILES["attach"]);
                if($dathsl == false) {
                    redirect(base_url("nuber/msdquo/".$id."?err"));
                    exit();
                }
                $post["msfile"] = json_encode($dathsl);
            }

        //$post["idbap"] = $idw;
        $post["mstgup"] = date("Y-m-d H:i:s");
        $post["msuser"] = $this->session->userdata("id_pengguna");



        if(isset($post["msfile"])) {
            unset($post["id"]);
             $quo = $this->db->where('id_rha',$id)->update("ts_agrah",$post);
             //$c=$this->procomp($idw);
             //$bap=$this->db->insert("tp_owatad_pab",$post);
            return $quo;
        } else return false;
    }

    public function quotil($id) {
        return $this->db->select("id_rha,msfile,mstgup,msuser")->where("id_rha",$id)->get('ts_agrah')->result();
    
    }

    private function ulo_quo($files,$path = "assets/uploads/quo/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|gif|xls|xlsx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() .'_'. md5($image) . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }

// akhir model quotation 



    public function haprha($id) {
        return $this->db->where('id_rha', $id)->delete('ts_agrah');
    }

    public function gcro() {

        $this->db->select("v_ts_agrah_apv.*");
        $this->db->from("v_ts_agrah_apv");
        $this->db->where("v_ts_agrah_apv.sales",$this->session->userdata("username"));
        $this->db->order_by("tg_buat", "desc");
       
        return $this->db->get()->result();
    } 

    public function gcropv() {

        $this->db->select("v_ts_agrah_apv.*");
        $this->db->from("v_ts_agrah_apv");
        $this->db->where("v_ts_agrah_apv.sales",$this->session->userdata("username"));
        $this->db->or_where("v_ts_agrah_apv.id_spv",$this->session->userdata("id_pengguna"));
        $this->db->order_by("tg_buat", "desc"); 

        /*$this->db->select("ts_agrah.*,pengguna.nama_lengkap,pengguna.id_spv");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
        $this->db->where("ts_agrah.sales",$this->session->userdata("username"));
        $this->db->or_where("pengguna.id_spv",$this->session->userdata("id_pengguna"));
        $this->db->order_by("tg_buat", "desc");*/
        
        return $this->db->get()->result();
    }

    public function segment_semua($i) {
        if($i==1){
         $data = $this->db->get("ts_agrah_seg")->result();
        }else{
         $data = $this->db->where('id_seg <',7)->get("ts_agrah_seg")->result();   
        }
        
        return $data;
    }
    public function media_semua() {
        $data = $this->db->get("ts_aidem")->result();
        return $data;
    }
    public function request_semua() {
        $data = $this->db->get("ts_tseuqer")->result();
        return $data;
    }
    public function service_semua() {
        $data = $this->db->get("ts_ecivres")->result();
        return $data;
    }
	
    public function semua() {

        $this->db->select("ts_agrah.*,pengguna.nama_lengkap");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
         $this->db->order_by("tg_buat", "desc");
                
        return $this->db->get()->result();
    }  

    public function semnmc() {

        $this->db->select("v_ts_agrah_apv.*");
        $this->db->from("v_ts_agrah_apv");
        $this->db->where('ket like','%nmc%');
        $this->db->or_where('nmc !=','');
        $this->db->order_by("tg_buat", "desc");
                
        return $this->db->get()->result();
    }  


    public function gpsales() {
        $this->db->select("ts_agrah.*,pengguna.nama_lengkap");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
        $this->db->where("ts_agrah.penerima",$this->session->userdata("username"));
        $this->db->order_by("tg_buat", "desc");
        
        return $this->db->get()->result();
    } 

        public function gpsaleso() {

        $this->db->select("v_ts_agrah_apv.*");
        $this->db->from("v_ts_agrah_apv");
        $this->db->where("v_ts_agrah_apv.tg_buat like ", "2022-%"); // 13 April 2022
        $this->db->where("v_ts_agrah_apv.penerima","Presales");
        $this->db->or_where("v_ts_agrah_apv.penerima","Manage Service");
        $this->db->order_by("tg_buat", "desc");  
          
        /*$this->db->select("ts_agrah.*,pengguna.nama_lengkap");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
        $this->db->where("ts_agrah.penerima","Presales");
        $this->db->order_by("tg_buat", "desc");*/
        
        return $this->db->get()->result();
    }


    public function gmanser() {  // untuk manage service

        $this->db->select("v_ts_agrah_apv.*");
        $this->db->from("v_ts_agrah_apv");
        $this->db->where("v_ts_agrah_apv.penerima","Manage Service");
        $this->db->order_by("tg_buat", "desc");  
        
        return $this->db->get()->result();
    }

    public function gmanser_id($id) {  // untuk manage service

        $this->db->select("v_ts_agrah_apv.*");
        $this->db->from("v_ts_agrah_apv");
        $this->db->where("id_rha", $id);
        $this->db->where("v_ts_agrah_apv.penerima","Manage Service");
        $this->db->order_by("tg_buat", "desc");  
        
        return $this->db->get()->row();
    }

    public function insert_log_email($params)
    {
        $this->db->insert("ts_agrah_email", $params);
        return true;
    }

    public function update_status_agrah($id, $status)
    {
        $this->db->update('ts_agrah', $status, ['id_rha' => $id]);
        return true;
    }

        public function gsalespv() {

        $this->db->select("v_ts_agrah_apv.*");
        $this->db->from("v_ts_agrah_apv");
        $this->db->where("v_ts_agrah_apv.sales",$this->session->userdata("username"));
        $this->db->or_where("v_ts_agrah_apv.id_spv",$this->session->userdata("id_pengguna"));
        $this->db->order_by("tg_buat", "desc"); 

        /*$this->db->select("ts_agrah.*,pengguna.nama_lengkap,pengguna.id_spv");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
        $this->db->where("ts_agrah.sales",$this->session->userdata("username"));
        $this->db->or_where("pengguna.id_spv",$this->session->userdata("id_pengguna"));
        $this->db->order_by("tg_buat", "desc");*/
        
        return $this->db->get()->result();
    }

    public function gsalesmg() {


        $this->db->select("v_ts_agrah_apv.*");
        $this->db->from("v_ts_agrah_apv");
        $this->db->where("v_ts_agrah_apv.sales",$this->session->userdata("username"));
        $this->db->or_where("v_ts_agrah_apv.id_spv",$this->session->userdata("id_pengguna"));
        $this->db->or_where("v_ts_agrah_apv.id_mgr",$this->session->userdata("id_pengguna"));
        $this->db->order_by("tg_buat", "desc"); 
        /*
        $this->db->select("ts_agrah.*,pengguna.nama_lengkap,pengguna.id_spv,pengguna.id_mgr");
        $this->db->from("ts_agrah");
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
        $this->db->where("ts_agrah.sales",$this->session->userdata("username"));
        $this->db->or_where("pengguna.id_spv",$this->session->userdata("id_pengguna"));
        $this->db->or_where("pengguna.id_mgr",$this->session->userdata("id_pengguna"));
        $this->db->order_by("tg_buat", "desc");*/
        return $this->db->get()->result();
    }

        public function gsales() {

        $this->db->select("v_ts_agrah_apv.*");
        $this->db->from("v_ts_agrah_apv");
        $this->db->where("v_ts_agrah_apv.sales",$this->session->userdata("username"));
        $this->db->order_by("tg_buat", "desc");
       
        return $this->db->get()->result();
    } 

    public function harga_bid($id) {
        $this->db->select("ts_agrah.*,pengguna.nama_lengkap");
        $this->db->from("ts_agrah");
        $this->db->where("ts_agrah.id_rha",$id);
        $this->db->join("pengguna","pengguna.username = ts_agrah.sales","left");
               
        return $this->db->get()->row(); //Ok tested 
    } 

    public function pildatwops($n,$b) {
        $data = $this->db->where("kbl", $b)->order_by("tg_issue", "asc")->get("v_wo_per_sales_det")->result();
        return $det;
    }
    
    public function pildatpre($n,$b) {
        $nama=str_replace('%20',' ',$n);
        $bl=date('Y')."-".$b;
            
        if($nama=="TOTAL"){
        $data = $this->db->like("tgljwb", $bl)->order_by("tgljwb", "asc")->get("v_vipot_presales_detil")->result();            
        }else{
        $data = $this->db->where("nama_lengkap",$nama)->like("tgljwb", $bl)->order_by("tgljwb", "asc")->get("v_vipot_presales_detil")->result();    
        }


        return $data;
    }

    public function pilpersal($pilih) {

        $data = $this->db->like("bln", $pilih)->order_by("jmlreq", "desc")->get("v_req_per_sales")->result();
        return $data;
    }

   public function pilwo($pilih) {

        $data = $this->db->like("bln", $pilih)->order_by("jml", "asc")->get("v_wo_summary_hit")->result();
        return $data;
    }
    
    public function pilperseg($pilih) {

        $data = $this->db->like("bln", $pilih)->order_by("jmlreq", "desc")->get("v_ts_req_segment")->result();
        return $data;
    }
    public function wosummary($th=null) {

        if(empty($th)){$thn=date('Y');}else{$thn=$th;}

        $data = $this->db->where('thn',$thn)->order_by("TOTAL", "asc")->get("v_wo_hasil_pivot")->result();
        return $data;
    }

    public function wosales($th=null) {

        if(empty($th)){$thn=2021;}else{$thn=$th;}

        $data = $this->db->where('thn',$thn)->order_by("TOTAL", "asc")->get("v_wo_per_sales_hit")->result();
        return $data;
    }

    public function wosegme($th=null) {

        if(empty($th)){$thn=2021;}else{$thn=$th;}

        $data = $this->db->where('thn',$thn)->order_by("TOTAL", "asc")->get("v_wo_summary_seg_hit")->result();
        return $data;
    }

     public function wostatu($th=null) {

        if(empty($th)){$thn=2021;}else{$thn=$th;}

        $data = $this->db->where('thn',$thn)->order_by("TOTAL", "asc")->get("v_wo_status_pivot")->result();
        return $data;
    }   
    public function summary() {
        $data = $this->db->order_by("TOTAL", "desc")->get("v_summary_per_sales")->result();
        return $data;
    }
    public function summarypre($pilih) {
        //$data = $this->db->get("v_summary_presales")->result();
        if(empty($pilih)){
            $pilih=date('Y');
        }
        $data = $this->db->where("thn",$pilih)->get("v_summary_presales_by_year")->result();
        return $data;
    }
    public function summaryprethn($pilih) {

        if(empty($pilih)){
            $pilih=date('Y');
        }
        
        $data = $this->db->where("thn",$pilih)->group_by("thn")->get("v_total_presales_by_year")->result();
        return $data;
    }
    public function summarydur() {
        $data = $this->db->order_by("NAMA", "asc")->order_by("DURASI", "asc")->order_by("TOTAL", "desc")->get("v_summary_presales_durasi")->result();
        return $data;
    }
    public function harga_id($id){
        $data = $this->db->where("id_rha", $id)->get("v_ts_agrah_apv")->row();
        return $data; //Ok tested
    }

	public function minharga() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        
        if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_filex($_FILES["attach"]);
                if($gambar == false) {
                    redirect(base_url("panel/hargatamb/?err"));
                    exit();
                }
                $post["lampiran"] = json_encode($gambar);
            }



        if(isset($post["penerima"])) {
            $num = $this->db->insert("ts_agrah",$post);
            return $num;
        } else return false;
    }

    public function ubaharga($id) {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        
        if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_filex($_FILES["attach"]);
                if($gambar == false) {
                    redirect(base_url("panel/hargadit/?err"));
                    exit();
                }
                $post["lampiran"] = json_encode($gambar);
            }



        if(isset($post["penerima"])) {
            $num = $this->db->where("id_rha",$id)->update("ts_agrah",$post);
            return $num;
        } else return false;
    }


	public function isiharga() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        unset($post["hg_catatan_ddl"]);
        unset($post["hg_catatan_txt"]);
        unset($post["nm_custo"]);

        if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_filex($_FILES["attach"]);
                if($gambar == false) {
                    redirect(base_url("panel/hargatamb/?err"));
                    exit();
                }
                $post["lampirana"] = json_encode($gambar);
            }
         /*   $this->db->set($data);
            $this->db->where('id',$id);
            $update = $this->db->update('user'); */

        if(isset($post["id_rha"]) && isset($post["hg_instalasi"]) && isset($post["hg_bulanan"]) && isset($post["hg_assignby"]) && isset($post["tg_jwb"])) {

            $this->db->where("id_rha",$post["id_rha"]);
            $num = $this->db->update("ts_agrah",$post);
            return $num;
        } else return false;
    }

public function isiitembp($post){

            unset($post["btnSimpan"]);  
            $simpan = $this->db->insert("ts_agrah_bp",$post);
            return $simpan;      
}

public function isirevenue($post){

            unset($post["btnSimpan"]); 
            unset($post["lama_ddl"]);
            unset($post["lama_txt"]); 
            $simpan = $this->db->insert("ts_agrah_rve",$post);
            return $simpan;      
}

public function cekrve($id){

        $cek =$this->db->where("idrha", $id)->get("ts_agrah_rve")->num_rows();
        return $cek;      
}

public function jmlbp($id){

$query=$this->db->query("SELECT idrha as idrha,sum(`toven`) as totven,sum(`tojlm`) as totjlm FROM `ts_agrah_bp` WHERE idrha=$id GROUP by idrha");
return $query->row();

}

public function cekbp($id){

$query=$this->db->query("SELECT idrha as idrha,sum(`toven`) as totven,sum(`tojlm`) as totjlm FROM `ts_agrah_bp` WHERE idrha=$id GROUP by idrha");
return $query->num_rows();

}

public function pilitembp($pilih) {

        $data = $this->db->where("idrha", $pilih)->order_by("id", "asc")->get("ts_agrah_bp")->result();
        return $data;
}

public function pilbprve($pilih) {

        $data = $this->db->where("idrha", $pilih)->get("ts_agrah_rve")->row();;
        return $data;
}

public function ediharga() {

        $post = $this->input->post();
        unset($post["btnSimpan"]);

        if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_filex($_FILES["attach"]);
                if($gambar == false) {
                    redirect(base_url("panel/?err"));
                    exit();
                }
                $post["lampirana"] = json_encode($gambar);
            }


        if(isset($post["id_rha"]) && isset($post["hg_instalasi"]) && isset($post["hg_bulanan"]) && isset($post["tg_jwb"]) && isset($post["hg_edit"])) {
            $data = $this->db->where("id_rha",$post["id_rha"])->get("ts_agrah")->row();
            $post["hg_edit"]=$post["hg_edit"]+$data->hg_edit;
            
            $this->db->where("id_rha",$post["id_rha"]);
            $num = $this->db->update("ts_agrah",$post);
            return $num;
        } else return false;
    }

private function upload_filex($files,$path = "assets/uploads/lampiran/"){
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|jpeg|bmp|gif|doc|docx|xls|xlsx|ppt|pptx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
}	



}