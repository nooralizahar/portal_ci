<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Odpdata_model extends CI_Model {


    public function semua() {
    	$data = $this->db->order_by('tg_cre','desc')->get("v_t_odp")->result();
        return $data;
    }

    public function oltref($r) {
        $rx=str_replace('%20', ' ', $r);
        $data = $this->db->where('nmolt',$rx)->get("v_olt_peta")->result();
        return $data;
    }

    public function odcref($r) {
        $data = $this->db->where('idodc',$r)->order_by('member','asc')->get("v_odc_member_final")->result();
        return $data;
    }

    public function cari($nama) {
        if(empty($nama)){
        
        $data = $this->db->order_by('tg_cre','desc')->limit(5)->get("v_t_odp")->result();
        }else{
        $data = $this->db->like('nama', $nama, 'both')->order_by('tg_cre','desc')->get("v_t_odp")->result();
        }
        return $data;
    }

    public function odpcbx() {
        $data = $this->db->select("idodp,nama,alamat,kordinat")->where('idodc',0)->order_by('nama','desc')->get("v_odc_odp_brig")->result();
        return $data;
    }

    public function semlok() {
        $data = $this->db->order_by('lokasi','asc')->get("t_odp_lok")->result();
        return $data;
    }

    public function tambah() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["tg_cre"] = date("Y-m-d H:i:s");
        $post["us_cre"] = $this->session->userdata("username");
        if(isset($post["nama"])) {
            $num = $this->db->insert("t_odp",$post);
            return $num;
        } else return false;
    }

    public function oltsem() {
        $data = $this->db->order_by('tgbua','desc')->get("t_olt")->result();
        return $data;
    }


    public function olttam() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["tgbua"] = date("Y-m-d H:i:s");
        $post["usbua"] = $this->session->userdata("username");
        if(isset($post["nmolt"])) {
            $num = $this->db->insert("t_olt",$post);
            return $num;
        } else return false;
    }

    public function tampel() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["tg_cre"] = date("Y-m-d H:i:s");
        $post["us_cre"] = $this->session->userdata("username");
        if(isset($post["userid"])) {
            $num = $this->db->insert("t_odp_rel",$post);
            return $num;
        } else return false;
    }


    public function pilih($param) {

        $data = $this->db->where("idodp", $param)->get("t_odp")->result();//->limit(1)
        return $data;

    }
    public function odphapus($param) {
        $hapus=$this->db->where("idodp",$param)->delete("t_odp");
        $hapus=$this->db->where("idodp",$param)->delete("t_odp_rel");
    return $hapus;

    }

    public function pilihp($param) {

        $data = $this->db->where("idodpr", $param)->get("t_odp_rel")->result();//->limit(1)
        return $data;

    }

    public function pelanggan($param) {

        $data = $this->db->where("idodp", $param)->order_by("noport","ASC")->get("t_odp_rel")->result();//->limit(1)
        return $data;

    }

    public function pelsemua() {

        $data = $this->db->get("v_t_odp_rel")->result();//->limit(1)
        return $data;

    }

    public function pelodpset() {

        $data = $this->db->where("idodp !=", 1)->get("v_t_odp_rel")->result();//->limit(1)
        return $data;

    }

    public function ubah() {
        
        if(!empty($this->input->post("status"))){$stat=1;}else{$stat=0;}
        $id=$this->input->post("id");
        $tgup=date("Y-m-d H:i:s");
        $usup=$this->session->userdata("username");
        $nmed=$this->input->post("nama");
        $aled=$this->input->post("alamat");
        $loed=$this->input->post("lokasi");
        $sted=$stat;
        $value=$this->input->post("ver");
        //$checked = ($value === FALSE) ? 0 : 1;
        $koed=$this->input->post("kordinat");
        $kaed=$this->input->post("kapasitas");
        $oled=$this->input->post("oltrel");

        $kolom = ['tg_upd'=>$tgup,'us_upd'=>$usup,'nama'=>$nmed,'alamat'=>$aled,'lokasi'=>$loed,'status'=>$sted,'ver'=>$value,'kordinat'=>$koed,'oltrel'=>$oled,'kapasitas'=>$kaed];

        $data=$this->db->where('idodp', $id)->update('t_odp',$kolom );     
        return $data;
    }

        public function ubahp() {
        
        $id=$this->input->post("id");
        $ido=$this->input->post("idodp");
        $tgup=date("Y-m-d H:i:s");
        $usup=$this->session->userdata("username");
        $uied=$this->input->post("userid");
        $pled=$this->input->post("pelanggan");
        $aled=$this->input->post("alamat");
        $nped=$this->input->post("noport");
        $koed=$this->input->post("kordinat");

        $kolom = ['tg_upd'=>$tgup,'us_upd'=>$usup, 'idodp'=>$ido,'userid'=>$uied,'pelanggan'=>$pled, 'alamat'=>$aled,'noport'=>$nped,'kordinat'=>$koed];

        $data=$this->db->where('idodpr', $id)->update('t_odp_rel',$kolom );     
        return $data;
    }


    public function hapusp($idp) {
        return $this->db->where('idodpr', $idp)->delete('t_odp_rel');
    }

    public function updqr($qid,$qhash,$qfile) {
        
        //$id=$this->input->post("id");
        $tgup=date("Y-m-d H:i:s");
        $usup=$this->session->userdata("username");
        //$nmed=$this->input->post("nama");
        //$aled=$this->input->post("alamat");
        //$koed=$this->input->post("kordinat");
        //$kaed=$this->input->post("kapasitas");

        $kol = ['tg_upd'=>$tgup,'us_upd'=>$usup,'fileqr'=>$qfile,'qrhash'=>$qhash];

        $data=$this->db->where('idodp', $qid)->update('t_odp',$kol );     
        return $data;
    }

    public function odppidai($param) {

            $data = $this->db->where("qrhash",$param)->get("v_t_odp")->result();
            return $data;

    }

    public function cekpidai($param) {

            $data = $this->db->where("qrhash",$param)->get("v_t_odp")->num_rows();
            return $data;

    }

    public function odpuse($idodp) {

            $data = $this->db->where("idodp",$idodp)->get("t_odp_rel")->num_rows();
            return $data;

    }

    public function odppel($idodp) {

            $data = $this->db->where("idodp",$idodp)->get("t_odp_rel")->result();
            return $data;

    }

    public function pilodp($pilih,$param) {

            $data = $this->db->like($pilih,$param)->get("v_t_odp")->result();
            return $data;

    }

    public function pilpel($pilih,$param) {

            $data = $this->db->like($pilih,$param)->get("v_t_pelanggan")->result();
            return $data;

    }

    public function selodp($param) {

        $data = $this->db->where("idodp", $param)->get("v_t_odp")->result();//->limit(1)
        return $data;

    }



}