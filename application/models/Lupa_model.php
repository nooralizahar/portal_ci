<?php

class Lupa_model extends CI_Model{

    function __construct(){
        
        parent::__construct();
        //$this->load->database();
        $this->load->library('session');
    }
    


    public function KirimE($alamat){
        $waktu = date('Y-m-d H:i:s');
        $subject = 'Permintaan Setting Ulang Sandi';  //email subject
        $hashala = password_hash($alamat, PASSWORD_DEFAULT);
        $forgot = uniqid();
        $this->updforgot($alamat,$forgot);
        

            $this->load->library('phpmailer_lib');
            $mail = $this->phpmailer_lib->load();

            $email_admin = 'no-reply@jlm.net.id';
            $nama_admin = 'Aplikasi Portal - Lupa Sandi';
            $password_admin = 'jlm2021-@#$';

            $mail->isSMTP();
            $mail->SMTPKeepAlive = true;
            $mail->Charset  = 'UTF-8';
            $mail->IsHTML(true);
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = true;
            $mail->Host = "mail.jlm.net.id";
            $mail->SMTPSecure = "tls";
            $mail->Port = 587;
            $mail->Username = $email_admin;
            $mail->Password = $password_admin;
            $mail->WordWrap = 500;

            $mail->setFrom($email_admin, $nama_admin);
            $mail->addAddress('toby@jlm.net.id');
            

            $mail->Subject          = $subject;
            //$mail_data['subject']   = $n.'_'.$pengguna->nama_lengkap;
            //$mail_data['induk']     = 'Permintaan - '.$n.'_'.$pengguna->nama_lengkap;
            // pesan

            $mail->Body = '<h1>LUPA KATA SANDI</h1><p></p><p>Waktu Permintaan : '.$waktu.' </p>
                <p></p><br>Tekan alamat ini untuk setting ulang sandi :&nbsp;<a href=\'http://portal.bnetfit.com/lupa_sandi/set_sandi/'.$forgot.'_'.$hashala.'\'>http://portal.bnetfit.com/lupa_sandi/set_sandi/'.$forgot.'_'.$hashala.'</a><p></p>
            <p><b></b><br><b></b></p><p></p><p></p>Terima Kasih<p><br><br></p>Admin Portal JLM';

            if ($mail->send()) {
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Link berhasil dikirimkan ke email '.$alamat.' </div>');
                redirect(base_url("lupa_sandi/kirim/?suc"));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Link lupa sandi  gagal dikirimkan. </div>');
                redirect(base_url("lupa_sandi/kirim/?suc"));
            }
        }

        
    
    //activate account
    function verifyEmail($key){
        $data = array('blokir' => 0);
        $this->db->where('md5(email)',$key);
        return $this->db->update('pengguna', $data);    //update  blokir 0 user aktif
    }

    function vresandi($key){
        $this->db->where('forgot',$key);
        return $this->db->get('pengguna')->row();    //verifikasi email
    }

    function updforgot($key,$var){
        $q = $this->db->query("SELECT * FROM pengguna WHERE email = ?", array($key))->row();

        $data = array('forgot' => $var, 'forgot_count'=>$q->forgot_count+1);
        $this->db->where('email',$key);
        return $this->db->update('pengguna', $data);    //verifikasi email
    }

    function savsandi($var1,$var2){

        $new_pass = md5($var2);

        $data = array('password' => $new_pass,'forgot' => '');

        $this->db->where('id_pengguna',$var1);
        return $this->db->update('pengguna', $data);    //update sandi baru
    }
    
}

?>