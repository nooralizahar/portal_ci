<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Nmcfee_model extends CI_model{

    public function appvsales($id,$post) {
      
      unset($post["btnSimpanSGM"]);
        $waktu = date("Y-m-d H:i:s");
        $apvsig= $this->session->userdata('id_pengguna');

        $post = array(
        	'idrha' => $id,
            'usapv1' => $apvsig,
            'stapv1' => $this->input->post('stappv'),
            'caapv1' => $this->input->post('caappv'),
            'tgapv1' => $waktu
          ); 

                      
            $apv = $this->db->insert("ts_agrah_apv",$post);

            //$notif=$this->kirimwa($hp,$na,$nosu,$assu,$sisu);

            return $apv;
    }

    public function appvbudev($id,$post) {
      
      unset($post["btnSimpanBDM"]);
        $waktu = date("Y-m-d H:i:s");
        $apvsig= $this->session->userdata('id_pengguna');

        $post = array(
        	'usapv2' => $apvsig,
            'stapv2' => $this->input->post('stappv'),
            'caapv2' => $this->input->post('caappv'),
            'tgapv2' => $waktu
          ); 

                      
            $apv = $this->db->where('idrha',$id)->update("ts_agrah_apv",$post);

            //$notif=$this->kirimwa($hp,$na,$nosu,$assu,$sisu);

            return $apv;
    }

    public function appvdirut($id,$post) {
      
      unset($post["btnSimpanDIR"]);
        $waktu = date("Y-m-d H:i:s");
        $apvsig= $this->session->userdata('id_pengguna');

        $post = array(
        	'usapv3' => $apvsig,
            'stapv3' => $this->input->post('stappv'),
            'caapv3' => $this->input->post('caappv'),
            'tgapv3' => $waktu
          ); 

                      
            $apv = $this->db->where('idrha',$id)->update("ts_agrah_apv",$post);

            //$notif=$this->kirimwa($hp,$na,$nosu,$assu,$sisu);

            return $apv;
    }

}