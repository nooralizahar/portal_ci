<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Survey_model extends CI_Model {



    public function tugas() {
        $data = $this->db->get("to_sagut")->result();
        return $data;
    }
    public function picfoc() {
        $data = $this->db->get("v_data_survey_pic")->result();
        return $data;
    }
    
    public function semua() {
        $data = $this->db->order_by('tgbua','desc')->get("v_data_survey")->result();
        return $data;
    }

    public function pilih($p) {
        $data = $this->db->where('usbua',$p)->order_by('tgbua','desc')->get("v_data_survey")->result();
        return $data;
    }

    public function pilihin($i) {
        $data = $this->db->where_in('jabreq',$i)->order_by('tgbua','desc')->get("v_data_survey")->result();
        return $data;
    }

    public function spvpil($p) {      
         $data = $this->db->where('usbua',$p)->or_where('spvreq',$p)->order_by('tgbua','desc')->get("v_data_survey")->result();   
        return $data;
    }

    public function survalok($ids=null) {
        $data = $this->db->where('idsvy',$ids)->get("v_data_survey")->result();
        return $data;
    }
    public function semtsur() {
        $data = $this->db->get("t_yevrus")->result();
        return $data;
    }

   public function pusvei($id){

        $dat = $this->db->where('idsvy',$id)->delete("t_yevrus");
        return $dat;
    }

    public function cusrhg() {
        $data = $this->db->where('tg_jwb !=',null)->order_by('id_rha','desc')->get("ts_agrah")->result();
        return $data;
    }

    public function staff() {
        $data = $this->db->where("id_mgr",$this->session->userdata("id_pengguna"))->get("pengguna")->result();
        return $data;
    }
   
    public function laporan() {
        
        $data = $this->db->order_by("id", "desc")->get("t_satifitka")->result();
        return $data;
    }

    public function dakhir() {

        $data = $this->db->where("employee_id", $this->session->userdata("id_pengguna"))->order_by("id", "desc")->limit(1)->get("t_satifitka")->result();
        return $data;

    }

    public function laporanpilih() {

        $data = $this->db->where("employee_id", $this->session->userdata("id_pengguna"))->order_by("date", "desc")->get("t_satifitka")->result();//->limit(1)
        return $data;

    }    

    public function laporanstaff($param) {

        $data = $this->db->where("employee_id", $param)->order_by("id", "desc")->get("v_aktifitas_b")->result();//->limit(1)
        return $data;

    }

    public function picsvy() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        unset($post["picjlm_ddl"]);
        unset($post["picjlm_txt"]);
        

        /*if($_FILES["attach"]["name"][0] != "") {
                $datsur = $this->ulo_filesvy($_FILES["attach"]);
                if($datsur == false) {
                    redirect(base_url("survey/survtamb/?err"));
                    exit();
                }
                $post["lampirans"] = json_encode($datsur);
            }*/


        $post["tgpicjlm"] = date("Y-m-d H:i:s");
        $post["usalok"] = $this->session->userdata("id_pengguna");



        if(isset($post["picjlm"])) {
            $svy = $this->db->where('idsvy',$this->input->post('idsvy'))->update("t_yevrus",$post);
            return $svy;
        } else return false;
    }

    public function hassvy() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);       

      if($_FILES["attach"]["name"][0] != "") {
                $dathsl = $this->ulo_filesvy($_FILES["attach"]);
                if($dathsl == false) {
                    redirect(base_url("survey/survuplo/?err"));
                    exit();
                }
                $post["lampiranh"] = json_encode($dathsl);
            }


        $post["tgupload"] = date("Y-m-d H:i:s");
        $post["usupload"] = $this->session->userdata("id_pengguna");



        if(isset($post["lampiranh"])) {
            $svy = $this->db->where('idsvy',$this->input->post('idsvy'))->update("t_yevrus",$post);
            return $svy;
        } else return false;
    }

    public function tambsvy() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        unset($post["pelsvy_ddl"]);
        unset($post["pelsvy_txt"]);
        

        if($_FILES["attach"]["name"][0] != "") {
                $datsur = $this->ulo_filesvy($_FILES["attach"]);
                if($datsur == false) {
                    redirect(base_url("survey/survtamb/?err"));
                    exit();
                }
                $post["lampirans"] = json_encode($datsur);
            }


        $post["tgbua"] = date("Y-m-d H:i:s");
        $post["usbua"] = $this->session->userdata("id_pengguna");



        if(isset($post["pelsvy"])) {
            $svy = $this->db->insert("t_yevrus",$post);
            return $svy;
        } else return false;
    }

    private function ulo_filesvy($files,$path = "assets/uploads/yevrus/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|jpeg|bmp|gif|doc|docx|xls|xlsx|ppt|pptx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }    


}