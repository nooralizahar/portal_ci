<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Lokasi_model extends CI_Model {

// dbz digunakan untuk mengakses database ke-2
// private $dbz;

 public function __construct()
 {
  parent::__construct();
         $this->dbz = $this->load->database('newdb', TRUE);
 }

    public function semlok() {
        
        $data = $this->dbz->where('area_name','Perumahan')->or_where('area_name','Lingkungan')->order_by("name","asc")->get("v_location")->result();//->limit(1) order_by("name","asc")->
        return $data;

    }


    public function semolt() {
        
        $data ="select concat(b.olt_name,'-s',LPAD(split_part(split_part(replace(card,'gpon-onu_1/',''),':',1),'/',1),2,'0'),'_p',LPAD(split_part(split_part(replace(card,'gpon-onu_1/',''),':',1),'/',2),2,'0')) as olt_detil,a.olt_ip,split_part(replace(card,'gpon-onu_1/',''),':',1) as slotport from public.t_net_pon_card a left join public.t_net_olt b on a.olt_ip=b.olt_ip group by b.olt_name,a.olt_ip,slotport order by b.olt_name,olt_detil asc;";
        return $this->dbz->query($data)->result();

    }

    public function semoltc() {
        
        $data = $this->db->where('flag','0')->get("v_t_net_olt_flag")->result();
        return $data;

    }

    public function seminc() {
        
        //SELECT sales_id,sales_name,count(activation_date)as jml,to_char(activation_date, 'YYYY-MM')  as bln FROM "public"."v_account" where activation_date is not null group by sales_id,sales_name,bln;
        $data = $this->dbz->select("sales_id,sales_name,count(activation_date)as jml,to_char(activation_date, 'YYYY-MM')  as bln " )->where('status_desc','ACTIVE')->where('sales_name !=','migrasi')->group_by("sales_id,sales_name,bln")->order_by('jml','desc')->get("v_account")->result();//->limit(1) order_by("name","asc")->
        return $data;

    }

    public function usribo() {
        
        $data = $this->dbz->select('sys_user_id,name,status,department_name')->order_by('name','asc')->get("v_sys_user")->result();
        return $data;

    }

    public function incsal($id) {
        
        $data = $this->dbz->select("sales_id,sales_name,count(activation_date)as jml,to_char(activation_date, 'YYYY-MM')  as bln " )->where('sales_id',$id)->group_by("sales_id,sales_name,bln")->order_by('jml','desc')->get("v_account")->result();//->limit(1) order_by("name","asc")->
        return $data;

    }
    public function lokkas() {
        
        $data = $this->db->where('usver !=','')->where('usapv !=','')->get("t_odp_lok")->result();//->limit(1) order_by("name","asc")->
        return $data;

    }


    public function totlok() {
        
        $data = $this->dbz->get("v_location")->num_rows();
        return $data;

    }

    public function perlok() {
        
        $data = $this->dbz->where('area_name','Perumahan')->get("v_location")->num_rows();
        return $data;

    }

    public function pilare($p) {
        
        $data = $this->dbz->where('area_name',$p)->get("v_location")->result();
        return $data;

    }

    public function pillok($pilih) {
        
        $data = $this->dbz->where("t_location_id",$pilih)->get("v_location")->row();//->limit(1)
        return $data;

    }

    public function pilrek($pil) {
        
        $rek = $this->db->where("idlok",$pil)->get("v_odp_rek_kom")->row();//->limit(1)
        return $rek;

    }
    public function pilbrg($p) {
        
        $brg = $this->db->where("idlok",$p)->get("t_odp_brg_kel")->result();//->limit(1)
        return $brg;

    }
    public function pilodp($p) {
        
        //->limit(1)
        if($this->db->where("lokasi",$p)->get("t_odp")->num_rows()>0){
          $odp = $this->db->where("lokasi",$p)->get("t_odp")->result();  
        }else{
          $odp=0;
        }
        return $odp;

    }
    public function pilsta($param) {
        
        $data = $this->db->where("idlok",$param)->get("t_odp_lok")->row();//->limit(1)
        return $data;

    }
    public function semkom($param) {
        
        $data = $this->db->where("idlok_kom",$param)->get("t_odp_lok_kom")->result();//->limit(1)
        return $data;

    }
    public function datopp() {
        
        $data = $this->db->get("v_sales_opp")->result();//->limit(1)
        return $data;

    }
    public function semiom() {
        
        $data = $this->db->get("t_odp_mom_kom")->result();//->limit(1)
        return $data;

    } 

    public function semsta() {
        
        $data = $this->db->get("t_odp_lok")->result();//->limit(1)
        return $data;

    }    
    public function hitlok($p) {
        
        $data = $this->db->where("idlok",$p)->get("t_odp_lok")->num_rows();//->limit(1)
        return $data;

    }
    public function semlap($tgl) {
        
        $data = $this->db->where('tgcai like',$tgl.'%')->get("v_laporan_kom")->result();//->limit(1)
        return $data;

    } 
    public function piliom($p) {
        
        $data = $this->db->where("idlok",$p)->get("t_odp_mom_kom")->row();//->limit(1)
        return $data;

    }  

    public function cekiom($p) {
        
        $data = $this->db->where("idlok",$p)->get("t_odp_mom_kom")->num_rows();//->limit(1)
        return $data;

    }  
    public function semrek() {
        
        $data = $this->db->get("v_odp_rek_kom")->result();//->limit(1)
        return $data;

    }

    public function datbank() {
        
        $ret= $this->db->order_by("nama","asc")->get("a_ref_bank")->result();//->limit(1)
        return $ret;

    }


    public function verkom($id) {

        $kolom = [
            'usver' => $this->session->userdata('id_pengguna'),'tgver'=>date("Y-m-d H:i:s")];

        $data=$this->db->where('idlok', $id)->update('t_odp_lok',$kolom );
        
        return $data;
    }

    public function apvkom($id) {

        $kolom = [
            'usapv' => $this->session->userdata('id_pengguna'),'tgapv'=>date("Y-m-d H:i:s")];

        $data=$this->db->where('idlok', $id)->update('t_odp_lok',$kolom );
        
        return $data;
    }

    public function simlok($par) {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        if(empty($this->input->post('tgins'))){
        $post["ussur"] = $this->session->userdata('id_pengguna');       
        $mbaru = $this->db->insert("t_odp_lok",$post);
        }else{
        $post["usins"] = $this->session->userdata('id_pengguna');

        $post["usrfs"] = $this->session->userdata('id_pengguna');
        $mbaru = $this->db->where('idlok',$par)->update("t_odp_lok",$post);
        }
            
            return $mbaru;
        
    }   

    public function rekbar() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["usbua"] = $this->session->userdata('id_pengguna');
        $post["tgbua"] = date('Y-m-d H:i:s');

            $mbaru = $this->db->insert("t_odp_rek_kom",$post);
            return $mbaru;
        
    }

    public function kombar() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["usbua"] = $this->session->userdata('id_pengguna');
        $post["tgbua"] = date('Y-m-d H:i:s');

            $mbaru = $this->db->insert("t_odp_lok_kom",$post);
            return $mbaru;
        
    }

    public function brgkel() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["usbua"] = $this->session->userdata('id_pengguna');
        $post["tgbua"] = date('Y-m-d H:i:s');

            $brg = $this->db->insert("t_odp_brg_kel",$post);
            return $brg;
        
    }


}