<?php


defined("BASEPATH") OR exit("Akses script tidak diizinkan!");
class Ucorpe_model extends CI_Model {


    public function sepmat() {
        $tpe = $this->db->get("app_rednet_atresep")->result();
        return $tpe;
    }

    public function orpmat() {
        $tam = $this->db->get("app_rednet_ratfad")->result();
        return $tam;
    }

	public function orpmis() {
		
      $post = $this->input->post();  
      unset($post["btnSimpan"]);       
      
      if($_FILES["lampro"]["name"][0] != "") {
                $dathsl = $this->orplif($_FILES["lampro"]);
                if($dathsl == false) {
                    redirect(base_url("panep/keyorp/?err"));
                    exit();
                }
                $post["lampro"] = json_encode($dathsl);
            }

        $post["tgi"] = date("Y-m-d H:i:s");
        $post["usi"] = $this->session->userdata("id_pengguna");

        if(isset($post["kodpro"])) {
            $bla=$this->db->insert("app_rednet_ratfad",$post);
            return $bla;
        } else return false;

	}

    private function orplif($files,$path = "assets/uploads/lampro/"){
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'pdf|jpg',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid(). "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }

    
}