<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Datbar_model extends CI_Model {

    public function focsem() {
        $data = $this->db->where('id_jabatan',18)->where("gudang",$this->session->userdata("gudang"))->order_by('nama_lengkap','asc')->get("pengguna")->result();
        return $data;
    }

    public function brgsem() {
        $data = $this->db->where("gudang",$this->session->userdata("gudang"))->order_by('tgbua','asc')->get("foc_logbrg")->result();
        return $data;
    }

    public function jebar() {
        $data = $this->db->get("v_jebrg")->result();
        return $data;
    }
    
    public function brgtam() {
        $post = $this->input->post();
        
        $post["tgbua"] = date("Y-m-d H:i:s");
        $post["usbua"] = $this->session->userdata("username");
        $post["gudang"] = $this->session->userdata("gudang");
        if(isset($post["btnSimpan"])) {
            unset($post["btnSimpan"]);
            $num = $this->db->insert("foc_logbrg",$post);
           
           return $num;
        } else return false;
    }
    public function brgpil($param) {

        $d = $this->db->where("jebrg", $param)->where("gudang",$this->session->userdata("gudang"))->get("v_foc_logbrg")->result();
        return $d;
    }
    public function pilbar($n) {

        $d = $this->db->where("id", $n)->get("foc_logbrg")->row();
        return $d;
    }
    public function pillog($n) {

        $d = $this->db->where("id", $n)->get("foc_logbook")->row();
        return $d;
    }
    public function logsem() {
    	$data = $this->db->order_by('tglog','desc')->get("v_foc_logbook")->result();
        return $data;
    }


    public function logtam() {
        $post = $this->input->post();
        
        $post["tgbua"] = date("Y-m-d H:i:s");
        $post["usbua"] = $this->session->userdata("username");
        $post["cabang"] = $this->session->userdata("gudang");
        if(isset($post["btnSimpan"])) {
            unset($post["btnSimpan"]);
            $num = $this->db->insert("foc_logbook",$post);
           
           return $num;
        } else return false;
    }

    public function logpil($param) {

        $d = $this->db->where("id", $param)->get("foc_logbook")->result();
        return $d;
    }

    public function logsim($param) {

        $post = $this->input->post();
        
        $post["tguba"] = date("Y-m-d H:i:s");
        $post["usuba"] = $this->session->userdata("username");
        $post["cabang"] = $this->session->userdata("gudang");
        if(isset($post["btnSimpan"])) {
            unset($post["btnSimpan"]);
            unset($post["tgbua"]);
            unset($post["usbua"]);
            $sim = $this->db->where("id", $param)->update("foc_logbook",$post);
           
           return $sim;
        } else return false;
    }

    public function loghit($param) {
        $harini= date('Y-m-d'); //2020-01-01
        $blnini= date('Y-m'); //2020-01
        if($param==$harini){
        $d = $this->db->where("tglog", $param)->get("foc_logbook")->num_rows();
        }elseif($param==$blnini){
        $d = $this->db->where("tglog like", $param."%")->get("foc_logbook")->num_rows();
        }
        return $d;
    }
   public function logpus($id) {
        return $this->db->where('id', $id)->delete('foc_logbook');
    }
   public function brgpus($id) {
        return $this->db->where('id', $id)->delete('foc_logbrg');
    }
    public function brgsim($param) {

        $post = $this->input->post();
        
        $post["tguba"] = date("Y-m-d H:i:s");
        $post["usuba"] = $this->session->userdata("username");
        $post["gudang"] = $this->session->userdata("gudang");
        if(isset($post["btnSimpan"])) {
            unset($post["btnSimpan"]);
            unset($post["tgbua"]);
            unset($post["usbua"]);
            $sim = $this->db->where("id", $param)->update("foc_logbrg",$post);
           
           return $sim;
        } else return false;
    }
 
}