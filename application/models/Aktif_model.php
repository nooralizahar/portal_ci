<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Aktif_model extends CI_Model {


    public function rekap($th) {

        $data = $this->db->where("thn",$th)->where("employee_id", $this->session->userdata("id_pengguna"))->order_by("pel", "desc")->get("v_aktifitas_rekap_hit")->result();//->limit(1)
        return $data;

    }  
    public function rekap4m($th,$pil) {

        $data = $this->db->where("thn",$th)->where("employee_id", $pil)->order_by("pel", "desc")->get("v_aktifitas_rekap_hit")->result();//->limit(1)
        return $data;

    }  

    public function tugas() {
        $data = $this->db->get("to_sagut")->result();
        return $data;
    }

    public function atifpil($param) {

        $data = $this->db->where("id", $param)->order_by("date", "desc")->get("t_satifitka")->result();//->limit(1)
        return $data;

    }

    public function staff() {
        $data = $this->db->where("id_mgr",$this->session->userdata("id_pengguna"))->get("pengguna")->result();
        return $data;
    }
   
    public function laporan() {
        
        $data = $this->db->order_by("id", "desc")->get("t_satifitka")->result();
        return $data;
    }

    public function dakhir() {

        $data = $this->db->where("employee_id", $this->session->userdata("id_pengguna"))->order_by("id", "desc")->limit(1)->get("t_satifitka")->result();
        return $data;

    }

    public function laporanpilih() {

        $data = $this->db->where("employee_id", $this->session->userdata("id_pengguna"))->order_by("date", "desc")->get("v_t_satifitka")->result();//->limit(1)
        return $data;

    }    

    public function laporanstaff($param) {

        $data = $this->db->where("employee_id", $param)->order_by("id", "desc")->get("v_aktifitas_b")->result();//->limit(1)
        return $data;

    }

    public function atifselesai() {
        $id=$this->input->post("id");
        $ko=$this->input->post("latlng");
        $kolom = ['site'=>$ko,'is_solved'=>1,'updated_at'=>date("Y-m-d H:i:s")];

        $data=$this->db->where('id', $id)->update('t_satifitka',$kolom );     
        return $data;
    }



    public function atifubah() {
        $id=$this->input->post("id");
        $ko=$this->input->post("latlng");
        $cr=$this->input->post("tgla")." ".$this->input->post("wktm");
        $up=$this->input->post("tgla")." ".$this->input->post("wkts");
        $kolom = ['date'=>$this->input->post("tgla"),'site'=>$ko,'is_solved'=>1,'created_at'=>$cr,'updated_at'=>$up];

        $data=$this->db->where('id', $id)->update('t_satifitka',$kolom );     
        return $data;
    }


    public function atiftamb() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        $post["created_at"] = date("Y-m-d H:i:s");
        $post["employee_id"] = $this->session->userdata("id_pengguna");
        if(isset($post["customer"])) {
            $num = $this->db->insert("t_satifitka",$post);
            return $num;
        } else return false;
    }


}