<?php


defined("BASEPATH") OR exit("Akses ditolak");

class Konfig_web_model extends CI_Model {

    public function status_maintenance() {
        $kondisiWeb = $this->db->get("status_web")->row();
        return ($kondisiWeb->maintenance == 1) ? true : false;
    }

    public function ubah_status_maintenance($k) {
        $this->db->update("status_web",array(
            "maintenance" => $k
        ));
        return true;
    }

}