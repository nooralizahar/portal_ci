<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Noctiket_model extends CI_Model {

   public function sem_ntik($id) {
   	if($id==1){
        $data = $data = $this->db->where('status_id !=' ,'Closed')->order_by('id','desc')->get("v_noc_tiket_uta")->result(); // tiket tunda
    }else if($id==2){
        $data = $this->db->where('status_id' ,'Closed')->order_by('id','desc')->get("v_noc_tiket_uta")->result(); // tiket selesai
    }else {
    	$data = $this->db->order_by('id','desc')->get("v_noc_tiket_uta")->result(); // semua tiket
    }
    return $data;
    }

    public function semreq() {
        $dareq = $this->db->get("noc_tiket_req")->result();
        return $dareq;
    }

    public function semsta() {
        $dasta = $this->db->get("noc_tiket_sta")->result();
        return $dasta;
    }

    public function semrty() {
        $darty = $this->db->get("noc_tiket_rty")->result();
        return $darty;
    }

    public function semven() {
        $daven = $this->db->get("noc_tiket_ven")->result();
        return $daven;
    }

    public function sempsi() {
        $dareq = $this->db->get("noc_tiket_psi")->result();
        return $dareq;
    }

    public function semlca() {
        $dasta = $this->db->get("noc_tiket_lca")->result();
        return $dasta;
    }

    public function semcgr() {
        $darty = $this->db->get("noc_tiket_cgr")->result();
        return $darty;
    }

    public function semrca() {
        $daven = $this->db->get("noc_tiket_rca")->result();
        return $daven;
    }

    public function semcby() {
        $daven = $this->db->get("v_noc_tiket_cby")->result();
        return $daven;
    }

    public function semlby() {
        $daclo = $this->db->get("v_noc_tiket_lby")->result();
        return $daclo;
    }

    public function baru() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        $post["created_id"] = $this->session->userdata('id_pengguna');

            $tbaru = $this->db->insert("noc_tiket_uta",$post);
            return $tbaru;
        
    }

    public function thapus($param) {
        $hapus=$this->db->where("id",$param)->delete("noc_tiket_uta");
        $hapus=$this->db->where("tiket_id",$param)->delete("noc_tiket_upd");
    return $hapus;

    }

    public function tpilih($param) {
            $dapil = $this->db->where("id",$param)->get("noc_tiket_uta")->result();
        return $dapil;

    }


}