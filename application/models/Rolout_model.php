<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Rolout_model extends CI_model{

    public function cbkaseg(){
        $dat = $this->db->get("v_app_latrop_lebak_cbseg")->result();
        return $dat;

    }

    public function cbkajen(){
        $dat = $this->db->get("v_app_latrop_lebak_cbjka")->result();
        return $dat;

    }
    
    public function puskab($i) {
        return $this->db->where("idkab",$i)->delete("app_latrop_lebak");
    }

    public function tamkab() {
        return $this->db->get("app_latrop_lebak")->result();
    }

    public function simkab() {
        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            unset($post["btnSimpan"]);
            unset($post["sekab_ddl"]); unset($post["sekab_txt"]);
            unset($post["jekab_ddl"]); unset($post["jekab_txt"]);
            
            $post["usbua"]=$this->session->userdata('id_pengguna');
            $post["tgbua"]=date('Y-m-d H:i:s');
            $sim = $this->db->insert("app_latrop_lebak",$post);
            return $sim; 
        } else return false;
    }
    
    public function cbtiseg(){
        $dat = $this->db->get("v_app_latrop_gnait_cbseg")->result();
        return $dat;

    }
    public function cbtiket(){
        $dat = $this->db->get("v_app_latrop_gnait_cbket")->result();
        return $dat;

    }
    public function cbodcdp(){
        $dat = $this->db->get("v_odcp_tcp")->result();
        return $dat;

    }
    public function tilala($a) {
        return $this->db->where("idala",$a)->get("v_app_latrop_gnait_lit")->result();
    }

    public function pustia($i) {
        return $this->db->where("idtia",$i)->delete("app_latrop_gnait_lit");
    }

    public function simtia() {
        $post = $this->input->post();

        
        if($_FILES["attach"]["name"][0] != "") {
                $fototia = $this->upload_filex($_FILES["attach"]);
                if($fototia == false) {

                    redirect(base_url("/nuber/tiabar/"));
                    exit();
                }
                
                $post["tfoto"] = json_encode($fototia);
            }


        if(isset($post["btnSimpan"])) {
            unset($post["btnSimpan"]);
            unset($post["tiseg_ddl"]); unset($post["tiseg_txt"]);
            unset($post["tiket_ddl"]); unset($post["tiket_txt"]);
            unset($post["odcdp_ddl"]); unset($post["odcdp_txt"]);

            $post["ustia"]=$this->session->userdata('id_pengguna');
            $post["tgtia"]=date('Y-m-d H:i:s');
            $sim = $this->db->insert("app_latrop_gnait_lit",$post);
            return $sim; 
        } else return false;
    }
    public function datala($pil=null) {
        if(empty($pil)){
            return $this->db->get("v_app_latrop_gnait")->result();
        }else{
            return $this->db->where("idala",$pil)->get("v_app_latrop_gnait")->row();
        }
        
    }

    public function akhala() {
        return $this->db->order_by("idala","desc")->limit(1)->get("app_latrop_gnait")->row();
    }

    public function simala() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $post["usbua"] = $this->session->userdata('id_pengguna');
        $post["tgbua"] = date('Y-m-d H:i:s');

            $isi = $this->db->insert("app_latrop_gnait",$post);
            return $isi;   
    }


	public function simrea() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        
        if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_filex($_FILES["attach"]);
                if($gambar == false) {

                    redirect(base_url($this->uriku."/kaarea/"));
                    exit();
                }
                $post["piran"] = json_encode($gambar);
            }

        $post["phrea"]=$this->session->userdata("id_pengguna");
        $post["tgrea"]=date("Y-m-d H:i:s");

        if(isset($post["delak"])) {
            $num = $this->db->insert("app_latrop_aeraak",$post);
            return $num;
        } else return false;
    }


    private function upload_filex($files,$path = "assets/uploads/netdev/"){
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|jpeg|bmp|gif|doc|docx|xls|xlsx|ppt|pptx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }	



}