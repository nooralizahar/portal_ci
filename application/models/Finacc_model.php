<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Finacc_model extends CI_Model {

    public function isibuarba() {

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            unset($post["btnSimpan"]);
            $post["ubu"]=$this->session->userdata('id_pengguna');
            $post["tbu"]=date('Y-m-d H:i:s');
            $sim = $this->db->insert("fat_knabraul",$post);
            return $sim; 
        } else return false;
    }

    public function simket(){
        $post = $this->input->post();
        $idre = $this->input->post("idt");
        $uref = $this->session->userdata('id_pengguna');
        $tref = date('Y-m-d H:i:s');

        if(isset($post["btnSimpan"])) {
           unset($post["btnSimpan"]);
           $post["ubu"]= $idre;
           $post["tbu"]= $tref;
           $sim = $this->db->insert("fat_knabraul_ket",$post);

            $snit=$this->sumket($idre);
            $jp10=$snit->rp*0.1;
            $jp23=$snit->rp*0.02;
            $jnit=($snit->rp+$jp10)-$jp23;

            $setnom = ['nit'=>$jnit,'p10'=>$jp10,'p23'=>$jp23,'umo'=>$uref,'tmo'=>$tref];

            $this->db->where("idt",$idre)->update("fat_knabraul",$setnom); 

        return $sim; 
        } else return false;

    }

    public function pusnom($id){    
        $setnom = ['nit'=>0,'p10'=>0,'p23'=>0];
        return $this->db->where('idt', $id)->update("fat_knabraul",$setnom);
    }

    public function hittak($id){    
        $hit=$this->db->select("cet")->where('idt', $id)->get("fat_knabraul")->row();
        $uhi=$hit->cet+1;
        $sethit = ['cet'=>$uhi];
        return $this->db->where('idt', $id)->update("fat_knabraul",$sethit);
    }

    public function pusket($id){    
        return $this->db->where('idk', $id)->delete("fat_knabraul_ket");
    }

    public function sumket($id){
        return $this->db->select('sum(nit) as rp')->where('idt', $id)->group_by('idt')->get("fat_knabraul_ket")->row();
    }

    public function pilket($p){
        $dat = $this->db->where("idt",$p)->get("fat_knabraul_ket")->result();
        return $dat;

    }
    public function datbuarba(){
        $dat = $this->db->get("v_fat_knabraul")->result();
        return $dat;

    }
    public function pilbuarba($p){
            $dat = $this->db->where("idt",$p)->get("fat_knabraul")->result();
        return $dat;    
    }


}