<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Wordata_model extends CI_Model {

    public function pilwo($idwo){
        $data = $this->db->where('id_worde',$idwo)->get("tp_owatad")->row();
        return $data;
    }

    public function setemail(){
            //$this->db->select("*");
            $this->db->from("set_email")->where('idket',2);
            $sem = $this->db->get()->row();
            return $sem;
    }

    function woblok($id){
        // Load PHPMailer library
        $this->load->library('phpmailer_lib');

        $detwo=$this->dawoaf($id);
        foreach ($detwo as $wo) {
            $nowo=$wo->id_worde;
            $nacu=$wo->nm_custo;
            $alcu=$wo->al_custo;
            $cacu=$wo->ca_custo;
            $trfs=tgl_indome($wo->tg_erefs);
            $nasa=$wo->nm_sales;
        }

        $dat=$this->setemail();
        $alamat='toby@jlm.net.id,support@jlm.net.id,cro@jlm.net.id';//$alx;//str_replace('%10','@',$alx);
        // PHPMailer object
        $mail = $this->phpmailer_lib->load();

        // SMTP configuration
        $mail->isSMTP();
        $mail->Host     = $dat->smtp;//'smtp.email.co.id';
        $mail->SMTPAuth = true;
        $mail->Username = $dat->email;//'email@lengkap.co.id';
        $mail->Password = $dat->password;//'password';
        $mail->SMTPSecure = $dat->secure;//'tls';
        $mail->Port     = $dat->port;//587;

        $mail->setFrom($dat->email, $dat->nama);
        //$mail->addReplyTo('toby@jlm.net.id', 'Balas');

        // Add a recipient
        //$mail->addAddress('toby@jlm.net.id');

        $addresses = explode(',',$alamat);
        foreach ($addresses as $address) { //awal foreach

        // for clear last recipients

        $mail->ClearAllRecipients();

        $mail->addAddress($address);

        // Add cc or bcc 
        $mail->addCC('engineering@jlm.net.id');
        //$mail->addBCC('droid.jig@gmail.com');

        // Email subject
        $mail->Subject = 'No. WO '.$nowo.' permintaan SOFTBLOCK a/n '.$nacu;

        // Set email format to HTML
        $mail->isHTML(true);

        // Email body content
        $mailContent = 'Kepada Tim Support & Engineering.<br><br>
Bersama ini kami informasikan permintaan SOFTBLOCK dari pelanggan, sebagai berikut :<br><br>

<table border="0">
<tr><td valign="top"><b>No. WO</b>         </td><td valign="top">&nbsp;:&nbsp;</td><td valign="top">'.$nowo.'</td></tr>
<tr><td valign="top"><b>Pelanggan</b> </td><td valign="top">&nbsp;:&nbsp;</td><td valign="top">'.$nacu.'</td></tr>
<tr><td valign="top"><b>Alamat        </b> </td><td valign="top">&nbsp;:&nbsp;</td><td valign="top">'.$alcu.'</td></tr>
<tr><td valign="top"><b>Kapasitas     </b> </td><td valign="top">&nbsp;:&nbsp;</td><td valign="top">'.$cacu.'</td></tr>
<tr><td valign="top"><b>Tgl. RFS      </b> </td><td valign="top">&nbsp;:&nbsp;</td><td valign="top">'.$trfs.'</td></tr>
<tr><td valign="top"><b>Sales         </b> </td><td valign="top">&nbsp;:&nbsp;</td><td valign="top">'.$nasa.'</td></tr>
</table>
<br>
Demikian hal ini kami sampaikan, atas perhatiannya terima kasih.<br><br>

Salam,<br><br><br>
('.$this->session->userdata('nama_lengkap').')

 <p><small><sub>Dikirim dari https://portal.bnetfit.com on '.date('d-M-Y H:i:s').'</sub></small> </p> ';
        $mail->Body = $mailContent;

        // Send email
        if(!$mail->send()){
            echo 'Notifikasi gagal di kirim.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }else{
            echo 'Library berjalan dengan baik, notifikasi berhasil dikirim.';
        }
    } //akhir foreach
}

    public function worev($id) {
        $idrev = $this->db->select("id_worev")->from("tp_owatad_rev")->order_by("id_worev","desc")->limit(1)->get()->result();

        foreach ($idrev as $iw) {
            $nrev=$iw->id_worev;
        }

        if(empty($idrev)){
         $no=1;
        }else{
         $no=$nrev+1;   
        }
        $urev=$this->session->userdata('id_pengguna');
        $trev=date('Y-m-d H:i:s');

        if(isset($idrev)) {
        $q="INSERT INTO tp_owatad_rev SELECT ".$no." as id_worev,".$urev." as us_rev,'".$trev."' as tg_rev,a.* FROM `tp_owatad` a WHERE `id_worde`='".$id."'";
        $r=$this->db->query($q);

        return $this->db->where("id_worde",$id)->delete("tp_owatad");
        } else return false;
    
    }

    public function cekwo($id){
        $d = $this->db->where("id_worde", $id)->get("tp_owatad")->num_rows();
    return $d;
    }

    public function dafper() {
        $data = $this->db->order_by('id_gam')->get("tp_rabmag")->result();
        return $data;
    }

    public function desper() {
        $data = $this->db->get("v_ds_gam")->result();
        return $data;
    }

    public function dasite($idp) {
        $data = $this->db->where('id_worde',$idp)->get("v_pmo_site")->result();
        return $data;
    }

    public function perhap($idp) {
        return $this->db->where("id_tpl",$idp)->delete("tp_lopot");
    }

    public function prohap($idp) {
        return $this->db->where("id_wopro",$idp)->delete("ts_wopro");
    }

    public function wosemua($n=null) {

        $this->db->from("v_tp_owatad");
        
        if(empty($n)){
          $this->db->where("id_worde like ","22%");   
        }elseif(strtolower($n)=='semua'){
          //$this->db->where("id_worde like ","22%"); 
        }else{
          $this->db->where("id_worde",$n);
        }
        
        // $this->db->or_where("id_worde like ","21%");
        $this->db->order_by("id_worde", "desc");
        $data = $this->db->get(); 
        return $data->result();

    }

    public function woaktif() {

        $this->db->from("v_tp_owatad_aktif");
        $this->db->order_by("id_worde", "desc");
        $data = $this->db->get(); 
        return $data->result();

    }

    public function woplist() {

        $this->db->from("v_app_latrop_eunever_pl");
        $this->db->where("rv",0);
        $this->db->order_by("id_worde", "desc");
        $data = $this->db->get(); 
        return $data->result();

    }

    public function blnwo() {
        
        $data = $this->db->query("SELECT substring(`id_worde`,1,4) as bln FROM `tp_owatad` GROUP BY substring(`id_worde`,1,4) ORDER BY substring(`id_worde`,1,4) DESC"); 
        return $data->result();

    }

 /* OLD BEF220304   public function wodateks() {
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));

        $this->db->from("v_tp_owatad");
        $this->db->where("tg_issue like ", $blnini."%");
        $this->db->order_by("id_worde", "desc");
        $data = $this->db->get(); 
        return $data->result();

    }*/

    public function wodateks() {
        /* RUN ON 2200304 */
        $post=$this->input->post();

        if(isset($post['btnGen'])){
            $blnwo=$this->input->post('bln');            
        }else{
            $blnwo= date('ym');
        }

        $this->db->from("v_tp_owatad");
        $this->db->where("id_worde like ", $blnwo."%");
        $this->db->order_by("id_worde", "desc");
        $data = $this->db->get(); 
        return $data->result();

    }

    public function wo4baa() {

        $this->db->from("v_tp_owatad");
        $this->db->where("dan",1);
        $this->db->order_by("id_worde", "desc");
        $data = $this->db->get(); 
        return $data->result();

    }

    public function prosemua($id) {

        $this->db->from("ts_wopro");
        $this->db->where("id_worde", $id);
        $data = $this->db->get(); 
        return $data->result();

    }

    public function wordpilih($pilih) {

        $data = $this->db->like("tg_issue", $pilih)->order_by("id_worde", "desc")->get("tp_owatad")->result();
        return $data;

    }

    public function dawoaf($p) {

        $afrev = $this->db->where("id_worde", $p)->get("v_tp_owatad")->result();
        return $afrev;

    }
    public function dawobe($p) {

        $berev = $this->db->where("id_worde", $p)->get("tp_owatad_rev")->result();
        return $berev;

    }

    public function wopilih() {

        $data = $this->db->where("us_assto", $this->session->userdata("id_pengguna"))->order_by("id_worde", "desc")->get("v_tp_owatad")->result();
        return $data;

    }

    public function topopil($id) {

        $this->db->from("tp_lopot");
        $this->db->where("id_worde", $id);
        $data = $this->db->get(); 
        return $data->result();

    }

    public function asstopi($id) {

        $this->db->from("tp_owatad_assto");
        $this->db->where("wono", $id);
        $data = $this->db->get(); 
        return $data->result();

    }

    public function toptamb() {
        $post = $this->input->post();
        unset($post["ds_gam_ddl"]);
        unset($post["ds_gam_txt"]);
        unset($post["btnSubmit"]);

        if($this->input->post('id_cir')=='89'){
        $post = array(
            'id_worde'   => $this->input->post('id_worde'),
            'id_cir'   => $this->input->post('id_cir'),
            'id_gam'   => '89',
            'ds_gam'   => 'upload','po_inp'   => '-','po_out'   => '-',
            'ip_add'   => '-','cf_vla'   => '-'
        );
        }

        $post["us_tpl"] =$this->session->userdata('username');
        $post["tg_tpl"] =date('Y-m-d H:i:s');

        $a=$this->db->where('wono',$this->input->post('id_worde'))->get("tp_owatad_assto")->num_rows();
        if($a==0){
        $taken = array(
            'wono'   => $this->input->post('id_worde'),
            'filetopo'   => '',
            'us_assto' => $this->session->userdata('id_pengguna'),
            'tg_assto'     => date('Y-m-d H:i:s')
        );
        $this->db->insert("tp_owatad_assto",$taken);
        }else{}

        if(isset($post["id_worde"])) {
            $num = $this->db->insert("tp_lopot",$post);
            return $num;
        } else return false;

    }

    public function protamb() {
        $post = $this->input->post();
        unset($post["site_ddl"]);
        unset($post["site_txt"]);
        unset($post["btnSubmit"]);

        if(isset($post["id_worde"])) {
            $num = $this->db->insert("ts_wopro",$post);
            return $num;
        } else return false;
    }

    public function wo_before_sap() {
        $post = $this->input->post();
        unset($post["sv_custo_ddl"]);
        unset($post["sv_custo_txt"]);
        unset($post["nm_svsta_ddl"]);
        unset($post["nm_svsta_txt"]);
        unset($post["ca_custo_ddl"]);
        unset($post["ca_custo_txt"]);
        unset($post["if_custo_ddl"]);
        unset($post["if_custo_txt"]);
        unset($post["md_custo_ddl"]);
        unset($post["md_custo_txt"]);
        unset($post["nm_sales_ddl"]);
        unset($post["nm_sales_txt"]);
        unset($post["btnSubmit"]);

        if(isset($post["id_worde"])) {
            $num = $this->db->insert("tp_owatad",$post);
            return $num;
        } else return false;
    }



    public function procomp($id) {
        $komplit = array(
            'id_worde'   => $id,
            'tg_pro'   => date('Y-m-d'),
            'site' => '',
            'uraian' => 'completed',
            'tg_cre'     => date('Y-m-d H:i:s')
        );

        if(isset($id)) {
            $kom = $this->db->insert("ts_wopro",$komplit);
            return $kom;
        } else return false;
    }
    public function wodispatch() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);

        if(isset($post["wono"])) {
            $num = $this->db->insert("tp_owatad_assto",$post);
            return $num;
        } else return false;
    }

    public function uplobaa() {
        $post = $this->input->post();
        
        unset($post["btnSubmit"]);       
      $idw = $this->input->post('idbaa');
      if($_FILES["attach"]["name"][0] != "") {
                $dathsl = $this->ulo_baa($_FILES["attach"]);
                if($dathsl == false) {
                    redirect(base_url("panel/baatamb/".$idw."?err"));
                    exit();
                }
                $post["filebaa"] = json_encode($dathsl);
            }

        $post["idbaa"] = $idw;
        $post["nobaa"] = '';
        $post["tgbaa"] = date("Y-m-d H:i:s");
        $post["usbaa"] = $this->session->userdata("id_pengguna");



        if(isset($post["filebaa"])) {
            //$baa = $this->db->where('id_worde',$idw)->update("tp_owatad",$post);
             $baa=$this->db->insert("tp_owatad_aab",$post);
            return $baa;
        } else return false;
    }

    public function uplobap() {
        $post = $this->input->post();
        
        unset($post["btnSubmit"]);       
      $idw = $this->input->post('idbap');
      if($_FILES["attach"]["name"][0] != "") {
                $dathsl = $this->ulo_bap($_FILES["attach"]);
                if($dathsl == false) {
                    redirect(base_url("panel/baptamb/".$idw."?err"));
                    exit();
                }
                $post["filebap"] = json_encode($dathsl);
            }

        $post["idbap"] = $idw;
        $post["nobap"] = '';
        $post["tgbap"] = date("Y-m-d H:i:s");
        $post["usbap"] = $this->session->userdata("id_pengguna");



        if(isset($post["filebap"])) {
            //$baa = $this->db->where('id_worde',$idw)->update("tp_owatad",$post);
             $c=$this->procomp($idw);
             $bap=$this->db->insert("tp_owatad_pab",$post);
            return $bap;
        } else return false;
    }

    private function ulo_baa($files,$path = "assets/uploads/baa/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|gif',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() .'_'. md5($image) . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }

    private function ulo_bap($files,$path = "assets/uploads/bap/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|gif',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() .'_'. md5($image) . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }

    private function upload_filey($files,$path = "assets/uploads/logo/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|jpeg|bmp|gif|doc|docx|xls|xlsx|ppt|pptx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() .'_'. md5($image) . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }




}