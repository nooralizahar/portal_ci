<?php


defined("BASEPATH") OR exit("Akses script tidak diizinkan!");
class Eunvlas_model extends CI_Model {

	public function napmis() {
		
        $post = $this->input->post();

        unset($post["btnSubmit"]);
        unset($post["idr_ddl"]);
        unset($post["idr_txt"]);

        $post["tgi"] = date("Y-m-d H:i:s");
        $post["usi"] = $this->session->userdata("id_pengguna");

        if(isset($post["idr"])) {
            $sim= $this->db->insert("app_latrop_eunever",$post);
            return $sim;
        } else return false;

	}

    public function vhilip() {
        $pil = $this->db->get("v_app_latrop_eunever")->result();
        return $pil;
    }

    public function vsupah($i) {
        return $this->db->where('idr', $i)->delete('app_latrop_eunever');
    }

// mulai -> komponen untuk import data revenue 

    public function dafile() {
        $data = $this->db->order_by("idupl","desc")->get("v_tbl_file")->result();
        return $data;
    }

    public function lfile() {
        $ldata = $this->db->order_by("idupl","desc")->limit(1)->get("tbl_file")->result();
        return $ldata;
    }

    public function ulosuke() {
      $post = $this->input->post();  
      unset($post["btnImport"]);       
      
      if($_FILES["nafile"]["name"][0] != "") {
                $dathsl = $this->upsuke($_FILES["nafile"]);
                if($dathsl == false) {
                    redirect(base_url("salvenue/?err"));
                    exit();
                }
                $post["nafi"] = json_encode($dathsl);
            }

        $post["tgupl"] = date("Y-m-d H:i:s");
        $post["usupl"] = $this->session->userdata("id_pengguna");

        if(isset($post["nafi"])) {
            
            $bla=$this->db->insert("tbl_file",$post);
            return $bla;
        } else return false;
    }

    private function upsuke($files,$path = "assets/uploads/fileim/"){
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'xls|xlsx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid(). "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }


    function insertdatxls($data){
        $this->db->insert_batch('app_latrop_eunever', $data);
    }
// akhir -> komponen untuk import data revenue

    
}