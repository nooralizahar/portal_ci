<?php

defined("BASEPATH") OR exit("Akses ditolak!");
class Diskusi_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        //$this->load->model("Pemberitahuan_model","pemberitahuan");
    }

    public function kirim($post) {
        unset($post["btnSubmit"]);
        $to_user = $post["penerima"];
        if(count($to_user) == 0) return false;
        unset($post["penerima"]);
        $insert = $this->db->insert("ts_isuksid",$post);
        $id_disx = $this->db->insert_id();
        $ko_disx = sha1(uniqid("ID_")."-".$id_disx);
        if($insert) {
            //$this->tandai_disposisi($id_pesan,$id_disposisi . "/" . $kode_disposisi);
            foreach($to_user as $user) {
                $relasi = array(
                    "id_disx" => $id_disx,
                    //"id_pesan" => $id_pesan,
                    "dari_user" => $this->session->userdata("id_pengguna"),
                    "ke_user" => $user,
                    "ko_disx" => $ko_disx
                );
                $this->db->insert("ts_isuksidrel",$relasi);
                /* $this->pemberitahuan->buat(array(
                    "id_pengguna" => $user,
                    "dari_pengguna" => $this->session->userdata("id_pengguna"),
                    "judul" => "Disposisi",
                    "pesan" => $post["isi_disposisi"],
                    "link" => "baca_disposisi_masuk/" . $id_disposisi . "/" . $kode_disposisi
                ));*/
            }
            return true;
        } else return false;
    }

    public function disxkirm(){
        $get = $this->input->get();
        if(isset($get["inst"])) {
            $this->db->where("ts_isuksid.perihal",$get["inst"]);
        }
        $this->db->select("ts_isuksidrel.*");
        $this->db->select("ts_isuksid.*");
        $this->db->from("ts_isuksidrel");
        $this->db->join("ts_isuksid","ts_isuksidrel.id_disx = ts_isuksid.id_disx","left");
        $this->db->where("ts_isuksidrel.dari_user",$this->session->userdata("id_pengguna"));
        //$this->db->group_by("ts_isuksidrel.ko_disx");
        $this->db->order_by("ts_isuksid.wk_kirm","desc");

        $data = $this->db->get()->result();

        foreach($data as $key=>$masuk) {
            $this->db->select("pengguna.nama_lengkap,ts_isuksidrel.ko_disx,ts_isuksidrel.dibaca");
            $this->db->from("pengguna");
            $this->db->join("ts_isuksidrel","ts_isuksidrel.ke_user = pengguna.id_pengguna","left");
            $this->db->where("ts_isuksidrel.id_disx",$masuk->id_disx);
            $this->db->where("ts_isuksidrel.ko_disx",$masuk->ko_disx);

            $data[$key]->penerima = $this->db->get()->result();
        }

        return $data;
    }


    public function disxtrma(){
        $get = $this->input->get();
        if(isset($get["inst"])) {
            $this->db->where("ts_isuksid.perihal",$get["inst"]);
        }

        $this->db->select("ts_isuksidrel.*");
        $this->db->select("ts_isuksid.*");
        $this->db->select("ts_isuksid.selesai AS disx_selesai");
        $this->db->select("pengguna.nama_lengkap");
        $this->db->from("ts_isuksidrel");
        $this->db->join("ts_isuksid","ts_isuksidrel.id_disx = ts_isuksid.id_disx");
        $this->db->join("pengguna","ts_isuksidrel.dari_user = pengguna.id_pengguna","left");
        $this->db->where("ts_isuksidrel.ke_user",$this->session->userdata("id_pengguna"));
        //$this->db->group_by("ts_isuksidrel.ko_disx");
        $this->db->order_by("ts_isuksid.wk_kirm","desc");

        $data = $this->db->get()->result();
        return $data;
    }



    private function tandai_disposisi($id_pesan,$kode) {
        $this->db->where("id_pesan",$id_pesan);
        $this->db->where("ke_user",$this->session->userdata("id_pengguna"));
        $this->db->update("relasi_pesan",array(
            "disposed" => $kode
        ));
        return true;
    }



    public function disxsatuid($id_disx,$ko_disx) {
        $this->db->select("ts_isuksid.*");
        $this->db->where("ts_isuksid.id_disx",$id_disx);
        $this->db->from("ts_isuksid");

        $data = $this->db->get()->row();

        $this->db->select("pengguna.nama_lengkap,ts_isuksidrel.dari_user,ts_isuksidrel.ko_disx,ts_isuksidrel.dibaca");
        $this->db->from("pengguna");
        $this->db->join("ts_isuksidrel","ts_isuksidrel.ke_user = pengguna.id_pengguna","left");
        $this->db->where("ts_isuksidrel.id_disx",$id_disx);
        $this->db->where("ts_isuksidrel.ko_disx",$ko_disx);

        $pengguna = $this->db->get()->result();

        //$id_pesan = $this->db->select("id_pesan")->from("ts_isuksidrel")->where("kode_disposisi",$kode_disposisi)
         //   ->limit(1)->get()->row()->id_pesan;

        //$lampiran_pesan = $this->db->where("id_pesan",$id_pesan)->get("pesan")->row();

        $data->penerima = $pengguna;
        //$data->lampiran_surat = $lampiran_pesan;

        return $data;
    }


    public function ambil_satu_disposisi_masuk($id_disposisi,$kode_disposisi) {
        $this->db->select("disposisi.*");
        $this->db->where("disposisi.id_disposisi",$id_disposisi);
        $this->db->from("disposisi");

        $data = $this->db->get()->row();

        $this->db->select("pengguna.nama_lengkap AS pengirim,ts_isuksidrel.dari_user,ts_isuksidrel.ke_user,ts_isuksidrel.kode_disposisi,ts_isuksidrel.dibaca,ts_isuksidrel.starred,ts_isuksidrel.id_ts_isuksidrel");
        $this->db->from("pengguna");
        $this->db->join("ts_isuksidrel","ts_isuksidrel.dari_user = pengguna.id_pengguna","left");
        $this->db->where("ts_isuksidrel.id_disposisi",$id_disposisi);
        $this->db->where("ts_isuksidrel.kode_disposisi",$kode_disposisi);
        $this->db->where("ts_isuksidrel.ke_user",$this->session->userdata("id_pengguna"));

        $pengguna = $this->db->get()->row();

        $id_pesan = $this->db->select("id_pesan")->from("ts_isuksidrel")->where("kode_disposisi",$kode_disposisi)
            ->limit(1)->get()->row()->id_pesan;

        $lampiran_pesan = $this->db->where("id_pesan",$id_pesan)->get("pesan")->row();

        $data->penerima = $pengguna;
        $data->lampiran_surat = $lampiran_pesan;

        return $data;
    }



    public function ambil_disposisi_per_tanggal($tgl) {
        $this->db->where("DATE(waktu_kirim)",$tgl);
        return $this->db->get("disposisi")->num_rows();
    }

    public function konfirmbaca($id_disx,$ko_disx) {
        $this->db->where("id_disx",$id_disx)
            ->where("ko_disx",$ko_disx)
            ->where("ke_user",$this->session->userdata("id_pengguna"))
            ->update("ts_isuksidrel",array("dibaca"=>1));
        return true;
    }

    public function selesai($id_disx) {
        $this->db->where("id_disx",$id_disx)
            ->update("ts_isuksid",array("selesai"=>1));
        return true;
    }

    public function follow_up($post) {
        return $this->db->insert("ts_isuksidres",$post);
    }

    public function ambil_follow_up($id_disx) {
        $this->db->select("ts_isuksidres.*,pengguna.nama_lengkap");
        $this->db->from("ts_isuksidres");
        $this->db->join("pengguna","ts_isuksidres.id_pengguna = pengguna.id_pengguna","left");
        return $this->db
            ->where("id_disx",$id_disx)
            ->get()->result();
    }

    public function update_star($id_relasi_disposisi,$stat) {
        $this->db->where("id_relasi_disposisi",$id_relasi_disposisi);
        $this->db->update("relasi_disposisi",array(
            "starred" => $stat
        ));
    }

}