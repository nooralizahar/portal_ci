<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Divisi_model extends CI_Model {

    public function divisi_semua() {
        $data = $this->db->order_by("nm_div",'asc')->get("ts_isivid")->result();
        return $data;
    }

    public function divisi_sesama($id) {
    $data = $this->db->where("id_div",$id)->get("ts_isivid")->result();
    return $data;
    }

    public function divtamb() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);

        if(isset($post["nm_div"])) {
            $num = $this->db->insert("ts_isivid",$post);
            return $num;
        } else return false;
    }
    
    public function divedit() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
      

        if(isset($post["nm_div"])) {
            $this->db->where("id_div",$post["id_div"]);
            unset($post["id_div"]);
            $num = $this->db->update("ts_isivid",$post);
            return $num;
        } else return false;
    }

    public function pildiv($id) {
        $data = $this->db->where("id_div",$id)->get("ts_isivid")->row();
        return $data;
    }

    public function ambil_berdasarkan_id($id) {
        $data = $this->db->where("id_div",$id)->get("ts_isivid")->row();
        return $data;
    }
    private function upload_filey($files,$path = "assets/uploads/logo/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|jpeg|bmp|gif|doc|docx|xls|xlsx|ppt|pptx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() .'_'. md5($image) . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }

}