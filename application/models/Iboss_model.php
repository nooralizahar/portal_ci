<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Iboss_model extends CI_Model {

// dbz digunakan untuk mengakses database ke-2
// private $dbz;

 public function __construct()
 {
  parent::__construct();
         $this->dbz = $this->load->database('newdb', TRUE);
        // $this->dbe = $this->load->database('dbbaru', TRUE);
 }

    public function smehrb() {
        $data = $this->dbz->where("product_type_desc","SME")->order_by("activation_date","DESC")->get("v_grid_order_monitoring")->result();
        //$data = $this->dbz->select("account_no,name,sales_name,product_type_desc,status_desc,activation_date,current_holder")->where("rfs_date","2022-03-17")->get("v_grid_order_monitoring")->result();
        return $data;
    }

    public function jmlpel($param) {
        
        $data = $this->dbz->select("status,count(t_account_id) as jml")->where("status",$param)->group_by("status")->get("t_account")->result();
        return $data;

    }

    public function detpel($param) {
        
        $data = $this->dbz->select("nama_pelanggan,odp_code,tgl_aktif,status,nama_ikr")->where("status",$param)->order_by('tgl_aktif','desc')->get("v_jlm_detil_cus")->result();
        return $data;

    }

    public function detodpthn() {
        
        $data = $this->dbz->get("v_jlm_odp_by_year")->result();
        return $data;

    }

    public function detpelthn() {
        
        $data = $this->dbz->get("v_jlm_rekap_cus_by_year")->result();
        return $data;

    }

    public function detodplok($p) {
        
        $data = $this->dbz->where('thn',$p)->get("v_jlm_odp_by_loc_pivot")->result();
        return $data;

    }

    public function roll_rekap_tahun()
    {
        $data = $this->db->query("SELECT YEAR(tgpas) as tahun, SUM(takab) as total FROM app_latrop_lebak GROUP BY YEAR(tgpas) ORDER BY YEAR(tgpas) DESC");
        return $data;
    }

    public function odc_rekap_tahun()
    {
        $data = $this->db->query("SELECT YEAR(tg_cre) as tahun, COUNT(idodc) as total FROM t_odc GROUP BY YEAR(tg_cre) ORDER BY YEAR(tg_cre) DESC ");
        return $data;
    }
    
    

    public function pelakt($param,$s) { 
        if($s=='h'){   
        $data = $this->dbz->where("tgl_aktif",$param)->get("v_jlm_detil_cus")->num_rows();
        }else{
        $data = $this->dbz->like('CAST(tgl_aktif AS VARCHAR)',$param, 'after')->get("v_jlm_detil_cus")->num_rows();    
        }
        return $data;
    }

    public function odpakt($param) { 
        $data = $this->dbz->like('CAST(create_date AS VARCHAR)',$param, 'after')->get("t_net_odp")->num_rows();    
        return $data;
    }


    public function pilodp($pil,$par) {
    	if($pil=='code' and $par=='*'){
    	$data = $this->dbz->where('coordinate is NOT NULL', NULL, FALSE)->get("v_net_odp",700,750)->result();
    	}elseif($pil=='code' and !empty($par)){
        $data = $this->dbz->where($pil.' like ','%'.$par.'%')->where('coordinate is NOT NULL', NULL, FALSE)->get("v_net_odp")->result();
        }elseif($pil=='location_name' and !empty($par)){
        $data = $this->dbz->where($pil.' like ','%'.$par.'%')->where('coordinate is NOT NULL', NULL, FALSE)->get("v_net_odp")->result();    
	    }
        return $data;
    }

    public function pilsat($pil,$par) {
        if($pil=='code' and $par=='*'){
        $data = $this->dbz->where('coordinate is NOT NULL', NULL, FALSE)->limit(1)->get("v_net_odp",700,750)->row();
        }elseif($pil=='code' and !empty($par)){
        $data = $this->dbz->where($pil.' like ','%'.$par.'%')->where('coordinate is NOT NULL', NULL, FALSE)->limit(1)->get("v_net_odp")->row();
        }elseif($pil=='location_name' and !empty($par)){
        $data = $this->dbz->where($pil.' like ','%'.$par.'%')->where('coordinate is NOT NULL', NULL, FALSE)->limit(1)->get("v_net_odp")->row();
        }
        return $data;       

    }

    public function piltikor($p) {
        $data = $this->dbz->where("t_net_odp_id",$p)->get("v_net_odp")->result();
        return $data;
    }


    public function datodp() {
        $data = $this->dbz->order_by("create_date","desc")->get("v_net_odp")->result();
        return $data;
    }

    public function jmlodp() {
        $data = $this->dbz->get("t_net_odp")->num_rows();
        return $data;
    }

    public function odpdetil($p) {
        $data = $this->dbz->where('t_net_odp_id',$p)->order_by('tgl_aktif','asc')->get("v_net_odp_isi_detil")->result();
        return $data;
    }


    public function kapodp() {
        $data = $this->dbz->select("sum(capacity) as cap")->get("t_net_odp")->row();
        return $data;
    }

    public function odpkos() {
        $all = $this->dbz->get("t_net_odp")->num_rows();
        $use = $this->dbz->get("v_net_odp_used")->num_rows();
        $data=$all-$use;
        return $data;
    }

    public function odpsis($opr) {
        $data = $this->dbz->where("p ".$opr."",50)->get("v_net_odp_persen")->num_rows();
        return $data;
    }

    public function odpnuh() {
        $data = $this->dbz->select("count(code) as jml")->where("sisa",0)->get("v_net_odp_balance")->row();
        return $data;
    }


    public function semolt() {
        
        $data ="select concat(b.olt_name,'-s',LPAD(split_part(split_part(replace(card,'gpon-onu_1/',''),':',1),'/',1),2,'0'),'_p',LPAD(split_part(split_part(replace(card,'gpon-onu_1/',''),':',1),'/',2),2,'0')) as olt_detil,a.olt_ip,split_part(replace(card,'gpon-onu_1/',''),':',1) as slotport from public.t_net_pon_card a left join public.t_net_olt b on a.olt_ip=b.olt_ip group by b.olt_name,a.olt_ip,slotport order by b.olt_name,olt_detil asc;";
        return $this->dbz->query($data)->result();

    }

    public function seminc() {
        
        //SELECT sales_id,sales_name,count(activation_date)as jml,to_char(activation_date, 'YYYY-MM')  as bln FROM "public"."v_account" where activation_date is not null group by sales_id,sales_name,bln;
        $data = $this->dbz->select("sales_id,sales_name,count(activation_date)as jml,to_char(activation_date, 'YYYY-MM')  as bln " )->where('status_desc','ACTIVE')->where('sales_name !=','migrasi')->group_by("sales_id,sales_name,bln")->order_by('jml','desc')->get("v_account")->result();//->limit(1) order_by("name","asc")->
        return $data;

    }

    public function usribo() {
        
        $data = $this->dbz->select('sys_user_id,name,status,department_name')->order_by('name','asc')->get("v_sys_user")->result();
        return $data;

    }

    public function incsal($id) {
        
        $data = $this->dbz->select("sales_id,sales_name,count(activation_date)as jml,to_char(activation_date, 'YYYY-MM')  as bln " )->where('sales_id',$id)->group_by("sales_id,sales_name,bln")->order_by('jml','desc')->get("v_account")->result();//->limit(1) order_by("name","asc")->
        return $data;

    }
 


    public function totlok() {
        
        $data = $this->dbz->get("v_location")->num_rows();
        return $data;

    }

    public function perlok() {
        
        $data = $this->dbz->where('area_name','Perumahan')->get("v_location")->num_rows();
        return $data;

    }

    public function pilare($p) {
        
        $data = $this->dbz->where('area_name',$p)->get("v_location")->result();
        return $data;

    }

    public function pillok($pilih) {
        
        $data = $this->dbz->where("t_location_id",$pilih)->get("v_location")->row();//->limit(1)
        return $data;

    }


    public function odpdolo() {
        
        $data = $this->dbz->select('t_net_odp_id, code,location_name,coordinate')->get("v_net_odp")->result();
        $ddat = $this->db->empty_table('t_net_odp_clone');
        $wakt = date('Y-m-d H:i:s');

        foreach ($data as $k) {
            $dat = array(
            'id'   => $k->t_net_odp_id,
            'nama' => $k->code,
            'alamat'   => $k->location_name,
            'kordinat' => $k->coordinate,           
            'tgl'  => $wakt);

        $this->db->insert("t_net_odp_clone",$dat);
            
        }

        return $data;

    }

    public function oltdolo() {
        
        $data = $this->semolt();
        $ddat = $this->db->empty_table('t_net_olt_clone');
        $wakt = date('Y-m-d H:i:s');

        foreach ($data as $o) {
            $dat = array(
            'olt_detil' => $o->olt_detil,
            'olt_ip'    => $o->olt_ip,
            'slotport'  => $o->slotport,           
            'tgl'       => $wakt);

        $this->db->insert("t_net_olt_clone",$dat);
            
        }

        return $data;

    }



}