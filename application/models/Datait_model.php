<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Datait_model extends CI_Model {


    public function tugas() {
        $data = $this->db->get("to_sagut")->result();
        return $data;
    }

    public function odcsemua() {
        $data = $this->db->get("t_odc")->result();
        return $data;
    }

    public function odpsemua() {
        $data = $this->db->get("t_odp")->result();
        return $data;
    }

    public function dafper() {
        $data = $this->db->order_by('id_gam')->get("tp_rabmag")->result();
        return $data;
    }

    public function desper() {
        $data = $this->db->get("v_ds_gam")->result();
        return $data;
    }

    public function dasite($idp) {
        $data = $this->db->where('id_worde',$idp)->get("v_pmo_site")->result();
        return $data;
    }

    public function perhap($idp) {
        return $this->db->where("id_tpl",$idp)->delete("tp_lopot");
    }

    public function prohap($idp) {
        return $this->db->where("id_wopro",$idp)->delete("ts_wopro");
    }

    public function wosemua() {

        $this->db->from("v_tp_owatad");
        $this->db->order_by("id_worde", "desc");
        $data = $this->db->get(); 
        return $data->result();

    }

    public function wo4baa() {

        $this->db->from("v_tp_owatad");
        $this->db->where("dan",1);
        $this->db->order_by("id_worde", "desc");
        $data = $this->db->get(); 
        return $data->result();

    }

    public function laporan() {
        
        $data = $this->db->order_by("id", "desc")->get("reports")->result();
        return $data;
    }

    public function vodcsemua() {
        $data = $this->db->get("v_t_odc")->result();
        return $data;
    }

    public function vodpsemua() {
        $data = $this->db->get("t_odp")->result();
        return $data;
    }
    public function prosemua($id) {

        $this->db->from("ts_wopro");
        $this->db->where("id_worde", $id);
        $data = $this->db->get(); 
        return $data->result();

    }

    public function ftthsemua() {

        $data = $this->db->get("radcheck")->result();
        return $data;

    }

    public function dakhir() {

        $data = $this->db->where("employee_id", $this->session->userdata("id_pengguna"))->order_by("id", "desc")->limit(1)->get("reports")->result();
        return $data;

    }

    public function laporanpilih() {

        $data = $this->db->where("employee_id", $this->session->userdata("id_pengguna"))->order_by("date", "desc")->get("reports")->result();//->limit(1)
        return $data;

    }    

    public function rhrgpilih($pilih) {

        $data = $this->db->like("tg_buat", $pilih)->order_by("tg_buat", "desc")->get("ts_agrah")->result();
        return $data;

    }

    public function wordpilih($pilih) {

        $data = $this->db->like("tg_issue", $pilih)->order_by("id_worde", "desc")->get("tp_owatad")->result();
        return $data;

    }

    public function wopilih() {

        $data = $this->db->where("us_assto", $this->session->userdata("id_pengguna"))->order_by("id_worde", "desc")->get("v_tp_owatad")->result();
        return $data;

    }

    public function ftthpilih($pilih) {

        $data = $this->db->like("registerdate", $pilih)->order_by("registerdate", "desc")->get("radcheck")->result();
        return $data;

    }


    public function imtcpilih($pilih) {

        $data = $this->db->like("tgmtc", $pilih)->order_by("tgmtc", "desc")->get("no_ctmofni")->result();
        return $data;

    }

    public function afocpilih($pilih) {

        $data = $this->db->like("date", $pilih)->order_by("date", "desc")->get("v_reports")->result();
        return $data;

    }


    public function nflixsemua() {
        $data = $this->db->order_by("registerdate","desc")->get("v_xilfrsu")->result();
        return $data;
    }

    public function mtainsemua() {
        $data = $this->db->order_by("tgmtc","desc")->get("no_ctmofni")->result();
        return $data;
    }

    public function selesai($id) {

        $kolom = [
            'appv1' => $this->session->userdata("nama_lengkap"),'tglapv1'=>date("Y-m-d H:i:s")];

        $data=$this->db->where('id_rpu', $id)->update('ts_esacrup',$kolom );
        
        return $data;
    }
    public function selesai2($id) {

        $kolom = [
            'appv2' => $this->session->userdata("nama_lengkap"),'tglapv2'=>date("Y-m-d H:i:s")];

        $data=$this->db->where('id_rpu', $id)->update('ts_esacrup',$kolom );
        
        return $data;
    }

    public function selesai3($id) {

        $kolom = [
            'appv3' => $this->session->userdata("nama_lengkap"),'tglapv3'=>date("Y-m-d H:i:s")];

        $data=$this->db->where('id_rpu', $id)->update('ts_esacrup',$kolom );
        
        return $data;
    }

    public function lfocselesai() {
        $id=$this->input->post("id");
        $ko=$this->input->post("latlng");
        $kolom = ['site'=>$ko,'is_solved'=>1,'updated_at'=>date("Y-m-d H:i:s")];

        $data=$this->db->where('id', $id)->update('reports',$kolom );     
        return $data;
    }

    public function purcsemua() {
        $data = $this->db->order_by("tglbuat","desc")->get("ts_esacrup")->result();
        return $data;
    }

    public function purcpilih($pilih) {

        $data = $this->db->where("id_rpu", $pilih)->get("ts_esacrup")->row();
        return $data;

    }

    public function bargpilih($pilih) {

        $data = $this->db->where("id_rpu", $pilih)->get("ts_esacrupitem")->result();
        return $data;

    }

    public function barghrg($pilih) {

        $data = $this->db->where("id_brg", $pilih)->get("ts_esacrupitem")->row();
        return $data;

    }

    public function purcusrnm() {
        $data = $this->db->where("pengirim", $this->session->userdata("username"))->order_by("tglbuat","desc")->get("ts_esacrup")->result();
        return $data;
    }
    public function purcusrja() {
        $data = $this->db->where("appv1 !=",null)->get("ts_esacrup")->result();
        return $data;
    }
    public function purcusrap() {
        $data = $this->db->where("penerima", $this->session->userdata("id_pengguna"))->order_by("tglbuat","desc")->get("ts_esacrup")->result();
        return $data;
    }
    public function purcapusr() {
        $data = $this->db->where("appv1 !=",null)->get("ts_esacrup")->result();
        return $data;
    }

    public function purckirm($id) {
        if($this->session->userdata("id_spv")==38){
        $field = [
            'penerima' => $this->session->userdata("id_spv"),
            'appv1' => 'System',
            'tglapv1' => date('Y-m-d H:i:s'),
        ];
        }else{
        $field = [
            'penerima' => $this->session->userdata("id_spv"),
        ];}

        $data=$this->db->where('id_rpu', $id)->update('ts_esacrup',$field );
        
        return $data;
    }

    public function purcapvus()
    {  /*$query = $this->db->query("SELECT `ts_esacrup`.*,sum(`ts_esacrupitem`.`brghar`) as harga FROM `ts_esacrup` LEFT JOIN `ts_esacrupitem` ON `ts_esacrup`.`id_rpu`=`ts_esacrupitem`.`id_rpu` GROUP by `id_rpu`"); */
        $query = $this->db->query("SELECT * FROM `v_ts_esacrup` WHERE `harga`!=0"); 
        return $query->result();
    }

    public function purcapvfi()
    {  /*$query = $this->db->query("SELECT `ts_esacrup`.*,sum(`ts_esacrupitem`.`brghar`) as harga FROM `ts_esacrup` LEFT JOIN `ts_esacrupitem` ON `ts_esacrup`.`id_rpu`=`ts_esacrupitem`.`id_rpu` GROUP by `id_rpu`"); */
        $query = $this->db->query("SELECT * FROM `v_ts_esacrup` WHERE `harga`>5000000"); 
        return $query->result();
    }

    public function purcharga($id) {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $field = [
            'brghar' => $post["brghar"],
        ];

        $data=$this->db->where('id_brg', $id)->update('ts_esacrupitem',$field );
        
        return $data;
    }

    public function bargsemua() {
        $data = $this->db->order_by("id_brg","desc")->get("ts_esacrupitem")->result();
        return $data;
    }

    public function nflixtamba($usernm,$nama) {
        $now = date('Y-m-d H:i:s');
        $num = rand();
        $nam = substr($nama,rand(1,5),3).$num."@bnetfit.id";
        $pwd = uniqid();
        $insdata = array(
                    "username" => $usernm,
                    "usrnetflix" => strtolower($nam),
                    "pwdnetflix" => $pwd,
                    "tg_cre" => $now,
                    "us_cre" => $this->session->userdata("username")
                );
        $this->db->insert("t_xilfrsu",$insdata);
        return $insdata;
    }

    public function odcbyid($id) {
    $data = $this->db->where("idodc",$id)->get("t_odc")->result();
    return $data;
    }

    public function lfoctamb() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        $post["created_at"] = date("Y-m-d H:i:s");
        $post["employee_id"] = $this->session->userdata("id_pengguna");
        if(isset($post["customer"])) {
            $num = $this->db->insert("reports",$post);
            return $num;
        } else return false;
    }
    public function topopil($id) {

        $this->db->from("tp_lopot");
        $this->db->where("id_worde", $id);
        $data = $this->db->get(); 
        return $data->result();

    }
    public function asstopi($id) {

        $this->db->from("tp_owatad_assto");
        $this->db->where("wono", $id);
        $data = $this->db->get(); 
        return $data->result();

    }
    public function toptamb() {
        $post = $this->input->post();
        unset($post["ds_gam_ddl"]);
        unset($post["ds_gam_txt"]);
        unset($post["btnSubmit"]);

        if($this->input->post('id_cir')=='89'){
        $post = array(
            'id_worde'   => $this->input->post('id_worde'),
            'id_cir'   => $this->input->post('id_cir'),
            'id_gam'   => '89',
            'ds_gam'   => 'upload','po_inp'   => '-','po_out'   => '-',
            'ip_add'   => '-','cf_vla'   => '-'
        );
        }

        $post["us_tpl"] =$this->session->userdata('username');
        $post["tg_tpl"] =date('Y-m-d H:i:s');

        $a=$this->db->where('wono',$this->input->post('id_worde'))->get("tp_owatad_assto")->num_rows();
        if($a==0){
        $taken = array(
            'wono'   => $this->input->post('id_worde'),
            'filetopo'   => '',
            'us_assto' => $this->session->userdata('id_pengguna'),
            'tg_assto'     => date('Y-m-d H:i:s')
        );
        $this->db->insert("tp_owatad_assto",$taken);
        }else{}

        if(isset($post["id_worde"])) {
            $num = $this->db->insert("tp_lopot",$post);
            return $num;
        } else return false;

    }

    public function odctamb() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);

        if(isset($post["nama"])) {
            $num = $this->db->insert("t_odc",$post);
            return $num;
        } else return false;
    }

    public function odptamb() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);

        if(isset($post["nama"])) {
            $num = $this->db->insert("t_odp",$post);
            return $num;
        } else return false;
    }


    public function protamb() {
        $post = $this->input->post();
        unset($post["site_ddl"]);
        unset($post["site_txt"]);
        unset($post["btnSubmit"]);

        if(isset($post["id_worde"])) {
            $num = $this->db->insert("ts_wopro",$post);
            return $num;
        } else return false;
    }

    public function procomp($id) {
        $komplit = array(
            'id_worde'   => $id,
            'tg_pro'   => date('Y-m-d'),
            'site' => '',
            'uraian' => 'completed',
            'tg_cre'     => date('Y-m-d H:i:s')
        );

        if(isset($id)) {
            $kom = $this->db->insert("ts_wopro",$komplit);
            return $kom;
        } else return false;
    }
    public function wodispatch() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);

        if(isset($post["wono"])) {
            $num = $this->db->insert("tp_owatad_assto",$post);
            return $num;
        } else return false;
    }

    public function purctamb() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        
        if(isset($post["no_rpu"])) {
            $num = $this->db->insert("ts_esacrup",$post);
            return $num;
        } else return false;
    }

    public function bargtamb() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        
        if(isset($post["id_rpu"])) {
            $num = $this->db->insert("ts_esacrupitem",$post);
            return $num;
        } else return false;
    }

    public function barghapu($idbrg) {
        return $this->db->where("id_brg",$idbrg)->delete("ts_esacrupitem");
    }

    public function bargid($id) {

        $this->db->from("ts_esacrupitem");
        $this->db->where("id_rpu", $id);
        $data = $this->db->get(); 
        return $data->result();

    }
    public function edit() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
      

        if(isset($post["nama_dinas"])) {
            $this->db->where("id_dinas",$post["id_dinas"]);
            unset($post["id_dinas"]);
            $num = $this->db->update("dinas",$post);
            return $num;
        } else return false;
    }

    public function ambil_berdasarkan_id($id) {
        $data = $this->db->where("id_dinas",$id)->get("dinas")->row();
        return $data;
    }


    private function upload_filey($files,$path = "assets/uploads/logo/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|jpeg|bmp|gif|doc|docx|xls|xlsx|ppt|pptx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() .'_'. md5($image) . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }




}