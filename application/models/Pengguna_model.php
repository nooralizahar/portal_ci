<?php


defined("BASEPATH") OR exit("Akses ditolak!");

class Pengguna_model extends CI_Model {

    public function login($username,$password) {
      /*  $this->db->select("pengguna.*");
        $this->db->select("jabatan.nama_jabatan");
        $this->db->select("dinas.nama_dinas");
        $this->db->from("pengguna");
        $this->db->join("jabatan","pengguna.id_jabatan = jabatan.id_jabatan","left");
        $this->db->join("dinas","pengguna.id_dinas = dinas.id_dinas","left");*/
        $this->db->select("v_pengguna_login.*");
        $this->db->from("v_pengguna_login");
        $data = $this->db->where("username",$username)->where("password",md5($password))->where("blokir",0)->get();
        if($data->num_rows() > 0) {
            return $data->row_array();
        } else return false;
    }

    public function login_intra($username) {
        $this->db->select("v_pengguna_login.*");
        $this->db->from("v_pengguna_login");
        $data = $this->db->where("username", $username)->where("blokir", 0)->get();
        if($data->num_rows() > 0) {
            return $data->row_array();
        } else return false;
    }

    public function tamakt($akt,$usr){
        $tgakt = date('Y-m-d H:i:s');
        
        if(empty($this->session->userdata("id_div"))){
            $iddiv =0;  
        }else{
            $iddiv = $this->session->userdata("id_div");
        }

        $dat = array(
                    "tgakt" => $tgakt,
                    "iddiv" => $iddiv,
                    "nmusr" => $usr,
                    "aktifitas" => $akt,
                    "status" => true
                );

    $sim = $this->db->insert("app_latrop_gol",$dat);
    return $sim;
    }

    public function tambah_penerima_otomatis() {
        $post = $this->input->post();
        $daftar_penerima = $post["penerima"];
        $this->db->where("id_pengguna",$this->session->userdata("id_pengguna"));
        $this->db->delete("penerima_otomatis");
        foreach($daftar_penerima as $key=>$pn) {
            $ins[$key]["penerima"] = $pn;
            $ins[$key]["id_pengguna"] = $this->session->userdata("id_pengguna");
        }
        return $this->db->insert_batch("penerima_otomatis",$ins);
    }

    public function ambil_penerima_otomatis() {
        $this->db->select("penerima");
        $this->db->where("id_pengguna",$this->session->userdata("id_pengguna"));
        $this->db->from("penerima_otomatis");
        return $this->db->get()->result();
    }

    public function semua() {
        $this->db->select("pengguna.*,ts_natabaj.nm_jab,ts_isivid.nm_div");
        $this->db->from("pengguna");
        $this->db->join("ts_natabaj","pengguna.id_jabatan = ts_natabaj.id_jab","left");
        $this->db->join("ts_isivid","pengguna.id_div = ts_isivid.id_div");

        $data = $this->db->get()->result();
        return $data;
    }

    public function nsemua() {

        $data = $this->db->get('v_pengguna_login')->result();
        return $data;
    }

    public function nocsem() {
        $this->db->select("pengguna.*,ts_natabaj.nm_jab,ts_isivid.nm_div");
        $this->db->from("pengguna");
        $this->db->join("ts_natabaj","pengguna.id_jabatan = ts_natabaj.id_jab","left");
        $this->db->join("ts_isivid","pengguna.id_div = ts_isivid.id_div");
        $this->db->where("pengguna.id_jabatan",17);

        $data = $this->db->get()->result();
        return $data;
    }

    public function semua_psales() {
        $this->db->select("pengguna.*,ts_natabaj.nm_jab,ts_isivid.nm_div");
        $this->db->from("pengguna");
        $this->db->join("ts_natabaj","pengguna.id_jabatan = ts_natabaj.id_jab","left");
        $this->db->join("ts_isivid","pengguna.id_div = ts_isivid.id_div");
        $this->db->where("pengguna.id_jabatan",6);
        $data = $this->db->get()->result();
        return $data;
    }

        public function semua_saladm() {
        $indata=array(4,14);
        $this->db->select("pengguna.*,ts_natabaj.nm_jab,ts_isivid.nm_div");
        $this->db->from("pengguna");
        $this->db->join("ts_natabaj","pengguna.id_jabatan = ts_natabaj.id_jab","left");
        $this->db->join("ts_isivid","pengguna.id_div = ts_isivid.id_div");
        $this->db->where_in("pengguna.id_jabatan",$indata);
        $data = $this->db->get()->result();
        return $data;
    }
        public function ambil_semuax() {
        $data = $this->db->get("pengguna")->result();
        return $data;
    }

        public function data_atasan() {
        $this->db->select("pengguna.*,ts_natabaj.nm_jab,ts_isivid.nm_div");
        $this->db->from("pengguna");
        $this->db->join("ts_natabaj","pengguna.id_jabatan = ts_natabaj.id_jab","left");
        $this->db->join("ts_isivid","pengguna.id_div = ts_isivid.id_div");
        $this->db->where("pengguna.blokir =",0);
        $this->db->where("ts_natabaj.nm_jab like",'%manager%');
        $this->db->or_where("ts_natabaj.nm_jab like",'%GM%');
        $this->db->or_where("ts_natabaj.nm_jab like",'%visor%');
        $this->db->or_where("ts_natabaj.nm_jab like",'%direktur%');
        $this->db->order_by('nama_lengkap','asc');
        $data = $this->db->get()->result();
        return $data;
    }

        public function data_head() {
        $this->db->select("pengguna.*,ts_natabaj.nm_jab,ts_isivid.nm_div");
        $this->db->from("pengguna");
        $this->db->join("ts_natabaj","pengguna.id_jabatan = ts_natabaj.id_jab","left");
        $this->db->join("ts_isivid","pengguna.id_div = ts_isivid.id_div");
        $this->db->where("pengguna.blokir =",0);
        $this->db->where("ts_natabaj.nm_jab like",'%manager%');
        $this->db->or_where("ts_natabaj.nm_jab like",'%GM%');
        $this->db->order_by('nama_lengkap','asc');
        $data = $this->db->get()->result();
        return $data;
    }
        public function ambil_saya() {
        $this->db->select("pengguna.id_pengguna,pengguna.nama_lengkap,pengguna.blokir,pengguna.nip,pengguna.disposisi,pengguna.notifikasi,pengguna.paraf,jabatan.nama_jabatan,jabatan.ks_jabpa,jabatan.ks_jabpta,dinas.nama_dinas,dinas.kodrat,dinas.kodta");
        $this->db->from("pengguna");
        $this->db->join("jabatan","pengguna.id_jabatan = jabatan.id_jabatan","left");
        $this->db->join("dinas","pengguna.id_dinas = dinas.id_dinas");
        $this->db->where("pengguna.id_pengguna",$this->session->userdata("id_pengguna"));
        $data = $this->db->get()->result();
        return $data;
    }



    public function tambah() {
        //panel/tambahpengguna

        $post = $this->input->post();
        unset($post["btnSubmit"]);

         /*  if($_FILES["attachtp"]["name"][0] != "") {
                $gambar = $this->upload_filex($_FILES["attachtp"]);
                if($gambar == false) {
                    redirect(base_url("panel/tambahpengguna/?err"));
                    exit();
                }
                $post["paraf"] = json_encode($gambar);
            }*/

        if(isset($post["nama_lengkap"]) && isset($post["id_jabatan"]) && isset($post["id_div"]) && isset($post["username"]) && isset($post["password"]) && isset($post["notifikasi"])) {
            $post["password"] = md5($post["password"]);
            $post["hashword"] = password_hash($post["password"], PASSWORD_DEFAULT);
            $post["blokir"] = 0;

            $num = $this->db->insert("pengguna",$post);
            return $num;
        } else return false;
    }

    public function ambil_berdasarkan_id($id)
    {
        $data = $this->db->where("idp", $id)->get("v_pengguna")->row();
        return $data;
    }



    public function edit() {
        $post = $this->input->post();

        if(isset($post["password"])) {
            if($post["password"] == "")
                unset($post["password"]);
            else
                $post["password"] = md5($post["password"]);
        }



        unset($post["btnSubmit"]);

   

        if(isset($post["nama_lengkap"]) && isset($post["id_jabatan"]) && isset($post["id_div"]) && isset($post["username"])) {
            
            $this->db->where("id_pengguna",$post["id_pengguna"]);
            unset($post["id_pengguna"]);
            $num = $this->db->update("pengguna",$post);
            return $num;
        } else return false;
    }

        public function editpro() {
        
        $post = $this->input->post();

        if(isset($post["password"])) {
            if($post["password"] == "")
                unset($post["password"]);
            else
                $post["password"] = md5($post["password"]);
        }



        unset($post["btnSubmit"]);
        unset($post["id_jabatan"]);
        unset($post["id_div"]);
        unset($post["id_mgr"]);
        unset($post["level"]);
        unset($post["email"]);
        unset($post["mn_akses"]);
        unset($post["username"]);
        unset($post["blokir"]);

        if(isset($post["nama_lengkap"])) {
            
            $this->db->where("id_pengguna",$post["id_pengguna"]);
            unset($post["id_pengguna"]);
            $num = $this->db->update("pengguna",$post);
            return $num;
        } else return false;
    }

    public function edit_profil_pribadi() {
        $data = $this->input->post();
        if($data["username"] != "" && $data["cpassword"] != "") {
            if(md5($data["cpassword"]) == $this->session->userdata("password")) {
                // TO-DO : Update data user

                $this->db->where("id_pengguna",$this->session->userdata("id_pengguna"));
                unset($data["cpassword"]);

                if($data["password"] == ""){
                    unset($data["password"]);
                } else {
                    $data["password"] = md5($data["password"]);
                    $this->session->set_userdata("password",$data["password"]);
                }

                $this->session->set_userdata("username",$data["username"]);
                return ($this->db->update("pengguna",$data) == true) ? 3 : 0;

            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    public function blokir($id) {
        $this->db->where("id_pengguna",$id)->update("pengguna",array("blokir"=>1));
    }

    public function buka($id) {
        $this->db->where("id_pengguna",$id)->update("pengguna",array("blokir"=>0));
    }

    public function ambil_per_grup() {
       // $this->db->where("id_dinas !=",$this->session->userdata("id_dinas"));
        $daftar_div = $this->db->get("ts_isivid")->result();
        $detail_user = array();

        foreach($daftar_div as $divisi) {
            $detail_user[$divisi->nm_div] = $this->db->where("id_pengguna !=",$this->session->userdata("id_pengguna"))->select("pengguna.id_pengguna,pengguna.nama_lengkap,pengguna.id_div,ts_natabaj.nm_jab")->join("ts_natabaj","pengguna.id_jabatan = ts_natabaj.id_jab")->where("pengguna.id_div",$divisi->id_div)->get("pengguna")->result();
        }

        return $detail_user;
    }
    public function ambil_pengguna_sedinas() {
        $this->db->select("pengguna.*,jabatan.nama_jabatan");
        $this->db->where("pengguna.id_dinas",$this->session->userdata("id_dinas"));
        $this->db->join("jabatan","pengguna.id_jabatan = jabatan.id_jabatan","left");
        $daftar_pengguna = array();
        $daftar_pengguna[$this->session->userdata("nama_dinas")] = $this->db->get("pengguna")->result();
        return $daftar_pengguna;
    }

    public function ambil_pengguna_pilih() {
        
        $this->db->where("id_dinas","37");
        $this->db->or_where("id_dinas",$this->session->userdata("id_dinas"));

        $daftar_pilih = $this->db->get("dinas")->result();
        $detail_user = array();

        foreach($daftar_pilih as $dinas) {
            $detail_user[$dinas->nama_dinas] = $this->db->where("id_pengguna !=",$this->session->userdata("id_pengguna"))->select("pengguna.id_pengguna,pengguna.nama_lengkap,jabatan.nama_jabatan")->join("jabatan","pengguna.id_jabatan = jabatan.id_jabatan")->where("pengguna.id_dinas",$dinas->id_dinas)->get("pengguna")->result();
        }

        return $detail_user;
    }

    private function upload_filex($files,$path = "assets/uploads/paraf/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|jpeg|bmp|gif|doc|docx|xls|xlsx|ppt|pptx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() .'_'. md5($image) . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }



}