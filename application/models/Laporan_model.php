<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Laporan_model extends CI_Model {

    public function dafreq() {
        $data = $this->db->get("v_t_import_requester")->result();
        return $data;
    }

    public function semua() {
    	$thnini= date('Y');
        $data = $this->db->where('Created_Time like', $thnini.'%')->order_by('Request_ID','desc')->get("t_datame")->result();
        return $data;
    }
    public function pelanggan() {
    	$data = $this->db->get("v_requester_o")->result();
        return $data;
    }
    public function vendor() {
    	$data = $this->db->get("v_proside_o")->result();
        return $data;
    }

    public function view(){
        $blnini= date('Y/m');
		$pelang=$this->input->post('napel');
		$tgawal=$this->input->post('tgl_awal');
		$tgakhi=$this->input->post('tgl_akhir');
		
        return $this->db->where('Created_Time BETWEEN "'. date('Y/m/d', strtotime($tgawal)). '" and "'. date('Y/m/d', strtotime($tgakhi)).'"')->where('Requester', $pelang)->get('v_t_datame')->result(); 
    }
	public function hitung(){
        $blnini= date('Y/m');
		$pelang=$this->input->post('napel');
		$tgawal=$this->input->post('tgl_awal');
		$tgakhi=$this->input->post('tgl_akhir');

        return $this->db->where('Created_Time BETWEEN "'. date('Y/m/d', strtotime($tgawal)). '" and "'. date('Y/m/d', strtotime($tgakhi)).'"')->where('Requester', $pelang)->get('v_t_datame')->num_rows(); 
    }
    public function hitava(){
        $blnini= date('Y/m');
        $pelang=$this->input->post('napel');
        $tgawal=$this->input->post('tgl_awal');
        $tgakhi=$this->input->post('tgl_akhir');

       // $q ="SELECT `Requester`, SEC_TO_TIME( SUM( TIME_TO_SEC( `Durasi` ) ) ) AS TOT FROM `v_t_datame` WHERE `Requester`=".$pelang." AND `Created_Time` BETWEEN ".$tgawal." AND ".$tgakhi." GROUP BY `Requester`";
       
        return $this->db->select("`Requester`, SEC_TO_TIME( SUM( TIME_TO_SEC( `Durasi` ) ) ) AS tot")->where('Created_Time BETWEEN "'. date('Y/m/d', strtotime($tgawal)). '" and "'. date('Y/m/d', strtotime($tgakhi)).'"')->where('Requester', $pelang)->get('v_t_datame')->result(); 
    }
}