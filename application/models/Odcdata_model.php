<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Odcdata_model extends CI_Model {


    public function semua() {
    	$data = $this->db->order_by('tg_cre','desc')->get("v_t_odc")->result();
        return $data;
    }

    public function tambah() {

        $post = $this->input->post();
        
        unset($post["btnSimpan"]);       
        
      if($_FILES["attach"]["name"][0] != "") {
                $fot = $this->ulo_fot($_FILES["attach"]);
                if($fot == false) {
                    redirect(base_url("focmen/odcin/?err"));
                    exit();
                }
                $post["foto"] = json_encode($fot);
            }


        $post["tg_cre"] = date("Y-m-d H:i:s");
        $post["us_cre"] = $this->session->userdata("username");
        if(isset($post["nama"])) {
            $num = $this->db->insert("t_odc",$post);
            return $num;
        } else return false;
    }

    public function ubah() {
        
        //if(!empty($this->input->post("status"))){$stat=1;}else{$stat=0;


        $id=$this->input->post("id");
        $tgup=date("Y-m-d H:i:s");
        $usup=$this->session->userdata("username");
        $nmed=$this->input->post("nama");
        $aled=$this->input->post("alamat");
        $loed=$this->input->post("lokasi");
        //$checked = ($value === FALSE) ? 0 : 1;
        $koed=$this->input->post("kordinat");
        $kaed=$this->input->post("kapasitas");
        //$oled=$this->input->post("oltrel");

        $kolom = ['tg_upd'=>$tgup,'us_upd'=>$usup,'nama'=>$nmed,'alamat'=>$aled,'lokasi'=>$loed,'kordinat'=>$koed,'kapasitas'=>$kaed];

            if($_FILES["attach"]["name"][0] != "") {
                $fot = $this->ulo_fot($_FILES["attach"]);
                if($fot == false) {
                    redirect(base_url("focmen/odcin/?err"));
                    exit();
                }
                $kolom["foto"] = json_encode($fot);
            }


        $data=$this->db->where('idodc', $id)->update('t_odc',$kolom );     
        return $data;
    }

    public function rmolt($idt) {
               $hapus=$this->db->where("idolt",$idt)->delete("t_olt");
           return $hapus; 
    }
    
    public function pilih($param) {

        $data = $this->db->where("idodc", $param)->get("t_odc")->result();//->limit(1)
        return $data;

    }

    public function to_odp($id) {

        /*$p = array(
            'idodc' => 0,
            'odcport' => 0
          ); 

        //if(isset($post["idodc"])) {
            $h = $this->db->where('idodp',$id)->update("t_odp",$p);
            return $h;
        //} */

            $h = $this->db->where('id',$id)->delete("t_odc_odp");
            return $h;

    }

    public function odpdet($param) {

        $data = $this->db->where("idodc", $param)->get("v_odc_odp_brig")->result();//->limit(1)
        return $data;

    }
    public function oltdet($param) {

        $data = $this->db->where("idodc", $param)->get("t_olt")->result();//->limit(1)
        return $data;

    }

    public function tamodc() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $tgupd = date("Y-m-d H:i:s");
        $usupd = $this->session->userdata("id_pengguna");

        $split = explode("+",$this->input->post('idodp'));
        $odpid = current($split);
        $odpna = end($split);

        $post = array(
            'id' => $odpid ,
            'nama' => $odpna ,
            'idodc' => $this->input->post('idodc'),
            'odcport' => $this->input->post('odcport'),
            'tg_upd' => $tgupd,
            'us_upd' => $usupd
          ); 


        if(isset($post["idodc"])) {
            //$num = $this->db->where('idodp',$this->input->post('idodp'))->update("t_odp",$post);
            $num = $this->db->insert("t_odc_odp",$post);
            return $num;
        } else return false;
    }

    public function tamolt() {
        $post = $this->input->post();
        unset($post["btnSimpan"]);
        $nmip =explode('|', $this->input->post('nmolt')); 
        $post = array(
            'nmolt' => $nmip[0],
            'ipolt' => $nmip[1],
            'usbua' => $this->session->userdata("username"),
            'tgbua' => date("Y-m-d H:i:s"),
            'idodc' => $this->input->post('idodc'),
            'portodc' => $this->input->post('portodc')
          ); 


        if(isset($post["idodc"])) {
            $num = $this->db->insert("t_olt",$post);
            return $num;
        } else return false;
    }


    public function hapus($param) {
        $d=$this->db->where("idodc",$param)->get("t_odp")->num_rows();
        /*if(){
            //echo "Detil ODC masih belum kosong";
            return false;
        }else{
           $hapus=$this->db->where("idodc",$param)->delete("t_odc");
           return $hapus;  
        }*/
        if($d==0) {
           $hapus=$this->db->where("idodc",$param)->delete("t_odc");
           return $hapus; 
            return $num;
        } else return false;

    }
    public function pilodc($pil,$par) {
        if($pil=='nama' and $par=='*'){
        $data = $this->db->where('kordinat is NOT NULL', NULL, FALSE)->get("t_odc",700,750)->result();
        }elseif($pil=='nama' and !empty($par)){
        $data = $this->db->where($pil.' like ','%'.$par.'%')->where('kordinat is NOT NULL', NULL, FALSE)->get("t_odc")->result();
        }elseif($pil=='alamat' and !empty($par)){
        $data = $this->db->where($pil.' like ','%'.$par.'%')->where('kordinat is NOT NULL', NULL, FALSE)->get("t_odc")->result();    
        }
        return $data;
    }

    public function pilsat($pil,$par) {
        if($pil=='nama' and $par=='*'){
        $data = $this->db->where('kordinat is NOT NULL', NULL, FALSE)->limit(1)->get("t_odc",700,750)->row();
        }elseif($pil=='nama' and !empty($par)){
        $data = $this->db->where($pil.' like ','%'.$par.'%')->where('kordinat is NOT NULL', NULL, FALSE)->limit(1)->get("t_odc")->row();
        }elseif($pil=='alamat' and !empty($par)){
        $data = $this->db->where($pil.' like ','%'.$par.'%')->where('kordinat is NOT NULL', NULL, FALSE)->limit(1)->get("t_odc")->row();
        }
        return $data;       

    }
    private function ulo_fot($files,$path = "assets/uploads/odc/"){
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|gif',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() .'_'. md5($image) . "." . $ext;

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }    


}