<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Nocmainte_model extends CI_Model {

    public function semua() {
        $data = $this->db->order_by("tgmtc","desc")->get("no_ctmofni")->result();
        return $data;
    }



    public function baru() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);
        $post["us_cre"] = $this->session->userdata('username');
        $post["tg_cre"] = date('Y-m-d H:i:s');

            $mbaru = $this->db->insert("no_ctmofni",$post);
            return $mbaru;
        
    }

    public function pilih($param) {

        $p = $this->db->where("idmtc", $param)->get("no_ctmofni")->result();
        return $p;
    }

    public function simpan($param) {

        $tgl=date("Y-m-d H:i:s");
        $usr=$this->session->userdata("username");
        $tgm=$this->input->post("tgmtc");
        $jmm=$this->input->post("jmmtc");
        $jsm=$this->input->post("jsmtc");
        $dnt=$this->input->post("dntime");
        $dem=$this->input->post("demtc");
        $imm=$this->input->post("immtc");
        $nmv=$this->input->post("nmven");

        $mtcupd= ['tgmtc'=>$tgm,'jmmtc'=>$jmm,'jsmtc'=>$jsm,'dntime'=>$dnt,'demtc'=>$dem,'immtc'=>$imm,'nmven'=>$nmv,'tg_upd'=>$tgl,'us_upd'=>$usr];

        $u=$this->db->where('idmtc', $param)->update('no_ctmofni',$mtcupd );
        return $u;
    }

    public function hapus($param) {
        $hapus=$this->db->where("idmtc",$param)->delete("no_ctmofni");
        
    return $hapus;

    }

    public function jadtam() {
            $jadpil = $this->db->where("pbsta",'Y')->get("no_ctmofni")->result();
        return $jadpil;

    }

    public function jadhit() {
            $jadpil = $this->db->where("pbsta",'Y')->get("no_ctmofni")->num_rows();
        return $jadpil;

    }


}