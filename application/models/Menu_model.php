<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    function get_menu(){
        $this->db->select('*');
        $this->db->from('newlook_menu');
        $this->db->where('parent_id', 0);
        $this->db->where('is_active', 1);

        $categories = $this->db->get()->result();
        $i=0;
        foreach($categories as $p_cat){
            $categories[$i]->sub = $this->get_sub_menu($p_cat->id);
            $i++;
        }
        return $categories;
    }

    function get_sub_menu($id){
        $this->db->select('*');
        $this->db->from('newlook_menu');
        $this->db->where('parent_id', $id);
        $this->db->where('is_active', 1);

        $categories = $this->db->get()->result();
        $i=0;
        foreach($categories as $p_cat){
            $categories[$i]->sub = $this->get_sub_menu($p_cat->id);
            $i++;
        }
        return $categories;
    }


    function get_menu_pta(){
        $this->db->select('*');
        $this->db->from('menu_pta');
        $this->db->where('parent_id', 0);
        $this->db->where('is_active', 1);

        $categories = $this->db->get()->result();
        $i=0;
        foreach($categories as $p_cat){
            $categories[$i]->sub = $this->get_sub_menu_pta($p_cat->id);
            $i++;
        }
        return $categories;
    }


    function get_menu2(){
        $this->db->select('*');
        $this->db->from('menu2');
        $this->db->where('parent_id', 0);
        $this->db->where('is_active', 1);

        $categories = $this->db->get()->result();
        $i=0;
        foreach($categories as $p_cat){
            $categories[$i]->sub = $this->get_sub_menu2($p_cat->id);
            $i++;
        }
        return $categories;
    }

        function get_menu3(){
        $this->db->select('*');
        $this->db->from('menu3');
        $this->db->where('parent_id', 0);
        $this->db->where('is_active', 1);

        $categories = $this->db->get()->result();
        $i=0;
        foreach($categories as $p_cat){
            $categories[$i]->sub = $this->get_sub_menu3($p_cat->id);
            $i++;
        }
        return $categories;
    }



    function get_sub_menu_pta($id){
        $this->db->select('*');
        $this->db->from('menu_pta');
        $this->db->where('parent_id', $id);
        $this->db->where('is_active', 1);

        $categories = $this->db->get()->result();
        $i=0;
        foreach($categories as $p_cat){
            $categories[$i]->sub = $this->get_sub_menu_pta($p_cat->id);
            $i++;
        }
        return $categories;
    }

        function get_sub_menu2($id){
        $this->db->select('*');
        $this->db->from('menu2');
        $this->db->where('parent_id', $id);
        $this->db->where('is_active', 1);

        $categories = $this->db->get()->result();
        $i=0;
        foreach($categories as $p_cat){
            $categories[$i]->sub = $this->get_sub_menu($p_cat->id);
            $i++;
        }
        return $categories;
    }
    
    function get_sub_menu3($id){
        $this->db->select('*');
        $this->db->from('menu3');
        $this->db->where('parent_id', $id);
        $this->db->where('is_active', 1);

        $categories = $this->db->get()->result();
        $i=0;
        foreach($categories as $p_cat){
            $categories[$i]->sub = $this->get_sub_menu($p_cat->id);
            $i++;
        }
        return $categories;
    }

}