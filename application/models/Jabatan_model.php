<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Jabatan_model extends CI_Model {

    public function ambil_semua() {
        $data = $this->db->order_by("nm_jab",'asc')->get("ts_natabaj")->result();
        return $data;
    }
    public function jsemua() {
        $data = $this->db->get("ts_natabaj")->result();
        return $data;
    }
    public function jpresales() {
        $this->db->select("ts_natabaj.*");
        $this->db->from("ts_natabaj");
        $this->db->where("nm_jab","Presales");
        $data = $this->db->get()->result();
        return $data;
    }

    public function jhrga() {
        $this->db->select("ts_natabaj.*");
        $this->db->from("ts_natabaj");
        $this->db->where("nm_jab","Staff GA");
        $data = $this->db->get()->result();
        return $data;
    }
    public function tambah() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);

        if(isset($post["nm_jab"])) {
            $num = $this->db->insert("ts_natabaj",$post);
            return $num;
        } else return false;
    }

    public function edit() {
        $post = $this->input->post();
        unset($post["btnSubmit"]);

        if(isset($post["nm_jab"])) {
            $this->db->where("id_jab",$post["id_jab"]);
            unset($post["id_jab"]);
            $num = $this->db->update("ts_natabaj",$post);
            return $num;
        } else return false;
    }

    public function ambil_berdasarkan_id($id) {
        $data = $this->db->where("id_jab",$id)->get("ts_natabaj")->row();
        return $data;
    }

    public function hirpil($param) {

        $hp = $this->db->where("id_jabatan", $param)->get("v_hirarki")->result();
        return $hp;
    }
    public function jabpil($param) {
        $jp = $this->db->where("id_jab", $param)->get("ts_natabaj")->row();
        return $jp;
    }
}