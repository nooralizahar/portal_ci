<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">

  <title>Portal PT. Jala Lintas Media</title>
  
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="Portal, Sistem Informasi Sales, SIS, Sistem Informasi HRGA, Sistem Informasi OTB, ODP, ODC, Aplikasi ODC, Aplikasi ODP, Jala Lintas Media ">
  <meta name="description" content="Portal - PT. Jala Lintas Media">


  <link rel="icon" href="<?php echo base_url("favicon.png"); ?>" type="image/png" sizes="14x5">
  <link rel='stylesheet prefetch' href='<?php echo base_url("/assets/css/reset.min.css");?>'>
  <link rel="stylesheet" href="<?php echo base_url("/assets/css/bootstrap.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("/assets/css/animate.min.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("/assets/css/font-awesome.min.css");?>">
  <link rel='stylesheet prefetch' href='<?php echo base_url("/assets/css/font-roboto.css");?>'>
  <link rel="stylesheet" href="<?php echo base_url("/assets/css/stybot.css");?>">

</head>

<body data-spy="scroll" data-offset="50" data-target=".navbar-collapse">
<section id="home">
<div class="haldul">
  <h1>PT. Jala Lintas Media</h1> 
 <!-- <img src="<?php // echo base_url("assets/images/headoffice.jpg");?>" width='20%' height='20%'>
   <i class='fa fa-paint-brush'></i> + <i class='fa fa-code'></i> -->
</div>

		<!-- start navigation -->
		<div class="navbar navbar-fixed-top navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#ininavbarnya">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="http://jlm.net.id" target="_blank" class="navbar-brand"><img src="<?php echo base_url("/assets/images/logo.png");?>" class="img-responsive" alt="logo"></a>
				</div>
				<div class="collapse navbar-collapse" id="ininavbarnya">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#home" class="smoothScroll">Beranda</a></li>
						<!--<li><a href="#about" class="smoothScroll">Profil</a></li>
						<li><a href="#layanan" class="smoothScroll">Layanan</a></li>-->
						<li><a href="#contact" class="smoothScroll">Kontak</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end navigation -->

<div class="module form-module">
    <div class="form">
        <h2>Buat Akun</h2>
        <form>
            <input type="text" placeholder="nama pengguna"/>
            <input type="password" placeholder="kata kunci"/>
            <input type="email" placeholder="email"/>
            <input type="tel" placeholder="no telepon"/>
            <button>Registrasi</button>
        </form>
    </div>
  <div class="form">
      <h2>Portal JLM</h2>
        <div class="panel panel-danger">
            <?php if(isset($_GET["err"])): ?>
            <div class="alert bg-danger" role="alert">
            <small>Nama Pengguna atau Kata Sandi salah.</small> <a href="#" onclick="goBack()" class="pull-right"><i class="fa fa-times fa-lg"></i></a>
            </div>
            <?php endif; ?>
        </div>
      <form action="" method="post">
        <input name="username" type="text" placeholder="Nama Pengguna"/>
        <input name="password" type="password" placeholder="Kata Sandi"/>
        <div class="pull-right"><a href="/lupa_sandi">Lupa Sandi?</a></div>
        <button name="btnSubmit">Masuk</button>
    </form>
  </div>
  <!-- <div class="cta"><a href="#"></a></div> -->

</div>


<div class="halki"><span> Dev. 19.12 </span></div>
<!--<div><small><?="<pre>".print_r($this->session->userdata())."</pre>";?></small></div>-->
<script type="text/javascript">
$(document).ready(function(){

   var userId=<%: Session["userId"] %>;
    alert(userId);   
})   
</script>
</section>		
<!--		<div id="about">  awal tentang -->
<!--			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 wow fadeInLeft" data-wow-delay="0.9s">
						<h3>Tata Naskah Elektronik</h3>
						<h4>Aman, Hemat, Cepat &amp; Terintegrasi</h4>
						<p>Aplikasi Tata Naskah Elektronik adalah sebuah aplikasi yang biasa digunakan dalam menggerakan sistem birokrasi. Kecepatan, ketepatan suatu sistem melalui aplikasi ini akan menentukan baik buruknya kualitas dari layanan birokrasi sebuah institusi.</p>
						<p>Untuk itu Aplikasi ini hadir sebagai solusi untuk mentransformasikan proses korespondensi yang sebelumnya berbasis pada perpindahan dokumen kertas, menjadi bentuk elektronik. Seluruh proses mulai dari penerimaan surat masuk hingga disposisi, maupun proses pembuatan naskah keluar dari mulai pembuatan konsep, penomoran, persetujuan, hingga distribusi kepada penerima, dilakukan secara elektronik.</p>
					</div>
					<div class="col-md-6 col-sm-6 wow fadeInRight" data-wow-delay="0.9s">
						<span class="text-top">Integrasi <small>100%</small></span>
							<div class="progress">
								<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
							</div>
						<span>Cepat<small>80%</small></span>
							<div class="progress">
								<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
							</div>
						<span>Hemat <small>70%</small></span>
							<div class="progress">
								<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;"></div>
							</div>
						<span>Aman<small>100%</small></span>
							<div class="progress">
								<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
							</div>
					</div>
				</div>
			</div>
		</div>
	 akhir tentang --> 



	<!--	<div id="layanan"> awal layanan 
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.1s">
								<i class="fa fa-envelope-o"></i>
							</div>
							<div class="media-body wow fadeIn">
								<h3 class="media-heading">Terintegrasi</h3>
								<p>Semua data berada dalam satu server, dan dapat diakses seuai kebutuhan dan kewenangan.  <a rel="nofollow" href="http://fortawesome.github.io/Font-Awesome/examples/" target="_parent">examples</a></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.3s">
								<i class="fa fa-paper-plane"></i>
							</div>
							<div class="media-body wow fadeIn">
								<h3 class="media-heading">Lebih Cepat</h3>
								<p>Proses persetujuan lebih cepat, dapat diakses dimana saja.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.9s">
								<i class="fa fa-money"></i>
							</div>
							<div class="media-body wow fadeIn" data-wow-delay="0.6s">
								<h3 class="media-heading">Lebih Hemat</h3>
								<p>Dengan menggunakan media elektronik dapat menghemat waktu dan penggunaan kertas.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.6s">
								<i class="fa fa-laptop"></i>
							</div>
							<div class="media-body wow fadeIn" data-wow-delay="0.3s">
								<h3 class="media-heading">User Interface (UI)</h3>
								<p>UI kami sajikan dengan tampilan yang sederhana dan mudah digunakan.</p>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.4s">
								<i class="fa fa-comments-o"></i>
							</div>
							<div class="media-body wow fadeIn" data-wow-delay="0.3s">
								<h3 class="media-heading">Support</h3>
								<p>Support kami selalu siap membantu anda.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.8s">
								<i class="fa fa-check-circle"></i>
							</div>
							<div class="media-body wow fadeIn" data-wow-delay="0.6s">
								<h3 class="media-heading">Aman</h3>
								<p>Selama digunakan sesuai prosedur dan aturan yang berlaku.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>  akhir layanan -->
		



		
		<div id="contact"> <!-- awal kontak -->
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-4 wow fadeInLeft" data-wow-delay="0.6s">
						<h3>PT. Jala Lintas Media</h3>
						<p>One Stop ICT Solutions</p>
						<ul class="social-icon">
							<li><a href="https://www.facebook.com/jlm.isp" class="fa fa-facebook"></a></li>
							<li><a href="https://twitter.com/jlmisp" class="fa fa-twitter"></a></li>
							<li><a href="#" class="fa fa-instagram"></a></li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
						<address>
							<h3>Kantor Pusat</h3>
							<p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;Jl. Mayor Oking No.89 KM 2, Cibinong-Bogor 16918, Indonesia</p>
							<p><i class="fa fa-phone"></i>&nbsp;&nbsp;+62 21 3973 9090 (Hunting)</p>
							<p><i class="fa fa-envelope-o"></i>&nbsp;&nbsp;marketing@jlm.net.id</p>
						</address>
					</div>
					<form action="#" method="post" class="col-md-6 col-sm-4" id="contact-form" role="form">
							<div class="col-md-6 col-sm-12 wow fadeIn" data-wow-delay="0.3s">
								<input name="name" type="text" class="form-control" id="name" placeholder="Name">
							</div>
							<div class="col-md-6 col-sm-12 wow fadeIn" data-wow-delay="0.3s">
								<input name="email" type="email" class="form-control" id="email" placeholder="Email">
							</div>
							<div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.9s">
								<textarea name="message" rows="5" class="form-control" id="message" placeholder="Message"></textarea>
							</div>
							<div class="col-md-offset-9 col-md-3 col-sm-6 wow fadeIn" data-wow-delay="0.3s">
								<input name="submit" type="submit" class="form-control" id="submit" value="Send">
							</div>
					</form>
				</div>
			</div>
		</div>  <!-- akhir kontak -->
		


  <script src='<?php echo base_url("/assets/js/jquery-2.1.3.min.js");?>'></script>
  <script src='<?php echo base_url("/assets/js/wow.min.js"); ?>'></script>
  <script src='<?php echo base_url("/assets/js/smoothscroll.js"); ?>'></script>
  <script src='<?php echo base_url("/assets/js/bootstrap.min.js"); ?>'></script>
 <!-- <script src='<?php // echo base_url("/assets/js/imagesloaded.min.js"); ?>'></script>		
  <script src='<?php // echo base_url("/assets/js/jquery.flexslider.js"); ?>'></script>
  <script src='<?php // echo base_url("/assets/js/custom.js"); ?>'></script>	-->	
<script>
function goBack() {
  window.history.back();
}
</script>

  
</body>
</html>
