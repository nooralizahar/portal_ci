
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <?php if($this->session->userdata('id_jabatan')==1): ?>
            <li><a href="<?php echo base_url("panel/pengguna"); ?>"> Pengguna</a></li>
            <?php endif; ?>
            <li class="active">Edit Profil </li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal mengedit pengguna! Coba lagi nanti <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data pengguna berhasil diedit di sistem! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Edit Profil</div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $pengguna->id_pengguna; ?>" name="id_pengguna">
                        <div class="form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" class="form-control" value="<?php echo $pengguna->nama_lengkap ?>" required/>
                        </div>

                        <div class="form-group">
                            <label for="">Password (<label for="bPassword">Ubah</label> <input type="checkbox" id="bPassword"/>)</label>
                            <input type="text" id="iPassword" name="password" class="form-control" disabled="true" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Nomor Induk Karyawan</label>
                            <input type="text" name="nip" class="form-control" value="<?php echo $pengguna->nip ?>" required/>
                        </div>
 
                        <div class="form-group">
                            <label for="">No. HP</label>
                            <input type="text" name="notifikasi" class="form-control" value="<?php echo $pengguna->notifikasi ?>" required/>
                        </div>  


                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                        <!--<input type="submit" name="btnSubmit" class="fa btn btn-success" value="&#xf0c7; Simpan"> -->
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script>
    $(document).ready(function(){
        $("#msg").froalaEditor({
            height: 300
        });
        $("#penerima").select2();
        $("#jabatan").select2({
            placeholder: "Pilih Jabatan",
            allowClear: true
        });
        $("#divisi").select2({
            placeholder: "Pilih Divisi",
            allowClear: true
        });

        $("#attachep").fileinput({'showUpload':false, 'previewFileType':'any'});

        $("#bPassword").on("change",function(){
            if(this.checked){
                $("#iPassword").prop("disabled",false).val("");
            } else {
                $("#iPassword").prop("disabled",true).val("");
            }
        })

        $(".attach-imgep").magnificPopup({
            type: "image"
        });

        $(function(){
            $('.attach-docep').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                return false;
            });
        })

    })
</script>
