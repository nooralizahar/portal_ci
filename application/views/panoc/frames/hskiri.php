<body>


    <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand"><img src="<?= base_url('third_party'); ?>/images/logo_1_noc.svg" alt="Logo"></a>
                <a class="navbar-brand hidden"><img src="<?= base_url('third_party'); ?>/images/logo-mini.png" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <!-- QUERY MENU-->

                    <?php
                    $role_id = $this->session->userdata('role_id');
                    $queryMenu = "SELECT `user_menu`.`id`, `menu`
                            FROM `user_menu` JOIN `user_access_menu` 
                            ON `user_menu`.`id` = `user_access_menu`.`menu_id`
                        WHERE `user_access_menu`.`role_id` = $role_id
                    ORDER BY `user_access_menu`.`menu_id` ASC
            ";
                    $menu = $this->db->query($queryMenu)->result_array();

                    ?>
                    <?php foreach ($menu as $m) : ?>
                        <h3 class="menu-title">
                            <?= $m['menu']; ?>
                        </h3><!-- /.menu-title -->
                        <?php
                        $menuId = $m['id'];
                        $querySubMenu = "SELECT *
                                    FROM `user_sub_menu` JOIN `user_menu` 
                                    ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
                                WHERE `user_sub_menu`.`menu_id` = $menuId
                                AND `user_sub_menu`.`is_active` = 1
                                
                                ";
                        $subMenu = $this->db->query($querySubMenu)->result_array();
                        ?>
                        <?php foreach ($subMenu as $sm) : ?>
                            <?php if ($judul_konten == $sm['judul']) : ?>
                                <li class="menu-item active">
                                <?php else : ?>
                                <li class="menu-item">
                                <?php endif; ?>
                                <a href="<?= base_url($sm['url']); ?>"> <i class="menu-icon <?= $sm['icon']; ?>"></i><?= $sm['judul']; ?></a>
                                </li>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->