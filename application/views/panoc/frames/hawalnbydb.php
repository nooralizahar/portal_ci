<?php
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
$blmbaca = $this->db->where("dibaca",0)->where("ke_user",$this->session->userdata("id_pengguna"))->get("ts_isuksidrel")->num_rows();
$blmbaca2 = $this->db->where("dibaca",0)->where("id_pengguna !=",$this->session->userdata("id_pengguna"))->where("id_disx",$this->uri->segment(3))->get("ts_isuksidres")->num_rows();
$allblmbaca=$blmbaca2+$blmbaca;

$divi= $this->db->where("id_div",$this->session->userdata("id_div"))->get("ts_isivid")->result();

foreach($divi as $dvs): 
    $nmdv=$dvs->nm_div; 
    $iddv=$dvs->id_div;
endforeach;

$idku=$this->session->userdata("id_pengguna");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (isset($judul)) ? $judul : ""; ?></title>

    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-table.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/datepicker3.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-timepicker.min.css");?> type="text/css" />
    <link href="<?php echo base_url("assets/css/styles.css");?>" rel="stylesheet">

    <link rel="icon" href="<?php echo base_url("favicon.png"); ?>" type="image/png" sizes="14x5">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/css/fileinput.min.css">

    <!-- Include Editor style. -->
    <!--<link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_style.min.css' rel='stylesheet' type='text/css' />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <!--Icons-->
    <script src="<?php echo base_url("assets/js/lumino.glyphs.js");?>"></script>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url("assets/js/html5shiv.js");?>"></script>
    <script src="<?php echo base_url("assets/js/respond.min.js");?>"></script>
    <![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/js/fileinput.min.js"></script>


    <link rel="stylesheet" href="<?php echo base_url("assets/css/magnific-popup.css"); ?>">
    <script src="<?php echo base_url("assets/js/bootstrap-datetimepicker.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.magnific-popup.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jQuery.print.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/bootbox.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.shorten.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
    <!-- Include JS file. -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/js/froala_editor.min.js'></script>

    <script>
        BASE_URL = "<?php echo base_url(); ?>";
	    $.FroalaEditor.DEFAULTS.key = "bvA-21sD-16A-13ojmweC8ahD6f1n==";
    </script>

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>JLM</span>PORTAL</a>

            <ul class="user-menu">

                <li class="dropdown pull-right">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <?php echo $this->session->userdata("nama_lengkap"); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profil</a></li>
                        <li><a href="#" id="pengaturan"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Ganti Sandi</a></li>
                        <li><a href="<?php echo base_url("panel/logout/"); ?>"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <!-- QUERY MENU-->

                    <?php
                    $role_id = $this->session->userdata('role_id');
                    $queryMenu = "SELECT `user_menu`.`id`, `menu`
                            FROM `user_menu` JOIN `user_access_menu` 
                            ON `user_menu`.`id` = `user_access_menu`.`menu_id`
                        WHERE `user_access_menu`.`role_id` = $role_id
                    ORDER BY `user_access_menu`.`menu_id` ASC
            ";
                    $menu = $this->db->query($queryMenu)->result_array();

                    ?>
                    <?php foreach ($menu as $m) : ?>
                        <h3 class="menu-title">
                            <?= $m['menu']; ?>
                        </h3><!-- /.menu-title -->
                        <?php
                        $menuId = $m['id'];
                        $querySubMenu = "SELECT *
                                    FROM `user_sub_menu` JOIN `user_menu` 
                                    ON `user_sub_menu`.`menu_id` = `user_menu`.`id`
                                WHERE `user_sub_menu`.`menu_id` = $menuId
                                AND `user_sub_menu`.`is_active` = 1
                                
                                ";
                        $subMenu = $this->db->query($querySubMenu)->result_array();
                        ?>
                        <?php foreach ($subMenu as $sm) : ?>
                            <?php if ($judul_konten == $sm['judul']) : ?>
                                <li class="menu-item active">
                                <?php else : ?>
                                <li class="menu-item">
                                <?php endif; ?>
                                <a href="<?= base_url($sm['url']); ?>"> <i class="menu-icon <?= $sm['icon']; ?>"></i><?= $sm['judul']; ?></a>
                                </li>
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                </ul>
            </div>

</div><!--/.sidebar-->



