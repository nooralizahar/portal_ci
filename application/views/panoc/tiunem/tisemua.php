
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
        <li class="active"> Tiket</li>
    </ol>



 


</div><!--/.row--><br />

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["hsucc"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["herr"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
            <div class="panel-heading"> <?php echo $lbr; ?></div>
            <div class="panel-body">
<div class="row">
   <div class="col-xs-12 col-md-6 col-lg-3"> 
                <a href="<?php echo base_url("panoc/tikbaru"); ?>" class="btn btn-success"><i class="fa fa-plus-circle fa-lg"></i> <br><small><sup>BARU</sup></small></a>
   </div>
   <p><h1><br></h1></p>
</div>

<?php 
if($lbr=="Tiket Tertunda"){
    $this->load->view("panoc/tiunem/jmltinda");

}else if ($lbr=="Tiket Selesai"){
    $this->load->view("panoc/tiunem/jmltisai");
}else{
    $this->load->view("panoc/tiunem/jmltimua");
}




?>



                
<!-- Awal Aplikasi Tiket NOC -->
               <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-page-size="25" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true">#</th>
                        <th data-field="b"  data-sortable="true">Request ID</th>
                        <th data-field="c"  data-sortable="true">Status</th>
                        <th data-field="d"  data-sortable="true">Subject</th>
                        <th data-field="e"  data-sortable="true">Requester</th>
                        <th data-field="f"  data-sortable="true">Request Type</th>
                        <th data-field="g"  data-sortable="true">Vendor</th>
                        <th data-field="h"  data-sortable="true">Problem Side</th>
                        <th data-field="i"  data-sortable="true">Downtime</th>
                        <th data-field="j"  data-sortable="true">Uptime</th>
                        <th data-field="k"  data-sortable="true">Duration</th>
                        <th data-field="l"  data-sortable="true">Created By</th>
                        <th data-field="m"  data-sortable="true">Closed By</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach($tiket as $t): ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $t->id; 
                            if($this->session->userdata('mn_akses')=='super'){?> &nbsp;&nbsp;<a href="<?php echo base_url("panoc/tihapus/").$t->id; ?>"><i class='fa fa-trash'></i></a><?php }else{}?></td>
                            <td><center>


                                <?php 
                                
                                if ($t->status_id!='Closed'){
                                    echo $t->status_id;
                                }else{ 
                                	if ($lbr=="Tiket Selesai"){
                                	?>
                                    <div class="form-group">
                                    <a href='#' data-toggle='tooltip' data-placement='right' title="<?php echo 'Resolution : '.$t->resolution; ?>"><?php echo $t->status_id; ?></a>
                                    </div>
                                <?php }else{
                                	echo $t->status_id;
                                }} ?>
                                

                                </center>
                            </td>
                            <td><a href="<?php echo base_url("panoc/tiupdat/").$t->id; ?>"><?php echo $t->subject; ?></a></td>
                            <td><?php echo $t->requester_id; ?></td>
                            <td><?php echo $t->request_type_id; ?></td>
                            <td><?php echo $t->vendor_id; ?></td>
                            <td><?php echo $t->problem_side_id; ?></td>
                            <td><?php echo $t->downtime_date; ?></td>
                            <td><?php echo $t->uptime_date; ?></td>
                            <td><?php echo $t->duration; ?></td>
                            <td><?php echo $t->created_id; ?></td>
                            <td><?php echo $t->closed_id; ?></td>
                        </tr>
                        <?php $i++; ?>
                        <?php endforeach; ?>
                    </tbody>
                </table>

<!-- Akhir Aplikasi Tiket NOC -->


            </div>
        </div>
    </div>
</div><!--/.row-->

</div><!--/.main-->
