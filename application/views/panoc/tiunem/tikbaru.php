<?php $sales=$this->session->userdata("username")?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panoc/tisemua/"); ?>"> Tiket </a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Buat Tiket Baru</div>
                <div class="panel-body">
				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data Berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <form action="" method="POST" enctype="multipart/form-data">
                      
                        <div class="col-md-12">
                            <label>Judul </label>
                            <input type="text" name="subject" placeholder="Ketikan Judul Tiket" class="form-control" required/><br />
                        </div>
                        
                     
                        <div class="col-md-4">
                            <label>Requester <b style="color: red">*</b></label>
                            <select name="requester_id" class="form-control req">
                                <option value=""> -- Tidak Ada Pilihan --</option>
                                <?php foreach($dreqter as $rt):?>
                                    
                                    <option value="<?php echo $rt->id; ?>"> <?php echo $rt->nama_requester;?></option>
                                <?php endforeach;?>
                            </select><br />
                       </div>
                        <div class="col-md-4">
                            <label>Status <b style="color: red">*</b></label>
                            <select name="status_id" class="form-control sta">
                                <?php foreach($dstatus as $s):?>
                                    <option value="<?php echo $s->id; ?>"> <?php echo $s->nama_status;?></option>
                                <?php endforeach;?>
                            </select><br />
                        </div> 
                        <div class="col-md-4">
                            <label>Request Type <b style="color: red">*</b></label>
                            <select name="request_type_id" class="form-control rty">
                                <?php foreach($dreqtyp as $t):?>
                                    <option value="<?php echo $t->id; ?>"> <?php echo $t->nama_request_type;?></option>
                                <?php endforeach;?>
                            </select><br />
                        </div> 
                      
                       
                        <div class="col-md-4">
                            <label>Vendor <b style="color: red">*</b></label>
                            <select name="vendor_id" class="form-control ven">
                                <?php foreach($dvendor as $v):?>
                                    <option value="<?php echo $v->id; ?>"> <?php echo $v->nama_vendor;?></option>
                                <?php endforeach;?>
                            </select><br />
                        </div> 
                        <div class="col-md-4">
                            <label>Problem Side </label>
                            <select name="problem_side_id" class="form-control psi">
                                <option value="0"> -- Tidak ada Pilihan --</option>
                                <?php foreach($dprosid as $p):?>
                                    <option value="<?php echo $p->id; ?>"> <?php echo $p->nama_problem_side;?></option>
                                <?php endforeach;?>
                            </select><br />
                        </div> 
                        <div class="col-md-4">
                            <label>Link Category </label>
                            <select name="link_category_id" class="form-control lca">
                                <option value="0"> -- Tidak ada Pilihan --</option>
                                <?php foreach($dlincat as $l):?>
                                    <option value="<?php echo $l->id; ?>"> <?php echo $l->nama_link_category;?></option>
                                <?php endforeach;?>
                            </select><br />
                        </div>
                        
                        
                        <div class="col-md-4">
                            <label>Cause Group </label>
                            <select name="cause_group_id" class="form-control cgr">
                                <option value="0"> -- Tidak ada Pilihan --</option>
                                <?php foreach($dcaugro as $c):?>
                                    <option value="<?php echo $c->id; ?>"> <?php echo $c->nama_cause_group;?></option>
                                <?php endforeach;?>
                            </select><br />
                        </div> 
                        <div class="col-md-4">
                            <label>Root Cause </label>
                            <select name="root_cause_id" class="form-control rca">
                                <option value="0"> -- Tidak ada Pilihan --</option>
                                <?php foreach($drotcau as $o):?>
                                    <option value="<?php echo $l->id; ?>"> <?php echo $o->nama_root_cause;?></option>
                                <?php endforeach;?>
                            </select><br />
                        </div>
                        <div class="col-md-6">
                            <label for="datetimepicker">Downtime </label>
                            <input type="text" id="downtime_date" name="downtime_date" class="form-control tanggal" value="<?php date_default_timezone_set('Asia/Jakarta');echo date('Y-m-d H:i'); ?>"><br />
                        </div>  
                        <div class="col-md-6">
                            <label for="datetimepicker1">Uptime </label>
                            <input type="text" id="datetime" name="uptime_date" class="form-control" ><br />
                        </div>                    
                        <center>
                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('ket',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>


    $(document).ready(function(){


        $(".req").select2();
        $(".sta").select2();
        $(".rty").select2();
        $(".ven").select2();
        $(".psi").select2();
        $(".lca").select2();
        $(".cgr").select2();
        $(".rca").select2();


        $(".tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>