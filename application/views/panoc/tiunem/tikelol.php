
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
        <li class="active">Kelola Item Tiket</li>
    </ol>
</div><!--/.row--><br />

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <?php if(isset($_GET["succ"]) && $_GET["succ"] == 1): ?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Pengguna berhasil diblokir! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
            <?php elseif(isset($_GET["succ"]) && $_GET["succ"] == 2): ?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Pengguna berhasil dibuka! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
            <?php endif;?>
            <div class="panel-heading">Daftar Item </div>
            <div class="panel-body">
                <a href="<?php echo base_url("panel/itemtamb"); ?>" class="btn btn-success"><i class="fa fa-plus-circle fa-lg"></i> <br><small><sup>BARU</sup></small></a>

            </div>
        </div>
    </div>
</div><!--/.row-->

</div><!--/.main-->
