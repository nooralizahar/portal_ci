<?php $sales=$this->session->userdata("username")?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panoc/tisemua/"); ?>"> Tiket </a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Ubah Tiket</div>
                <div class="panel-body">
				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data Berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <form action="" method="POST" enctype="multipart/form-data">
                      <?php foreach($dtiket as $ti):?>
                        <div class="col-md-12">
                            <h2><?php echo "#".$ti->id; ?></h2>
                            <h3><?php echo $ti->subject; ?></h3><hr />
                        </div>
                        <table class="table table-striped table-responsive" data-toggle="table">
                            <tr><td></td><td></td><td></td></tr>
                            <tr><td>Requester</td><td>&nbsp;:&nbsp;</td><td><?php echo $ti->requester_id;?></td></tr>
                            <tr><td>Status</td><td>&nbsp;:&nbsp;</td><td><?php echo $ti->status_id;?></td></tr>
                            <tr><td>Request Type</td><td>&nbsp;:&nbsp;</td><td><?php echo $ti->request_type_id;?></td></tr>
                            <tr><td>Vendor</td><td>&nbsp;:&nbsp;</td><td><?php echo $ti->vendor_id;?></td></tr>
                        </table>

                        <div class="col-md-4">
                            <label>Problem Side </label>
                            <select name="problem_side_id" class="form-control psi" required>
                                <option value="0"> -- Tidak ada Pilihan --</option>
                                <?php foreach($dprosid as $p):?>
                                    <option value="<?php echo $p->id; ?>" <?php echo ($ti->problem_side_id ==  $p->id) ? "selected" : ""; ?>><?php echo $p->nama_problem_side; ?></option>
                                <?php endforeach;?>
                            </select><br />
                        </div> 
                        <div class="col-md-4">
                            <label>Link Category </label>
                            <select name="link_category_id" class="form-control lca" required>
                                <option value="0"> -- Tidak ada Pilihan --</option>
                                <?php foreach($dlincat as $l):?>
                                    <option value="<?php echo $l->id; ?>" <?php echo ($ti->link_category_id ==  $l->id) ? "selected" : ""; ?>><?php echo $l->nama_link_category; ?></option>
                                    
                                <?php endforeach;?>
                            </select><br />
                        </div>

                        
                        <div class="col-md-4">
                            <label>Cause Group </label>
                            <select name="cause_group_id" class="form-control cgr" required>
                                <option value="0"> -- Tidak ada Pilihan --</option>
                                <?php foreach($dcaugro as $c):?>
                                    <option value="<?php echo $c->id; ?>" <?php echo ($ti->cause_group_id ==  $c->id) ? "selected" : ""; ?>><?php echo $c->nama_cause_group; ?></option>
                                    
                                <?php endforeach;?>
                            </select><br />
                        </div> 
                        <div class="col-md-4">
                            <label>Root Cause </label>
                            <select name="root_cause_id" class="form-control rca" required>
                                <option value="0"> -- Tidak ada Pilihan --</option>
                                <?php foreach($drotcau as $o):?>
                                    <option value="<?php echo $o->id; ?>" <?php echo ($ti->root_cause_id ==  $o->id) ? "selected" : ""; ?>><?php echo $o->nama_root_cause; ?></option>
                                    
                                <?php endforeach;?>
                            </select><br />
                        </div>
                        <div class="col-md-4">
                            <label>Created By </label>
                            <select name="created_by" class="form-control rca" disabled="disabled">
                                <?php foreach($dcreaby as $e):?>
                                    <option value="<?php echo $e->id; ?>" <?php echo ($ti->created_id ==  $e->id) ? "selected" : ""; ?>><?php echo $e->nama_created_by; ?></option>
                                    
                                <?php endforeach;?>
                            </select><br />
                        </div>
                        <div class="col-md-4">
                            <label for="datetimepicker">Downtime </label>
                            <input type="text" id="downtime_date" name="downtime_date" class="form-control tanggal" value="<?php echo $ti->downtime_date; ?>" readonly/><br />
                        </div>  
                        <div class="col-md-4">
                            <label for="datetimepicker1">Uptime </label>
                            <input type="text" id="datetime" name="uptime_date" class="form-control" ><br />
                        </div>
                        <div class="col-md-4">
                            <label>Closed By </label>
                            <select name="closed_by" class="form-control rca" readonly>
                                <option value="0"> -- Tidak ada Pilihan --</option>
                                <?php foreach($dclosby as $y):?>
                                    <option value="<?php echo $y->id; ?>" <?php echo ($ti->closed_id ==  $y->id) ? "selected" : ""; ?>><?php echo $y->nama_closed_by; ?></option>
                                    
                                <?php endforeach;?>
                            </select><br />
                        </div>

                        <?php endforeach;?>  
                        <div class="col-md-12">                  
                        <center>
                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('ket',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>


    $(document).ready(function(){


        $(".req").select2();
        $(".sta").select2();
        $(".rty").select2();
        $(".ven").select2();
        $(".psi").select2();
        $(".lca").select2();
        $(".cgr").select2();
        $(".rca").select2();


        $(".tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>