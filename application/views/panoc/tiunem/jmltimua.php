<div class="row">
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked folder"><use xlink:href="#stroked-folder"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->where("status_id",5)->get("noc_tiket_uta")->num_rows()); ?></a>
                        </div>
                        <div class="text-muted"> Closed</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked open folder"><use xlink:href="#stroked-open-folder"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->where("status_id",2)->get("noc_tiket_uta")->num_rows()); ?></a>
                        </div>
                        <div class="text-muted"> Open</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked round coffee mug"><use xlink:href="#stroked-round-coffee-mug"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->where("status_id",3)->get("noc_tiket_uta")->num_rows()); ?></a>
                        </div>
                        <div class="text-muted"> On Hold</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked eye"><use xlink:href="#stroked-eye"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->where("status_id",4)->get("noc_tiket_uta")->num_rows()); ?></a>
                        </div>
                        <div class="text-muted"> On Monitoring</div>
                    </div>
                </div>
            </div>
        </div>
</div>