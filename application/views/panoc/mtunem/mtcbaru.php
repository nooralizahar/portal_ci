<?php $sales=$this->session->userdata("username")?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panoc/mtcsemu/"); ?>">Maintenance</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> BARU</div>
                <div class="panel-body">
				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data Berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
<form method=POST enctype='multipart/form-data' action=''>
                
                <div class='form-group row'>
                    <label for='tgmtc1' class='col-sm-2 form-control-label'>Tanggal</label>
                    <div class='col-sm-3'>
                      <input type='date' name='tgmtc' class='form-control' id='tgmtc1' required>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='jmmtc1' class='col-sm-2 form-control-label'>Waktu (Durasi)</label>
                    <div class='col-sm-2'>
                      <input type='text' name='jmmtc' class='form-control' id='jmmtc'required>
                    </div>
                    <label for='jamul1' class='col-sm-1 form-control-label'>s/d</label>
                    <div class='col-sm-2'>
                      <input type='text' name='jsmtc' class='form-control' id='jsmtc' onkeyup="hitmenit()" required>
                    </div>
                </div> 

                <div class='form-group row'>
                    <label for='dntime1' class='col-sm-2 form-control-label'>Lama Downtime </label>
                    <div class='col-sm-3'>
                      <input type='text' name='dntime' class='form-control' id='dntime' required>
                    </div>
                    <label for='dntime2' class='col-sm-7 form-control-label'>menit</label>
                </div>
                
                <div class='form-group row'>
                    <label for='demtc1' class='col-sm-2 form-control-label'>Deskripsi</label>
                    <div class='col-sm-10'>
                      <input type='text' name='demtc' class='form-control' id='demtc1' required>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='immtc1' class='col-sm-2 form-control-label'>Impact</label>
                    <div class='col-sm-10'>
                    <textarea name='immtc' class='ckeditor form-control' id='immtc1' rows='10' required></textarea>
                      
                    </div>
        
                </div>

                <div class='form-group row'>
                    <label for='nmven' class='col-sm-2 form-control-label'>Vendor</label>
                    <div class='col-sm-10'>
                      <input type='text' name='nmven' class='form-control' id='nmven1'required>
                    </div>  
        
                    
                </div>  
 
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                      <button type='button' onclick="goBack()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>

                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('immtc',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>
    function goBack() {
  		window.history.back();
	}

	function hitmenit() {

    var wak1 = document.getElementById("jmmtc").value;
    var wak2 = document.getElementById("jsmtc").value;
    var wa = wak1.split(":");
    var wb = wak2.split(":");
    var t1= new Date(2020,10,21,wa[0],wa[1]);
 	var t2= new Date(2020,10,21,wb[0],wb[1]);
 	var di=t2.getTime() - t1.getTime();
 	var menit = Math.floor(di / 1000 / 60);

   
        document.getElementById("dntime").value=menit;
	}

    $(document).ready(function(){


        $(".req").select2();


        $(".tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>