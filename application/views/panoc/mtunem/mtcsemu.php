<?php 
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panoc/mtcsemu/"); ?>">Maintenance</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Semua Data</div>
                <div class="panel-body">
				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data Berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

<!-- isi awal -->

       <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tgmtc"  data-sortable="true">Tanggal</th>
                        <th data-field="waktu"  data-sortable="true">Waktu</th>
                        <th data-field="durasi"  data-sortable="true">Durasi</th>
                        <th data-field="deskri"  data-sortable="true">Deskripsi</th> 
                        <th data-field="impact"  data-sortable="true">Impact</th>  
                        <th data-field="vendor"  data-sortable="true">Vendor</th>       
                        <th>Publish</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($d_mtain as $dpro): ?>
                        <tr>
                            <td><?php echo $dpro->tgmtc; ?></td>
                            <td><?php echo "<center>".$dpro->jmmtc." s/d ".$dpro->jsmtc."</center>"; ?></td>
                            <td><?php echo "<center>".$dpro->dntime."</center>"; ?></td>
                            <td><?php echo $dpro->demtc; ?></td>
                            <td><?php echo substr(strip_tags($dpro->immtc),0,50)."<b>...</b>"; ?></td>
                            <td><?php echo $dpro->nmven; ?></td>
                            
                           
                            <td>
                              <?php if($dpro->pbsta=='N'){$pub="selesai";}else{$pub="ya";} 
                              echo"<center>"; if ( $nmjab=='admin'){echo"<a href=".base_url("panoc/mtcubah/")."$dpro->idmtc><i class='fa fa-pencil-square-o fa-1x'></i></a>&nbsp;&nbsp;<a href=$dpro->idmtc><i class='fa fa-trash-o fa-1x'></i></a>&nbsp;&nbsp;";if($pub=='selesai') {echo"";}else{echo"<a href=$dpro->idmtc><i class='fa fa-envelope fa-1x'></i></a>";}}else{ if($pub=='selesai') {echo"<a href=#><i class='fa fa-info-circle fa-1x'></i></a>";}else{echo"<a href=".base_url("panoc/mtcubah/")."$dpro->idmtc><i class='fa fa-pencil-square-o fa-1x'></i></a>&nbsp;&nbsp;<a href=$dpro->idmtc><i class='fa fa-envelope fa-1x'></i></a>";}} 

                              ?>
                              <small><br><?php echo $pub; ?></small></center>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>



<!-- isi akhir -->

                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('immtc',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>


    $(document).ready(function(){


        $(".req").select2();


        $(".tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>