
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panoc/womonito/"); ?>"> Monitor WO</a></li>
            <li class="active"> Lihat Detil WO</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="color:#010288"><b>Detil Pelaksanaan No. WO : <?php echo $idno; ?></b></h4>
                </div>
        <div class="panel-body">

            <?php $this->load->view("/panel/ompunem/datrev")?>

            <br>

            <?php $this->load->view("/panel/ompunem/datpro")?>

            <br>

            <?php $this->load->view("/panel/ompunem/dattek")?>

            <br>

            <?php $this->load->view("/panel/ompunem/datbaa")?>

        </div>
      </div>
    </div>

    </div><!--/.row end-->





