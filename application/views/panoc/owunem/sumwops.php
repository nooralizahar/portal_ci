

            <div class="form-group"> 
                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="15"><center><h3>TAHUN <?= date('Y') ?></h3></center></th></tr>
                            <tr>

                                <th>&nbsp;No.</th>
                                <th>&nbsp;NAMA</th>
                                <th><center>JAN</center></th>
                                <th><center>FEB</center></th>
                                <th><center>MAR</center></th>
                                <th><center>APR</center></th>
                                <th><center>MEI</center></th>
                                <th><center>JUN</center></th>
                                <th><center>JUL</center></th>
                                <th><center>AGU</center></th>
                                <th><center>SEP</center></th>
                                <th><center>OKT</center></th>
                                <th><center>NOP</center></th>
                                <th><center>DES</center></th>
                                <th><center>TOTAL</center></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php 
                            $daf=$wosal;
                            $no=1;
                            foreach($daf as $h): ?>
                                <tr >
                                    <td align="right"><?php echo $no++; ?> </td>
                                    <td><?php echo $h->nama; ?> </td>
                                    <td align="center"><a href="<?php echo base_url("panel/wodetps/".$h->nama."/".date('Y')."-01"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->nama.",&#013; Detil bulan : Januari ".date('Y'); ?>'><?php echo $h->JAN; ?></a> </td>
                                    <td align="center"><?php echo $h->FEB; ?> </td>
                                    <td align="center"><?php echo $h->MAR; ?> </td>
                                    <td align="center"><?php echo $h->APR; ?> </td>
                                    <td align="center"><?php echo $h->MEI; ?> </td>
                                    <td align="center"><?php echo $h->JUN; ?> </td>
                                    <td align="center"><?php echo $h->JUL; ?> </td>
                                    <td align="center"><?php echo $h->AGU; ?> </td>
                                    <td align="center"><?php echo $h->SEP; ?> </td>
                                    <td align="center"><?php echo $h->OKT; ?> </td>
                                    <td align="center"><?php echo $h->NOP; ?> </td>
                                    <td align="center"><?php echo $h->DES; ?> </td>
                                    <td align="center"><?php echo $h->TOTAL; ?> </td>

                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>

            </div>
