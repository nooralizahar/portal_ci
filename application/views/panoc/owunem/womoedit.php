<?php 
$cek=$this->db->where('id_worde',$idno)->get("v_pmo_site")->num_rows();
    if($cek==0){
     $sit='';
    }else{
        
         $sit='1';   
   
    }

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/womonito/"); ?>"> Monitor WO</a></li>
            <li class="active"> Progres WO</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="womobaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah progress.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Progres baru berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsuc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])):?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data tidak bisa dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h3 style="color:#010288"><b> No. WO : <?php echo $idno.' '.$sit; ?></b></h3>
                </div>
        <div class="panel-body">
            
 <?php $this->load->view("/panel/ompunem/datrev")?><br>
        <form class='add-item' method=POST action='/panel/womoedit' onsubmit="FormSubmit(this);">
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Tanggal</label>
                <div class='col-sm-10'>
                    <input type='date' name='tg_pro' class='form-control' id='tg_pro' required>
                    <input type='hidden' name='id_worde' class='form-control' id='id_worde' value="<?php echo $idno; ?>">
                </div>
          </div> 
         <div class='form-group row'>
                <label for='lblsite' class='col-sm-2 form-control-label'>Site <small style="color:red"><b> *</b></small></label>
                <div class='col-sm-4'>
                                <input type="hidden" name="site" class="form-control" />
                                <select name="site_ddl" id="site_ddl" onchange="DropDownChanged(this);" class="form-control" />
                                    <option value=""></option>
                                    <?php foreach($datsit as $s):?>
                                    <option value="<?php echo $s->site; ?>"><?php echo $s->site; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Lainnya..</option>
                                </select> <input type="text" name="site_txt" style="display: none;" class="form-control" placeholder="Ketik lainnya disini" />
                </div>
                <div class='col-sm-3'>
                <small style="color: red">* : kosongkan jika bukan multiple site</small></div>
          </div> 
         <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Uraian</label>
                <div class='col-sm-10'>
                    <input type='text' name='uraian' class='form-control' id='uraian' placeholder="Ketik uraian disini" required>
                </div>
          </div> 
           
                  
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/womonito') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>



    <br>



        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tgpro"  data-sortable="true">Tanggal</th>
                        <?php if($sit!=''):?>
                        <th data-field="site"  data-sortable="true">Site</th>
                        <?php endif; ?>
                        <th data-field="urpro"  data-sortable="true">Uraian Progres</th>

                                
                        <th>Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($d_wopro as $t): ?>
                        <tr>
                            <td><?php echo tgl_indome($t->tg_pro); ?></td>
                            <?php if($sit!=''):?>
                            <td><?php echo $t->site; ?></td>
                            <?php endif; ?>
                            <td><?php echo $t->uraian; ?></td>
                            
                            
                           
                            <td><center>
                              <!-- <a href="<?php echo base_url("panel/odcedit/".$datwo->id_worder); ?>" ><i class="fa fa-pencil fa-lg"></i></a>--> &nbsp;  
                                <a href="<?php echo base_url("panel/prohap/".$t->id_worde."/".$t->id_wopro); ?>" ><i class="fa fa-trash fa-lg" data-toggle='tooltip' title='Hapus'></i></a></center>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


<br>

<?php $this->load->view("/panel/ompunem/dattek")?>

        </div>
      </div>
    </div>

    </div><!--/.row end-->


<script type="text/javascript">

function DropDownChanged(oDDL) {
    var oTextbox = oDDL.form.elements["site_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}

function FormSubmit(oForm) {
    var oHidden = oForm.elements["site"];
    var oDDL = oForm.elements["site_ddl"];
    var oTextbox = oForm.elements["site_txt"];
    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;
}

</script>



    


