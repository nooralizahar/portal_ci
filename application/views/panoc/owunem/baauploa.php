<?php 


?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/womonito/"); ?>"> Monitor WO</a></li>
            <li class="active"> Topology WO</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="baabaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data baru berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsuc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])):?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data tidak bisa dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h3 style="color:#010288"><b> No. WO : <?php echo $idno; ?></b></h3>
                </div>
        <div class="panel-body">

                <div class="form-group col-sm-12"> 
                <div class="panel panel-success">
                    <div class="panel-heading">Upload Berita Acara Aktifasi</div>
                <div class="panel-body" style="max-height: 200px;overflow-y: scroll;">
                	<div class="col-sm-12"><center>
                		 <?php foreach($datlod as $d): 
                		 	if(!empty($d->filebaa)):
                        $fc=explode('.',$d->filebaa);
                		 	?>
                          <?php if($fc[1]=='pdf'): ?>
                          <iframe src="https://docs.google.com/gview?url=<?php echo base_url('assets/uploads/baa/'.$d->filebaa); ?>&embedded=true" frameborder="0" width="95%"></iframe>
                          <?php else: ?>
                    		 	<img src=<?php echo base_url('assets/uploads/baa/'.$d->filebaa); ?> ><br>
                    		 	<small><?= $d->filetopo ?><br></small>
                          <?php endif; ?>
                      <br><br>
                		 	<?php else: ?>
                		 	<img src=<?php echo base_url('assets/uploads/topo/netcom.png'); ?> ><br>
                		 	<small>default<br></small><br><br>
                		 	<?php endif; ?>

                		 <?php endforeach; ?>
                    </center></div>
                </div>

                <center><br><br>    <form action="" method="POST" enctype="multipart/form-data">
                                                                            
                        <div class="col-sm-12">
                           <input type="hidden" name='idbaa' class='form-control' id='no_wor1' value='<?= $idno ?>' readonly>
                           <!--<input type="file" id="image" name="image" class="form-control">--><input type="file" multiple id="attach" name="attach[]" class="form-control"> <small>format file : *.gif,*.jpg,*.png & *.pdf. <br></small><br />
                           <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>UNGGAH</sup></small></button>
                           <!-- <input type="submit" name="btnSubmit" class="fa fa-lg btn btn-success" value="&#xf1d9; Kirim"> -->

                        </div>
                    </form></center>
                </div>
                </div>



        </div>
      </div>
    </div>

    </div><!--/.row end-->


<script>
    $(document).ready(function(){

            $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});

           // $(".pilpic").select2();
    })
</script>


    

