<?php 
foreach($datlod as $d){
	$fiup=$d->filetopo;
}

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/womonito/"); ?>"> Monitor WO</a></li>
            <li class="active"> Topology WO</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="topobaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data baru berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsuc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])):?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data tidak bisa dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h3 style="color:#010288"><b> No. WO : <?php echo $idno; ?></b></h3>
                </div>
        <div class="panel-body">
<?php if(!empty($fiup)):?>
<?php else: ?>
              <?php $this->load->view("/panel/ompunem/datrev")?>

            <br>
        <div class="panel-heading" style="color:blue">Input Data Teknis</div>
        <form class='add-item' method=POST action='' onsubmit="FormSubmit(this);">
                
                <div class='form-group row'>
                    <label for='to_mit1' class='col-sm-2 form-control-label'>No. WO</label>
                    <div class='col-sm-4'>
                      <input type='text' name='id_worde' class='form-control' id='no_wor1' value='<?= $idno ?>' readonly>
                    </div>
                    
                    <label for='ca_not1' class='col-sm-1 form-control-label'>Circuit ID</label>
                    <div class='col-sm-5'>
                      <input type='text' name='id_cir' class='form-control' id='id_cir1'placeholder="Ketik CircuitID disini">
                    </div>
                </div>


                <div class='form-group row'>
                     <label for='idgam1' class='col-sm-2 form-control-label'>Perangkat</label>
                     <div class='col-sm-4'>
                     <select name='id_gam' class='form-control' id='idgam1'>";
                                <?php foreach($datgam as $l):?>
                                    <option value="<?php echo $l->id_gam; ?>"><?php echo $l->nm_gam; ?></option>
                                <?php endforeach;?>
                   </select></div>   
                    
                     <label for='ds_gam1' class='col-sm-1 form-control-label'>Deskripsi</label>
                     <div class='col-sm-5'>
                                <input type="hidden" name="ds_gam" class="form-control" />
                                <select name="ds_gam_ddl" id="ds_gam_ddl" onchange="DropDownChanged(this);" class="form-control" />

                                    <?php foreach($datdes as $dc):?>
                                    <option value="<?php echo $dc->ds_gam; ?>"><?php echo $dc->ds_gam; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Lainnya..</option>
                                </select> <input type="text" name="ds_gam_txt" style="display: none;" class="form-control" placeholder="Ketik lainnya disini" />
                      </div>    
                </div>
                
                
                <div class='form-group row'>
                    <label for='po_inp1' class='col-sm-2 form-control-label'>Port IN</label>
                    <div class='col-sm-4'>
                      <input type='text' name='po_inp' class='form-control' id='po_inp1' placeholder="Ketik Port Masuk disini">
                    </div>  
                    
                    <label for='po_out1' class='col-sm-2 form-control-label'>Port OUT</label>
                    <div class='col-sm-4'>
                      <input type='text' name='po_out' class='form-control' id='po_out1' placeholder="Ketik Port Keluar disini">
                    </div>                  
                </div>   

                <div class='form-group row'>
                    <label for='ip_add1' class='col-sm-2 form-control-label'>IP Address</label>
                    <div class='col-sm-10'>
                      <input type='text' name='ip_add' class='form-control' id='ip_add1' placeholder="Ketik IP Address disini">
                    </div>  
                </div>  
                
                <div class='form-group row'>
                    <label for='cf_vla1' class='col-sm-2 form-control-label'>VLAN</label>
                    <div class='col-sm-10'>
                      <input type='text' name='cf_vla' class='form-control' id='cf_vla1' placeholder="Ketik VLAN disini">
                    </div>  
                </div>
    
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/womonito') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>
<?php endif; ?>
    <br>
                <div class="form-group col-sm-12"> 
                <div class="panel panel-success">
                    <div class="panel-heading" style="color:blue">Upload Topology</div>
                <div class="panel-body" style="max-height: 200px;overflow-y: scroll;">
                	<div class="col-sm-12"><center>
                		 <?php foreach($datlod as $d): 
                		 	if(!empty($d->filetopo)):
                        $fc=explode('.',$d->filetopo);
                		 	?>
                          <?php if($fc[1]=='pdf'): ?>
                          <iframe src="https://docs.google.com/gview?url=<?php echo base_url('assets/uploads/topo/'.$d->filetopo); ?>&embedded=true" frameborder="0" width="95%"></iframe>
                          <?php else: ?>
                    		 	<img src=<?php echo base_url('assets/uploads/topo/'.$d->filetopo); ?> ><br>
                    		 	<small><?= $d->filetopo ?><br></small>
                          <?php endif; ?>
                      <br><br>
                		 	<?php else: ?>
                		 	<img src=<?php echo base_url('assets/uploads/topo/netcom.png'); ?> ><br>
                		 	<small>default<br></small><br><br>
                		 	<?php endif; ?>

                		 <?php endforeach; ?>
                    </center></div>
                </div>

                <center><br><br>    <form action="" method="POST" enctype="multipart/form-data">
                                                                            
                        <div class="col-sm-12">
                           <input type="hidden" name='id_worde' class='form-control' id='no_wor1' value='<?= $idno ?>' readonly>
                           <input type='hidden' name='id_cir' class='form-control' id='id_cir1' value='89' readonly>
                           <input type="file" id="image" name="image" class="form-control"> <small>format file : *.gif,*.jpg,*.png & *.pdf. <br></small><br />
                           <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>UNGGAH</sup></small></button>
                           <!-- <input type="submit" name="btnSubmit" class="fa fa-lg btn btn-success" value="&#xf1d9; Kirim"> -->

                        </div>
                    </form></center>
                </div>
                </div>


        <table class='table table-bordered'> <!--class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" -->
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>CircuitID</th>
                      <th>Perangkat</th>
                      <th>Deskripsi</th>
                      <th>Port</th>
                      <th>IP Add</th>
                      <th>VLAN</th>
                      <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $no=1;
                         foreach($dattop as $t): 
                    if($no%2){$color="#aecbff";}else{$color="#deeaff";}
                            ?>
                       
                     <tr style="bgcolor: <?= $color ?>" >
                     <td><?= $no ?></td>

                     <td><?= $t->id_cir ?></td>
                     <td><?= $t->id_gam ?></td>
                     <td><?= $t->ds_gam ?></td>
                     <td><?= $t->po_inp ?> | <?= $t->po_out ?></td>
                     <td><?= $t->ip_add ?></td>
                     <td><?= $t->cf_vla ?></td>

                            <td><center>
                              <a href="<?php echo base_url("panel/peruba/".$t->id_tpl); ?>" ><i class="fa fa-pencil fa-lg"></i></a> &nbsp;  
                                <a href="<?php echo base_url("panel/perhap/".$t->id_worde."/".$t->id_tpl); ?>" ><i class="fa fa-trash fa-lg"></i></a>
                            </center></td>
                        </tr>
                        <?php $no++; endforeach; ?>
                    </tbody>
            </table>

        </div>
      </div>
    </div>

    </div><!--/.row end-->

<script type="text/javascript">

function DropDownChanged(oDDL) {
    var oTextbox = oDDL.form.elements["ds_gam_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}

function FormSubmit(oForm) {
    var oHidden = oForm.elements["ds_gam"];
    var oDDL = oForm.elements["ds_gam_ddl"];
    var oTextbox = oForm.elements["ds_gam_txt"];
    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;
}

</script>

<script>
 $("#image").fileinput({'showUpload':false, 'previewFileType':'any'});   
</script>


    

