
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url($this->uriku."/pengguna"); ?>">Kelola Pengguna</a></li>
            <li class="active">Tambah Pengguna</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah pengguna! Coba lagi nanti <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Pengguna berhasil didaftarkan di sistem! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Isi Pengguna Baru</div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" name="username" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Nomor Induk Karyawan</label>
                            <input type="text" name="nip" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Departemen</label>
                            <select name="id_div" id="pildivisi" class="form-control" required>
                                <option value=""></option>
                                <?php foreach($daftar_divisi as $divisi): ?>
                                    <option value="<?php echo $divisi->id_div; ?>"><?php echo $divisi->nm_div; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Jabatan</label>
                            <select name="id_jabatan" id="piljabatan" class="form-control" required>
                                <option value=""></option>
                                <?php foreach($daftar_jabatan as $jabatan):?>
                                    <option value="<?php echo $jabatan->id_jab; ?>"><?php echo $jabatan->nm_jab; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Atasan</label>
                            <select name="id_spv" id="pilatasan" class="form-control ">
                                <option value=""></option>
                                <?php foreach($daftas as $atasan):?>
                                    <option value="<?php echo $atasan->id_pengguna; ?>"><?php echo $atasan->nama_lengkap." &rarr; ".$atasan->nm_jab; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Dept. Head</label>
                            <select name="id_spv" id="pildephed" class="form-control ">
                                <option value=""></option>
                                <?php foreach($dafhed as $dh):?>
                                    <option value="<?php echo $dh->id_pengguna; ?>"><?php echo $dh->nama_lengkap." &rarr; ".$dh->nm_jab; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">No. HP</label>
                            <input type="text" name="notifikasi" class="form-control">
                        </div>                        
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" name="email" class="form-control">
                        </div>  
              <!--          <div class="form-group">
                        <p>Paraf</p>
                            <input type="file" multiple id="attachtp" name="attachtp[]" class="form-control">
                        </div> -->
                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                        <!-- <input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script>
    $(document).ready(function(){
        $("#msg").froalaEditor({
            height: 300
        });
        $("#pilatasan").select2({
            placeholder: "Pilih Atasan",
            allowClear: true
        });
        $("#pildephed").select2({
            placeholder: "Pilih Dept. Head",
            allowClear: true
        });        
        $("#penerima").select2();
        $("#piljabatan").select2({
            placeholder: "Pilih Jabatan",
            allowClear: true
        });
        $("#pildivisi").select2({
            placeholder: "Pilih Dept.",
            allowClear: true
        });

        $("#attachtp").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>
