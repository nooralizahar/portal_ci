
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url($this->uriku."/pengguna"); ?>">Kelola Pengguna</a></li>
            <li class="active">Edit Pengguna</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal mengedit pengguna! Coba lagi nanti <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data pengguna berhasil diedit di sistem! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Form Edit Pengguna <?=$pengguna->idp?></div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $pengguna->idp; ?>" name="id_pengguna">
                        <div class="form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" class="form-control" value="<?php echo $pengguna->nama ?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" name="username" class="form-control" value="<?php echo $pengguna->username ?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Password (<label for="bPassword">Ubah</label> <input type="checkbox" id="bPassword"/>)</label>
                            <input type="text" id="iPassword" name="password" class="form-control" disabled="true" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Nomor Induk Karyawan</label>
                            <input type="text" name="nip" class="form-control" value="<?php echo $pengguna->nik?>" required/>
                        </div>
                        <div class="form-group">
                            <label for="">iBoss Link <?=$pengguna->idb;?></label>
                            <select name="id_iboss" id="id_iboss" class="form-control">
                                <option value=""></option>
                                <?php foreach($datibo as $d): ?>
                                    <option value="<?php echo $d->sys_user_id; ?>" <?php echo ($pengguna->idb == $d->sys_user_id) ? "selected" : ""; ?>><?php echo $d->name; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Bagian</label>
                            <select name="id_div" id="divisi" class="form-control" required>
                                <option value=""></option>
                                <?php foreach($daftar_divisi as $divisi): ?>
                                    <option value="<?php echo $divisi->id_div; ?>" <?php echo ($pengguna->id_div == $divisi->id_div) ? "selected" : ""; ?>><?php echo $divisi->nm_div; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Jabatan</label>
                            <select name="id_jabatan" id="jabatan" class="form-control" required>
                                <option value=""></option>
                                <?php foreach($daftar_jabatan as $jabatan):?>
                                    <option value="<?php echo $jabatan->id_jab; ?>" <?php echo ($pengguna->id_jabatan == $jabatan->id_jab) ? "selected" : ""; ?>><?php echo $jabatan->nm_jab; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Atasan</label>
                            <select name="id_spv" id="id_spv" class="form-control">
                                <option value=""></option>
                                <?php foreach($daftas as $atasan):?>
                                    <option value="<?php echo $atasan->id_pengguna; ?>" <?php echo ($pengguna->id_spv == $atasan->id_pengguna) ? "selected" : ""; ?>>

                                        <?php echo $atasan->nama_lengkap." &rarr; ".$atasan->nm_jab; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Dept. Head</label>
                            <select name="id_mgr" id="id_mgr" class="form-control">
                                <option value=""></option>
                                <?php foreach($dafhed as $dh):?>
                                    <option value="<?php echo $dh->id_pengguna; ?>" <?php echo ($pengguna->id_mgr == $dh->id_pengguna) ? "selected" : ""; ?>>

                                        <?php echo $dh->nama_lengkap." &rarr; ".$dh->nm_jab; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">No. HP</label>
                            <input type="text" name="notifikasi" class="form-control" value="<?php echo $pengguna->hp;?>" required/>
                        </div>  
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" name="email" class="form-control" value="<?php echo $pengguna->email;?>">
                        </div>      
                        <div class="form-group">
                            <label for="">Akses Gudang</label>
                            <input type="text" name="gudang" class="form-control" value="<?php echo $pengguna->gudang;?>">
                        </div>                    
                        <!--<div class="form-group">
                            <label for="cek">Pemberi Disposisi (centang bila benar)</label>
                            <input id="cek" type="checkbox" name="disposisi" <?php echo ($pengguna->disposisi == 1) ? "checked" : ""; ?>>
                        </div>
                        <div class="form-group">
                         <label>Paraf (specimen)</label> 
                                        <div class="panel-footer"> 
                                        <?php
                                        if($pengguna->paraf != null):
                                            $paraf= json_decode($pengguna->paraf);
                                            ?>
                                            <h4>Paraf (specimen)</h4>
                                        <ul class="list-group">
                                            <?php foreach($paraf as $item):
                                                $cls = (is_doc($item->file)) ? "attach-docep" : "attach-imgep";
                                                ?>
                                                <li class="list-group-item"><a href="<?php// echo base_url("assets/uploads/paraf/".$item->file); ?>" class="<?php //echo $cls; ?>" data-judul="<?php //echo $item->judul; ?>"><?php //echo $item->judul; ?></a>
                                                    <img src="<?php echo base_url("assets/uploads/paraf/".$item->file); ?>"> </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <?php endif;?>
                                        </div>
                            <input type="file" multiple id="attachep" name="attachep[]" class="form-control" value="<?php echo $pengguna->paraf ?>" >
                        </div>-->

                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                        <!--<input type="submit" name="btnSubmit" class="fa btn btn-success" value="&#xf0c7; Simpan"> -->
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script>
    $(document).ready(function(){
        $("#msg").froalaEditor({
            height: 300
        });
        $("#penerima").select2();
        $("#jabatan").select2({
            placeholder: "Pilih Jabatan",
            allowClear: true
        });
        $("#divisi").select2({
            placeholder: "Pilih Divisi",
            allowClear: true
        });

        $("#attachep").fileinput({'showUpload':false, 'previewFileType':'any'});

        $("#bPassword").on("change",function(){
            if(this.checked){
                $("#iPassword").prop("disabled",false).val("");
            } else {
                $("#iPassword").prop("disabled",true).val("");
            }
        })

        $(".attach-imgep").magnificPopup({
            type: "image"
        });

        $(function(){
            $('.attach-docep').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                return false;
            });
        })

    })
</script>
