﻿<?php $level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;

?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	

    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url($this->uri->segment(1)."/"); ?>">Dashboard</a></li>
            <li class="active"> Aktifitas</li>
        </ol>
    </div><!--/.row-->

    <hr/>
    
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Aktifitas</div>
                <div class="panel-body">    

                  <div class="row">
                      <div class="col-lg-12">

                          <div class="tabbable-panel">
                              <div class="tabbable-line">
                                  <ul class="nav nav-tabs " id="tabku">

                                      <li class="active">
                                          <a href="#tabsatu" data-toggle="tab">
                                           Akt. Harian </a>
                                      </li>
                                      <li>
                                          <a href="#tabtiga" data-toggle="tab">
                                           Akt. Rekap </a>
                                      </li>
                                      <?php if($idjab==1 || $idjab==9 || $idjab==10 || $idjab==12 || $idjab==15): ?> 
                                      <li>
                                          <a href="#tabdua" data-toggle="tab" onclick="//location= '?staff=0-nodata';">
                                          Akt. Staff </a>
                                      </li> 
                                      <?php endif; ?>
                                  </ul>
                                  
                                  <div class="tab-content">

                                      <div class="tab-pane active" id="tabsatu">
                                        <?php $this->load->view($this->uriku."/aktifi/atifdtil")?>

                                      </div>

                                      <div class="tab-pane" id="tabtiga">
                                        <?php $this->load->view($this->uriku."/aktifi/atifrekp")?>

                                      </div> 

                                     <div class="tab-pane" id="tabdua">
                                        <?php $this->load->view($this->uriku."/aktifi/atifbawh")?>

                                      </div> 


                                  </div>
                              </div>
                          </div>


                      </div> <!--/.col-->
                  </div><!--/.row-->           		

  
			  
                </div> <!--/.panel body -->
            </div> <!--/.panel default -->
          </div> <!--/.col -->
      </div> <!--/.row -->
</div>

 <script type="text/javascript">
 	/*
	
  1. Getting the hash value from current URL
  2. Traversing all tab buttons
  3. checking hash attribute with in each href of tab buttons
  4. if there click on it to perform hash change
   //akhil
 
$(".nav-tabs").find("li a").last().click();

var url = document.URL;
var hash = url.substring(url.indexOf('#'));

$(".nav-tabs").find("li a").each(function(key, val) {

  if (hash == $(val).attr('href')) {

    $(val).click();

  }
  $(val).click(function(ky, vl) {

    console.log($(this).attr('href'));
    location.hash = $(this).attr('href');
  });

});*/
 	
 </script>
