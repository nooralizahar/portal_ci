﻿
<?php
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
$divi= $this->db->where("id_div",$this->session->userdata("id_div"))->get("ts_isivid")->result();

foreach($divi as $dvs): 
    $nmdv=$dvs->nm_div; 
    $iddv=$dvs->id_div;
endforeach;

$mharga= $this->db->where("penerima",$nmjab)->get("ts_agrah")->num_rows();
$nharga= $this->db->where("sales",$this->session->userdata("username"))->get("ts_agrah")->num_rows();
$dissuk=0;//$this->db->where("ke_user",$this->session->userdata("id_pengguna"))->get("relasi_disposisi")->num_rows();
$markaz = 0;//$this->db->where("id_dinas",$kantr)->get("dinas")->result();
?>
<?php if($idjab==1):?>
<?php else: ?>
<?php endif; ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Laporan Gagal disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Laporan berhasil disimpan<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>

 <!--      <div class="panel-heading"><i class="fa fa-money fa-lg"></i> Permintaan Harga</div>-->
           <div class="panel-body">
            <?php //echo $this->uri->segment(1);?>
            <a href="<?php echo base_url($this->uriku."/atiftamb"); ?>" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i> <br><small><sup>BARU</sup></small></a>&nbsp;<a href="<?php echo base_url($this->uriku."/loadexcel"); ?>" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg"></i> <br><small><sup>EXCEL</sup></small></a>
                <table class="table table-striped" id="tarminga" data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                       
                        <th data-field="tanggal" data-sortable="true">Tanggal</th>
                        <th data-field="pelanggan" data-sortable="true">Pemohon</th>
                        <th data-field="aktifitas" data-sortable="true">Aktifitas</th>
                        <th data-field="kasus" data-sortable="true">Uraian / Kasus</th>
                        <th data-field="kendala" data-sortable="true">Kendala</th>
                        <th data-field="tindakan" data-sortable="true">Tindakan</th>
                        <th data-field="mulai" data-sortable="true"><center>Mulai</center></th>
                        <th data-field="selesai" data-sortable="true"><center>Selesai</center></th>
                        <th data-field="status" data-sortable="true"><center>Status</center></th>
                        
                        
<!--                     <th>Pilihan</th>-->
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafaktif as $b):?>
                           
                        
                        <tr >
                            <?php 
                            $date1 = new DateTime($b->created_at);
                            $date2 = new DateTime($b->updated_at);

                            $diff = $date2->diff($date1);

                            $hours = $diff->h;
                            $hours = $hours + ($diff->days*24);

                            $sisa=$hours%24; 
                            $hari=($hours-$sisa)/24; 

                            $minutes=$diff->i;
                            $minutes=$minutes + ($diff->days*1440);
                            $sisam=$minutes%1440; 
                            
                            if($hari!=0){$a=$hari." hari ";}else{$a=" ";} 
                            //if($sisa!=0){$b=$sisa." jam ";}else{$b=$sisa." jam ";}
                            //if($minutes!=0){$c=$minutes." menit ";}
                            $x=$a."&nbsp;".$sisa." jam ".$sisam." menit ";
                            ?>
                            
                            
                            <td><?php echo date("d-M-y",strtotime($b->date)); ?></td>
                            <td><?php echo $b->customer; ?></td>
                            <td><?php echo $b->jobs; ?></td>
                            <td><?php echo $b->case; ?></td>
                            <td><?php echo $b->issues; ?></td>
                            <td><?php echo $b->action; ?></td>
                            <td><?php echo date("H:i:s",strtotime($b->created_at)); ?></td>
                            <td><?php echo date("H:i:s",strtotime($b->updated_at)); ?></td>
                            <td><?php if($b->is_solved==0){echo "<a href='".base_url($this->uriku)."/atifubah/$b->id'>open</a>";}elseif($b->is_solved==1){echo "<a href='".base_url($this->uriku)."/atifubah/$b->id'>closed</a>";}else{echo "progress";} echo "<small><br>".$x."</small>";?></td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                   </table>
                  </div>
                </div>
            </div>
        </div> 




 
