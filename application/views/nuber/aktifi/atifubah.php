
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/pengguna"); ?>">Dashboard Aktifitas</a></li>
            <li class="active">Laporan Harian</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Edit Laporan</div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <?php foreach($dafakpil as $b):?>
                        <div class='form-group row'>
                            <label for='lbltgl' class='col-sm-3 form-control-label'>Tanggal </label>
                            <div class="col-sm-9">
                            <!--<label for='lbltgl' class='col-sm-3 form-control-label'><?php $tgl //= date("d M Y", strtotime($b->date)); echo "$tgl"; ?> </label>-->
                            <input type='date'  class='form-control' value="<?php echo $b->date; ?>" name="tgla" >
                            </div>
                            <input type="hidden" name="id" class="form-control" value="<?php echo $b->id; ?>" style="color:blue" readonly/>
                        </div> 
                        <div class='form-group row'>
                            <label for='lblcus' class='col-sm-3 form-control-label'>Pelanggan / Div./ Dep./ </label>
                            <input type="text" name="customer" name="cus" class=" col-sm-9 form-control" value="<?php echo $b->customer; ?>">
                        </div>
                        <div class='form-group row'>
                            <label for='lbltug' class='col-sm-3 form-control-label'>Tugas / Aktifitas </label>
                            <input type="text" name="jobs" name="jobs" class=" col-sm-9 form-control" value="<?php echo $b->jobs; ?>">
                        </div> 
                        <div class='form-group row'>
                            <label for='lblkas' class='col-sm-3 form-control-label'>Uraian / Kasus </label>
                            <label for='isikas' class='col-sm-9 form-control-label' style="color:blue">&nbsp;&nbsp;<?php echo $b->case; ?> </label>
                        </div> 
                        <div class='form-group row'>
                            <label for='lblken' class='col-sm-3 form-control-label'>Kendala </label>
                            <label for='isiken' class='col-sm-9 form-control-label' style="color:blue">&nbsp;&nbsp;<?php echo $b->issues; ?> </label>
                        </div> 
                        <div class='form-group row'>
                            <label for='lbltin' class='col-sm-3 form-control-label'>Tindakan </label>
                            <label for='isitin' class='col-sm-9 form-control-label' style="color:blue">&nbsp;&nbsp;<?php echo $b->action; ?> </label>
                        </div> 
                        <div class='form-group row'>
                            <label for='lbltin' class='col-sm-3 form-control-label'>Mulai </label>
                            <!--<label for='isitin' class='col-sm-9 form-control-label' style="color:blue">&nbsp;&nbsp;<?php //$datem = date("H:i", strtotime($b->created_at)); //echo "$datem"; ?> </label>-->
                            <div class="col-sm-9">
                            <input type='time'  class='form-control' value="" placeholder="00:00:00" name="wktm" /></div>
                        </div> 
                        <div class='form-group row'>
                            <label for='lbltin' class='col-sm-3 form-control-label'>Selesai </label>
                            <div class="col-sm-9">
                            <input type='time'  class='form-control' value="" placeholder="00:00:00" name="wkts" onkeyup="getLocation()"/>
                            
                            </div>

                        </div> 

                        <?php endforeach; ?>
                        <div class='form-group row'>
                        <center><div id="lokasi"></div></center><br>
                        </div> 

                        <center>
                        <button type="submit" name="btnUbah" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>UBAH</sup></small></button></center>

                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row <?php //echo base_url("panel/purcappv/".$dat->id_rpu); ?>"-->

</div>	<!--/.main-->
<script type="text/javascript">
    $(document).ready(function(){
      //$('.date').mask('00/00/0000');
      $('.wkts').mask('00:00:00');
      //$('.date_time').mask('00/00/0000 00:00:00');
     });
</script>
<script type="text/javascript">
    
    var x = document.getElementById("lokasi");
                function getLocation() {
                    if (navigator.geolocation) {
                        navigator.geolocation.watchPosition(showPosition);
                        jamOn();
                    } else { 
                        x.innerHTML = "Browser tidak mendukung Geo Lokasi.";}
                    }
                    
                function showPosition(position) {
                    x.innerHTML="<center><a href='https://www.google.com/maps/@"+ position.coords.latitude +","+ position.coords.longitude+",19z' target='_blank'><small><sub>LOKASI SEKARANG</sub></small></a></br><input type='text' class='form-control text-center' name='latlng' value='" + position.coords.latitude +"," + position.coords.longitude+"' readonly/><br></center>";
                }
                
</script>

