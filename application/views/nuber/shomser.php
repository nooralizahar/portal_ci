﻿
<?php
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
$divi= $this->db->where("id_div",$this->session->userdata("id_div"))->get("ts_isivid")->result();

foreach($divi as $dvs): 
    $nmdv=$dvs->nm_div; 
    $iddv=$dvs->id_div;
endforeach;

$mharga= $this->db->where("penerima",$nmdv)->get("ts_agrah")->num_rows();
$nharga= $this->db->where("sales",$this->session->userdata("username"))->get("ts_agrah")->num_rows();
$dissuk=0;//$this->db->where("ke_user",$this->session->userdata("id_pengguna"))->get("relasi_disposisi")->num_rows();
$markaz = 0;//$this->db->where("id_dinas",$kantr)->get("dinas")->result();
?>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-blue panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $mharga; ?></div>
                        <div class="text-muted">Req. Harga</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-red panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $dissuk;?></div>
                        <div class="text-muted">Diskusi</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div><?php echo $nmjab; ?></div>
                        <div class="text-muted">Akses</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked app-window-with-content"><use xlink:href="#stroked-app-window-with-content"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div ><?php echo $nmdv;  ?></div>
                        <div class="text-muted">Divisi</div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/.row-->


    <hr>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                <?php echo $this->session->flashdata("pesan");?>

 <!--      <div class="panel-heading"><i class="fa fa-money fa-lg"></i> Permintaan Harga</div>-->
           <div class="panel-body">
                <table class="table table-striped" id="tarminga" data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-page-size="25" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tglkirim" data-sortable="true">Tgl. Kirim</th>
                        <th data-field="sales" data-sortable="true">Sales</th>
                        <th data-field="pelanggan" data-sortable="true">Pelanggan</th>
                        <th data-field="site" data-sortable="true">Site</th>
                        <th data-field="uraian" data-sortable="true">Uraian</th>
                      <!--  <th data-field="rpinstall" data-sortable="true">Instalasi (Rp.)</th>
                        <th data-field="rpbulanan" data-sortable="true">Bulanan (Rp.)</th>
                        <th data-field="cha" data-sortable="true">Catatan</th>-->
                        <th data-field="quo" data-sortable="true" class="text-center">BoQ<br>Uploader</th>
                        <th data-field="tgljawab" data-sortable="true" class="text-center">Waktu<br>Upload</th>
                        <th data-field="kpi" data-sortable="true" class="text-center">KPI <br> (hari & jam)</th>
<!--                        <th>Pilihan</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                    if($nmjab=="Administrator"){$daf=$dafmua;}elseif($nmjab=="CRO Supervisor"){$daf=$dafcropv;}elseif($nmjab=="CRO"){$daf=$dafcro;}elseif($nmjab=="Sales"){$daf=$dafrsa;}elseif($nmjab=="Sales Supervisor"){$daf=$dafrsapv;}elseif($nmjab=="Sales Jr. Manager"){$daf=$dafrsamg;}elseif($nmdv=="Manage Service"){$daf=$dafmserv;}else{$daf=$dafreq;}
                    foreach($daf as $harga): ?>
                        <tr >
                            <?php $idr=strval($harga->id_rha); 
                            $date1 = new DateTime($harga->tg_buat);
                            $date2 = new DateTime($harga->mstgup);

                            $diff = $date2->diff($date1);

                            $hours = $diff->h;
                            $hours = $hours + ($diff->days*24);
                            ?>
                            <td><?php echo date("d-M-y H:i:s",strtotime($harga->tg_buat)); ?> </td>
                            <td>
                                <?php echo $harga->nama_lengkap."<br>"; 
                                
                                if($harga->stapv1==1 && $harga->stapv2==1 && $harga->stapv3==1){
                                	if($harga->sales==$this->session->userdata("username")){
                                        echo "<center><a href=".base_url('panel/takquo/'.$harga->id_rha)." data-toggle='tooltip' data-placement='top' title='Cetak Quotation'><i class='fa fa-print fa-lg' style='color:green'></i><sup><small><br>QUO</small></sup></a></center>";     
                                	}
                                    }else{
                                        echo "";
                                    }

                                ?>
                            </td>
                            <td>

                            <?php if($harga->hg_instalasi==0 && $harga->hg_bulanan==0) : 
                                if($harga->sales==$this->session->userdata("username")){?>

                                &nbsp;<a href="<?php echo base_url("panel/hargapus/".$harga->id_rha);  ?>"><i class="fa fa-trash"></i></a>&nbsp;
                            <?php } endif; ?>

                                <?php  
                                    echo $harga->id_rha." - ".$harga->nm_custo."<br><br>";

                                    if($harga->stapv1==null){
                                        echo ""; 
                                    }else{

                                        if($harga->stapv1==1){$st1="<a href='#' data-toggle='tooltip' data-placement='top' title='Approved $harga->tgapv1'><i class='fa fa-check fa-lg' style='color:green'></i></a>&nbsp;";
                                        }elseif(($harga->stapv1==2)){$st1="<a href='#' data-toggle='tooltip' data-placement='top' title='Rejected $harga->tgapv1'><i class='fa fa-times fa-lg' style='color:red'></i></a>&nbsp;";
                                        } 

                                        if($harga->stapv2==1){$st2="<a href='#' data-toggle='tooltip' data-placement='top' title='Approved $harga->tgapv1'><i class='fa fa-check fa-lg' style='color:green'></i></a>&nbsp;";
                                        }elseif(($harga->stapv2==2)){$st2="<a href='#' data-toggle='tooltip' data-placement='top' title='Rejected $harga->tgapv1'><i class='fa fa-times fa-lg' style='color:red'></i></a>&nbsp;";
                                        } 

                                        if($harga->stapv3==1){$st3="<a href='#' data-toggle='tooltip' data-placement='top' title='Approved $harga->tgapv1'><i class='fa fa-check fa-lg' style='color:green'></i></a>&nbsp;";
                                        }elseif(($harga->stapv3==2)){$st3="<a href='#' data-toggle='tooltip' data-placement='top' title='Rejected $harga->tgapv1'><i class='fa fa-times fa-lg' style='color:red'></i></a>&nbsp;";
                                        } 
                                        echo "<sup><small>S : ".$st1."<br> B : ".$st2."<br> D : ".$st3."</small></sup><br>";
                                    }

                            ?> 
                            </td>
                            <td><?php  echo $harga->al_custo; ?></td>
                            <td><?php if($harga->hg_bulanan==""):?><img src="<?php echo base_url('/assets/images/baru.gif') ?>" height="20px"><?php endif;?>&nbsp;<?php  echo $harga->request.", media : ".$harga->media.", service : ".$harga->service.", ";

                                if($harga->ix==0){}else{echo "IX : ".$harga->ix." Mbps";} 
                                if($harga->iix==0){}else{echo "IIX : ".$harga->iix." Mbps";}
                                if($harga->mix==0){}else{echo "MIX : ".$harga->mix." Mbps";}
                                if($harga->llo==0){}else{echo "LOCALLOOP : ".$harga->llo." Mbps";}
                                if($harga->colo==''){}else{echo ", Colocation : ".$harga->colo;}
                                if($harga->tele==0){}else{echo ", Telephony : ".$harga->tele." Line(s)";}
                                if($harga->ket==''){}else{echo "<br> <b>Remark :</b> ". strip_tags(substr($harga->ket,0,50))."<a href='"; echo base_url($this->uriku."/mintil/").$harga->id_rha."' class='btn btn-primary btn-sm'><small> Detil ...</small></a><br> ";}
                                if($harga->lampiran==''){ 

                                }else{ 

                            if($harga->lampiran != null):
                            $lampiran = json_decode($harga->lampiran);
                            ?>
                                <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <a href="<?php echo base_url("assets/uploads/lampiran/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>"><?php echo $item->judul; ?></a>
                                <?php endforeach; ?> 
                            
                             <?php endif;?>



                             <?php  }

                              
                            ?></td>
                            <!--  <td class="text-right"><?php  echo number_format($harga->hg_instalasi); ?></td>
                            <td class="text-right"><?php  echo number_format($harga->hg_bulanan); ?> </td> 
                            <td><small><b><?php echo $harga->hg_catatan;?></b><br>


                            </td>-->
                            <td class="text-center">
                                <?php if($this->session->userdata("id_div")==19): ?>

                                   <?php if($harga->msfile==''): ?> 

                                    <a href="<?php echo base_url($this->uriku."/msdquo/".$harga->id_rha); ?>"class="btn btn-info btn-xs" data-toggle="tooltip" title="Upload BoQ"><i class="fa fa-upload fa-lg"></i></a>
                                   

                                   <?php else: 

                                        $lampiran = json_decode($harga->msfile); ?>

                                        <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                        
                                        <?php foreach($lampiran as $item):
                                        $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                        ?>
                                        <a href="<?php echo base_url("assets/uploads/quo/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>"><?php echo $item->judul; ?></a>
                                        <?php endforeach; ?> 

                                        <?php
                                            if ($harga->id_status_email == 0) {
                                                echo '

                                            <br><a href="'.base_url($this->uriku.'"/pusboq/"'.$harga->id_rha).'" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Hapus BoQ"><i class="fa fa-trash-o fa-lg"></i></a>
                                                &nbsp;<a class="btn btn-info btn-xs sendEmail" type="button" data-toggle="modal" data-id="'.$harga->id_rha.'"><i class="fa fa-print"></i></a>&nbsp;';
                                            }
                                        ?>

                                        <small><br><?php echo $harga->msnama; ?></small>
                            
                                    <?php endif;?>

                                <?php endif;?>   

                            </td>
                            <td class="text-center"><center><?php  echo date("d-M-y H:i:s",strtotime($harga->mstgup)); ?>

                            </center></td>
                            <td><?php $sisa=$hours%24; $hari=($hours-$sisa)/24; echo "<small>".$hari." hari ".$sisa." jam </small>"; if($harga->hg_edit>0) {?>&nbsp;<sup><span class="badge"><small><?php echo $harga->hg_edit;?></small></span></sup><?php }else{} ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                   </table>
                  </div>
                </div>
            </div>
        </div> 

        <!-- Modal -->
        <div class="modal" id="sendEmail" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Send Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form action="/nuber/sendEmail" method="POST">
              <div class="modal-body">
                <input type="hidden" name="id_rha" class="id">
                <select class="form-control email" style="width: 100%" type="email" multiple="multiple" name="email[]">
                    <option value="purchasing@jlm.net.id">purchasing@jlm.net.id</option>
                    <option value="presales@jlm.net.id">presales@jlm.net.id</option>
                    <option value="daniel.suryo@jlm.net.id">daniel.suryo@jlm.net.id</option>
                    <option value="suryo.jatmiko@jlm.net.id">suryo.jatmiko@jlm.net.id</option>
                </select>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
              </form>
            </div>
          </div>
        </div>
        <<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script type="text/javascript">
            $(".email").select2({
                tags: true,
                placeholder: "input email",
                tokenSeparators: [',', ' ']
            })
        </script>
        <script type="text/javascript">
            $(document).on('click', '.sendEmail', function() {
                var id = $(this).data("id");
                $('.id').val(id);
                $('#sendEmail').modal('show');
                console.log(id);
            });
        </script>



 
