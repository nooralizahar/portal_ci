﻿
<?php



?>

    <div class="form-control row">

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-blue panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $marea; ?></div>
                        <div class="text-muted">Req. Area </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-red panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo 0;?></div>
                        <div class="text-muted">Diskusi</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div><?php echo $this->najab; ?></div>
                        <div class="text-muted">Akses</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked app-window-with-content"><use xlink:href="#stroked-app-window-with-content"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div ><?php echo $this->nadiv;  ?></div>
                        <div class="text-muted">Divisi</div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/.row-->


    <hr>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">

                    <?php echo $this->session->flashdata("pesan");?>


           <div class="panel-body">
                <table class="table table-striped" id="tarminga" data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-page-size="25" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a" data-sortable="true" class="text-center">ID #</th>
                        <th data-field="b" data-sortable="true" class="text-center">Buka Area</th>
                        <th data-field="c" data-sortable="true" class="text-center">Kel./Desa</th>
                        <th data-field="d" data-sortable="true" class="text-center">Kecamatan</th>
                        <th data-field="e" data-sortable="true" class="text-center">Site</th>
                        <th data-field="f" data-sortable="true" class="text-center">Tikor</th>
                        <th data-field="g" data-sortable="true" class="text-center">Buka Area<br>oleh</th>
                        <th data-field="h" data-sortable="true" class="text-center">PIC</th>
                        <th data-field="i" data-sortable="true" class="text-center">Tgl. Kirim</th>  
                        <th data-field="j" data-sortable="true" class="text-center">Lampiran</th>
                        <th data-field="k" data-sortable="true" class="text-center">HoP / Occ / CPO</th>
                        <th data-field="ops" data-sortable="true">Opsi</th>
                        <th data-field="kpi" data-sortable="true" class="text-center">KPI <br> (hari & jam)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 

                    foreach($dafnedev as $d): ?>
                        <tr >
                            <?php 
                            $idr=strval($d->idrea); 
                            $date1= new DateTime($d->tgrea);
                            $date2= new DateTime();

                            $diff = $date2->diff($date1);

                            $hours = $diff->h;
                            $hours = $hours + ($diff->days*24);
                            ?>
                            <td><?php echo $d->idrea;?></td>
                            <td><?php echo $d->nalok;?></td>
                            <td><?php echo $d->salok; ?></td>
                            <td><?php echo $d->talok; ?></td>                            
                            <td><?php echo $d->rilok; ?></td>
                            <td><?php echo $d->tikor; ?></td>
                            <td><?php echo $d->bagian; ?></td>
                            <td><?php echo $d->nama; ?></td>
                            <td><?php echo date("d-M-y H:i:s",strtotime($d->tgrea)); ?> </td>

                            <td>
                                <?php if($d->piran==''){ 

                                    }else{ 

                                        if($d->piran != null):
                                        $l = json_decode($d->piran);
                                        ?>
                                            <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                            <?php foreach($l as $item):
                                            $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                            ?>

                                            <!-- Preview Lampiran Biasa
                                            <a href="<?php echo base_url("assets/uploads/netdev/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><?php echo $item->judul; ?></a> -->

                                            <!-- Lihat Lampiran dalam Halaman -->
                                            <a href="<?php echo base_url($this->uriku."/hatran/".$d->idrea); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->file; ?>"><?php echo $item->judul; ?></a>

                                            <?php endforeach; ?> 
                                        
                                         <?php endif;?>

                                 <?php  } ?>

                            </td>
                            <td><?php echo $d->hplok." / ".$d->oclok." / [".$d->polok."/".$d->klien."]";?></td>
                                        
                            <td class="text-center">
                                <?php if($d->phrea==$this->idaku):?>

                                    <!-- batas awal CPO -->
                                    <?php if($d->polok==$d->klien):?>
                                    <?php else: ?>
                                    <a href="<?php echo base_url($this->uriku."/dafcpo/".$d->idrea) ?>" data-toggle='tooltip' title='B. Daftar Customer PO'><i class='fa fa-user-plus fa-lg' style='color:blue'></i></a>
                                    <?php endif;?>
                                    <!-- batas akhir CPO -->
                                    

                                    <!-- batas awal ODP -->
                                    <a href="<?php echo base_url($this->uriku."/dafodp/".$d->idrea) ?>" data-toggle='tooltip' title='C. Tikor ODP'><i class='fa fa-hdd-o fa-lg' style='color:blue'></i></a>
                                    <!-- batas akhir ODP -->

                                    <!-- batas awal Kompetitor -->
                                    <a href="<?php echo base_url($this->uriku."/dafkom/".$d->idrea) ?>" data-toggle='tooltip' title='D. Kompetitor'><i class='fa fa-user-circle-o fa-lg' style='color:blue'></i></a>
                                    <!-- batas akhir Kompetitor -->


                                <?php endif;?>  

                                <?php if($d->phrea==$this->idaku):?>
                                                             

                                <?php endif;?>


                            </td>
                            <!--<td class="text-center"><?php if($d->idiom==0){echo "<a href='#' data-toggle='tooltip' title='Belum'><i class='fa fa-minus-square' style='color:blue'></i></a>";}else{echo "<a href='#' data-toggle='tooltip' title='Selesai'><i class='fa fa-check-circle' style='color:green'></i></a>";} ?></td>
                            <td class="text-center"><a href="#" data-toggle='tooltip' title='Desktop Study'><i class="fa fa-search"></i></a></td>-->
                            <td><?php $sisa=$hours%24; $hari=($hours-$sisa)/24; echo "<small>".$hari." hari ".$sisa." jam </small>"; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                   </table>
                  </div>
                </div>
            </div>
        </div> 




 
