﻿
<?php
        $thnini= date('Y');
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));
        $harini= date('Y-m-d');
        $harlal= date('Y-m-d',strtotime("-1 day")); 

if ($this->uriku=='nuber'){
    
}else{
//echo "<div class='col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main'>";
}
?>


<div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
          <li><center> REKAPITULASI ROLLOUT TAHUN <?=date('Y')?></center></li>

    </ol>
<br/> <?php //echo $this->uriku; ?>

        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-home fa-3x"></i>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?=$this->db->where("bltia like ",date('Y')."-%")->get("app_latrop_gnait")->num_rows();?></div>
                        <div class="text-muted"> Alamat Pole</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-teal-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-map-signs fa-3x"></i>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?=$this->db->where("bltia like ",date('Y')."-%")->get("v_app_latrop_gnait_lit_hit")->num_rows();?></div>
                        <div class="text-muted"> Jumlah Tiang</div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div> 

<div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
          <li><center> REKAPITULASI TIANG KESELURUHAN</center></li>

    </ol>
<br/>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-arrows-alt fa-3x"></i>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?=$this->db->where("tiseg","Distribusi")->get("app_latrop_gnait_lit")->num_rows();?></div>
                        <div class="text-muted"> Distribusi</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-anchor fa-3x"></i>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?=$this->db->where("tiseg","Tumpuan")->get("app_latrop_gnait_lit")->num_rows();?></div>
                        <div class="text-muted"> Tumpuan</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-blue panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-share-alt fa-3x"></i>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?=$this->db->where("tiseg","Feeder")->get("app_latrop_gnait_lit")->num_rows();?></div>
                        <div class="text-muted"> Feeder</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-blue-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-flag fa-3x"></i>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?=$this->db->get("app_latrop_gnait_lit")->num_rows();?></div>
                        <div class="text-muted"> Total</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div> 



<div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
          <li><center> REKAPITULASI TARIKAN KABEL </center></li>

    </ol>
<br/>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-blue-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-calendar-check-o fa-3x"></i>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $u=$this->db->select("sum(jml) as tjml")->where("bln like",$thnini."%")->get("v_app_latrop_lebak_bln")->row(); echo $u->tjml."<small> m</small>";?></div>
                        <div class="text-muted"> Jumlah Keseluruhan</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-calendar-o fa-3x"></i>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("bln",$blnini)->get("v_app_latrop_lebak_bln")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> Bulan ini</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa fa-calendar fa-3x"></i>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("bln",$blnlal)->get("v_app_latrop_lebak_bln")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> Bulan lalu</div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div> 

<div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
          <li><center> REKAPITULASI JENIS KABEL</center></li>

    </ol>
<br/>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                    <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                12    
                            </strong>
                    </span>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("jekab","12 Core")->get("v_app_latrop_lebak_jka")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> 12 Core</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                     <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                24    
                            </strong>
                    </span>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("jekab","24 Core")->get("v_app_latrop_lebak_jka")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> 24 Core</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                    <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                48    
                            </strong>
                    </span>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("jekab","48 Core")->get("v_app_latrop_lebak_jka")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> 48 Core</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                    <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                96    
                            </strong>
                    </span>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("jekab","96 Core")->get("v_app_latrop_lebak_jka")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> 96 Core</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
          <li><center> DETIL TARIKAN KABEL</center></li>

    </ol>
<br/>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-merah panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-arrows-alt fa-3x"></i>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("sekab","Distribusi")->get("v_app_latrop_lebak_seg")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> Distribusi</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-merah-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-anchor fa-3x"></i>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("sekab","Backbone")->get("v_app_latrop_lebak_seg")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> Backbone</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-share-alt fa-3x"></i>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("sekab","Feeder")->get("v_app_latrop_lebak_seg")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> Feeder</div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>




