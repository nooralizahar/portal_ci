        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">

                    <?php echo $this->session->flashdata("pesan");?>


           <div class="panel-body">
                <table class="table table-striped" id="tarminga" data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-page-size="25" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a" data-sortable="true" class="text-center">ID #</th>
                        <th data-field="b" data-sortable="true" class="text-center">Buka Area</th>
                        <th data-field="c" data-sortable="true" class="text-center">Kel./Desa</th>
                        <th data-field="d" data-sortable="true" class="text-center">Kecamatan</th>
                        <th data-field="e" data-sortable="true" class="text-center">Site</th>
                        <th data-field="f" data-sortable="true" class="text-center">Tikor</th>
                        <th data-field="g" data-sortable="true" class="text-center">Buka Area<br>oleh</th>
                        <th data-field="h" data-sortable="true" class="text-center">PIC</th>
                        <th data-field="i" data-sortable="true" class="text-center">Tgl. Kirim</th>  
                        <th data-field="j" data-sortable="true" class="text-center">Lampiran</th>
                        <th data-field="k" data-sortable="true" class="text-center">HoP / Occ / CPO</th>
                        <th data-field="ops" data-sortable="true">Opsi</th>
                        <th data-field="kpi" data-sortable="true" class="text-center">KPI <br> (hari & jam)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 

                    foreach($dafneapv as $d): ?>
                        <tr >

                            <td><?php if($d->apsa==0){echo "<span class='blinking'>".$d->idrea."</span>";}else{echo $d->idrea;}?></td>
                            <td><?php echo $d->nalok;?></td>
                            <td><?php echo $d->salok; ?></td>
                            <td><?php echo $d->talok; ?></td>                            
                            <td><?php echo $d->rilok; ?></td>
                            <td><?php echo $d->tikor; ?></td>
                            <td><?php echo $d->bagian; ?></td>
                            <td><?php echo $d->nama; ?></td>
                            <td><?php echo date("d-M-y H:i:s",strtotime($d->tgrea)); ?> </td>

                            <td>
                                <?php if($d->piran==''){ 

                                    }else{ 

                                        if($d->piran != null):
                                        $l = json_decode($d->piran);
                                        ?>
                                            <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                            <?php foreach($l as $item):
                                            $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                            ?>

                                            <!-- Preview Lampiran Biasa
                                            <a href="<?php echo base_url("assets/uploads/netdev/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><?php echo $item->judul; ?></a> -->

                                            <!-- Lihat Lampiran dalam Halaman -->
                                            <a href="<?php echo base_url($this->uriku."/hatran/".$d->idrea); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->file; ?>"><?php echo $item->judul; ?></a>

                                            <?php endforeach; ?> 
                                        
                                         <?php endif;?>

                                 <?php  } ?>

                            </td>
                            <td><?php echo $d->hplok." / ".$d->oclok." / [".$d->polok."/".$d->klien."]";?></td>
                                        
                            <td class="text-center">
                              
                                    <!-- batas awal ODP -->
                                    <?php if($d->apsa==0):?>
                                    <a href="<?php echo base_url($this->uriku."/dafapv/".$d->idrea) ?>" data-toggle='tooltip' title='Setujui'><i class='fa fa-pencil-square-o fa-lg' style='color:blue'></i></a>
                                    <?php elseif($d->apsb==0 ):?>                                      
                                            <a href="<?php echo base_url($this->uriku."/dafapv/".$d->idrea) ?>" data-toggle='tooltip' title='Setujui'><i class='fa fa-pencil-square-o fa-lg' style='color:blue'></i></a>
                                    <?php else:?>
                                      <a href="<?php echo base_url($this->uriku."/dafapv/".$d->idrea) ?>" data-toggle='tooltip' title='Preview'><i class='fa fa-eye fa-lg' style='color:blue'></i></a>                                  
                                    <?php endif;?>
                                    <!-- batas akhir ODP -->



                            </td>
                            <!--<td class="text-center"><?php if($d->idiom==0){echo "<a href='#' data-toggle='tooltip' title='Belum'><i class='fa fa-minus-square' style='color:blue'></i></a>";}else{echo "<a href='#' data-toggle='tooltip' title='Selesai'><i class='fa fa-check-circle' style='color:green'></i></a>";} ?></td>
                            <td class="text-center"><a href="#" data-toggle='tooltip' title='Desktop Study'><i class="fa fa-search"></i></a></td>-->
                            <td>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                   </table>
                  </div>
                </div>
            </div>
        </div> 