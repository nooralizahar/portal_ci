<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url($this->uriku."/"); ?>">Dashboard</a></li>
            <li class="active">&nbsp;Kompetitor</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> D. Kompetitor</div>
                    <?php echo $this->session->flashdata("pesan");?>
                <div class="panel-body">


        <form class='add-item' method=POST action='' onsubmit="FormSubmit(this);">
         <div class='form-group row'>
                <div class='col-sm-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Nama Kompetitor</span>
                    <input type='text' name='komba' class='form-control' id='komba' placeholder="isi kompetitor disini" required/>
                    <input type='hidden' name='idrea' class='form-control' id='idrea' value="<?=$id?>">
                        </div>
                    </div>
                </div>   

        </div>


        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-code fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
               <!-- <button type='button' onclick="window.history.back()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>-->
            </div>
        </div>
                
        </form>

    <br>

        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true" class="text-center">ID#</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Nama Kompetitor</th>
                        <th data-field="h"  data-sortable="true" class="text-center">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafkom as $t): ?>
                        <tr>
                           <td class="text-right"><?php echo $t->idkom; ?></td>
                           <td class="text-center"><?php echo $t->komba; ?></td>
                           <td class="text-center">

                                <a href="<?php echo base_url($this->uriku."/puskom/".$t->idkom."/".$t->idrea); ?>" data-toggle='tooltip' title='Anda yakin akan menghapus data ini?'><i class="fa fa-trash-o fa-lg"></i></a>&nbsp;&nbsp;
 
                            </td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                </div> <!--/.panel body-->
            </div>
        </div>
    </div><!--/.row-->

</div>  <!--/.main-->



