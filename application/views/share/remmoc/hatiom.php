<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url($this->uriku."/"); ?>">Dashboard</a></li>
            <li> Buka Area</li>
            <li class="active"> Persetujuan</li>
        </ol>
    </div><!--/.row-->

    <hr/>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                <div class="panel-heading"> A. Detil Area</div>
                <div class="panel-body">
        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a" data-sortable="true" class="text-center">ID #</th>
                        <th data-field="b" data-sortable="true" class="text-center">Buka Area</th>
                        <th data-field="c" data-sortable="true" class="text-center">Kel./Desa</th>
                        <th data-field="d" data-sortable="true" class="text-center">Kecamatan</th>
                        <th data-field="e" data-sortable="true" class="text-center">Site</th>
                        <th data-field="f" data-sortable="true" class="text-center">Tikor</th>
                        <th data-field="g" data-sortable="true" class="text-center">Buka Area<br>oleh</th>
                        <th data-field="h" data-sortable="true" class="text-center">PIC</th>
                        <th data-field="i" data-sortable="true" class="text-center">Tgl. Kirim</th>  
                        <th data-field="j" data-sortable="true" class="text-center">Lampiran</th>
                        <th data-field="k" data-sortable="true" class="text-center">HoP / Occ / CPO</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 

                    foreach($dafiom as $d): ?>
                        <tr >

                            <td><?php echo $d->idrea;?></td>
                            <td><?php echo $d->nalok;?></td>
                            <td><?php echo $d->salok; ?></td>
                            <td><?php echo $d->talok; ?></td>                            
                            <td><?php echo $d->rilok; ?></td>
                            <td><?php echo $d->tikor; ?></td>
                            <td><?php echo $d->bagian; ?></td>
                            <td><?php echo $d->nama; ?></td>
                            <td><?php echo date("d-M-y H:i:s",strtotime($d->tgrea)); ?> </td>

                            <td>
                                <?php if($d->piran==''){ 

                                    }else{ 

                                        if($d->piran != null):
                                        $l = json_decode($d->piran);
                                        ?>
                                            <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                            <?php foreach($l as $item):
                                            $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                            ?>

                                            <!-- Preview Lampiran Biasa
                                            <a href="<?php echo base_url("assets/uploads/netdev/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><?php echo $item->judul; ?></a> -->

                                            <!-- Lihat Lampiran dalam Halaman -->
                                            <a href="<?php echo base_url($this->uriku."/hatran/".$d->idrea); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->file; ?>"><?php echo $item->judul; ?></a>

                                            <?php endforeach; ?> 
                                        
                                         <?php endif;?>

                                 <?php  } ?>

                            </td>
                            <td><?php echo $d->hplok." / ".$d->oclok." / [".$d->polok."/".$d->klien."]";?></td>
                                        
                            <?php $naju=$d->nama; $taju=$d->tgrea; $apva=$d->apsa; $tpva=$d->tgsa; $kpva=$d->kosa; $apvb=$d->apsb; $tpvb=$d->tgsb; $kpvb=$d->kosb; $npva=$d->npsa; $npvb=$d->npsb;?> 
                            <!--<td class="text-center"><?php if($d->idiom==0){echo "<a href='#' data-toggle='tooltip' title='Belum'><i class='fa fa-minus-square' style='color:blue'></i></a>";}else{echo "<a href='#' data-toggle='tooltip' title='Selesai'><i class='fa fa-check-circle' style='color:green'></i></a>";} ?></td>
                            <td class="text-center"><a href="#" data-toggle='tooltip' title='Desktop Study'><i class="fa fa-search"></i></a></td>
                            <td>
                            </td>-->
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                   </table>
                  </div>
                </div>
            </div>
        </div> 
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> B. Daftar Customer PO</div>
                <div class="panel-body">

    <br>

        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true" class="text-center">ID#</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Nama Klien</th>
                        <th data-field="b"  data-sortable="true" class="text-center">Alamat</th>            
                        <th data-field="c"  data-sortable="true" class="text-center">Tikor Klien</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafcpo as $t): ?>
                        <tr>
                            <td class="text-right"><?php echo $t->idkli; ?></td>
                            <td class="text-center"><?php echo $t->nakli; ?></td>
                            <td class="text-center"><?php echo $t->alkli; ?></td>
                            <td class="text-center"><?php echo $t->tikli; ?></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                </div> <!--/.panel body-->
            </div>
        </div>
    </div><!--/.row-->


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> C. Tikor ODP</div>

                <div class="panel-body">

    <br>

       <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true" class="text-center">ID#</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Tikor ODP Baru</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafodp as $t): ?>
                        <tr>
                           <td class="text-right"><?php echo $t->idodp; ?></td>
                           <td class="text-center"><?php echo $t->korba; ?></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                </div> <!--/.panel body-->
            </div>
        </div>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> D. Kompetitor</div>
                <div class="panel-body">

    <br>

        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true" class="text-center">ID#</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Nama Kompetitor</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafkom as $t): ?>
                        <tr>
                           <td class="text-right"><?php echo $t->idkom; ?></td>
                           <td class="text-center"><?php echo $t->komba; ?></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                </div> <!--/.panel body-->
            </div>
        </div>
    </div><!--/.row-->

    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading"><center> Persetujuan IoM Sales</center></div>
                <div class="panel-body">
<table width="100%" border="0"><tr><td class="text-center">
            <h5>Diajukan oleh </h5><br><br>
            <h5><?php echo $naju;?></h5>
            <h5><?php echo date("d-M-Y H:i",strtotime($taju));?></h5><h5>&nbsp;</h5></td>


        <form class='add-item' method=POST action=''>
        <?php if($apva==154):?>
            <td class="text-center" width="40%">
            <h5>Mengetahui,</h5><br><br>
            <h5><?php echo $npva;?></h5>
            <h5><?php echo date("d-M-Y H:i",strtotime($tpva))."<br>".hitkpi($tpva,$taju);?></h5>
            <h6>Catatan : <?php echo $kpva;?></h6></td>
        <?php else:?>

         <div class='form-group row'>
                <div class='col-sm-12'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix"> Catatan</span>
                    <input type='text' name='koapv' class='form-control' id='koapv' >
                    <input type='hidden' name='idrea' class='form-control' id='idrea' value="<?php echo $id; ?>" >
                        </div>
                    </div>
                </div>   
        </div> 
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-check-circle fa-lg"></i><br><small><sup>SUBMIT</sup></small></button>

            </div>
        </div>
        <?php endif; ?>

        <?php if($apva!=0 && $this->idaku==5):?>

            <?php if($apvb==5):?>
                <td class="text-center"><h5>Menyetujui,</h5><br><br>
                <h5><?php echo $npvb;?></h5>
                <h5><?php echo date("d-M-Y H:i",strtotime($tpvb))."<br>".hitkpi($tpvb,$taju);?></h5>
                <h6>Catatan : <?php echo $kpvb;?></h6></td>
            <?php else:?>

         <div class='form-group row'>
                <div class='col-sm-12'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix"> Catatan</span>
                    <input type='text' name='koapv' class='form-control' id='koapv' >
                    <input type='hidden' name='idrea' class='form-control' id='idrea' value="<?php echo $id; ?>" >
                        </div>
                    </div>
                </div>   
        </div> 
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-check-circle fa-lg"></i><br><small><sup>SUBMIT</sup></small></button>

            </div>
        </div>
             <?php endif; ?>
        <?php endif; ?>

   
                
        </form>
</tr></table>



</div>  <!--/.main-->



