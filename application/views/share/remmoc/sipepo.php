<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url($this->uriku."/"); ?>"> Dashboard</a></li>
            <li class="active"> Kabel</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> B. Daftar Customer PO</div>
                    <?php echo $this->session->flashdata("pesan");?>
                <div class="panel-body">


        <form class='add-item' method=POST action='' onsubmit="FormSubmit(this);">
         <div class='form-group row'>
                <div class='col-sm-3'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Nama Klien</span>
                    <input type='text' name='nakli' class='form-control' id='nakli' >
                    <input type='hidden' name='idrea' class='form-control' id='idrea' value="<?=$id?>">
                        </div>
                    </div>
                </div>  
                <div class='col-sm-5'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Alamat</span>
                    <input type='text' name='alkli' class='form-control' id='alkli' >
                        </div>
                    </div>
                </div>   


                <div class='col-sm-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Tikor Klien</span>
                            <input type='text' name='tikli' class='form-control' id='tikli' >
                            
                        </div>
                    </div>
                </div>   

        </div>


        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-user-plus fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
               <!-- <button type='button' onclick="window.history.back()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>-->
            </div>
        </div>
                
        </form>

    <br>

        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true" class="text-center">ID#</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Nama Klien</th>
                        <th data-field="b"  data-sortable="true" class="text-center">Alamat</th>            
                        <th data-field="c"  data-sortable="true" class="text-center">Tikor Klien</th>
                        <th data-field="h"  data-sortable="true" class="text-center">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafcpo as $t): ?>
                        <tr>
                            <td class="text-right"><?php echo $t->idkli; ?></td>
                            <td class="text-center"><?php echo $t->nakli; ?></td>
                            <td class="text-center"><?php echo $t->alkli; ?></td>
                            <td class="text-center"><?php echo $t->tikli; ?></td>
                           <td class="text-center">

                                <a href="<?php echo base_url($this->uriku."/puscpo/".$t->idkli."/".$t->idrea); ?>" data-toggle='tooltip' title='Anda yakin akan menghapus data ini?'><i class="fa fa-trash-o fa-lg"></i></a>&nbsp;&nbsp;
 
                            </td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                </div> <!--/.panel body-->
            </div>
        </div>
    </div><!--/.row-->

</div>  <!--/.main-->


<script>

    $(document).ready(function(){

            $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});

            $(".psekab").select2();
            $(".pjekab").select2();
            $(".podcdp").select2();

    })
</script>
<script type="text/javascript">

function DDCjekab(oDDL) {
    var oTextbox = oDDL.form.elements["jekab_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}
function DDCsekab(oDDLl) {
    var oTextboxl = oDDLl.form.elements["sekab_txt"];
    if (oTextboxl) {
        oTextboxl.style.display = (oDDLl.value == "") ? "" : "none";
        if (oDDLl.value == "")
            oTextboxl.focus();
    }
}

function DDCodcdp(oDDLj) {
    var oTextboxj = oDDLj.form.elements["odcdp_txt"];
    if (oTextboxj) {
        oTextboxj.style.display = (oDDLj.value == "") ? "" : "none";
        if (oDDLj.value == "")
            oTextboxj.focus();
    }
}


function FormSubmit(oForm) {
    var oHidden = oForm.elements["jekab"];
    var oDDL = oForm.elements["jekab_ddl"];
    var oTextbox = oForm.elements["jekab_txt"];

    var oHiddenl = oForm.elements["sekab"];
    var oDDLl = oForm.elements["sekab_ddl"];
    var oTextboxl = oForm.elements["sekab_txt"];

    var oHiddenj = oForm.elements["odcdp"];
    var oDDLj = oForm.elements["odcdp_ddl"];
    var oTextboxj = oForm.elements["odcdp_txt"];


    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;

    if (oHiddenl && oDDLl && oTextboxl)
        oHiddenl.value = (oDDLl.value == "") ? oTextboxl.value : oDDLl.value;

    if (oHiddenj && oDDLj && oTextboxj)
        oHiddenj.value = (oDDLj.value == "") ? oTextboxj.value : oDDLj.value;

}

</script>
