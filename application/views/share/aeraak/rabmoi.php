<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url($this->uriku."/");?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url($this->uriku."/iombar/"); ?>"> IoM</a></li>
            <li class="active"> Hasil Rapat</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4> HASIL RAPAT</h4>
                </div>
        <div class="panel-body">
         <?php echo $this->session->flashdata("pesan");?>
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

  
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#requester" aria-controls="requester" role="tab" data-toggle="tab"> Baru</a></li>
                            <li role="presentation" class="active"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Daftar Hasil</a></li>
                        </ul>



<div class="tab-content">
  <div role="tabpanel" class="tab-pane" id="requester">
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">
<!-- Awal Isi Tab-->
<form method=POST enctype='multipart/form-data' action=''>


                <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Lokasi </label>
                <div class='col-sm-4'>
                    <select name="idlok" id="idlok" class="form-control lokasi" required>
                                <option value="">--Pilih Lokasi--</option>
                                <?php foreach($datlok as $l):?>
                                    <option value="<?php echo $l->idrea; ?>"><?php echo $l->nalok." - ".$l->sales; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
                </div> 
                
                <div class='form-group row'>
                    <label for='tgmtc1' class='col-sm-2 form-control-label'>Tanggal</label>
                    <div class='col-sm-3'>
                      <input type='date' name='tgmom' class='form-control' id='tgmom' required>
                    </div>

                </div>
                <div class='form-group row'>
                    <label for='jmmtc1' class='col-sm-2 form-control-label'>Waktu (Durasi)</label>
                    <div class='col-sm-2'>
                      <input type='text' name='jmmom' class='form-control' id='jmmom' placeholder="00:00:00" required>
                    </div>
                    <label for='jamul1' class='col-sm-1 form-control-label'>s/d</label>
                    <div class='col-sm-2'>
                      <input type='text' name='jsmom' class='form-control' id='jsmom' placeholder="00:00:00" onkeyup="hitmenit()" required>
                    </div>
                </div> 

                <div class='form-group row'>
                    <label for='lamarpt' class='col-sm-2 form-control-label'>Durasi </label>
                    <div class='col-sm-3'>
                      <input type='text' name='durasi' class='form-control' id='durasi' readonly/>
                    </div>
                    <label for='dntime2' class='col-sm-7 form-control-label'>menit</label>
                </div>
                <div class="form-group row">
                    <label for='tgmtc1' class='col-sm-2 form-control-label'>Dari JLM</label>
                    <div class='col-sm-4'>
                      <input type='text' name='fmjlm' class='form-control' id='fmjlm' placeholder="Ketik partisipan JLM disini gunakan tanda koma (,)" required>
                    </div>
                    <label for='tgmtc1' class='col-sm-2 form-control-label'>Dari Perum / Lingk.</label>
                    <div class='col-sm-4'>
                      <input type='text' name='fmpeli' class='form-control' id='fmpeli' placeholder="Ketik partisipan warga disini gunakan tanda koma (,)" required>
                    </div>
                </div>
                
                <div class='form-group row'>
                    <label for='perihal' class='col-sm-2 form-control-label'>Perihal</label>
                    <div class='col-sm-10'>
                      <input type='text' name='perihal' class='form-control' id='perihal' placeholder="Ketik perihal disini" required>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='uraian' class='col-sm-2 form-control-label'>Uraian</label>
                    <div class='col-sm-10'>
                    <textarea name='uraian' class='ckeditor form-control' id='uraian' rows='7' required></textarea>
                      
                    </div>
        
                </div>
 
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                      <button type='button' onclick="goBack()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>



<!-- Akhir Isi Tab-->
             </div>
         </div>
     </div>
</div>


<div role="tabpanel" class="tab-pane active" id="problem">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">
<div class="row">
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-2">  </div>
        </div>
        <hr/>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->get('app_latrop_aeraak_iom')->num_rows()); ?></div>
                        <div class="text-muted"> IoM</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    <a href="<?php echo base_url("focmen/odpcs/"); ?>" >
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->get('app_latrop_aeraak_iom')->num_rows()); ?></div>
                        <div class="text-muted"> Perumahan</div>
                    </div></a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked line graph"><use xlink:href="#stroked-line-graph"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large">0<a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php  //$l=$tlok-$plok; echo number_format($l);?> </a></div>
                        <div class="text-muted">Lingkungan</div>
                    </div>
                </div>
            </div>
        </div>


</div> <br>

    <br>

    <br>

        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="b" data-sort-order="asc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true">#</th>
                        <th data-field="b"  data-sortable="true">Lokasi</th>
                        <th data-field="c"  data-sortable="true">Tgl./Waktu</th>
                        <th data-field="d"  data-sortable="true">JLM</th>
                        <th data-field="e"  data-sortable="true">Perum./Lingk.</th>
                        <th data-field="f"  data-sortable="true">Perihal</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($datiom as $dat): ?>
                        <tr>
                            <td><?php echo $dat->idiom; ?></td>
                            <td><?php echo $dat->idlok; ?></td>
                            <td><?php echo $dat->tgmom; ?></td>
                            <td><?php echo $dat->fmjlm; ?></td>
                             <td><?php echo $dat->fmpeli; ?></td>
                            <td><?php echo $dat->perihal; ?></td>
                            

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>









            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>  <!--/.main-->



<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('immtc',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>
    function goBack() {
  		window.history.back();
	}

	function hitmenit() {

    var wak1 = document.getElementById("jmmom").value;
    var wak2 = document.getElementById("jsmom").value;
    var wa = wak1.split(":");
    var wb = wak2.split(":");
    var t1= new Date(2020,10,21,wa[0],wa[1]);
 	var t2= new Date(2020,10,21,wb[0],wb[1]);
 	var di=t2.getTime() - t1.getTime();
 	var menit = Math.floor(di / 1000 / 60);

   
        document.getElementById("durasi").value=menit;
	}

    $(document).ready(function(){


        $(".req").select2();


        $(".tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>