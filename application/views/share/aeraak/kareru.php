<?php ?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url($this->uriku)?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url($this->uriku."/kaarea/"); ?>">Buka Area</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Form Permintaan Buka Area</div>
                <div class="panel-body">
				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php echo $this->session->flashdata("pesan");?>
                            </div>
                        </div>
                    </div>
<form action="" method="POST" enctype="multipart/form-data" onsubmit="FormSubmit(this);">

<div class='form-group row'> 
        <div class="col-md-12">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Kepada </span>
                    <!--<select name="netdev" class="form-control penerima" multiple>
                        <?php foreach($daftar_jabatan as $jabatan):?>
                            <option value="<?php echo $jabatan->nm_jab; ?>" selected> <?php echo $jabatan->nm_jab;?></option>
                        <?php endforeach;?>
                    </select> -->
                    <select name="delak" class="form-control deplak" > //multiple
                                <option value="<?php echo $datdiv->id_div; ?>" selected> <?php echo $datdiv->nm_div."";?></option>           
                    </select>
                </div>
            </div>    
        </div>   
</div>

<div class='form-group row'> 
                <div class='col-sm-3'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Site</span>
                                <input type="hidden" name="rilok" class="form-control" />
                                <select name="rilok_ddl" onchange="DDCrilok(this);" class="form-control prilok" style="width: 100%" />
                                    <option value=""></option>
                                    <?php foreach($drilok as $s):?>
                                    <option value="<?php echo $s->rilok; ?>"><?php echo $s->rilok; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Kategori Lainnya...</option>
                                </select> <input type="text" placeholder="isi kategori lainnya disini" name="rilok_txt" style="display: none;" class="form-control"/>

                        </div> 
                    </div>
                </div>   

                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Nama Area </span>
                                <input type="text" name="nalok" placeholder="isi nama area disini" class="form-control" required/>
                        </div>
                    </div>    
                </div> 


                <div class='col-sm-3'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Kelurahan/Desa</span>
                                <input type="hidden" name="salok" class="form-control" />
                                <select name="salok_ddl" onchange="DDCsalok(this);" class="form-control psalok" style="width: 100%" />
                                    <option value=""></option>
                                    <?php foreach($dsalok as $t):?>
                                    <option value="<?php echo $t->salok; ?>"><?php echo $t->salok; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Kelurahan/Desa Lainnya...</option>
                                </select> <input type="text" placeholder="isi kelurahan/desa lainnya disini" name="salok_txt" style="display: none;" class="form-control"/>
                        </div> 
                    </div>
                </div>

                <div class='col-sm-3'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Kecamatan</span>
                                <input type="hidden" name="talok" class="form-control" />
                                <select name="talok_ddl" onchange="DDCtalok(this);" class="form-control ptalok" style="width: 100%" />
                                    <option value=""></option>
                                    <?php foreach($dtalok as $t):?>
                                    <option value="<?php echo $t->talok; ?>"><?php echo $t->talok; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Kecamatan Lainnya...</option>
                                </select> <input type="text" placeholder="ketik Kecamatan lainnya disini" name="talok_txt" style="display: none;" class="form-control"/>
                        </div> 
                    </div>
                </div>

</div>

<div class='form-group row'> 
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Tikor Area</span>
                        <input type="text" name="tikor" placeholder="isi tikor disini" class="form-control" required/>
                </div>
            </div>    
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Homepass </span>
                        <input type="text" name="hplok" placeholder="isi homepass disini" class="form-control" required/>
                </div>
            </div>    
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Occupancy </span>
                        <input type="text" name="oclok" placeholder="isi occupancy disini" class="form-control" required/>
                </div>
            </div>    
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">PO </span>
                        <input type="text" name="polok" placeholder="isi PO disini" class="form-control" required/>
                </div>
            </div>    
        </div>
</div>

<div class='form-group row'> 
        <div class="col-md-12">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Lampiran</span>
                <input type="file" multiple id="attach" name="attach[]" class="form-control">
                            
                </div>
            </div>
            <small>format file : .pdf (maks. 2 MB)</small>
        </div>                            
</div>   

<div class='form-group row'> 
        <div class="col-md-12">
      <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-paper-plane-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>  
      </div>
</div>  
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('kerea',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>


    $(document).ready(function(){

        $(".deplak").select2();
        $(".prilok").select2();
        $(".psalok").select2();
        $(".ptalok").select2();

        $("#tanggal").datepicker({
            format: "yyyy-mm-dd"
        });

        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>


<script type="text/javascript">

function DDCsalok(oDDL) {
    var oTextbox = oDDL.form.elements["salok_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}
function DDCrilok(oDDLl) {
    var oTextboxl = oDDLl.form.elements["rilok_txt"];
    if (oTextboxl) {
        oTextboxl.style.display = (oDDLl.value == "") ? "" : "none";
        if (oDDLl.value == "")
            oTextboxl.focus();
    }
}

function DDCtalok(oDDLj) {
    var oTextboxj = oDDLj.form.elements["talok_txt"];
    if (oTextboxj) {
        oTextboxj.style.display = (oDDLj.value == "") ? "" : "none";
        if (oDDLj.value == "")
            oTextboxj.focus();
    }
}


function FormSubmit(oForm) {
    var oHidden = oForm.elements["salok"];
    var oDDL = oForm.elements["salok_ddl"];
    var oTextbox = oForm.elements["salok_txt"];

    var oHiddenl = oForm.elements["rilok"];
    var oDDLl = oForm.elements["rilok_ddl"];
    var oTextboxl = oForm.elements["rilok_txt"];

    var oHiddenj = oForm.elements["talok"];
    var oDDLj = oForm.elements["talok_ddl"];
    var oTextboxj = oForm.elements["talok_txt"];


    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;

    if (oHiddenl && oDDLl && oTextboxl)
        oHiddenl.value = (oDDLl.value == "") ? oTextboxl.value : oDDLl.value;

    if (oHiddenj && oDDLj && oTextboxj)
        oHiddenj.value = (oDDLj.value == "") ? oTextboxj.value : oDDLj.value;

}

</script>
