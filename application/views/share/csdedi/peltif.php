<?php

$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;

    $idwo=$this->db->order_by("id_worde","desc")->limit(1)->get("tp_owatad")->result();
        foreach($idwo as $dat): 
        $updidwor=$dat->tg_cre;   
        endforeach;
?>


    <?php if ($nmjab=='Sales Admin' || $nmjab=='Project Management Officer'){?>
        <form method=POST action="<?php echo base_url("panel/woimport/"); ?>">
        <div class='form-group row'>
          <label for='nowo' class='col-sm-2 form-control-label'>No. WO</label>
          <div class='col-sm-4'>
            <input type='text' height='100' name='nowo' class='form-control' id='nowo' placeholder='contoh : 190200022' required>
          </div>
            <button type='submit' class='col-sm-2 btn btn-primary'><i class='fa fa-download fa-1x'></i>&nbsp;<small>SAP IMPORT </small></button>&nbsp;&nbsp;
            <a type='button' href="<?php echo base_url("panel/konsaptes/"); ?>" class='btn btn-primary'><i class='fa fa-link fa-1x'></i>&nbsp;<small>CEK KONEKSI</small></sup></a>
        </div>

        </form>
     <?php }else{?>

     <?php }?>


    <br>
<?php if ($nmjab=='Project Management Officer'){?>
&nbsp;<a href="<?php echo base_url("panel/woeksport"); ?>" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg"></i> <br><small><sup>EXCEL</sup></small></a>
<?php }elseif ($nmjab=='Sales Admin'){?>
&nbsp;<a href="<?php echo base_url("panel/wobaru"); ?>" class="btn btn-primary"><i class="fa fa-file fa-lg"></i> <br><small><sup>BARU</sup></small></a><br>
<?php }else{}?>
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-3"><br>
        <?php 
            echo "<small style='color:white; background-color:black;'>&nbsp;Last update : ". date("d-M-y H:i:s", strtotime($updidwor))."&nbsp;</small>";
         ?></div>
        </div>
        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="nowo"  data-sortable="true">No. WO</th>
                        <th data-field="pela"  data-sortable="true">Pelanggan</th>
                        <th data-field="sale"  data-sortable="true">Sales</th>
                        <th data-field="laya"  data-sortable="true">Layanan</th>
                        <th data-field="kapa"  data-sortable="true">Kapasitas</th>
                        <th data-field="vend"  data-sortable="true">Vendor</th>
                        <th data-field="rfs"  data-sortable="true">RFS</th>
                        <th data-field="sta"  data-sortable="true">Status</th> 
                    <?php if($this->divku==23):?>
                    <?php else: ?>  
                        <th data-field="asig"  data-sortable="true"><?php if($this->session->userdata('id_jabatan')==22){echo 'BAA by';}else{echo 'Taken by';} ?></th> 
                    <?php endif;?>                                
                        <th>Opsi</th>
                   
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        
                        if ($nmjab=='Sales Admin'){
                            // $daftar=$data_wopil; old data akses
                            $daftar=$datait_pe;
                        }else{
                            $daftar=$datait_pe;
                        }

                        foreach($daftar as $datwo): ?>
                        <tr>
                            <td><?php echo $datwo->id_worde; ?><?php echo ($datwo->rev > 0) ? "<small style='color:blue;'><sup><b>&nbsp;$datwo->rev</sup></b></small>":""; ?></td>
                            <td><?php echo $datwo->nm_custo; ?><br>
								<?php if($datwo->filebaa==''){ 

	                                }else{ 

	                            if($datwo->filebaa != null):
	                            $fibaa= json_decode($datwo->filebaa);
	                            ?>
	                                
	                                <?php foreach($fibaa as $item):
	                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
	                                ?>
	                                 <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
	                                <a href="<?php echo base_url("assets/uploads/baa/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><small><?php $ij=explode(' ',$item->judul); echo $ij[0].'...'; ?></a> &nbsp;&nbsp;<?php $nb=explode(' ',$datwo->nama_baa); echo 'by <b>'.$nb[0].'</b>'; ?></small><br>
	                                <?php endforeach;  ?> 
	                            
	                            <?php endif; }?>
                                <?php if($datwo->filebap==''){ 

                                    }else{ 

                                if($datwo->filebap != null):
                                $fibap= json_decode($datwo->filebap);
                                ?>
                                    
                                    <?php foreach($fibap as $item):
                                    $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                    ?>
                                     <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                    <a href="<?php echo base_url("assets/uploads/bap/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><small><?php $ij=explode(' ',$item->judul); echo $ij[0].'...'; ?></a> &nbsp;&nbsp;<?php $nb=explode(' ',$datwo->nausbap); echo 'by <b>'.$nb[0].'</b>'; ?></small><br>
                                    <?php endforeach;  ?> 
                                
                                <?php endif; }?>
                                <?php if($datwo->cid != null){echo '<br><small>CID/SID : '.$datwo->cid.'/'.$datwo->no_baa.'</small>';}else{echo '';}?>
                            </td>
                            <td><?php echo $datwo->nm_sales; ?></td>
                            <td><?php echo $datwo->sv_custo; ?></td>
                            <td><?php echo $datwo->ca_custo; ?></td>
                            <td><?php echo $datwo->nm_ven; ?></td>
                            <td><?php echo '<small>'.tgl_indome($datwo->tg_erefs).'</small>'; ?></td>

                            <td class="text-center"><?php if($datwo->sta==0){echo "<small><i class='fa fa-circle-o' style='color:red'></i> <br>open</small>";}else{ if($datwo->dan==1){echo "<small><i class='fa fa-check-circle' style='color:green'></i><br> closed</small>";}else{if($datwo->can==1){echo "<small><i class='fa fa-times-circle' style='color:red'></i><br> canceled</small>";}else{echo "<small><i class='fa fa-check-circle' style='color:orange'></i><br> on progress</small>";} }}?></td>
                    <?php if($this->divku==23):?>
                    <?php else: ?> 
                            <td><?php if($this->session->userdata('id_jabatan')==22){echo $datwo->nama_baa;}else{echo $datwo->nama_lengkap;} ?></td>
                            <?php endif; ?>
                            <td>
                              <center>
                              <?php if ( $nmjab=='Project Management Officer'){?>
                                <?php if($datwo->dan==1): ?>
                                      <a href="<?php echo base_url("panel/womoliat/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Lihat'><i class="fa fa-eye fa-lg"></i></a>
                                <?php elseif($datwo->can==1): ?>
                                      <a href="<?php echo base_url("panel/womoedit/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Isi'><i class="fa fa-plus fa-lg"></i></a>
                                <?php else :?>     
                                <a href="<?php echo base_url("panel/womoedit/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Isi'><i class="fa fa-plus fa-lg"></i></a>
                                <a href="<?php echo base_url("panel/womocomp/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Completed'><i class="fa fa-check-circle fa-lg text-success"></i></a>
                                <?php endif; ?>
                                
                                	<?php if ($nmjab=='Project Management Officer'){
                                            if ($datwo->us_assto==null){
                                        ?>
                                		<!--<a id="alokasi" href="<?php echo base_url("panel/womoalok/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Assign To'><i class="fa fa-external-link fa-lg"></i></a>
                                		<a id="alokasi" href="" data-toggle='tooltip' title='Assign To'><i class="fa fa-external-link fa-lg"></i></a>-->
                                		
                                	<?php }else{}}?>

                             <?php }elseif ($nmjab=='Network Engineer'){?>
                                <?php if(empty($datwo->us_assto)): ?>
                                <a href="<?php echo base_url("panel/topotamb/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Tambah Topology'><i class="fa fa-connectdevelop fa-lg"></i></a>

                                <?php else :
                                    if($datwo->us_assto==$this->idaku || $this->idaku==211 ): ?>

                                    <a href="<?php echo base_url("panel/topotamb/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Edit Topology'><i class="fa fa-pencil-square fa-lg"></i></a>

                                    <?php endif; ?>

                                <a href="<?php echo base_url("panel/womoliat/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Lihat'><i class="fa fa-eye fa-lg"></i></a> 
                               
                                <?php endif; ?>
                             <?php }elseif ($nmjab=='FOC Admin'){?>

                                <?php if(is_null($datwo->nama_baa)): ?>
                                    <a href="<?php echo base_url("panel/baatamb/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Upload BAA'><i class="fa fa-clipboard fa-lg"></i></a>
                                    
                                <?php else :?> 
                                    <a href="<?php echo base_url("panel/womoliat/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Lihat'><i class="fa fa-eye fa-lg"></i></a>

                                <?php endif; ?>
                              
                              <?php }elseif ($nmjab=='Sales Admin'){?>

                                <?php if($datwo->dan==1): ?>
                                    <a href="<?php echo base_url("panel/womoliat/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Detil'><i class="fa fa-eye fa-lg"></i></a>
                                    <a href="<?php echo base_url("panel/womorevi/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Revisi'><i class="fa fa-pencil-square fa-lg"></i></a>

                                <?php else :?>     
                                     <a href="<?php echo base_url("panel/womorevi/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Revisi'><i class="fa fa-pencil-square fa-lg"></i></a>
                                    <a href="<?php echo base_url("panel/baptamb/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Upload BAP'><i class="fa fa-clipboard fa-lg"></i></a>
                                     <a href="<?php echo base_url("panel/womoliat/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Detil'><i class="fa fa-eye fa-lg"></i></a>
                                <?php endif; ?>

                                <?php if(empty($datwo->ph_ven)): ?>
                                    <a href="<?php echo base_url("panel/tarsap/".$datwo->id_worde."/".$this->session->userdata('id_pengguna')); ?>" data-toggle='tooltip' title='Tarik CID'><i class="fa fa-refresh fa-lg"></i></a>
                                <?php endif;?>  

                              <?php }elseif ($nmjab=='CRO'){?>  
                                
                                <a href="<?php echo base_url("panel/womoliat/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Detil'><i class="fa fa-eye fa-lg"></i></a>
                                <?php if($datwo->softblok==1): ?>
                                <a href="<?php echo base_url("panel/wosofblo/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Softblock'><i class="fa fa-ban fa-lg"></i></a>
                                <?php endif; ?>
                              <?php }else{?>

                                <a href="<?php echo base_url("panel/womoliat/".$datwo->id_worde); ?>" data-toggle='tooltip' title='Detil'><i class="fa fa-eye fa-lg"></i></a>
                              <?php }?>

                              </center>
                            </td>
                            
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>