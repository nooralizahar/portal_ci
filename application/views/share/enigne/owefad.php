        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="f"  data-sortable="true">EWO #</th>
                        <th data-field="g"  data-sortable="true">Area</th>
                        <th data-field="i"  data-sortable="true">ODC</th>
                        <th data-field="a"  data-sortable="true">OTB</th>
                        <th data-field="b"  data-sortable="true">JCL</th>            
                        <th data-field="h"  data-sortable="true">Opsi</th> 
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafewo as $e): ?>
                        <tr>
                            <td><?php echo $e->ideo; ?></td>
                            <td><?php echo $e->area; ?></td>
                            <td><?php echo $e->nodc.'&rarr;'.$e->kodc; ?></td>
                            <td><?php echo $e->notb.'port :'.$e->potb; ?></td>
                            <td><?php echo $e->njcl.'port :'.$e->pjcl; ?><br><?php echo 'ka :'.$e->kabl.' tu :'.$e->tube.' co :'.$e->tube; ?></td>
                            <td class="text-center"><?php //if($this->session->userdata('id_jab')==4): ?>
                                    <a href="<?php echo base_url($this->uriku."/ewohap/".$e->ideo); ?>" data-toggle='tooltip' title='Hapus EWO'><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;
                                <?php //endif;?>
                            </td>


                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table> 