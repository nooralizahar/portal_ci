        <form class='add-item' method=POST action=''>
<center><h4>DATA ODC</h4></center>
 
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Area&nbsp; &nbsp;</span>
               
                    <input type='text' name='area' class='form-control' id='area' > 
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Tanggal&nbsp; &nbsp;</span>
               
                    <input type='date' name='tewo' class='form-control' id='tewo' > 
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Nama &nbsp; &nbsp;</span>
                    <select name="nodc" id="podc" class="form-control odcpil" style="width:100%;" onchange="OCodc(this.value)">
                                <option value=""></option>
                                <?php foreach($odcsem as $w):?>
                                    <option value="<?php echo $w->idodc.'|'.$w->kordinat; ?>"><?php echo $w->nama." - ".$w->alamat; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
            </div>    
        </div>  
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Kordinat&nbsp; &nbsp;</span>
                    <input type='hidden' name='nodc' class='form-control' id='nodc' readonly/> 
                    <input type='text' name='kodc' class='form-control' id='kodc' readonly/> 
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">POP</span>
                    <input type='text' name='npop' class='form-control' id='npop'>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">OLT PORT</span>
                       <input type='text' name='polt' class='form-control' id='polt'>
                </div>
            </div>
        </div>
<center><h4>DATA OTB & CORE</h4></center>
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">OTB</span>
                    <select name="notb" id="notb" class="form-control otbpil" style="width:100%;">
                                <option value=""></option>
                                <?php foreach($otbsem as $o):?>
                                    <option value="<?php echo $o->id_otb; ?>"><?php echo $o->nm_otb; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
            </div>
        </div> 
  
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">PORT</span>
                    <input type='text' name='potb' class='form-control' id='potb'>
                </div>
            </div>
        </div> 

<center><h4>DATA JOIN CLOSURE & CORE</h4></center>
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">JC</span>
                    <select name="njcl" id="njcl" class="form-control otbpil" style="width:100%;">
                                <option value=""></option>
                                <?php foreach($jclsem as $j):?>
                                    <option value="<?php echo $j->idjc; ?>"><?php echo $j->jocl; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
            </div>
        </div> 
  
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">PORT</span>
                    <input type='text' name='pjcl' class='form-control' id='pjcl'>
                </div>
            </div>
        </div> 
        <div class="col-md-4">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Kabel</span>
                    <input type='text' name='kabl' class='form-control' id='kabl'>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Tube</span>
                       <input type='text' name='tube' class='form-control' id='tubr'>
                </div>
            </div>
        </div>
   
        <div class="col-md-4">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Core</span>
                       <input type='text' name='core' class='form-control' id='core'>
                </div>
            </div>
        </div>

        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="window.history.back()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button> <!-- onclick="window.location.href = '<?php //echo base_url('cidsid/') ?>';" -->
            <a href="<?php echo base_url($this->uriku."/ewoexp"); ?>" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg"></i> <br><small><sup>EXCEL</sup></small></a>
            </div>
        </div>
                
        </form>