
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url($this->uriku."/"); ?>"> Dashboard</a></li>
            <li class="active"> Alamat Pole</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Form Isian Alamat Pole</div>
                    <?php echo $this->session->flashdata("pesan");?>
                <div class="panel-body">

    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">


        <div class="panel"> <!-- awal panel -->

                    <div class="tabbable-panel">
                        <div class="tabbable-line">

                        <ul class="nav nav-tabs" role="tablist">
                            <?php if($this->divku == 20):?>
                            <li role="presentation"><a href="#tabnol" aria-controls="tabnol" role="tab" data-toggle="tab"> Baru</a></li>
                            <?php endif; ?>
                            <li role="presentation" class="active"><a href="#tabatu" aria-controls="tabatu" role="tab" data-toggle="tab"> Daftar</a></li>
                        </ul>



<div class="tab-content">

  <div role="tabpanel" class="tab-pane" id="tabnol">
     <div class="panel-body">
         <div class="panel panel-primary">
           <div class="panel-body">

            <?php $this->load->view("/share/tuolor/rabait")?>
           </div>
         </div>
     </div>
  
  </div>


<div role="tabpanel" class="tab-pane active" id="tabatu">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">

            <?php $this->load->view("/share/tuolor/litait")?>

                </div>
            </div>
        </div>
</div>
    

    </div>
  </div>
 </div>
</div> <!-- akhir panel -->



            </div>
        </div>
    </div>  


                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->


