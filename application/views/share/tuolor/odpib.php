<?php
    $thnini= date('Y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    if(substr($thnbli, 2)==01){$bln=12;}else{$bln='0'.substr($thnbli, 2)-1;}
    $thnblu= ((substr($thnbli,0, 2))-1).$bln;
    $blnini=date('Y/m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 
    $dver=$this->db->where("ver",1)->get("t_odp")->num_rows();
    $dunv=$this->db->where("ver",0)->get("t_odp")->num_rows();

        $tik=$this->db->order_by("idodp","desc")->limit(1)->get("t_odp")->result();
        foreach($tik as $d): 
        $lastik=$d->tg_cre;   
        endforeach;

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <!--<li><a href="<?php //echo base_url("focmen/"); ?>"> Menu ODP</a></li>-->
            <li class="active"> Daftar ODP</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <!--<h5> Masukan Data ODP</h5>-->
                </div>
        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                /*if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }*/
                ?>
                <div id="collapseOne" class="<?php // echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <!--<li role="presentation"><a href="#requester" aria-controls="requester" role="tab" data-toggle="tab"> ODP Baru</a></li>-->
                            <li role="presentation" class="active"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Daftar ODP iBoss</a></li>
                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane" id="requester">
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">
        <form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Nama / Kode ODP</label>
                <div class='col-sm-6'>
                    <input type='text' name='nama' class='form-control' id='nama' required>
                    
                </div>
                <label for='lblstatus' class='col-sm-1 form-control-label'>Status</label>

                <div class='col-sm-3'>
                    <input type="radio" name="status" value="0" checked>
                            <label for="InActive" style="text-decoration: none;">&nbsp;&nbsp;&nbsp;InActive&nbsp;&nbsp;&nbsp;</label>
                            <input type="radio" name="status" value="1"> <label for="Active" style="text-decoration: none;">&nbsp;&nbsp;&nbsp;Active&nbsp;&nbsp;&nbsp;</label>
                    
                </div>
          </div>
        <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Lokasi </label>
                <div class='col-sm-5'>
                    <select name="lokasi" id="lokasi" class="form-control odp">
                                <option value=""></option>
                                <?php foreach($daflok as $l):?>
                                    <option value="<?php echo $l->t_location_id; ?>"><?php echo $l->area_name." - ".$l->name; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
          </div>   
        <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Alamat </label>
                <div class='col-sm-10'>
                    <input type='text' name='alamat' class='form-control' id='alamat' required>
                </div>
        </div> 
        <div class='form-group row'>
                <label for='lblkordinat' class='col-sm-2 form-control-label'>Kordinat</label>
                <div class='col-sm-10'>
                      <input type='text' name='kordinat' class='form-control' id='kordinat' required>
                </div>
                
        </div>    
        <div class='form-group row'>
                <label for='lblkapasitas' class='col-sm-2 form-control-label'>Kapasitas</label>
                <div class='col-sm-10'>
                       <input type='text' name='kapasitas' class='form-control' id='kapasitas' required>
                </div>
        </div>

        <div class='form-group row'>
                <label for='lblkapasitas' class='col-sm-2 form-control-label'>Relasi OLT</label>
                <div class='col-sm-8'>
                    <select name="oltrel" id="oltrel" class="form-control">
                                <option value=""></option>
                                <?php foreach($dafolt as $a):?>
                                    <option value="<?php echo $a->idolt; ?>">

                                        <?php echo $a->nmolt."&nbsp;&rarr; slot : ".$a->noslot.", port : ".$a->noport; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
                <div class='col-sm-2'>
                    <a href="<?php echo base_url("focmen/oltin/"); ?>" class="btn btn-success form-control" ><i class="fa fa-plus-square-o"></i> OLT</a>
                </div>
        </div>
   
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('focmen/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
             </div>
         </div>
     </div>
</div>


<div role="tabpanel" class="tab-pane active" id="problem">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"><?php 
           // echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-2">  </div>
        </div>
        <hr/>

</div> <br>
        <form class='add-item' target="_blank" method=POST action='<?php echo base_url("focmen/jaodp/"); ?>'>
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Pilih Berdasarkan</label>
                <div class='col-sm-2'>
                    <select name="pilih" class="form-control">
                        <option value="code" selected=""> Kode </option>
                        <option value="location_name"> Lokasi</option>
                    </select>
                </div>
                <div class='col-sm-2'>
                    <input type='text' name='keyword' class='form-control' id='keyword' required>
                </div>

                <div class='col-sm-2'>
                    <button class="btn btn-success form-control" type="submit"><i class="fa fa-map-marker"></i> Peta</button>
                </div>
        </div> 
        </form>
   <!-- <br>
    <h4 ><div style="color: green">Verified : <?= $dver." (".number_format(($dver/($dver+$dunv))*100)." %)" ?></div><div style="color: red"> Unverified : <?= $dunv." (".number_format(($dunv/($dver+$dunv))*100)." %)" ?> </div></h4>  data-sort-name="name" data-sort-order="desc"-->
    <br>

        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true">
                    <thead>
                    <tr>
                        <th data-field="i"  data-sortable="true">ID</th>
                        <th data-field="a"  data-sortable="true">Kode</th>
                        <th data-field="b"  data-sortable="true">Lokasi (iBoss)</th>
                        <th data-field="c"  data-sortable="true">Alamat</th>
                        <th data-field="d"  data-sortable="true">Koordinat</th>
                        <th data-field="e"  data-sortable="true">Kapasitas</th>
                        <th data-field="f"  data-sortable="true">Pelanggan</th>
                        <th data-field="g"  data-sortable="true">Sisa</th>
                        <th data-field="h"  data-sortable="true">Tgl. Input</th>
                        <th data-field="j"  data-sortable="true">Input by</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($odpsem as $dat): ?>
                        <tr>
                            <td><?php echo $dat->t_net_odp_id; ?></td>
                            <td><?php echo $dat->code; ?></td>
                            <td><?php echo $dat->location_name; ?></td>
                            <td><?php echo $dat->address; ?></td>
                            <td><a href="<?php echo base_url("focmen/peta/".$dat->t_net_odp_id); ?>" target='_blank'><?php echo $dat->coordinate; ?></a></td>
                            <td class="text-right"><?php echo $dat->capacity; ?></td>
                            <td class="text-right"><a href="<?php echo base_url("focmen/odpdet/".$dat->t_net_odp_id); ?>"><?php $i=isiodp($dat->t_net_odp_id); echo $i; ?></a></td>
                            <td class="text-right"><?php echo ($dat->capacity-$i).'&nbsp;<small><sup>('.number_format((($dat->capacity-$i)/$dat->capacity)*100,0).'%)</sup></small>';?></td>
                            <td><?php echo $dat->create_date; ?></td>
                            <td><?php echo $dat->creaby; ?></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

