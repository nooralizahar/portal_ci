<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url($this->uriku."/otblis/"); ?>"> OTB</a></li>
            <li class="active"> Lokasi</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Masukan Data Lokasi</h5>
                </div>
        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#lokbar" aria-controls="lokbar" role="tab" data-toggle="tab"> Lokasi Baru</a></li>
                            <li role="presentation" class="active"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Daftar Lokasi</a></li>
                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane" id="lokbar">
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">
        <form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Nama Lokasi</label>
                <div class='col-sm-10'>
                    <input type='text' name='nm_lok' class='form-control' id='nm_lok' required>
                    
                </div>
                

          </div>  
        <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Alamat</label>
                <div class='col-sm-10'>
                       <input type='text' name='al_lok' class='form-control' id='al_lok' required>
                </div>
        </div> 
        <div class='form-group row'>
                <label for='lblkordinat' class='col-sm-2 form-control-label'>Kordinat</label>
                <div class='col-sm-10'>
                      <input type='text' name='ti_kor' class='form-control' id='ti_kor' required>
                </div>
                
        </div>    


        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('otbmen/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
             </div>
         </div>
     </div>
</div>


<div role="tabpanel" class="tab-pane active" id="problem">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"><?php 
            //echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-2">  </div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-jingga panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_isakol")->num_rows()); ?></a></div>
                        <div class="text-muted" > Lokasi</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->where("nm_otb like","OTB%")->get("t_bto")->num_rows()); ?></div>
                        <div class="text-muted"> OTB</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked chain"><use xlink:href="#stroked-chain"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->where("nm_otb like","JB%")->get("t_bto")->num_rows()); ?></div>
                        <div class="text-muted"> Join Box</div>
                    </div>
                </div>
            </div>
        </div>

</div> <br>

    <br>



        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true">ID#</th>
                        <th data-field="b"  data-sortable="true">Lokasi</th>
                        <th data-field="c"  data-sortable="true">Alamat</th>
                        <th data-field="d"  data-sortable="true">Tikor</th>
                        <th data-field="e"  data-sortable="true">Opsi</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($otblosem as $dat): ?>
                        <tr>
                            <td><?php echo $dat->id_lok; ?></td>
                            <td><?php echo $dat->nm_lok; ?></td>
                            <td><?php echo $dat->al_lok; ?></td>
                            <td><a href="<?php //echo base_url("otbmen/peta/".$dat->idodp); ?>" target='_blank'><?php echo $dat->ti_kor; ?></a></td>
                            <td><center>
                                <a href="<?php echo base_url($this->uriku."/loked/".$dat->id_lok); ?>" data-toggle='tooltip' title='Ubah Lokasi'><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp; <!-- Edit Pengguna -->
                               <!--  <a href="<?php echo base_url($this->uriku."/otbli/".$dat->id_lok); ?>" data-toggle='tooltip' title='Tambah OTB'><i class="fa fa-plus fa-lg"></i></a>&nbsp;&nbsp; Edit Pengguna -->
                                <?php if($this->session->userdata('level')=='admin'): ?>
                                	<a href="<?php echo base_url($this->uriku."/lokha/".$dat->id_lok); ?>" data-toggle='tooltip' title='Hapus ODP'><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;
                                <?php endif;?>

                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

