<form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Nama / Kode ODC</label>
                <div class='col-sm-6'>
                    <input type='text' name='nama' class='form-control' id='nama' required>
                    
                </div>


          </div>
        <div class='form-group row'>
            
                <label for='lblalamat' class='col-sm-2 form-control-label'>Lokasi </label>
                <div class='col-sm-5'>
               
                    <select name="lokasi" id="lokasi" class="form-control lokpil" style="width:90%;">
                                <?php foreach($daflok as $l):?>
                                    <option value="<?php echo $l->t_location_id; ?>"><?php echo $l->area_name." - ".$l->name; ?></option>
                                <?php endforeach;?>
                    </select>
              
            </div>
          </div>   
        <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Alamat </label>
                <div class='col-sm-10'>
                    <input type='text' name='alamat' class='form-control' id='alamat' required>
                </div>
        </div> 
        <div class='form-group row'>
                <label for='lblkordinat' class='col-sm-2 form-control-label'>Kordinat</label>
                <div class='col-sm-10'>
                      <input type='text' name='kordinat' class='form-control' id='kordinat' required>
                </div>
                
        </div>    
        <div class='form-group row'>
                <label for='lblkapasitas' class='col-sm-2 form-control-label'>Kapasitas</label>
                <div class='col-sm-10'>
                       <input type='text' name='kapasitas' class='form-control' id='kapasitas' required>
                </div>
        </div>
   
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('focmen/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('focmen/odpdlo') ?>';" class='btn btn-primary'><i class='fa fa-download left fa-lg'></i><br> <sup><small>ODP DOWNLOAD</small></sup></button>
            </div>
        </div>
                
</form>