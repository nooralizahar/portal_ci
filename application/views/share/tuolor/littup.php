<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url($this->uriku."/"); ?>"> Dashboard</a></li>
            <li class="active"> Alamat Pole</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
 <?php if($this->divku == 20):?>
                <div class="panel-heading"> Detil Alamat Pole # <?=$al?></div>
                    <?php echo $this->session->flashdata("pesan");?>
                <div class="panel-body">
 


        <form class='add-item' method=POST action='' onsubmit="FormSubmit(this);" enctype="multipart/form-data">
         <div class='form-group row'>
                <label for='lblno' class='col-sm-1 form-control-label'>Segment</label>
                <div class='col-sm-3'>
                                <input type="hidden" name="tiseg" class="form-control" />
                                <select name="tiseg_ddl" onchange="DDCtiseg(this);" class="form-control ptiseg" style="width: 100%" />
                                    <option value=""></option>
                                    <?php foreach($dafseg as $s):?>
                                    <option value="<?php echo $s->tiseg; ?>"><?php echo $s->tiseg; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Segment Lainnya...</option>
                                </select> <input type="text" placeholder="isi Segment lainnya disini" name="tiseg_txt" style="display: none;" class="form-control"/>
                                <input type='hidden' name='idala' class='form-control' id='idala' value="<?php echo $al; ?>" >
                </div>   
                <label for='lblno' class='col-sm-1 form-control-label'>Latitude </label>
                <div class='col-sm-3'>
                    <input type='text' name='tilat' class='form-control' id='tilat' placeholder="contoh : -6.453631" >
                    
                </div>     
                <label for='lblno' class='col-sm-1 form-control-label'>Longitude </label>
                <div class='col-sm-3'>
                    <input type='text' name='tilon' class='form-control' id='tilon' placeholder="contoh : 106.818428">
                    
                </div>         


        </div>
         <div class='form-group row'>
                <label for='lblno' class='col-sm-1 form-control-label'>Keterangan </label>
                <div class='col-sm-5'>
                                <input type="hidden" name="tiket" class="form-control" />
                                <select name="tiket_ddl" onchange="DDCtiket(this);" class="form-control ptiket" style="width: 100%" />
                                    <option value=""></option>
                                    <?php foreach($dafket as $t):?>
                                    <option value="<?php echo $t->tiket; ?>"><?php echo $t->tiket; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Ket. Lainnya...</option>
                                </select> <input type="text" placeholder="ketik Ket. lainnya disini" name="tiket_txt" style="display: none;" class="form-control"/>
                </div>
                <label for='lblno' class='col-sm-2 form-control-label'>ODC / ODP </label>
                <div class='col-sm-4'>
                                <input type="hidden" name="odcdp" class="form-control" />
                                <select name="odcdp_ddl" onchange="DDCodcdp(this);" class="form-control podcdp" style="width: 100%" />
                                    <option value=""></option>
                                    <?php foreach($dafdcp as $p):?>
                                    <option value="<?php echo $p->id; ?>"><?php echo $p->odcp; ?></option>
                                    <?php endforeach;?>
                                    <option value="">ODC / ODP Lainnya...</option>
                                </select> <input type="text" placeholder="ketik Ket. lainnya disini" name="odcdp_txt" style="display: none;" class="form-control"/>
                </div>

         </div>

        <div class='form-group row'>
                <label for='lblno' class='col-sm-1 form-control-label'>Foto</label>
                <div class='col-sm-11'>
                    <input type="file" multiple id="attach" name="attach[]" class="form-control"> <small>format file : *.jpg atau *.png. <br></small><br />
                </div>
        </div>

        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button> <a href="/nuber/export_tiang/<?=$al?>" class='btn btn-primary'><i class='fa fa-file-excel-o fa-lg'></i><br> <sup><small>EXCEL</small></sup></a>
               <!-- <button type='button' onclick="window.history.back()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>-->
            </div>
        </div>
                
        </form>

<?php else :?>

<br>
<h4>&nbsp;&nbsp;Data Alamat Pole # <?=$al?></h4>
<?php endif;?>


        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true" class="text-center">ID#</th>
                        <th data-field="b"  data-sortable="true" class="text-center">Segment</th>            
                        <th data-field="c"  data-sortable="true" class="text-center">Latiitude</th>
                        <th data-field="d"  data-sortable="true" class="text-center">Longitude</th>
                        <th data-field="e"  data-sortable="true" class="text-center">Foto</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Ket.</th>
                        <th data-field="g"  data-sortable="true" class="text-center">ODC / ODP</th>
                        <th data-field="h"  data-sortable="true" class="text-center">Update</th>
                        <th data-field="i"  data-sortable="true" class="text-center">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($daftil as $t): ?>
                        <tr>
                            <td class="text-right"><?php echo $t->idtia; ?></td>
                            <td><?php echo $t->tiseg; ?></td>
                            <td class="text-center"><?php echo $t->tilat; ?></td>
                            <td class="text-center"><?php echo $t->tilon; ?></td>
                            <td class="text-center">
                                    <?php if($t->tfoto==''){ 

                                    }else{ 

                                        if($t->tfoto != null):
                                        $l = json_decode($t->tfoto);
                                        ?>
                                            <!-- <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>-->
                                            <?php foreach($l as $item):
                                            $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                            ?>

                                            <!-- Preview Lampiran Biasa -->
                                            <a href="<?php echo base_url("assets/uploads/netdev/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><?php echo "<img src=".base_url("assets/uploads/netdev/".$item->file)." width='31' height='31'><br><small>".substr($item->judul,0,5)."...".substr($item->judul,-3,3); ?></a> 

                                            <!-- Lihat Lampiran dalam Halaman 
                                            <a href="<?php echo base_url($this->uriku."/hatran/".$t->idtia); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->file; ?>"><?php echo $item->judul; ?></a>-->

                                            <?php endforeach; ?> 
                                        
                                         <?php endif;?>

                                 <?php  } ?>
                                
                            </td>
                            <td class="text-center"><?php echo $t->tiket; ?></td>
                            <td class="text-center"><?php echo $t->odcp; ?></td>
                            <td class="text-center"><?php echo "<small>".$t->staf."<br>".$t->tgtia."</small>"; ?></td>
                           <td class="text-center">
                            <?php if($this->divku == 20):?>
                                <?php if($t->ustia==$this->idaku):?>
                                    <a href="<?php echo base_url($this->uriku."/pustia/".$al."/".$t->idtia); ?>" data-toggle='tooltip' title='Anda yakin akan menghapus data ini?'><i class="fa fa-trash-o fa-lg"></i></a>&nbsp;&nbsp;

                                <?php else :?>
                                <?php endif;?>
                            
                            <?php else :?>
                            
                            <?php endif;?>                                
 
                            </td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                </div> <!--/.panel body-->
            </div>
        </div>
    </div><!--/.row-->

</div>  <!--/.main-->


<script>

    $(document).ready(function(){

            $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});

            $(".ptiseg").select2();
            $(".ptiket").select2();
            $(".podcdp").select2();

    })
</script>
<script type="text/javascript">

function DDCtiket(oDDL) {
    var oTextbox = oDDL.form.elements["tiket_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}
function DDCtiseg(oDDLl) {
    var oTextboxl = oDDLl.form.elements["tiseg_txt"];
    if (oTextboxl) {
        oTextboxl.style.display = (oDDLl.value == "") ? "" : "none";
        if (oDDLl.value == "")
            oTextboxl.focus();
    }
}

function DDCodcdp(oDDLj) {
    var oTextboxj = oDDLj.form.elements["odcdp_txt"];
    if (oTextboxj) {
        oTextboxj.style.display = (oDDLj.value == "") ? "" : "none";
        if (oDDLj.value == "")
            oTextboxj.focus();
    }
}


function FormSubmit(oForm) {
    var oHidden = oForm.elements["tiket"];
    var oDDL = oForm.elements["tiket_ddl"];
    var oTextbox = oForm.elements["tiket_txt"];

    var oHiddenl = oForm.elements["tiseg"];
    var oDDLl = oForm.elements["tiseg_ddl"];
    var oTextboxl = oForm.elements["tiseg_txt"];

    var oHiddenj = oForm.elements["odcdp"];
    var oDDLj = oForm.elements["odcdp_ddl"];
    var oTextboxj = oForm.elements["odcdp_txt"];


    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;

    if (oHiddenl && oDDLl && oTextboxl)
        oHiddenl.value = (oDDLl.value == "") ? oTextboxl.value : oDDLl.value;

    if (oHiddenj && oDDLj && oTextboxj)
        oHiddenj.value = (oDDLj.value == "") ? oTextboxj.value : oDDLj.value;

}

</script>
