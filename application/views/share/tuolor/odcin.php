<?php
    $thnini= date('Y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    if(substr($thnbli, 2)==01){$bln=12;}else{$bln='0'.substr($thnbli, 2)-1;}
    $thnblu= ((substr($thnbli,0, 2))-1).$bln;
    $blnini=date('Y/m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 


        $tik=$this->db->order_by("idodc","desc")->limit(1)->get("t_odc")->result();
        foreach($tik as $d): 
        $lastik=$d->tg_cre;   
        endforeach;

        if($tik){
            $vtgl=date("d-m-Y H:i:s", strtotime($lastik));
            
        }else{
            $vtgl=" tidak ada ";
        }

        $nkap=$this->db->select('SUM(kapasitas) as kapasitas, SUM(sisa) as sisa')->get("v_t_odc")->result();
        foreach ($nkap as $n) {
           $kap=$n->kapasitas;
           $sis=$n->sisa;
        }

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url($this->uriku."/"); ?>"> Menu </a></li>
            <li class="active"> Data ODC</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Masukan Data ODC</h5>
                </div>
        <div class="panel-body">
    
    <div class="row"> 

                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#odcbaru" aria-controls="odcbaru" role="tab" data-toggle="tab"> ODC Baru</a></li>
                            <li role="presentation"><a href="#daftarolt" aria-controls="daftarolt" role="tab" data-toggle="tab">Daftar OLT</a></li>
                            <li role="presentation" class="active"><a href="#daftarodc" aria-controls="daftarodc" role="tab" data-toggle="tab">Daftar ODC</a></li>
                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["dsuc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Sinkronisasi data ODP berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["derr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Sinkronisasi data ODP gagal.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">

    <div role="tabpanel" class="tab-pane" id="odcbaru">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                
                <?php $this->load->view("share/tuolor/odcina")?>  
    
                </div>
            </div>
        </div>
    </div>

<!-- TAB awal daftar OLT --->

<div role="tabpanel" class="tab-pane" id="daftarolt">
    <h3>DAFTAR OLT</h3>
    <center><h5 style="color: red"><b>Data OLT diambil dari <a href="https://app.bnetfit.com" target="_blank">Aplikasi iBoss</a></b></h5></center>

    <button type='button' onclick="window.location.href = '<?php echo base_url('focmen/oltdlo') ?>';" class='btn btn-primary'><i class='fa fa-download left fa-lg'></i><br> <sup><small>SYNC OLT</small></sup></button>
        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="i"  data-sortable="true">Detil OLT</th>
                        <th data-field="a"  data-sortable="true">IP Address</th>
                        <th data-field="b"  data-sortable="true">Slot/Port</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafolt as $dat): ?>
                        <tr>
                            <td><a href="<?php echo base_url("focmen/oltmap/".$dat->olt_detil); ?>" data-toggle='tooltip' title='Peta'><?php echo $dat->olt_detil; ?></a></td>
                            <td><?php echo $dat->olt_ip; ?></td>
                            <td><?php echo $dat->slotport; ?></td>


                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


</div>

<!-- TAB akhir daftar OLT --->

<!-- TAB awal daftar ODC --->


<div role="tabpanel" class="tab-pane active" id="daftarodc">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"><?php 
            echo "<small>Data Terakhir : ".$vtgl."</small>";
         ?></a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-2">  </div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_odc")->num_rows()); ?></a></div>
                        <div class="text-muted" > ODC</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked upload"><use xlink:href="#stroked-upload"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($kap); ?></div>
                        <div class="text-muted"> Kapasitas</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked download"><use xlink:href="#stroked-download"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($sis); ?></div>
                        <div class="text-muted"> Sisa</div>
                    </div>
                </div>
            </div>
        </div>






</div> <br>
        <form class='add-item' target="_blank" method=POST action='<?php echo base_url("focmen/jaodc/"); ?>'>
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Pilih Berdasarkan</label>
                <div class='col-sm-2'>
                    <select name="pilih" class="form-control">
                        <option value="nama" selected=""> Kode / Nama </option>
                        <option value="alamat"> Alamat</option>
                    </select>
                </div>
                <div class='col-sm-2'>
                    <input type='text' name='keyword' class='form-control' id='keyword' required>
                </div>

                <div class='col-sm-2'>
                    <button class="btn btn-success form-control" type="submit"><i class="fa fa-map-marker"></i> Peta</button>
                    
                </div>
        </div> 
        </form>
    <br>


        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    
                    <tr>
                        <th data-field="i"  data-sortable="true">ID</th>
                        <th data-field="a"  data-sortable="true" width="5%">Nama</th>
                        <th data-field="b"  data-sortable="true"width="5%">Lokasi <small><sup>iBoss</sup></small></th>
                        <th data-field="c"  data-sortable="true">Alamat</th>
                        <th data-field="d"  data-sortable="true">Koordinat</th>
                        <th data-field="e"  data-sortable="true">Kap.<br>port</th>
                        
                        <th data-field="f"  data-sortable="true">ODP</th>
                        <th data-field="g"  data-sortable="true">OLT</th>

                        <th data-field="h"  data-sortable="true">Sisa<br>port</th> 
                        <th data-field="j"  data-sortable="true">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($odcsem as $dat): ?>
                        <tr>
                            <td><?php echo $dat->idodc; ?></td>
                            <td><a href="#" data-toggle='tooltip' title="<?php echo "Di buat : ".$dat->us_cre." tgl. ".$dat->tg_cre.".&#013; Di update : ".$dat->us_upd." tgl. ".$dat->tg_upd."."; ?>"><?php echo $dat->nama; ?></a>&nbsp;<a href="<?php echo base_url("focmen/odcol/".$dat->idodc); ?>" data-toggle='tooltip' title="Detil OLT"><i class="fa fa-plus-circle"></i></a>&nbsp;<a href="<?php echo base_url("focmen/odcmap/".$dat->idodc); ?>" data-toggle='tooltip' title="Detil ODC"><i class="fa fa-sitemap"></i></a></td>
                            <td><?php echo lokcek($dat->lokasi); ?></td>
                            <td><?php echo $dat->alamat; ?></td>
                            <td><?php echo $dat->kordinat; ?></td>
                            <td><?php echo $dat->kapasitas; ?></td>
                            <td><?php echo $dat->pakai; ?></td>
                            <td><?php echo $dat->olt; ?></td>
                            <td><?php echo $dat->sisa; ?></td>
                            <td><center>
                                <a href="<?php echo base_url("focmen/odced/".$dat->idodc); ?>" data-toggle='tooltip' title='Ubah'><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp; <!-- Edit ODC -->
                                <a href="<?php echo base_url("focmen/odcli/".$dat->idodc); ?>" data-toggle='tooltip' title='Detil'><i class="fa fa-plus fa-lg"></i></a>&nbsp;&nbsp; 
                                <?php if($this->session->userdata('level')=='admin'): ?>
                                	<!-- <a href="<?php //echo base_url("focmen/odcha/".$dat->idodc); ?>" data-toggle='tooltip' title='Hapus ODC'><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp; -->
                                <?php endif;?>
                               <!-- <?php if($dat->fileqr==null): ?>
                                <a href="<?php echo base_url("focmen/odcqr/".$dat->idodc); ?>" data-toggle='tooltip' title='Generate'><i class="fa fa-qrcode fa-lg"></i></a>
                                <?php else :?>
                                <a href="<?php echo base_url("focmen/odcce/".$dat->idodc); ?>" data-toggle='tooltip' title='Cetak' ><i class="fa fa-print fa-lg"></i></a>
                                <?php endif;?> -->
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>








    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

<script>


    $(document).ready(function(){


        $(".lokpil").select2();


    })
</script>  