<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url($this->uriku."/"); ?>"> Dashboard</a></li>
            <li class="active"> Kabel</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Data Kabel</div>
                    <?php echo $this->session->flashdata("pesan");?>
                <div class="panel-body">


        <form class='add-item' method=POST action='' onsubmit="FormSubmit(this);">
         <div class='form-group row'>
                <div class='col-sm-3'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Tanggal</span>
                    <input type='date' name='tgpas' class='form-control' id='tgpas' required/>
                        </div>
                    </div>
                </div>  
                <div class='col-sm-9'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Alamat</span>
                    <input type='text' name='jakab' class='form-control' id='jakab' required/>
                        </div>
                    </div>
                </div>   
        </div>
        <div class='form-group row'>        
                <div class='col-sm-3'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Segment</span>
                                <input type="hidden" name="sekab" class="form-control" />
                                <select name="sekab_ddl" onchange="DDCsekab(this);" class="form-control psekab" style="width: 100%" />
                                    <option value=""></option>
                                    <?php foreach($dafseg as $s):?>
                                    <option value="<?php echo $s->sekab; ?>"><?php echo $s->sekab; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Segment Lainnya...</option>
                                </select> <input type="text" placeholder="isi segment lainnya disini" name="sekab_txt" style="display: none;" class="form-control"/>

                        </div> 
                    </div>
                </div>   

                <div class='col-sm-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Jenis Kabel</span>
                                <input type="hidden" name="jekab" class="form-control" />
                                <select name="jekab_ddl" onchange="DDCjekab(this);" class="form-control pjekab" style="width: 100%" />
                                    <option value=""></option>
                                    <?php foreach($dafjka as $t):?>
                                    <option value="<?php echo $t->jekab; ?>"><?php echo $t->jekab; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Jenis Kabel Lainnya...</option>
                                </select> <input type="text" placeholder="isi jenis kabel lainnya disini" name="jekab_txt" style="display: none;" class="form-control"/>
                        </div> 
                    </div>
                </div>

                <div class='col-sm-5'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Panjang Tarikan</span>
                            <input type='text' name='takab' class='form-control' id='takab' >
                            <span class="input-group-addon">meter</span>
                        </div>
                    </div>
                </div>   

        </div>

         <div class='form-group row'>
                <div class='col-sm-12'>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon pixelfix">Keterangan</span>
                    <input type='text' name='kekab' class='form-control' id='kekab' >
                        </div>
                    </div>
                </div>   
        </div>

        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                <a href="/nuber/export_kabel" class='btn btn-primary'><i class='fa fa-file-excel-o fa-lg'></i><br> <sup><small>EXCEL</small></sup></a>
            </div>
        </div>
                
        </form>

    <br>
<h4>Data Realisasi Gelar Kabel</h4>
        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true" class="text-center">ID#</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Tanggal</th>
                        <th data-field="b"  data-sortable="true" class="text-center">Alamat</th>            
                        <th data-field="c"  data-sortable="true" class="text-center">Segment</th>
                        <th data-field="d"  data-sortable="true" class="text-center">Jenis Kabel</th>
                        <th data-field="e"  data-sortable="true" class="text-center">Panjang<br> Tarikan (m)</th>
                        <th data-field="h"  data-sortable="true" class="text-center">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafkab as $t): ?>
                        <tr>
                            <td class="text-right"><?php echo $t->idkab; ?></td>
                            <td class="text-center"><?php echo $t->tgpas; ?></td>
                            <td><?php echo $t->jakab; ?></td>
                            <td class="text-center"><?php echo $t->sekab; ?></td>
                            <td class="text-center"><?php echo $t->jekab; ?></td>
                            <td class="text-center"><?php echo $t->takab; ?></td>
                           <td class="text-center">
                            <?php if($t->usbua==$this->idaku): ?>
                                <a href="<?php echo base_url($this->uriku."/puskab/".$t->idkab); ?>" data-toggle='tooltip' title='Anda yakin akan menghapus data ini?'><i class="fa fa-trash-o fa-lg"></i></a>&nbsp;&nbsp;

                            <?php else :?>
                            <?php endif;?>
                                
                               <!-- <a href="<?php //echo base_url("panaf/pdfgen/".$l->id); ?>" data-toggle='tooltip' title='<?//=$judpdf?>' target="_blank"><i class="fa fa-file-pdf-o fa-lg"></i></a> -->
 
                            </td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                </div> <!--/.panel body-->
            </div>
        </div>
    </div><!--/.row-->

</div>  <!--/.main-->


<script>

    $(document).ready(function(){

            $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});

            $(".psekab").select2();
            $(".pjekab").select2();
            $(".podcdp").select2();

    })
</script>
<script type="text/javascript">

function DDCjekab(oDDL) {
    var oTextbox = oDDL.form.elements["jekab_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}
function DDCsekab(oDDLl) {
    var oTextboxl = oDDLl.form.elements["sekab_txt"];
    if (oTextboxl) {
        oTextboxl.style.display = (oDDLl.value == "") ? "" : "none";
        if (oDDLl.value == "")
            oTextboxl.focus();
    }
}

function DDCodcdp(oDDLj) {
    var oTextboxj = oDDLj.form.elements["odcdp_txt"];
    if (oTextboxj) {
        oTextboxj.style.display = (oDDLj.value == "") ? "" : "none";
        if (oDDLj.value == "")
            oTextboxj.focus();
    }
}


function FormSubmit(oForm) {
    var oHidden = oForm.elements["jekab"];
    var oDDL = oForm.elements["jekab_ddl"];
    var oTextbox = oForm.elements["jekab_txt"];

    var oHiddenl = oForm.elements["sekab"];
    var oDDLl = oForm.elements["sekab_ddl"];
    var oTextboxl = oForm.elements["sekab_txt"];

    var oHiddenj = oForm.elements["odcdp"];
    var oDDLj = oForm.elements["odcdp_ddl"];
    var oTextboxj = oForm.elements["odcdp_txt"];


    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;

    if (oHiddenl && oDDLl && oTextboxl)
        oHiddenl.value = (oDDLl.value == "") ? oTextboxl.value : oDDLl.value;

    if (oHiddenj && oDDLj && oTextboxj)
        oHiddenj.value = (oDDLj.value == "") ? oTextboxj.value : oDDLj.value;

}

</script>
