<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url($this->uriku."/"); ?>"> Dashboard</a></li>
            <li><a href="<?php echo base_url($this->uriku."/otblis/"); ?>"> OTB</a></li>
            <li class="active"> Ubah</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Ubah Data Core OTB</h5>
                </div>
        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                    <div class="panel">


 <?php echo $this->session->flashdata("pesan");?>
        <div class="panel panel-primary">
        	<?php foreach ($corst as $s):?>
        	<h3 style="color:blue">&nbsp;&nbsp;<span class="label label-primary"><?php echo $s->nm_otb." CORE ".$s->id_cor;?></span></h3>
        	<?php endforeach;?>
           <div class="panel-body">
    <?php foreach ($corpi as $d):?>

        <form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lbloudes' class='col-sm-2 form-control-label'>OUT DESCRIPTION</label>
                <div class='col-sm-10'>
                    <input type='text' name='ou_des' class='form-control' id='ou_des' value="<?php echo $d->ou_des; ?>">
                    
                </div>

        </div>  
        <div class='form-group row'>
                <!--<label for='lbloudes' class='col-sm-2 form-control-label'>IN DESCRIPTION</label>-->
                <div class='col-sm-10'>
                    <input type='hidden' name='id_otr' class='form-control' id='id_otr' value="<?php echo $d->id_otr; ?>">
                </div>

        </div>  

        <div class='form-group row'>
                <label for='lblindes' class='col-sm-2 form-control-label'>IN DESCRIPTION</label>
                <div class='col-sm-10'>
                    <select name="in_des" id="in_des" class="form-control pilihcore">
                                <option value="">- Pilih Core -</option>
                                <option value="NOT SPLICE" <?php echo ($d->in_des == "NOT SPLICE") ? "selected" : ""; ?>>NOT SPLICE</option>
                                <option value="SPLICE IN OTB" <?php echo ($d->in_des == "SPLICE IN OTB") ? "selected" : ""; ?>>SPLICE IN OTB</option>
                                <?php 
                                    if($d->in_des=="NOT SPLICE"){$pilot=$otbcx;}else{$pilot=$otbco;}
                                foreach($pilot as $a):?>

                                        <option value="<?php echo $a->id_otr; ?>" <?php echo ($d->in_des == $a->id_otr) ? "selected" : ""; ?>>

                                        <?php echo "Linked to ".$a->nm_otb." &rarr; Core ". $a->id_cor; ?></option>


                                <?php endforeach;?>
                    </select>
                </div>
        </div> 

  <?php endforeach; ?> 
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                <button type='button' onclick="goBack()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
             </div>
         </div>





                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

<script>


    $(document).ready(function(){
        $(".pilihcore").select2();

    })

    function goBack() {
  		window.history.back();
	}
</script>