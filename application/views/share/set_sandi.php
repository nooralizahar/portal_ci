<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">

  <title><?php echo $judul; ?></title>

  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="<?php echo base_url("/assets/css/bootstrap.min.css");?>">

</head>

<body data-spy="scroll" data-offset="50" data-target=".navbar-collapse">  

<div class="container">
    <div class="row">
        <br>
        <div class="col-xs-12 .col-md-8-centered well">

      <form action="<?php echo  base_url("lupa_sandi/simpan_sandi/")?>" method="post">
            <fieldset>
                <legend class="text-center">Setting Ulang Kata Sandi</legend>
               
                <?php echo $this->session->flashdata('pesan'); ?>
                
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-xs-12 .col-md-8">
                            <label for="txt_sandi" class="control-label text-center">Ketik Sandi Baru</label>
                            <input class="form-control" id="txt_sandi" name="txt_sandi" placeholder="Sandi Baru" value="<?php echo set_value('txt_sandi'); ?>" type="password" /> 
                            <input class="form-control" id="txt_id" name="txt_id" value="<?php echo $data->id_pengguna; ?>" type="hidden" /> 
                            <span class="text-danger"><?php echo form_error('txt_sandi'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-xs-12 .col-md-8">
                            <label for="ula_sandi" class="control-label text-center">Ulang Sandi Baru</label>
                            <input class="form-control" id="ula_sandi" name="ula_sandi" placeholder="Konfirmasi Sandi Baru" value="<?php echo set_value('ula_sandi'); ?>" type="password" />  
                            <span class="text-danger"><?php echo form_error('ula_sandi'); ?></span>
                        </div>
                    </div>
                </div>
                <br>
                <!-- sigup button -->
                <div class="form-gruop">
                    <div class="row colbox">
                        <div class="col-xs-12 .col-md-8">
                            <input id="btn_signup" name="btn_signup" type="submit" class="btn btn-primary col-xs-12 .col-md-8" value="Kirim" />
                            <br><br>
                            <input type="reset" onclick="goBack()" id="btn_reset" name="btn_reset" class="btn btn-default col-xs-12 .col-md-8" value="Batal"/>
                        </div>
                    </div>
                </div>
            </fieldset>
          </form>

            
        </div>
    </div>

</div>

 
<script>
function goBack() {
  window.history.back();
}
</script> 

<script src='<?php echo base_url("/assets/js/bootstrap.min.js"); ?>'></script>
 
</body>
</html>