<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">

  <title><?php echo $judul; ?></title>

  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="<?php echo base_url("/assets/css/bootstrap.min.css");?>">

</head>

<body data-spy="scroll" data-offset="50" data-target=".navbar-collapse">  

<div class="container">
    <div class="row">
        <br>
    <div class="col-xs-12 .col-md-8-centered well">
      <form action="<?php echo base_url("lupa_sandi/kirim/")?>" method="post">
            <fieldset>
                <legend class="text-center">Lupa Kata Sandi</legend>

                <?php echo $this->session->flashdata('pesan'); echo $check_domain_ref;?>

                <div class="form-group">
                    <div class="row colbox">
                        <div class="col-xs-12 .col-md-8">
                            <label for="txt_email" class="control-label text-center">Alamat Email </label>
                            <input class="form-control" name="alamat_email" placeholder="Ketik alamat Email disini" type="email" />  
                            <span class="text-danger"></span>
                        </div>
                    </div>
                </div>

                <br>
                <!-- sigup button -->
                <div class="form-gruop">
                    <div class="row colbox">

                        <div class="col-xs-12 .col-md-8">
                            <input  name="btnKirim" type="submit" class="btn btn-primary col-xs-12 .col-md-8" value="Kirim" />
                            <br><br>
                            <input type="reset" onclick="window.history.back()" name="btnKembali" class="btn btn-default col-xs-12 .col-md-8" value="Batal"/>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
            
        </div>
    </div>

</div>

  <script src='<?php echo base_url("/assets/js/bootstrap.min.js"); ?>'></script>
 
</body>
</html>


