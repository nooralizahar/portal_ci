
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("survey/"); ?>"> Permintaan Survey</a></li>
            <li class="active"> Preview</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading"></div>
                <div class="panel-body">

                    <?php foreach ($pilsvy as $p): ?>
                        
                    <table>
                        <tr><td>Requester</td><td>&nbsp;:&nbsp;</td><td><?php echo $p->requester; ?></td></tr>
                        <tr><td>Tgl. Request</td><td>&nbsp;:&nbsp;</td><td><?php echo date('d-M-y H:i:s',strtotime($p->tgbua)); ?></td></tr>
                    </table><br><br>

                        <div class="form-group"> 
                            <label for="">Nama Pelanggan </label>
                                <input type="hidden" name="idsvy" class="form-control" value="<?php  $ids=$p->idsvy; echo $ids; ?>"/>
              <input type="text" name="pelsvy" class="form-control" value="<?php  echo $p->pelsvy; ?>" readonly/>

                        </div>
  
                        <div class="form-group">
                            <label for="">Alamat</label>
                            <input type="text" name="alasvy" id="alasvy" class="form-control" value="<?php  echo $p->alasvy; ?>" readonly/>

                            
                        </div>
                        <div class="form-group">
                            <label for="">Waktu Survey</label>
                            <input type="text" name="wktsvy" class="form-control" value="<?php echo "Hari ".hari_indo($p->tglsvy).", ".tgl_indome($p->tglsvy)." ".$p->jamsvy;?>" readonly/>
                        </div>

                        <div class="form-group">
                            <label for="">PIC Pelanggan / No. Kontak </label>
                            <input type="text" name="picsvy" class="form-control"  value="<?php echo $p->picsvy." / ".$p->tlpsvy;?>" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="">PIC JLM</label>
                            <input type="text" name="picjlm" class="form-control" value="<?php  echo $p->focjlm; ?>" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="">Keterangan / SoW</label>
                            <input type="text" name="ketsvy" class="form-control" value="<?php  echo $p->ketsvy; ?>" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="">Lampiran &nbsp;</label>
                           <?php if($p->lampirans==''){ 

                                }else{ 

                            if($p->lampirans != null):
                            $lampiran = json_decode($p->lampirans);
                            ?>
                                <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <a href="<?php echo base_url("assets/uploads/yevrus/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><?php echo $item->judul; ?></a>
                                <?php endforeach;  ?> 
                            
                             <?php endif; }?>
                        </div>

                        <div class="form-group">
                            <label for="">Hasil Survey &nbsp;</label>
                            <?php if($p->lampiranh==''){ 

                                }else{ 

                            if($p->lampiranh != null):
                            $lampiran = json_decode($p->lampiranh);
                            ?>
                                
                                <?php foreach($lampiran as $item):

                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                <a href="<?php echo base_url("assets/uploads/yevrus/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><?php echo $item->judul; ?></a>,&nbsp;&nbsp;
                                <?php endforeach;  ?> 
                            
                             <?php endif; }?>
                        </div>

                        <center>
                        <button type='button' onclick="window.location.href = '<?php echo base_url('/survey') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button></center>


                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main   getLocation() -->

        <script type="text/javascript">
            var x = document.getElementById("lokasifoc");

                function jamOn() {
                    var today = new Date();
                    var h = today.getHours();
                    var m = today.getMinutes();
                    var s = today.getSeconds();
                        m = tamNol(m);
                        s = tamNol(s);

                    document.getElementById('jamDigi').innerHTML =
                    h + ":" + m + ":" + s;

                    var t = setTimeout(jamOn, 500);



                    var bMulai=document.getElementById('btnMulai');
                    var bSlsai=document.getElementById('btnSelesai');
                        bMulai.style.display="none";
                        bSlsai.style.display="inline"; 
                                   

                }

                function tamNol(i) {
                    if (i < 10) {i = "0" + i};  // tambah nol utk angka < 10
                    return i;
                }

                

                function getLocation() {
                    if (navigator.geolocation) {
                        navigator.geolocation.watchPosition(showPosition);
                        jamOn();
                    } else { 
                        x.innerHTML = "Browser tidak mendukung Geo Lokasi.";}
                    }
                    
                function showPosition(position) {
                    x.innerHTML="<center><a href='https://www.google.com/maps/@"+ position.coords.latitude +","+ position.coords.longitude+",19z' target='_blank'><small><sub>LOKASI SEKARANG</sub></small></a></br><input type='text' class='form-control text-center' name='latlng' value='" + position.coords.latitude +"," + position.coords.longitude+"' readonly/><br></center>";
                }
                
        </script>


