<?php 
$this->uriku=$this->uri->segment(1);
// print_r($this->session->id_div);
// die();
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
      <li><a href="#">Permintaan</a></li>
      <li><a href="#">Pembelian</a></li>

    </ol>
  </div><!--/.row-->

  <hr/>
  <div class="row">
    <div class="col-lg-12" id="import">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h5> Isi Pembelian Baru</h5>
        </div>

        <div class="panel-body">

          <div class="row"> 

            <div class="col-lg-12">

              <div class="tabbable-panel">

                <div class="tabbable-line">
                  <ul class="nav nav-tabs">

                    <li class="active">
                      <a href="#tabru" data-toggle="tab">
                      Permintaan</a>
                    </li>
                    <?php 
                    $id = $this->session->id_jabatan;
                    if($id == 9 || $id == 10 || $id == 12 || $id == 13 || $id == 15 || $id == 16 || $id == 19 || $id == 20 || 
                      $id == 21 || $id == 23 || $id == 24 || $id == 25 || $id == 28 || $id == 30 || $id == 40 || $id == 60 || $id == 61 || $id == 62 || 
                      $id == 71 || $id == 72 || $id == 84): ?>
                      <li>
                        <a href="#tabju" data-toggle="tab">
                        Persetujuan </a>
                      </li>
                      <?php
                      $check_approve = $this->db->join('pengguna', 'v_app_latrop_nailat.usbu = pengguna.id_pengguna', 'left')->where("pengguna.id_spv", $this->session->userdata("id_pengguna"))->where('v_app_latrop_nailat.status_approve', 0)->get("v_app_latrop_nailat")->num_rows();
                      if ($check_approve != 0):
                        ?>
                        <small class="float-right rounded-circle btn btn-danger rounded-circle btn-sm"><?= $check_approve ?></small>
                      <?php endif; ?>
                    <?php endif; ?>
                    <?php 
                    $id = $this->session->id_div;
                    if($id == 16): ?> 
                      <li>
                        <a href="#tabpo" data-toggle="tab">
                        Purchasing Order </a>
                      </li>
                      <?php
                      $check_po = $this->db->where('tgl_proses', NULL)->where('alasan', NULL)->where('status_approve', 1)->get("v_app_latrop_nailat")->num_rows();
                      if ($check_po != 0):
                        ?>
                        <small class="float-right rounded-circle btn btn-danger rounded-circle btn-sm"><?= $check_po ?></small>
                      <?php endif; ?>
                    <?php endif; ?>

                  </ul>

                  <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="tabru">
                     <div class="panel-body">
                       <div class="panel panel-primary">
                        <h3>Form Isian</h3>
                        <div class="panel-body">
                          <?php $this->load->view('share/nailat/tabru', true) ?>
                        </div>
                      </div>
                    </div>
                  </div>

                    <div role="tabpanel" class="tab-pane" id="tabju">
                      <div class="panel-body">
                        <div class="panel panel-primary">
                          <h3>Form Persetujuan</h3>
                          <div class="panel-body">
                            <?php $this->load->view('share/nailat/tabju', true) ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                    <div role="tabpanel" class="tab-pane" id="tabpo">
                      <div class="panel-body">
                        <div class="panel panel-primary">
                          <h3>Form Purchasing Order</h3>
                          <div class="panel-body">
                            <?php $this->load->view('share/nailat/tabpo', true) ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  </div> <!--/ .content-->

                </div>

              </div>

            </div> <!--/.col-->



          </div>










        </div>          







      </div>
    </div>

  </div>


</div>  <!--/.main-->



<script>

  function CekTgl() {
    var dateString = document.getElementById('tgta').value;
    var myDate = new Date(dateString);
    var today = new Date();
    //$('#tgl_surat').reset();
    if ( myDate <= today ) { 
      $('#tgta').after('<p style="color:red;">Tanggal dibutuhkan tidak boleh lebih kecil atau sama dengan tanggal hari ini!.</p>');
      return false;
    }

    return true;
  }

</script>


