<?php

$nmjab=$this->session->userdata("nm_jab");
$this->uriku=$this->uri->segment(1);

?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php //echo base_url("talian/sirang/"); ?>">Detail Barang</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php $n=sprintf("%04d",$id); echo "Pemohon ".$depr->nama_lengkap.", Dept. ".$depr->nm_div.", No. PR. ".$n.", dibutuhkan tgl. ".tgl_indome($depr->tgta).".";  ?></div> <!--$idrpu.-->
                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php echo $this->session->flashdata("pesan");?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <br><h4 class="text-center" style="color:#010288"><b><u>RINCIAN PERMINTAAN</u></b></h4>
                        </div> 
                    </div>
                    <div class="row">
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="panel panel-default">
                            <?php echo $this->session->flashdata("pesan");?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                        <thead>
                            <tr>
                                <th data-field="koba"  data-sortable="true">Kode Asset</th>
                                <th data-field="jeba"  data-sortable="true">Jenis Barang</th>
                                <th data-field="juba"  data-sortable="true">Jumlah</th>
                                <th data-field="keba"  data-sortable="true">Keterangan</th>
                                <th data-field="estimasi"  data-sortable="true">Estimasi</th>
                                <th data-field="harga"  data-sortable="true">Harga</th>
                                <th data-field="url_ref"  data-sortable="true">Referensi</th>
                                <th data-field="attach"  data-sortable="true">Attachment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($daba as $db): ?>
                                <tr>
                                    <td><?php echo $db->koba; ?></td>
                                    <td><?php echo $db->jeba; ?></td>
                                    <td><?php echo $db->juba; ?>
                                    <br><a class="juba" type="button" data-id="<?php echo $db->idre ?>" data-ta="<?php echo $db->idta ?>" data-toggle='modal'><i class="fa fa-pencil fa-lg"></i></a></td>
                                    <td><?php echo $db->keba; ?></td>
                                    <td><?php echo number_format($db->estimasi,2,',','.'); ?></td>
                                    <td><?php echo number_format($db->harga,2,',','.'); ?> 
                                    <br><a class="harga" type="button" data-id="<?php echo $db->idre ?>" data-ta="<?php echo $db->idta ?>" data-toggle='modal'><i class="fa fa-pencil fa-lg"></i></a></td>
                                    <td>
                                        <?php
                                            if ($db->url_ref != '-' && $db->url_ref != NULL && $db->url_ref != '#') {
                                                echo '<a target="_blank" href="'.$db->url_ref.'">Link Referensi</a>';
                                            } else {
                                                echo '-';
                                            }
                                        ?>
                                    </td>
                                    <td><a target="_blank" href="/assets/uploads/po/<?php echo $db->attach; ?>"><?php echo $db->attach; ?></a></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div><!--/.row-->

<!-- Modal -->
<div class="modal" id="harga" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Update Harga</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <form action="/nuber/sirang_detail_harga_form" method="POST">
      <div class="modal-body">
        <input type="hidden" name="idre" class="id">
        <input type="hidden" name="idta" class="ta">
        <input type="number" min="1000" name="harga" class="form-control">
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
</form>
</div>
</div>
</div>

<!-- Modal -->
<div class="modal" id="juba" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Update Jumlah barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <form action="/nuber/sirang_detail_juba_form" method="POST">
      <div class="modal-body">
        <input type="hidden" name="idre" class="id">
        <input type="hidden" name="idta" class="ta">
        <input type="number" min="1" name="juba" class="form-control">
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
</form>
</div>
</div>
</div>





</div>	<!--/.main-->


<script type="text/javascript">
    $(document).on('click', '.harga', function() {
        var id = $(this).data("id");
        var ta = $(this).data("ta");
        $('.id').val(id);
        $('.ta').val(ta);
        $('#harga').modal('show');
    });
</script>

<script type="text/javascript">
    $(document).on('click', '.juba', function() {
        var id = $(this).data("id");
        var ta = $(this).data("ta");
        $('.id').val(id);
        $('.ta').val(ta);
        $('#juba').modal('show');
    });
</script>