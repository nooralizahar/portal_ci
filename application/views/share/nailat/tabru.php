



<form class='add-item' method=POST action=''>

  <div class="col-md-6">
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon pixelfix">N  a  m  a &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <input type='text' name='nama' class='form-control' value="<?=$this->session->userdata("nama_lengkap");?>" id='nama' readonly/>

      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon pixelfix">No. PR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        <input type='text' name='idta' class='form-control' id='idta' value="<?php echo $nopr;?>" readonly>

      </div>
    </div>
  </div>  

  <div class="col-md-6">
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon pixelfix">Dept. / Bagian</span>
        <input type='text' name='dept' class='form-control' id='dept'value="<?=$this->session->userdata("nm_div");?>" readonly/>
      </div>
    </div>
  </div> 
  <div class="col-md-6">
    <div class="form-group">
      <div class="input-group">
        <span class="input-group-addon pixelfix">Dibutuhkan Tgl.</span>
        <input type='date' name='tgta' class='form-control' id='tgta' onchange="CekTgl();" required>
      </div>
    </div>
  </div> 

  <div class='form-group row'>
    <label for='tombol' class='col-sm-2 form-control-label'></label>
    <div class='col-sm-10'>
      <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
      <!-- <button type='button' onclick="goBack()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button> -->
    </div>
  </div>

</form>
<p> </p>

<div class="row">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <?php echo $this->session->flashdata("pesan");?>
      </div>
    </div>
  </div>
  <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
    <thead>

      <tr>
        <th data-field="e"  data-sortable="true" class="text-center">Tgl. PR</th>
        <th data-field="a"  data-sortable="true" class="text-center">No. PR</th>
        <th data-field="b"  data-sortable="true" class="text-center">Nama </th>
        <th data-field="c"  data-sortable="true" class="text-center">Dept.</th>
        <th data-field="d"  data-sortable="true" class="text-center">Dibutuhkan Tgl.</th>
        <th data-field="e"  data-sortable="true" class="text-center">Keterangan</th>
        <th data-field="f"  data-sortable="true" class="text-center">Approval</th>
        <th>Opsi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($dapr as $pr): ?>
        <tr>
          <td class="text-center"><?php echo $pr->tgbu; ?></td>
          <td class="text-center"><?php $n= sprintf("%04d", $pr->idta); echo $n; ?></td>
          <td class="text-center"><?php echo $pr->nama_lengkap; ?></td>
          <td class="text-center"><?php echo $pr->nm_div; ?></td>
          <td class="text-center"><?php echo tgl_indome($pr->tgta); ?></td>
          
          <td class="text-center"><?php echo $pr->alasan; ?></td>
          <td><center> 
            <?php
            if ($pr->status_approve == 1) {
              echo '<a data-toggle="tooltip" title="Approved"><i class="fa fa-check fa-lg"></i></a> ';
            } elseif ($pr->status_approve == 3) {
              echo '<a data-toggle="tooltip" title="Rejected"><i class="fa fa-ban fa-lg"></i></a> ';
            } else {
              echo '<a class="text text-info btn-sm" href="'.base_url($this->uriku.'/send_email_permintaan/'.$pr->idta).'" data-toggle="tooltip" title="Kirim Email"><i class="fa fa-envelope fa-lg"></i></a> ';
            }
            ?>
          </center></td>
          <td><center> 
            <?php
            if ($pr->tgl_proses != NULL) {
                echo '<a target="_blank" class="text text-info btn-sm" href="'.base_url($this->uriku.'/sirang_detail/'.$pr->idta).'" data-toggle="tooltip" title="Detail Barang"><i class="fa fa-file-o fa-lg"></i></a>';
              $id = $this->session->id_jabatan;
                    if($id == 7 || $id == 8 || $id == 9 || $id == 10 || $id == 12 || $id == 13 || $id == 15 || $id == 16 || $id == 19 || $id == 20 || 
                      $id == 21 || $id == 23 || $id == 24 || $id == 25 || $id == 28 || $id == 30 || $id == 40 || $id == 60 || $id == 61 || $id == 62 || 
                      $id == 71 || $id == 72 || $id == 84) {
                      if ($pr->tgl_proses == NULL) {
                          echo '<a target="_blank" class="text text-primary btn-sm" href="'.base_url($this->uriku.'/sirang/'.$pr->idta).'" data-toggle="tooltip" title="Edit barang"><i class="fa fa-pencil fa-lg"></i></a> ';
                      }
              }
            } else {
              echo '<a target="_blank" class="text text-primary btn-sm" href="'.base_url($this->uriku.'/sirang/'.$pr->idta).'" data-toggle="tooltip" title="Edit barang"><i class="fa fa-pencil fa-lg"></i></a> ';
            }
            ?>
          </center></td>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
