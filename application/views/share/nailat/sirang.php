<?php

$nmjab=$this->session->userdata("nm_jab");
$this->uriku=$this->uri->segment(1);

?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php //echo base_url("talian/sirang/"); ?>">Isi Barang</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php $n=sprintf("%04d",$id); echo "Pemohon ".$depr->nama_lengkap.", Dept. ".$depr->nm_div.", No. PR. ".$n.", dibutuhkan tgl. ".tgl_indome($depr->tgta).".";  ?></div> <!--$idrpu.-->
                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php echo $this->session->flashdata("pesan");?>
                            </div>
                        </div>
                    </div>
                    <?php if($nmjab!="Purchasing"):?>                    
                        <div class="row">
                            <form action="" method="POST" enctype="multipart/form-data" onsubmit="FormSubmit(this);">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon pixelfix">Kode Asset</span>
                                            <input type="hidden" name="idta" class="form-control" value="<?php echo $id; ?>" readonly/>
                                            <input type="text" name="koba" class="form-control" required/><br />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon pixelfix">Jenis Barang</span>
                                            <input type="hidden" name="jeba" class="form-control" />
                                            <select name="jeba_ddl" onchange="DDCjeba(this);" class="form-control piljeba" style="width: 100%" />
                                            <option value=""></option>
                                            <?php foreach($jeba as $b):?>
                                                <option value="<?php echo $b->jeba; ?>"><?php echo $b->jeba; ?></option>
                                            <?php endforeach;?>
                                            <option value="">Lainnya...</option>
                                        </select> <input type="text" placeholder="ketik jenis barang lainnya disini" name="jeba_txt" style="display: none;" class="form-control"/> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon pixelfix">Jumlah</span>
                                        <input type='text' name='juba' class='form-control' id='juba' required>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon pixelfix">Satuan</span>
                                        <select name="saba" id="saba" class="form-control" required>
                                         <option value="unit" selected>unit</option>
                                         <option value="pcs">pieces</option>
                                         <option value="roll">roll</option>
                                         <option value="meter">meter</option>
                                         <option value="pak">pack</option>
                                     </select>
                                 </div>
                             </div>
                         </div>


                         <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon pixelfix">Keterangan</span>
                                    <input type="text" name="keba" class="form-control" required/><br />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon pixelfix">Estimasi harga</span>
                                    <input type="number" min="1000" name="estimasi" class="form-control" required/><br />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon pixelfix">Link referensi</span>
                                    <input type="text" value="-" name="url_ref" class="form-control" required/><br />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon pixelfix">Attachment</span>
                                    <input type="file" name="attach" class="form-control"/><br />
                                </div>
                            </div>
                        </div>

                        <div class="col-md-10 form-group">
                            <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                            <button type='button' onclick="window.history.back()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>SELESAI</small></sup></button>
                        </div>
                    </form>
                </div>
            <?php endif;?>
            <div class="row">
                <div class="col-md-12">
                    <br><h4 class="text-center" style="color:#010288"><b><u>RINCIAN PERMINTAAN</u></b></h4>
                </div> 
            </div>
            <div class="row">
             <div class="col-md-12">
                <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                        <tr>
                            <th data-field="koba"  data-sortable="true">Kode Asset</th>
                            <th data-field="jeba"  data-sortable="true">Jenis Barang</th>
                            <th data-field="juba"  data-sortable="true">Jumlah</th>
                            <th data-field="keba"  data-sortable="true">Keterangan</th>
                            <th data-field="estimasi"  data-sortable="true">Estimasi</th>
                            <th data-field="harga"  data-sortable="true">Harga</th>   
                            <th data-field="url_ref"  data-sortable="true">Referensi</th>
                            <th data-field="attach"  data-sortable="true">Attachment</th>
                            <th>Pilihan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($daba as $db): ?>
                            <tr>
                                <td><?php echo $db->koba; ?></td>
                                <td><?php echo $db->jeba; ?></td>
                                <td><?php echo $db->juba; ?></td>
                                <td><?php echo $db->keba; ?></td>
                                <td><?php echo number_format($db->estimasi,2,',','.'); ?></td>
                                <td><?php echo number_format($db->harga,2,',','.'); ?></td>
                                
                                <td>
                                    <?php
                                    if ($db->url_ref != '-' && $db->url_ref != NULL && $db->url_ref != '#') {
                                        echo '<a target="_blank" href="'.$db->url_ref.'">Link Referensi</a>';
                                    } else {
                                        echo '-';
                                    }
                                    ?>
                                </td>
                                <td><a target="_blank" href="/assets/uploads/po/<?php echo $db->attach; ?>"><?php echo $db->attach; ?></a></td>
                                
                                <td>
                                    <center>
                                        <!--<a href="<?php //echo base_url("panel/purcedit/".$dat->id_brg); ?>" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil-square fa-lg"></i></a> --> 
                                        <?php if($nmjab=="Purchasing"):?>
                                        <?php else:?>                         
                                            <a href="<?php echo base_url($this->uriku."/harang/".$db->idre); ?>" data-toggle='tooltip' title='Hapus Barang'><i class="fa fa-trash fa-lg"></i></a>
                                        <?php endif;?>
                                    </center>                            
                                    
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div><!--/.row-->





</div>	<!--/.main-->

<script>

    $(document).ready(function(){

        $(".piljeba").select2();

    })

    function DDCjeba(oDDLm) {
        var oTextboxm = oDDLm.form.elements["jeba_txt"];
        if (oTextboxm) {
            oTextboxm.style.display = (oDDLm.value == "") ? "" : "none";
            if (oDDLm.value == "")
                oTextboxm.focus();
        }
    }


    function FormSubmit(oForm) {

        var oHiddenm = oForm.elements["jeba"];
        var oDDLm = oForm.elements["jeba_ddl"];
        var oTextboxm = oForm.elements["jeba_txt"];

        if (oHiddenm && oDDLm && oTextboxm)
            oHiddenm.value = (oDDLm.value == "") ? oTextboxm.value : oDDLm.value;
    }

</script>
</script>

