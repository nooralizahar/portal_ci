

<div class="row">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <?php echo $this->session->flashdata("pesan");?>
      </div>
    </div>
  </div>
  <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
    <thead>

      <tr>
        <th data-field="e"  data-sortable="true" class="text-center">Tgl. PR</th>
        <th data-field="a"  data-sortable="true" class="text-center">No. PR</th>
        <th data-field="b"  data-sortable="true" class="text-center">Nama </th>
        <th data-field="c"  data-sortable="true" class="text-center">Dept.</th>
        <th data-field="d"  data-sortable="true" class="text-center">Dibutuhkan Tgl.</th>
        <th data-field="e"  data-sortable="true" class="text-center">Keterangan</th>
        <th data-field="f"  data-sortable="true" class="text-center">Approval</th>
        <th>Opsi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($dapr_prv as $pr): ?>
        <tr>
          <td class="text-center"><?php echo $pr->tgbu; ?></td>
          <td class="text-center"><?php $n= sprintf("%04d", $pr->idta); echo $n; ?></td>
          <td class="text-center"><?php echo $pr->nama_lengkap; ?></td>
          <td class="text-center"><?php echo $pr->nm_div; ?></td>
          <td class="text-center"><?php echo tgl_indome($pr->tgta); ?></td>

          <td class="text-center"><?php echo $pr->alasan; ?></td>
          <td><center> 
            <?php
            if ($pr->status_approve == 1) {
              echo '<a data-toggle="tooltip" title="Approved"><i class="fa fa-check fa-lg"></i></a> ';
            } elseif ($pr->status_approve == 3) {
              echo '<a data-toggle="tooltip" title="Rejected"><i class="fa fa-ban fa-lg"></i></a> ';
            } else {
              echo '<a class="btn btn-success btn-sm" href="'.base_url($this->uriku.'/approval/'.$pr->idta).'" data-toggle="tooltip" title="Approve">Approve</a> ';
              echo '<a type="button" class="btn btn-danger btn-sm reject" data-id='.$pr->idta.' data-toggle="modal" title="Reject">Reject</a>';
            }
            ?>
          </center></td>
          <td><center>
            <a target="_blank" href="<?php echo base_url($this->uriku."/sirang_detail/".$pr->idta); ?>" data-toggle="tooltip" title="Detail Barang"><i class="fa fa-file-o fa-lg"></i></a>
          </center></td>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <!-- Modal -->
  <div class="modal" id="reject" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Reject Request</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/nuber/reject_po" method="POST">
          <div class="modal-body">
            <input type="hidden" name="idta" class="id">
            <div class="form-group mb-2">
              <label>Alasan <span class="text-danger">*</span></label>
              <input type="text" name="alasan" class="form-control" required>
            </div>
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Save changes</button>
              <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      </div>
    </div>


  </div>

  <script type="text/javascript">
    $(document).on('click', '.reject', function() {
      var id = $(this).data("id");
      $('.id').val(id);
      $('#reject').modal('show');
      console.log(id);
    });
  </script>