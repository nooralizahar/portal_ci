<div class="row">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <?php echo $this->session->flashdata("pesan");?>
      </div>
    </div>
  </div>
  <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
    <thead>

      <tr>
        <th data-field="e"  data-sortable="true" class="text-center">Tgl. PR</th>
        <th data-field="a"  data-sortable="true" class="text-center">No. PR</th>
        <th data-field="b"  data-sortable="true" class="text-center">Nama </th>
        <th data-field="c"  data-sortable="true" class="text-center">Dept.</th>
        <th data-field="g"  data-sortable="true" class="text-center">Tgl Approve</th>
        <th data-field="h"  data-sortable="true" class="text-center">Status</th>
        <th data-field="j"  data-sortable="true" class="text-center">Keterangan</th>
        <th data-field="f"  data-sortable="true" class="text-center">Proses</th>
        <th>Opsi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($sepr as $pr): ?>
        <?php 
          $approve = new DateTime(date('Y-m-d', strtotime($pr->tgl_approve)));
          if ($pr->tgl_proses == NULL) {
            $proses = new DateTime();
          } else {
            $proses = new DateTime(date('Y-m-d', strtotime($pr->tgl_proses)));
          }
        if ($approve->diff($proses)->days >= 3) {
          $format = "text-danger";
        } else {
          $format = null;
        }
        ?>
        <tr class="<?php echo $format; ?>">
          <td class="text-center"><?php echo $pr->tgbu; ?></td>
          <td class="text-center"><?php $n= sprintf("%04d", $pr->idta); echo $n; ?></td>
          <td class="text-center"><?php echo $pr->nama_lengkap; ?></td>
          <td class="text-center"><?php echo $pr->nm_div; ?></td>
          <td class="text-center"><?php echo date('d M Y H:i', strtotime($pr->tgl_approve)); ?></td>
          <td>
            <?php
              if ($pr->tgl_proses == NULL) {
                if ($pr->alasan == NULL) {
                  echo "<span class='text-warning'>Waiting</span>";
                } else {
                  echo "<span class='text-danger'>Ignore</span>";
                }
              } else {
                echo "<span class='text-success'>Done</span>";
              }
            ?>
          </td>
          <td class="text-center"><?php echo $pr->alasan; ?></td>
          <td><center> 
            <?php
            $check_harga = $this->db->where('idta', $pr->idta)->where('harga', 0)->get('app_latrop_nailat_rel')->num_rows();
            if ($check_harga == 0) {
              $check_po = $this->db->where('idta', $pr->idta)->get('app_latrop_nailat_po')->num_rows();
              if ($check_po == 1) {
                  echo '<a class="btn btn-success btn-sm" target="_blank" href="'.base_url($this->uriku."/print_po/".$pr->idta).'" title="Print">Cetak PO</a> ';
                  echo '<a class="btn btn-danger btn-sm" href="'.base_url($this->uriku."/cancel_po/".$pr->idta).'" title="Cancel">Cancel PO</a>';
              } else {
                if ($pr->alasan == NULL) {
                  echo '<a type="button" class="btn btn-primary btn-sm print" data-id='.$pr->idta.' data-toggle="modal" title="Buat">Buat PO</a> ';
                  if ($pr->tgl_proses == NULL) {
                    echo '<a class="btn btn-info btn-sm" target="_blank" href="'.base_url($this->uriku.'/sirang_detail_harga/'.$pr->idta).'" title="Revisi Harga">Revisi Harga</a> ';
                  }
                } else {
                  echo '-';
                }
              }
            } else {
                if ($pr->alasan == NULL) {
                  if ($pr->tgl_proses == NULL) {
                    echo '<a class="btn btn-warning btn-sm" target="_blank" href="'.base_url($this->uriku.'/sirang_detail_harga/'.$pr->idta).'" title="Edit Harga">Edit Harga</a> ';
                    echo '<a type="button" class="btn btn-danger btn-sm ignore" data-id='.$pr->idta.' data-toggle="modal" title="Ignore">Ignore PO</a>';
                  }
                } else {
                  echo '-';
                }
            }
            ?>
          </center></td>
          <td><center>
            <a target="_blank" href="<?php echo base_url($this->uriku."/sirang_detail/".$pr->idta); ?>" data-toggle="tooltip" title="Detail Barang"><i class="fa fa-file-o fa-lg"></i></a>
          </center></td>

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
  <!-- Modal -->
  <div class="modal" id="print" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Detail Purchasing Order</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/nuber/save_purchasing_order" method="POST">
          <div class="modal-body">
            <input type="hidden" name="idta" class="id">
            <div class="form-group mb-2">
              <label>Nama Vendor <span class="text-danger">*</span></label>
              <input type="text" name="penerima" class="form-control" required>
            </div>
            <div class="form-group mb-2">
              <label>PIC Vendor <span class="text-danger">*</span></label>
              <input type="text" name="atas_nama" class="form-control" required>
            </div>
            <div class="form-group mb-2">
              <label>Alamat Vendor <span class="text-danger">*</span></label>
              <select class="form-control alamat_penerima" style="width: 100%" type="text" multiple="multiple" name="alamat_penerima" required>
                <?php foreach ($alamat as $value): ?>
                  <option value="<?php echo $value->alamat_penerima ?>"><?php echo $value->alamat_penerima ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="form-group mb-2">
              <label>No Telp Vendor</label>
              <input type="number" min="11" name="notelp_penerima" class="form-control">
            </div>
            <div class="form-group mb-2">
              <label>Tanggal PO <span class="text-danger">*</span></label>
              <input type="date" name="tanggal_po" class="form-control" required>
            </div>
            <div class="form-group mb-2">
              <label>PO No <span class="text-danger">*</span></label>
              <input type="text" name="po_no" class="form-control" required>
            </div>
            <div class="form-group mb-2">
              <label>Quaotatio No <span class="text-danger">*</span></label>
              <input type="text" name="quatation_no" class="form-control" required>
            </div>
            <div class="form-group mb-2">
              <label>Diskon</label>
              <input type="number" min="0" value="0" name="diskon" class="form-control">
              <small class="text-danger">diisi menggunakan rupiah</small>
            </div>
            <div class="form-group mb-2">
              <label>Biaya Pengiriman</label>
              <input type="number" min="0" value="0" name="biaya_pengiriman" class="form-control">
              <small class="text-danger">diisi menggunakan rupiah</small>
            </div>
            <div class="form-group mb-2">
              <label>Peruntukan</label>
              <input type="text" name="term_payment" class="form-control">
            </div>
            <div class="form-group mb-2">
              <label>PPN %</label>
              <input type="number" min="0" value="10" name="ppn" class="form-control">
            </div>
            <div class="form-group mb-2">
              <label>Alamat Pengirim <span class="text-danger">*</span></label>
              <select class="form-control alamat_pengirim" style="width: 100%" type="text" multiple="multiple" name="alamat_pengirim" required>
                <?php foreach ($alamat as $value): ?>
                  <option value="<?php echo $value->alamat_pengirim ?>"><?php echo $value->alamat_pengirim ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save changes</button>
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <!-- Modal -->
  <div class="modal" id="ignore" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Ignore Purchasing Order</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="/nuber/ignore_po" method="POST">
          <div class="modal-body">
            <input type="hidden" name="idta" class="id">
            <div class="form-group mb-2">
              <label>Alasan <span class="text-danger">*</span></label>
              <input type="text" name="alasan" class="form-control" required>
            </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save changes</button>
            <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>



<<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">
  $(".alamat_penerima").select2({
    tags: true,
    placeholder: "Penerima",
    tokenSeparators: [],
    maximumSelectionLength: 1
  })
</script>
<script type="text/javascript">
  $(".alamat_pengirim").select2({
    tags: true,
    placeholder: "Pengirim",
    tokenSeparators: [],
    maximumSelectionLength: 1
  })
</script>
<script type="text/javascript">
  $(document).on('click', '.print', function() {
    var id = $(this).data("id");
    $('.id').val(id);
    $('#print').modal('show');
    console.log(id);
  });
</script>
<script type="text/javascript">
  $(document).on('click', '.ignore', function() {
    var id = $(this).data("id");
    $('.id').val(id);
    $('#ignore').modal('show');
    console.log(id);
  });
</script>
