﻿<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	

    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('panel/'); ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li> DiskuC </li>
            <li> Daftar </li>
        </ol>
    </div><!--/.row-->

    <hr/>
    
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">DAFTAR DISKUC </div>
                &nbsp;&nbsp;&nbsp;&nbsp;<button type='button' onclick="window.history.back()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                <div class="panel-body">   


        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="v"  data-sortable="true">Awal/Akhir</th>
                        <th data-field="s"  data-sortable="true">Sales/CRO</th>
                        <th data-field="t"  data-sortable="true">Presales</th>
                        <th data-field="u"  data-sortable="true">Customer</th>
                        <th data-field="w"  data-sortable="true">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
         <?php 
                        
                        foreach($dafcat as $c): ?>
                        <tr>
                            <td><small><?php echo date('d M y H:i', strtotime($c->awal)).' / '; if($c->awal==$c->akhir){echo " - ";}else{echo  date('d M y H:i', strtotime($c->akhir));} ?></small>
                                <?php if(lascat($c->id_rha,$this->usrku)!=$this->usrku):?><img src="<?php echo base_url('/assets/images/new.gif') ?>" width="40px" height="30px">
                                <?php endif; ?>
                            </td>
                            <td><?php echo $c->nawal; ?></td>
                            <td><?php echo $c->napre; ?></td>
                            <td><?php echo $c->id_rha.' - '.$c->capel; ?></td>
                            <td class="text-center"><a href="<?php echo base_url('chat/detail/'); echo $c->id_rha; ?>"><i class="fa fa-comments fa-lg"></i></a></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
        </table>

			  
                </div> <!--/.panel body -->
            </div> <!--/.panel default -->
          </div> <!--/.col -->
      </div> <!--/.row -->
</div>

 
