﻿<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	

    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('panel/'); ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li> DiskuC </li>
            <li> Detail</li>
        </ol>
    </div><!--/.row-->

    <hr/>
    
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">DETAIL DISKUC <?= $id ?></div>
                &nbsp;&nbsp;&nbsp;&nbsp;<button type='button' onclick="window.history.back()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                <div class="panel-body">  

<div class="row">
        <div class="card col-lg-12">
              
         <?php $no=1; foreach($detcat as $t):     ($no%2==0) ? $r='pull-right' : $r='pull-left' ?>


            <div class="card-body col-lg-8 <?= $r; ?>">
              <div>
                <p class="alert alert-<?php if($no%2==0){$s='success';}else{$s='info';} echo $s; ?>"><span class="small text-primary pull-left"><?= $t->nama; ?></span><br><?= $t->chat; ?> <br><span class="small text-muted pull-right"><?= date('d M y H:i',strtotime($t->tgchat)); ?></span></p>
              </div>
            </div>
            

         <?php $no++; endforeach; ?>     


        </div>
</div>
<div class="row">        
        <div class="card-footer col-lg-12">
            <form action="<?= base_url(); ?>/chat/kirim" method="post">
              <input type="text" height="100px" class="form-control" name="chat" placeholder="Ketik pesan disini"/>
              <input type="hidden" name="id_rha" value="<?= $id; ?>" />
              <button class="btn btn-primary form-control" type="submit" name="btnChat" style="padding-top: .55rem;">
                Kirim
              </button>
            </form>
        </div>
</div>



        </div>




                </div> <!--/.panel body -->
            </div> <!--/.panel default -->
          </div> <!--/.col -->
      </div> <!--/.row -->
</div>

 
