<?php

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (isset($judul)) ? $judul : ""; ?></title>

    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-table.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/datepicker3.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-timepicker.min.css");?> type="text/css" />
    <link href="<?php echo base_url("assets/css/styles.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/stybotn.css");?>" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url("favicon.png"); ?>" type="image/png" sizes="14x5">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/css/fileinput.min.css">

    <!-- Include Editor style. -->
    <!--<link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_style.min.css' rel='stylesheet' type='text/css' />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <!--Icons-->
    <script src="<?php echo base_url("assets/js/lumino.glyphs.js");?>"></script>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url("assets/js/html5shiv.js");?>"></script>
    <script src="<?php echo base_url("assets/js/respond.min.js");?>"></script>
    <![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/js/fileinput.min.js"></script>


    <link rel="stylesheet" href="<?php echo base_url("assets/css/magnific-popup.css"); ?>">
    <script src="<?php echo base_url("assets/js/bootstrap-datetimepicker.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.magnific-popup.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jQuery.print.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/bootbox.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.shorten.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
    <!-- Include JS file. -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/js/froala_editor.min.js'></script>

    <script>
        BASE_URL = "<?php echo base_url(); ?>";
	    $.FroalaEditor.DEFAULTS.key = "bvA-21sD-16A-13ojmweC8ahD6f1n==";
    </script>

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>JLM</span>PORTAL</a>

            <ul class="user-menu">

                <li class="dropdown pull-right">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>&nbsp;&nbsp; <?php echo $this->session->userdata("nama_lengkap"); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo base_url("nuber/logout/"); ?>"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Keluar</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
<br>
     &nbsp;&nbsp;<b>DASHBOARD</b>

    <ul class="nav menu">

       <li class="<?php echo $menu["dashboard"] ?>"><a href="<?php echo base_url($this->uriku); ?>"><i class="fa fa-dashboard"></i>&nbsp;&nbsp;<?php echo $this->nadiv;?></a></li>
    <?php if($this->jabku==40): ?>
       <li class="<?php echo $menu["metuj"] ?>"><a href="<?php echo base_url("nuber/dasrol/"); ?>"><i class="fa fa-dashboard"></i>&nbsp;&nbsp;Rollout</a></li>
    <?php endif; ?>
       <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>PERSONAL</b> <small style="font-size: 8px;"></small><sup><?php if(!empty($this->levku)){ echo "[".$this->levku."]";}else{}?></sup></small>
        <li class="<?php echo $menu["profil"] ?>"><a href="<?php echo base_url("nuber/profilvi/").$this->idaku; ?>"><i class="fa fa-user"></i>&nbsp;&nbsp;Profilku</a></li>

        <li class="<?php echo $menu["uprofil"] ?>"><a href="<?php echo base_url("nuber/profilku/").$this->idaku; ?>"><i class="fa fa-cog"></i>&nbsp;&nbsp;Ubah Sandi</a></li>
        <li class="<?php echo $menu["aprofil"] ?>"><a href="<?php echo base_url("nuber/profilak/"); ?>"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Aktifitas</a></li>

        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>PERMINTAAN</b>
    <?php if($this->divku==18 || $this->divku==12): ?>        
        <li class="<?php echo $menu["kaarea"] ?>"><a href="<?php echo base_url("nuber/kaarea/"); ?>"><i class="fa fa-sitemap"></i>&nbsp;&nbsp;Buka Area</a></li>
    <?php endif; ?>
    <?php if($this->divku==24): ?>
        <li class="<?php echo $menu["survey"] ?>"><a href="<?php echo base_url("survey/"); ?>"><i class="fa fa-camera"></i>&nbsp;&nbsp;Survei</a></li>
    <?php endif; ?>
        <li class="<?php echo $menu["talian"] ?>"><a href="<?php echo base_url("nuber/talian/"); ?>"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;Pembelian</a></li> 

         &nbsp;&nbsp;<b>MENU AKSES</b>

    <?php if($this->divku==2): ?>
        <li class="<?php echo $menu["menol"] ?>"><a href="<?php echo base_url("nuber/dafset/"); ?>"><i class="fa fa-list"></i>&nbsp;&nbsp;Daftar Asset</a></li>
    <?php elseif($this->divku==12): ?>
        <!--<li class="<?php //echo $menu["mesat"] ?>"><a href="<?php //echo base_url("nuber/iombar/"); ?>"><i class="fa fa-list"></i>&nbsp;&nbsp;IoM</a></li> -->
    <?php elseif($this->divku==23): ?>
         <?php if($this->jabku==68): ?>
        <li><a href="#" class="text-muted"><i class="fa fa-minus-circle text-danger"></i>&nbsp;&nbsp;Tidak Tersedia</a></li>    
         <?php else: ?>
        <li class="<?php echo $menu["menol"] ?>"><a href="<?php echo base_url("datpel/"); ?>"><i class="fa fa-list"></i>&nbsp;&nbsp;Data Order</a></li>
        <li class="<?php echo $menu["mesat"] ?>"><a href="<?php echo base_url("datpel/aktif/"); ?>"><i class="fa fa-users"></i>&nbsp;&nbsp;Data Pelanggan</a></li>
        <li class="<?php echo $menu["medua"] ?>"><a href="<?php echo base_url("selser/repsla/"); ?>"><i class="fa fa-clipboard"></i>&nbsp;&nbsp;Laporan SLA</a></li>
        <li class="<?php echo $menu["metig"] ?>"><a href="<?php echo base_url("khusus/vendat"); ?>"><i class="fa fa-clipboard"></i>&nbsp;&nbsp;Data Vendor</a></li>
        <?php endif; ?>
    <?php elseif($this->divku==6 || $this->divku==20 || $this->divku==18 ): ?>
        <li class="<?php echo $menu["menol"] ?>"><a href="<?php echo base_url("otbmen/otbli/"); ?>"><i class="fa fa-archive"></i>&nbsp;&nbsp;OTB</a></li>
        <li class="<?php echo $menu["mesat"] ?>"><a href="<?php echo base_url("nuber/tiabar/"); ?>"><i class="fa fa-map-signs"></i>&nbsp;&nbsp;Tiang</a></li>
        <li class="<?php echo $menu["medua"] ?>"><a href="<?php echo base_url("nuber/kabbar/"); ?>"><i class="fa fa-chain"></i>&nbsp;&nbsp;Kabel</a></li>
        <li class="<?php echo $menu["metig"] ?>"><a href="<?php echo base_url("nuber/odpin/");?>"><i class="fa fa-list"></i>&nbsp;&nbsp;Daftar ODP</a></li>
        <li class="<?php echo $menu["meemp"] ?>"><a href="<?php echo base_url("nuber/odcin/");?>"><i class="fa fa-th-list"></i>&nbsp;&nbsp;Daftar ODC</a></li>
        <li class="<?php echo $menu["melim"] ?>"><a href="<?php echo base_url("nuber/odprek/");?>"><i class="fa fa-signal"></i>&nbsp;&nbsp;Rekapitulasi</a></li>
        <li class="<?php echo $menu["meena"] ?>"><a href="<?php echo base_url("datewo/");?>"><i class="fa fa-cog"></i>&nbsp;&nbsp;E W O</a></li>
    <?php endif; ?>

    <?php if($this->levku=='admin'): ?>
        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>KELOLA</b>
        <li class="<?php echo $menu["pengguna"]; ?>"><a href="<?php echo base_url("nuber/pengguna"); ?>"><i class="fa fa-users"></i>&nbsp;&nbsp;Pengguna</a></li> 
        <li class="<?php echo $menu["divisi"]; ?>"><a href="<?php echo base_url("nuber/divisi"); ?>"><i class="fa fa-pie-chart"></i>&nbsp;&nbsp;Bagian</a></li>      
        <li class="<?php echo $menu["jabatan"]; ?>"><a href="<?php echo base_url("nuber/jabatan"); ?>"><i class="fa fa-star"></i>&nbsp;&nbsp;Jabatan</a></li>
        <li>&nbsp;</li>
    <?php endif; ?>
    </ul>

</div><!--/.sidebar-->



