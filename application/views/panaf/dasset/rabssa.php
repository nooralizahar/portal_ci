
        <form class='add-item' method=POST action='' onsubmit="FormSubmit(this);">
         <div class='form-group row'>
                <label for='lblno' class='col-sm-1 form-control-label'>Lokasi </label>
                <div class='col-sm-3'>
                                <input type="hidden" name="loasse" class="form-control" />
                                <select name="loasse_ddl" onchange="DDClokasi(this);" class="form-control pilkasi" style="width: 100%" />
                                    <option value=""></option>
                                    <?php foreach($lokasi as $s):?>
                                    <option value="<?php echo $s->loasse; ?>"><?php echo $s->loasse; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Lainnya...</option>
                                </select> <input type="text" placeholder="ketik Lokasi lainnya disini" name="loasse_txt" style="display: none;" class="form-control"/>
                </div>                
                <label for='lblno' class='col-sm-1 form-control-label'>Pemasok </label>
                <div class='col-sm-3'>
                                <input type="hidden" name="pemaso" class="form-control" />
                                <select name="pemaso_ddl" onchange="DropDownChanged(this);" class="form-control pilmaso" style="width: 100%" />
                                    <option value=""></option>
                                    <?php foreach($pemasok as $s):?>
                                    <option value="<?php echo $s->pemaso; ?>"><?php echo $s->pemaso; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Lainnya...</option>
                                </select> <input type="text" placeholder="ketik pemasok lainnya disini" name="pemaso_txt" style="display: none;" class="form-control"/>
                </div>

                <label for='lblno' class='col-sm-1 form-control-label'>Kode </label>
                <div class='col-sm-3'>
                    <input type='text' name='koasse' class='form-control' id='kasse' >
                    
                </div>
        </div>

        <div class='form-group row'>
                <label for='lblkabrg' class='col-sm-2 form-control-label'>Jenis</label>
                <div class='col-sm-4'>
                
                        <input type="hidden" name="jeasse" class="form-control" />
                        <select name="jeasse_ddl" onchange="DDCjenis(this);" class="form-control piljeni" style="width: 100%" />
                        <option value=""></option>
                        <?php foreach($jenis as $i):?>
                        <option value="<?php echo $i->jeasse; ?>"><?php echo $i->jeasse; ?></option>
                        <?php endforeach;?>
                        <option value="">Lainnya...</option>
                        </select> <input type="text" placeholder="ketik jenis lainnya disini" name="jeasse_txt" style="display: none;" class="form-control"/> 
                </div>

                <label for='lblkabrg' class='col-sm-2 form-control-label'>Merk, Type/Model</label>
                <div class='col-sm-4'>

                        <input type="hidden" name="measse" class="form-control" />
                        <select name="measse_ddl" onchange="DDCmerk(this);" class="form-control pilmerk" style="width: 100%" />
                        <option value=""></option>
                        <?php foreach($merk as $k):?>
                        <option value="<?php echo $k->measse; ?>"><?php echo $k->measse; ?></option>
                        <?php endforeach;?>
                        <option value="">Lainnya...</option>
                        </select> <input type="text" placeholder="ketik merk/type lainnya disini" name="measse_txt" style="display: none;" class="form-control"/> 
                    
                </div>
        </div>

        <div class='form-group row'>
                <label for='lblkabrg' class='col-sm-2 form-control-label'>No. Serial A</label>
                <div class='col-sm-4'>
                    <input type='text' name='nomsna' class='form-control' id='nomsna'>
                    
                </div>
                <label for='lblkabrg' class='col-sm-2 form-control-label'>No. Serial B</label>
                <div class='col-sm-4'>
                    <input type='text' name='nomsnb' class='form-control' id='nomsna'>
                    
                </div>
        </div>

        <div class='form-group row'>
                <label for='lblkabrg' class='col-sm-2 form-control-label'>Deskripsi</label>
                <div class='col-sm-10'>
                    <input type='text' name='deasse' class='form-control' id='desse'>
                    
                </div>
        </div>

        <div class='form-group row'>
                <label for='lbltglp' class='col-sm-2 form-control-label'>Tgl. Pembelian</label>
                <div class='col-sm-4'>
                    <input type='date' name='tgbeli' class='form-control' id='tgbeli'>    
                </div>
                <label for='lbltgla' class='col-sm-2 form-control-label'>Tgl. Terima</label>
                <div class='col-sm-4'>
                    <input type='date' name='tgrima' class='form-control' id='tgrima'>    
                </div>
        </div>

        <div class='form-group row'>
                <label for='lbltgle' class='col-sm-2 form-control-label'>Tgl. Penyerahan</label>
                <div class='col-sm-4'>
                    <input type='date' name='tgsera' class='form-control' id='tgsera'>    
                </div>
                 <label for='lbltgle' class='col-sm-2 form-control-label'>Pengguna</label>
                <div class='col-sm-4'>
                    <!--<input type='text' name='pnguna' class='form-control' id='pnguna'>--> 

                    <select name="pnguna" id="pnguna" class="form-control piluser" style="width: 100%">
                                <option value=""></option>
                                <?php foreach($user as $r):?>
                                    <option value="<?php echo $r->id; ?>"><?php echo $r->opsi; ?></option>
                                <?php endforeach;?>
                    </select>   
                </div>           
        </div>



        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('panhg/simset/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
<script>

    $(document).ready(function(){

            $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});

            $(".pilmaso").select2();
            $(".piljeni").select2();
            $(".pilmerk").select2();
            $(".piluser").select2();
            $(".pilkasi").select2();
    })
</script>

<script type="text/javascript">

function DropDownChanged(oDDL) {
    var oTextbox = oDDL.form.elements["pemaso_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}
function DDClokasi(oDDLl) {
    var oTextboxl = oDDLl.form.elements["loasse_txt"];
    if (oTextboxl) {
        oTextboxl.style.display = (oDDLl.value == "") ? "" : "none";
        if (oDDLl.value == "")
            oTextboxl.focus();
    }
}

function DDCjenis(oDDLj) {
    var oTextboxj = oDDLj.form.elements["jeasse_txt"];
    if (oTextboxj) {
        oTextboxj.style.display = (oDDLj.value == "") ? "" : "none";
        if (oDDLj.value == "")
            oTextboxj.focus();
    }
}

function DDCmerk(oDDLm) {
    var oTextboxm = oDDLm.form.elements["measse_txt"];
    if (oTextboxm) {
        oTextboxm.style.display = (oDDLm.value == "") ? "" : "none";
        if (oDDLm.value == "")
            oTextboxm.focus();
    }
}

function FormSubmit(oForm) {
    var oHidden = oForm.elements["pemaso"];
    var oDDL = oForm.elements["pemaso_ddl"];
    var oTextbox = oForm.elements["pemaso_txt"];

    var oHiddenl = oForm.elements["loasse"];
    var oDDLl = oForm.elements["loasse_ddl"];
    var oTextboxl = oForm.elements["loasse_txt"];

    var oHiddenj = oForm.elements["jeasse"];
    var oDDLj = oForm.elements["jeasse_ddl"];
    var oTextboxj = oForm.elements["jeasse_txt"];

    var oHiddenm = oForm.elements["measse"];
    var oDDLm = oForm.elements["measse_ddl"];
    var oTextboxm = oForm.elements["measse_txt"];

    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;

    if (oHiddenl && oDDLl && oTextboxl)
        oHiddenl.value = (oDDLl.value == "") ? oTextboxl.value : oDDLl.value;

    if (oHiddenj && oDDLj && oTextboxj)
        oHiddenj.value = (oDDLj.value == "") ? oTextboxj.value : oDDLj.value;

    if (oHiddenm && oDDLm && oTextboxm)
        oHiddenm.value = (oDDLm.value == "") ? oTextboxm.value : oDDLm.value;
}

</script>