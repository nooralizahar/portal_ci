
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url("panaf/"); ?>"> Dashboard</a></li>
            <li class="active"> Bukti Pengeluaran Bank</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading"> Form Isian Bukti Pengeluaran Bank</div>
                <div class="panel-body">

    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">


        <div class="panel"> <!-- awal panel -->

                    <div class="tabbable-panel">
                        <div class="tabbable-line">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#requester" aria-controls="requester" role="tab" data-toggle="tab">  Pengeluaran Baru</a></li>
                            <li role="presentation" class="active"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Daftar Bukti Pengeluaran</a></li>
                        </ul>



<div class="tab-content">
  <div role="tabpanel" class="tab-pane" id="requester">
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">
        <form class='add-item' method=POST action=''>
         <div class='form-group row'>
                <label for='lblno' class='col-sm-2 form-control-label'>No. </label>
                <div class='col-sm-2'>
                    <input type='text' name='urt' class='form-control' id='no' >
                    
                </div>
                <label for='lbltglbuk' class='col-sm-1 form-control-label'>Tanggal </label>
                <div class='col-sm-2'>
                    <input type='date' name='tgt' class='form-control' id='tgt'>    
                </div>

        </div>
        <div class="form-group row">
                <label for='lbltglinv' class='col-sm-2 form-control-label'>Tgl. Invoice </label>
                <div class='col-sm-2'>
                    <input type='date' name='tgi' class='form-control' id='tgi'>    
                </div>

            
        </div>

        <div class='form-group row'>
                <label for='lblkepada' class='col-sm-2 form-control-label'>Dibayarkan kepada</label>
                <div class='col-sm-10'>
                    <input type='text' name='dik' class='form-control' id='dik' required>
                    
                </div>

        </div>

        <!-- <div class="form-group row">
                <label for='lblnominal' class='col-sm-2 form-control-label'>Nominal</label>
                <div class='col-sm-6'>
                    <input type='text' name='nominal' class='form-control' id='nominal' required>
                    
                </div>
            
        </div> -->
  
        <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Supplier Invoice</label>
                <div class='col-sm-10'>
                    <input type='text' name='sui' class='form-control' id='sui' required>
                </div>
        </div> 

        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('panaf/tiarbank/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
             </div>
         </div>
     </div>
</div>


<div role="tabpanel" class="tab-pane active" id="problem">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"><?php 
            //echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-2">  </div>
        </div>
        <hr/>

</div> <br>

   <!-- <br>
    <h4 ><div style="color: green">Verified : <?= $dver." (".number_format(($dver/($dver+$dunv))*100)." %)" ?></div><div style="color: red"> Unverified : <?= $dunv." (".number_format(($dunv/($dver+$dunv))*100)." %)" ?> </div></h4> -->
    <br>

        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="i"  data-sortable="true">No.</th>
                        <th data-field="q"  data-sortable="true">Tanggal</th>
                        <th data-field="v"  data-sortable="true">Tgl. Inv.</th>
                        <th data-field="a"  data-sortable="true">Dibayar Kepada</th>
                        <th data-field="j"  data-sortable="true">Supplier Inv.</th>
                        <th data-field="d"  data-sortable="true">Item</th>
                        <th data-field="c"  data-sortable="true">Keterangan</th>
                        <th data-field="b"  data-sortable="true">Nominal</th>
                        <th data-field="e"  data-sortable="true">Token</th>                        
                        <th data-field="h"  data-sortable="true">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($datiarba as $l): ?>
                        <tr>
                            <td><?php echo $l->urt; ?></td>
                            <td><?php echo tgl_indome($l->tgt); ?></td>
                            <td><?php echo tgl_indome($l->tgi); ?></td>
                            <td><?php echo $l->dik; ?></td>
                            <td><?php echo $l->sui; ?></td>
                            <td class="text-center"><?php echo $l->jml; ?></td>
                            <td><?php echo $l->ket; ?></td>
                            <td class="text-right"><a href="#" data-toggle='tooltip' title='<?php echo terbilang($l->nit); ?>'><?php echo number_format($l->nit); ?></a></td>
                            <td><?php echo $l->tok; ?></td>

                            <td><center>
                            <?php if($l->cet==0):  $judpdf='Generate PDF';?>
                                <a href="<?php echo base_url("panaf/ubtiarba/".$l->idt); ?>" data-toggle='tooltip' title='Isi Keterangan'><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;&nbsp; 

                            <?php else : $judpdf='View PDF';?>
                            <?php endif;?>
                                
                                <a href="<?php echo base_url("panaf/pdfgen/".$l->idt); ?>" data-toggle='tooltip' title='<?=$judpdf?>' target="_blank"><i class="fa fa-file-pdf-o fa-lg"></i></a>
 
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


  </div>
 </div>
</div> <!-- akhir panel -->



            </div>
        </div>
    </div>  


                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->


