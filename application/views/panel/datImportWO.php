<?php
session_start();
//include "../../config/koneksi.php";
//include "../../config/tgl_indo.php";
$nowos=$_GET['nowo'];
$Session = $_GET['Session'];
$cookie = "Cookie: B1SESSION=".$Session; // deklarasi cookie dari session

$ch = curl_init('https://hanacomp.beonesolution.com:50000/b1s/v1/ServiceCalls?$select=%2A&$filter=startswith%28Subject,%27WO%20'.$nowos.'%27%29%0A%0A%0A');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

// set header get dengan cookie
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    $cookie)
);

$result = curl_exec($ch);
$err = curl_error($ch);
curl_close($ch);
 if($err){
   echo($err);
 }else{
   // echo($result); // print hasil get
 }



$hasil= json_decode($result,true);
echo "<pre>";
//print_r($hasil);  
echo "</pre>";  

//echo $hasil["odata.metadata"] 

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Aplikasi OTB - SAP WO</title>

    <link href="../../css/bootstrap.css" rel="stylesheet" />
    <link href="../../css/font-awesome.css" rel="stylesheet" />
    <script src="../../js/bootstrap.min.js"></script>

<style>

hr {
  display: block;
  margin-top: 0.5em;
  margin-bottom: 0.5em;
  margin-left: auto;
  margin-right: auto;
  border-style: inset;
  border-width: 1px;
}
</style>

</head>

<body>
<h3><center><b>SAP B1</b> INTERFACING </center> </h3>
<h5><center>IMPORT UNTUK TOPOLOGY </center> </h5>
<?php echo "<div class='container' style='width:98%;'>";
      echo "<button type='button' onclick=\"window.location.href='../../unem.php?dom=ygolopot';\" class='btn btn-info'><i class='fa fa-arrow-left fa-2x'></i><br><br> <sup><small>KEMBALI</small></sup></button>";   
?>
<!--<p>
Meta  : <?php //echo $hasil["odata.metadata"] ?><br>
<br>
<br><br>
</p> -->
<?php for ($x = 0; $x < 1; $x++) {?>
<small><br>SAP Doc Num : <?php echo $hasil["value"]["$x"]["DocNum"] ?><small>
<center><table width='90%' border='0px'>
<tr><td colspan=3></td></tr>
<?php echo "<tr><td width='25%'>DOC Number</td><td width='2%' align='center'> :</td> <td width='68%'>".substr($hasil["value"]["$x"]["Subject"],3,9)."</td></tr><tr><td width='25%'>Issued Date</td><td width='2%' align='center'> :</td> <td width='68%'>".tgl_indo($hasil["value"]["$x"]["CreationDate"])."</td></tr><tr><td width='25%'>No. PO/IRF/SRF </td><td width='2%' align='center'> :</td> <td width='68%'>".$hasil["value"]["$x"]["U_NO_PO"]."</td></tr><tr><td width='25%'>Product Desc </td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_PRODUCT_DESC"]."</td></tr><tr><td width='25%'>Service Initial </td><td width='2%' align='center'> :</td> <td width='68%'> ".$hasil["value"]["$x"]["U_SERVICES"]."</td></tr><tr><td width='25%'>No. Survei </td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_NO_SURVEI"]."</td></tr><tr><td width='25%'>Sales Person </td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_SALES"]."</td></tr>" ;?>

<h5><tr><td colspan=3 background="../../images/bgblue.png"><hr><b> Customer Description</b><hr></td></tr> </h5>

<?php echo "<tr><td width='25%'>Name</td><td width='2%' align='center'> :</td> <td width='68%'><b>". substr($hasil["value"]["$x"]["Subject"],13,75)."</b></td></tr><tr><td width='25%'>Address</td><td width='2%' align='center'> :</td> <td width='68%'>".$hasil["value"]["$x"]["U_ADD_CUST"]."</td></tr><tr><td width='25%'>PIC</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_PIC_CUST"]."</td></tr><tr><td width='25%'>Phone Number / HP</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_PHONE_CUST"]."</td></tr><tr><td width='25%'></td><td width='2%' align='center'> :</td> <td width='68%'>".$hasil["value"]["$x"]["U_HP_CUST"]."</td></tr><tr><td width='25%'>Email</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_EMAIL_CUST"]."</td></tr>" ;?>

<h5><tr><hr><td colspan=3 background="../../images/bgblue.png"><hr><b> Services Description </b><hr></td></tr></h5>

<?php echo "<tr><td width='25%'>Services</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_PRODUCT_DESC"]."</td></tr><tr><td width='25%'> Destination</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_ADD_DEST"]."</td></tr><tr><td width='25%'>Capacity</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_CAPACITY"]."</td></tr><tr><td width='25%'> Equipment</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_EQUIPMENT"]."</td></tr><tr><td width='25%'>MAC Address </td><td width='2%' align='center'> :</td> <td width='68%'>.</td></tr><tr><td width='25%'> IP Address</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_IP_ADDR"]."</td></tr><tr><td width='25%'>Media </td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_MEDIA"]."</td></tr> <tr><td width='25%'>Interface</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_INTERFACE"]."</td></tr>" ;?>

<h5><tr><td colspan=3 background="../../images/bgblue.png"><hr><b> Vendor Description</b><hr></td></tr></h5>

<?php echo "<tr><td width='25%'>Name</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_VENDOR"]."</td></tr><tr><td width='25%'>PIC</td><td width='2%' align='center'> :</td> <td width='68%'> ". $hasil["value"]["$x"]["U_PICVENDOR"]."</td></tr><tr><td width='25%'> Capacity</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_CAPACITY_V"]."</td></tr><tr><td width='25%'>Phone / HP</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["Room"]."</td></tr><tr><td width='25%'>Email</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["Room"]."</td></tr>" ;?>

<h5><tr><td colspan=3 background="../../images/bgblue.png"><hr><b> Installation Description</b><hr></td></tr> </h5>

<?php echo "<tr><td width='25%'>Destination</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_ADD_DEST"]."</td></tr><tr><td width='25%'>PIC on Location</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_PIC_CUST"]."</td></tr><tr><td width='25%'>Phone / HP</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["U_PHONE_CUST"]."/". $hasil["value"]["$x"]["U_HP_CUST"]."</td></tr><tr><td width='25%'>FAX </td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["Room"]."</td></tr><tr><td width='25%'>Email</td><td width='2%' align='center'> :</td> <td width='68%'>". $hasil["value"]["$x"]["Room"]."</td></tr><tr><td width='25%'>Start Date</td><td width='2%' align='center'> :</td> <td width='68%'> ". tgl_indo($hasil["value"]["$x"]["CreationDate"])."</td></tr><tr><td width='25%'>RFS Date</td><td width='2%' align='center'> :</td> <td width='68%'>". tgl_indo($hasil["value"]["$x"]["U_RFS_DATE"])."</td></tr><tr><td colspan=3><hr></td></tr></table></center>" ;?>

<?php   
  
  $idwo = substr($hasil["value"]["$x"]["Subject"],3,9);
  $tgis = $hasil["value"]["$x"]["CreationDate"];
  $tgrf = $hasil["value"]["$x"]["U_RFS_DATE"];
  $nopo = $hasil["value"]["$x"]["U_NO_PO"];
  $desc = $hasil["value"]["$x"]["U_PRODUCT_DESC"];
  $serv = $hasil["value"]["$x"]["U_SERVICES"];
  $nasl = $hasil["value"]["$x"]["U_SALES"];
  $nacu = substr($hasil["value"]["$x"]["Subject"],13,40);
  $alcu = $hasil["value"]["$x"]["U_ADD_CUST"];
  $kocu = $hasil["value"]["$x"]["U_PIC_CUST"];
  $phcu = $hasil["value"]["$x"]["U_PHONE_CUST"];
  $hpcu = $hasil["value"]["$x"]["U_HP_CUST"];
  $emcu = $hasil["value"]["$x"]["U_EMAIL_CUST"];
  $cacu = $hasil["value"]["$x"]["U_CAPACITY"];
  $mecu = $hasil["value"]["$x"]["U_MEDIA"];
  $ifcu = $hasil["value"]["$x"]["U_INTERFACE"];
  $nave = $hasil["value"]["$x"]["U_VENDOR"];
  $kove = $hasil["value"]["$x"]["U_PICVENDOR"];
  $phve = '';
  $dsap = $hasil["value"]["$x"]["DocNum"];


 }  
 $tgljam= date('Y-m-d H:i:sa');


  mysqli_query($conn,"INSERT INTO tp_owatad(id_worde,
                                 tg_issue,
                                 tg_erefs,
                                 no_porde,
                                 ds_pdesc,
                                 nm_sales,
                                 nm_custo,
                                 al_custo,
                                 cp_custo,                 
                                 ph_custo,
                                 em_custo,
                                 sv_custo,
                                 ca_custo,
                                 nm_svsta,
                                 md_custo,
                                 if_custo,
                                 nm_ven,
                                 cp_ven,
                                 ph_ven,
                                 fi_attach,
                                 tg_cre,
                                 us_cre,
                                 gl_lat,
                                 gl_lon) 
                         VALUES('$idwo',
                                '$tgis',
                                '$tgrf',
                                '$nopo',
                                '$desc',
                                '$nasl',
                                '$nacu',
                                '$alcu',
                                '$kocu',               
                                '$phcu/$hpcu',
                                '$emcu',
                                '$serv',
                                '$cacu',
                                '$desc',   
                                '$mecu',
                                '$ifcu',
                                '$nave',
                                '$kove',
                                '$phve',
                                '$dsap',
                                '$tgljam',
                                '$_SESSION[namauser]',
                                '$_SESSION[sflat]',
                                '$_SESSION[sflon]')");  




?>
<br>
<br>
</body>
</html>
