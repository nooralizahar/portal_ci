      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">PMO DASHBOARD</div>
                <div class="panel-body">

                  <div class="row">
                      <div class="col-lg-12">
                      <div class="tabbable-panel">
                              <div class="tabbable-line">
                                  <ul class="nav nav-tabs ">

                                      <li class="active">
                                          <a href="#tabsa" data-toggle="tab">
                                          Work Order</a>
                                      </li>
                                      <li>
                                          <a href="#tabdu" data-toggle="tab">
                                          Rekapitulasi </a>
                                      </li>

                                  </ul>
                                  
                                  <div class="tab-content">
                                      <div class="tab-pane active" id="tabsa">
                                        <?php $this->load->view("/panel/ompunem/infopmo")?>

                                      </div>
                                      <div class="tab-pane" id="tabdu">
                                        <?php $this->load->view("/panel/inforin")?>

                                      </div>
                                  </div>  

                              </div>
                      </div>
                      </div> <!--/.col-->
                  </div><!--/.row-->   


                </div> <!--/.panel body -->
            </div> <!--/.panel default -->
          </div> <!--/.col -->
      </div> <!--/.row -->