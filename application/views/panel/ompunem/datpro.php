        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tgpro"  data-sortable="true">Tanggal</th>
                        <th data-field="urpro"  data-sortable="true">Uraian Progres</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($d_wopro as $dpro): ?>
                        <?php if($dpro->site!=""): ?>
                        <tr><td colspan="2"><?php echo $dpro->site; ?></td></tr>
                        <?php endif; ?>
                        <tr>
                            <td><?php echo $dpro->tg_pro; ?></td>
                            <td><?php echo $dpro->uraian; ?></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>