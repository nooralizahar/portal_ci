
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/womonito"); ?>"> WO Monitoring</a></li>
            <li class="active"> Baru</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading"> Isi Work Order Baru</div>
                <div class="panel-body">
                    <form action="" enctype="multipart/form-data" method="POST" onsubmit="FormSubmit(this);">
                        <div class="form-group"> 
                            <div class="col-sm-3">
                            <label for="">No. WO </label>
                            <input type="text" name="id_worde" class="form-control" placeholder="Ketik No. Work Order disini" required/>
                            </div>
                            <div class="col-sm-3">
                            <label for="">No. PO</label>
                            <input type="text" name="no_porde" class="form-control" placeholder="Ketik No. PO disini, ketik (-) jika tidak ada No. PO " required/>
                            </div>
                            <div class="col-sm-2">
                            <label for="">CID</label>
                            <input type="text" name="ph_ven" class="form-control" placeholder="Ketik CID disini" required/>
                            </div>
                            <div class="col-sm-4">
                            <label for="">SID</label>
                            <input type="text" name="no_baa" class="form-control" placeholder="Ketik SID disini, ketik (-) jika tidak ada SID" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <label for="">Nama Pelanggan</label>
                            <input type="text" name="nm_custo" class="form-control" placeholder="Ketik Nama Pelanggan disini" required/>
                            
                        </div>
                        <div class="form-group">
                            <label for="">Alamat Pelanggan</label>
                            <input type="text" name="al_custo" class="form-control" placeholder="Ketik Alamat disini" required/>
                        </div>
                        <div class="form-group row">
                            <label for='lblno' class='col-sm-1 form-control-label'>Sales</label>
                            <div class='col-sm-3'>
                                            <input type="hidden" name="nm_sales" class="form-control" />
                                            <select name="nm_sales_ddl" onchange="DDCnm_sales(this);" class="form-control pnm_sales" style="width: 100%" />
                                                <option value=""></option>
                                                <?php foreach($dafnm_sales as $c):?>
                                                <option value="<?php echo $c->nm_sales; ?>"><?php echo $c->nm_sales; ?></option>
                                                <?php endforeach;?>
                                                <option value="">Lainnya...</option>
                                            </select> <input type="text" placeholder="ketik lainnya disini" name="nm_sales_txt" style="display: none;" class="form-control"/>
                            </div>
                            <label class='col-sm-1 form-control-label'>Initial Serv.</label>
                            <div class='col-sm-3'>
                                            <input type="hidden" name="sv_custo" class="form-control" />
                                            <select name="sv_custo_ddl" onchange="DDCsv_custo(this);" class="form-control psv_custo" style="width: 100%" />
                                                <option value=""></option>
                                                <?php foreach($dafsv_custo as $t):?>
                                                <option value="<?php echo $t->sv_custo; ?>"><?php echo $t->sv_custo; ?></option>
                                                <?php endforeach;?>
                                                <option value="">Lainnya...</option>
                                            </select> <input type="text" placeholder="ketik lainnya disini" name="sv_custo_txt" style="display: none;" class="form-control"/>
                            </div>
                            <label for='lblno' class='col-sm-1 form-control-label'>Service </label>
                            <div class='col-sm-3'>
                                            <input type="hidden" name="nm_svsta" class="form-control" />
                                            <select name="nm_svsta_ddl" onchange="DDCnm_svsta(this);" class="form-control pnm_svsta" style="width: 100%" />
                                                <option value=""></option>
                                                <?php foreach($dafnm_svsta as $p):?>
                                                <option value="<?php echo $p->nm_svsta; ?>"><?php echo $p->nm_svsta; ?></option>
                                                <?php endforeach;?>
                                                <option value="">Lainnya...</option>
                                            </select> <input type="text" placeholder="ketik Ket. lainnya disini" name="nm_svsta_txt" style="display: none;" class="form-control"/>
                            </div> 


                        </div>


                        <div class="form-group row">
                            <label class='col-sm-1 form-control-label'>Interface</label>
                            <div class='col-sm-3'>
                                            <input type="hidden" name="if_custo" class="form-control" />
                                            <select name="if_custo_ddl" onchange="DDCif_custo(this);" class="form-control pif_custo" style="width: 100%" />
                                                <option value=""></option>
                                                <?php foreach($dafif_custo as $t):?>
                                                <option value="<?php echo $t->if_custo; ?>"><?php echo $t->if_custo; ?></option>
                                                <?php endforeach;?>
                                                <option value="">Lainnya...</option>
                                            </select> <input type="text" placeholder="ketik lainnya disini" name="if_custo_txt" style="display: none;" class="form-control"/>
                            </div>
                            <label class='col-sm-1 form-control-label'>Media</label>
                            <div class='col-sm-3'>
                                            <input type="hidden" name="md_custo" class="form-control" />
                                            <select name="md_custo_ddl" onchange="DDCmd_custo(this);" class="form-control pmd_custo" style="width: 100%" />
                                                <option value=""></option>
                                                <?php foreach($dafmd_custo as $t):?>
                                                <option value="<?php echo $t->md_custo; ?>"><?php echo $t->md_custo; ?></option>
                                                <?php endforeach;?>
                                                <option value="">Lainnya...</option>
                                            </select> <input type="text" placeholder="ketik lainnya disini" name="md_custo_txt" style="display: none;" class="form-control"/>
                            </div>
                            <label for='lblno' class='col-sm-1 form-control-label'>Capacity</label>
                            <div class='col-sm-3'>
                                            <input type="hidden" name="ca_custo" class="form-control" />
                                            <select name="ca_custo_ddl" onchange="DDCca_custo(this);" class="form-control pca_custo" style="width: 100%" />
                                                <option value=""></option>
                                                <?php foreach($dafca_custo as $c):?>
                                                <option value="<?php echo $c->ca_custo; ?>"><?php echo $c->ca_custo; ?></option>
                                                <?php endforeach;?>
                                                <option value="">Lainnya...</option>
                                            </select> <input type="text" placeholder="ketik lainnya disini" name="ca_custo_txt" style="display: none;" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Alamat Instalasi</label>
                            <input type="text" name="de_custo" class="form-control" placeholder="Ketik Alamat Instalasi disini" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Tgl. RFS</label>
                            <input type="date" name="tg_erefs" class="form-control" required/>
                        </div>

                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
                        <!-- <input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

<script>

    $(document).ready(function(){

            $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});

            $(".pilcus").select2();
    })
</script>

<script>

    $(document).ready(function(){

            $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});

            $(".pca_custo").select2();
            $(".psv_custo").select2();
            $(".pif_custo").select2();
            $(".pmd_custo").select2();
            $(".pnm_svsta").select2();
            $(".pnm_sales").select2();

    })
</script>
<script type="text/javascript">

function DDCsv_custo(oDDL) {
    var oTextbox = oDDL.form.elements["sv_custo_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}

function DDCif_custo(oDDLk) {
    var oTextboxk = oDDLk.form.elements["if_custo_txt"];
    if (oTextboxk) {
        oTextboxk.style.display = (oDDLk.value == "") ? "" : "none";
        if (oDDLk.value == "")
            oTextboxk.focus();
    }
}

function DDCmd_custo(oDDLh) {
    var oTextboxh = oDDLh.form.elements["md_custo_txt"];
    if (oTextboxh) {
        oTextboxh.style.display = (oDDLh.value == "") ? "" : "none";
        if (oDDLh.value == "")
            oTextboxh.focus();
    }
}

function DDCca_custo(oDDLl) {
    var oTextboxl = oDDLl.form.elements["ca_custo_txt"];
    if (oTextboxl) {
        oTextboxl.style.display = (oDDLl.value == "") ? "" : "none";
        if (oDDLl.value == "")
            oTextboxl.focus();
    }
}

function DDCnm_sales(oDDLi) {
    var oTextboxi = oDDLi.form.elements["nm_sales_txt"];
    if (oTextboxi) {
        oTextboxi.style.display = (oDDLi.value == "") ? "" : "none";
        if (oDDLi.value == "")
            oTextboxi.focus();
    }
}

function DDCnm_svsta(oDDLj) {
    var oTextboxj = oDDLj.form.elements["nm_svsta_txt"];
    if (oTextboxj) {
        oTextboxj.style.display = (oDDLj.value == "") ? "" : "none";
        if (oDDLj.value == "")
            oTextboxj.focus();
    }
}


function FormSubmit(oForm) {
    var oHidden = oForm.elements["sv_custo"];
    var oDDL = oForm.elements["sv_custo_ddl"];
    var oTextbox = oForm.elements["sv_custo_txt"];

    var oHiddenk = oForm.elements["if_custo"];
    var oDDLk = oForm.elements["if_custo_ddl"];
    var oTextboxk = oForm.elements["if_custo_txt"];

    var oHiddenh = oForm.elements["md_custo"];
    var oDDLh = oForm.elements["md_custo_ddl"];
    var oTextboxh = oForm.elements["md_custo_txt"];

    var oHiddenl = oForm.elements["ca_custo"];
    var oDDLl = oForm.elements["ca_custo_ddl"];
    var oTextboxl = oForm.elements["ca_custo_txt"];

    var oHiddeni = oForm.elements["nm_sales"];
    var oDDLi = oForm.elements["nm_sales_ddl"];
    var oTextboxi = oForm.elements["nm_sales_txt"];

    var oHiddenj = oForm.elements["nm_svsta"];
    var oDDLj = oForm.elements["nm_svsta_ddl"];
    var oTextboxj = oForm.elements["nm_svsta_txt"];


    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;

    if (oHiddenk && oDDLk && oTextboxk)
        oHiddenk.value = (oDDLk.value == "") ? oTextboxk.value : oDDLk.value;

    if (oHiddenh && oDDLh && oTextboxh)
        oHiddenh.value = (oDDLh.value == "") ? oTextboxh.value : oDDLh.value;

    if (oHiddeni && oDDLi && oTextboxi)
        oHiddeni.value = (oDDLi.value == "") ? oTextboxi.value : oDDLi.value;

    if (oHiddenl && oDDLl && oTextboxl)
        oHiddenl.value = (oDDLl.value == "") ? oTextboxl.value : oDDLl.value;

    if (oHiddenj && oDDLj && oTextboxj)
        oHiddenj.value = (oDDLj.value == "") ? oTextboxj.value : oDDLj.value;

}

</script>



</div>	<!--/.main-->

