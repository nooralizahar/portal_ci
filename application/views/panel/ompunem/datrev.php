<?php $crev=$this->db->where('id_worde',$idno)->get('tp_owatad_rev')->num_rows(); ?>
<div class="panel-body" style="max-height: 200px;overflow-y: scroll;">

      <div class="form-group row">
           <?php 
    if(empty($crev)){echo "<div class='col-lg-2'></div><div class='col-lg-9'>";$j="WORK ORDER<br>DEPARTEMENT COMMERCIAL<hr/>";}else{echo "<div class='col-lg-6'><small>";$j="Data Setelah Revisi";}
    ?> 
        
            <table border='0px' width="95%">

                    <tbody>
                        <tr align="center"><td colspan="3" ><b style="color:green"> <?=$j?></b></td></tr>
                        <?php foreach($afrev as $ar): 
                                    $docnum=$ar->fi_attach;
                                    $idword=$ar->id_worde;
                                    $tgissu=$ar->tg_issue;
                                    $nopord=$ar->no_porde;
                                    $prodes=$ar->ds_pdesc;
                                    $servin=$ar->sv_custo;
                                    $nasale=$ar->nm_sales;
                                    $nacust=$ar->nm_custo;
                                    $alcust=$ar->al_custo;
                                    $kocust=$ar->cp_custo;
                                    $jacust=$ar->ja_custo;
                                    $phcust=$ar->ph_custo;
                                    $emcust=$ar->em_custo;
                                    $decust=$ar->de_custo;
                                    $sites = $ar->exe_sel;
                                    $cacust=$ar->ca_custo;
                                    $mdcust=$ar->md_custo;
                                    $ipcust=$ar->ip_custo;
                                    $ifcust=$ar->if_custo;
                                    $eqcust=$ar->eq_custo;
                                    $navend=$ar->nm_ven;
                                    $cpvend=$ar->cp_ven;
                                    $phvend=$ar->ph_ven;
                                    $tgeref=$ar->tg_erefs;
                                    $sidref=$ar->no_baa;
                                    $cidref=$ar->cid;
                              endforeach;
                              ?>
                        <tr>
                        <td width="30%">&nbsp;CID/SID / No. WO </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $phvend."/".$sidref." / ".$idword; ?></td><!-- $cidref 220218 ganti $phvend-->
                        </tr>
                        <tr>
                        <td>&nbsp;Issued Date</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo tgl_indo($tgissu); ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;No. PO/IRF/SRF</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $nopord; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Product Description</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $prodes; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Service Initial</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $servin; ?></td>
                        </tr>
                        <td>&nbsp;No. Survey</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php //echo $ar->sv_custo; ?></td>
                        </tr>
                        <tr>
                        <tr>
                        <td>&nbsp;Sales</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $nasale; ?></td>
                        </tr>
                        <tr><td colspan="3" style="background: -webkit-linear-gradient(bottom, rgba(99, 217, 233,1) 0%, rgba(99, 217, 233, 0.2) 75%, rgba(99, 217, 233, 0.4) 90%, rgba(99, 217, 233,0.6) 100%); }">&nbsp;Customer Description</td></tr>
                        <tr>
                        <td valign='top'>&nbsp;Name</td><td valign='top'>&nbsp;:&nbsp;</td><td valign='top'>&nbsp;<?php echo $nacust.'<b> ('.$sites.')</b>'; ?></td>
                        </tr>
                        <tr>
                        <td valign='top'>&nbsp;Address </td><td valign='top'>&nbsp;:&nbsp;</td><td valign='top'>&nbsp;<?php echo $alcust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;PIC</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $kocust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Jabatan </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $jacust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Phone Number</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $phcust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;HP </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $phcust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Email </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $emcust; ?></td>
                        </tr>
                        <tr><td colspan="3" style="background: -webkit-linear-gradient(bottom, rgba(99, 217, 233,1) 0%, rgba(99, 217, 233, 0.2) 75%, rgba(99, 217, 233, 0.4) 90%, rgba(99, 217, 233,0.6) 100%); }">&nbsp;Services Description</td></tr>
                        <tr>
                        <td>&nbsp;Services</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $prodes; ?></td>
                        </tr>
                        <tr>
                        <td valign='top'>&nbsp;Destination </td><td valign='top'>&nbsp;:&nbsp;</td><td valign='top'>&nbsp;<?php   echo $decust.' <b>('.$sites.')</b>'; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Capacity</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $cacust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Equipment </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $eqcust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;IP Address</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $ipcust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Media</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $mdcust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Interface </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $ifcust; ?></td>
                        </tr>
                        <tr><td colspan="3" style="background: -webkit-linear-gradient(bottom, rgba(99, 217, 233,1) 0%, rgba(99, 217, 233, 0.2) 75%, rgba(99, 217, 233, 0.4) 90%, rgba(99, 217, 233,0.6) 100%); }">&nbsp;Vendor Description</td></tr>
                        <tr>
                        <td>&nbsp;Name</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $navend; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;PIC </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $cpvend; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Capacity</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php //echo $ar->ca_custo; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Phone </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php //echo $phvend; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Email</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php //echo $ar->ip_custo; ?></td>
                        </tr>
                        <tr><td colspan="3" style="background: -webkit-linear-gradient(bottom, rgba(99, 217, 233,1) 0%, rgba(99, 217, 233, 0.2) 75%, rgba(99, 217, 233, 0.4) 90%, rgba(99, 217, 233,0.6) 100%); }">&nbsp;Installation Description</td></tr>
                        <tr>
                        <td valign='top'>&nbsp;Destination </td><td valign='top'>&nbsp;:&nbsp;</td><td valign='top'>&nbsp;<?php echo $decust.'<b> ('.$sites.')</b>'; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;PIC</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $kocust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Phone/HP</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $phcust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;FAX </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php //echo $ar->al_custo; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Email</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo $emcust; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Start Date</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo tgl_indo($tgissu); ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;RFS Date </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php echo tgl_indo($tgeref); ?></td>
                        </tr>
                        
                    </tbody>
            </table>
    <?php 
    if(empty($crev)){}else{echo "</small>";}
    ?>
        
        </div>
    <?php 
    if(empty($crev)){}else{
    ?> 
        <div class="col-lg-6"><small>
            <table border='0px' width="95%">

                    <tbody>
                        <tr align="center"><td colspan="3" ><b style="color:red"> Data Sebelum Revisi </b></td></tr>
                        <?php foreach($berev as $br): ?>
                        <tr>
                        
                        <td width="30%">&nbsp;CID/SID / No. WO </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($phvend!=$br->ph_ven){echo "<s style='text-decoration: line-through red;'>".$br->ph_ven."</s>";}else{echo $br->ph_ven;}?><?php echo "/".$br->no_baa." / ".$br->id_worde; ?></td> <!-- $cidref. v--> 
                        </tr>
                        <tr>
                        <td>&nbsp;Issued Date</td><td>&nbsp;:&nbsp;</td><td>&nbsp;
                              <?php if ($tgissu!=$br->tg_issue){echo "<s style='text-decoration: line-through red;'>".tgl_indo($br->tg_issue)."</s>";}else{echo tgl_indo($br->tg_issue);} 
                              ?>  </td>
                        </tr>
                        <tr>
                        <td>&nbsp;No. PO/IRF/SRF</td><td>&nbsp;:&nbsp;</td><td>&nbsp;      <?php if ($nopord!=$br->no_porde){echo "<s style='text-decoration: line-through red;'>".$br->no_porde."</s>";}else{echo $br->no_porde;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Product Description</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($prodes!=$br->ds_pdesc){echo "<s style='text-decoration: line-through red;'>".$br->ds_pdesc."</s>";}else{echo $br->ds_pdesc;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Service Initial</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($servin!=$br->sv_custo){echo "<s style='text-decoration: line-through red;'>".$br->sv_custo."</s>";}else{echo $br->sv_custo;} 
                              ?></td>
                        </tr>
                        <td>&nbsp;No. Survey</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php //echo $ar->sv_custo; ?></td>
                        </tr>
                        <tr>
                        <tr>
                        <td>&nbsp;Sales</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($nasale!=$br->nm_sales){echo "<s style='text-decoration: line-through red;'>".$br->nm_sales."</s>";}else{echo $br->nm_sales;} 
                              ?></td>
                        </tr>
                        <tr><td colspan="3" style="background: -webkit-linear-gradient(bottom, rgba(99, 217, 233,1) 0%, rgba(99, 217, 233, 0.2) 75%, rgba(99, 217, 233, 0.4) 90%, rgba(99, 217, 233,0.6) 100%); }">&nbsp;Customer Description</td></tr>
                        <tr>
                        <td>&nbsp;Name</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($nacust!=$br->nm_custo){echo "<s style='text-decoration: line-through red;'>".$br->nm_custo."</s>";}else{echo $br->nm_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Address </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($alcust!=$br->al_custo){echo "<s style='text-decoration: line-through red;'>".$br->al_custo."</s>";}else{echo $br->al_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;PIC</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($kocust!=$br->cp_custo){echo "<s style='text-decoration: line-through red;'>".$br->cp_custo."</s>";}else{echo $br->cp_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Jabatan </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($jacust!=$br->ja_custo){echo "<s style='text-decoration: line-through red;'>".$br->ja_custo."</s>";}else{echo $br->ja_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Phone Number</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($phcust!=$br->ph_custo){echo "<s style='text-decoration: line-through red;'>".$br->ph_custo."</s>";}else{echo $br->ph_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;HP </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($phcust!=$br->ph_custo){echo "<s style='text-decoration: line-through red;'>".$br->ph_custo."</s>";}else{echo $br->ph_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Email </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($emcust!=$br->em_custo){echo "<s style='text-decoration: line-through red;'>".$br->em_custo."</s>";}else{echo $br->em_custo;} 
                              ?></td>
                        </tr>
                        <tr><td colspan="3" style="background: -webkit-linear-gradient(bottom, rgba(99, 217, 233,1) 0%, rgba(99, 217, 233, 0.2) 75%, rgba(99, 217, 233, 0.4) 90%, rgba(99, 217, 233,0.6) 100%); }">&nbsp;Services Description</td></tr>
                        <tr>
                        <td>&nbsp;Services</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($prodes!=$br->ds_pdesc){echo "<s style='text-decoration: line-through red;'>".$br->ds_pdesc."</s>";}else{echo $br->ds_pdesc;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Destination </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($decust!=$br->de_custo){echo "<s style='text-decoration: line-through red;'>".$br->de_custo."</s>";}else{echo $br->de_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Capacity</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($cacust!=$br->ca_custo){echo "<s style='text-decoration: line-through red;'>".$br->ca_custo."</s>";}else{echo $br->ca_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Equipment </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($eqcust!=$br->eq_custo){echo "<s style='text-decoration: line-through red;'>".$br->eq_custo."</s>";}else{echo $br->eq_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;IP Address</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($ipcust!=$br->ip_custo){echo "<s style='text-decoration: line-through red;'>".$br->ip_custo."</s>";}else{echo $br->ip_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Media</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($mdcust!=$br->md_custo){echo "<s style='text-decoration: line-through red;'>".$br->md_custo."</s>";}else{echo $br->md_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Interface </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($ifcust!=$br->if_custo){echo "<s style='text-decoration: line-through red;'>".$br->if_custo."</s>";}else{echo $br->if_custo;} 
                              ?></td>
                        </tr>
                        <tr><td colspan="3" style="background: -webkit-linear-gradient(bottom, rgba(99, 217, 233,1) 0%, rgba(99, 217, 233, 0.2) 75%, rgba(99, 217, 233, 0.4) 90%, rgba(99, 217, 233,0.6) 100%); }">&nbsp;Vendor Description</td></tr>
                        <tr>
                        <td>&nbsp;Name</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($navend!=$br->nm_ven){echo "<s style='text-decoration: line-through red;'>".$br->nm_ven."</s>";}else{echo $br->nm_ven;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;PIC </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($cpvend!=$br->cp_ven){echo "<s style='text-decoration: line-through red;'>".$br->cp_ven."</s>";}else{echo $br->cp_ven;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Capacity</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php //echo $ar->ca_custo; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Phone </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php //if ($phvend!=$br->ph_ven){echo "<s style='text-decoration: line-through red;'>".$br->ph_ven."</s>";}else{echo $br->ph_ven;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Email</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php //echo $ar->ip_custo; ?></td>
                        </tr>
                        <tr><td colspan="3" style="background: -webkit-linear-gradient(bottom, rgba(99, 217, 233,1) 0%, rgba(99, 217, 233, 0.2) 75%, rgba(99, 217, 233, 0.4) 90%, rgba(99, 217, 233,0.6) 100%); }">&nbsp;Installation Description</td></tr>
                        <tr>
                        <td>&nbsp;Destination </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($decust!=$br->de_custo){echo "<s style='text-decoration: line-through red;'>".$br->de_custo."</s>";}else{echo $br->de_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;PIC</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($kocust!=$br->cp_custo){echo "<s style='text-decoration: line-through red;'>".$br->cp_custo."</s>";}else{echo $br->cp_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Phone/HP</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($phcust!=$br->ph_custo){echo "<s style='text-decoration: line-through red;'>".$br->ph_custo."</s>";}else{echo $br->ph_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;FAX </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php //echo $ar->al_custo; ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Email</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($emcust!=$br->em_custo){echo "<s style='text-decoration: line-through red;'>".$br->em_custo."</s>";}else{echo $br->em_custo;} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;Start Date</td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($tgissu!=$br->tg_issue){echo "<s style='text-decoration: line-through red;'>".tgl_indo($br->tg_issue)."</s>";}else{echo tgl_indo($br->tg_issue);} 
                              ?></td>
                        </tr>
                        <tr>
                        <td>&nbsp;RFS Date </td><td>&nbsp;:&nbsp;</td><td>&nbsp;<?php if ($tgeref!=$br->tg_erefs){echo "<s style='text-decoration: line-through red;'>".tgl_indo($br->tg_erefs)."</s>";}else{echo tgl_indo($br->tg_erefs);} 
                              ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table></small>
        </div>
  <?php } ?>
    </div> <!-- end row-->

</div> <!-- end panel body -->