﻿<?php
    $thnini= date('y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    $thnblu= date('ym',strtotime("-1 month"));
    $blnini=date('Y-m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 

    $idwo=$this->db->order_by("id_worde","desc")->limit(1)->get("tp_owatad")->result();
        foreach($idwo as $dat): 
        $updidwo=$dat->tg_cre;   
        endforeach; 
        

?>

<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"> Work Order</a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-3">
        <?php 
            echo "<small>Pengkinian : ". date("d-M-y H:i:s", strtotime($updidwo))."</small>";
         ?></div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar blank"><use xlink:href="#stroked-calendar-blank"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/word/$blnini"); ?>"><?php echo $this->db->where("tg_issue like ",$blnini."%")->get("tp_owatad")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar blank"><use xlink:href="#stroked-calendar-blank"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/word/$blnlal"); ?>"><?php echo $this->db->where("tg_issue like ",$blnlal."%")->get("tp_owatad")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/word/20$thnini"); ?>"><?php echo $this->db->where("tg_issue like ","20".$thnini."%")->get("tp_owatad")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/word/20$thnlal"); ?>"><?php echo $this->db->where("tg_issue like ","20".$thnlal."%")->get("tp_owatad")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun Lalu</div>
                    </div>
                </div>
            </div>
        </div>
 </div>

 <div class="row">       
        <ol class="breadcrumb">
          <li><a href="#"> Detil Bulan</a></li>

        </ol>
        <br>
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar blank"><use xlink:href="#stroked-calendar-blank"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"><?php $all=$this->db->where("tg_issue like ",$blnini."%")->get("tp_owatad")->num_rows(); echo $all; ?></a></div>
                        <div class="text-muted">WO Bulan ini</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-merah panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked chevron up"><use xlink:href="#stroked-chevron-up"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datvie0"><?php 

                            $oi=$this->db->where("tg_issue like ",$blnini."%")->where("sta",0)->get("v_tp_owatad")->num_rows();
                                        $ci=$this->db->where("tg_issue like ",$blnini."%")->where("dan",1)->get("v_tp_owatad")->num_rows();
                          $bi=$this->db->where("tg_issue like ",$blnini."%")->where("can",1)->get("v_tp_owatad")->num_rows();
                         echo $oi; ?></a></div>
                        <div class="text-muted">Open</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clock"><use xlink:href="#stroked-clock"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datvie3"><?php $p=$all-($oi+$ci+$bi); echo $p; ?></a></div>
                        <div class="text-muted">On Progress</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datvie1"><?php echo $ci; ?></a></div>
                        <div class="text-muted">Completed</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-merah panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datvie2"><?php echo $bi; ?></a></div>
                        <div class="text-muted">Canceled</div>
                    </div>
                </div>
            </div>
        </div>




        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-ungu-l2 panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar blank"><use xlink:href="#stroked-calendar-blank"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/word/$blnini"); ?>"><?php $allb=$this->db->where("tg_issue like ",$blnlal."%")->get("tp_owatad")->num_rows(); echo $allb; ?></a></div>
                        <div class="text-muted">WO Bulan lalu</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-merah-l2 panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked chevron up"><use xlink:href="#stroked-chevron-up"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datlal0"><?php 
                            $ob=$this->db->where("tg_issue like ",$blnlal."%")->where("sta",0)->get("v_tp_owatad")->num_rows();
                            $cb=$this->db->where("tg_issue like ",$blnlal."%")->where("dan",1)->get("v_tp_owatad")->num_rows();
                            $bb=$this->db->where("tg_issue like ",$blnlal."%")->where("can",1)->get("v_tp_owatad")->num_rows();
                         echo $ob; ?></a></div>
                        <div class="text-muted">Open</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga-l2 panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clock"><use xlink:href="#stroked-clock"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datlal3"><?php $pb=$allb-($ob+$cb+$bb); echo $pb; ?></a></div>
                        <div class="text-muted">On Progress</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal-l2 panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datlal1"><?php echo $cb; ?></a></div>
                        <div class="text-muted">Completed</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-merah-l2 panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datlal2"><?php echo $bb; ?></a></div>
                        <div class="text-muted">Canceled</div>
                    </div>
                </div>
            </div>
        </div>
</div> <!--/.row -->



<div class="modal fade" id="datvie0" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:red'>Detil Open</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body" style="max-height: 400px;overflow-y: scroll;">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnini."%")->where("sta",0)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; <b style='color:red'> ".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="datvie1" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:green'>Detil Completed</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body" style="max-height: 400px;overflow-y: scroll;">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnini."%")->where("dan",1)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp;<b style='color:green'>".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="datvie2" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:pink'>Detil Canceled</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnini."%")->where("can",1)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp;<b style='color:pink'>".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>
<div class="modal fade" id="datvie3" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:orange'>Detil On Progress</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnini."%")->where("dan",0)->where("sta !=",0)->where("can !=",1)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp;<b style='color:orange'>".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>








<div class="modal fade" id="datlal0" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:red'>Detil Open</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body" style="max-height: 400px;overflow-y: scroll;">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnlal."%")->where("sta",0)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; <b style='color:red'> ".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="datlal1" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:green'>Detil Completed</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body" style="max-height: 400px;overflow-y: scroll;">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnlal."%")->where("dan",1)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp;<b style='color:green'>".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="datlal2" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:pink'>Detil Canceled</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnlal."%")->where("can",1)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp;<b style='color:pink'>".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="datlal3" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:orange'>Detil On Progress</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnlal."%")->where("dan",0)->where("sta !=",0)->where("can !=",1)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp;<b style='color:orange'>".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>