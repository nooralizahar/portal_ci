﻿<?php
    $thnini= date('y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    $thnblu= date('ym',strtotime("-1 month"));
    $blnini=date('Y-m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 


?>

<div class="row">
<div style="padding: 70px 30px;" class="container col-xs-12">

  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Work Order</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body panel-primary">
            <!--<h3>Data Belum Tersedia</h3>-->
            <div class="panel-body">
                <?php $this->load->view("/panel/ompunem/infowobl")?>
            </div>

        </div>
      </div>
    </div>


    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title" >
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Annually</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body" style="max-width: 98%;overflow-x: scroll;"><!--<h3>Data Belum Tersedia</h3>-->


            <center><h2><u>RANGKUMAN WORK ORDER</u></h2></center>
            <?php $this->load->view("/panel/rpteng/sumreq")?>


        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title" >
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Per Sales</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
        <div class="panel-body" style="max-width: 98%;overflow-x: scroll;"><!--<h3>Data Belum Tersedia</h3>-->

            <center><h2><u>RANGKUMAN PER SALES</u></h2></center>
            <?php $this->load->view("/panel/ompunem/sumwops")?> 


           
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title" >
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Status</a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
        <div class="panel-body" style="max-width: 98%;overflow-x: scroll;"><!--<h3>Data Belum Tersedia</h3>-->

            <center><h2><u>RANGKUMAN STATUS</u></h2></center>
            <?php $this->load->view("/panel/ompunem/sumsta")?> 


           
        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title" >
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">Lainnya</a>
        </h4>
      </div>
      <div id="collapse9" class="panel-collapse collapse">
        <div class="panel-body"><!--<h3>Data Belum Tersedia</h3>-->

            <center><h2><u>RANGKUMAN LAINNYA</u></h2></center>
            <?php //$this->load->view("/panel/rpteng/sumpre")?> 


           
        </div>
      </div>
    </div>



  </div> 
</div>

</div> <!--/.row -->
 
