<?php

$usrnm=$this->session->userdata("username");

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/purcbaru/"); ?>">Permohonan </a></li>
            <li class="active">Cetak PO</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12" id="approval">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal mengirim respon! Coba lagi nanti <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Persetujuan berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h3>Cetak PO</h3>
                </div>
                <div class="panel-body">
                    <b></b><br />

                    <form action="" method="POST" enctype="multipart/form-data">
                    <table class="table table-responsive">
                            <tr>
                             <td width="15%">Kepada </td><td width="2%">:</td><td>Nama Supplier<?php //echo $d_pusm->no_rpu; ?></td>
                            </tr>
                            <tr>
                             <td>Alamat </td><td>:</td><td>Alamat Supplier<?php //echo $d_pusm->pengirim; ?></td>
                            </tr>
                            <tr>
                            <td>No. Penawaran </td><td>:</td><td>Penawaran Supplier<?php //echo $d_pusm->peruntuk; ?></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table class="table table-responsive">
                                        <tr>
                                            <td align='center'>Jumlah</td><td>Jenis</td><td>Uraian</td>
                                           <td>Harga (Rp.)</td>
                                        </tr>
                                        
                                    <?php foreach($d_basm as $dat){
                                    echo "<tr><td align='center'>".$dat->brgjml." ".$dat->brgstn." </td><td>".$dat->brgjen."</td><td> ".$dat->brgdet."</td><td> ".number_format($dat->brghar)."</td>";
                                    echo"</tr>";

                                   }
                                    ?>
                                    
                                    </table>                                    
                                </td>
                            </tr>
                        

                     </table>
                    </form>
                </div>
            </div>
        </div>




    </div><!--/.row end-->




</div>	<!--/.main-->

