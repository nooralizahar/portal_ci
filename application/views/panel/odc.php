<?php
  $aksi="dom/odc/odc_act.php";
  
switch($_GET[act]){
	// Tampil Topology

	default:
		$usr=$_SESSION[namauser];
		$upu=strtoupper($usr);
		
	 									
      $sumtam= mysqli_query($conn, "SELECT count(idodc) as jmhodc, sum(kapasitas) as jmhkap FROM t_odc"); 
      $j=mysqli_fetch_array($sumtam); 

      $sumtap= mysqli_query($conn, "SELECT count(idodc) as jmhpel FROM t_odp where idodc<>''"); 
      $k=mysqli_fetch_array($sumtap); 

      $tampil= mysqli_query($conn, "SELECT t_odc.*,count(t_odp.idodc<>'') as used FROM t_odc left join t_odp on t_odc.idodc=t_odp.idodc group by t_odc.idodc ");

     // $tampil= mysqli_query($conn, "SELECT t_odc.*,count(t_odc_rel.noport) AS isi FROM t_odc LEFT JOIN t_odc_rel ON t_odc.idodp=t_odc_rel.idodp group by t_odc.idodc"); 	
		
    if ($_SESSION[leveluser]=='admin'){	
	
	
     
	  echo "<h2>Data Optical Distribution Center (ODC) <small><sup> a </sup></small></h2> <br>
      <hr>



	   <form class='add-item' method=POST action='$aksi?dom=odc&act=tupni'>
		<div class='form-group row'>
					<label for='lblnama' class='col-sm-2 form-control-label'>Nama / Kode ODC</label>
					<div class='col-sm-10'>
					  <input type='text' name='nama' class='form-control' id='nama' required>
					</div>
		  </div>  
		 <div class='form-group row'>
					<label for='lblalamat' class='col-sm-2 form-control-label'>Alamat </label>
					<div class='col-sm-10'>
					  <input type='text' name='alamat' class='form-control' id='alamat' required>
					</div>
		  </div> 
        <div class='form-group row'>
				<label for='lblkordinat' class='col-sm-2 form-control-label'>Coordinate</label>
				<div class='col-sm-10'>
					  <input type='text' name='kordinat' class='form-control' id='kordinat' required>
				</div>
				
		</div>	  
		  <div class='form-group row'>
					<label for='lblkapasitas' class='col-sm-2 form-control-label'>Kapasitas</label>
					<div class='col-sm-10'>
					  <input type='text' name='kapasitas' class='form-control' id='kapasitas' required>
					</div>
		  </div>
	        
				  
			    <div class='form-group row'>
				    <label for='tombol' class='col-sm-2 form-control-label'></label>
					<div class='col-sm-10'>
					  <button type='submit' name='cdomat' class='btn btn-info'><i class='fa fa-plus-circle fa-2x'></i><br> <sup><small>TAMBAH</small></sup></button>
					  <button type='button' onclick=\"window.location.href='draobd.php?dom=home';\" class='btn btn-info'><i class='fa fa-arrow-left fa-2x'></i><br> <sup><small>KEMBALI</small></sup></button>
					</div>
    			</div>
				
				</form>";
	}else{
        
	  
	  echo "<h2>Data Optical Distribution Center (ODC) <small><sup> u </sup></small></h2> 
			<button type='button' onclick=\"window.location.href='draobd.php?dom=home';\" class='btn btn-info'><i class='fa fa-arrow-left fa-2x'></i><br> <sup><small>KEMBALI</small></sup></button>
			<hr/> ";
	 
	}	

	$tsedia=$j[jmhkap]-$k[jmhpel];
    
    echo "
	<div class='form-group row'>
		<div class='col-sm-3'>
			<div class='panel panel-info'>
		      <div class='panel-heading text-center'><h5>Jumlah </h5></div>
		      <div class='panel-body text-center'><h4>$j[jmhodc]</h4></div>
		    </div>
	    </div>

	    <div class='col-sm-3'>
			<div class='panel panel-primary'>
		      <div class='panel-heading text-center'><h5>Kapasitas</h5></div>
		      <div class='panel-body text-center'><h4>$j[jmhkap]</h4></div>
		    </div>
	    </div>

	    <div class='col-sm-3'>
			<div class='panel panel-warning'>
		      <div class='panel-heading text-center'><h5>Digunakan</h5></div>
		      <div class='panel-body text-center'><h4>$k[jmhpel]</h4></div>
		    </div>
	    </div>

	    <div class='col-sm-3'>
			<div class='panel panel-success'>
		      <div class='panel-heading text-center'><h5>Tersedia</h5></div>
		      <div class='panel-body text-center'><h4>$tsedia</h4></div>
		    </div>
	    </div>
    </div>


    <div class='container' style='overflow-y:scroll;width:98%;'>
		  <table class='table table-striped table-bordered dax'>
		  <thead class='btn-primary'>
          <tr>
			  <th>No</th>
			  <th>Nama / Kode ODC</th>
			  <th>Lokasi</th>
			  <th>Peta</th>
			  <th>Kapasitas (port)</th>
			  <th>Sisa</th>
			  <th>Detil</th>
		  </tr>
		</thead><tbody>"; 
    $no=1;
    while ($r=mysqli_fetch_array($tampil)){
		if($no%2){$color="#aecbff";}else{$color="#deeaff";}
	    $txtnacu=str_replace(' ', '&nbsp;', $r[nama]);
       
        if($r[used]>0){$warna=$color;}else{$warna="#00ff00";}
		$sisa=$r[kapasitas]-$r[used];
		if($sisa==0){$warna="#ff0000";}else{}
/*		$svsta=strtoupper(substr($r[nm_svsta],0,3));
		if($svsta=="TER"){$warna=$color;}*/
       echo "
			 <tr>
			 <td bgcolor = $color>";
			 if ($_SESSION[leveluser]=='admin'){
			 	if ($r[tg_cre]=='0000-00-00 00:00:00'){
				echo"&nbsp;<a href='$aksi?dom=odc&act=done&nwo=$r[idodc]'><i class='fa fa-question fa-1x'></i></a>&nbsp;"; //unem.php?dom=done&vnowo=$r[id_worde]
				}else{echo"&nbsp;<a href='#' data-toggle='tooltip' title='Dibuat $r[tg_cre]'><i class='fa fa-check fa-1x'></i></a>&nbsp;"; }
			}else{
				echo"";
			}

		echo"$no</td>
             <td bgcolor = $color>$r[nama]</td>
             <td bgcolor = $color>$r[alamat]</td>
			 <td bgcolor = $color align=center><a href='https://www.google.com/maps/@$r[kordinat],21z' target='_blank'>$r[kordinat]</a></td>
			 <td bgcolor = $color align=right>$r[kapasitas]</td>
			 <td bgcolor = $color align=right>$sisa</td>
		 
			 
			<td bgcolor = $warna><center>";

			if ($_SESSION[leveluser]=='admin'){
				echo"&nbsp;<small><a href=?dom=odc&act=pdoide&ido=$r[idodc]&nmo=$txtnacu><i class='fa fa-pencil-square fa-2x'></i></a></small>&nbsp;";
				if($sisa==0){
				echo"&nbsp;<small><a href='?dom=odc&act=pdoeiv&ido=$r[idodc]&nmo=$txtnacu' data-toggle='tooltip' title='Kapasitas Penuh'><i class='fa fa-times fa-2x'></i></a></small>&nbsp;";
				}else{
				echo"&nbsp;<small><a href=?dom=odc&act=dpodda&ido=$r[idodc]&nmo=$txtnacu><i class='fa fa-plus fa-2x'></i></a></small>&nbsp;";
				}
			}else{
				echo"&nbsp;<small><a href='?dom=odc&act=pdoeiv&ido=$r[idodc]&nmo=$txtnacu'><i class='fa fa-sitemap fa-2x'></i></a</small>nbsp;";
			}
				
				echo"</center></td>
			 </tr>
			 ";
      $no++;
    }
    echo "</tbody></table></div>";
	  




	break;
 
 case "dpodda":

 $nmcs=strtoupper($_GET[nmo]);
     if ($_SESSION[leveluser]=='admin'){	
	
      $sumtam= mysqli_query($conn, "SELECT count(idodc) as jmhodc, sum(kapasitas) as jmhkap FROM t_odc where idodc='$_GET[ido]'"); 
      $a=mysqli_fetch_array($sumtam); 

      $sumtap= mysqli_query($conn, "SELECT count(idocpr) as jmhpel FROM t_odc_rel where idodc='$_GET[ido]'"); 
      $b=mysqli_fetch_array($sumtap); 

      $sumpak= mysqli_query($conn, "SELECT count( idodc ) AS jmhpak FROM t_odp WHERE idodc='$_GET[ido]'"); 
      $c=mysqli_fetch_array($sumpak);

	  $tampil = mysqli_query($conn, "SELECT * FROM t_odp WHERE idodc='$_GET[ido]'");	  	
      

	  echo "<h2>Pemakaian ODC <font style='color:green'>$nmcs</font></h2><br>

	  <hr/>";

$tsedia=$a[jmhkap]-$c[jmhpak];

echo" 		<form method=POST enctype='multipart/form-data' action='$aksi?dom=odc&act=tedisi'>
				
				<div class='form-group row'>
					<label for='ido' class='col-sm-2 form-control-label'>Nama / Kode ODC</label>
					<div class='col-sm-10'>
					  <input type='hidden' name='idodc' class='form-control' id='idodc' value='$_GET[ido]'>
					  <input type='text' name='nama' class='form-control' id='nama' value='$_GET[nmo]' readonly>
					</div>

				</div>
			    <div class='form-group row'>

					<label for='usrid' class='col-sm-2 form-control-label'>Untuk ODP</label>
					<div class='col-sm-4'>
					 <!-- <input type='text' name='userid' class='form-control' id='userid' required> -->
					<select name='idodp' class='form-control' id='idodp'>";
			 
			          $tampilx=mysqli_query($conn, "SELECT * FROM t_odp WHERE idodc=0 ORDER BY nama");
			          if ($r[nama]==''){
			            echo "<option value=0 selected>- Pilih ODP -</option>";
			          }   

			          while($w=mysqli_fetch_array($tampilx)){

			            if ($r[nama]==$w[nama]){
			              echo "<option value=$w[idodp]|$x|$y selected>$w[nama]</option>";
			            }
			            else{
			              echo "<option value=$w[idodp]|$x|$y>$w[nama]</option>";
			            }
			          }
			    echo "</select>
			    	 </div>	

			    	<label for='nopo' class='col-sm-2 form-control-label'>Port ODC</label>
					<div class='col-sm-4'>
					  <input type='text' name='odcport' class='form-control' id='odcport' required>
					</div>					
					</div>	

			
			    <div class='form-group row'>
				    <label for='tombol' class='col-sm-2 form-control-label'></label>
					<div class='col-sm-10'>
					  <button type='submit' name='pdoisi' class='btn btn-info'><i class='fa fa-plus-circle fa-2x'></i><br> <sup><small>TAMBAH</small></sup></button>
					  <button type='button' onclick=\"window.location.href='unem.php?dom=odc';\" class='btn btn-info'><i class='fa fa-arrow-left fa-2x'></i><br> <sup><small>KEMBALI</small></sup></button>
					</div>
    			</div>
				
				</form>";

    
    echo "

	<div class='form-group row'>
	    <div class='col-sm-4'>
			<div class='panel panel-primary'>
		      <div class='panel-heading text-center'><h5>Kapasitas</h5></div>
		      <div class='panel-body text-center'><h4>$a[jmhkap]</h4></div>
		    </div>
	    </div>

	    <div class='col-sm-4'>
			<div class='panel panel-warning'>
		      <div class='panel-heading text-center'><h5>Digunakan</h5></div>
		      <div class='panel-body text-center'><h4>$c[jmhpak]</h4></div>
		    </div>
	    </div>

	    <div class='col-sm-4'>
			<div class='panel panel-success'>
		      <div class='panel-heading text-center'><h5>Tersedia</h5></div>
		      <div class='panel-body text-center'><h4>$tsedia</h4></div>
		    </div>
	    </div>
    </div>

	<br>
	<table class='table table-bordered'>
		<thead class='btn-info'>
          <tr>
			  <th>#</th>
			  <th>Port ODC</th>
			  <th>Kode ODP</th>
			  <th>Kordinat</th>
			  <th>&nbsp;</th>
		
		  </tr>
		</thead>"; 
    $no=1;
    while ($r=mysqli_fetch_array($tampil)){
		if($no%2){$color="#aecbff";}else{$color="#deeaff";}
       echo "<tbody>
			 <tr bgcolor = $color>
			 <td>$no</td>
			 <td>$r[odcport]</td>
             <td>$r[nama]</td>
			 <td>$r[kordinat]</td>
             <td>

             <a href='$aksi?dom=odc&act=supah&idop=$r[idodp]&nmo=$_GET[nmo]'><i class='fa fa-trash fa-2x'></i></a></center></td>
			 </tr>
			 </tbody>";
      $no++;
    }
    echo "</table>";
	}else{
		
	  echo "<h2>Maaf tidak ada otorisasi<font style='color:green'></font></h2>
						<hr/>
      <br/><br/>";
			

	}
	
    break;
 
  
 case "pdoeiv":

	$tampil = mysqli_query($conn, "SELECT * FROM t_odp_rel WHERE idodp='$_GET[ido]'");		   
	   
	   if ($_SESSION[leveluser]=='admin'){
	   echo "<h2>Detil ODC<sup> a</sup></h2>";
	   
	   }else{
	   	echo "<h2>Detil ODC<sup> u</sup></h2>";
	   }

	  
   echo "
	<button type='button' onclick=\"window.location.href='unem.php?dom=odc';\" class='btn btn-info'><i class='fa fa-arrow-left fa-2x'></i><br> <sup><small>KEMBALI</small></sup></button><hr/>

    <div class='container' style='width:98%;'>
		  <table class='table table-striped table-bordered dax'>
		<thead class='btn-info'>
          <tr>
			  <th>#</th>

			  <th>Port ODC</th>
			  <th>Nama ODP</th>
			  <th>kordinat</th>
			  <th>&nbsp;</th>
		
		  </tr>
		</thead>"; 
    $no=1;
    while ($r=mysqli_fetch_array($tampil)){
		if($no%2){$color="#aecbff";}else{$color="#deeaff";}
       echo "<tbody>
			 <tr bgcolor = $color>
			 <td>$no</td>

             <td>$r[noport]</td>
			 <td>$r[nama]</td>
			 <td>$r[kordinat]</td>
             <td></td>
			 </tr>
			 </tbody>";
      $no++;
    }
    echo "</table>";
		
		
 break;



case "pdoide":

if ($_SESSION[leveluser]=='admin'){	
	
	$edidat= mysqli_query($conn, "SELECT * FROM t_odc WHERE idodc='$_GET[ido]'");	  	
    $d=mysqli_fetch_array($edidat);  
	  echo "<h2>Edit ODC $_GET[nmo]<sup><font style='color:green'>$d[kapasitas]</font></sup></h2><br>

	  <hr/>
<form class='add-item' method=POST action='$aksi?dom=odc&act=napmis'>
		<div class='form-group row'>
					<label for='lblnama' class='col-sm-2 form-control-label'>Nama / Kode ODC</label>
					<div class='col-sm-10'>
					<input type='hidden' name='jumport' class='form-control' id='jumport' value='$d[kapasitas]' >
					<input type='hidden' name='idodc' class='form-control' id='idodp' value='$d[idodc]' readonly>
					  <input type='text' name='nama' class='form-control' id='nama' value='$d[nama]' >
					</div>
		  </div>  
		 <div class='form-group row'>
					<label for='lblalamat' class='col-sm-2 form-control-label'>Alamat </label>
					<div class='col-sm-10'>
					  <input type='text' name='alamat' class='form-control' id='alamat' value='$d[alamat]' >
					</div>
		  </div> 
        <div class='form-group row'>
				<label for='lblkordinat' class='col-sm-2 form-control-label'>Coordinate</label>
				<div class='col-sm-10'>
					  <input type='text' name='kordinat' class='form-control' id='kordinat' value='$d[kordinat]'>
				</div>
				
		</div>	  
		  <div class='form-group row'>
					<label for='lblkapasitas' class='col-sm-2 form-control-label'>Kapasitas</label>
					<div class='col-sm-10'>
					  <input type='text' name='kapasitas' class='form-control' id='kapasitas' value='$d[kapasitas]' >
					</div>
		  </div>
	        
				  
			    <div class='form-group row'>
				    <label for='tombol' class='col-sm-2 form-control-label'></label>
					<div class='col-sm-10'>
					  <button type='submit' name='pdomat' class='btn btn-info'><i class='fa fa-save fa-2x'></i><br> <sup><small>SIMPAN</small></sup></button>
					  <button type='button' onclick=\"window.location.href='unem.php?dom=odc';\" class='btn btn-info'><i class='fa fa-arrow-left fa-2x'></i><br> <sup><small>KEMBALI</small></sup></button>
					</div>
    			</div>
				
				</form>";
    



    

	
	}else{
		
	  echo "<br><h2>Tidak Ada Akses.<font style='color:green'></font></h2>
						<hr/>
      <br/><center> <button type='button' onclick=\"window.location.href='draobd.php?dom=home';\" class='btn btn-info'><i class='fa fa-arrow-left fa-2x'></i><br> <sup><small>KEMBALI</small></sup></button></center><br/>";
			

	}
	
	
	
    break;




	
}



?>
<script type="text/javascript">




   function kokla() {
    
    var dat= document.getElementById("userids").value;
    var arr = dat.split("|");
    var var0 = arr[0];
    var var1 = arr[1];
    var var2 = arr[2];
    console.log(var0);
    console.log(var1);
    console.log(var2);
        document.getElementById("userid").value=var0;
        document.getElementById("pelanggan").value=var1;
        document.getElementById("kordinat").value=var2;
        document.getElementById("noport").focus();
}
</script>
<script>

	$(document).ready(function(){
	  $('[data-toggle="tooltip"]').tooltip();
	});


 /*   $(document).ready(function() {

        $(".add-item").validate({
            errorClass: "error text-danger",
            validClass: "success text-success",
            rules: {
                nama: "required",
                alamat: "required",
                kordinat: "required",
                kapasitas: "required",
            },
            messages: {
                nama: {
                    required: "Nama harus diisi"
                },
                alamat: {
                    required: "Alamat harus diisi"
                },
                kordinat: {
                    required: "Kordinat harus diisi (copas dari google)"
                },
                kapasitas: {
                    required: "Jumlah Port/Kapasitas harus diisi"
                },
            }
        });
    });*/

</script>

