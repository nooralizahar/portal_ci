
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
        <li class="active">Surat Terkirim</li>
    </ol>
</div><!--/.row--><br />

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Surat TerkirimX</div>
            <div class="panel-body">
                <table data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                       <!-- <th data-field="id_pesan"  data-sortable="true">ID Surat</th> -->
                        <th data-field="agenda"  data-sortable="true">Agenda</th>
                        <th data-field="sifat"  data-sortable="true">Sifat</th>
                        <th data-field="nota"  data-sortable="true">Ket.</th>
                        <!-- <th data-field="subjek"  data-sortable="true">Penerima</th> -->
                        <th data-field="isi_pesan"  data-sortable="true">Ringkasan</th>
                        <th data-field="waktu" data-sortable="true">Waktu</th>
                        <th>Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($dikirim as $sr): ?>
                        <tr>
                           <!-- <td align=center><?php // echo $sr->id_pesan; ?></td> -->
                            <td align=center><?php echo $sr->nomor_agenda; ?></td>
                            <td align=center><?php echo $sr->sifat; ?></td>
                            <td align=center><?php if($sr->nota==1){echo "Nota Dinas";}else{echo "Surat";}; ?></td>
                          <!--  <td align=center>

                            <?php //foreach($dikirim as $key=>$penerima):?>
                                <?php //if($penerima->dibaca == 1): ?>
                                      <b style='background-color: rgba(6, 142, 113, 0.76); color: white;'><?php //echo $key+1 . ". " . $penerima->ke_user; ?> <i class="fa fa-eye fa-lg fa-fw"></i></b><br />
                                <?php //else:?>
                                      <b><?php // echo $key+1 . ". " . $penerima->ke_user; ?></b><br />
                                <?php //endif;?>
                            <?php //endforeach;?> 
 
                                
                            </td> -->
                            <!-- <td align=center><?php echo $sr->subjek; ?></td> -->
                            <td align=center><?php echo strip_tags(character_limiter($sr->isi_pesan,20)); ?></td>
                            
                            <td align=center><?php echo date("d-M-y H:m:s",strtotime($sr->waktu_kirim)); ?></td>
                            <td>
                                <a href="<?php echo base_url("panel/baca_outbox/" . $sr->id_pesan); ?>" class="btn btn-success"><i class="fa fa-envelope-open fa-lg"></i><br><small><sup>BACA</sup></small></a>
                              <!--  <a href="#" class="btn btn-warning"><i class="fa fa-trash fa-lg"></i><br><small><sup>HAPUS</sup></small></a> -->
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!--/.row-->

</div><!--/.main-->
