
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/"); ?>"> BOD Dashboard</a></li>
            <li class="active"> Work Order</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="color:#010288"><b>Daftar Work Order <?php if(strlen($pil)==4){ echo "Tahun ".$pil;}else{echo "Bulan ".date("M-Y",strtotime($pil));} ?></b></h4>
                </div>
        <div class="panel-body">

   <br>

        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="nowo"  data-sortable="true">No. WO</th>
                        <th data-field="pelg"  data-sortable="true">Pelanggan</th>
                        <th data-field="urai"  data-sortable="true">Uraian</th>
                        <th data-field="tgrfs"  data-sortable="true">RFS </th>
                        <th data-field="vend"  data-sortable="true">vendor</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($d_word as $dpro): ?>
                        <tr>
                            <td><?php echo $dpro->id_worde; ?></td>
                            <td><?php echo $dpro->nm_custo; ?></td>
                            <td><?php echo $dpro->sv_custo."&rarr;".$dpro->ca_custo; ?></td>
                            <td><?php echo $dpro->tg_erefs; ?></td>
                            <td><?php echo $dpro->nm_ven; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>

        </div>
      </div>
    </div>

    </div><!--/.row end-->





