
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
        <li class="active">Kelola Bagian</li>
    </ol>
</div><!--/.row--><br />



<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Daftar Bagian</div>
            <div class="panel-body">
                <a data-toggle='modal' data-target='#divtamb' href='#' class="btn btn-success"><i class="fa fa-plus-circle fa-lg fa-fw"></i><br><small><sup>BARU</sup></small></a>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah divisi! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Tambah divisi berhasil! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>


                <table data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="nama_div"  data-sortable="true">Nama Bagian</th>
              
                        <th>Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($daftar_divisi as $divisi): ?>
                        <tr>
                            <td><?php echo $divisi->nm_div; ?></td>
                            <td>
                                <a href="<?php echo base_url("panel/divisiedit/".$divisi->id_div); ?>" class="btn btn-primary"><i class="fa fa-pencil fa-lg"></i><br><small><sup>EDIT</sup></small></a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!--/.row-->



</div><!--/.main-->


<div id="divtamb" class="modal fade" role="dialog">
    
  <div class="modal-dialog modal-sm">
        <form action="<?php echo base_url('panel/tambdiv/')?>" method="POST" enctype="multipart/form-data">
     <!-- Isi Modal-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
        <center><h4 class="modal-title">Bagian Baru</h4></center>
      </div>
      <div class="modal-body">
        <div class="form-group row">
            <div class="col-sm-12">
            <input type="text" class="form-control" name="nm_div" id="nm_div" value="">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <center>
         <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
         <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle fa-lg"></i><br><small><sup>BATAL</sup></button>
       </center>
      </div>
    </div>

    </form>
  </div>
</div>