<?php 
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/mtainmon/"); ?>"> Maintenance Info</a></li>
            <li class="active"> Edit Data</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h3 style="color:#010288"><b> Edit Info </b></h3>
                </div>
        <div class="panel-body">

<form method=POST enctype='multipart/form-data' action='$aksi?dom=ctmofni&act=tupni'>
                
                <div class='form-group row'>
                    <label for='tgmtc1' class='col-sm-2 form-control-label'>Tanggal</label>
                    <div class='col-sm-3'>
                      <input type='date' name='tgmtc' class='form-control' id='tgmtc1' required>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='jmmtc1' class='col-sm-2 form-control-label'>Waktu (Durasi)</label>
                    <div class='col-sm-2'>
                      <input type='text' name='jmmtc' class='form-control' id='jmmtc1'required>
                    </div>
                    <label for='jamul1' class='col-sm-1 form-control-label'>s/d</label>
                    <div class='col-sm-2'>
                      <input type='text' name='jsmtc' class='form-control' id='jsmtc1'required>
                    </div>
                </div>

                <div class='form-group row'>
                    <label for='dntime1' class='col-sm-2 form-control-label'>Lama Downtime </label>
                    <div class='col-sm-3'>
                      <input type='text' name='dntime' class='form-control' id='dntime1' required>
                    </div>
                    <label for='dntime2' class='col-sm-7 form-control-label'>menit</label>
                </div>
                
                <div class='form-group row'>
                    <label for='demtc1' class='col-sm-2 form-control-label'>Deskripsi</label>
                    <div class='col-sm-10'>
                      <input type='text' name='demtc' class='form-control' id='demtc1' required>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='immtc1' class='col-sm-2 form-control-label'>Impact</label>
                    <div class='col-sm-10'>
                    <textarea name='immtc' class='ckeditor form-control' id='immtc1' rows='10' required></textarea>
                      
                    </div>
        
                </div>

                <div class='form-group row'>
                    <label for='nmven' class='col-sm-2 form-control-label'>Vendor</label>
                    <div class='col-sm-10'>
                      <input type='text' name='nmven' class='form-control' id='nmven1'required>
                    </div>  
        
                    
                </div>  
 
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/mtainmon') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>

    <br>

        </div>
      </div>
    </div>

    </div><!--/.row end-->






    

    <form action="" method="post" style="display: none;">
        <input type="hidden" name="password" id="confirmPassword">
        <input type="submit" name="selesaiBtnSubmit" id="selesaiBtnSubmit">
    </form>

</div>	<!--/.main-->

