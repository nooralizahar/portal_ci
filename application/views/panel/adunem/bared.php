<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("datbar/logboo/"); ?>"> Buku Log</a></li>
            <li class="active"> Edit</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">

        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#otbplat" aria-controls="otbplat" role="tab" data-toggle="tab"> Edit OTB</a></li>

                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="otbplat">
    <div class="panel-body">
        <div class="panel panel-primary">

           <div class="panel-body">


        <form class='add-item' method=POST action=''>
<?php foreach($logpil as $l):?> 
        <div class='form-group row'>
                <label for='lbltglog' class='col-sm-2 form-control-label'>Tanggal</label>
                <div class='col-sm-4'>
                    <input type='date' name='tglog' class='form-control' id='tglog' value="<?= $l->tglog; ?>" required>
                    
                </div>
                <label for='lblusr' class='col-sm-2 form-control-label'>Pemakai</label>
                <div class='col-sm-4'>
                    <select name="nmusr" id="nmusr" class="form-control pilusr">
                                <option value="">-- Pilih Pemakai --</option>       
                                <?php foreach($pmakai as $p):?>
                                    <option value="<?php echo $p->id_pengguna; ?>" <?php echo ($l->nmusr == $p->id_pengguna) ? "selected" : ""; ?>><?php echo $p->nama_lengkap; ?></option>
                                <?php endforeach;?>
                    </select>
                    
                </div>

          </div>  
        <div class='form-group row'>
                <label for='lblnmtoo' class='col-sm-2 form-control-label'>Tools</label>
                <div class='col-sm-4'>
                    <input type='hidden' name='id' class='form-control' id='id' value="<?= $l->id; ?>">
                       <input type='text' name='nmtoo' class='form-control' id='nmtoo' value="<?= $l->nmtoo; ?>">
                </div>
                <label for='lblnmper' class='col-sm-2 form-control-label'>Perangkat</label>
                <div class='col-sm-4'>
                     <input type='text' name='nmper' class='form-control' id='nmper' value="<?= $l->nmper; ?>">
                </div>

        </div> 

        <div class='form-group row'>
                <label for='lblnmtoo' class='col-sm-2 form-control-label'>Jam Keluar</label>
                <div class='col-sm-4'>
                       <input type='time' name='jakel' class='form-control' id='jakel' value="<?= $l->jakel; ?>">
                </div>
                <label for='lblnmper' class='col-sm-2 form-control-label'>Jam Masuk</label>
                <div class='col-sm-4'>
                     <input type='time' name='jamas' class='form-control' id='jamas'value="<?= $l->jamas; ?>">
                </div>
        </div>

        <div class='form-group row'>
                <label for='lblketer' class='col-sm-2 form-control-label'>Keterangan</label>
                <div class='col-sm-10'>
                       <input type='text' name='keter' class='form-control' id='keter' value="<?= $l->keter; ?>" required>
                </div>

        </div> 
<?php endforeach;?>
   
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                <button type='button' onclick="goBack()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>





             </div>
         </div>
     </div>
</div>



                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

<script>


    $(document).ready(function(){
        $(".pilusr").select2();

    })

    function goBack() {
  		window.history.back();
	}
</script>