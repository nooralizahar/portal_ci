<div class="row">

        <div class='form-group row'>
            
            <div class='col-sm-10'>
                <a href="<?php echo base_url("datbar/eksporxls/3"); ?>" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg"></i> <br><small><sup>EXCEL</sup></small></a>&nbsp;
                <!-- <button type='button' onclick="window.location.href = '<?php echo base_url('datbar/logbar/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button> -->
            </div>
        </div>


        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-2">  </div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-blue panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked wireless router"><use xlink:href="#stroked-wireless-router"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php $tper=$this->db->where("jebrg",3)->where("gudang",$this->session->userdata("gudang"))->get("foc_logbrg")->num_rows(); echo number_format($tper); ?></a></div>
                        <div class="text-muted" > Perangkat</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-merah panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked download"><use xlink:href="#stroked-download"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->where("jebrg",3)->where("gudang",$this->session->userdata("gudang"))->where("tgmas !=","0000-00-00")->get("foc_logbrg")->num_rows()); ?></div>
                        <div class="text-muted"> Masuk</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked upload"><use xlink:href="#stroked-upload"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $kper=$this->db->where("jebrg",3)->where("gudang",$this->session->userdata("gudang"))->where("tgkel !=","0000-00-00")->get("foc_logbrg")->num_rows(); echo number_format($kper); ?></div>
                        <div class="text-muted"> Keluar</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-merah-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked notepad"><use xlink:href="#stroked-notepad"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $sper=$tper-$kper; echo number_format($sper); ?></div>
                        <div class="text-muted"> Stok/Sisa</div>
                    </div>
                </div>
            </div>
        </div>
</div> <br>




<table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="a" data-sort-order="desc">
                    <thead>
                    
                    <tr>
                        <th data-field="a"  data-sortable="true" class="text-center">Type</th>
                        <th data-field="b"  data-sortable="true" class="text-center">SN</th>
                        <th data-field="c"  data-sortable="true" class="text-center">Tgl. Masuk</th>
                        <th data-field="d"  data-sortable="true" class="text-center">Tgl. Keluar</th>
                        <th data-field="e"  data-sortable="true" class="text-center">Peruntukan</th>
                        <th data-field="f"  data-sortable="true" class="text-center">PIC</th>
                        <th data-field="g"  data-sortable="true" class="text-center">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($logper as $dat): ?>
                        <tr>
                            <td class="text-center"><?php echo $dat->tybrg; ?></td>
                            <td class="text-center"><?php echo $dat->snbrg; ?></td>
                            <td class="text-left"><?php echo $dat->tgmas; ?></td>
                            <td class="text-left"><?php echo $dat->tgkel; ?></td>
                            <td class="text-left"><?php echo $dat->untuk; ?></td>
                            <td class="text-center"><?php echo $dat->pic; ?></td>
                            <td><center>
                                <a href="<?php echo base_url("datbar/brged/".$dat->id); ?>" data-toggle='tooltip' title='Ubah Barang'><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp; <!-- Edit OTB -->
                               <!-- <a href="<?php echo base_url("otbmen/odpli/".$dat->id_otb); ?>" data-toggle='tooltip' title='Tambah Pelanggan'><i class="fa fa-plus fa-lg"></i></a>&nbsp;&nbsp;   -->
                                <?php if($this->session->userdata('level')=='admin'): ?>
                                    <a href="<?php echo base_url("datbar/brgha/".$dat->id); ?>" data-toggle='tooltip' title='Hapus Barang'><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;
                                <?php endif;?>
                                <!--<?php if($dat->fileqr==null): ?>
                                <a href="<?php echo base_url("otbmen/odpqr/".$dat->id_otb); ?>" data-toggle='tooltip' title='Generate'><i class="fa fa-qrcode fa-lg"></i></a>
                                <?php else :?>
                                <a href="<?php echo base_url("otbmen/odpce/".$dat->id_otb); ?>" data-toggle='tooltip' title='Cetak' ><i class="fa fa-print fa-lg"></i></a>
                                <?php endif;?>-->
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
</table>