<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("datbar/logboo/"); ?>"> Barang Log</a></li>
            <li class="active"> Edit</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">

        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#otbplat" aria-controls="otbplat" role="tab" data-toggle="tab"> Edit Barang</a></li>

                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="otbplat">
    <div class="panel-body">
        <div class="panel panel-primary">

           <div class="panel-body">


<form class='add-item' method=POST action=''>
 <?php

 ?>
        <div class='form-group row'>
                <input type='hidden' name='id' class='form-control' id='id' value="<?=$pb->id;?>">
                <label for='lbltglog' class='col-sm-2 form-control-label'>Jenis Barang</label>
                <div class='col-sm-4'>
                    <select name="jebrg" id="jebrg" class="form-control pilnis">
                                <option value="">-- Pilih Jenis --</option>       
                                <?php foreach($nisbar as $n):?>
                                    <option value="<?php echo $n->jebrg; ?>" <?php echo ($pb->jebrg == $n->jebrg) ? "selected" : ""; ?>><?php echo $n->debrg; ?></option>
                                <?php endforeach;?>
                    </select>
                    
                </div>
                <label for='lblusr' class='col-sm-2 form-control-label'>PIC</label>
                <div class='col-sm-4'>
                    <select name="pic" id="pic" class="form-control pilpic" style="width:100%">
                                <option value="">-- Pilih Pemakai --</option>
                                <?php foreach($pmakai as $x):?>
                                    <option value="<?php echo $x->id_pengguna; ?>" <?php echo ($pb->pic == $x->id_pengguna) ? "selected" : ""; ?>>

                                        <?php echo $x->nama_lengkap; ?></option>
                                <?php endforeach;?>
                    </select>
                    
                </div>

          </div>  
        <div class='form-group row'>
                <label for='lblnmtoo' class='col-sm-2 form-control-label'>Type</label>
                <div class='col-sm-4'>
                       <input type='text' name='tybrg' class='form-control' id='tybrg' value="<?php echo $pb->tybrg; ?>" /readonly>
                </div>
                <label for='lblnmper' class='col-sm-2 form-control-label'>Serial No.</label>
                <div class='col-sm-4'>
                     <input type='text' name='snbrg' class='form-control' id='snbrg' value="<?php echo $pb->snbrg; ?>" /readonly>
                </div>

        </div> 

        <div class='form-group row'>
                <label for='lblnmtoo' class='col-sm-2 form-control-label'>Tgl. Masuk</label>
                <div class='col-sm-4'>
                       <input type='date' name='tgmas' class='form-control' id='tgmas' value="<?php echo $pb->tgmas; if($pb->tgmas=='0000-00-00'){$ro='';}else{$ro='/readonly';}?>" <?=$ro?>>
                </div>
                <label for='lblnmper' class='col-sm-2 form-control-label'>Tgl. Keluar</label>
                <div class='col-sm-4'>
                     <input type='date' name='tgkel' class='form-control' id='tgkel'>
                </div>
        </div>

        <div class='form-group row'>
                <label for='lblketer' class='col-sm-2 form-control-label'>Peruntukan</label>
                <div class='col-sm-10'>
                       <input type='text' name='untuk' class='form-control' id='untuk'  value="<?php echo $pb->untuk; ?>" >
                </div>
        </div>
   
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                <button type='button' onclick="goBack()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>

             </div>
         </div>
     </div>
</div>



                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

<script>


    $(document).ready(function(){
        $(".pilpic").select2();

    })

    function goBack() {
  		window.history.back();
	}
</script>