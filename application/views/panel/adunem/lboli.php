<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("otbmen/"); ?>"> Log</a></li>
            <li class="active"> Buku</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Buku Log</h5>
                </div>
        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#requester" aria-controls="requester" role="tab" data-toggle="tab"> Log Baru</a></li>
                            <li role="presentation" class="active"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Log Semua</a></li>
                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane" id="requester">
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">
        <form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lbltglog' class='col-sm-2 form-control-label'>Tanggal</label>
                <div class='col-sm-4'>
                    <input type='date' name='tglog' class='form-control' id='tglog' required>
                    
                </div>
                <label for='lblusr' class='col-sm-2 form-control-label'>Pemakai</label>
                <div class='col-sm-4'>
                    <select name="nmusr" id="nmusr" class="form-control pemakai" required style="width:100%">
                                <option value="">-- Pilih Pemakai --</option>
                                <?php foreach($pmakai as $a):?>
                                    <option value="<?php echo $a->id_pengguna; ?>">

                                        <?php echo $a->nama_lengkap; ?></option>
                                <?php endforeach;?>
                    </select>
                    
                </div>

          </div>  
        <div class='form-group row'>
                <label for='lblnmtoo' class='col-sm-2 form-control-label'>Tools</label>
                <div class='col-sm-4'>
                       <input type='text' name='nmtoo' class='form-control' id='nmtoo'>
                </div>
                <label for='lblnmper' class='col-sm-2 form-control-label'>Perangkat</label>
                <div class='col-sm-4'>
                     <input type='text' name='nmper' class='form-control' id='nmper'>
                </div>

        </div> 

        <div class='form-group row'>
                <label for='lblnmtoo' class='col-sm-2 form-control-label'>Jam Keluar</label>
                <div class='col-sm-4'>
                       <input type='time' name='jakel' class='form-control' id='jakel'>
                </div>
                <label for='lblnmper' class='col-sm-2 form-control-label'>Jam Masuk</label>
                <div class='col-sm-4'>
                     <input type='time' name='jamas' class='form-control' id='jamas'>
                </div>
        </div>

        <div class='form-group row'>
                <label for='lblketer' class='col-sm-2 form-control-label'>Keterangan</label>
                <div class='col-sm-10'>
                       <input type='text' name='keter' class='form-control' id='keter'>
                </div>

        </div>

   
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('otbmen/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
             </div>
         </div>
     </div>
</div>


<div role="tabpanel" class="tab-pane active" id="problem">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"><?php 
            //echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-2">  </div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-jingga panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("foc_logbook")->num_rows()); ?></a></div>
                        <div class="text-muted" > Total Log</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar blank"><use xlink:href="#stroked-calendar-blank"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($lhrini); ?></div>
                        <div class="text-muted"> Hari Ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($lblini); ?></div>
                        <div class="text-muted"> Bulan Ini</div>
                    </div>
                </div>
            </div>
        </div>

</div> <br>




        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sortable="false"  >
                    <thead>
                    
                    <tr>
                        <th data-field="i"  data-sortable="true" class="text-center">Hari</th>
                        <th data-field="a"  data-sortable="true" class="text-center">Tanggal</th>
                        <th data-field="b"  data-sortable="true" class="text-center">Pemakai</th>
                        <th data-field="c"  data-sortable="true" class="text-center">Tools</th>
                        <th data-field="d"  data-sortable="true" class="text-center">Perangkat</th>
                        <th data-field="e"  data-sortable="true" class="text-center">Keluar</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Masuk</th>
                        <th data-field="g"  data-sortable="true" class="text-center">Keterangan</th>
                        <th data-field="h"  data-sortable="true" class="text-center">Opsi</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($logboo as $dat): ?>
                        <tr>
                            <td class="text-center"><?php echo hari_indo($dat->tglog); ?></td>
                            <td class="text-center"><?php echo tgl_indosm($dat->tglog); ?></td>
                            <td class="text-left"><?php echo $dat->nama_lengkap; ?></td>
                            <td class="text-left"><?php echo $dat->nmtoo; ?></td>
                            <td class="text-left"><?php echo $dat->nmper; ?></td>
                            <td class="text-center"><?php echo $dat->jakel; ?></td>
                            <td class="text-center"><?php echo $dat->jamas; ?></td>
                            <td class="text-left" ><?php echo $dat->keter; ?></td>
                            <td><center>
                                <a href="<?php echo base_url("datbar/loged/".$dat->id); ?>" data-toggle='tooltip' title='Ubah Log'><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp; <!-- Edit OTB -->
                               <!-- <a href="<?php echo base_url("otbmen/odpli/".$dat->id_otb); ?>" data-toggle='tooltip' title='Tambah Pelanggan'><i class="fa fa-plus fa-lg"></i></a>&nbsp;&nbsp;   -->
                                <?php if($this->session->userdata('level')=='admin'): ?>
                                	<a href="<?php echo base_url("datbar/logha/".$dat->id); ?>" data-toggle='tooltip' title='Hapus Log'><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;
                                <?php endif;?>
                                <!--<?php if($dat->fileqr==null): ?>
                                <a href="<?php echo base_url("otbmen/odpqr/".$dat->id_otb); ?>" data-toggle='tooltip' title='Generate'><i class="fa fa-qrcode fa-lg"></i></a>
                                <?php else :?>
                                <a href="<?php echo base_url("otbmen/odpce/".$dat->id_otb); ?>" data-toggle='tooltip' title='Cetak' ><i class="fa fa-print fa-lg"></i></a>
                                <?php endif;?>-->
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

<script>

    $(document).ready(function(){
        $(".pemakai").select2();

    })

</script>