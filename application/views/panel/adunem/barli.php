<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("otbmen/"); ?>"> Stok</a></li>
            <li class="active"> Barang</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Stok Barang <?php echo $this->session->userdata("gudang")?></h5>
                </div>
        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#baru" aria-controls="baru" role="tab" data-toggle="tab"> Baru</a></li>
                            <li role="presentation" class="active"><a href="#sfp" aria-controls="sfp" role="tab" data-toggle="tab"> SFP</a></li>
                            <li role="presentation"><a href="#patchcore" aria-controls="patchcore" role="tab" data-toggle="tab"> Patchcord</a></li>
                            <li role="presentation"><a href="#perangkat" aria-controls="perangkat" role="tab" data-toggle="tab"> Perangkat</a></li>
                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane" id="baru">
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">
        <form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lbltglog' class='col-sm-2 form-control-label'>Jenis Barang</label>
                <div class='col-sm-4'>
                    <select name="jebrg" id="jebrg" class="form-control" required>
                        <option value="1">SFP</option>
                        <option value="2">Patchcord</option>
                        <option value="3">Perangkat</option>
                    </select> 

                   <!-- <select name="jebrg" id="jebrg" class="form-control jebar" style="width:100%">
                                <option value="">-- Pilih Jenis--</option>
                                <?php foreach($nisbar as $b):?>
                                    <option value="<?php echo $b->jebrg; ?>">

                                        <?php echo $b->nama; ?></option>
                                <?php endforeach;?>-->
                    </select>
                    
                    
                </div>
                <label for='lblusr' class='col-sm-2 form-control-label'>PIC</label>
                <div class='col-sm-4'>
                    <select name="pic" id="pic" class="form-control pemakai" style="width:100%">
                                <option value="">-- Pilih Pemakai --</option>
                                <?php foreach($pmakai as $a):?>
                                    <option value="<?php echo $a->id_pengguna; ?>">

                                        <?php echo $a->nama_lengkap; ?></option>
                                <?php endforeach;?>
                    </select>
                    
                </div>

          </div>  
        <div class='form-group row'>
                <label for='lblnmtoo' class='col-sm-2 form-control-label'>Type / Deskripsi</label>
                <div class='col-sm-4'>
                       <input type='text' name='tybrg' class='form-control' id='tybrg'>
                </div>
                <label for='lblnmper' class='col-sm-2 form-control-label'>Serial No. / Panjang</label>
                <div class='col-sm-4'>
                     <input type='text' name='snbrg' class='form-control' id='snbrg'>
                </div>

        </div> 

        <div class='form-group row'>
                <label for='lblnmtoo' class='col-sm-2 form-control-label'>Tgl. Masuk</label>
                <div class='col-sm-4'>
                       <input type='date' name='tgmas' class='form-control' id='tgmas'>
                </div>
                <label for='lblnmper' class='col-sm-2 form-control-label'>Tgl. Keluar</label>
                <div class='col-sm-4'>
                     <input type='date' name='tgkel' class='form-control' id='tgkel'>
                </div>
        </div>

        <div class='form-group row'>
                <label for='lblketer' class='col-sm-2 form-control-label'>Peruntukan</label>
                <div class='col-sm-10'>
                       <input type='text' name='untuk' class='form-control' id='untuk'>
                </div>

        </div>

   
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('datbar/logbar/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
             </div>
         </div>
     </div>
</div>


<div role="tabpanel" class="tab-pane active" id="sfp">
        
    <div class="panel panel-primary">
        <div class="panel-body">
        	<p>SFP</p>
        	<?php $this->load->view("/panel/adunem/spfli")?>
        </div>
    </div>
                              
</div>

<div role="tabpanel" class="tab-pane" id="patchcore">
    
    <div class="panel panel-primary">
        <div class="panel-body">
			<p>Patchcore</p>
			<?php $this->load->view("/panel/adunem/patli")?>
        </div>
    </div>
    
</div>

 
 <div role="tabpanel" class="tab-pane" id="perangkat">
   
        <div class="panel panel-primary">
           <div class="panel-body">
				<p>Perangkat</p>
				<?php $this->load->view("/panel/adunem/perli")?>
            </div>
        </div>
    
</div>




                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

<script>

    $(document).ready(function(){
        $(".pemakai").select2();

    })

</script>