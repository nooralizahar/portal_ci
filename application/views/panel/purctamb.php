<?php
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;
endforeach;

$norpu= $this->db->where("id_rpu",$idrpu)->get("ts_esacrup")->result();
foreach($norpu as $dat): 
    $rpu=$dat->no_rpu; 
    $utk=$dat->peruntuk;
endforeach;

?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/purcbaru/"); ?>">Isi Barang</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo "No. Req. Pembelian <b>".$rpu."</b> Untuk <b>".$utk."</b>";?></div> <!--$idrpu.-->
                <div class="panel-body">

				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data Gagal disimpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data Berhasil disimpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["herr"])):?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data Gagal dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["hsucc"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data Berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
 <?php if($nmjab!="Purchasing"):?>                    
<div class="row">
                    <form action="" method="POST" enctype="multipart/form-data">

                        <div class="col-md-1">
                            <label>Jumlah</label>
                            <input type="text" name="brgjml" class="form-control" required/><br>
                            <input type="hidden" name="id_rpu" value="<?php echo $idrpu;?>" class="form-control" readonly/>
                            
                        </div>
                        <div class="col-md-1">
                            <label>Satuan</label>
                            <select name="brgstn" id="brgstn" class="form-control" required>
                                     <option value="unit" selected>Unit</option>
                                     <option value="pcs">Pieces</option>
                                     <option value="roll">Roll</option>
                                     <option value="meter">Meter</option>
                                     <option value="pak">Pack</option>
                            </select>
                            
                        </div>
                        <div class="col-md-2">
                            <label>Jenis Barang </label>
                            <input type="text" name="brgjen" class="form-control" required/><br />
                        </div>
                        <div class="col-md-6">
                            <label>Uraian </label>
                            <input type="text" name="brgdet" class="form-control" value="" required/>
                        </div>

                        <!-- <div class="col-md-2">
                            <label>Jumlah Barang</label>
                            <input type="text" name="jmbrg" class="form-control" required/>
                        </div>
                        <div class="col-md-2">
                            <label>Nama Barang </label>
                            <input type="text" name="nmbrg" class="form-control" required/>
                        </div>
                        <div class="col-md-6">
                            <label>Uraian Spesifikasi</label>
                            <input type="text" name="nmbrg" class="form-control" required/><br />
                        </div>-->

                        <div class="col-md-12">
                         <!--   <label>Keterangan</label>
                            <textarea name="ket" id="ckeditor" ></textarea><br />
                            <label>Lampiran</label>
                            <input type="file" multiple id="attach" name="attach[]" class="form-control">
                            <small>format file : .jpg, .png, .pdf, .jpeg, .bmp, .gif, .doc, .docx, .xls, .xlsx, .ppt, .pptx. (maks. 2 MB)</small>--><br />
                            <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                            <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/purcbaru') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                        </div>
                    </form>
</div>
 <?php endif;?>
<div class="row">
    <div class="col-md-12">
                    <br><h4 style="color:#010288"><b><u>DAFTAR BARANG</u></b></h4>
    </div> 
</div>
<div class="row">
     <div class="col-md-12">
        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tgbu"  data-sortable="true">Jumlah</th>
                        <th data-field="norp"  data-sortable="true">Jenis</th>
                        <th data-field="peru"  data-sortable="true">Uraian</th>
                        <?php if($nmjab=="Purchasing"):?>
                        <th data-field="harg"  data-sortable="true">Harga</th>    
                        <?php endif;?>
                        <th>Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($d_barg as $dat): ?>
                        <tr>
                            <td><?php echo $dat->brgjml." ".$dat->brgstn; ?></td>
                            <td><?php echo $dat->brgjen; ?></td>
                            <td><?php echo $dat->brgdet; ?></td>

                        <?php if($nmjab=="Purchasing"):?>
                        <td><?php echo $dat->brghar; ?></td>    
                        <?php endif;?>
                            
                           
                            <td>
                                <center>
                                <a href="<?php echo base_url("panel/purcedit/".$dat->id_brg); ?>" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil-square fa-lg"></i></a> 
                                <?php if($nmjab=="Purchasing"):?>
                                <?php else:?>                         
                                <a href="<?php echo base_url("panel/purchapu/".$dat->id_brg); ?>" data-toggle='tooltip' title='Hapus'><i class="fa fa-trash fa-lg"></i></a>
                                <?php endif;?>
                                 </center>                            
                             
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>
</div>
</div>
                </div>
            </div>
        </div>
    </div><!--/.row-->





</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('ket',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>


    $(document).ready(function(){
        $("#msg").froalaEditor({
            height: 300
        });

        $(".penerima").select2();

        $("#tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>