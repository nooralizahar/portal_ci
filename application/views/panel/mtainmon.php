<?php 
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/mtainmon/"); ?>"> Maintenance Info</a></li>
            <li class="active"> Tambah Data</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data baru berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h3 style="color:#010288"><b> Tambah Info </b></h3>
                </div>
        <div class="panel-body">

<form method=POST enctype='multipart/form-data' action='$aksi?dom=ctmofni&act=tupni'>
                
                <div class='form-group row'>
                    <label for='tgmtc1' class='col-sm-2 form-control-label'>Tanggal</label>
                    <div class='col-sm-3'>
                      <input type='date' name='tgmtc' class='form-control' id='tgmtc1' required>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='jmmtc1' class='col-sm-2 form-control-label'>Waktu (Durasi)</label>
                    <div class='col-sm-2'>
                      <input type='text' name='jmmtc' class='form-control' id='jmmtc1'required>
                    </div>
                    <label for='jamul1' class='col-sm-1 form-control-label'>s/d</label>
                    <div class='col-sm-2'>
                      <input type='text' name='jsmtc' class='form-control' id='jsmtc1'required>
                    </div>
                </div>

                <div class='form-group row'>
                    <label for='dntime1' class='col-sm-2 form-control-label'>Lama Downtime </label>
                    <div class='col-sm-3'>
                      <input type='text' name='dntime' class='form-control' id='dntime1' required>
                    </div>
                    <label for='dntime2' class='col-sm-7 form-control-label'>menit</label>
                </div>
                
                <div class='form-group row'>
                    <label for='demtc1' class='col-sm-2 form-control-label'>Deskripsi</label>
                    <div class='col-sm-10'>
                      <input type='text' name='demtc' class='form-control' id='demtc1' required>
                    </div>
                </div>
                <div class='form-group row'>
                    <label for='immtc1' class='col-sm-2 form-control-label'>Impact</label>
                    <div class='col-sm-10'>
                    <textarea name='immtc' class='ckeditor form-control' id='immtc1' rows='10' required></textarea>
                      
                    </div>
        
                </div>

                <div class='form-group row'>
                    <label for='nmven' class='col-sm-2 form-control-label'>Vendor</label>
                    <div class='col-sm-10'>
                      <input type='text' name='nmven' class='form-control' id='nmven1'required>
                    </div>  
        
                    
                </div>  
 
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/mtainmon') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>



    <br>



        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tgmtc"  data-sortable="true">Tanggal</th>
                        <th data-field="waktu"  data-sortable="true">Waktu</th>
                        <th data-field="durasi"  data-sortable="true">Durasi</th>
                        <th data-field="deskri"  data-sortable="true">Deskripsi</th> 
                        <th data-field="impact"  data-sortable="true">Impact</th>  
                        <th data-field="vendor"  data-sortable="true">Vendor</th>       
                        <th>Publish</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($d_mtain as $dpro): ?>
                        <tr>
                            <td><?php echo $dpro->tgmtc; ?></td>
                            <td><?php echo "<center>".$dpro->jmmtc." s/d ".$dpro->jsmtc."</center>"; ?></td>
                            <td><?php echo "<center>".$dpro->dntime."</center>"; ?></td>
                            <td><?php echo $dpro->demtc; ?></td>
                            <td><?php echo substr(strip_tags($dpro->immtc),0,50)."<b>...</b>"; ?></td>
                            <td><?php echo $dpro->nmven; ?></td>
                            
                           
                            <td>
                              <?php if($dpro->pbsta=='N'){$pub="selesai";}else{$pub="ya";} 
                              echo"<center>"; if ( $nmjab=='admin'){echo"<a href=$dpro->idmtc><i class='fa fa-pencil-square-o fa-1x'></i></a>&nbsp;&nbsp;<a href=$dpro->idmtc><i class='fa fa-trash-o fa-1x'></i></a>&nbsp;&nbsp;";if($pub=='selesai') {echo"";}else{echo"<a href=$dpro->idmtc><i class='fa fa-envelope fa-1x'></i></a>";}}else{ if($pub=='selesai') {echo"<a href=#><i class='fa fa-info-circle fa-1x'></i></a>";}else{echo"<a href=$dpro->idmtc><i class='fa fa-pencil-square-o fa-1x'></i></a>&nbsp;&nbsp;<a href=$dpro->idmtc><i class='fa fa-envelope fa-1x'></i></a>";}} 

                              ?>
                              <small><br><?php echo $pub; ?></small></center>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>

        </div>
      </div>
    </div>

    </div><!--/.row end-->






    

    <form action="" method="post" style="display: none;">
        <input type="hidden" name="password" id="confirmPassword">
        <input type="submit" name="selesaiBtnSubmit" id="selesaiBtnSubmit">
    </form>

</div>	<!--/.main-->

<script>
    $(document).ready(function(){
        $(".attach-img").magnificPopup({
            type: "image"
        });


        $("#isi_disposisi").shorten({
            moreText: "Selengkapnya",
            lessText: "Kecilkan",
            showChars: 300
        });

        $("#attach").fileinput({'showUpload':false});

        $(".attach").on("click",function(ev){
            var data_file = $(this).data().file;
            var content = "<ol>";

            $.each(data_file,function(key,item){
                var cls = item.file.split(".").length;
                var tipe_gambar = ["jpg","png","jpeg","bmp","gif"];
                cls = ($.inArray(item.file.split(".")[cls-1],tipe_gambar) == -1) ? "attach-doc" : "attach-img";
                content += "" +
                    "<li><a class='"+cls+"' href='"+BASE_URL+"assets/uploads/lampiran/follow_up_disposisi/"+item.file+"'>"+item.judul+"</a></li>";
            });

            content += "</ol>";

            bootbox.alert({
                size: "normal",
                title: "Lampiran Tanggapan",
                message: content
            });


            $(".attach-img").magnificPopup({
                type: "image"
            });


            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                $("#myModal").css("z-index",2000);
                return false;
            });


            ev.preventDefault();
        });


        $("#selesai").on("click",function(ev){

            swal({
                title: "Konfirmasi Penyelesaian",
                text: "Masukkan password anda untuk mengkonfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword").val(input);
                $("#selesaiBtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });


        $(function(){
            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                return false;
            });
        })
    });
</script>