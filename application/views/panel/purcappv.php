<?php

$usrnm=$this->session->userdata("username");

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/purcbaru/"); ?>">Permohonan </a></li>
            <li class="active">Detil Pembelian</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12" id="approval">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal mengirim respon! Coba lagi nanti <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Persetujuan berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h3>Detil Permohonan Pembelian</h3>
                </div>
                <div class="panel-body">
                    <b></b><br />

                    <form action="" method="POST" enctype="multipart/form-data">
                    <table class="table table-responsive">
                            <tr>
                                <td>No. PP</td><td>:</td><td><?php echo $d_pusm->no_rpu; ?></td>
                            </tr>
                            <tr>
                                <td>Dari</td><td>:</td><td><?php echo $d_pusm->pengirim; ?></td>
                            </tr>
                            <tr>
                                <td>Untuk</td><td>:</td><td><?php echo $d_pusm->peruntuk; ?></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table class="table table-responsive">
                                        <tr>
                                            <td>Jumlah</td><td>Jenis</td><td>Uraian</td>
                                            <?php if($usrnm=="jpurnama" || $usrnm=="virianto" ):?><td>Harga (Rp)</td><?php endif?>
                                        </tr>
                                        
                                    <?php foreach($d_basm as $dat){
                                    echo "<tr><td align='center'>".$dat->brgjml." </td><td>".$dat->brgjen."</td><td> ".$dat->brgdet."</td>";
                                    if($usrnm=="jpurnama" || $usrnm=="virianto"){
                                    echo "<td> ".number_format($dat->brghar)."</td>";
                                    }

                                    echo"</tr>";

                                   }
                                    ?>
                                    
                                    </table>                                    
                                </td>
                            </tr>
                        <?php if($usrnm=="jpurnama"):?>
                            <?php if($d_pusm->appv2!=null):?>
                                <?php echo "<tr><td colspan=3 class='text-left'><b><small>Permohonan ini disetujui atasan langsung : ".$d_pusm->appv1." pada tgl. ".$d_pusm->tglapv1."</small></b></td></tr>"; ?>
                                <tr style="text-align: center">
                                    <td colspan="3"><?php 
                                       // $this->load->library('ciqrcode');
                                       //  header("Content-Type: image/png");
                                       // $params['data'] = 'This is a text to encode become QR Code';
                                        // $this->ciqrcode->generate($params);
                                    echo "Disetujui Oleh,<br><br>".$d_pusm->appv2."<br>".$d_pusm->tglapv2; ?><br>
                                        <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/purcbaru') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                                    </td>
                                </tr>


                            <?php else: ?>
                            <?php echo "<tr><td colspan=3 class='text-left'><b><small>Permohonan ini disetujui atasan langsung : ".$d_pusm->appv1." pada tgl. ".$d_pusm->tglapv1."</small></b></td></tr>"; ?>
                            <tr style="text-align: center">
                                <td colspan="3"><a id="selesai2" class="btn btn-block btn-success" href=""><i class="fa fa-check fa-lg"></i> Setujui</a></td>
                            </tr>
                            <?php endif;?>

                        <?php elseif($usrnm=="virianto"):?>
                            <?php if($d_pusm->appv3!=null):?>
                                <?php echo "<tr><td colspan=3 class='text-left'><b><small>Permohonan ini disetujui atasan langsung : ".$d_pusm->appv1." pada tgl. ".$d_pusm->tglapv1."</small></b></td></tr>"; ?>
                                <?php echo "<tr><td colspan=3 class='text-left'><b><small>dan disetujui juga oleh : ".$d_pusm->appv2." pada tgl. ".$d_pusm->tglapv2."</small></b></td></tr>"; ?>
                                <tr style="text-align: center">
                                    <td colspan="3"><?php 
                                       // $this->load->library('ciqrcode');
                                       //  header("Content-Type: image/png");
                                       // $params['data'] = 'This is a text to encode become QR Code';
                                        // $this->ciqrcode->generate($params);
                                    echo "Disetujui Oleh,<br><br>".$d_pusm->appv3."<br>".$d_pusm->tglapv3; ?><br>
                                        <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/purcbaru') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                                    </td>
                                </tr>


                            <?php else: ?>
                            <?php echo "<tr><td colspan=3 class='text-left'><b><small>Permohonan ini disetujui atasan langsung : ".$d_pusm->appv1." pada tgl. ".$d_pusm->tglapv1."</small></b></td></tr>"; ?>
                            <?php echo "<tr><td colspan=3 class='text-left'><b><small>dan disetujui juga oleh : ".$d_pusm->appv2." pada tgl. ".$d_pusm->tglapv2."</small></b></td></tr>"; ?>                            
                            <tr style="text-align: center">
                                <td colspan="3"><a id="selesai3" class="btn btn-block btn-success" href=""><i class="fa fa-check fa-lg"></i> Setujui</a></td>
                            </tr>
                            <?php endif;?>


                        <?php else: ?>
                            <?php if($d_pusm->appv1!=null):?>
                                <tr style="text-align: center">
                                    <td colspan="3"><?php 
                                       // $this->load->library('ciqrcode');
                                       //  header("Content-Type: image/png");
                                       // $params['data'] = 'This is a text to encode become QR Code';
                                        // $this->ciqrcode->generate($params);
                                    echo "Disetujui Oleh,<br><br>".$d_pusm->appv1."<br>".$d_pusm->tglapv1; ?><br>
                                        <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/purcbaru') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                                    </td>
                                </tr>


                            <?php else: ?>
                            <tr style="text-align: center">
                                <td colspan="3"><a id="selesai" class="btn btn-block btn-success" href=""><i class="fa fa-check fa-lg"></i> Setujui</a></td>
                            </tr>
                            <!--<tr>
                                   <td colspan="3"><a id="tolak" class="btn btn-block btn-danger" href="#"><i class="fa fa-times fa-lg"></i> Tolak</a></td>
                            </tr>-->
                            <?php endif;?>
                        <?php endif;?>

                     </table>
                    </form>
                </div>
            </div>
        </div>




    </div><!--/.row end-->



<?php if($usrnm=="jpurnama"):?>
    <form action="" method="post" style="display: none;">
        <input type="hidden" name="usr" id="usr" value="<?php echo $usrnm; ?>">
        <input type="hidden" name="password" id="confirmPassword2">
        <input type="submit" name="selesai2BtnSubmit" id="selesai2BtnSubmit">
    </form>
<?php elseif($usrnm=="virianto"):?>
    <form action="" method="post" style="display: none;">
        <input type="hidden" name="usr" id="usr" value="<?php echo $usrnm; ?>">
        <input type="hidden" name="password" id="confirmPassword3">
        <input type="submit" name="selesai3BtnSubmit" id="selesai3BtnSubmit">
    </form>
<?php else: ?>
    <form action="" method="post" style="display: none;">
        <input type="hidden" name="password" id="confirmPassword">
        <input type="submit" name="selesaiBtnSubmit" id="selesaiBtnSubmit">
    </form>

<?php endif;?>
</div>	<!--/.main-->

<script>
    $(document).ready(function(){

        $("#selesai").on("click",function(ev){

            swal({
                title: "Konfirmasi Pesetujuan",
                text: "Masukkan sandi untuk konfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword").val(input);
                $("#selesaiBtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });

        $("#selesai2").on("click",function(ev){

            swal({
                title:"Konfirmasi Pesetujuan",
                text: "Masukkan sandi untuk konfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword2").val(input);
                $("#selesai2BtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });

        $("#selesai3").on("click",function(ev){

            swal({
                title:"Konfirmasi Pesetujuan",
                text: "Masukkan sandi untuk konfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword3").val(input);
                $("#selesai3BtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });        

        /*$("#tolak").on("click",function(ev){

            swal({
                title:"Konfirmasi Penolakan",
                text: "Masukkan sandi untuk konfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword").val(input);
                $("#tolakBtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });*/

    });
</script>