<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-table.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/datepicker3.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/styles.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/stybotn.css");?>" rel="stylesheet">

    <link rel="icon" href="<?php echo base_url("favicon.png"); ?>" type="image/png" sizes="14x5">



    <!-- Include Editor style. -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <!--Icons-->
    <script src="<?php echo base_url("assets/js/lumino.glyphs.js");?>"></script>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url("assets/js/html5shiv.js");?>"></script>
    <script src="<?php echo base_url("assets/js/respond.min.js");?>"></script>
    <![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>






</head>

<body>
<section >
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"> 
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>-->
            <a class="navbar-brand" href="#"><span>ODP&nbsp;</span>READER</a>

            <ul class="user-menu">

                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Tamu<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:history.back()"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kembali</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>
<!-- <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
<ul class="nav menu">
        <li><a href="javascript:history.back()"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Kembali</a></li>
</ul>
</div>-->
<div class="row" style="margin-top: 7px;">     
        <br><center>
    	<h3>Data Gagal Ditemukan</h3>
        <h4><?php //echo $nodok; ?>Tidak Terdaftar</h4>
        <hr>
        <?php //echo $_SERVER['DOCUMENT_ROOT'];?>
        <canvas></canvas>

        <hr></center>
        <ul></ul>

  </div>        
</section>



</body>
</html>
<?php
/* End of file footer.php */
/* Location: ./application/modules/infoeskpn/views/footer.php */