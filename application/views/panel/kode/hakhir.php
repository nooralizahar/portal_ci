<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$account = $this->account->get();
?>
        <ul id="slide-out" class="side-nav">
            <li>
                <div class="user-view">
                    <div class="background">
                        <img src="<?php echo base_url("assets/infoeskpn/images/bg.jpg"); ?>" alt="">
                    </div>
                    <a href=""><img class="circle" src="<?php echo base_url("assets/img/avatar.jpg"); ?>"></a>
                    <a href="#">
                        <span class="white-text name">
                        <?php echo "Tamu"; //$account->name; ?> 
                        </span>
                    </a>
                    <a href="#">
                        <span class="white-text email">
                            <?php //echo $account->email; ?>        
                        </span>
                    </a>
                </div>
            </li>
            <li><a href="<?php echo site_url('infoeskpn/mutama') ?>">Menu Utama</a></li>
            <li><a href="<?php echo site_url('infoeskpn/halpidai') ?>">Pidai Kode QR</a></li>
            <li><a href="<?php echo site_url('infoeskpn/cekasli') ?>">E-SKPN Asli</a></li>
            <li><a href="<?php echo site_url('infoeskpn/halberita') ?>">Berita</a></li>
            <li><a href="<?php echo site_url('infoeskpn/haljuklak') ?>">Juklak</a></li>
            <li><a href="<?php echo site_url('infoeskpn/haldaftar') ?>">Pendaftaran</a></li>
            <li><a href="<?php echo site_url('infoeskpn/halumuman') ?>">Pengumuman</a></li>
            <li><div class="divider"></div></li>
            <li><a href="#">Bantuan</a></li>
        </ul>

        <div id="logoff" class="modal">
            <div class="modal-content">
                <h5>Keluar Aplikasi?</h5>
            </div>
            <div class="modal-footer">
                 <a class="modal-action modal-close waves-effect waves-green btn-flat">Tidak</a>
                <a href="<?php echo site_url('infoeskpn/login/signout') ?>" class="modal-action modal-close waves-effect waves-green btn-flat">Ya</a>
            </div>
        </div>
          
    </section>

	<script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js"); ?>"></script>
	<script src="<?php echo base_url("assets/infoeskpn/materialize/js/materialize.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/angular/angular.min.js") ?>"></script>
    <script src="<?php echo base_url("assets/angular/dirPagination.js"); ?>"></script>
    <script>
        let base_url = '<?php echo site_url('infoeskpn') ?>';

        let app = angular.module('app', ['angularUtils.directives.dirPagination']);

        $(".button-collapse").sideNav();
         //$('.carousel.carousel-slider').carousel({fullWidth: true});
        $('.parallax').parallax();

        $('.modal').modal();

        $('select').material_select();
    
    </script>
    <script src="<?php echo base_url("assets/app/infoeskpn/main-utama.js"); ?>"></script>
    <script src="<?php echo base_url("assets/app/infoeskpn/main-other.js"); ?>"></script>
    <!-- </body></html> -->
</body>
</html>
<?php
/* End of file footer.php */
/* Location: ./application/modules/infoeskpn/views/footer.php */