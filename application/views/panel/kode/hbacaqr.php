<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Scan ODP</title>

    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-table.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/datepicker3.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/styles.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/stybotn.css");?>" rel="stylesheet">

    <link rel="icon" href="<?php echo base_url("favicon.png"); ?>" type="image/png" sizes="14x5">



    <!-- Include Editor style. -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <!--Icons-->
    <script src="<?php echo base_url("assets/js/lumino.glyphs.js");?>"></script>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url("assets/js/html5shiv.js");?>"></script>
    <script src="<?php echo base_url("assets/js/respond.min.js");?>"></script>
    <![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>






</head>

<body>
<section >
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"> 
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>-->
            <a class="navbar-brand" href="#"><span>ODP&nbsp;</span>READER</a>
            <img src="<?php echo base_url("/assets/images/logo.png"); ?>" class="brand-logo center" alt="<?php echo $title; ?>">
            <ul class="user-menu">

                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Tamu<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:history.back()"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kembali</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>
<!-- <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
<ul class="nav menu">
        <li><a href="javascript:history.back()"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Kembali</a></li>
</ul>
</div>-->
<div class="row" style="margin-top: 7px;">     
        <br><center>
    	<h3>Pidai Kode QR</h3>
        <h5>Scan Data ODP</h5>
        <hr>
        <?php //echo $_SERVER['DOCUMENT_ROOT'];?>
        <canvas></canvas>
        <center>
        	<form action="<?php echo base_url('odpscan/periksa');?>" method="post">
                <span>Waktu Pidai <b id="timer">5</b> <b>detik</b>.</span>
        		<div id="isi"></div>
                <div class="form-group">
                    <!-- <button class="btn btn-default" type="submit" name="btnSimpan" >Periksa</button> --> 
                </div>
               <noscript><input type="submit" /></noscript> 
        	</form>
        </center>
        <hr></center>
        <ul></ul>

  </div>        
</section>


        <script src="<?php echo base_url("assets/appcam/js/qrcodelib.js"); ?>"></script>
        <script src="<?php echo base_url("assets/appcam/js/webcodecamjs.js"); ?>"></script>
        <script type="text/javascript">
            var inp = document.getElementById("isi");
        	var txt = "innerText" in HTMLElement.prototype ? "innerText" : "textContent";
            var arg = {
                resultFunction: function(result) {
                	var aChild = document.createElement('li');
                	aChild[txt] = result.format + ' : ' + result.code;
                   // document.querySelector('body').appendChild(aChild);
                    if(result.code!=null){var i="Memidai Sukses, silahkan menunggu!";}else{}

                    inp.innerHTML="<input type='hidden' name='cariqr' value='" + result.code +"'><b style='color:green'>"+i+"</b></center>";
                }
            };
            new WebCodeCamJS("canvas").init(arg).play();
        </script>

        <script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
        
        <script>
            window.onload = function() {
            // Onload event of Javascript
            // Initializing timer variable
            var x = 5;
            var y = document.getElementById("timer");
            // Display count down for 20s
            setInterval(function() {
            if (x <= 6 && x >= 1) {
            x--;
            y.innerHTML = '' + x + '';
            if (x == 1) {
            x = 6;
            }
            }
            }, 1000);



            };   
        </script>


</body>
<SCRIPT LANGUAGE="JavaScript">

    var t = setTimeout("document.forms[0].submit();",20000); //2 seconds measured in miliseconds

</SCRIPT>
</html>
<?php
/* End of file footer.php */
/* Location: ./application/modules/infoeskpn/views/footer.php */