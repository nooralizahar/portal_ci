<!DOCTYPE html>
<html>
<head>
      <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>

    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-table.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/datepicker3.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/styles.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/stybotn.css");?>" rel="stylesheet">

    <link rel="icon" href="<?php echo base_url("favicon.png"); ?>" type="image/png" sizes="14x5">



    <!-- Include Editor style. -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <!--Icons-->
    <script src="<?php echo base_url("assets/js/lumino.glyphs.js");?>"></script>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url("assets/js/html5shiv.js");?>"></script>
    <script src="<?php echo base_url("assets/js/respond.min.js");?>"></script>
    <![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>






</head>

<body>
<section >
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <!--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"> 
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>-->
            <a class="navbar-brand" href="#"><span>ODP&nbsp;</span>READER</a>
            <img src="<?php echo base_url("/assets/images/logo.png"); ?>" class="brand-logo center" alt="<?php echo $title; ?>">
            <ul class="user-menu">

                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Tamu<span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="javascript:history.back()"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Kembali</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>
<!-- <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
<ul class="nav menu">
        <li><a href="javascript:history.back()"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Kembali</a></li>
</ul>
</div>-->
<div class="row" style="margin-top: 7px;">     
        <br><center>
        <?php foreach ($data as $d):?> 
            
        
    	<h3>Data Berhasil Ditemukan</h3>
        <h4><?php echo $d->nama;?> </h4>
        <small><?php echo $d->alamat;?> </small>
        <h4><?php echo $d->kordinat;?> </h4>
        <img src='<?php echo base_url('/assets/qrfiles/').$d->fileqr;?>'>
        <h4><b style="color:green">ODP TERDAFTAR</b></h4>
        <h4>Kapasitas : <?php echo $d->kapasitas; $k=$d->kapasitas;?> port</h4>
        <hr>
        <?php //echo $_SERVER['DOCUMENT_ROOT'];?>
        <!--<canvas></canvas>-->
        <?php endforeach; ?> 
 
        <hr></center>
        <center>
        <h4>Sisa : <?php $s=$k-$pkai; echo $s; ?> port</h4>  
        <table border="1">
                <tr><th>&nbsp;Port&nbsp;&nbsp;</th><th>&nbsp;Pelanggan&nbsp;&nbsp;</th><th>&nbsp;Alamat&nbsp;&nbsp;</th></tr>
        <?php foreach ($dpel as $c):?>
                <tr><td align="center"><?php echo $c->noport;?></td><td><?php echo "&nbsp;".substr($c->pelanggan, 0,3)."XXXXXXXX&nbsp;&nbsp;";?></td><td><?php echo "&nbsp;".$c->kordinat."&nbsp;";?></td></tr>
        <?php endforeach; ?> 
        </table></center>

  </div>        
</section>


<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
</body>
</html>
<?php
/* End of file footer.php */
/* Location: ./application/modules/infoeskpn/views/footer.php */