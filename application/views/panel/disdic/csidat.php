<?php
    $thnini= date('Y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    if(substr($thnbli, 2)==01){$bln=12;}else{$bln='0'.substr($thnbli, 2)-1;}
    $thnblu= ((substr($thnbli,0, 2))-1).$bln;
    $blnini=date('Y/m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 


?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("khusus/"); ?>"> Master Khusus</a></li>
            <li class="active"> CID/SID</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
        <div class="panel-body">
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["suc"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["hsuc"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Hapus Data berhasil <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["herr"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Hapus data gagal <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>                
                <?php endif; ?>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#requester" aria-controls="requester" role="tab" data-toggle="tab">Master Data</a></li>
                            <!--<li role="presentation"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Problem Side</a></li>-->
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="requester">
                                <div class="panel-body">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">

        <form class='add-item' method=POST action=''>

 
        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">C I D&nbsp; &nbsp;</span>
               
                    <input type='text' name='cid' class='form-control' id='cid' value="<?=rand(10000 , 99999)?>"> 
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">S I D &nbsp; &nbsp; &nbsp; &nbsp;</span>
                      <input type='text' name='sid' class='form-control' id='sid'value="<?=rand(10000 , 99999)?>">
                </div>
            </div>    
        </div>  

        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Lokasi</span>
                    <input type='text' name='lok' class='form-control' id='lok'>
                    <!-- <select name="lokasi" id="lokasi" class="form-control lokpil" style="width:90%;">
                                <?php foreach($daflok as $l):?>
                                    <option value="<?php echo $l->t_location_id; ?>"><?php echo $l->area_name." - ".$l->name; ?></option>
                                <?php endforeach;?>
                    </select> -->
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Bandwidth</span>
                       <input type='text' name='ban' class='form-control' id='ban'>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Vendor</span>
                    <input type='text' name='ven' class='form-control' id='ven'>
                </div>
            </div>
        </div> 

        <div class="col-md-6">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Link WO #</span>
                    <select name="won" id="won" class="form-control wonpil" style="width:90%;">
                                <option value=""></option>
                                <?php foreach($dafwon as $w):?>
                                    <option value="<?php echo $w->id_worde; ?>"><?php echo $w->id_worde." - ".$w->nm_custo; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
            </div>
        </div>   
        <div class="col-md-12">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Alamat</span>
                    <input type='text' name='ala' class='form-control' id='ala'>
                </div>
            </div>
        </div> 
   
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('cidsid/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            <a href="<?php echo base_url("cidsid/csiexp"); ?>" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg"></i> <br><small><sup>EXCEL</sup></small></a>
            </div>
        </div>
                
        </form>

           <!-- <a href="<?php echo base_url("cidsid/csitam"); ?>" class="btn btn-success"><i class="fa fa-plus-circle fa-lg"></i> <br><small><sup>BARU</sup></small></a>-->



                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="problem">
                                <div class="panel-body">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>          

<div class="row">
       <!-- <ol class="breadcrumb">
          <li><a href="#"><?php 
            //echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></a></li>

        </ol> 
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-4">  </div>
        </div>
        <hr/> 
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_rodnev")->num_rows()); ?></a></div>
                        <div class="text-muted">Total Vendor</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnlal"); ?>"><?php echo number_format($this->db->where('stat','Active')->get("t_nanaggnal")->num_rows()); ?></a></div>
                        <div class="text-muted">Active</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php echo number_format($this->db->where('stat','Inactive')->get("t_nanaggnal")->num_rows()); ?> </a></div>
                        <div class="text-muted">Inactive</div>
                    </div>
                </div>
            </div>
        </div>-->

</div> 


    <br>



        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="f"  data-sortable="true">WO #</th>
                        <th data-field="g"  data-sortable="true">Pelanggan</th>
                        <th data-field="i"  data-sortable="true">Layanan</th>
                        <th data-field="a"  data-sortable="true">CID</th>
                        <th data-field="b"  data-sortable="true">SID</th>
                        <th data-field="c"  data-sortable="true">Lokasi/Site</th>
                        <th data-field="d"  data-sortable="true">Bandwidth</th> 
                        <th data-field="e"  data-sortable="true">Alamat</th>            
                        <th data-field="h"  data-sortable="true">Opsi</th> 
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafcsi as $c): ?>
                        <tr>
                            <td><?php echo $c->won; ?></td>
                            <td><?php echo $c->nm_custo; ?></td>
                            <td><?php echo $c->sv_custo.'&rarr;'.$c->ca_custo; ?></td>
                            <td><?php echo $c->cid; ?></td>
                            <td><?php echo $c->sid; ?></td>
                            <td><?php echo $c->lok; ?></td>
                            <td><?php echo $c->ban; ?></td>
                            <td><?php echo $c->ala; ?></td>
                            <td class="text-center"><?php if($this->session->userdata('id_jab')==4): ?>
                                    <a href="<?php echo base_url("cidsid/csihap/".$c->id); ?>" data-toggle='tooltip' title='Hapus CID/SID'><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;
                                <?php endif;?>
                            </td>


                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>




        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

<script>
    $(document).ready(function(){

        $(".wonpil").select2();

    })
</script>  