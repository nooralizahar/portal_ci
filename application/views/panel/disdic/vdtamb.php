
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("khusus/"); ?>"> Khusus</a></li>
            <li class="active"> Isi Data Berlangganan</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Isi Data Baru</div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                    	<div class="form-group">
                            <div class="col-md-12">
                            
                            <div class="alert alert-warning text-center"> DATA BERLANGGANAN VENDOR </div>
                            
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                            <label>Order dari</label>
                            <select name="ordb" class="form-control orderby">
                                <option value="JLM"> JLM</option>
                                <option value="OMG"> OMG</option>
                            </select>
                            </div>
                            <div class="col-md-8">
                            <label for="">Nama Vendor </label>
                            <select name="vnam" class="form-control vendor">

                                <?php foreach($dven as $d):?>
                                    <option value="<?php echo $d->vid; ?>"> <?php echo $d->vnama;?></option>
                                <?php endforeach;?>
                            </select>
                            </div>
                            <div class="col-md-2">
                            <label><i class="fa fa-plus-square-o"></i></label>
                            
                            <a href="venbar/" class="btn btn-success form-control" ><i class="fa fa-plus-square-o"></i> Vendor</a> 
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                            <label>Status</label>
                            <select name="stat" class="form-control status">
                                <option value="Active"> Active</option>
                                <option value="InActive"> Inactive</option>
                            </select>
                            </div>
                            <div class="col-md-3">
                            <label for="">No. PO</label>
                            <input type="text" name="nopo" class="form-control" required/>
                            </div>
                            <div class="col-md-3">
                            <label for="">Tgl. PO</label>
                            <input type="date" name="tgpo" class="form-control" required/>
                            </div>
                            <div class="col-md-3">
                            <label for="">Tgl. Aktif</label>
                            <input type="date" name="tgac" class="form-control">
                            </div>


                        </div>
  <!--                      <div class="form-group">
                            <label for="">Tanggal</label>
                            
                        </div> -->
                        <div class="form-group">
                            <div class="col-md-4">
                            <label for="">Kontrak (Bln)</label>
                            <input type="text" name="lcon" class="form-control">
                            </div>
                            <div class="col-md-4">
                            <label for="">Akhir Kontrak (1 Thn)</label>
                            <input type="date" name="acon" class="form-control">
                            </div>
                            <div class="col-md-4">
                            <label for="">Durasi Kontrak (Bln)</label>
                            <input type="text" name="dcon" class="form-control">
                            </div>
                            
                        </div>
                        <div class="form-group">

                            <div class="col-md-2">
                            <label for="">CID/SID</label>
                            <input type="text" name="csid" class="form-control" >
                            </div>

                            <div class="col-md-2">
                            <label for="">Kota</label>
                            <input type="text" name="kota" class="form-control">
                            </div>

                            <div class="col-md-2">
                            <label for="">Site</label>
                            <input type="text" name="site" class="form-control">
                            </div>

                            <div class="col-md-4">
                            <label for="">Alamat Pemasangan</label>
                            <input type="text" name="alam" class="form-control">
                            </div>

                            <div class="col-md-2">
                            <label for="">Segment</label>
                            <select name="segm" class="form-control segment">
                                <?php foreach($dseg as $d):?>
                                    <option value="<?php echo $d->id_seg; ?>"> <?php echo $d->ds_seg;?></option>
                                <?php endforeach;?>
                            </select>
                            </div>                            

                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                            
                            <div class="alert alert-info text-center"> V E N D O R </div>
                            
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-4">
                            <label for="">Instalation Charge</label>
                            <input type="text" name="vich" class="form-control">
                            </div>
                            <div class="col-md-4">
                            <label for="">Monthly Charge</label>
                            <input type="text" name="vmch" class="form-control">
                            </div>
                            <div class="col-md-2">
                            <label for="">Bandwidth</label>
                            <input type="text" name="vbwi" class="form-control">
                            </div>
                            <div class="col-md-2">
                            <label for="">Tgl. Aktivasi</label>
                            <input type="date" name="vtga" class="form-control" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                            
                            <div class="alert alert-success text-center"> C U S T O M E R </div>
                            
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <div class="col-md-4">
                            <label for="">Instalation Charge</label>
                            <input type="text" name="cich" class="form-control">
                            </div>
                            <div class="col-md-4">
                            <label for="">Monthly Charge</label>
                            <input type="text" name="cmch" class="form-control">
                            </div>
                            <div class="col-md-2">
                            <label for="">Bandwidth</label>
                            <input type="text" name="cbwi" class="form-control">
                            </div>
                            <div class="col-md-2">
                            <label for="">Tgl. Aktivasi</label>
                            <input type="date" name="ctga" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <label for="">Keterangan</label>
                            <input type="text" name="kete" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                        <center>
                        <button type="submit" name="btnSimpan" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
                        <!-- <input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script>


    $(document).ready(function(){

        $(".segment").select2();
        $(".vendor").select2();
        //$(".orderby").select2();
        //$(".status").select2();

    })
</script>