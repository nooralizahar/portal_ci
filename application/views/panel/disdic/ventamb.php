
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("khusus/"); ?>"> Khusus</a></li>
            <li><a href="<?php echo base_url("khusus/vdtamb"); ?>"> Isi Data Baru</a></li>
            <li class="active"> Tambah Vendor</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Isi Data Vendor</div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="col-md-12">
                            <label for="">N a m a </label>
                            <input type="text" name="vnama" class="form-control" required/>
                            </div>
   
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <label for="">A l a m a t </label>
                            <input type="text" name="valam" class="form-control" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <label for="">K o n t a k </label>
                            <input type="text" name="vkont" class="form-control" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <label for=""><br></label>
                            </div>
                        </div>
                     <center>
                        <button type="submit" name="btnSimpan" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
                        <!-- <input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

