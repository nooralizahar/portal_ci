<?php $sales=$this->session->userdata("username");
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;
endforeach;
$divi= $this->db->where("id_div",$this->session->userdata("id_div"))->get("ts_isivid")->result();

foreach($divi as $dvs): 
    $nmdv=$dvs->nm_div; 
    $iddv=$dvs->id_div;
endforeach;

$usrnm=$this->session->userdata("username");

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/purcbaru/"); ?>">Pembelian Baru</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Form Permintaan Pembelian</div>
                <div class="panel-body">
				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal Simpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Berhasil Simpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

        <?php //if($nmdv!="BOD"): ?>
                    <form action="" method="POST" enctype="multipart/form-data">

                       <!-- <div class="col-md-12">
                            <label>General Affair</label>
                            <select name="penerima" class="form-control penerima" multiple>
                                <?php foreach($daftar_jabatan as $jabatan):?>
                                    <option value="<?php echo $jabatan->nm_jab; ?>"> <?php echo $jabatan->nm_jab;?></option>
                                <?php endforeach;?>
                            </select>

                            <select name="penerima[]" class="form-control penerima" multiple>
                                <?php foreach($daftar_pengguna as $dinas => $group_pengguna): ?>
                                    <optgroup label="<?php echo $dinas ?>">
                                        <?php foreach($group_pengguna as $pengguna):?>
                                            <option value="<?php echo $pengguna->id_pengguna; ?>" <?php echo (termasuk_penerima($pengguna->id_pengguna,$penerima_otomatis) || isset($_GET["pn"]) && $_GET["pn"] == $pengguna->id_pengguna) ? "selected" : ""; ?>><?php echo $pengguna->nama_lengkap . ", " . $pengguna->nama_jabatan . " - " . $dinas; ?></option>
                                        <?php endforeach;?>
                                    </optgroup>
                                <?php endforeach; ?>
                            </select>

                        </div> -->
                        <div class="col-md-6">
                            <label>No. Pengajuan Pembelian </label>
                            <input type="text" name="no_rpu" value="<?php echo date("d").sprintf("%03d",mt_rand(10,100))."/RPU/".strtoupper(substr($sales,0,3))."-".date("m")."/".date("y")."";?>" class="form-control" readonly/>
                            <input type="hidden" name="pengirim" class="form-control" value="<?php echo $sales; ?>">
                        </div>
                        <div class="col-md-6">
                            <label>Tgl. Pemakaian </label>
                            <input type="date" name="tglpakai" class="form-control" required/><br />
                        </div>
                        <div class="col-md-12">
                            <label>Peruntukan </label>
                            <input type="text" name="peruntuk" placeholder="Nama Proyek / Nomor WO / Arahan Tugas" class="form-control" value="" required/>
                        </div>


                        <div class="col-md-12">
                         <!--   <label>Keterangan</label>
                            <textarea name="ket" id="ckeditor" ></textarea><br />
                            <label>Lampiran</label>
                            <input type="file" multiple id="attach" name="attach[]" class="form-control">
                            <small>format file : .jpg, .png, .pdf, .jpeg, .bmp, .gif, .doc, .docx, .xls, .xlsx, .ppt, .pptx. (maks. 2 MB)</small>--><br />
                            <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                            <button type='button' onclick="window.location.href = '<?php echo base_url('/panel') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                        </div>
                    </form>
             <?php //else: ?>
                <div><center><h3>DAFTAR APPROVAL PURCHASE REQUEST</h3></center></div>
             <?php //endif ?>
             <?php //echo $this->session->userdata("username");?>
        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="tgbu" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tgbu"  data-sortable="true">Tgl. Buat</th>
                        <th data-field="norp"  data-sortable="true">No. RPU</th>
                        <th data-field="peru"  data-sortable="true">Peruntukan</th>
                        <th data-field="tgpa"  data-sortable="true">Tgl. Pakai</th>
                        

                         
                        <?php if($nmdv!="BOD"): ?>
                            <th>Appv. by</th>         
                            <th>Pilihan</th>
                        <?php else:?>
                            <?php if($usrnm=="jpurnama" || $usrnm=="virianto"):?>
                            <th>Tot. Harga (Rp.)</th>    
                            <?php endif?>
                            <th>Approved</th>
                        <?php endif?>

                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        

                        if($nmjab=="Administrator"){$dcek=$d_purc;}elseif($nmjab=="Purchasing"){$dcek=$d_prja;}elseif($nmjab=="IT & Engineering GM" || $nmjab=="HRD & GA GM" || $nmjab=="Sales GM" || $nmjab=="Operation GM" || $nmjab=="Direktur" || $nmjab=="MR Manager"){$dcek=$d_prap;}else{$dcek=$d_prus;}
                        if($usrnm=="jpurnama"){$dcek=$d_apus;}elseif($usrnm=="virianto"){$dcek=$d_apfi;}
                        
                        foreach($dcek as $dat): ?>
                        <tr>
                            <td><?php echo $dat->tglbuat; ?></td>
                            <td><?php echo $dat->no_rpu; ?></td>
                            <td><?php echo $dat->peruntuk; ?></td>
                            <td><?php echo $dat->tglpakai; ?></td>
                            <?php if($nmdv!="BOD"): ?>
                            <td><a href="#" data-toggle='tooltip' title="<?php echo 'Disetujui : '.$dat->tglapv1;?>"><?php echo $dat->appv1; ?></a></td>
                            <?php endif?>
                            <?php if($usrnm=="jpurnama" || $usrnm=="virianto" ):?>
                            <td class="text-right"><?php echo number_format($dat->harga); ?></td>    
                            <?php endif?>
                            <td>
                            <center>  
                             <?php if($nmdv!="BOD"): ?> 

                                <?php if ($dat->penerima==null):?>                  
                                &nbsp;&nbsp;<a href="<?php echo base_url("panel/purctamb/".$dat->id_rpu); ?>" data-toggle='tooltip' title='Isi Barang'><i class="fa fa-plus fa-lg"></i></a>
                                &nbsp;&nbsp;<a href="<?php echo base_url("panel/purckirm/".$dat->id_rpu); ?>" data-toggle='tooltip' title='Kirim'><i class="fa fa-paper-plane-o fa-lg"></i></a>
                                <?php else:?>
                                    <?php if($nmjab=="Purchasing" ):?>
                                        &nbsp;&nbsp;<a href="<?php echo base_url("panel/purctamb/".$dat->id_rpu); ?>" data-toggle='tooltip' title='Isi Harga'><i class="fa fa-dollar fa-lg"></i></a>
                                        <?php if($dat->appv1!=null && $dat->appv2!=null):?>
                                          &nbsp;&nbsp;<a href="<?php echo base_url("panel/purcctak/".$dat->id_rpu); ?>" data-toggle='tooltip' title='Cetak PO'><i class="fa fa-print fa-lg"></i></a>   
                                        <?php endif;?>
                                    <?php else:?>
                                        &nbsp;&nbsp;<a href="#" data-toggle='tooltip' title='Sudah Dikirim'><i class="fa fa-check fa-lg"></i></a>
                                    <?php endif?> 
                                
                                <?php endif?>
                             

                            <?php else:?>

                             	<?php if($usrnm=="jpurnama"):?>

	                                <?php 
	                                if($dat->appv2!=''){
	                                    $judul='Selesai';
	                                    $kelas='fa fa-check fa-lg';
	                                }else{
	                                    $judul='Detil';
	                                    $kelas='fa fa-eye fa-lg';
	                                } ?>
	                                <a href="<?php echo base_url("panel/purcappv/".$dat->id_rpu); ?>" data-toggle='tooltip' title="<?php echo $judul;?>"><i class="<?php echo $kelas;?>"></i></a>

	                            <?php elseif($usrnm=="virianto"):?>

	                            	<?php 
	                                if($dat->appv3!=''){
	                                    $judul='Selesai';
	                                    $kelas='fa fa-check fa-lg';
	                                }else{
	                                    $judul='Detil';
	                                    $kelas='fa fa-eye fa-lg';
	                                } ?>
	                                <a href="<?php echo base_url("panel/purcappv/".$dat->id_rpu); ?>" data-toggle='tooltip' title="<?php echo $judul;?>"><i class="<?php echo $kelas;?>"></i></a>

                            	<?php else:?>
	                                <?php 
	                                if($dat->appv1!=''){
	                                    $judul='Selesai';
	                                    $kelas='fa fa-check fa-lg';
	                                }else{
	                                    $judul='Detil';
	                                    $kelas='fa fa-eye fa-lg';
	                                } ?>
	                                <a href="<?php echo base_url("panel/purcappv/".$dat->id_rpu); ?>" data-toggle='tooltip' title="<?php echo $judul;?>"><i class="<?php echo $kelas;?>"></i></a>
                             	<?php endif?>                             

                            <?php endif?>   
                                 
                            </center>                            
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                </div>
            </div>
        </div>
    </div><!--/.row-->

    <form action="" method="post" style="display: none;">
        <input type="hidden" name="password" id="confirmPassword">
        <input type="submit" name="selesaiBtnSubmit" id="selesaiBtnSubmit">
    </form>



</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('ket',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>

   $(document).ready(function(){

        $("#msg").froalaEditor({
            height: 300
        });

        $(".penerima").select2();

        $("#tanggal").datepicker({
            format: "yyyy-mm-dd"
        });

        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>


<script>
    $(document).ready(function(){
        $(".attach-img").magnificPopup({
            type: "image"
        });


        $("#isi_disposisi").shorten({
            moreText: "Selengkapnya",
            lessText: "Kecilkan",
            showChars: 300
        });

        $("#attach").fileinput({'showUpload':false});

        $(".attach").on("click",function(ev){
            var data_file = $(this).data().file;
            var content = "<ol>";

            $.each(data_file,function(key,item){
                var cls = item.file.split(".").length;
                var tipe_gambar = ["jpg","png","jpeg","bmp","gif"];
                cls = ($.inArray(item.file.split(".")[cls-1],tipe_gambar) == -1) ? "attach-doc" : "attach-img";
                content += "" +
                    "<li><a class='"+cls+"' href='"+BASE_URL+"assets/uploads/lampiran/follow_up_disposisi/"+item.file+"'>"+item.judul+"</a></li>";
            });

            content += "</ol>";

            bootbox.alert({
                size: "normal",
                title: "Lampiran Tanggapan",
                message: content
            });


            $(".attach-img").magnificPopup({
                type: "image"
            });


            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                $("#myModal").css("z-index",2000);
                return false;
            });


            ev.preventDefault();
        });


        $("#selesai").on("click",function(ev){

            swal({
                title: "Konfirmasi Penyelesaian",
                text: "Masukkan password anda untuk mengkonfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword").val(input);
                $("#selesaiBtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });


        $(function(){
            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                return false;
            });
        })
    });
</script>

