﻿
<?php
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
$divi= $this->db->where("id_div",$this->session->userdata("id_div"))->get("ts_isivid")->result();

foreach($divi as $dvs): 
    $nmdv=$dvs->nm_div; 
    $iddv=$dvs->id_div;
endforeach;

$mharga= $this->db->where("penerima",$nmjab)->get("ts_agrah")->num_rows();
$nharga= $this->db->where("sales",$this->session->userdata("username"))->get("ts_agrah")->num_rows();
$dissuk=0;//$this->db->where("ke_user",$this->session->userdata("id_pengguna"))->get("relasi_disposisi")->num_rows();
$markaz = 0;//$this->db->where("id_dinas",$kantr)->get("dinas")->result();
?>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> Dashboard</h1>
        </div>
    </div><!--/.row-->

    <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked round coffee mug"><use xlink:href="#stroked-round-coffee-mug"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $nilnmc=$this->db->where('ket like','%nmc%')->or_where('nmc !=','')->get("ts_agrah")->num_rows(); echo $nilnmc; ?></div>
                        <div class="text-muted">Req. NMC</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $nilsem=$this->db->get("ts_agrah")->num_rows(); echo $nilsem-$nilnmc; ?></div>
                        <div class="text-muted">Req. No NMC</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $nilsem; ?></div>
                        <div class="text-muted">Total Request</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-blue panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked tag"><use xlink:href="#stroked-tag"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div ><?php echo $nmjab;  ?></div>
                        <div class="text-muted">Akses</div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/.row-->


    <hr>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal tambah harga! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Tambah harga berhasil disimpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["esucc"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Edit harga berhasil disimpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["eerr"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal edit harga! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["bsucc"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Approval berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["berr"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Approval Gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>

 <!--      <div class="panel-heading"><i class="fa fa-money fa-lg"></i> Permintaan Harga</div>-->
           <div class="panel-body">
                <table class="table table-striped" id="tarminga" data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-page-size="25" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tglkirim" data-sortable="true">Tgl. Kirim</th>
                        <th data-field="sales" data-sortable="true">Sales</th>
                        <th data-field="pelanggan" data-sortable="true">Pelanggan</th>
                        <th data-field="site" data-sortable="true">Site</th>
                        <!--<th data-field="uraian" data-sortable="true">Uraian</th>-->
                        <th data-field="rpinstall" data-sortable="true">Instalasi (Rp.)</th>
                        <th data-field="rpbulanan" data-sortable="true">Bulanan (Rp.)</th>
                        <th data-field="nmc" data-sortable="true">NMC</th>
                        <th data-field="catharga" data-sortable="true">Catatan</th>
                        
                        <th data-field="tgljawab" data-sortable="true">Approved by</th>
<!--                         <th data-field="kpi" data-sortable="true">KPI (hari)</th>
                       <th>Pilihan</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                    //if($nmjab=="Administrator"){$daf=$dafmua;}elseif($nmjab=="Sales"){$daf=$dafrsa;}elseif($nmjab=="Sales Supervisor"){$daf=$dafrsapv;}elseif($nmjab=="Sales Jr. Manager"){$daf=$dafrsamg;}else{$daf=$dafreq;}
                    $daf=$dafnmc;
                    foreach($daf as $harga): ?>
                        <tr >
                            <?php $idr=strval($harga->id_rha); 
                            $date1 = new DateTime($harga->tg_buat);
                            $date2 = new DateTime($harga->tg_jwb);

                            $diff = $date2->diff($date1);

                            $hours = $diff->h;
                            $hours = $hours + ($diff->days*24);
                            ?>
                            <td><?php echo date("d-M-y H:i:s",strtotime($harga->tg_buat)); ?> </td>
                            <td><?php echo $harga->nama_lengkap; ?> </td>
                            <td><?php  echo $harga->nm_custo; ?> </td>
                            <td><?php  echo $harga->al_custo; ?></td>
                            <!--<td><?php  echo $harga->request.", media : ".$harga->media.", service : ".$harga->service.", ";

                                if($harga->ix==0){}else{echo "IX : ".$harga->ix." Mbps";} 
                                if($harga->iix==0){}else{echo "IIX : ".$harga->iix." Mbps";}
                                if($harga->mix==0){}else{echo "MIX : ".$harga->mix." Mbps";}
                                if($harga->llo==0){}else{echo "LOCALLOOP : ".$harga->llo." Mbps";}
                                if($harga->colo==''){}else{echo ", Colocation : ".$harga->colo;}
                                if($harga->tele==0){}else{echo ", Telephony : ".$harga->tele." Line(s)";}
                                if($harga->ket==''){}else{echo "<br> <b>Remark :</b> ". strip_tags(substr($harga->ket,0,50))."<a href='"; echo base_url("/panel/hargadetil/").$harga->id_rha."' class='btn btn-primary btn-sm'><small> Detil ...</small></a><br> ";}
                                if($harga->lampiran==''){ 

                                }else{ 

                            if($harga->lampiran != null):
                            $lampiran = json_decode($harga->lampiran);
                            ?>
                                <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <a href="<?php echo base_url("assets/uploads/lampiran/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>"><?php echo $item->judul; ?></a>
                                <?php endforeach; ?> 
                            
                             <?php endif;?>



                             <?php  }

                              
                            ?></td> -->
                            <td class="text-right"><?php  echo number_format($harga->hg_instalasi); ?></td>
                            <td class="text-right"><?php  echo number_format($harga->hg_bulanan); ?> </td>
                            <td class="text-center"><?php  echo $harga->nmc; ?>                            </td>
                            <td><small><b><?php echo $harga->hg_catatan;?></b></small><br><br><a href="<?php if(!empty($harga->request)){$p=1; echo base_url("panel/vbpl/".$harga->id_rha);}else{$p=0; echo base_url("panel/vbpb/".$harga->id_rha);}  ?>" class="btn btn-success"><small style="color: white"><?php if(empty($harga->hg_assignby)){}else{echo "<sup><small>PRA </small></sup>BP by ".substr($harga->npresales, 0,12)."<br>".date("d-M-y H:i:s",strtotime($harga->tg_jwb));} ?></small></a></td>

                            <td class="text-left"><small><?php 


                            if($harga->stapv1==1){echo "<a href='#' data-toggle='tooltip' data-placement='left' title='Approved $harga->tgapv1'><i class='fa fa-check fa-lg' style='color:green'></i></a>&nbsp;";}elseif(($harga->stapv1==2)){echo "<a href='#' data-toggle='tooltip' data-placement='left' title='Rejected $harga->tgapv1'><i class='fa fa-times fa-lg' style='color:red'></i></a>&nbsp;";} $datnam1=explode(" ",$harga->nmapv1); echo $datnam1[0]."<br>"; echo $harga->caapv1."<br>";

                            if($harga->stapv2==1){echo "<a href='#' data-toggle='tooltip' data-placement='left' title='Approved $harga->tgapv2'><i class='fa fa-check fa-lg' style='color:green'></i></a>&nbsp;";}elseif(($harga->stapv2==2)){echo "<a href='#' data-toggle='tooltip' data-placement='left' title='Rejected $harga->tgapv2'><i class='fa fa-times fa-lg' style='color:red'></i></a>&nbsp;";} $datnam2=explode(" ",$harga->nmapv2); echo $datnam2[0]."<br>"; echo $harga->caapv2."<br>";

                            if($harga->stapv3==1){echo "<a href='#' data-toggle='tooltip' data-placement='left' title='Approved $harga->tgapv3'><i class='fa fa-check fa-lg' style='color:green'></i></a>&nbsp;";}elseif(($harga->stapv3==2)){echo "<a href='#' data-toggle='tooltip' data-placement='left' title='Rejected $harga->tgapv3'><i class='fa fa-times fa-lg' style='color:red'></i></a>&nbsp;";} $datnam3=explode(" ",$harga->nmapv3); echo $datnam3[0]."<br>"; echo $harga->caapv3."<br>";
                             ?>

                            </small></td>
                           <!-- <td><?php $sisa=$hours%24; $hari=($hours-$sisa)/24; echo "<small>".$hari." hari ".$sisa." jam </small>"; if($harga->hg_edit>0) {?>&nbsp;<sup><span class="badge"><small><?php echo $harga->hg_edit;?></small></span></sup><?php }else{} ?>
                            </td> -->
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                   </table>
                  </div>
                </div>
            </div>
        </div> 




 
