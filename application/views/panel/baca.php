
<?php 
foreach($data_saya as $dat):
$nm_din=str_replace(" ","_",$dat->nama_dinas); 
$ks_dra=$dat->kodrat;
$ks_dta=$dat->kodta;
$ks_pag=$dat->ks_jabpa; 
$ks_pta=$dat->ks_jabpta;
endforeach;

?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/inbox/"); ?>">Surat Masuk</a></li>
            <li class="active"><?php echo $pesan->subjek; ?></li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-success">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal mengirim disposisi! Coba lagi nanti <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Disposisi berhasil dikirim ke tujuan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["uplo"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> File Sudah di Upload <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div> 
                <?php endif; ?>
                <div class="panel-heading">
                    <i style="float: right; display: block; margin-top: 13px;" data-toggle='tooltip' title='Tandai surat' class="fa fa-star fa-lg star <?php echo ($pesan->starred == 1) ? "star-active" : ""; ?>" data-starred="<?php echo $pesan->starred; ?>" data-id="<?php echo $pesan->id_relasi_pesan; ?>"></i> <?php echo $pesan->subjek; ?>
                </div>
                <div class="panel-body">

                    <font size="2" face="arial" color="green">Dibuat oleh <?php echo $pesan->pengirim; ?></font> <br><small><font color="grey"><b>Pada <?php echo date("d-M-y H:m:s",strtotime($pesan->waktu_kirim));?></b></small></font>
                    <hr style="margin-top: 5px;margin-bottom: 5px;" />
                        
                    <table width="100%"><tr>
                        <td align="left">&nbsp;&nbsp;
                        <?php if($this->session->userdata("disposisi") == 1): ?>
                        <a href="<?php echo base_url("panel/buatdisposisi/".$pesan->id_pesan); ?>"><i class="fa fa-external-link fa-lg fa-fw"></i> Disposisi</a>
                        <?php endif; ?>
                        <?php if($pesan->nota == 2): ?>
                        <a href="<?php echo base_url("panel/mindata/?sub=Re : ".$pesan->subjek."&pn=".$pesan->dari_user); ?>"><i class="fa fa-reply fa-lg fa-fw"></i> Balas</a>
                        <?php endif; ?>
                        <?php if($pesan->nota == 1): ?>
                        <a href="<?php echo base_url("panel/notadinas/?sub=Re : ".$pesan->subjek."&pn=".$pesan->dari_user); ?>"><i class="fa fa-reply fa-lg fa-fw"></i> Balas</a>
                        <?php endif; ?>
                        <?php if($pesan->nota == 0): ?>
                        <a href="<?php echo base_url("panel/compose/?sub=Re : ".$pesan->subjek."&pn=".$pesan->dari_user); ?>"><i class="fa fa-reply fa-lg fa-fw"></i> Balas</a>
                        <?php endif; ?>
                        <a href="<?php echo base_url("panel/compose/?sub=Fwd : ".$pesan->subjek."&id=".$pesan->id_pesan); ?>"><i class="fa fa-share fa-lg fa-fw"></i> Forward</a>
                        </td>
                        <td align="right" width="35%">
                            
                            <?php if($pesan->nota == 2): ?>
                                <a href="#" data-toggle="modal" data-target="#mo_upload"><b style='background-color: rgba(254, 0, 0, 0.76); color: white;'>&nbsp;Upload Disini >></b><i class="fa fa-folder fa-lg fa-fw"></i> : <?php echo $pesan->ruang_berkas ?></a>
                            <?php else: ?>
                            <?php 
                            if($pesan->lampiran != null):
                            $lampiran = json_decode($pesan->lampiran);
                            ?>
                                 Lampiran : 
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <a href="<?php echo base_url("assets/uploads/lampiran/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>"><?php echo $item->judul; ?></a>
                                <?php endforeach; ?> 
                             <?php endif;?>&nbsp;&nbsp;&nbsp;&nbsp;  
                             <?php endif;?>
                        </td>
                        </tr>
                    </table>
                        
                    <hr style="margin-top: 5px;margin-bottom: 5px;" />
                    <?php if($pesan->external == 1): ?>
                    <ul>
                        <li>Nomor surat : <?php echo $pesan->nomor_surat; ?></li>
                        <li>Surat dari : <?php echo $pesan->surat_dari; ?></li>
                        <li>Tanggal surat : <?php echo $pesan->tanggal_surat; ?></li>
                        <li>Tanggal terima : <?php echo $pesan->tanggal_terima; ?></li>
                        <li>Nomor agenda : <?php echo $pesan->nomor_agenda; ?></li>
                    </ul>
                    <?php endif;?>
                    <?php echo $pesan->isi_pesan; ?>

                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                    Tracking
                </div>
                <ul class="list-group">
                    <li class="list-group-item"><font size="2" face="arial" color="green">Dibuat oleh <?php echo $pesan->pengirim; ?></font> <br><small><font color="grey"><b>Pada <?php echo date("d-M-y H:m:s",strtotime($pesan->waktu_kirim));?></b></small></font></li>
                    <li class="list-group-item"></li>
                </ul>
            </div>
        </div>
          <!-- <div class="panel panel-success">
                <div class="panel-heading">
                    Tindakan
                </div>
                <ul class="list-group">
                    <?php if($this->session->userdata("disposisi") == 1): ?>
                        <li class="list-group-item"><a href="<?php echo base_url("panel/buatdisposisi/".$pesan->id_pesan); ?>"><i class="fa fa-external-link fa-lg fa-fw"></i>&nbsp;Disposisi</a></li>
                    <?php endif; ?>
                    <li class="list-group-item"><a href="<?php echo base_url("panel/compose/?sub=Re : ".$pesan->subjek."&pn=".$pesan->dari_user); ?>"><i class="fa fa-reply fa-lg fa-fw"></i> Balas</a></li>
                    <li class="list-group-item"><a href="<?php echo base_url("panel/compose/?sub=Fwd : ".$pesan->subjek."&id=".$pesan->id_pesan); ?>"><i class="fa fa-share fa-lg fa-fw"></i> Forward</a></li>
                </ul>
            </div> -->
            <!-- <?php
            if($pesan->lampiran != null):
            $lampiran = json_decode($pesan->lampiran);
            ?>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    Lampiran File
                </div>
                <ul class="list-group">
                    <?php foreach($lampiran as $item):
                        $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                        ?>
                        <li class="list-group-item"><a href="<?php echo base_url("assets/uploads/lampiran/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>"><?php echo $item->judul; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif;?>
        </div>-->
    </div><!--/.row-->

</div>	<!--/.main-->

<div id="mo_upload" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <form action="<?php echo base_url('panel/updata/')?>" method="POST" enctype="multipart/form-data">
        
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Files Upload</h4>
                </div>
                <div class="modal-body">

                   
                    <?php 
                         
                        if (!file_exists("assets/uploads/mindata/".$pesan->ruang_berkas."/".$nm_din)) {
                            mkdir("assets/uploads/mindata/".$pesan->ruang_berkas."/".$nm_din, 0777, true);
                            copy("assets/index.php","assets/uploads/mindata/".$pesan->ruang_berkas."/".$nm_din."/index.php");
                        }
                    ?>
                    <i class="fa fa-folder fa-lg fa-fw"></i> \<?php echo $pesan->ruang_berkas; ?>\
                    <?php echo $nm_din; ?><br>

                       <?php
                        $dir = "assets/uploads/mindata/".$pesan->ruang_berkas."/".$nm_din;
                       
                        // Open a directory, and read its contents
                        /*if (is_dir($dir)){
                          if ($dh = opendir($dir)){
                            while (($file = readdir($dh)) !== false){
                              echo "<li class='list-group-item'>" . $file . "</li>";
                            }
                            closedir($dh);
                          }
                        }*/


                            $d=base_url($dir);
                            $response=scan($dir);
                            foreach ($response as $key => $data) {
                                echo /*$key.*/""; 
                                
                                
                                if($data["type"]=="folder"){
                                    echo "&nbsp;&nbsp;&nbsp;<i class='fa fa-folder fa-lg fa-fw'></i>"; 
                                    echo $data["name"]."<br>";
                                    
                                }else{
                                     $f=$data["name"];

                                    echo "&nbsp;&nbsp;&nbsp;<i class='fa fa-file fa-lg fa-fw'></i>";
                                    echo "<a href=$d/$f download>$f</a><br>";
                                }
                            }
                        
                        ?> 
                     
                        <input type="hidden" id="dpath" name="dpath" value="<?php echo $dir; ?>" class="form-control">
                        <input type="hidden" id="idpsn" name="idpsn" value="<?php echo $pesan->id_pesan; ?>" class="form-control">
                        
                        <input type="file" multiple id="upload_md" name="upload_md[]" class="form-control">
                </div>

                <div class="modal-footer">

                    <button type="submit" name="btnSubmitX" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>BATAL</sup></small></button>
                </div>
            </div>

        </form>

    </div>
</div>

<?php 



?>


<script>
    $(document).ready(function(){
        $(".attach-img").magnificPopup({
            type: "image"
        });

    $("#upload_md").fileinput({'showUpload':false, 'previewFileType':'any'});

        $(function(){
            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                return false;
            });
        })


        setTimeout(function(){
            $('[data-toggle="tooltip"]').tooltip();
        },1000);

        $(".star.fa-star").on("click",function(){
            var curStar = $(this);
            update_star(curStar);
            curStar.removeClass("fa-star").addClass("fa-spinner fa-pulse");
        });

    });


    function update_star(curStar) {
        $.ajax({
            url: BASE_URL + "/ajax/update_star/",
            method: "POST",
            data: {
                id_relasi_pesan: curStar.data().id,
                starred: (curStar.data().starred == 1) ? 0 : 1
            },
            success: function(response){
                if(curStar.data().starred == 0) {
                    curStar.data("starred",1);
                    curStar.addClass("star-active");
                    curStar.parent().children("span").html(1)
                } else {
                    curStar.data("starred",0);
                    curStar.removeClass("star-active");
                    curStar.parent().children('span').html(0);
                }
                curStar.removeClass('fa-spinner fa-pulse');
                curStar.addClass("fa-star");
            }
        })
    }
</script>