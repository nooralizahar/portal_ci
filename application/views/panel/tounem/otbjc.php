<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("otbmen/"); ?>"> OTB</a></li>
            <li class="active"> Join Closure (JC)</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Tambah Join Closure</h5>
                </div>
        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#otbplat" aria-controls="otbplat" role="tab" data-toggle="tab"> JC Baru</a></li>

                        </ul>

                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="otbplat">
    <div class="panel-body">
        <div class="panel panel-primary">

        	<h3 style="color:blue">&nbsp;&nbsp;<span class="label label-primary"><?php $ido=$otpil->id_otb; $capa=$otpil->jm_por; if($capa==12 or $capa==24){$caps=$capa/6;}else{$caps=$capa/12;} echo $otpil->id_otb.'|'.$otpil->nm_otb.'|'.$capa.'core';?></span></h3>

           <div class="panel-body">
        <form class='add-item' method=POST action=''>

        <div class="col-md-3">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Jumlah (tube)</span>
                       <input type='text' name='tube' class='form-control' value="<?=$caps;?>" id='tube' readonly/>
                       <input type='hidden' name='idot' class='form-control' id='idot' value="<?=$ido;?>">   
                </div>
            </div>
        </div>
 
        <div class="col-md-9">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Nama / Kode JC</span>
                    <input type='text' name='jocl' class='form-control' id='jocl' placeholder="isi nama/kode join closure disini" required>
                    
                </div>
            </div>
        </div>  

        <div class="col-md-8">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Alamat</span>
                    <input type='text' name='alam' class='form-control' id='alam' placeholder="isi alamat disini">
                </div>
            </div>
        </div> 
        <div class="col-md-4">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon pixelfix">Tikor</span>
                    <input type='text' name='tiko' class='form-control' id='tiko' placeholder="isi koordinat disini">
                </div>
            </div>
        </div> 

        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="goBack()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
<p> </p>

<div class="row">
           <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    
                    <tr>
                        <th data-field="a"  data-sortable="true" class="text-center">ID#</th>
                        <th data-field="b"  data-sortable="true" class="text-center">ID OTB</th>
                        <th data-field="c"  data-sortable="true" class="text-center">Nama / Kode JC</th>
                        <th data-field="d"  data-sortable="true" class="text-center">Alamat</th>
                        <th data-field="e"  data-sortable="true" class="text-center">Tikor</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Tube</th>
                        <th data-field="g"  data-sortable="true" class="text-center">Opsi</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($jcpil as $dat): ?>
                        <tr>
                            <td class="text-center"><?php echo $dat->idjc; ?></td>
                            <td class="text-left"><a href="<?php echo base_url("otbmen/ganma/".$dat->idjc); ?>" data-toggle='tooltip' title='Ganti Nama'><?php echo $dat->idot; ?></a>
                            </td>
                            <td class="text-center"><?php echo $dat->jocl; ?></td>
                            <td class="text-center"><a href="<?php //echo base_url("otbmen/peta/".$dat->id_otb); ?>" target='_blank'><?php echo $dat->alam; ?></a></td>
                            <td class="text-right"><?php echo $dat->tiko; ?></td>
                            <td class="text-right"><?php echo $dat->tube; ?></td>

                           
                            <td><center>
                                <a href="<?php echo base_url("otbmen/jcled/".$dat->idjc); ?>" data-toggle='tooltip' title='Ubah'><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp; <!-- Edit OTB -->
                               <!-- <a href="<?php echo base_url("otbmen/otbjc/".$dat->idjc); ?>" data-toggle='tooltip' title='Tambah JC'><i class="fa fa-plus-square fa-lg"></i></a>&nbsp;&nbsp;
                               <a href="<?php echo base_url("otbmen/odpli/".$dat->id_otb); ?>" data-toggle='tooltip' title='Tambah Pelanggan'><i class="fa fa-plus fa-lg"></i></a>&nbsp;&nbsp;   -->
                                <?php if($this->session->userdata('level')=='admin'): ?>
                                    <?php if($dat->ubua==$this->session->userdata('id_pengguna')): ?>
                                    <a href="<?php echo base_url("otbmen/jclha/".$dat->idjc); ?>" data-toggle='tooltip' title='Hapus OTB'><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;
                                    <?php endif;?>
                                <?php endif;?>
                                <!--<?php if($dat->fileqr==null): ?>
                                <a href="<?php echo base_url("otbmen/odpqr/".$dat->id_otb); ?>" data-toggle='tooltip' title='Generate'><i class="fa fa-qrcode fa-lg"></i></a>
                                <?php else :?>
                                <a href="<?php echo base_url("otbmen/odpce/".$dat->id_otb); ?>" data-toggle='tooltip' title='Cetak' ><i class="fa fa-print fa-lg"></i></a>
                                <?php endif;?>-->
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>

</div>






             </div>
         </div>
     </div>
</div>



                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

<script>


    $(document).ready(function(){
        $(".pilihcore").select2();

    })

    function goBack() {
  		window.history.back();
	}
</script>