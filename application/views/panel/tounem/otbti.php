<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("otbmen/"); ?>"> OTB</a></li>
            <li class="active"> Detil</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Detil Data OTB</h5>
                </div>
        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <!--<li role="presentation"><a href="#lokbar" aria-controls="lokbar" role="tab" data-toggle="tab"> Lokasi Baru</a></li> -->
                            <li role="presentation" class="active"><a href="#detil" aria-controls="detil" role="tab" data-toggle="tab">Detil</a></li>
                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane" id="lokbar">
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">



                
       
             </div>
         </div>
     </div>
</div>


<div role="tabpanel" class="tab-pane active" id="detil">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">
<div class="row">

        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-2">  </div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-jingga panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php $co=$this->db->where("id_otb",$pi)->get("t_bto_rel")->num_rows(); echo number_format($co); ?></a></div>
                        <div class="text-muted" > Core</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $ns=$this->db->where("id_otb",$pi)->where("in_des","NOT SPLICE")->get("t_bto_rel")->num_rows(); echo number_format($ns); ?></div>
                        <div class="text-muted"> Not Splice</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked chain"><use xlink:href="#stroked-chain"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($co-$ns); ?></div>
                        <div class="text-muted"> Linked</div>
                    </div>
                </div>
            </div>
        </div>


</div> <br>

        <div class='form-group row'>
            
            <div class='col-sm-10'>
                <a href="<?php //echo base_url("otbmen/eksporxls/".$pi); ?>" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg"></i> <br><small><sup>EXCEL</sup></small></a>&nbsp;
                 <button type='button' onclick="window.location.back()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>

    <br>



        <!--<table class="table table-bordered" data-toggle="table" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-size="50" data-pagination="true" data-sort-name="name" data-sort-order="desc"> -->
        <table class="table table-bordered">
                    <thead>
                    <tr> <th colspan="6"><center><h3><?php echo $onm; ?></h3></center></th></tr>
                    <tr>
                        <th data-field="a"  data-sortable="true" width="30%"><center>OUT DESCRIPTION</center></th>
                        <th data-field="b"  data-sortable="true" width="5%"><center>NO.</center></th>
                        <th data-field="c"  data-sortable="true" width="10%"><center>CORE</center></th>
                        <th data-field="d"  data-sortable="true" width="30%"><center>IN DESCRIPTION</center></th>
                        <th data-field="e"  data-sortable="true" width="10%"><center>UPDATE</center></th>
                        <th data-field="f"  data-sortable="true" width="5%"><center>OPSI</center></th>
                    
                    </tr>
                    </thead>
                    <tbody>
                        
                    <?php foreach($otbde as $dat):       
                        /* warna bundle */
                     if($dat->id_cor<13){$color="#aecbff";$dana=$dat->id_cor;}
                     if ($dat->id_cor>=13){$color="#ffd592";$dana=$dat->id_cor-12;}
                     if ($dat->id_cor>=25){$color="#95ff92";$dana=$dat->id_cor-24;}
                     if ($dat->id_cor>=37){$color="#cba973";$dana=$dat->id_cor-36;}
                     if ($dat->id_cor>=49){$color="#b5b5b5";$dana=$dat->id_cor-48;}
                     if ($dat->id_cor>=61){$color="#ffffff";$dana=$dat->id_cor-60;}
                     if ($dat->id_cor>=73){$color="#ff1c37";$dana=$dat->id_cor-72;}
                     if ($dat->id_cor>=85){$color="#555555";$dana=$dat->id_cor-84;}
                     if ($dat->id_cor>=97){$color="#ffe03e";$dana=$dat->id_cor-96;}
                     if ($dat->id_cor>=109){$color="#d022ff";$dana=$dat->id_cor-108;}
                     if ($dat->id_cor>=121){$color="#ff22e9";$dana=$dat->id_cor-120;}
                     if ($dat->id_cor>=133){$color="#27eeff";$dana=$dat->id_cor-132;}  
                     /*warna core */

                     if($dana==1){$warna="#0000ff";$wteks="BIRU";}
                     if($dana==2){$warna="#ffae00";$wteks="ORANGE";}
                     if($dana==3){$warna="#14c500";$wteks="HIJAU";}
                     if($dana==4){$warna="#bd7e1b";$wteks="COKLAT";}
                     if($dana==5){$warna="#b5b5b5";$wteks="ABU-ABU";}
                     if($dana==6){$warna="#ffffff";$wteks="PUTIH";}
                     if($dana==7){$warna="#ff0000";$wteks="MERAH";}
                     if($dana==8){$warna="#212121";$wteks="HITAM";}
                     if($dana==9){$warna="#ffee00";$wteks="KUNING";}
                     if($dana==10){$warna="#b400d3";$wteks="UNGU";}
                     if($dana==11){$warna="#ff00d6";$wteks="PINK";}
                     if($dana==12){$warna="#27eeff";$wteks="TOSCA";}                   
 
                     ?>

                        <tr bgcolor="<?php echo $color; ?>">
                            <td align="center"><?php echo $dat->ou_des; ?></td>
                            <td align="center"><?php echo $dat->id_cor; ?></td>
                            <td  bgcolor="<?php echo $warna; ?>" align="center"><b><?php echo $wteks; ?></b></td>
                            <td><?php echo $dat->nm_gab; ?></td>
                            <td align="center"><small><?php echo $dat->tg_upd."<br> oleh ".$dat->us_upd; ?></small></td>
                            <td><center>
                                <a href="<?php echo base_url("otbmen/otbed/".$dat->id_otb."/".$dat->id_otr); ?>" data-toggle='tooltip' title='Ubah'><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp; 
                                <!--<a href="<?php echo base_url("otbmen/otbli/".$dat->id_lok); ?>" data-toggle='tooltip' title='Tambah OTB'><i class="fa fa-plus fa-lg"></i></a>&nbsp;&nbsp; 
                                <?php if($this->session->userdata('level')=='admin'): ?>
                                	<a href="<?php echo base_url("otbmen/lokha/".$dat->id_lok); ?>" data-toggle='tooltip' title='Hapus ODP'><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;
                                <?php endif;?>-->

                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

