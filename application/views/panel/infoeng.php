﻿<?php
    $thnini= date('y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    $thnblu= date('ym',strtotime("-1 month"));
    $blnini=date('Y-m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 

    $x=$this->db->order_by("custno","desc")->limit(1)->get("radcheck")->result();
        foreach($x as $dat): 
        $dupd=$dat->registerdate;   
        endforeach; 
    $rhrg=$this->db->order_by("tg_buat","desc")->limit(1)->get("ts_agrah")->result();
        foreach($rhrg as $dat): 
        $updrhrg=$dat->tg_buat;   
        endforeach; 
    $idwo=$this->db->order_by("id_worde","desc")->limit(1)->get("tp_owatad")->result();
        foreach($idwo as $dat): 
        $updidwo=$dat->tg_issue;   
        endforeach; 
        
        $imtc=$this->db->order_by("tgmtc","desc")->limit(1)->get("no_ctmofni")->result();
        foreach($imtc as $dat): 
        $updimtc=$dat->tgmtc;   
        endforeach;
?>

<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"> Work Order</a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-3">
        <?php 
            echo "<small>Pengkinian : ". date("d-m-Y", strtotime($updidwo))."</small>";
         ?></div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/word/$blnini"); ?>"><?php echo $this->db->where("tg_issue like ",$blnini."%")->get("tp_owatad")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/word/$blnlal"); ?>"><?php echo $this->db->where("tg_issue like ",$blnlal."%")->get("tp_owatad")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked basket"><use xlink:href="#stroked-basket"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/word/20$thnini"); ?>"><?php echo $this->db->where("tg_issue like ","20".$thnini."%")->get("tp_owatad")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked basket"><use xlink:href="#stroked-basket"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/word/20$thnlal"); ?>"><?php echo $this->db->where("tg_issue like ","20".$thnlal."%")->get("tp_owatad")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun Lalu</div>
                    </div>
                </div>
            </div>
        </div>


</div> <!--/.row -->

<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"> Registrasi FTTH</a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-3">
        <?php 
            echo "<small>Pengkinian : ". date("d-m-Y", strtotime($dupd))."</small>";
         ?></div>
        </div>

        <hr/>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-merah panel-widget ">
                <div class="row no-padding">

                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked home blank"><use xlink:href="#stroked-home"></use></svg>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        
                        <div class="large"><a href="<?php echo base_url("panel/ftth/$blnini"); ?>"><?php echo $this->db->where("registerdate like ",$blnini."%")->get("radcheck")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan ini </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-merah-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/ftth/$blnlal"); ?>"><?php echo $this->db->where("registerdate like ",$blnlal."%")->get("radcheck")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked chain"><use xlink:href="#stroked-chain"></use></svg>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/ftth/20$thnini"); ?>"><?php echo $this->db->where("registerdate like ","20".$thnini."%")->get("radcheck")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked chain"><use xlink:href="#stroked-chain"></use></svg>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/ftth/20$thnlal"); ?>"><?php echo $this->db->where("registerdate like ","20".$thnlal."%")->get("radcheck")->num_rows(); ?></div>
                        <div class="text-muted">Tahun Lalu</div>
                    </div>
                </div>
            </div>
        </div>


</div> <!--/.row -->

 
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"> Maintenance</a></li>
        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-3">
        <?php 
            echo "<small>Pengkinian : ". date("d-m-Y", strtotime($updimtc))."</small>";
         ?></div>
        </div>

        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-bido panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg>
                        <small><?php echo $blnini; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/imtc/$blnini"); ?>"><?php echo $this->db->where("tgmtc like ",$blnini."%")->get("no_ctmofni")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg>
                        <small><?php echo $blnlal; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/imtc/$blnlal"); ?>"><?php echo $this->db->where("tgmtc like ",$blnlal."%")->get("no_ctmofni")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>
                        <small><?php echo "20".$thnini; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/imtc/20$thnini"); ?>"><?php echo $this->db->where("tgmtc like ","20".$thnini."%")->get("no_ctmofni")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>
                        <small><?php echo "20".$thnlal; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/imtc/20$thnlal"); ?>"><?php echo $this->db->where("tgmtc like ","20".$thnlal."%")->get("no_ctmofni")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun Lalu</div>
                    </div>
                </div>
            </div>
        </div>


</div> <!--/.row -->