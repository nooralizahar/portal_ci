﻿<?php
    $thnini= date('y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    if(substr($thnbli, 2)==01){$bln=12;}else{$bln='0'.substr($thnbli, 2)-1;}
    $thnblu= ((substr($thnbli,0, 2))-1).$bln;
    $blnini=date('Y-m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 


        $afoc=$this->db->order_by("id","desc")->limit(1)->get("reports")->result();
        foreach($afoc as $dat): 
        $updafoc=$dat->date;   
        endforeach; 

        $imtc=$this->db->order_by("tgmtc","desc")->limit(1)->get("no_ctmofni")->result();
        foreach($imtc as $dat): 
        $updimtc=$dat->tgmtc;   
        endforeach; 

?>
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"> Maintenance</a></li>
        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-3">
        <?php 
            echo "<small>Pengkinian : ". date("d-m-Y", strtotime($updimtc))."</small>";
         ?></div>
        </div>

        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-bido panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg>
                        <small><?php echo $blnini; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/imtc/$blnini"); ?>"><?php echo $this->db->where("tgmtc like ",$blnini."%")->get("no_ctmofni")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg>
                        <small><?php echo $blnlal; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/imtc/$blnlal"); ?>"><?php echo $this->db->where("tgmtc like ",$blnlal."%")->get("no_ctmofni")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>
                        <small><?php echo "20".$thnini; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/imtc/20$thnini"); ?>"><?php echo $this->db->where("tgmtc like ","20".$thnini."%")->get("no_ctmofni")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>
                        <small><?php echo "20".$thnlal; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/imtc/20$thnlal"); ?>"><?php echo $this->db->where("tgmtc like ","20".$thnlal."%")->get("no_ctmofni")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun Lalu</div>
                    </div>
                </div>
            </div>
        </div>


</div> <!--/.row -->

<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"> Aktifitas FOC</a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-3">
        <?php 
            echo "<small>Pengkinian : ". date("d-m-Y", strtotime($updafoc))."</small>";
         ?></div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">

                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/afoc/$blnini"); ?>"><?php echo $this->db->where("date like ",$blnini."%")->get("reports")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan ini </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/afoc/$blnlal"); ?>"><?php echo $this->db->where("date like ",$blnlal."%")->get("reports")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked notepad"><use xlink:href="#stroked-notepad"></use></svg>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/afoc/20$thnini"); ?>"><?php echo $this->db->where("date like ","20".$thnini."%")->get("reports")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked notepad"><use xlink:href="#stroked-notepad"></use></svg>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php echo base_url("panel/afoc/20$thnlal"); ?>"><?php echo $this->db->where("date like ","20".$thnlal."%")->get("reports")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun Lalu</div>
                    </div>
                </div>
            </div>
        </div>


</div> <!--/.row -->




 
