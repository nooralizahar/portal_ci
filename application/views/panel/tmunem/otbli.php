<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("otbmen/"); ?>"> OTB</a></li>
            <li class="active"> Data</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Masukan Data OTB</h5>
                </div>
        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#requester" aria-controls="requester" role="tab" data-toggle="tab"> OTB Baru</a></li>
                            <li role="presentation" class="active"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Daftar OTB</a></li>
                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane" id="requester">
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">
        <form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Nama / Kode OTB</label>
                <div class='col-sm-10'>
                    <input type='text' name='nm_otb' class='form-control' id='nm_otb' required>
                    
                </div>

          </div>  
        <div class='form-group row'>
                <label for='lblkap' class='col-sm-2 form-control-label'>Pemilik</label>
                <div class='col-sm-2'>
                       <input type='text' name='ow_otb' class='form-control' id='ow_otb' value="JLM" required>
                </div>
                <label for='lblid_lok' class='col-sm-2 form-control-label'>Lokasi</label>
                <div class='col-sm-4'>
                    <select name="id_lok" id="id_lok" class="form-control" required>
                                <option value="">- Pilih Lokasi -</option>
                                <?php foreach($daflok as $a):?>
                                    <option value="<?php echo $a->id_lok; ?>">

                                        <?php echo $a->nm_lok; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
                <div class='col-sm-2'>
                    <a href="<?php echo base_url("otbmen/otblo/"); ?>" class="btn btn-success form-control" ><i class="fa fa-plus-square-o"></i> LOK. BARU</a>
                </div>
        </div> 

        <div class='form-group row'>
                <label for='lblkap' class='col-sm-2 form-control-label'>Kapasitas</label>
                <div class='col-sm-10'>
                       <input type='text' name='jm_por' class='form-control' id='jm_por' required>
                </div>
        </div>


   
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('otbmen/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
             </div>
         </div>
     </div>
</div>


<div role="tabpanel" class="tab-pane active" id="problem">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"><?php 
            //echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-2">  </div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_bto")->num_rows()); ?></a></div>
                        <div class="text-muted" > OTB</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked upload"><use xlink:href="#stroked-upload"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->get("t_bto_rel")->num_rows()); ?></div>
                        <div class="text-muted"> Core</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked chain"><use xlink:href="#stroked-chain"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->where("in_des !=","NOT SPLICE")->get("t_bto_rel")->num_rows()); ?></div>
                        <div class="text-muted"> Linked</div>
                    </div>
                </div>
            </div>
        </div>

</div> <br>




        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    
                    <tr>
                        <th data-field="a"  data-sortable="true" class="text-center">ID#</th>
                        <th data-field="b"  data-sortable="true" class="text-center">Nama / Kode OTB</th>
                        <th data-field="c"  data-sortable="true" class="text-center">Owner</th>
                        <th data-field="d"  data-sortable="true" class="text-center">Lokasi</th>
                        <th data-field="e"  data-sortable="true" class="text-center">Core</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Digunakan</th>
                        <th data-field="g"  data-sortable="true" class="text-center">Sisa</th>
                        <th data-field="h"  data-sortable="true" class="text-center">Opsi</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($otbsem as $dat): ?>
                        <tr>
                            <td class="text-center"><?php echo $dat->id_otb; ?></td>
                            <td class="text-left"><?php echo $dat->nm_otb; ?></td>
                            <td class="text-center"><?php echo $dat->ow_otb; ?></td>
                            <td class="text-center"><a href="<?php //echo base_url("otbmen/peta/".$dat->id_otb); ?>" target='_blank'><?php echo $dat->nm_lok; ?></a></td>
                            <td class="text-right"><?php echo $dat->jm_por; ?></td>
                            <td class="text-right"><?php echo ($dat->jm_por)-($dat->sisa); ?></td>
                            <td class="text-right" ><?php echo $dat->sisa; ?></td>
                            <td><center>
                                <a href="<?php echo base_url("otbmen/otbti/".$dat->id_otb); ?>" data-toggle='tooltip' title='Ubah'><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp; <!-- Edit OTB -->
                               <!-- <a href="<?php echo base_url("otbmen/odpli/".$dat->id_otb); ?>" data-toggle='tooltip' title='Tambah Pelanggan'><i class="fa fa-plus fa-lg"></i></a>&nbsp;&nbsp;   -->
                                <?php if($this->session->userdata('level')=='admin'): ?>
                                	<a href="<?php echo base_url("otbmen/otbha/".$dat->id_otb); ?>" data-toggle='tooltip' title='Hapus OTB'><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;
                                <?php endif;?>
                                <!--<?php if($dat->fileqr==null): ?>
                                <a href="<?php echo base_url("otbmen/odpqr/".$dat->id_otb); ?>" data-toggle='tooltip' title='Generate'><i class="fa fa-qrcode fa-lg"></i></a>
                                <?php else :?>
                                <a href="<?php echo base_url("otbmen/odpce/".$dat->id_otb); ?>" data-toggle='tooltip' title='Cetak' ><i class="fa fa-print fa-lg"></i></a>
                                <?php endif;?>-->
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

