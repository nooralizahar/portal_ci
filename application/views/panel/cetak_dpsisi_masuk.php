
<?php
$kantr= $this->session->userdata("id_dinas");
$markaz = $this->db->where("id_dinas",$kantr)->get("dinas")->result();
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/disposisi_masuk/"); ?>">Cetak</a></li>
            <li class="active">Cetak Lembaran Disposisi</li>
        </ol>
    </div><!--/.row-->
    <hr/>
    <div class="col-lg-12" id="disposisi">
            <table border="1px" width="100%">
                <tbody>
                    <tr>
                        <!--<td ></td>-->
                        <td align="center" colspan="7">
                        <?php   
                    foreach($markaz as $mkz):
                        if (empty($mkz->nama_ldinas)){
                            echo "<h3>PT. JALA LINTAS MEDIA</h3>";
                        }else{
                            echo "<h3>".strtoupper($mkz->nama_ldinas)."</h3>"; 
                        }
                        if (empty($mkz->almt_dinas)){
                            echo "<h5>Jl. Mayor Oking No. 89 Cibinong - Bogor</h5>";
                        }else{
                            echo "<h5>".$mkz->almt_dinas."</h5>";
                        }
                        if (empty($mkz->website)){
                            echo "<h5>Website : http://www.jlm.net.id</h5>";
                        }else{
                        echo "<h5>Website : ".$mkz->website."</h5>";
                        }
                        if (empty($mkz->email)){
                            echo "<h5>Email : marketing@jlm.net.id</h5>";
                        }else{                        
                        echo "<h5>Email : ".$mkz->email."</h5>";
                        }
                    endforeach;

                        ?>
       
                       </td>
                    </tr>
                   <hr/>
                    <tr>
                        <td align="center" colspan="4"> <h4>LEMBAR DISPOSISI</h4></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4"><small>PERHATIAN : Dilarang Memisahkan sehelai suratpun yang tergabung dalam berkas ini</small></td>
                    </tr>   
                            <tr>
                                <td colspan="2" id="right" width="50%">Tanggal Penerima Surat &nbsp;: <b><?php echo date("d-M-y",strtotime($disposisi->waktu_kirim)); ?></b><br>Nomor Agenda &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<?php echo $disposisi->lampiran_surat->nomor_agenda;?></td>
                                <td id="right" width="20%" align="center">
                                    Status<br>
                                    <input type="checkbox" name="bahasa1" value="php" />Asli <input type="checkbox" name="bahasa1" value="php" />Tembusan
                                </td>
                                <td id="" rowspan="2" width="60%">
                                    Klarifikasi Surat<br>
                                    <?php echo "<center><h4><b>".$disposisi->lampiran_surat->sifat."</b></h4></center>"; ?>
                                   <!-- <input type="checkbox" name="bahasa1" value="php" />Kilat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="bahasa1" value="php" />Sangat Rahasia<br>
                                    <input type="checkbox" name="bahasa1" value="php" />Sangat Segera  &nbsp;&nbsp;<input type="checkbox" name="bahasa1" value="php" />Rahasia<br>
                                    <input type="checkbox" name="bahasa1" value="php" />Segera &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="bahasa1" value="php" />Penting<br>
                                    <input type="checkbox" name="bahasa1" value="php" />Biasa -->
                                </td>
                            </tr>

                            <tr>
                                    <td colspan="2" id="right" width="50%">
                                        Nomor Surat : <b><?php echo $disposisi->lampiran_surat->nomor_surat; ?></b><br>
                                        Dari&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <?php echo $disposisi->penerima->pengirim; ?> <br>
                                        Hal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <b><?php echo $disposisi->lampiran_surat->subjek; ?></b><br>
                                        Lampiran &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                                    </td>                                   
                                    <td id="left" width="10%" align="center"> Tgl : <b><?php echo date("d-M-y",strtotime($disposisi->lampiran_surat->tanggal_surat)); ?></b></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"><h4>DISPOSISI KETUA/WAKIL KETUA *</h4></td>
                                </tr>
                                <tr>
                                    <td id="right" width="40%">
                                        KEPADA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="checkbox" name="bahasa1" value="php" />Panitera<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="bahasa1" value="php" />Sekretaris
                                    </td>                                   
                                    <td id="left" width="10%" align="left" colspan="3"> <input type="checkbox" name="bahasa1" value="php" />Wakil Ketua</td>
                                </tr>

                                <tr>
                                    <td colspan="4">
                                        PETUNJUK&nbsp; : 
                                        <?php echo "<center><h4><b>".$disposisi->instruksi_disposisi."</b></h4></center>"; ?><br>
                                        <!-- <input type="checkbox" name="bahasa1" value="php" />Setuju<br>                                     
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="bahasa1" value="php" />Tolak<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="bahasa1" value="php" />Teliti<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="bahasa1" value="php" />Untuk Diketahui 
                                    </td>                                   
                                    <td colspan="2" id="teng" width="10%" align="left">
                                        <input type="checkbox" name="bahasa1" value="php" />Untuk Ditindaklanjuti<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Untuk Diperhatikan<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Untuk Dipedomani<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Jawab Surat
                                    </td>
                                    <td colspan="2" id="left" width="10%" align="left">
                                        <input type="checkbox" name="bahasa1" value="php" />Bicarakan dengan saya<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Diteruskan<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Selesaikan-->
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4"><h4>ISI DISPOSISI</h4></td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4"><br><?php echo "".$disposisi->isi_disposisi.""; ?><br></td>
                                </tr>;
                            
                                <tr>
                                    <td align="center" colspan="2"><h4>SEKRETARIS</h4></td>
                                    <td align="center" colspan="2"><h4>PANITERA</h4></td>
                                </tr>;
                                <tr>
                                    <td colspan="2" id="lbr1" width="40%">
                                        KEPADA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="checkbox" name="bahasa1" value="php" />Kabag Perenc. & Kepegawaian<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="bahasa1" value="php" />Kabag Umum dan Keuangan
                                    </td>
                                    <td colspan="2" id="lbr1" width="40%">
                                        KEPADA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="checkbox" name="bahasa1" value="php" />Panitera Muda Banding<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="bahasa1" value="php" />Panitera Muda Hukum
                                    </td>
                                </tr>
                                <tr>
                                    <td id="right" width="30%">
                                        PETUNJUK&nbsp; : <input type="checkbox" name="bahasa1" value="php" />Laksanakan Disposisi<br>                                       
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="bahasa1" value="php" />Tolak<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="bahasa1" value="php" />Teliti<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="bahasa1" value="php" />Untuk Diketahui
                                    </td>                                   
                                    <td colspan="2" id="teng" width="10%" align="left">
                                        <input type="checkbox" name="bahasa1" value="php" />Perhatikan Disposisi Ketua<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Disiapkan<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Jawab Surat<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Selesaikan
                                    </td>
                                    <td colspan="2" id="left" width="10%" align="left">
                                        <input type="checkbox" name="bahasa1" value="php" />Bicarakan dengan saya<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Segera<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Perbanyak ......... Kali<br>
                                        <input type="checkbox" name="bahasa1" value="php" />Asli Kepada .........
                                    </td>
                                </tr>
                            
                                <tr>
                                    <td colspan="2" id="lbr1" width="40%">
                                        DISPOSISI : <br><br><br>
                                    </td>
                                    <td colspan="2" id="lbr1" width="40%">
                                        DISPOSISI : <br><br><br>
                                    </td>
                                </tr>
                            
                                <tr>
                                    <td align="center" colspan="2"><h4>KEPALA BAGIAN</h4></td>
                                    <td align="center" colspan="2"><h4>PANITERA MUDA</h4></td>
                                </tr>
                                <tr>
                                    <td colspan="2" id="lbr1" width="40%">
                                        KEPADA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="checkbox" name="bahasa1" value="php" />Kasubag Perc Prog Ang <input type="checkbox" name="bahasa1" value="php" />Kasubag TU RT<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="bahasa1" value="php" />Kasubag Kepeg IT &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="bahasa1" value="php" />Kasubag Keu & Pel
                                    </td>
                                    <td colspan="2" rowspan="3" id="lbr1" width="40%">
                                        <u><b>CATATAN PANMUD</b></u><br>
                                        Kepada : <br>
                                        Petunjuk :
                                    </td>
                                </tr>
                                <tr>
                                    <td id="lbr1" colspan="2">DISPOSISI : <br><br><br></td>                                 
                                </tr>
                                <tr>
                                    <td id="lbr1" colspan="2">
                                        <u><b>CATATAN KASUBAG</b></u><br>
                                        Kepada : <br>
                                        Petunjuk : <br><br><br><br>
                                    </td>                                   
                                </tr>
                                <tr>
                                    <td id="lbr1" colspan="2">
                                        Staf Yang Menerima : <br>
                                        Tanggal Penyelesaian :<br><br>
                                    </td>
                                    <td id="lbr1" colspan="2">
                                        Staf Yang Menerima : <br>
                                        Tanggal Penyelesaian :<br><br>
                                    </td>
                                </tr>
                </tbody>
            </table>
            <b>*Coret yang tidak perlu</b>
            <br><br>
    </div>
</div>

</div>	<!--/.main-->

<script>
    $(document).ready(function(){
        $(".attach-img").magnificPopup({
            type: "image"
        });

        setTimeout(function() {
            $('[data-toggle="tooltip').tooltip();
        },1000);

        $("#attach").fileinput({'showUpload':false});

        var followUpContainer = $("#followUp");
        var followUpContainerHeight = followUpContainer[0].scrollHeight;
        followUpContainer.scrollTop(followUpContainerHeight);

        $(".attach").on("click",function(ev){
            var data_file = $(this).data().file;
            var content = "<ol>";

            $.each(data_file,function(key,item){
                var cls = item.file.split(".").length;
                var tipe_gambar = ["jpg","png","jpeg","bmp","gif"];
                cls = ($.inArray(item.file.split(".")[cls-1],tipe_gambar) == -1) ? "attach-doc" : "attach-img";
                content += "" +
                    "<li><a class='"+cls+"' href='"+BASE_URL+"assets/uploads/lampiran/follow_up_disposisi/"+item.file+"'>"+item.judul+"</a></li>";
            });

            content += "</ol>";

            bootbox.alert({
                size: "normal",
                title: "Lampiran follow-up",
                message: content
            });


            $(".attach-img").magnificPopup({
                type: "image"
            });


            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                $("#myModal").css("z-index",2000);
                return false;
            });


            ev.preventDefault();
        });

        $("#isi_disposisi").shorten({
            moreText: "Selengkapnya",
            lessText: "Kecilkan",
            showChars: 300
        });

        $(function(){
            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                return false;
            });
        });

        $(".star.fa-star").on("click",function(){
            var curStar = $(this);
            update_star(curStar);
            curStar.removeClass("fa-star").addClass("fa-spinner fa-pulse");
        });
    });

    function update_star(curStar) {
        $.ajax({
            url: BASE_URL + "/ajax/update_star_disposisi/",
            method: "POST",
            data: {
                id_relasi_disposisi: curStar.data().id,
                starred: (curStar.data().starred == 1) ? 0 : 1
            },
            success: function(response){
                if(curStar.data().starred == 0) {
                    curStar.data("starred",1);
                    curStar.addClass("star-active");
                    curStar.parent().children("span").html(1)
                } else {
                    curStar.data("starred",0);
                    curStar.removeClass("star-active");
                    curStar.parent().children('span').html(0);
                }
                curStar.removeClass('fa-spinner fa-pulse');
                curStar.addClass("fa-star");
            }
        });
    }
</script>