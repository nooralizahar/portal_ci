<?php $sales=$this->session->userdata("username")?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/womoalok/"); ?>">Dispatch WO</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah progress.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Progres baru berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h3 style="color:#010288"><b> No. WO : <?php echo $idno; ?></b></h3>
                </div>
        <div class="panel-body">

        <form class='add-item' method=POST action=''>
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>No. WO</label>
                <div class='col-sm-10'>
                    <input type='text' name='wono' class='form-control' id='id_worde' value="<?php echo $idno; ?>" /readonly>
                </div>
          </div>  
         <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Dispacth Kepada</label>
                <div class='col-sm-10'>
                            <select name="us_assto" class="form-control penerima" multiple>
                                <?php foreach($daftar_saladm as $jabatan):?>
                                    <option value="<?php echo $jabatan->id_pengguna; ?>"> <?php echo $jabatan->nama_lengkap."&nbsp;".$jabatan->nm_jab;?></option>
                                <?php endforeach;?>
                            </select>           
                </div>
          </div> 
           
                  
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/womonito') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>



    <br>

                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('ket',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>


    $(document).ready(function(){
        $("#msg").froalaEditor({
            height: 300
        });

        $(".penerima").select2();
        $(".segment").select2();
        $(".media").select2();
        $(".request").select2();
        $(".service").select2();

        $("#tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>