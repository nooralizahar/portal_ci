﻿<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	

    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/"); ?>">Dashboard</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>
<?php if ($this->session->userdata('id_jabatan')==5) : ?>
  
<?php $this->load->view("/panel/ompunem/dashpmo")?>

<?php elseif ($this->session->userdata('id_jabatan')==4) : ?>
  
<?php $this->load->view("/panel/ompunem/dashpmo") //$this->load->view("/panel/dashkos")?>

<?php else :?>    
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">COMMERCIAL DASHBOARD</div>
                <div class="panel-body">    

                  <div class="row">
                      <div class="col-lg-12">


                          <div class="tabbable-panel">
                              <div class="tabbable-line">
                                  <ul class="nav nav-tabs ">


								<?php if ($this->session->userdata('id_jabatan')==9 || $this->session->userdata('id_jabatan')==32 || $this->session->userdata('id_jabatan')==20 ) :?>
                                      <li class="active">
                                          <a href="#tabnol" data-toggle="tab">
                                          Approval NMC</a>
                                      </li>									
                                      <li>
                                          <a href="#tabsatu" data-toggle="tab">
                                          Permintaan Harga</a>
                                      </li>
                                      <li>
                                          <a href="#tabdua" data-toggle="tab">
                                          Rekapitulasi </a>
                                      </li>

								<?php else :?>
                                      <li class="active">
                                          <a href="#tabsatu" data-toggle="tab">
                                          Permintaan Harga</a>
                                      </li>
                                      <li>
                                          <a href="#tabdua" data-toggle="tab">
                                          Rekapitulasi </a>
                                      </li>
								<?php endif;?>
                                  </ul>
                                  
                                  <div class="tab-content">

                                  	<?php if ($this->session->userdata('id_jabatan')==9 || $this->session->userdata('id_jabatan')==32 || $this->session->userdata('id_jabatan')==20 ) :?>

                                  	  <div class="tab-pane active" id="tabnol">
                                        <?php $this->load->view("/panel/dashcomappv")?>

                                      </div>

                                      <div class="tab-pane" id="tabsatu">
                                        <?php $this->load->view("/panel/dashcomdtil")?>

                                      </div>
                                      <?php else :?>
                                      <div class="tab-pane active" id="tabsatu">
                                        <?php $this->load->view("/panel/dashcomdtil")?>

                                      </div>


                                      <?php endif;?>

                                      <div class="tab-pane" id="tabdua">
                                        <?php $this->load->view("/panel/dashcomrkap")?>

                                      </div>
                                  
                                  </div>
                              </div>
                          </div>


                      </div> <!--/.col-->
                  </div><!--/.row-->           		

  
			  
                </div> <!--/.panel body -->
            </div> <!--/.panel default -->
          </div> <!--/.col -->
      </div> <!--/.row -->

<?php endif;?>

</div>

 
