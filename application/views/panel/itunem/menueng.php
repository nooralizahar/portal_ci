﻿<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	

    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/dashit/"); ?>">Menu TI</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>
<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> DEPARTEMEN ENGINEERING</div>
                <div class="panel-body">                <!-- /. ROW  #2--> 		

<div class="row text-center pad-top"> <!-- awal dari halaman dalam  --> 
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol enam  -->
                      <div class="div-square">
                           <a href="<?php echo base_url("/panel/womonito")?>" >
             <i class="fa fa-connectdevelop fa-5x"></i>  
                      <h5>Topology</h5>
                      </a>
                      </div>
          </div>				

          		  <div class='col-lg-2 col-md-2 col-sm-2 col-xs-6'> <!-- tombol dua  -->
		                      <div class='div-square'>
		                      <a href="<?php echo base_url("focmen/odpin/"); ?>" >
		             <i class='fa fa-home fa-5x'></i>  
		                      <h5>O D P</h5>
		                      </a>
		                      </div>
		            </div>

				 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol tiga  -->
                      <div class="div-square">
                        <a href="<?php echo base_url("focmen/odcin/"); ?>" >
						<i class="fa fa-map-pin fa-5x"></i>
                      <h5>O L T</h5>
                      </a>
                      </div>  
                  </div>
                  
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol satu  -->
              <div class="div-square">
                  <a href="<?php echo base_url("focmen/odpcsn/"); ?>" >
                    <i class="fa fa-users fa-5x"></i>
                    <h5> Pelanggan</h5>
                  </a>
              </div>
      </div> 			  
 
				  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol dua  -->
                      <div class="div-square">
                           <a href="<?php echo base_url("otbmen/otbli/"); ?>" >
						    <i class="fa fa-server fa-5x"></i>
                            <h5>O T B</h5>
                           </a>
                      </div>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol enam  -->
                      <div class="div-square">
                           <a href=/panel/mtainmon >
             <i class="fa fa-info-circle fa-5x"></i>  
                      <h5>Maintenance Info</h5>
                      </a>
                      </div>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol tujuh -->
                      <div class="div-square">
                           <a href=https://rack.bnetfit.com/ >
             <i class="fa fa-archive fa-5x"></i>  
                      <h5>Data Rack</h5>
                      </a>
                      </div>
                  </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol satu  -->
              <div class="div-square">
                  <a href="/selser/repsla" >
                          <i class="fa fa-upload fa-5x"></i>
                    <h5>Laporan SLA</h5>
                  </a>
              </div>
        </div>  

        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol satu  -->
              <div class="div-square">
                  <a href="/datbar/logbar/" >
                          <i class="fa fa-table fa-5x"></i>
                    <h5>Data Barang</h5>
                  </a>
              </div>
        </div>   
                  
				  
</div>
			  
                <!-- /. ROW  #3--> 
			
<div class="row text-center pad-top">

           
 </div>  


			    <!-- /. ROW  #4-->
				
<div class="row text-center pad-top">	


     
                     
</div>
				  
			      <!-- /. ROW #5-->  
				                     
 <div class="row text-center pad-top">



</div> 
</div>

                </div> <!--/.panel body -->
            </div><!--/.panel heading -->
        </div> <!--/.panel default -->
    </div><!--/.col -->

</div>  <!--/.row -->
