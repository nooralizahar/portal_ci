﻿<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	

    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/dashit/"); ?>">Menu TI</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>
<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> DIVISI TEKNOLOGI INFORMASI</div>
                <div class="panel-body">                <!-- /. ROW  #2--> 		

<div class="row text-center pad-top"> <!-- awal dari halaman dalam  --> 
				
				  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol satu  -->
              <div class="div-square">
                  <a href="/panel/excel" >
			              <i class="fa fa-upload fa-5x"></i>
                    <h5>Import SLA</h5>
                  </a>
              </div>
          </div>

          		  <div class='col-lg-2 col-md-2 col-sm-2 col-xs-6'> <!-- tombol dua  -->
		                      <div class='div-square'>
		                      <a href="<?php echo base_url("focmen/odpin/"); ?>" >
		             <i class='fa fa-home fa-5x'></i>  
		                      <h5>O D P</h5>
		                      </a>
		                      </div>
		          </div>

				 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol tiga  -->
                      <div class="div-square">
                        <a href="<?php echo base_url("focmen/oltin/"); ?>" >
						<i class="fa fa-map-pin fa-5x"></i>
                      <h5>O L T</h5>
                      </a>
                      </div>  
                  </div>
                  
      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol satu  -->
              <div class="div-square">
                  <a href="<?php echo base_url("focmen/odpcsn/"); ?>" >
                    <i class="fa fa-users fa-5x"></i>
                    <h5> Pelanggan</h5>
                  </a>
              </div>
      </div> 			  
 
				  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol dua  -->
                      <div class="div-square">
                           <a href="<?php echo base_url("otbmen/"); ?>" >
						    <i class="fa fa-server fa-5x"></i>
                            <h5>O T B</h5>
                           </a>
                      </div>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol enam  -->
                      <div class="div-square">
                           <a href=/panel/mtainmon >
             <i class="fa fa-info-circle fa-5x"></i>  
                      <h5>Maintenance Info</h5>
                      </a>
                      </div>
                  </div>



				  
				  

				  
				  
				  

	  



				  
</div>
			  
                <!-- /. ROW  #3--> 
			
<div class="row text-center pad-top">

      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> 
              <div class="div-square">
                  <a href="<?php echo base_url("datbar/logboo/"); ?>" >
                    <i class="fa fa-book fa-5x"></i>
                    <h5> Log Book</h5>
                  </a>
              </div>
      </div>     

      <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> 
              <div class="div-square">
                  <a href="<?php echo base_url("datbar/logbar/"); ?>" >
                    <i class="fa fa-table fa-5x"></i>
                    <h5> Data Barang</h5>
                  </a>
              </div>
      </div> 

  
				 <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol enam  -->
                      <div class="div-square">
                           <a href=unem.php?dom=retuor >
						 <i class="fa fa-road fa-5x"></i>  
                      <h5>Router</h5>
                      </a>
                      </div>
          </div>

          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol enam  -->
                      <div class="div-square">
                           <a href=/panel/womonito >
             <i class="fa fa-connectdevelop fa-5x"></i>  
                      <h5>Topology</h5>
                      </a>
                      </div>
          </div>


				 <!-- <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">  tombol enam  
                      <div class="div-square">
                           <a href=../prefix/index.php/home >
						 <i class="fa fa-external-link fa-5x"></i>  
                      <h5>Prefix</h5>
                      </a>
                      </div>
                  </div>
				  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">  tombol enam  
                      <div class="div-square">
                           <a href=../report_sla/index.php/home >
						 <i class="fa fa-external-link fa-5x"></i>  
                      <h5>Report SLA</h5>
                      </a>
                      </div>
                  </div>-->



                  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol tujuh -->
                      <div class="div-square">
                           <a href=https://rack.bnetfit.com/ >
						 <i class="fa fa-archive fa-5x"></i>  
                      <h5>Data Rack</h5>
                      </a>
                      </div>
                  </div>
 
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol satu  -->
              <div class="div-square">
                  <a href="/selser/repsla" >
                    <i class="fa fa-upload fa-5x"></i>
                    <h5>Laporan SLA</h5>
                  </a>
              </div>
          </div>  


   
				  

                   
 </div>  


			    <!-- /. ROW  #4-->
				
<div class="row text-center pad-top">	

          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol satu -->
                      <div class="div-square">
                           <a href=unem.php?dom=fitnesni >
             <i class="fa fa-money fa-5x"></i>  
                      <h5>Insentif FTTH/X</h5>
                      </a>
                      </div>
          </div>

          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol dua  -->
                    <div class="div-square">
                      <a href=unem.php?dom=pasknil >
                        <i class="fa fa-object-group fa-5x"></i>  
                        <h5>SAP <b>B1</b></h5>
                      </a>
                    </div>
          </div>	
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol tiga  -->
                      <div class="div-square">
                           <a href=unem.php?dom=samage >
             <i class="fa fa-envelope fa-5x"></i>  
                      <h5><b>E-</b>GAMAS </h5>
                      </a>
                      </div>
           </div>
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> <!-- tombol empat  -->
                      <div class="div-square">
                           <a href=/panel/netflix >
             <i class="fa fa-users fa-5x"></i>  
                      <h5><b>DB</b>NETFLIX </h5>
                      </a>
                      </div>
           </div>

      
     
                     
</div>
				  
			      <!-- /. ROW #5-->  
				                     
 <div class="row text-center pad-top">



</div> 
</div>

                </div> <!--/.panel body -->
            </div><!--/.panel heading -->
        </div> <!--/.panel default -->
    </div><!--/.col -->

</div>  <!--/.row -->
