<?php
    $thnini= date('Y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    if(substr($thnbli, 2)==01){$bln=12;}else{$bln='0'.substr($thnbli, 2)-1;}
    $thnblu= ((substr($thnbli,0, 2))-1).$bln;
    $blnini=date('Y/m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 

        $tik=$this->db->order_by("RowID","desc")->limit(1)->get("t_datame")->result();
        foreach($tik as $d): 
        $lastik=$d->Created_Time;   
        endforeach;
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/dashit/"); ?>"> Menu TI</a></li>
            <li class="active"> Import</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                        <?php
                        /*$pesan = $this->session->flashdata('pesan');
                        if ($pesan) {
                            echo '<div class="alert alert-info" style="margin-top: 5px;margin-bottom: 5px;padding-top: 5px;padding-bottom: 5px;">' . $pesan . '</div>';
                        }*/
                        ?>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal tambah  !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Import berhasil disimpan ke tujuan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h5> Import Data Tiket</h5>
                </div>
        <div class="panel-body">
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"><?php 
            echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-3">
        <?php 
            //echo "<small>Pengkinian : ". date("d-m-Y", strtotime($updidwo))."</small>";
         ?></div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_datame")->num_rows()); ?></a></div>
                        <div class="text-muted">Total Tiket</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnlal"); ?>"><?php echo number_format($this->db->where('Created_Time like', $thnini.'%')->get("t_datame")->num_rows()); ?></a></div>
                        <div class="text-muted">Tahun Ini</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar blank"><use xlink:href="#stroked-calendar-blank"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php echo number_format($this->db->where('Created_Time like',$blnini.'%')->get("t_datame")->num_rows()); ?> </a></div>
                        <div class="text-muted">Bulan ini</div>
                    </div>
                </div>
            </div>
        </div>



</div> 

    <br>
<div class='form-group row'>
    <div class='col-sm-9'>
    </div>
    <div class='col-sm-3'>
        <a href="<?php echo base_url("assets/format/ImportTiket2020_Format.xls"); ?>" download class="btn btn-success"><i class="fa fa-download"></i>
  Download template XLS </a>
    </div>
</div>

        <form class='add-item' action="" method=POST id="import_form" enctype="multipart/form-data">
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>File Excel</label>
                <div class='col-sm-10'>
                <input type="file" name="file" id="file" required accept=".xls, .xlsx" /></p>
            <br />
            <!--<input type="submit" name="import" value="Import" class="btn btn-info" /> -->
                </div>
          </div>  

            
                  
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="import" class="btn btn-primary"><i class="fa fa-upload fa-lg"></i><br><small><sup>PROSES</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/menuit') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>


    <br>



        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

<script>
$(document).ready(function(){

    load_data();

    function load_data()
    {
        $.ajax({
            url:"<?php echo base_url(); ?>excel_import/fetch",
            method:"POST",
            success:function(data){
                $('#customer_data').html(data);
            }
        })
    }

    $('#import_form').on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url:"<?php echo base_url(); ?>panel/import",
            method:"POST",
            data:new FormData(this),
            contentType:false,
            cache:false,
            processData:false,
            success:function(data){
                $('#file').val('');
                load_data();
                alert(data);
            }
        })
    });

});
</script>