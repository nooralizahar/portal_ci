<?php
foreach($datait_byid as $datodc){
  $nmodc=$datodc->nama;
  $kor=$datodc->kordinat;
}
echo "<div class='col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main'>";
echo "</div>";//$kor
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">

  <title>Perangkat Bnetfit <?php echo $nmodc; ?></title>
  <link rel="shortcut icon" href="images/favicon.png">
<script src="http://maps.googleapis.com/maps/api/js"></script>

<script>
function initialize() {
  var propertiPeta = {
    center:new google.maps.LatLng(<?php echo $kor; ?>),
    zoom:9,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  
  var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);
  
  // membuat Marker
  var marker=new google.maps.Marker({
      position: new google.maps.LatLng(<?php echo $kor; ?>),
      map: peta,
      icon: "../../assets/images/bnetfit_ms.png",
      animation: google.maps.Animation.BOUNCE
  });

}

// event jendela di-load  
google.maps.event.addDomListener(window, 'load', initialize);
</script>
  
</head>
<body>

  <div id="googleMap" style="width:100%;height:90%;"></div>
  
</body>
</html>
