<div class="form-group"> 
                     <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="15"><center><h3>TAHUN <?= date('Y') ?></h3></center></th></tr>
                            <tr>

                                <th>&nbsp;No.</th>
                                <th>&nbsp;NAMA</th>
                                <th><center>JAN</center></th>
                                <th><center>FEB</center></th>
                                <th><center>MAR</center></th>
                                <th><center>APR</center></th>
                                <th><center>MEI</center></th>
                                <th><center>JUN</center></th>
                                <th><center>JUL</center></th>
                                <th><center>AGU</center></th>
                                <th><center>SEP</center></th>
                                <th><center>OKT</center></th>
                                <th><center>NOP</center></th>
                                <th><center>DES</center></th>
                                <th><center>TOTAL</center></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php 
                            $daf=$summarypre;
                            $no=1;
                            foreach($daf as $h): ?>
                                <tr >
                                    <td align="right"><?php echo $no++; ?> </td>
                                    <td><?php echo $h->NAMA; ?> </td>
                                    <td align="center"><?php echo $h->JAN; ?> </td>
                                    <td align="center"><?php echo $h->FEB; ?> </td>
                                    <td align="center"><?php echo $h->MAR; ?> </td>
                                    <td align="center"><?php echo $h->APR; ?> </td>
                                    <td align="center"><?php echo $h->MEI; ?> </td>
                                    <td align="center"><?php echo $h->JUN; ?> </td>
                                    <td align="center"><?php echo $h->JUL; ?> </td>
                                    <td align="center"><?php echo $h->AGU; ?> </td>
                                    <td align="center"><?php echo $h->SEP; ?> </td>
                                    <td align="center"><?php echo $h->OKT; ?> </td>
                                    <td align="center"><?php echo $h->NOP; ?> </td>
                                    <td align="center"><?php echo $h->DES; ?> </td>
                                    <td align="center"><?php echo $h->TOTAL; ?> </td>

                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>
 </div>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title" >
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseA">Summary per Sales</a>
        </h4>
      </div>
      <div id="collapseA" class="panel-collapse collapse">
        <div class="panel-body"><!--<h3>Data Belum Tersedia</h3>-->

            <center><h2><u>DETIL BERDASARKAN DURASI</u></h2></center>
            <?php $this->load->view("/panel/rptcom/sumpretil")?>

        </div>
      </div>
    </div>
</div>
     <div class="panel-heading">
        <h4 class="panel-title" >
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">BERDASARKAN DURASI</a>
        </h4>
      </div>
    <div id="collapse6" class="panel-collapse collapse">
        <div class="panel-body"><!--<h3>Data Belum Tersedia</h3>-->
            <div class="form-group">
      <center><h2>BERDASARKAN DURASI</h2></center>

                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="15"><center><h3>TAHUN <?= date('Y') ?></h3></center></th></tr>
                            <tr>

                                <th>&nbsp;No.</th>
                                <th>&nbsp;NAMA</th>
                                <th><center>DURASI</center></th>
                                <th><center>JAN</center></th>
                                <th><center>FEB</center></th>
                                <th><center>MAR</center></th>
                                <th><center>APR</center></th>
                                <th><center>MEI</center></th>
                                <th><center>JUN</center></th>
                                <th><center>JUL</center></th>
                                <th><center>AGU</center></th>
                                <th><center>SEP</center></th>
                                <th><center>OKT</center></th>
                                <th><center>NOP</center></th>
                                <th><center>DES</center></th>
                                <th><center>TOTAL</center></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php 
                            $daf=$summarydur;
                            $no=1;
                            foreach($daf as $h): ?>
                                <tr>
                                    <td align="right">
                                    <?php

                                    if ($h->DURASI=='SUB TOTAL'){
                                    $no=1; 
                                    $n='JUMLAH';
                                    }else{
                                    echo $no++; 
                                    $n=$h->NAMA;   
                                    } 

                                    ?> </td>
                                    <td ><?php echo $n; ?> </td>
                                    <td align="center"><?php echo $h->DURASI; ?> </td>
                                    <td align="center"><?php echo $h->JAN; ?> </td>
                                    <td align="center"><?php echo $h->FEB; ?> </td>
                                    <td align="center"><?php echo $h->MAR; ?> </td>
                                    <td align="center"><?php echo $h->APR; ?> </td>
                                    <td align="center"><?php echo $h->MEI; ?> </td>
                                    <td align="center"><?php echo $h->JUN; ?> </td>
                                    <td align="center"><?php echo $h->JUL; ?> </td>
                                    <td align="center"><?php echo $h->AGU; ?> </td>
                                    <td align="center"><?php echo $h->SEP; ?> </td>
                                    <td align="center"><?php echo $h->OKT; ?> </td>
                                    <td align="center"><?php echo $h->NOP; ?> </td>
                                    <td align="center"><?php echo $h->DES; ?> </td>
                                    <td align="center"><?php echo $h->TOTAL; ?> </td>

                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>
                    </div>
                </div>
            </div>