<?php
$idjab=$this->session->userdata("id_jabatan");
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/hargatamb/"); ?>">Pra BP Barang</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Form Pra BP Barang</div>
                <div class="panel-body">
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal tambah harga! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Tambah harga berhasil disimpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

<p><?php //echo $hjl; echo "<br>".$hve; echo "<br>".$hid; //untuk cek data masuk?></p>
    <?php if($this->session->userdata('id_jabatan')==6 && $dafrid->stapv1==null): ?> 
        <form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lblitem' class='col-sm-1 form-control-label'>Item</label>
                <div class='col-sm-4'>
                    <input type='text' name='item' class='form-control' placeholder="Ketik item disini" >
                    
                </div>

                <label for='lblqty' class='col-sm-1 form-control-label'>Qty </label>
                <div class='col-sm-1'>
                    <input type='text' name='qty' class='form-control' value="1">
                </div>
         </div>       
         <div class='form-group row'>
                <label for='lblhgven' class='col-sm-2 form-control-label'>Harga Vendor </label>
                <div class='col-sm-2'>
                    <input type='text' name='hgven' class='form-control' placeholder="Ketik harga disini">
                </div>
                <label for='lblhgven' class='col-sm-2 form-control-label'>Harga JLM </label>
                <div class='col-sm-2'>
                    <input type='text' name='hgjlm' class='form-control' placeholder="Ketik harga disini">
                </div>
                <label for='lblket' class='col-sm-1 form-control-label'>Keterangan </label>
                <div class='col-sm-2'>
                    <input type='text' name='ket' class='form-control' value="Excl. PPN">
                </div>
        </div>  
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="kembali()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button> <!-- "window.location.href = '<?php //echo base_url('focmen/') ?>';"-->
            </div>
        </div>
                
        </form>

        <?php else : ?>
            <center> <h4>----- CALCULATION -----</h4> </center>
        <?php endif; ?>
        </div> <!-- end 1st row -->

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                    <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="a" data-sort-order="asc">
                                <thead>
                                
                                <tr>
                                    <th data-field="a"  data-sortable="true" class="text-center">#</th>
                                    <th data-field="b"  data-sortable="true" class="text-center">Item</th>
                                    <th data-field="c"  data-sortable="true" class="text-center">Qty</th>
                                    <th data-field="d"  data-sortable="true" class="text-center">Harga Vendor</th>
                                    <th data-field="e"  data-sortable="true" class="text-center">Harga JLM</th>
                                    <th data-field="f"  data-sortable="true" class="text-center">Tot. Harga Vendor</th>
                                    <th data-field="g"  data-sortable="true" class="text-center">Tot. Harga JLM</th>
                                     <th data-field="h"  data-sortable="true" class="text-center">Keterangan</th>                                   

                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($dafibp as $bp): ?>
                                    <tr>
                                        <td class="text-center"><?php echo $bp->id; ?></td>
                                        <td class="text-left"><?php echo $bp->item; ?></td>
                                        <td class="text-right"><?php echo number_format($bp->qty); ?></td>
                                        <td class="text-right"><?php echo number_format($bp->hgven); ?></td>
                                        <td class="text-right"><?php echo number_format($bp->hgjlm); ?></td>
                                        <td class="text-right"><?php echo number_format($bp->toven); ?></td>    
                                        <td class="text-right"><?php echo number_format($bp->tojlm); ?></td>
                                        <td class="text-left"><?php echo $bp->ket; ?></td>

                                    </tr>
                                     
                                    <?php endforeach; ?>

                                </tbody>
                        </table>
                        <table border='0' width="100%"> 
                        	<tr><td>.</td><td>.</td><td>.</td><td>.</td><td>.</td><td class="text-left" style="color: white">Tot. Harga Vendor</td><td class="text-left" style="color: white">Tot. Harga JLM</td><td style="color: white">Keterangan</td></tr>
                        	<tr><td colspan="5" class="text-left" width="50%"><b>&nbsp;&nbsp;Total</b></td><td class="text-right"><?php if($hve==0){echo 0;}else{echo number_format($hve);} ?></td><td class="text-right"><?php if($hjl==0){echo 0;}else{echo number_format($hjl);} ?></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" ><b>&nbsp;&nbsp;Revenue Customer</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php echo number_format($hjl); ?></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" ><b>&nbsp;&nbsp;PPN 10%</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php if($hjl==0){echo 0;}else{ echo number_format($hjl*0.1);} ?></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" ><b>&nbsp;&nbsp;Total Revenue (Incl. PPN)</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php if($hjl==0){echo 0;}else{$trip=$hjl+($hjl*0.1); echo number_format($trip);} ?></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" ><b>&nbsp;&nbsp;Tax Corporate 25%</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php $tcor=($hjl-$hve)*0.25; echo number_format($tcor); ?></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" ><b>&nbsp;&nbsp;NMC </b>- <?php if(!empty($dafrid->nmc)){$pecax=explode('/',$dafrid->nmc); echo $pecax[1];}else{}  ?></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right text-danger"><b><?php if(!empty($dafrid->nmc)){$pecah=explode('/',$dafrid->nmc);}else{$pecah=null;} if(is_null($pecah)){$nmc=0;}else{$nmc=$pecah[0];} echo number_format($nmc); ?></b></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" >
                            <?php if($this->session->userdata('id_jabatan')!=6): ?>
                            <b>&nbsp;&nbsp;Manage Service</b>
                            <?php endif; ?>
                            </td>
                            <td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php if(!empty($dafrid->mse)){$mser=$dafrid->mse;}else{$mser=0;} echo number_format($mser); ?></td><td>&nbsp;
                        <?php if($this->session->userdata('id_jabatan')==6): ?> 
                        <a href="" data-toggle="modal" data-target="#edithItemcausegroup<?= $id; ?>">Manage Service</a>
                    <?php endif; ?>
                        </td></tr>
                        <tr><td colspan="5" class="text-left" >&nbsp;&nbsp;Margin</td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><b><?php $marg=$hjl-$hve-$tcor-$nmc-$mser; echo number_format($marg); ?></b></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" >&nbsp;&nbsp;Percentage</td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><b><?php if($marg<=0){echo "error";}else{echo number_format(($marg/$hjl)*100).'%';} ?></b></td><td></td></tr>



                        </table>
                        <p><br><br></p> 
<form class='add-item' method=POST action='<?php echo base_url('/panel/appvnmc/'.$id); ?>'>
    <?php if($idjab!=6):?>
            <div class='form-group row'>
            	<div class='col-sm-5'></div>
            	<div class='col-sm-2'>  
                    <select name="stappv" class="form-control" /> 
                       <option value="1" selected>Approved</option>
                       <option value="2">Rejected</option>
                    </select>
                </div>
                <div class='col-sm-5'></div>
            </div>        
            <div class='form-group row'>
            	<div class='col-sm-3'></div>
            	<div class='col-sm-6'>         		
                    <label>Catatan </label>
                    <input type='text' name='caappv' class='form-control' placeholder="Ketik catatan disini">
                </div>
                <div class='col-sm-3'></div>
            </div>
    <?php endif;?>
 			<div class='form-group row'>
 				<div class='col-sm-1'></div>
                <div class='col-sm-10'>
                	<center>
                    <?php if($idjab==9):?> <!-- Sales Manager Appv -->
                    <button type="submit" name="btnSimpanSGM" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button> 
                    <?php elseif($idjab==32):?> <!-- Busdev Manager Appv -->
                    <button type="submit" name="btnSimpanBDM" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                    <?php elseif($idjab==20):?>
                    <button type="submit" name="btnSimpanDIR" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                    <?php else:?>
                    <!--<button type="submit" name="btnSimpan" class="btn btn-success"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>-->
                    <?php endif;?>    
                    <button type='button' onclick="kembali()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </center>
                </div>                                   
            </div>    
        </form>
            <div class="col-md-1"></div>                            
        </div> <!-- end 2nd row -->

<?php

$id_mse = $id;

//foreach ($dafrid as $d) :
    
 
?>

    <div class="modal fade" id="edithItemcausegroup<?= $id_mse; ?>" tabindex="-1" role="dialog" aria-labelledby="edithItemcausegroupLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="edithItemcausegroup<?= $id_mse; ?>Label">Tambah Biaya Manage Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('panel/isimser/'); ?>" method="post">
                    <div class="modal-body">
                        <input type="hidden" name='id_rha' value="<?= $id_mse; ?>">
                        <div class="form-group">
                            <label>Pelanggan : <?php echo $dafrid->nm_custo; ?></label>
                            <label>Alamat : <?php echo $dafrid->al_custo; ?></label>

                        </div>                        
                        <div class="form-group">
                            <label>Biaya Manage Service (Rp.) </label>
                            <input type="text" class="form-control" id="mse" name="mse" placeholder="Ketik biaya disini" value="<?php echo $dafrid->mse; ?>">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php //endforeach; ?>



<script>
function kembali() {
  window.history.back();
}
</script>