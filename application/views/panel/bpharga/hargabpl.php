<?php




?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/hargatamb/"); ?>">Pra BP Link</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Form Pra BP Link</div>
                <div class="panel-body">
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal tambah harga! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Tambah harga berhasil disimpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

<p><?php //echo $hjl; echo "<br>".$hve; echo "<br>".$hid; //untuk cek data masuk?></p>



        </div> <!-- end 1st row -->

<div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
        <table width="100%">
        
        <tr><td colspan="8">        

        <?php if($this->session->userdata('id_jabatan')==6): ?>    
        <form class='add-item' method=POST action=''>
        
        <div class='form-group row'>
                <label for='lblitem' class='col-sm-1 form-control-label'>Item</label>
                <div class='col-sm-4'>
                    <input type='text' name='item' class='form-control' placeholder="Ketik item disini" required/>
                    
                </div>

                <label for='lblqty' class='col-sm-1 form-control-label'>Amount </label>
                <div class='col-sm-1'>
                    <input type='text' name='qty' class='form-control' value="1">
                </div>
                <label for='lblhgven' class='col-sm-2 form-control-label'>Unit Price </label>
                <div class='col-sm-2'>
                    <input type='text' name='hgven' class='form-control' placeholder="Ketik harga disini" required/>
                </div>
        </div>  
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="kembali()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button> <!-- "window.location.href = '<?php //echo base_url('focmen/') ?>';"-->
            </div>
        </div>
                
        </form>
        <?php else : ?>
            <center> <h4>----- C A P E X -----</h4> </center>
        <?php endif; ?>


        </td></tr>


        </table>
                    <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="a" data-sort-order="asc">
                                <thead>
                                <tr>
                                    <th data-field="a"  data-sortable="true" class="text-center">#</th>
                                    <th data-field="b"  data-sortable="true" class="text-center">Item</th>
                                    <th data-field="c"  data-sortable="true" class="text-center">Amount</th>
                                    <th data-field="d"  data-sortable="true" class="text-center">Unit Price</th>
                                    <th data-field="e"  data-sortable="true" class="text-center">Total Price</th>                                 
                                    <th data-field="f"  data-sortable="true" class="text-center">Keterangan</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($dafibp as $bp): ?>
                                    <tr>
                                        <td class="text-center"><?php echo $bp->id; ?></td>
                                        <td class="text-left"><?php echo $bp->item; ?></td>
                                        <td class="text-right"><?php echo number_format($bp->qty); ?></td>
                                        <td class="text-right"><?php echo number_format($bp->hgven); ?></td>
                                        <td class="text-right"><?php echo number_format($bp->toven); ?></td>
                                        <td></td>
                                    </tr>
                                     
                                    <?php endforeach; ?>

                                </tbody>
                        </table>
                        <table border='0' width="100%"> 
                        	<tr><td>.</td><td>.</td><td>.</td><td>.</td><td>.</td><td class="text-left" style="color: white">Unit Price</td><td class="text-left" style="color: white">Total Price</td><td style="color: white">Keterangan</td></tr>
                        	<tr><td colspan="5" class="text-left" width="50%" style="color: white"><b>Total</b></td><td class="text-right"><?php //if($hve==0){echo 0;}else{echo number_format($hve);} ?></td><td class="text-right"><?php //.. ?></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" ><b>Total CAPEX</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php if($hve==0){echo 0;}else{echo number_format($hve);} ?></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" ><b>CapexDistribution 100%</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php if($hve==0){echo 0;}else{echo number_format($hve);} ?></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" ><b>Capex disbursement 12% cost of interest</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php if($hve==0){echo 0;}else{$cadis=rumPMT(12,1,$hve); echo number_format($cadis);} ?></td><td></td></tr>
                        <tr><td colspan="5" class="text-left" ><b>Amortization of capex / month
</b></td><td class="text-right"><?php //.. ?></td><td class="text-right"><?php if($hve==0){echo 0;}else{$cadis=rumPMT(12,1,$hve); echo number_format($cadis);} ?></td><td>&nbsp;Per month</td></tr>
                        <tr><td colspan="5" class="text-left" ><b>Maintenance expenses 10%/yr from capex </b> <?php //if(!empty($dafrid->nmc)){$pecax=explode('/',$dafrid->nmc); echo $pecax[1];}else{}  ?></td><td class="text-right"><?php //..?></td><td class="text-right"><?php if($hve==0){echo 0;}else{echo number_format(($hve*0.1)/12);} ?></td><td>&nbsp;Per month</td></tr>



                        </table>





                        <p><br><br></p>                                    
            </div>    

            <div class="col-md-1"></div>                            
</div> <!-- end 2nd row -->



<div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
        
        <table width="100%">
        <tr><td colspan="8">

        <?php if($this->session->userdata('id_jabatan')==6): ?>         
        <form class='add-item' method=POST action='<?php echo base_url('/panel/revenue/'.$id); ?>' onsubmit="FSubmit(this);">
 
        <div class='form-group row'>
                <label for='lblitem' class='col-sm-1 form-control-label'> Bulanan (Rp.)</label>
                <div class='col-sm-4'>
                    <input type='text' name='bulanan' class='form-control' placeholder="Ketik harga bulanan disini" required/>
                    
                </div>

                <label for='lblqty' class='col-sm-1 form-control-label'> Lama Kontrak</label>
                <div class='col-sm-2'>
                    <input type="hidden" name="lama" class="form-control" />
                                <select name="lama_ddl" onchange="DDChanged(this);" class="form-control" />

                                    
                                    <option value="1">1 Tahun</option>
                                    <option value="2">2 Tahun</option>
                                    <option value="3">3 Tahun</option>
                                    <option value="4">4 Tahun</option>
                                    <option value="5">5 Tahun</option>
                                                                        
                                    <option value="">Lainnya..</option>
                                </select> <input type="text" name="lama_txt" style="display: none;" class="form-control" placeholder="Ketik lama kontrak lainnya disini" />
                </div>
                <label for='lblhgven' class='col-sm-2 form-control-label'> Instalasi (Rp.)</label>
                <div class='col-sm-2'>
                    <input type='text' name='instalasi' class='form-control' placeholder="Ketik biaya disini" value="0" required/>
                </div>
        </div>  
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
            </div>
        </div>
                
        </form>
        <?php else : ?>
            <center> <h4>----- R E V E N U E -----</h4> </center>
        <?php endif; ?>

        </td></tr>
        </table>
        <p>Term of Minimum Contract : <?php if($nilrve>0){echo $dafrve->lama;}else{echo 0;}  ?> Tahun</p>
                   <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="a" data-sort-order="asc">
                                <thead>

								<tr>
                                    <th class="text-left">REVENUE</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Unit Price</th>
                                    <th class="text-center">Total Price (Monthly)</th>
                                    <th class="text-center">Total Price (1 Year)</th>
								</tr>
								</thead>
								<tbody>
									<tr>
										<td class="text-left">Revenue</td><td class="text-right"><?php if($nilrve>0){echo number_format(($dafrve->bulanan*$dafrve->lama)*12);}else{echo 0;} ?></td><td class="text-right"><?php if($nilrve>0){echo number_format($dafrve->bulanan);}else{echo 0;} ?></td><td class="text-right"><?php if($nilrve>0){echo number_format($dafrve->bulanan);}else{echo 0;} ?></td><td class="text-right"><?php if($nilrve>0){echo number_format(($dafrve->bulanan*$dafrve->lama)*12);}else{echo 0;} ?></td>
									</tr>
									<tr><td class="text-left">Instalasi</td><td class="text-right"><?php if($nilrve>0){echo number_format($dafrve->instalasi);}else{echo 0;} ?></td><td class="text-right"></td><td class="text-right"><?php if($nilrve>0){echo number_format($dafrve->instalasi);}else{echo 0;} ?></td><td class="text-right"><?php if($nilrve>0){echo number_format($dafrve->instalasi);}else{echo 0;} ?></td></tr>
									<tr><td class="text-left">TOTAL REVENUE</td><td class="text-right"></td><td class="text-right"></td><td class="text-right"><?php if($nilrve>0){echo number_format($dafrve->instalasi+$dafrve->bulanan);}else{echo 0;} ?></td><td class="text-right"><?php if($nilrve>0){echo number_format((($dafrve->bulanan*$dafrve->lama)*12)+$dafrve->instalasi);}else{echo 0;}  ?></td></tr>
								</tbody>
					</table>
   
                        <p><br><br></p>                                    
            </div>    

            <div class="col-md-1"></div>                            
</div> <!-- end 2nd row -->






<?php

$id_mse = $id;

//foreach ($dafrid as $d) :
    
 
?>

    <div class="modal fade" id="edithItemcausegroup<?= $id_mse; ?>" tabindex="-1" role="dialog" aria-labelledby="edithItemcausegroupLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="edithItemcausegroup<?= $id_mse; ?>Label">Tambah Biaya Manage Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('panel/isimser/'); ?>" method="post">
                    <div class="modal-body">
                        <input type="hidden" name='id_rha' value="<?= $id_mse; ?>">
                        <div class="form-group">
                            <label>Pelanggan : <?php echo $dafrid->nm_custo; ?></label>
                            <label>Alamat : <?php echo $dafrid->al_custo; ?></label>

                        </div>                        
                        <div class="form-group">
                            <label>Biaya Manage Service (Rp.) </label>
                            <input type="text" class="form-control" id="mse" name="mse" placeholder="Ketik biaya disini" value="<?php echo $dafrid->mse; ?>">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php //endforeach; ?>



<script>
function kembali() {
  window.history.back();
}
</script>
<script type="text/javascript">

function DDChanged(oDDL) {
    var oTextbox = oDDL.form.elements["lama_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}

function FSubmit(oForm) {
    var oHidden = oForm.elements["lama"];
    var oDDL = oForm.elements["lama_ddl"];
    var oTextbox = oForm.elements["lama_txt"];
    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;
}

</script>