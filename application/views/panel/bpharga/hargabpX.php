<?php
foreach($bptot as $to) {
     	$hid=$to->idrha;
     	$hjl=$to->totjlm;
     	$hve=$to->totven;
} 
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/hargatamb/"); ?>">Pra BP Barang</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Form Pra BP Barang</div>
                <div class="panel-body">
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal tambah harga! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Tambah harga berhasil disimpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

<p><?php //echo $hjl; echo "<br>".$hid; ?></p>
        <form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lblitem' class='col-sm-1 form-control-label'>Item</label>
                <div class='col-sm-2'>
                    <input type='text' name='item' class='form-control' placeholder="Ketik item disini" >
                    
                </div>

                <label for='lblqty' class='col-sm-1 form-control-label'>Qty </label>
                <div class='col-sm-1'>
                    <input type='text' name='qty' class='form-control' value="1">
                </div>
                <label for='lblhgven' class='col-sm-1 form-control-label'>Harga Vendor </label>
                <div class='col-sm-2'>
                    <input type='text' name='hgven' class='form-control' placeholder="Ketik harga disini">
                </div>
                <label for='lblhgven' class='col-sm-1 form-control-label'>Harga JLM </label>
                <div class='col-sm-2'>
                    <input type='text' name='hgjlm' class='form-control' placeholder="Ketik harga disini">
                </div>
        </div>  
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('focmen/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
        </div> <!-- end 1st row -->

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                    <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="a" data-sort-order="asc">
                                <thead>
                                
                                <tr>
                                    <th data-field="a"  data-sortable="true" class="text-center">#</th>
                                    <th data-field="b"  data-sortable="true" class="text-center">Item</th>
                                    <th data-field="c"  data-sortable="true" class="text-center">Qty</th>
                                    <th data-field="d"  data-sortable="true" class="text-center">H. Vendor</th>
                                    <th data-field="e"  data-sortable="true" class="text-center">H. JLM</th>
                                    <th data-field="f"  data-sortable="true" class="text-center">TH. Vendor</th>
                                    <th data-field="g"  data-sortable="true" class="text-center">TH. JLM</th>
                                    

                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($dafibp as $bp): ?>
                                    <tr>
                                        <td class="text-center"><?php echo $bp->id; ?></td>
                                        <td class="text-left"><?php echo $bp->item; ?></td>
                                        <td class="text-right"><?php echo number_format($bp->qty); ?></td>
                                        <td class="text-right"><?php echo number_format($bp->hgven); ?></td>
                                        <td class="text-right"><?php echo number_format($bp->hgjlm); ?></td>
                                        <td class="text-right"><?php echo number_format($bp->toven); ?></td>    
                                        <td class="text-right"><?php echo number_format($bp->tojlm); ?></td>


                                    </tr>
                                     
                                    <?php endforeach; ?>

                                </tbody>
                        </table>
                        <table border='0' width="100%"> 
                        	<tr><td>.</td><td>.</td><td>.</td><td>.</td><td>.</td><td class="text-left">Total Hrg VEN</td><td class="text-left">Total Hrg JLM</td></tr>
                        	<tr><td colspan="5" class="text-left" width="75%"><b>Total</b></td><td class="text-right"><?php echo number_format($hve); ?></td><td class="text-right"><?php echo number_format($hjl); ?></td></tr>
                        <tr><td colspan="5" class="text-left" width="75%"><b>Revenue Customer</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php echo number_format($hjl); ?></td></tr>
                        <tr><td colspan="5" class="text-left" width="75%"><b>PPN 10%</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php echo number_format($hjl*0.1); ?></td></tr>
                        <tr><td colspan="5" class="text-left" width="75%"><b>Total Revenue (Incl. PPN)</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php $trip=$hjl+($hjl*0.1); echo number_format($trip); ?></td></tr>
                        <tr><td colspan="5" class="text-left" width="75%"><b>Tax Corporate 25%</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php $tcor=($hjl-$hve)*0.25; echo number_format($tcor); ?></td></tr>
                        <tr><td colspan="5" class="text-left" width="75%"><b>NMC</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php if(!empty($dafrid->nmc)){$pecah=explode('|',$dafrid->nmc);}else{$pecah=null;} if(is_null($pecah)){$nmc=0;}else{$nmc=$pecah[0];} echo number_format($nmc); ?></td></tr>
                        <tr><td colspan="5" class="text-left" width="75%"><b>Manage Service</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php $mser=0; echo number_format($mser); ?></td></tr>
                        <tr><td colspan="5" class="text-left" width="75%"><b>Margin</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php $marg=$hjl-$hve-$tcor-$nmc-$mser; echo number_format($marg); ?></td></tr>
                        <tr><td colspan="5" class="text-left" width="75%"><b>Percentage</b></td><td class="text-right"><?php //echo number_format($hve); ?></td><td class="text-right"><?php echo number_format(($marg/$hjl)*100).'%'; ?></td></tr>
                        </table>
                                                            
            </div>    
            <div class="col-md-1"></div>                            
        </div> <!-- end 2nd row -->

