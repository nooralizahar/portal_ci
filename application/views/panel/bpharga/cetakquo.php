
<?php

?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/"); ?>">Cetak</a></li>
            <li class="active">Cetak Quotation</li>
        </ol>
    </div><!--/.row-->
    <br/>
    <div class="row">
    &nbsp;&nbsp;&nbsp;<button onclick="$.print('#lbrquo')" class="btn btn-success"><i class="fa fa-print fa-lg fa-fw fa-2x"></i><br><small><sub>PRINTER</sub><small></button>
    </div>
    <br/>
    <div class="col-lg-12" id="lbrquo">

        <h3>Kepada :<br></h3>
        <h3><?php echo $dafrid->nm_custo; ?></h3>
            <table border="1px" width="100%">
                <tbody>
                    <tr><td align="center" colspan="7">

                    </td></tr>
                   <hr/>
                   
                    <tr>
                        <td align="center" colspan="4"> <h4>LEMBAR QUOTATION</h4></td>
                    </tr>
                    <tr>
                        <td align="left" width="25%"><h5>&nbsp;X</h5></td><td align="center" width="2%">:</td><td align="left" colspan="2"><h5>&nbsp;<?php ?></h5></td>
                    </tr>
                    <tr>
                        <td align="left"> <h5>&nbsp;X</h5></td><td align="center">:</td><td align="left" colspan="2"><h5>&nbsp;<?php  //echo tgl_indopj($d->tgl_surat)." / ".$d->no_surat;?></h5></td>
                    </tr>
                    <tr>
                        <td align="left"> <h5>&nbsp;X</h5></td><td align="center"><h5>:</h5></td><td align="left" colspan="2"><h5>&nbsp;<?php ?></h5></td>
                    </tr>
                    <tr>
                        <td align="left"> <h5>&nbsp;X</h5></td><td align="center"><h5>:</h5></td><td align="left" colspan="2"><h5>&nbsp;<?php ?></h5></td>
                    </tr>
                    <tr>
                        <td align="left"> <h5>&nbsp;X</h54></td><td align="center"><h5>:</h5></td><td align="left" colspan="2"><h5>&nbsp;<?php  //echo tgl_indopj($d->tgl_diterima);?></h5></td>
                    </tr>
                    <tr>
                        <td align="left"> <h5>&nbsp;X</h5></td><td align="center"><h5>:</h5></td><td align="left" colspan="2"><h5>&nbsp;<?php  //echo tgl_indopj($d->tgselesai);?></h5></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" width="50%" colspan="4" height="20%"> 

                            <h5>&nbsp;X</h5>
                  <table ><!-- class="table responsive"-->
                  <tr> <td><div style="font-size: 16px">
                                

                               
                            </td></tr> 
                  </table>
                        </td>

                        

                    </tr>
                    <tr>
                        <td align="left" colspan="4"> <h5> &nbsp;X<br> &nbsp;X : ..........................................<br> &nbsp;X : ..........................................</h5></td>
                    </tr>
               

    
                </tbody>
            </table>
            <!-- <b>*Coret yang tidak perlu</b> -->

            <br><br>
    </div>
</div>

</div>	<!--/.main-->

<script>
    $(document).ready(function(){
        $(".attach-img").magnificPopup({
            type: "image"
        });

        setTimeout(function() {
            $('[data-toggle="tooltip').tooltip();
        },1000);

        $("#attach").fileinput({'showUpload':false});

        var followUpContainer = $("#followUp");
        var followUpContainerHeight = followUpContainer[0].scrollHeight;
        followUpContainer.scrollTop(followUpContainerHeight);

        $(".attach").on("click",function(ev){
            var data_file = $(this).data().file;
            var content = "<ol>";

            $.each(data_file,function(key,item){
                var cls = item.file.split(".").length;
                var tipe_gambar = ["jpg","png","jpeg","bmp","gif"];
                cls = ($.inArray(item.file.split(".")[cls-1],tipe_gambar) == -1) ? "attach-doc" : "attach-img";
                content += "" +
                    "<li><a class='"+cls+"' href='"+BASE_URL+"assets/uploads/lampiran/follow_up_disposisi/"+item.file+"'>"+item.judul+"</a></li>";
            });

            content += "</ol>";

            bootbox.alert({
                size: "normal",
                title: "Lampiran follow-up",
                message: content
            });


            $(".attach-img").magnificPopup({
                type: "image"
            });


            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                $("#myModal").css("z-index",2000);
                return false;
            });


            ev.preventDefault();
        });

        $("#isi_disposisi").shorten({
            moreText: "Selengkapnya",
            lessText: "Kecilkan",
            showChars: 300
        });

        $(function(){
            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                return false;
            });
        });

        $(".star.fa-star").on("click",function(){
            var curStar = $(this);
            update_star(curStar);
            curStar.removeClass("fa-star").addClass("fa-spinner fa-pulse");
        });
    });

    function update_star(curStar) {
        $.ajax({
            url: BASE_URL + "/ajax/update_star_disposisi/",
            method: "POST",
            data: {
                id_relasi_disposisi: curStar.data().id,
                starred: (curStar.data().starred == 1) ? 0 : 1
            },
            success: function(response){
                if(curStar.data().starred == 0) {
                    curStar.data("starred",1);
                    curStar.addClass("star-active");
                    curStar.parent().children("span").html(1)
                } else {
                    curStar.data("starred",0);
                    curStar.removeClass("star-active");
                    curStar.parent().children('span').html(0);
                }
                curStar.removeClass('fa-spinner fa-pulse');
                curStar.addClass("fa-star");
            }
        });
    }
</script>