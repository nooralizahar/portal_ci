
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/dashit/"); ?>"> Akses IT</a></li>
            <li class="active"> Database Netflix</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="netflix">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Generate Key Netflix Gagal.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Generate Key Netflix Berhasil.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h5> Generate User Netflix</h5>
                </div>
        <div class="panel-body">


<div class="row">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $this->db->where("pulled",1)->get("radcheck")->num_rows(); ?></div>
                        <div class="text-muted">FTTH</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-red panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                    <svg class="glyph stroked database"><use xlink:href="#stroked-database"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><?php echo $this->db->get("t_xilfrsu")->num_rows(); ?></div>
                        <div class="text-muted">Generated</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-blue panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $y=($this->db->where("pulled",1)->get("radcheck")->num_rows())-($this->db->get("t_xilfrsu")->num_rows()); echo $y;?></div>
                        <div class="text-muted">Ungenerated</div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <br>



        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tgreg"  data-sortable="true">Registrasi</th>
                        <th data-field="usrnm"  data-sortable="true">Username</th>
                        <th data-field="nmlkp"  data-sortable="true">Nama Lengkap</th>
                        <th data-field="usrnf"  data-sortable="true">Username Netflix</th> 
                        <th data-field="pwdnf"  data-sortable="true">Sandi Netflix</th>            
                        <th>Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($d_nflix as $dat): ?>
                        <tr>
                            <td><?php echo $dat->registerdate; ?></td>
                            <td><?php echo $dat->username; ?></td>
                            <td><?php echo $dat->name; ?></td>
                            <td><?php echo $dat->usrnetflix; ?></td>
                            <td><?php echo $dat->pwdnetflix; ?></td>
                            <td><?php
                            $nam=preg_replace(array('/\s/','/\./'), '', $dat->name);
            if ($this->session->userdata("level")=='admin'){
                echo"&nbsp;<small><a href=/panel/netflix/$dat->username/$nam data-toggle='tooltip' title='Generate Netflix'><i class='fa fa-gear fa-2x'></i></a></small>&nbsp;";

            }else{
                echo"&nbsp;<small><a href='#'><i class='fa fa-sitemap fa-2x'></i></a</small>&nbsp;";
            }
                
                echo"</center>";?>

                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>

        </div>
      </div>
    </div>

    </div><!--/.row end-->






    

    <form action="" method="post" style="display: none;">
        <input type="hidden" name="password" id="confirmPassword">
        <input type="submit" name="selesaiBtnSubmit" id="selesaiBtnSubmit">
    </form>

</div>	<!--/.main-->

