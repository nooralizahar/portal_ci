﻿<?php $level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	

    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/"); ?>">Dashboard <?=$this->nadiv;?></a></li>

        </ol>
    </div><!--/.row-->

    <hr/>
 <?php if($nmjab=="FOC"): ?>   
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> LAPORAN FOC</div>
                <div class="panel-body">                

                  <div class="row text-center pad-top"> <!-- awal dari halaman dalam  --> 
              				

                    <?php $this->load->view("/panel/dashoprlfoc")?>

              				  
                  </div>
			  
                </div> <!--/.panel body -->
            </div> <!--/.panel default -->
          </div> <!--/.col -->
      </div> <!--/.row -->

 <?php elseif($nmjab=="FTTH" || $nmjab=="Asset Management GM" ): ?>   
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> DATA <?=strtoupper($this->nadiv);?></div>
                <div class="panel-body">                

                  <div class="row text-center pad-top"> <!-- awal dari halaman dalam  --> 
                      

                    <?php $this->load->view("/panel/dashoprftth")?>

                        
                  </div>
        
                </div> <!--/.panel body -->
            </div> <!--/.panel default -->
          </div> <!--/.col -->
      </div> <!--/.row -->


 <?php else: ?>
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> DASHBOARD Lainnya</div>
                <div class="panel-body">                

                  <div class="row text-center pad-top"> <!-- awal dari halaman dalam  --> 
                      



                        
                  </div>
        
                </div> <!--/.panel body -->
            </div> <!--/.panel default -->
          </div> <!--/.col -->
      </div> <!--/.row -->
 <?php endif; ?>


</div>

 
