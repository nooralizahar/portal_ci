<div class="form-group"> 
                     <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="15"><center><h3>TAHUN <?= empty($tgl) ? date('Y') : $tgl; ?></h3></center></th></tr>
                            <tr>

                                <th>&nbsp;No.</th>
                                <th>&nbsp;NAMA</th>
                                <th><center>JAN</center></th>
                                <th><center>FEB</center></th>
                                <th><center>MAR</center></th>
                                <th><center>APR</center></th>
                                <th><center>MEI</center></th>
                                <th><center>JUN</center></th>
                                <th><center>JUL</center></th>
                                <th><center>AGU</center></th>
                                <th><center>SEP</center></th>
                                <th><center>OKT</center></th>
                                <th><center>NOP</center></th>
                                <th><center>DES</center></th>
                                <th><center>TOTAL</center></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php 
                            $daf=$summarypre;
                            $no=1;
                            foreach($daf as $h): ?>
                                <tr >
                                    <td align="right" width="5%"><?php echo $no++; ?> </td>
                                    <td width="20%"><?php echo $h->NAMA; ?> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/01"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : Januari ".date('Y'); ?>'><?php echo $h->JAN; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/02"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : Februari ".date('Y'); ?>'><?php echo $h->FEB; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/03"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : Maret ".date('Y'); ?>'><?php echo $h->MAR; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/04"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : April".date('Y'); ?>'><?php echo $h->APR; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/05"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : Mei ".date('Y'); ?>'><?php echo $h->MEI; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/06"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : Juni ".date('Y'); ?>'><?php echo $h->JUN; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/07"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : Juli ".date('Y'); ?>'><?php echo $h->JUL; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/08"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : Agustus ".date('Y'); ?>'><?php echo $h->AGU; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/09"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : September ".date('Y'); ?>'><?php echo $h->SEP; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/10"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : Oktober ".date('Y'); ?>'><?php echo $h->OKT; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/11"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : Nopember ".date('Y'); ?>'><?php echo $h->NOP; ?></a> </td>
                                    <td align="center" width="5%"><a href="<?php echo base_url("panel/rantal/".$h->NAMA."/12"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->NAMA.",&#013; Detil bulan : Desember ".date('Y'); ?>'><?php echo $h->DES; ?></a> </td>
                                    <td align="center"><?php echo $h->TOTAL; ?> </td>

                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>


                     <table class="table table-bordered">
                            <tbody>
                            <?php 
                            $daf=$totalpre;
                            $no=1;
                            foreach($daf as $h): ?>
                                <tr >
                                    <td align="right" width="5%"><?php echo ""; ?> </td>
                                    <td align="right" width="20%"><?php echo "TOTAL ".$h->thn; ?> </td>
                                    <td align="center" width="5%"><?php echo $h->JAN; ?></td>
                                    <td align="center" width="5%"><?php echo $h->FEB; ?></td>
                                    <td align="center" width="5%"><?php echo $h->MAR; ?></td>
                                    <td align="center" width="5%"><?php echo $h->APR; ?></td>
                                    <td align="center" width="5%"><?php echo $h->MEI; ?></td>
                                    <td align="center" width="5%"><?php echo $h->JUN; ?></td>
                                    <td align="center" width="5%"><?php echo $h->JUL; ?></td>
                                    <td align="center" width="5%"><?php echo $h->AGU; ?></td>
                                    <td align="center" width="5%"><?php echo $h->SEP; ?></td>
                                    <td align="center" width="5%"><?php echo $h->OKT; ?></td>
                                    <td align="center" width="5%"><?php echo $h->NOP; ?></td>
                                    <td align="center" width="5%"><?php echo $h->DES; ?></td>
                                    <td align="center"><?php echo $h->TOTAL; ?> </td>

                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>

 
</div>