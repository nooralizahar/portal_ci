﻿<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	

    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/"); ?>">Dashboard</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>
    
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">ENGINEERING DASHBOARD</div>
                <div class="panel-body">    

                  <div class="row">
                      <div class="col-lg-12">

                          <div class="tabbable-panel">
                              <div class="tabbable-line">
                                  <ul class="nav nav-tabs ">
  
                                      <li class="active">
                                          <a href="#tabsatu" data-toggle="tab">
                                          Data</a>
                                      </li>
                                      <li>
                                          <a href="#tabdua" data-toggle="tab">
                                          Rincian </a>
                                      </li>
                                  </ul>
                                  
                                  <div class="tab-content">
  
                                      <div class="tab-pane active" id="tabsatu">
                                        <?php $this->load->view("/panel/infoeng")?>

                                      </div>

                                      <div class="tab-pane" id="tabdua">
                                        <?php $this->load->view("/panel/inforin")?>

                                      </div>

                                  </div>
                              </div>
                          </div>

                      </div> <!--/.col-->
                  </div><!--/.row-->           		

  
			  
                </div> <!--/.panel body -->
            </div> <!--/.panel default -->
          </div> <!--/.col -->
      </div> <!--/.row -->
</div>

 
