<?php
    $thnini= date('Y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    if(substr($thnbli, 2)==01){$bln=12;}else{$bln='0'.substr($thnbli, 2)-1;}
    $thnblu= ((substr($thnbli,0, 2))-1).$bln;
    $blnini=date('Y/m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 

        //$tik=$this->db->order_by("RowID","desc")->limit(1)->get("t_datame")->result();
        //foreach($tik as $d): 
        //$lastik=$d->Created_Time;   
        //endforeach;
?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("khusus/"); ?>"> Khusus</a></li>
            <li class="active"> Data Vendor</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Presales & Sales Admin</h5>
                </div>
        <div class="panel-body">
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["hsuc"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["herr"])):?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menghapus data. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["tsuc"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil diterminate. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>  
                <?php elseif(isset($_GET["terr"])):?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal terminate data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>              
                <?php endif; ?>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#daftar" aria-controls="daftar" role="tab" data-toggle="tab">Daftar</a></li>

                          <?php if($this->jabku==35 || $this->jabku==36 ):?>
                            <li role="presentation"><a href="#rekap" aria-controls="rekap" role="tab" data-toggle="tab">Rekap</a></li>
                          <?php endif; ?>
                          
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="daftar">
                                <div class="panel-body">
                                    <div class="panel panel-primary">
                            <div class="panel-body">
            <?php if($this->jabku==35 || $this->jabku==36 ):?>
            <a href="<?php echo base_url("khusus/vdtamb"); ?>" class="btn btn-success"><i class="fa fa-plus-circle fa-lg"></i> <br><small><sup>BARU</sup></small></a>
             
            <a href="<?php echo base_url("khusus/venexp"); ?>" class="btn btn-success"><i class="fa fa-file-excel-o fa-lg"></i> <br><small><sup>EXCEL</sup></small></a>
            <?php endif; ?>
<br/><br/>

<div class="row">

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_rodnev")->num_rows()); ?></a></div>
                        <div class="text-muted">Total Vendor</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnlal"); ?>"><?php echo number_format($this->db->where('stat','Active')->get("t_nanaggnal")->num_rows()); ?></a></div>
                        <div class="text-muted">Active</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php echo number_format($this->db->where('stat','Inactive')->get("t_nanaggnal")->num_rows()); ?> </a></div>
                        <div class="text-muted">Inactive</div>
                    </div>
                </div>
            </div>
        </div>

</div> 


    <br>


<!-- <div class="row">-->
        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <?php if($this->jabku==35 || $this->jabku==36 ):?>
                        <th data-field="l"  data-sortable="true">Pilihan</th>
                        <?php endif; ?>
                        <th data-field="a"  data-sortable="true">Vendor</th>
                        <th data-field="b"  data-sortable="true" class="text-center">Status</th>
                        <!-- <th data-field="c"  data-sortable="true">No. PO</th>-->
                        <th data-field="d"  data-sortable="true" class="text-center">CID/SID</th> 
                        <th data-field="e"  data-sortable="true" class="text-center">Tgl. Aktif</th>            
                        <th data-field="f"  data-sortable="true" class="text-center">Kontrak<br> (bln)</th>
                        <th data-field="g"  data-sortable="true" class="text-center">Akhir  <br>Kontrak<br>(1 thn)</th>
                        <th data-field="h"  data-sortable="true" class="text-center">Durasi <br>Kontrak<br>(bln)</th>
                        <th data-field="i"  data-sortable="true" class="text-center">Kota</th>
                        <th data-field="j"  data-sortable="true" class="text-center">Site</th>
                        <th data-field="k"  data-sortable="true" class="text-center">Alamat<br>Instalasi</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($vdsem as $dat): ?>
                        <tr>
                            <?php if($this->jabku==35 || $this->jabku==36 ):?>
                            <td class="text-center">
                                <a href="<?php echo base_url("khusus/ubah_data/".$dat->idtr); ?>" data-toggle='tooltip' title='Ubah Data'><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;<a href="<?php echo base_url("khusus/hapus_data/".$dat->idtr); ?>" data-toggle='tooltip' title='Hapus Data'><i class="fa fa-trash"></i></a>&nbsp;&nbsp;<a href="<?php echo base_url("khusus/terminated_data/".$dat->idtr); ?>" data-toggle='tooltip' title='Terminated'><i class="fa fa-minus-square-o"></i></a>
                            </td>
                             <?php endif; ?>
                            <td><?php echo $dat->vnama; ?></td>
                            <td><?php echo $dat->stat; ?></td>
                            <!-- <td><?php echo $dat->nopo; ?></td>  -->
                            <td><?php echo $dat->csid; ?></td>
                            <td><?php echo date("d-M-y ", strtotime($dat->tgac)); ?></td>
                            <td><?php echo $dat->lcon; ?></td>
                            <td><?php echo date("d-M-y ", strtotime($dat->acon)); ?></td>
                            <td><?php echo $dat->dcon; ?></td>
                            <td><?php echo $dat->kota; ?></td>
                            <td><?php echo $dat->site; ?></td>
                            <td class="text-left"><?php echo $dat->alam; ?></td>
                            
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>
<!-- </div> -->


                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php if($this->jabku==35 || $this->jabku==36 ):?>
                            <div role="tabpanel" class="tab-pane" id="rekap">
                                <div class="panel-body">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            
                                        <?php $this->load->view("/panel/vdunem/vdrekap")?>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
</div>          





        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

