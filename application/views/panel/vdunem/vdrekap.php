
        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>

                        <th data-field="a"  data-sortable="true">Vendor</th>
                        <th data-field="b"  data-sortable="true">Status</th>
                        <th data-field="e"  data-sortable="true">Kota</th>            
                        <th data-field="f"  data-sortable="true">Site</th>
                      <?php if($this->jabku==35 || $this->jabku==36 ):?>
                        <th data-field="g"  data-sortable="true">Bulanan (Rp.)</th>   
                      <?php endif; ?>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($vdrek as $dat): ?>
                        <tr>

                            <td><?php echo $dat->vnama; ?></td>
                            <td><?php echo $dat->stat; ?></td>
                            <td><?php echo $dat->kota; ?></td>
                            <td class="text-right"><a href="<?php echo base_url("khusus/site_data/".$dat->vnam."/".$dat->stat."/".$dat->kota); ?>"><?php echo $dat->site; ?></a></td>
                       <?php if($this->jabku==35 || $this->jabku==36 ):?>
                            <td class="text-right"><?php echo number_format($dat->bulanan); ?></td>
                       <?php endif; ?>        
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>
