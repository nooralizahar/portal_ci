
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("khusus/"); ?>"> Khusus</a></li>
            <li class="active"> Isi Data Berlangganan</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Ubah Data</div>
                <div class="panel-body">
                    <?php foreach ($gdat as $g) { ?>
                        
                    
                    <form action="" method="POST" enctype="multipart/form-data">
                    	<div class="form-group">
                            <div class="col-md-12">
                            
                            <div class="alert alert-warning text-center"> DATA BERLANGGANAN VENDOR </div>
                            <input type="hidden" name="idtr" class="form-control" value="<?=$g->idtr?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                            <label>Order dari</label>
                            <select name="ordb" class="form-control orderby">
                                <option value="JLM" <?php echo ($g->ordb == "JLM") ? "selected" : ""; ?>>JLM</option>

                                <option value="OMG" <?php echo ($g->ordb == "OMG") ? "selected" : ""; ?>>OMG</option>
                            </select>
                            </div>
                            <div class="col-md-8">
                            <label for="">Nama Vendor </label>
                            <select name="vnam" class="form-control vendor">

                                <?php foreach($dven as $d):?>
                                   <!-- <option value="<?php echo $d->vid; ?>"> <?php echo $d->vnama;?></option>-->
                                    <option value="<?php echo $d->vid; ?>" <?php echo ($g->vnam == $d->vid) ? "selected" : ""; ?>><?php echo $d->vnama;?></option>
                                <?php endforeach;?>
                            </select>
                            </div>
                            <div class="col-md-2">
                            <label><i class="fa fa-plus-square-o"></i></label>
                                <a href="http://intranet.jlm.net.id:8080/vendors" target="_blank" class="btn btn-success form-control" > Baru / Edit Vendor</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                            <label>Status</label>
                            <select name="stat" class="form-control status">
                               <!-- <option value="Active"> Active</option>
                                <option value="InActive"> Inactive</option> -->
                                <option value="Active" <?php echo ($g->stat == "Active") ? "selected" : ""; ?>>Active</option>

                                <option value="InActive" <?php echo ($g->stat == "InActive") ? "selected" : ""; ?>>InActive</option>
                            </select>
                            </div>
                            <div class="col-md-3">
                            <label for="">No. PO</label>
                            <input type="text" name="nopo" class="form-control" value="<?=$g->nopo?>">
                            </div>
                            <div class="col-md-3">
                            <label for="">Tgl. PO</label>
                            <input type="date" name="tgpo" class="form-control" value="<?=$g->tgpo?>">
                            </div>
                            <div class="col-md-3">
                            <label for="">Tgl. Aktif</label>
                            <input type="date" name="tgac" class="form-control" value="<?=$g->tgac?>">
                            </div>


                        </div>
  <!--                      <div class="form-group">
                            <label for="">Tanggal</label>
                            
                        </div> -->
                        <div class="form-group">
                            <div class="col-md-4">
                            <label for="">Kontrak (Bln)</label>
                            <input type="text" name="lcon" class="form-control" value="<?=$g->lcon?>">
                            </div>
                            <div class="col-md-4">
                            <label for="">Akhir Kontrak (1 Thn)</label>
                            <input type="date" name="acon" class="form-control" value="<?=$g->acon?>">
                            </div>
                            <div class="col-md-4">
                            <label for="">Durasi Kontrak (Bln)</label>
                            <input type="text" name="dcon" class="form-control" value="<?=$g->dcon?>">
                            </div>
                            
                        </div>
                        <div class="form-group">

                            <div class="col-md-3">
                            <label for="">CID/SID</label>
                            <input type="text" name="csid" class="form-control" value="<?=$g->csid?>">
                            </div>

                            <div class="col-md-3">
                            <label for="">Kota</label>
                            <input type="text" name="kota" class="form-control" value="<?=$g->kota?>">
                            </div>

                            <div class="col-md-3">
                            <label for="">Site</label>
                            <input type="text" name="site" class="form-control" value="<?=$g->site?>">
                            </div>

                            <div class="col-md-3">
                            <label for="">Segment</label>
                            <select name="segm" class="form-control segment">
                                <?php foreach($dseg as $d):?>
                                    <!-- <option value="<?php echo $d->id_seg; ?>"> <?php echo $d->ds_seg;?></option>-->
                                    <option value="<?php echo $d->id_seg; ?>" <?php echo ($g->segm == $d->id_seg) ? "selected" : ""; ?>><?php echo $d->ds_seg;?></option>
                                <?php endforeach;?>
                            </select>
                            </div>                            

                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <label for="" class="text-center">Alamat Pemasangan</label>
                            <input type="text" name="alam" class="form-control" value="<?=$g->alam?>"><br>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            
                            <div class="alert alert-info text-center"> V E N D O R </div>
                            
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-4">
                            <label for="">Instalation Charge</label>
                            <input type="text" name="vich" class="form-control" value="<?=$g->vich?>">
                            </div>
                            <div class="col-md-4">
                            <label for="">Monthly Charge</label>
                            <input type="text" name="vmch" class="form-control" value="<?=$g->vmch?>">
                            </div>
                            <div class="col-md-2">
                            <label for="">Bandwidth (Mbps)</label>
                            <input type="text" name="vbwi" class="form-control" value="<?=$g->vbwi?>">
                            </div>
                            <div class="col-md-2">
                            <label for="">Tgl. Aktivasi</label>
                            <input type="date" name="vtga" class="form-control" value="<?=$g->vtga?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                            
                            <div class="alert alert-success text-center"> C U S T O M E R </div>
                            
                            </div>
                        </div>
                        <div class="form-group">
                            
                            <div class="col-md-4">
                            <label for="">Instalation Charge</label>
                            <input type="text" name="cich" class="form-control" value="<?=$g->cich?>">
                            </div>
                            <div class="col-md-4">
                            <label for="">Monthly Charge</label>
                            <input type="text" name="cmch" class="form-control" value="<?=$g->cmch?>">
                            </div>
                            <div class="col-md-2">
                            <label for="">Bandwidth</label>
                            <input type="text" name="cbwi" class="form-control" value="<?=$g->cbwi?>">
                            </div>
                            <div class="col-md-2">
                            <label for="">Tgl. Aktivasi</label>
                            <input type="date" name="ctga" class="form-control" value="<?=$g->ctga?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                            <label for="">Keterangan</label>
                            <input type="text" name="kete" class="form-control" value="<?=$g->kete?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                        <center>
                        <button type="submit" name="btnSimpan" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
                        <!-- <input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                           </div>
                        </div>
                    </form>

                    <?php }?>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script>


    $(document).ready(function(){

        $(".segment").select2();
        $(".vendor").select2();
        //$(".orderby").select2();
        //$(".status").select2();

    })
</script>