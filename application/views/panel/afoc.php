
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/"); ?>"> BOD Dashboard</a></li>
            <li class="active"> Aktifitas FOC</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 style="color:#010288"><b>Daftar Aktifitas FOC <?php if(strlen($pil)==4){ echo "Tahun ".$pil;}else{echo "Bulan ".date("M-Y",strtotime($pil));} ?></b></h4>
                </div>
        <div class="panel-body">

   <br>

        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tgak"  data-sortable="true">Tanggal</th>
                        <th data-field="wakt"  data-sortable="true">Waktu</th>
                        <th data-field="nama"  data-sortable="true">N a m a</th>
                        <th data-field="prob"  data-sortable="true">Problem</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($d_afoc as $dpro): ?>
                        <tr>
                            <td><?php echo $dpro->date; ?></td>
                            <td><?php echo $dpro->start." - ".$dpro->end; ?></td>
                            <td><?php echo $dpro->name; ?></td>
                            <td><?php echo $dpro->case; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>

        </div>
      </div>
    </div>

    </div><!--/.row end-->





