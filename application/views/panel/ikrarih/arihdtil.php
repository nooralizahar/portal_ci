﻿<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/jabatan"); ?>"> Tata Kelola Jabatan </a></li>
            <li class="active"> Detil <?php echo $nmid->nm_jab; ?></li>
        </ol>
    </div><!--/.row-->

    <hr/>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">


 <div class="panel-heading"><i class="fa fa-sitemap fa-lg"></i> DETIL <?php echo strtoupper($nmid->nm_jab); ?></div>
           <div class="panel-body">

                <table class="table table-striped" id="tarminga" data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                       
                        <th data-field="nama" data-sortable="true">Nama </th>
                        <th data-field="atasan" data-sortable="true">Atasan</th>
                        <th data-field="segment" data-sortable="true">Segment</th>
                        
                        
<!--                     <th>Pilihan</th>-->
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dathir as $dh):?>
                           
                        
                        <tr >

                            <td><?php echo $dh->nama; ?></td>
                            <td><?php echo $dh->atasan; ?></td>
                            <td><?php ///echo $b->case; ?></td>

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                   </table>
                  </div>
                </div>
            </div>
        </div> 

</div>


 
