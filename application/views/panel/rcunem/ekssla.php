


    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Export
                        </a>
                    </h4>
                </div>
                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="panel-collapse collapse<?php //echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#requester" aria-controls="requester" role="tab" data-toggle="tab">Requester</a></li>
                            <li role="presentation"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Problem Side</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="requester">
                                <div class="panel-body">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <form action="<?php echo base_url("export/");?>" method="POST">
                                                <div class="col-lg-4"> 
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon pixelfix"> Requester</span>
                                                            <select class="form-control text-field" required name="nama">
                                                                <option class="text-field" value="">Pilih Requester!!</option>
                                                                <?php foreach ($requester as $r) { ?>
                                                                    <option class="text-field" value="<?php echo $r->Requester; ?>"><?php echo $r->Requester; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4"> 
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon pixelfix"> Tanggal Awal</span>
                                                            <input data-date-format="dd-mm-yyyy" type="text" required id="tgl_awal" name="tgl_awal" class="form-control text-field">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4"> 
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon pixelfix"> Tanggal Akhir</span>
                                                            <input data-date-format="dd-mm-yyyy" type="text" required id="tgl_akhir" name="tgl_akhir" class="form-control text-field">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> 
                                                    <button value="pdf" type="submit" name="pdf" class="btn btn-info">Export PDF</button>
                                                    <button value="excel" type="submit" name="excel" class="btn btn-info">Export Excel</button>
                                                </div> 
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="problem">
                                <div class="panel-body">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            <form action="<?php echo base_url(); ?>index.php/export/problem/" method="POST">
                                                <div class="col-lg-4"> 
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon pixelfix"> Problem Side</span>
                                                            <select class="form-control text-field" required name="p_nama">
                                                                <option class="text-field" value="">Pilih Problem Side!!</option>
                                                                <?php foreach ($problem as $row) { ?>
                                                                    <option class="text-field" value="<?php echo $row['Problem_Side']; ?>"><?php echo $row['Problem_Side']; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4"> 
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon pixelfix"> Tanggal Awal</span>
                                                            <input data-date-format="dd-mm-yyyy" type="text" required id="tgl_awal" name="p_tgl_awal" class="form-control text-field">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4"> 
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon pixelfix"> Tanggal Akhir</span>
                                                            <input data-date-format="dd-mm-yyyy" type="text" required id="tgl_akhir" name="p_tgl_akhir" class="form-control text-field">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> 
                                                    <button value="pdf" type="submit" name="p_pdf" class="btn btn-info">Export PDF</button>
                                                    <button value="excel" type="submit" name="p_excel" class="btn btn-info">Export Excel</button>
                                                </div> 
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


