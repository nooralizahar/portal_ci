
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
        <li class="active">SME & HRB</li>
    </ol>
</div><!--/.row--><br />

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Data SME & HRB</div>
            <div class="panel-body">
                
                <table data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a">No. Akun</th>
                        <th data-field="b">Nama Capel</th>
                        <th data-field="c">RFS</th>
                        <th data-field="d">Aktifasi</th>
                        <th data-field="e">Status</th>
                        <th data-field="f">Pesanan</th>
                        <th data-field="g">Posisi Terakhir</th>                        
                        <th data-field="h">Jenis Produk</th> 
                        <th class="text-center">Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>
                   <?php foreach($datsme as $d): ?>
                        <tr>
                            <td><?= $d->account_no;?></td>
                            <td><?= $d->name;?></td>
                            <td><?= $d->rfs_date; ?></td>
                            <td><?= $d->activation_date; ?></td>
                            <td><?= $d->status_desc; ?></td>
                            <td><?= $d->order_type_desc; ?></td>
                            <td><?= $d->holder_desc; ?></td>                            
                            <td><?= $d->product_type_desc." - ".$d->product_name; ?></td>
                   
                            <td class="text-center">
                                <a href="<?php //echo base_url("panel/disxcarim/" . $d->id_disx . "/" . $d->ko_disx); ?>" ><i class="fa fa-minus-circle fa-lg"></i></a>
<!--                                <a href="#" class="btn btn-warning">Hapus</a>-->
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

<?php //var_dump($datsme); ?>

            </div>
        </div>
    </div>
</div><!--/.row-->

</div><!--/.main-->
