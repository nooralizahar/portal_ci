
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url( $this->uriku."/"); ?>"> Dashboard</a></li>
            <li class="active"> Daftar Asset</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["suc"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading"> Form Isian Asset</div>
                <div class="panel-body">

    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">


        <div class="panel"> <!-- awal panel -->

                    <div class="tabbable-panel">
                        <div class="tabbable-line">

                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#tabnol" aria-controls="tabnol" role="tab" data-toggle="tab"> Baru</a></li>
                            <li role="presentation" class="active"><a href="#tabatu" aria-controls="tabatu" role="tab" data-toggle="tab"> Asset</a></li>
                        </ul>



<div class="tab-content">

  <div role="tabpanel" class="tab-pane" id="tabnol">
     <div class="panel-body">
         <div class="panel panel-primary">
           <div class="panel-body">

            <?php $this->load->view("panel/dasset/rabssa")?>

           </div>
         </div>
     </div>
  
  </div>


<div role="tabpanel" class="tab-pane active" id="tabatu">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">

                 <?php $this->load->view("panel/dasset/fadssa")?>

                </div>
            </div>
        </div>
</div>
    

    </div>
  </div>
 </div>
</div> <!-- akhir panel -->



            </div>
        </div>
    </div>  


                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->


