
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/pengguna"); ?>">Dashboard Operation</a></li>
            <li class="active">Laporan FOC</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading"></div>
                <div class="panel-body">

                    <center>
                        <table class="table table-striped">
                            <thead>
                            <tr> <th colspan="3"><center><h3>Uraian Tugas</h3></center></th></tr>
                            </thead>
                            <tbody>
                            <?php 
                            $daf=$hirdat;
                            //$no=1;
                            foreach($daf as $b): ?>
                                <tr >
                                <td>Pelanggan </td><td align="center">:</td><td><?php $lapid=$b->id;echo $b->customer; ?> </td>
                                </tr>
                                <tr><td>Tugas </td><td align="center">:</td><td><?php echo $b->jobs; ?></td></tr>
                                <tr><td>Kasus </td><td align="center">:</td><td><?php echo $b->case; ?></td></tr>
                                <tr><td>Kendala </td><td align="center">:</td><td><?php echo $b->issues; ?></td></tr>
                                <tr><td>Tindakan </td><td align="center">:</td><td><?php echo $b->action; ?></td></tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>
                       
                    <h2><div ><?php  $hrini=date('d-M-Y'); echo "$hrini"; ?></div><div id="jamDigi"></div></h2>
                    <form action="" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?php echo $lapid;?>">
                        <center><div id="lokasifoc"></div></center><br>
                        <div id="btnMulai">
                        <a onclick="getLocation()" class="btn btn-success"><i class="fa fa-play-circle fa-lg"></i><br><small><sup>MULAI</sup></small></a>
                        </div>
                        <div id="btnSelesai" style="display:none">
                        <button type="submit" name="btnSelesai" class="btn btn-success" ><i class="fa fa-stop fa-lg"></i><br><small><sup>SELESAI</sup></small></button>
                        </div>
                    </form>
                    </center>

                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main   getLocation() -->

        <script type="text/javascript">
            var x = document.getElementById("lokasifoc");

                function getLocation() {
                    if (navigator.geolocation) {
                        navigator.geolocation.watchPosition(showPosition);
                        
                    } else { 
                        x.innerHTML = "Browser tidak mendukung Geo Lokasi.";}
                        jamOn();
                    }
                    
                function showPosition(position) {
                    x.innerHTML="<center><a href='https://www.google.com/maps/@"+ position.coords.latitude +","+ position.coords.longitude+",19z' target='_blank'><small><sub>LOKASI SEKARANG</sub></small></a></br><input type='text' class='form-control text-center' name='latlng' value='" + position.coords.latitude +"," + position.coords.longitude+"' readonly/><br></center>";
                }

                function jamOn() {
                    var today = new Date();
                    var h = today.getHours();
                    var m = today.getMinutes();
                    var s = today.getSeconds();
                        m = tamNol(m);
                        s = tamNol(s);

                    document.getElementById('jamDigi').innerHTML =
                    h + ":" + m + ":" + s;

                    var t = setTimeout(jamOn, 500);



                    var bMulai=document.getElementById('btnMulai');
                    var bSlsai=document.getElementById('btnSelesai');
                        bMulai.style.display="none";
                        bSlsai.style.display="inline"; 
                                   

                }

                function tamNol(i) {
                    if (i < 10) {i = "0" + i};  // tambah nol utk angka < 10
                    return i;
                }

                


                
        </script>


