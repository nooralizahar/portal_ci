
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("focmen/odpin/"); ?>"> Menu FOC</a></li>
            <li class="active"> Edit ODP</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h5> Edit ODP</h5>
                </div>
        <div class="panel-body">
        <form class='add-item' method=POST action='focmen/odped/'>
             <?php foreach($dafodp as $b):?>
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Nama / Kode ODP</label>
                <div class='col-sm-6'>
                    <input type='text' name='nama' class='form-control' id='nama' value="<?php echo $b->nama; ?>">
                    <input type="hidden" name="id" class="form-control" value="<?php echo $b->idodp; ?>" style="color:blue" readonly/>
                </div>
                <label for='lblstatus' class='col-sm-1 form-control-label'>Status</label>

                <div class='col-sm-3'>
                    <input type="radio" name="status" value="0" <?php if($b->status==0){echo "checked='checked'";}?>>
                            <label for="InActive" style="text-decoration: none;">&nbsp;&nbsp;&nbsp;InActive&nbsp;&nbsp;&nbsp;</label>
                            <input type="radio" name="status" value="1" <?php if($b->status==1){echo "checked='checked'";}?>> <label for="Active" style="text-decoration: none;">&nbsp;&nbsp;&nbsp;Active&nbsp;&nbsp;&nbsp;</label>
                    
                </div>
          </div>  
         <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Lokasi </label>
                <div class='col-sm-5'>
                    <select name="lokasi" id="lokasi" class="form-control odp">
                            	<option value=""></option>
                                <?php foreach($daflok as $l):?>
                                    <option value="<?php echo $l->t_location_id; ?>" <?php echo ($b->lokasi == $l->t_location_id) ? "selected" : ""; ?>><?php echo $l->area_name." - ".$l->name; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
          </div> 
         <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Alamat </label>
                <div class='col-sm-10'>
                    <input type='text' name='alamat' class='form-control' id='alamat'value="<?php echo $b->alamat; ?>">
                </div>
          </div> 
        <div class='form-group row'>
                <label for='lblkordinat' class='col-sm-2 form-control-label'>Kordinat</label>
                <div class='col-sm-10'>
                      <input type='text' name='kordinat' class='form-control' id='kordinat' value="<?php echo $b->kordinat; ?>">
                </div>
                
        </div>    
        <div class='form-group row'>
                <label for='lblkapasitas' class='col-sm-2 form-control-label'>Kapasitas</label>
                <div class='col-sm-3'>
                       <input type='text' name='kapasitas' class='form-control' id='kapasitas' value="<?php echo $b->kapasitas ?>">

                </div>
                <div class='col-sm-2'>
               <!-- <?php //if($b->ver==1){ echo "<b>verified</b>";}else{echo "<input type='hidden' name='ver' class='form-control' value=1 ><input type='checkbox' name='ver' class='form-control' value=0 > <b>verifikasi</b>";} ?>                                  <option value="0">Unverified</option>
                                <option value="1">Verified</option>-->
                <select name='ver' id='ver' class="form-control">
                                 <option value="<?php echo $b->ver; ?>" <?php echo ($b->ver == 0) ? "selected" : ""; ?>> <?php echo ($b->ver==0) ? "Unverified":"Verified"; ?></option>
                                 <?php if($b->ver==0) : ?>
                                 <option value="1">Verified</option>
                                 <?php else : ?>
                                 
                                 <option value="0">Unverified</option>
                                 <?php endif; ?>
                </select>                 
                 

                </div>
               

        </div>

        <div class='form-group row'>
                <label for='lblkapasitas' class='col-sm-2 form-control-label'>Relasi OLT</label>
                <div class='col-sm-8'>
                    <select name="oltrel" id="oltrel" class="form-control">
                                <option value=""></option>
                                <?php foreach($dafolt as $a):?>
                                    <option value="<?php echo $a->idolt; ?>" <?php echo ($b->oltrel == $a->idolt) ? "selected" : ""; ?>>

                                        <?php echo $a->nmolt."&nbsp;&rarr; slot : ".$a->noslot.", port : ".$a->noport; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
                <div class='col-sm-2'>
                    <a href="<?php echo base_url("focmen/oltin/"); ?>" class="btn btn-success form-control" ><i class="fa fa-plus-square-o"></i> OLT</a>
                </div>
        </div>


            
        <?php endforeach; ?>          
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('focmen/odpin/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>




        </div>
      </div>
    </div>

    </div><!--/.row end-->



</div>	<!--/.main-->

