<?php
foreach($datpel as $d){
  $nodp=$d->pelanggan;
  $aodp=$d->alamat;
  $kodp=$d->kordinat;
}
echo "<div class='col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main'>";
echo "</div>";//$kor
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">

  <title>ODP Bnetfit <?php echo $nodp; ?></title>
  <link rel="shortcut icon" href="images/favicon.png">
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAArK11nKE_IVBCHbXbM52illBzDCAqeQs&callback=initMap"></script>

<script>
function initialize() {
  var propertiPeta = {
    center:new google.maps.LatLng(<?php echo $kodp; ?>),
    zoom:19,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  };
  
  var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);
  
  // membuat Marker
  var marker=new google.maps.Marker({
      position: new google.maps.LatLng(<?php echo $kodp; ?>),
      map: peta,
      icon: "../../assets/images/bnetfit_pl.png"

      //animation: google.maps.Animation.BOUNCE
  });

}

// event jendela di-load  
google.maps.event.addDomListener(window, 'load', initialize);
</script>
  
</head>
<body>
  <?php echo $nodp." (".$kodp.")"; ?><br><?php echo $aodp; ?>
  <div id="googleMap" style="width:100%;height:90%;"></div>
  
</body>
</html>
