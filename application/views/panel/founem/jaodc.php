
<?php
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">

  <title>Kode <?php echo $param; ?></title>
  <link rel="shortcut icon" href="images/favicon.png">
    <style>
      #mapCanvas {
        width: 100%;
        height: 640px;
      }
    </style>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAArK11nKE_IVBCHbXbM52illBzDCAqeQs&callback=initMap"></script>
<script>
function initMap() {
  <?php $p=explode(',',$pusat->kordinat);        
  $sat='{ lat: '.$p[0].', lng: '.$p[1].'}'; ?>    
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        center: <?=$sat?>,
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
    map.setTilt(50);
        
    // Multiple markers location, latitude, and longitude
    var markers = [
        <?php //if($datpel->num_rows > 0){
            foreach ($datodc as $k) {
                $kor=explode(',',$k->kordinat);        
                echo '["'.$k->nama.'", '.$kor[0].', '.$kor[1].'],';
            //}
        }
        ?>
    ];

    var staODP = [
        <?php //if($datpel->num_rows > 0){
            //foreach ($datodc as $s) {    
             //   echo $s->status.',';
            //}
        //}
        ?>
    ];
                        
    // Info window content
    var infoWindowContent = [
        <?php if($datodc != 0){
            foreach($datodc as $l){ ?>
                ['<div class="info_content">' +
                '<h3><?php echo $l->nama." <sup> Kap. : ".$l->kapasitas." port</sup>"; ?></h3>' +
                '<p><?php echo $l->alamat; ?></p>' + '</div>'],
        <?php 

            }
        }
        ?>
    ];
        
    // Add multiple markers to map
    
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Place each marker on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        if(staODP[i] == 0){
            pinma='../../assets/images/bnetfit_fu.png';
        }else{

            pinma='../../assets/images/bnetfit_ms.png';

        }


        marker = new google.maps.Marker({
            position: position,
            map: map,
            //label:''+sisaPort[i],
            icon: pinma,
            title: markers[i][0]
        });
        
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });
    
}

// Load initialize function
google.maps.event.addDomListener(window, 'load', initMap);
</script>

</head>
  <body>
        <?php //if($datpel->num_rows > 0){
            echo "<center><h3 style='color:blue'>Hasil Pencarian ODC ".$pilih." : ".$param."</h3></center>";
            foreach ($datodc as $k) {
             $kor=explode(',',$k->kordinat);        
                echo '["'.$k->nama.'", '.$kor[0].', '.$kor[1].'],';
            }
        //}

        //echo "Pusat :".$pusat->kordinat;
        ?>
    <div id="mapCanvas"></div>
  </body>
</html>