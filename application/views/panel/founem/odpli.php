<?php 
       
        $dpel=$this->dbz->where("t_net_odp_id",$id)->get("v_net_odp_isi_detil")->num_rows();
        $dkap=$this->dbz->where("t_net_odp_id",$id)->get("t_net_odp")->result();
        foreach($dkap as $d): 
        $kap=$d->capacity;  
        $nodp=$d->code;
        $kodp=$d->coordinate; 
        endforeach;
        $sta=$kap-$dpel;

function codexworldGetDistanceOpt($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
{
    $rad = M_PI / 180;
    //Calculate distance from latitude and longitude
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin($latitudeFrom * $rad) 
        * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
        * cos($latitudeTo * $rad) * cos($theta * $rad);

    return acos($dist) / $rad * 60 *  1.853;
}

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("focmen/odpin/"); ?>"> Daftar ODP</a></li>
            <li class="active"> Detil Pelanggan</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Hapus Data berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h4 class="text-primary"> Detil ODP No. <?php echo $id." (".$nodp." / kap. ".$kap." port)";?> </h4>
                </div>
        <div class="panel-body">

<?php if ($sta=== 0): ?>
    <div class="form-control row">
    	<h3 class="text-success text-center"><a href="<?php echo base_url("focmen/lopel/".$id); ?>" target='_blank'><i class='fa fa-map-marker fa-lg'></i>&nbsp;<?php echo $nodp; ?></a><br></h3>
        <h2 class="text-danger text-center">Port Sudah Penuh<br></h2>
    </div>
    <p><br><button type='button' onclick="window.history.back();" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button><br></p>
<?php elseif ($sta< 0): ?>
    <div class="form-control row">
        <h3 class="text-success text-center"><a href="<?php echo base_url("focmen/lopel/".$id); ?>" target='_blank'><i class='fa fa-map-marker fa-lg'></i>&nbsp;<?php echo $nodp; ?></a><br></h3>
        <h3 class="text-danger text-center">Pelanggan <?= $dpel?> site, melebihi kapasitas ODP<br></h3>
    </div>
    <p><br><button type='button' onclick="window.history.back();" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button><br></p>
<?php else: ?>
    <div class="form-control row">
        <h3 class="text-success text-center"><a href="<?php echo base_url("focmen/lopel/".$id); ?>" target='_blank'><i class='fa fa-map-marker fa-lg'></i>&nbsp;<?php echo $nodp; ?></a><br></h3>
        <h3 class="text-success text-center"><?php echo "Tersisa : ".$sta." port"; ?><br></h3>
    </div>
        <p><br><button type='button' onclick="window.history.back();" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button><br></p>

<?php endif; ?>



            <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" data-page-size="25">
                    <thead>
                    <tr>
                        <th data-field="n"  data-sortable="true">No. Urut</th>
                        <th data-field="i"  data-sortable="true">IKR</th>
                        <th data-field="a"  data-sortable="true">Pelanggan</th>
                        <th data-field="b"  data-sortable="true">Alamat</th>
                        <th data-field="c"  data-sortable="true">Koordinat</th>
                        <th data-field="e"  data-sortable="true">Jarak Gmap(m)</th>
                        <th data-field="f"  data-sortable="true">Tgl. Aktif</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $fkor=explode(",",$kodp);
                        $no=1;

                        foreach($dafpel as $d):
                            $tkor=explode(",",$d->tikor_pelanggan);
                        ?>  
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $d->nama_ikr; ?></td>
                            <td><?php echo $d->nama_pelanggan; ?></td>
                            <td><?php echo $d->alamat_pelanggan; ?></td>

                            <td><?php echo $d->tikor_pelanggan; ?></td>
                            
                            <td><?php if($d->tikor_pelanggan==null or $d->tikor_pelanggan==0 ){ echo "tikor kosong";}else{$j=codexworldGetDistanceOpt($fkor[0],$fkor[1],$tkor[0],$tkor[1]); echo number_format($j*1000,2);} ?></td>
                           <td><center><?php echo tgl_indome($d->tgl_aktif); ?>

                            </center></td>

                        </tr>
                        <?php $no++; endforeach; ?>
                                  
 
                    </tbody>
            </table>



        </div>
      </div>
    </div>

    </div><!--/.row end-->






</div>	<!--/.main-->

