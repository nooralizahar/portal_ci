<?php 
        $dkap=$this->db->where("idodp",$id)->get("t_odp")->result();
        $dpel=$this->db->where("idodp",$id)->get("t_odp_rel")->num_rows();
        foreach($dkap as $d): 
        $kap=$d->kapasitas;  
        $nodp=$d->nama;
        $kodp=$d->kordinat; 
        endforeach;
        $sta=$kap-$dpel;

function codexworldGetDistanceOpt($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
{
    $rad = M_PI / 180;
    //Calculate distance from latitude and longitude
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin($latitudeFrom * $rad) 
        * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
        * cos($latitudeTo * $rad) * cos($theta * $rad);

    return acos($dist) / $rad * 60 *  1.853;
}

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("focmen/odpin/"); ?>"> Menu ODP</a></li>
            <li class="active"> Daftar ODP</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Hapus Data berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h4 class="text-primary"> Daftar Pelanggan ODP No. <?php echo $id;?> </h4>
                </div>
        <div class="panel-body">

<?php if ($sta=== 0): ?>
    <div class="form-control row">
    	<h3 class="text-success text-center"><a href="<?php echo base_url("focmen/lopel/".$id); ?>" target='_blank'><i class='fa fa-map-marker fa-lg'></i>&nbsp;<?php echo $nodp; ?></a><br></h3>
        <h2 class="text-danger text-center">Port Sudah Penuh<br></h2>
    </div>
    <p><br><button type='button' onclick="window.history.back();" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button><br></p>
<?php else: ?>
		<h3 class="text-success text-center"><a href="<?php echo base_url("focmen/lopel/".$id); ?>" target='_blank'><i class='fa fa-map-marker fa-lg'></i>&nbsp;<?php echo $nodp; ?></a><br></h3>
        <h3 class="text-success text-center"><?php echo "Tersisa : ".$sta." port"; ?><br></h3>

                <form class='add-item' method=POST action='focmen/odpli/'>
             
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-1 form-control-label'>User ID</label>
                <div class='col-sm-5'>
                    <input type='text' name='userid' class='form-control' id='userid' required/>
                    <input type="hidden" name="idodp" class="form-control"  value="<?php echo $id; ?>" style="color:blue" readonly/>
                </div>
                <label for='lblalamat' class='col-sm-1 form-control-label'>Nama </label>
                <div class='col-sm-5'>
                    <input type='text' name='pelanggan' class='form-control' id='pelanggan' required/>
                </div>

        </div> 
        <div class='form-group row'>
                <label for='lblalamat' class='col-sm-1 form-control-label'>Alamat</label>
                <div class='col-sm-5'>
                      <input type='text' name='alamat' class='form-control' id='alamat' required/>
                </div>
                <label for='lblalamat' class='col-sm-1 form-control-label'>No. HP/Telp</label>
                <div class='col-sm-5'>
                      <input type='text' name='hp' class='form-control' id='hp' required/>
                </div>
        </div> 
        <div class='form-group row'>
                <label for='lblikr' class='col-sm-1 form-control-label'>IKR</label>
                <div class='col-sm-5'>
                      <input type='text' name='ikr' class='form-control' id='ikr' required/>
                </div>
                <label for='lbltarikan' class='col-sm-1 form-control-label'>Tarikan</label>
                <div class='col-sm-5'>
                      <input type='text' name='tarikan' class='form-control' id='tarikan' required/>
                </div>

        </div> 
        <div class='form-group row'>
                <label for='lblaktifasi' class='col-sm-1 form-control-label'>Tgl. Aktifasi</label>
                <div class='col-sm-5'>
                      <input type='date' name='aktifasi' class='form-control' id='aktifasi' required/>
                </div>
                <label for='lblponsn' class='col-sm-1 form-control-label'>S/N Perangkat</label>
                <div class='col-sm-5'>
                      <input type='text' name='ponsn' class='form-control' id='ponsn' required/>
                </div>
                
        </div> 

        <div class='form-group row'>

                <label for='lblport' class='col-sm-1 form-control-label'>No. Port</label>
                <div class='col-sm-5'>
                      <input type='text' name='noport' class='form-control' id='noport' required/>
                </div>
                <label for='lblkordinat' class='col-sm-1 form-control-label'>Koordinat</label>
                <div class='col-sm-5'>
                      <input type='text' name='kordinat' class='form-control' id='kordinat' >
                </div>
        </div> 

        <div class='form-group row'>

                
        </div>    
         
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                      <button type='button' onclick="window.history.back();" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>


<?php endif; ?>



            <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="i"  data-sortable="true">No. Port</th>
                        <th data-field="a"  data-sortable="true">Pelanggan</th>
                        <th data-field="b"  data-sortable="true">Alamat</th>
                        <th data-field="c"  data-sortable="true">Koordinat</th>
                        <!--<th data-field="d"  data-sortable="true">Tarikan</th>-->
                        <th data-field="e"  data-sortable="true">Hit. Jarak (m)</th>
                        <th data-field="f"  data-sortable="true">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $fkor=explode(",",$kodp);

                        foreach($dafpel as $d):
                            $tkor=explode(",",$d->kordinat);
                        ?>  
                        <tr>
                            <td><?php echo $d->noport; ?></td>
                            <td><?php echo $d->pelanggan; ?></td>
                            <td><?php echo $d->alamat; ?></td>

                            <td><a href="<?php echo base_url("focmen/petap/".$d->idodpr); ?>" target='_blank' data-toggle='tooltip' title='<?php echo "IKR : ".$d->ikr.",&#013; Tarikan : ".$d->tarikan; ?>'><?php echo $d->kordinat; ?></a></td>
                            <!-- <td><?php echo $d->tarikan; ?></td>-->
                            <td><?php if($d->kordinat==null or $d->kordinat==0 ){ echo "tikor kosong";}else{$j=codexworldGetDistanceOpt($fkor[0],$fkor[1],$tkor[0],$tkor[1]); echo number_format($j*1000,2);} ?></td>
                           <td><center>
                               <!-- <a href="<?php echo base_url("focmen/peled/".$d->idodpr); ?>" data-toggle='tooltip' title='Edit' ><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp; 
                                <a href="<?php echo base_url("focmen/pelha/".$d->idodpr); ?>" data-toggle='tooltip' title='Hapus' ><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp; 
                                <a href="<?php //echo base_url("focmen/odpli/".$dat->idodp); ?>" ><i class="fa fa-plus fa-lg"></i></a>  -->
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                                  
 
                    </tbody>
            </table>



        </div>
      </div>
    </div>

    </div><!--/.row end-->






</div>	<!--/.main-->

