<?php
    $thnini= date('Y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    if(substr($thnbli, 2)==01){$bln=12;}else{$bln='0'.substr($thnbli, 2)-1;}
    $thnblu= ((substr($thnbli,0, 2))-1).$bln;
    $blnini=date('Y/m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 

        $tik=$this->db->order_by("idolt","desc")->limit(1)->get("t_olt")->result();
        foreach($tik as $d): 
        $lastik=$d->tgbua;   
        endforeach;
?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("focmen/"); ?>"> FOC</a></li>
            <li class="active"> Data OLT</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Masukan Data OLT</h5>
                </div>
        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#requester" aria-controls="requester" role="tab" data-toggle="tab"> OLT Baru</a></li>
                            <!--<li role="presentation" ><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Daftar OLT</a></li>-->
                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="requester">
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">
        <form class='add-item' method=POST action=''>
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>N a m a  O L T</label>
                <div class='col-sm-10'>
                    <input type='text' name='nmolt' class='form-control' id='nmolt' required>
                </div>
        </div>  
        <div class='form-group row'>
                <label for='lblip' class='col-sm-2 form-control-label'>IP Address</label>
                <div class='col-sm-4'>
                    <input type='text' name='ipolt' class='form-control' id='ipolt' required>
                </div>
                <label for='lblipport' class='col-sm-2 form-control-label'>IP port</label>
                <div class='col-sm-4'>
                      <input type='text' name='ipport' class='form-control' id='ipport' required>
                </div>
        </div> 
        <div class='form-group row'>
                <label for='lblslot' class='col-sm-2 form-control-label'>Slot OLT</label>
                <div class='col-sm-4'>
                      <input type='text' name='noslot' class='form-control' id='noslot' required>
                </div>
                <label for='lblport' class='col-sm-2 form-control-label'>Port OLT</label>
                <div class='col-sm-4'>
                      <input type='text' name='noport' class='form-control' id='noport' required>
                </div>
                
        </div>    
   
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('focmen/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>

<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"><?php 
            //echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-4">  </div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_olt")->num_rows()); ?></a></div>
                        <div class="text-muted"> Jumlah OLT</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php //$this->db->select_sum('kapasitas'); $hasil= $this->db->get("t_odp")->row(); $y=$hasil->kapasitas; echo $y; ?></div>
                        <div class="text-muted"> ODP <></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php //echo number_format($this->db->get("t_odp_rel")->num_rows()); ?></div>
                        <div class="text-muted">  ODP >< </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar blank"><use xlink:href="#stroked-calendar-blank"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php //$u=$this->db->get("t_odp_rel")->num_rows(); $t=$y-$u; echo $t;?> </a></div>
                        <div class="text-muted"></div>
                    </div>
                </div>
            </div>
        </div>


</div> 

    <br>



        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="i"  data-sortable="true">ID</th>
                        <th data-field="a"  data-sortable="true">Nama OLT</th>
                        <th data-field="b"  data-sortable="true">IP Address</th>
                        <th data-field="c"  data-sortable="true">IP Port</th>
                        <th data-field="d"  data-sortable="true">Slot OLT</th>
                        <th data-field="e"  data-sortable="true">Port OLT</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($oltsem as $dat): ?>
                        <tr>
                            <td><?php echo $dat->idolt; ?></td>
                            <td><?php echo $dat->nmolt; ?></td>
                            <td><?php echo $dat->ipolt; ?></td>
                            <td><?php echo $dat->ipport; ?></td>
                            <td><?php echo $dat->noslot; ?></td>
                            <td><?php echo $dat->noport; ?></td>

                            <!--<td><center>
                                <a href="<?php //echo base_url("focmen/odped/".$dat->idodp); ?>" data-toggle='tooltip' title='Ubah'><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp; 
                                <a href="<?php //echo base_url("focmen/odpli/".$dat->idodp); ?>" data-toggle='tooltip' title='Pelanggan'><i class="fa fa-plus fa-lg"></i></a>&nbsp;&nbsp; 
                                <?php// if($dat->fileqr==null): ?>
                                <a href="<?php //echo base_url("focmen/odpqr/".$dat->idodp); ?>" data-toggle='tooltip' title='Generate'><i class="fa fa-qrcode fa-lg"></i></a>
                                <?php// else :?>
                                <a href="<?php //echo base_url("focmen/odpce/".$dat->idodp); ?>" data-toggle='tooltip' title='Cetak' ><i class="fa fa-print fa-lg"></i></a>
                                <?php //endif;?>
                            </center></td>-->

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>

             </div>
         </div>
     </div>
</div>


<div role="tabpanel" class="tab-pane" id="problem">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">


<!-- Pane Kedua Isi disini -->


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

