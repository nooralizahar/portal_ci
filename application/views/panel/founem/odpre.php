<?php 
       


?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li> Rekapitulasi</li>
            
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Hapus Data berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h4 class="text-primary"> Detil Rekapitulasi</h4>
                </div>
        <div class="panel-body">

<div class="row">
<div style="padding: 30px 30px;" class="container col-xs-12">

  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">REKAPITULASI INSTALASI ODP</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in"><!-- collapse in -->
        <div class="panel-body panel-primary">
            <!--<h3>Data Belum Tersedia</h3>-->
            <div class="panel-body" style="max-width: 98%;overflow-x: scroll;">
                <?php $this->load->view("/panel/founem/odpsu")?>
            </div>

        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title" >
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">REKAP ODP BERDASARKAN LOKASI TAHUN INI</a>
        </h4>
      </div>
      <div id="collapse9" class="panel-collapse collapse">
        <div class="panel-body">
            <!--<center><h2><u>RANGKUMAN LAINNYA</u></h2></center>-->
            <div class="panel-body" style="max-width: 98%;overflow-x: scroll;">
             <?php $this->load->view("/panel/founem/odpsu-lok")?>
            </div>

           
        </div>
      </div>
    </div>


   </div>
</div>
</div>





        </div>
      </div>
    </div>

    </div><!--/.row end-->






</div>	<!--/.main-->

