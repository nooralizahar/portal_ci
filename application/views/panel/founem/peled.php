
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("focmen/odpin/"); ?>"> Menu FOC</a></li>
            <li class="active"> Edit Pelanggan</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


                <div class="panel-heading">
                    <h5> Set ODP Pelanggan</h5>
                </div>
        <div class="panel-body">
        <form class='add-item' method=POST action='focmen/peled/'>
             <?php foreach($dafpel as $b):?>
                <div class='form-group row'>
                <label for='lblnama' class='col-sm-1 form-control-label'>User ID</label>
                <div class='col-sm-5'>
                    <input type='text' name='userid' class='form-control' id='userid' value="<?php echo $b->userid; ?>" readonly>
                    <input type="hidden" name="id" class="form-control"  value="<?php echo $b->idodpr; ?>" style="color:blue" readonly/>

                </div>
                <label for='lblikr' class='col-sm-2 form-control-label'>I K R </label>
                <div class='col-sm-4'>
                    <?php echo " : ".$b->ikr; ?>

                </div> 

        </div>  

        <div class='form-group row'>
                <label for='lblnama' class='col-sm-1 form-control-label'>N a m a </label>
                <div class='col-sm-5'>
                    <input type='text' name='pelanggan' class='form-control' id='pelanggan' value="<?php echo $b->pelanggan; ?>" readonly>
                </div>

                <label for='lbltarikan' class='col-sm-2 form-control-label'>Tarikan </label>
                <div class='col-sm-4'>
                    <?php echo " : ".$b->tarikan." meter"; ?>
   
                </div> 

        </div> 
        <div class='form-group row'>
                <label for='lblalamat' class='col-sm-1 form-control-label'>Alamat</label>
                <div class='col-sm-5'>
                      <input type='text' name='alamat' class='form-control' id='alamat' value="<?php echo $b->alamat; ?>" readonly>
                </div>
                <label for='lblaktifasi' class='col-sm-2 form-control-label'>Aktifasi </label>
                <div class='col-sm-4'>
                    <?php echo " : ".$b->aktifasi; ?>
   
                </div> 
                
        </div>
        <div class='form-group row'>
                <label for='lblkordinat' class='col-sm-1 form-control-label'>Koordinat</label>
                <div class='col-sm-5'>
                      <input type='text' name='kordinat' class='form-control' id='kordinat' value="<?php echo $b->kordinat; ?>" >
                </div>
                <label for='lbldibuat' class='col-sm-2 form-control-label'>Di buat oleh</label>
                <div class='col-sm-4'>
                    <?php echo " : ".$b->us_cre.", ".date('d-M-y H:i:s', strtotime($b->tg_cre)); ?>
   
                </div> 
                
        </div>  
        <div class='form-group row'>
                <label for='lblodp' class='col-sm-1 form-control-label'>ODP</label>
                <div class='col-sm-5'>
                    <select name="idodp" id="idodp" class="form-control odp">
                            
                                <?php foreach($lodp as $a):?>
                                    <option value="<?php echo $a->idodp; ?>" <?php echo ($b->idodp == $a->idodp) ? "selected" : ""; ?>><?php echo $a->nama; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
                <label for='lbldibuat' class='col-sm-2 form-control-label'>Di update oleh</label>
                <div class='col-sm-4'>
                    <?php echo " : ".$b->us_upd.", ".date('d-M-y H:i:s', strtotime($b->tg_upd)); ?>
   
                </div> 
        </div> 

        <div class='form-group row'>

                <label for='lblport' class='col-sm-1 form-control-label'>No. Port</label>
                <div class='col-sm-5'>
                      <input type='text' name='noport' class='form-control' id='noport' value="<?php echo $b->noport; ?>">
                </div>
        </div>

            
        <?php endforeach; ?>          
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                     <button type='button' onclick="window.history.back();" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                      <!--<button type='button' onclick="window.location.href = '<?php //echo base_url('focmen/odpcs/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>-->
                    </div>
                </div>
                
        </form>




        </div>
      </div>
    </div>

    </div><!--/.row end-->






    

    <form action="" method="post" style="display: none;">
        <input type="hidden" name="password" id="confirmPassword">
        <input type="submit" name="selesaiBtnSubmit" id="selesaiBtnSubmit">
    </form>

</div>	<!--/.main-->
<script>
function Kembali() {
  window.history.back();
}
</script>
<script>


    $(document).ready(function(){


        $(".odp").select2();


    })
</script>