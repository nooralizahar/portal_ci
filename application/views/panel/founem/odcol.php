<?php 
        $dkap=$this->db->where("idodc",$id)->get("t_odc")->result();
        $dpel=$this->db->where("idodc",$id)->get("t_odp")->num_rows();
        $dolt=$this->db->where("idodc",$id)->get("t_olt")->num_rows();
        foreach($dkap as $d): 
        $kap=$d->kapasitas;  
        $nodp=$d->nama;
        $kodp=$d->kordinat; 
        endforeach;
        $sta=$kap-$dolt;

    function codexworldGetDistanceOpt($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo){
    $rad = M_PI / 180;
    //Calculate distance from latitude and longitude
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin($latitudeFrom * $rad) 
        * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
        * cos($latitudeTo * $rad) * cos($theta * $rad);

    return acos($dist) / $rad * 60 *  1.853;
    }

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("focmen/odcin/"); ?>"> Menu ODC</a></li>
            <li class="active"> Data OLT</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php elseif(isset($_GET["hsuc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Hapus Data berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php elseif(isset($_GET["herr"])):?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Hapus Data berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h4 class="text-primary"> Detil OLT in ODC No. <?php echo $id;?> </h4>
                </div>
        <div class="panel-body">

<?php if ($sta=== 0): ?>
    <div class="form-control row">
    	<h3 class="text-success text-center"><a href="<?php echo base_url("focmen/lopel/".$id); ?>" target='_blank'><i class='fa fa-map-marker fa-lg'></i>&nbsp;<?php echo $nodp; ?></a><br></h3>
        <h2 class="text-danger text-center">Port Sudah Penuh<br></h2>
    </div>
    <p><br><button type='button' onclick="window.history.back();" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button><br></p>
<?php else: ?>
		<h3 class="text-success text-center"><a href="<?php echo base_url("focmen/loodc/".$id); ?>" target='_blank'><i class='fa fa-map-marker fa-lg'></i>&nbsp;<?php echo $nodp; ?></a><br></h3>
        <h3 class="text-success text-center"><?php echo "Tersisa : ".$sta." port"; ?><br></h3>

                <form class='add-item' method=POST action='focmen/odcol/'>
             



        <div class='form-group row'>

                <label for='lblport' class='col-sm-1 form-control-label'>No. Port</label>
                <div class='col-sm-1'>
                      <input type='hidden' name='idodc' class='form-control' id='idodc' value="<?php echo $id;?>" readonly/>
                      <input type='text' name='portodc' class='form-control' id='portodc' required/>
                </div>
                <label for='lblnama' class='col-sm-2 form-control-label'>Nama OLT </label>
                <div class='col-sm-5'>
                    <select name="nmolt" id="nmolt" class="form-control oltpil">
                                <option value=""></option>
                                <?php foreach($dafolt as $o):?>
                                    <option value="<?php echo $o->olt_detil.'|'.$o->olt_ip; ?>"><?php echo $o->olt_detil." - ".$o->olt_ip; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
        </div> 

        <div class='form-group row'>

                
        </div>    
         
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                      <button type='button' onclick="window.history.back();" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>


<?php endif; ?>



            <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="i"  data-sortable="true">No. Port ODC</th>
                        <th data-field="a"  data-sortable="true">Nama OLT</th>
                        <th data-field="b"  data-sortable="true">IP Address</th>
                        <th data-field="c"  data-sortable="true">Slot/Port</th>
                        <th data-field="f"  data-sortable="true">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        //$fkor=explode(",",$kodp);

                        foreach($detolt as $d):
                            //$tkor=explode(",",$d->kordinat);
                        ?>  
                        <tr>
                            <td><?php echo $d->portodc; ?></td>
                            <td><?php echo $d->nmolt; ?></td>
                            <td><?php echo $d->ipolt; ?></td>

                            <td><?php $x=explode('-',$d->nmolt); echo str_replace('s','',str_replace('_p','/', $x[1])); ?></td>

                           <td><center>
                                <!--<a href="<?php echo base_url("focmen/olted/".$d->idodp); ?>" data-toggle='tooltip' title='Edit' ><i class="fa fa-pencil fa-lg"></i></a>-->&nbsp;&nbsp; 
                                <a href="<?php echo base_url("focmen/oltha/".$id."/".$d->idolt); ?>" data-toggle='tooltip' title='Hapus' ><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp; 
                               <!-- <a href="<?php //echo base_url("focmen/odpli/".$dat->idodp); ?>" ><i class="fa fa-plus fa-lg"></i></a>  -->
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                                  
 
                    </tbody>
            </table>



        </div>
      </div>
    </div>

    </div><!--/.row end-->






</div>	<!--/.main-->

<script>


    $(document).ready(function(){


        $(".oltpil").select2();


    })
</script>