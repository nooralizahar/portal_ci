<?php 
        $dkap=$this->db->where("idodc",$id)->get("t_odc")->result();
        $dpel=($this->db->where("idodc",$id)->get("t_odc_odp")->num_rows())-1;
        $dolt=$this->db->where("idodc",$id)->get("t_olt")->num_rows();
        $cekp=$this->db->where("idodc",$id)->order_by('odcport','desc')->limit(1)->get("t_odc_odp")->row();

        if($cekp){
            $port=$cekp->odcport+1;
        }else{
            $port=1;
        }

        foreach($dkap as $d): 
        $kap=$d->kapasitas;  
        $nodp=$d->nama;
        $kodp=$d->kordinat; 
        endforeach;
        $sta=$kap-($dpel+$dolt);

function codexworldGetDistanceOpt($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
{
    $rad = M_PI / 180;
    //Calculate distance from latitude and longitude
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin($latitudeFrom * $rad) 
        * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
        * cos($latitudeTo * $rad) * cos($theta * $rad);

    return acos($dist) / $rad * 60 *  1.853;
}

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("focmen/odpin/");?>"> Menu ODC</a></li>
            <li class="active"> Daftar ODC</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Hapus Data berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h4 class="text-primary"> Detil ODC No. <?php echo $id;?> </h4>
                </div>
        <div class="panel-body">

<?php if ($sta=== 0): ?>
    <div class="form-control row">
    	<h3 class="text-success text-center"><a href="<?php echo base_url("focmen/lopel/".$id); ?>" target='_blank'><i class='fa fa-map-marker fa-lg'></i>&nbsp;<?php echo $nodp; ?></a><br></h3>
        <h2 class="text-danger text-center">Port Sudah Penuh<br></h2>
    </div>
    <p><br><button type='button' onclick="window.history.back();" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button><br></p>
<?php else: ?>
		<h3 class="text-success text-center"><a href="<?php echo base_url("focmen/loodc/".$id); ?>" target='_blank'><i class='fa fa-map-marker fa-lg'></i>&nbsp;<?php echo $nodp; ?></a><br></h3>
        <h3 class="text-success text-center"><?php echo "Tersisa : ".$sta." port"; ?><br></h3>

                <form class='add-item' method=POST action='focmen/odcli/'>
             



        <div class='form-group row'>

                <label for='lblport' class='col-sm-1 form-control-label'>No. Port</label>
                <div class='col-sm-1'>
                      <input type='hidden' name='idodc' class='form-control' id='odcport' value="<?php echo $id;?>" readonly/>
                      <input type='text' name='odcport' class='form-control' id='odcport' value="<?php echo $port;?>" required/>
                </div>
                <label for='lblnama' class='col-sm-2 form-control-label'>Nama / Kode ODP </label>
                <div class='col-sm-5'>
                    <select name="idodp" id="idodp" class="form-control odcpil">
                                <option value=""></option>
                                <?php foreach($semodp as $s):?>
                                    <option value="<?php echo $s->idodp."+".$s->nama; ?>"><?php echo $s->nama." - ".substr($s->alamat,0,35); ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
        </div> 

        <div class='form-group row'>

                
        </div>    
         
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                      <button type='button' onclick="window.history.back();" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('focmen/odpdlo') ?>';" class='btn btn-primary'><i class='fa fa-download left fa-lg'></i><br> <sup><small>SYNC ODP</small></sup></button>
                    </div>
                </div>
                
        </form>


<?php endif; ?>



            <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="i"  data-sortable="true">No. Port</th>
                        <th data-field="a"  data-sortable="true">Nama ODP</th>
                        <th data-field="b"  data-sortable="true">Alamat</th>
                        <th data-field="c"  data-sortable="true">Koordinat</th>
                        <!--<th data-field="d"  data-sortable="true">Tarikan</th>-->
                        <th data-field="e"  data-sortable="true">Hit. Jarak (m)</th>
                        <th data-field="f"  data-sortable="true">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $fkor=explode(",",$kodp);

                        foreach($dafodp as $d):
                            $tkor=explode(",",$d->kordinat);
                        ?>  
                        <tr>
                            <td><?php echo $d->odcport; ?></td>
                            <td><?php echo $d->nama; ?></td>
                            <td><?php echo $d->alamat; ?></td>

                            <td><a href="<?php echo base_url("focmen/petap/".$d->idodp); ?>" target='_blank' data-toggle='tooltip' title=''><?php echo $d->kordinat; ?></a></td>
                            <!-- <td><?php echo $d->tarikan; ?></td>-->
                            <td><?php if($d->kordinat==null or $d->kordinat==0 ){ echo "tikor kosong";}else{$j=codexworldGetDistanceOpt($fkor[0],$fkor[1],$tkor[0],$tkor[1]); echo number_format($j*1000,2);} ?></td>
                           <td><center>
                                <!--<a href="<?php echo base_url("focmen/odped/".$d->idodp); ?>" data-toggle='tooltip' title='Edit' ><i class="fa fa-pencil fa-lg"></i></a>-->&nbsp;&nbsp; 
                                <a href="<?php echo base_url("focmen/rm_odp/".$d->idodp."/".$id); ?>" data-toggle='tooltip' title='Hapus' ><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp; 
                               <!-- <a href="<?php //echo base_url("focmen/odpli/".$dat->idodp); ?>" ><i class="fa fa-plus fa-lg"></i></a>  -->
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                                  
 
                    </tbody>
            </table>



        </div>
      </div>
    </div>

    </div><!--/.row end-->






</div>	<!--/.main-->

<script>


    $(document).ready(function(){


        $(".odcpil").select2();


    })
</script>