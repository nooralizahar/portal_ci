<?php 
       


?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url('panel/')?>"> Dashboard</a></li>
            <li> Pelanggan <?= $ket ?></li>
            
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Hapus Data berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h4 class="text-primary"> Daftar <?= $ket ?> </h4>
                </div>
        <div class="panel-body">




            <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc" data-page-size="25">
                    <thead>
                    <tr>
                        <th data-field="n"  data-sortable="true">No. Urut</th>
                        <th data-field="i"  data-sortable="true">Tgl. Aktif</th>
                        <th data-field="a"  data-sortable="true">Pelanggan</th>
                        <th data-field="b"  data-sortable="true">Kode ODP</th>
                        <th data-field="f"  data-sortable="true">IKR</th>
                        <th data-field="c"  data-sortable="true">Status</th>
                        
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        //$fkor=explode(",",$kodp);
                        $no=1;

                        foreach($tilpel as $d):
                            //$tkor=explode(",",$d->tikor_pelanggan);
                        ?>  
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo tgl_indome($d->tgl_aktif); ?></td>
                            <td><?php echo $d->nama_pelanggan; ?></td>
                            <td><?php echo $d->odp_code; ?></td>
                            <td><?php echo $d->nama_ikr; ?></td>
                            <td><?php if($d->status=='ACCST-01'){echo "<div style='color:green'>Active</div>";}elseif($d->status=='ACCST-02'){echo "<div style='color:orange'>Blocked</div>";}elseif($d->status=='ACCST-03'){echo "<div style='color:red'>Terminate</div>";} ?></td>

                        </tr>
                        <?php $no++; endforeach; ?>
                                  
 
                    </tbody>
            </table>



        </div>
      </div>
    </div>

    </div><!--/.row end-->






</div>	<!--/.main-->

