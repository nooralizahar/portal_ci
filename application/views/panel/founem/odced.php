
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("focmen/odpin/"); ?>"> Menu FTTH</a></li>
            <li class="active"> Edit ODC</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h5> Edit ODP</h5>
                </div>
        <div class="panel-body">
        <form class='add-item' method=POST action='focmen/odced/' enctype="multipart/form-data">
             <?php foreach($dafodc as $b):?>
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Nama / Kode ODC</label>
                <div class='col-sm-6'>
                    <input type='text' name='nama' class='form-control' id='nama' value="<?php echo $b->nama; ?>">
                    <input type="hidden" name="id" class="form-control" value="<?php echo $b->idodc; ?>" style="color:blue" readonly/>
                </div>

          </div>  
         <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Lokasi </label>
                <div class='col-sm-5'>
                    <select name="lokasi" id="lokasi" class="col-sm-5 form-control lokpil">
                            	<option value=""></option>
                                <?php foreach($daflok as $l):?>
                                    <option value="<?php echo $l->t_location_id; ?>" <?php echo ($b->lokasi == $l->t_location_id) ? "selected" : ""; ?>><?php echo $l->area_name." - ".$l->name; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
          </div> 
         <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Alamat </label>
                <div class='col-sm-10'>
                    <input type='text' name='alamat' class='form-control' id='alamat'value="<?php echo $b->alamat; ?>">
                </div>
          </div> 
        <div class='form-group row'>
                <label for='lblkordinat' class='col-sm-2 form-control-label'>Kordinat</label>
                <div class='col-sm-10'>
                      <input type='text' name='kordinat' class='form-control' id='kordinat' value="<?php echo $b->kordinat; ?>">
                </div>
                
        </div>    
        <div class='form-group row'>
                <label for='lblkapasitas' class='col-sm-2 form-control-label'>Kapasitas</label>
                <div class='col-sm-3'>
                       <input type='text' name='kapasitas' class='form-control' id='kapasitas' value="<?php echo $b->kapasitas ?>">

                </div>      

        </div>

        <div class='form-group row'>
                <label for='lblkapasitas' class='col-sm-2 form-control-label'>Foto</label>
                <div class='col-sm-10'> 
                <input type="file" multiple id="attach" name="attach[]" class="form-control"> <small>format file : *.gif,*.jpg,*.png<br></small>
                </div>
        </div>


            
        <?php endforeach; ?>          
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('focmen/odcin/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>




        </div>
      </div>
    </div>

    </div><!--/.row end-->



</div>	<!--/.main-->

<script>


    $(document).ready(function(){

        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
        
        $(".lokpil").select2();


    })
</script>