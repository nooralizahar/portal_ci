
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/pengguna"); ?>"> Permintaan Survey</a></li>
            <li class="active"> Baru</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading"> Isi Permintaan Survey Baru</div>
                <div class="panel-body">
                    <form action="" enctype="multipart/form-data" method="POST" onsubmit="FormSubmit(this);">
                        <div class="form-group"> 
                            <label for="">Pelanggan </label>
                                <input type="hidden" name="pelsvy" class="form-control" />
                                <select name="pelsvy_ddl" id="pelsvy_ddl" onchange="DropDownChanged(this);" class="form-control pilcus" />

                                    <?php foreach($dafcus as $dc):?>
                                    <option value="<?php echo $dc->id_rha."|".$dc->al_custo; ?>"><?php echo $dc->id_rha." - ".$dc->nm_custo; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Lainnya..</option>
                                </select> <input type="text" name="pelsvy_txt" style="display: none;" class="form-control" placeholder="Ketik lainnya disini" />

                        </div>
  <!--                      <div class="form-group">
                            <label for="">Tanggal</label>
                            
                        </div> -->
                        <div class="form-group">
                            <label for="">Alamat</label>
                            <input type="text" name="alasvy" id="alasvy" class="form-control" required/>

                            
                        </div>
                        <div class="form-group">
                            <label for="">Tanggal</label>
                            <input type="date" name="tglsvy" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Waktu</label>
                            <input type="time" name="jamsvy" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">PIC Pelanggan</label>
                            <input type="text" name="picsvy" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Telp/HP PIC</label>
                            <input type="text" name="tlpsvy" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Keterangan / SoW</label>
                            <input type="text" name="ketsvy" class="form-control" required/>
                        </div>
                        <label>Lampiran</label>
                            <input type="file" multiple id="attach" name="attach[]" class="form-control">
                            <small>format file : .pdf (maks. 2 MB)</small><br><br />
                        <center>
                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
                        <!-- <input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

<script>

    $(document).ready(function(){

            $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});

            $(".pilcus").select2();
    })
</script>

</div>	<!--/.main-->

<script type="text/javascript">

function DropDownChanged(oDDL) {
    var oTextbox = oDDL.form.elements["pelsvy_txt"];
    var dat=oDDL.value;
    var arr = dat.split("|");
    var var0 = arr[0];
    var var1 = arr[1];

    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
    document.getElementById("alasvy").value=var1;

}

function FormSubmit(oForm) {
    var oHidden = oForm.elements["pelsvy"];
    var oDDL = oForm.elements["pelsvy_ddl"];
    var oTextbox = oForm.elements["pelsvy_txt"];
    var dat=oDDL.value;
    var arr = dat.split("|");
    var var0 = arr[0];
    var var1 = arr[1];

    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : var0; //oDDL.value
}

</script>