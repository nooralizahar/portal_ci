﻿<?php $level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">	

    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("panel/"); ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("survey/"); ?>"> Permintaan Survey</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>
    
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> PERMINTAAN SURVEY</div>
                <div class="panel-body">    

                  <div class="row">
                      <div class="col-lg-12">

                          <div class="tabbable-panel">
                              <div class="tabbable-line">
                                  <ul class="nav nav-tabs " id="tabku">
                                      <li class="active">
                                          <a href="#tabsatu" data-toggle="tab">
                                           Permintaan </a>
                                      </li>
                                      <?php if($idjab==1 || $idjab==9 || $idjab==10 || $idjab==12 || $idjab==15): ?> 
                                      <li>
                                          <a href="#tabdua" data-toggle="tab">
                                          Antrian </a>
                                      </li> 
                                      <?php endif; ?>
                                  </ul>
                                  
                                  <div class="tab-content">

                                      <div class="tab-pane active" id="tabsatu">
                                        <?php $this->load->view("/panel/survey/survdtil")?>

                                      </div>

                                     <div class="tab-pane" id="tabdua">
                                        <?php $this->load->view("/panel/survey/survantr")?>

                                      </div> 

                                  </div>
                              </div>
                          </div>

                      </div> <!--/.col-->
                  </div><!--/.row-->           		

  
			  
                </div> <!--/.panel body -->
            </div> <!--/.panel default -->
          </div> <!--/.col -->
      </div> <!--/.row -->
</div>

 
