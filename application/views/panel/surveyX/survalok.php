
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/pengguna"); ?>"> Permintaan Survey</a></li>
            <li class="active"> Baru</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Data Permintaan Survey</div>
                <div class="panel-body">

                    <?php foreach ($pilsvy as $p): ?>
                        
                    <table>
                        <tr><td>Requester</td><td>&nbsp;:&nbsp;</td><td><?php echo $p->requester; ?></td></tr>
                        <tr><td>Tgl. Request</td><td>&nbsp;:&nbsp;</td><td><?php echo date('d-M-y H:i:s',strtotime($p->tgbua)); ?></td></tr>
                    </table><br><br>

                        <div class="form-group"> 
                            <label for="">Nama Pelanggan </label>
                                <input type="hidden" name="idsvy" class="form-control" value="<?php  $ids=$p->idsvy; echo $ids; ?>"/>
              <input type="text" name="pelsvy" class="form-control" value="<?php  echo $p->pelsvy; ?>" readonly/>

                        </div>
  
                        <div class="form-group">
                            <label for="">Alamat</label>
                            <input type="text" name="alasvy" id="alasvy" class="form-control" value="<?php  echo $p->alasvy; ?>" readonly/>

                            
                        </div>
                        <div class="form-group">
                            <label for="">Waktu Survey</label>
                            <input type="text" name="wktsvy" class="form-control" value="<?php echo "Hari ".hari_indo($p->tglsvy).", ".tgl_indome($p->tglsvy)." ".$p->jamsvy;?>" readonly/>
                        </div>

                        <div class="form-group">
                            <label for="">PIC Pelanggan / No. Kontak </label>
                            <input type="text" name="picsvy" class="form-control"  value="<?php echo $p->picsvy." / ".$p->tlpsvy;?>" readonly/>
                        </div>

                        <div class="form-group">
                            <label for="">Keterangan / SoW</label>
                            <input type="text" name="ketsvy" class="form-control" value="<?php  echo $p->ketsvy; ?>" readonly/>
                        </div>
                        <div class="form-group">
                            <label for="">Lampiran &nbsp;</label>
                           <?php if($p->lampirans==''){ 

                                }else{ 

                            if($p->lampirans != null):
                            $lampiran = json_decode($p->lampirans);
                            ?>
                                <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <a href="<?php echo base_url("assets/uploads/yevrus/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><?php echo $item->judul; ?></a>
                                <?php endforeach;  ?> 
                            
                             <?php endif; }?>
                        </div>

                    <?php if($p->picjlm != null):?>
                        <div class="form-group">
                            <label for="">PIC JLM</label>
                            <input type="text" name="picjlm" class="form-control" value="<?php  echo $p->picjlm; ?>" readonly/>
                        </div>
                        <center>
                        <button type='button' onclick="window.location.href = '<?php echo base_url('/survey') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button></center>
                    <?php else:?>  
                    <form action="" enctype="multipart/form-data" method="POST" onsubmit="FormSubmit(this);">     
                        <div class="form-group">
                            <input type="hidden" name="idsvy" class="form-control" value="<?php echo $ids; ?>" />
                            <center><label for="">ALOKASI FOC JLM </label></center>
                            <input type="hidden" name="picjlm" class="form-control" />
                                <select name="picjlm_ddl" id="picjlm_ddl" onchange="DropDownChanged(this);" class="form-control pilpic" />

                                    <?php foreach($alopic as $dc):?>
                                    <option value="<?php echo $dc->id_pengguna; ?>"><?php echo $dc->nama_lengkap; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Lainnya..</option>
                                </select> <input type="text" name="picjlm_txt" style="display: none;" class="form-control" placeholder="Ketik lainnya disini" />
                        </div>
                        <center>
                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
  
                    </form>

                    <?php endif;?>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div><!--/.row-->

<script>

    $(document).ready(function(){

            $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});

            $(".pilpic").select2();
    })
</script>

</div>	<!--/.main-->

<script type="text/javascript">
function DropDownChanged(oDDL) {
    var oTextbox = oDDL.form.elements["picjlm_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}

function FormSubmit(oForm) {
    var oHidden = oForm.elements["picjlm"];
    var oDDL = oForm.elements["picjlm_ddl"];
    var oTextbox = oForm.elements["picjlm_txt"];
    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;
}


</script>