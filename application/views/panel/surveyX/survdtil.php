﻿
<?php
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
$divi= $this->db->where("id_div",$this->session->userdata("id_div"))->get("ts_isivid")->result();

foreach($divi as $dvs): 
    $nmdv=$dvs->nm_div; 
    $iddv=$dvs->id_div;
endforeach;

$mharga= $this->db->where("penerima",$nmjab)->get("ts_agrah")->num_rows();
$nharga= $this->db->where("sales",$this->session->userdata("username"))->get("ts_agrah")->num_rows();
$dissuk=0;//$this->db->where("ke_user",$this->session->userdata("id_pengguna"))->get("relasi_disposisi")->num_rows();
$markaz = 0;//$this->db->where("id_dinas",$kantr)->get("dinas")->result();
?>
<?php if($idjab==1):?>
<?php else: ?>
<?php endif; ?>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Laporan Gagal disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Laporan berhasil disimpan<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>

 <!--      <div class="panel-heading"><i class="fa fa-money fa-lg"></i> Permintaan Harga</div>-->
           <div class="panel-body">
            <a href="<?php echo base_url("survey/survtamb"); ?>" class="btn btn-success"><i class="fa fa-plus-circle fa-lg"></i> <br><small><sup>BARU</sup></small></a>
                <table class="table table-striped" id="tarminga" data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-page-size="15"> <!--data-page-size="25" data-sort-name="f" data-sort-order="desc" -->
                    <thead>
                    <tr>
                        <th data-field="f" data-sortable="true"><center>Tgl. Req.</center></th>
                        <th data-field="h" data-sortable="true"><center>Status</center></th>
                        <th data-field="i" data-sortable="true">Requester</th>
                        <th data-field="a" data-sortable="true">Pelanggan</th>
                        <th data-field="b" data-sortable="true">Alamat</th>
                        <th data-field="c" data-sortable="true">Waktu Survey</th>
                        <th data-field="d" data-sortable="true">Lampiran</th>
                        <th data-field="e" data-sortable="true"><center>PIC / No. HP</center></th>   
                        <th data-field="g" data-sortable="true"><center>FOC / HASIL SURVEY</center></th>
                        <th data-field="j" data-sortable="true"><center>KPI </center></th>

                        <th>Opsi</th>
                        
<!--                     <th>Pilihan</th>-->
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dafrsv as $b):?>
                           
                        
                        <tr >
                            <?php 
                            $date1 = new DateTime($b->tgbua);
                            $date2 = new DateTime($b->tgupload);

                            $diff = $date2->diff($date1);

                            $hours = $diff->h;
                            $hours = $hours + ($diff->days*24);

                            $sisa=$hours%24; 
                            $hari=($hours-$sisa)/24; 

                            $minutes=$diff->i;
                            $minutes=$minutes + ($diff->days*1440);
                            $sisam=$minutes%1440; 
                            
                            if($hari!=0){$a=$hari." hari ";}else{$a=" ";} 
                            //if($sisa!=0){$b=$sisa." jam ";}else{$b=$sisa." jam ";}
                            //if($minutes!=0){$c=$minutes." menit ";}
                            $x=$a."&nbsp;".$sisa." jam ".$sisam." menit ";
                            ?>
                            <td class="text-center"><?php echo date("d-M-y H:i:s",strtotime($b->tgbua)); ?></td>
                            <td class="text-center"> <!-- STATUS -->
                                <?php 
                                if(empty($b->focjlm)){
                                    echo "<i class='fa fa-circle-o' style='color:red' ></i><small> open</small>";
                                }else{
                                    if($b->lampiranh=='' || is_null($b->lampiranh) ){ 
                                    echo "<i class='fa fa-check-circle' style='color:orange'></i><small> progress</small>";
                                    }else{
                                        echo "<i class='fa fa-check-circle' style='color:green'></i><small> closed</small>";
                                    }
                                } ?>
                                
                            </td>

                            <td><?php echo $b->requester; ?></td>
                            <td><?php echo $b->pelsvy." - ".$b->nm_custo; ?></td>
                            <td><?php echo $b->alasvy; ?></td>
                            <td><?php echo "Hari ".hari_indo($b->tglsvy).", ".tgl_indome($b->tglsvy)." ".$b->jamsvy."<br>Ket. SOW : &nbsp;<b class='text-primary'>".$b->ketsvy."</b><br> Media : ".$b->media; ?></td>
                            <td>
                            <?php if($b->lampirans==''){ 

                                }else{ 

                            if($b->lampirans != null):
                            $lampiran = json_decode($b->lampirans);
                            ?>
                                
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                 <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                <a href="<?php echo base_url("assets/uploads/yevrus/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><?php echo $item->judul; ?></a><br>
                                <?php endforeach;  ?> 
                            
                             <?php endif; }?>


                            </td>
                            <td><?php echo $b->picsvy.' / '.$b->tlpsvy; ?></td>


                            <td> <!-- FOC / HASIL SURVEY-->
                            <!-- ALOKASI FOC DISINI -->
                            	<?php if(!empty($b->focjlm)){echo 'FOC : '.$b->focjlm;}else{} ?><br>

                            <!-- HASIL SURVEY DISINI-->
                           	<?php if($b->lampiranh==''){ 

                                }else{ 

                            if($b->lampiranh != null):
                            $lampiran = json_decode($b->lampiranh);
                            ?>
                                
                                <?php foreach($lampiran as $item):

                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                <a href="<?php echo base_url("assets/uploads/yevrus/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><?php echo $item->judul; ?></a><br>
                                <?php endforeach;  ?> 
                            
                             <?php endif; }?>
                            		
                            </td>

                            <td class="text-center"> <?php echo $x; ?> </td>

                            <td class="text-center">
                               <?php if ($nmjab=='FOC Admin'):?>

                                <?php if(is_null($b->picjlm)): ?>
                                    <a href="<?php echo base_url("survey/focalok/".$b->idsvy); ?>" data-toggle='tooltip' title='Tambah PIC FOC JLM'><i class="fa fa-user fa-lg"></i></a>
                                    
                                <?php else :?> 

                                    <?php if($b->lampiranh=='' || is_null($b->lampiranh) ): ?>
                                    <a href="<?php echo base_url("survey/survuplo/".$b->idsvy); ?>" data-toggle='tooltip' title='Upload Hasil Survey'><i class="fa fa-upload fa-lg"></i></a>
                                    
                                    <?php else :?>
                                    <a href="<?php echo base_url("survey/survliat/".$b->idsvy); ?>" data-toggle='tooltip' title='Lihat Hasil Detil'><i class="fa fa-eye fa-lg"></i></a>
                                    <?php endif;?>

                                <?php endif; ?>

                              <?php endif;?></td>
                            
                            
                           

                            

                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                   </table>
                  </div>
                </div>
            </div>
        </div> 




 
