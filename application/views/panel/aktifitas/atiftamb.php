
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/pengguna"); ?>">Dashboard Aktifitas</a></li>
            <li class="active">Laporan Harian</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Isi Laporan Baru</div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="">Pelanggan / Div. / Dep. </label>
                            <input type="text" name="customer" class="form-control" required/>
                            <input type="hidden" name="date" class="form-control" value="<?php echo date('Y-m-d'); ?>" required/>
                        </div>
  <!--                      <div class="form-group">
                            <label for="">Tanggal</label>
                            
                        </div> -->
                        <div class="form-group">
                            <label for="">Tugas / Aktifitas</label>
                            <input type="text" name="jobs" class="form-control" required/>
                           <!-- <select name="jobs" id="jobs" class="form-control" required/>
                                <option value="">-- Pilih Disini --</option>
                                <?php //foreach($daftugas as $b): ?>
                                    <option value="<?php //echo $b->tugas; ?>"><?php // echo $b->tugas; ?></option>
                                <?php //endforeach;?>
                            </select> -->
                            
                        </div>
                        <div class="form-group">
                            <label for="">Uraian/ Kasus / Masalah</label>
                            <input type="text" name="case" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Kendala</label>
                            <input type="text" name="issues" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Tindakan</label>
                            <input type="text" name="action" class="form-control" required/>
                        </div>

                        <center>
                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
                        <!-- <input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

