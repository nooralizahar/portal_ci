<?php $sales=$this->session->userdata("username")?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/hargabaru/"); ?>">Harga Baru</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Perbaharui Permintaan Harga</div>
                <div class="panel-body">
				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal meminta harga! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Permintaan harga berhasil dikirim! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <form action="" method="POST" enctype="multipart/form-data">

                        <div class="col-md-12">
                            <label>Presales</label>
                            <select name="penerima" class="form-control penerima" > //multiple
                                <?php foreach($daftar_jabatan as $jabatan):?>
                                    <!-- <option value="<?php echo $jabatan->nm_jab; ?>" selected> <?php echo $jabatan->nm_jab;?></option> -->

                                        <option value="<?php echo $jabatan->nm_jab; ?>" <?php echo ($dafrid->penerima == $jabatan->nm_jab) ? "selected" : ""; ?>><?=$dafrid->penerima;?></option>


                                <?php endforeach;?>
                               <!-- 2021 11 08 Req. non aktif via WAG by P. Ari <option value="<?php //echo $datdiv->nm_div; ?>"> <?php //echo $datdiv->nm_div."";?></option> -->
                                
                            </select>

                         <!--   <select name="penerima[]" class="form-control penerima" multiple>
                                <?php foreach($daftar_pengguna as $dinas => $group_pengguna): ?>
                                    <optgroup label="<?php echo $dinas ?>">
                                        <?php foreach($group_pengguna as $pengguna):?>
                                            <option value="<?php echo $pengguna->id_pengguna; ?>" <?php echo (termasuk_penerima($pengguna->id_pengguna,$penerima_otomatis) || isset($_GET["pn"]) && $_GET["pn"] == $pengguna->id_pengguna) ? "selected" : ""; ?>><?php echo $pengguna->nama_lengkap . ", " . $pengguna->nama_jabatan . " - " . $dinas; ?></option>
                                        <?php endforeach;?>
                                    </optgroup>
                                <?php endforeach; ?>
                            </select> -->

                        </div>
                        <div class="col-md-6">
                            <label>Nama Pelanggan </label>
                            <input type="text" name="nm_custo" placeholder="Company Name" class="form-control" value="<?php echo $dafrid->nm_custo; ?>">
							<input type="hidden" name="sales" class="form-control" value="<?php echo $sales; ?>">
                        </div>

                        <div class="col-md-6">
                            <label>Alamat / Site </label>
                            <input type="text" name="al_custo" placeholder="Site Installation" class="form-control" value="<?php echo $dafrid->al_custo; ?>" required/><br />
                        </div>
                        <div class="col-md-3">
                            <label>Segment</label>
                            <select name="sg_custo" class="form-control segment">
                                <?php foreach($daftar_segment as $dat):?>
                                    <!--<option value="<?php echo $dat->id_seg; ?>"> <?php echo $dat->ds_seg;?></option> -->
                                    <option value="<?php echo $dat->id_seg; ?>" <?php echo ($dafrid->sg_custo == $dat->id_seg) ? "selected" : ""; ?>><?=$dat->ds_seg;?></option>                                    
                                <?php endforeach;?>
                            </select><br />
                        </div>  
                        <div class="col-md-3">
                            <label>Media </label>
                            <select name="media" class="form-control media" multiple>
                                <?php foreach($daftar_media as $media):?>
                                    <!--<option value="<?php echo $media->media; ?>"> <?php echo $media->media;?></option> -->
                                    <option value="<?php echo $media->media; ?>" <?php echo ($dafrid->media == $media->media) ? "selected" : ""; ?>><?=$media->media;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Request </label>
                            <select name="request" class="form-control media" multiple>
                                <?php foreach($daftar_request as $request):?>
                                    <!--<option value="<?php echo $request->request; ?>"> <?php echo $request->request;?></option>-->
                                    <option value="<?php echo $request->request; ?>" <?php echo ($dafrid->request == $request->request) ? "selected" : ""; ?>><?=$request->request;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Service</label>
                            <select name="service" class="form-control media" multiple>
                                <?php foreach($daftar_service as $service):?>
                                    <option value="<?php echo $service->service; ?>"> <?php echo $service->service;?></option>
                                    <option value="<?php echo $service->service; ?>" <?php echo ($dafrid->service == $service->service) ? "selected" : ""; ?>><?=$service->service;?></option>
                                <?php endforeach;?>
                            </select><br />
                        </div>
                        <div class="col-md-1">
                            <label>IX</label>
                            <input type="text" name="ix" placeholder="Mbps" class="form-control" value="<?php echo $dafrid->ix; ?>">
                        </div>
                        <div class="col-md-1">
                            <label>IIX </label>
                            <input type="text" name="iix" placeholder="Mbps" class="form-control" value="<?php echo $dafrid->iix; ?>" >
                        </div>
                        <div class="col-md-1">
                            <label>MIX</label>
                            <input type="text" name="mix" placeholder="Mbps" class="form-control" value="<?php echo $dafrid->mix; ?>" >
                        </div>
                        <div class="col-md-1">
                            <label>L.Loop</label>
                            <input type="text" name="llo" placeholder="Mbps"  class="form-control" value="<?php echo $dafrid->llo; ?>" >
                        </div>
                        <div class="col-md-3">
                            <label>Colocation</label>
                            <input type="text" name="colo" placeholder="Location" class="form-control" value="<?php echo $dafrid->colo; ?>" >
                        </div>
                        <div class="col-md-2">
                            <label>Telephony</label>
                            <input type="text" name="tele" placeholder="lines" class="form-control" value="<?php echo $dafrid->tele; ?>" ><br />
                        </div>
                        <div class="col-md-3">
                            <label>NMC</label>
                            <input type="text" name="nmc" placeholder="contoh : 10000/onetime atau 50000/bulanan" class="form-control" value="<?php echo $dafrid->nmc; ?>" ><br />
                        </div>
                        <div class="col-md-12">
                            <label>Keterangan</label>
                            <textarea name="ket" id="ckeditor" ><?php echo $dafrid->ket; ?></textarea><br />
                        <label>Lampiran</label>
                            <?php if($dafrid->lampiran != null):
                            $lampiran = json_decode($dafrid->lampiran);
                            ?>
                              
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                
                                 <?php echo $cls; ?>
                                 <?php echo $item->judul; ?>

                                  <div class="row-iframe" >
                                  <center>
                                  <embed src="<?php echo base_url("assets/uploads/lampiran/".$item->file); ?>" width="900px" height="150px" />
                                  </center>
                                  </div>

                                <?php endforeach; ?> 
                            
                             <?php endif;?>

                            <input type="file" multiple id="attach" name="attach[]" class="form-control">
                            <small>format file : .jpg, .png, .pdf, .jpeg, .bmp, .gif, .doc, .docx, .xls, .xlsx, .ppt, .pptx. (maks. 2 MB)</small><br><br />
                            <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-paper-plane-o fa-lg"></i><br><small><sup>KIRIM</sup></small></button>
                            <!--<input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('ket',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>


    $(document).ready(function(){
        $("#msg").froalaEditor({
            height: 300
        });

        $(".penerima").select2();
        $(".segment").select2();
        $(".media").select2();
        $(".request").select2();
        $(".service").select2();

        $("#tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>