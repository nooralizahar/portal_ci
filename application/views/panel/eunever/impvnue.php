<?php 


?>

    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal upload file.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Upload file berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>

                <?php endif; ?>
                <div class="panel-heading">
                    Import Data Revenue
                </div>

        <div class="panel-body">
    <?php echo  $this->session->flashdata('pesan');?>
    <form action="/salvenue/ropmi" method="POST" enctype="multipart/form-data">
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>File Excel</label>
                <div class='col-sm-10'>
                <input type="file" multiple id="nafile" name="nafile[]" class="form-control"> <small>format file : *.xls,*.xlsx. <br></small>
  

                </div>
        </div>  
        <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-9'>
                      <button type="submit" name="btnImport" class="btn btn-primary"><i class="fa fa-upload fa-lg"></i><br><small><sup>IMPORT</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('/salvanue') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>

        </div>
                
    </form>

            <div class="form-group"> 

                       <!-- <table class="table table-bordered"> data-sort-name="name" data-sort-order="desc"-->
                       	<center><h3>DAFTAR FILE UNGGAH</h3></center>
                               <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true">
                            <thead>
                            
                            <tr>

                            
                                <th class="text-center">&nbsp;ID#</th>
                                <th>&nbsp;Nama File</th>
                                <th>&nbsp;Tgl. Unggah</th>
                                <th><center>Jml Data</center></th>
                                <th><center>Pengunggah</center></th>
                                <th><center>Opsi</center></th>

                            </tr>
                            </thead>

                            <tbody>
                            <?php 
                            if($dafimp==false){
                                echo "<tr><td></td></tr>";
                            }else{
                            
                            foreach($dafimp as $i): ?>
                                <tr >
                                   
                                    <td><?php echo $i->idupl; ?> </td>

                                    <td><?php if($i->nafi==''){ 

                                    }else{ 

                                if($i->nafi != null){
                                $fibla= json_decode($i->nafi);
                                ?>
                                    
                                    <?php foreach($fibla as $item):
                                    $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                    ?>
                                     
                                    <a href="<?php echo base_url("assets/uploads/fileim/".$item->file); ?>" class="<?php  echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><small><?php echo $item->judul;//$ij=explode(' ',$item->judul); echo $ij[0].'...'; ?></a></small><br>
                                    <?php endforeach;  ?> 
                                
                                <?php } }?> </td>

                                    <td><?php echo tgl_indopj($i->tgupl).' '.date('H:i:s',strtotime($i->tgupl)); ?> </td>
                                    <td class="text-right"><?php echo $i->jml; ?> </td>
                                    <td class="text-right"><?php echo $i->uploader; ?> </td>
                                    
                                    <td class="text-right">
                                    <?php if($i->idupl<3){echo "&nbsp;";}else{?>
                                        <a href="<?php echo base_url("panel/rbsuke/".$i->idupl); ?>" data-toggle='tooltip' title='rollback upload ID#  <?php echo $i->idupl; ?> ?' class='red-tooltip'><i class="fa fa-reply fa-lg fa-fw"></i></a>&nbsp;&nbsp;<a href="<?php echo base_url("panel/ullampsk/".$i->idupl); ?>" data-toggle='tooltip' title='Unggah File Lampiran' class='red-tooltip'><i class="fa fa-upload fa-lg fa-fw"></i></a> 
                                    <?php }?>
                                    </td>
                                    


                                </tr>
                            <?php endforeach; }?>
                            </tbody>
                           </table>

            </div>

<br><br>



        </div> <!-- panel body end -->
      </div><!-- panel-default end-->
    </div> <!--/.col-12 end-->

    </div><!--/.row end-->

</div>	<!--/.main-->

<script>

    $(document).ready(function(){
        $(".nafile").select2();
    })

    function kembali() {
        window.history.back();
    }

</script>