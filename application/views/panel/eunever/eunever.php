<?php 
//$blnini=date('Y-m');
//$blnlal=date('Y-m',strtotime("-1 month"));
//$all=$this->db->where("tg_issue like ",$blnini."%")->get("tp_owatad")->num_rows(); echo $all;
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/"); ?>"> Dashboard</a></li>
            <li class="active"> Sales Revenue</li>
        </ol>
    </div><!--/.row-->

    <hr/>


<div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                 <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["suc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsu"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data <?=$i?> berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                    </div>
                <?php elseif(isset($_GET["her"])):?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["isu"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Import Data Berhasil<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["ier"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Import Data Gagal<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?> 



<div class="panel-heading">
    <h4 style="color:#010288"><i class="fa fa-bar-chart" aria-hidden="true"></i>
 Daftar Revenue</h4>
</div>


        <div class="panel-body">
                  <div class="row">
                      <div class="col-lg-12">
                      <div class="tabbable-panel">
                              <div class="tabbable-line">
                                  <ul class="nav nav-tabs ">

                                      <li class="active">
                                          <a href="#tabsa" data-toggle="tab">
                                          Revenue </a>
                                      </li>
                                      <li>
                                          <a href="#tabdu" data-toggle="tab">
                                          Import</a>
                                      </li>
                                      <li>
                                          <a href="#tabga" data-toggle="tab">
                                          Rekapitulasi </a>
                                      </li>

                                  </ul>
                                  
                                  <div class="tab-content">
                                      <div class="tab-pane active" id="tabsa">
                                        <?php $this->load->view("/panel/eunever/datvnue")?>

                                      </div>
                                      <div class="tab-pane" id="tabdu">
                                        <?php $this->load->view("/panel/eunever/impvnue")?>

                                      </div>
                                      <div class="tab-pane" id="tabga">
                                        <?php //$this->load->view("/panel/revenue/inforin")?>

                                      </div>
                                  </div>  

                              </div>
                      </div>
                      </div> <!--/.col-->
                  </div><!--/.row-->   





        </div>
      </div>
    </div>

    </div><!--/.row end-->


 



</div>	<!--/.main-->
<?php 
  


?>

<div class="modal fade" id="datvie0" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:red'>Detil Open</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body"  style="max-height: 400px;overflow-y: scroll;">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnini."%")->where("sta",0)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; <b style='color:red'> ".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="datvie1" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:green'>Detil Completed</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body" style="max-height: 400px;overflow-y: scroll;">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnini."%")->where("dan",1)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp;<b style='color:green'>".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="datvie2" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:pink'>Detil Canceled</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnini."%")->where("can",1)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp;<b style='color:pink'>".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>


<div class="modal fade" id="datvie3" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel"><b style='color:orange'>Detil On Progress</b>
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body"  style="max-height: 400px;overflow-y: scroll;">
                    <table>
                    <?php 
                    $datref=$this->db->where("tg_issue like ",$blnini."%")->where("dan",0)->where("sta !=",0)->where("can !=",1)->get("v_tp_owatad")->result();
                    $a=1;
                    foreach ($datref as $dr) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp;<b style='color:orange'>".$dr->id_worde.'</b>&nbsp;'.substr($dr->nm_custo,0,50)." - <b>".$dr->nm_sales."</b></td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>


      <!--  <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-merah panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked chevron up"><use xlink:href="#stroked-chevron-up"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datvie0"><?php /*

                            $oi=$this->db->where("tg_issue like ",$blnini."%")->where("sta",0)->get("v_tp_owatad")->num_rows();
                                        $ci=$this->db->where("tg_issue like ",$blnini."%")->where("dan",1)->get("v_tp_owatad")->num_rows();
                          $bi=$this->db->where("tg_issue like ",$blnini."%")->where("can",1)->get("v_tp_owatad")->num_rows();
                         echo $oi; */ ?></a></div>
                        <div class="text-muted">Open</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clock"><use xlink:href="#stroked-clock"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datvie3"><?php //$p=$all-($oi+$ci+$bi); echo $p; ?></a></div>
                        <div class="text-muted">On Progress</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datvie1"><?php //echo $ci; ?></a></div>
                        <div class="text-muted">Completed</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-merah panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="#<?php //echo base_url("panel/word/$blnini"); ?>" data-toggle="modal" data-target="#datvie2"><?php //echo $bi; ?></a></div>
                        <div class="text-muted">Canceled</div>
                    </div>
                </div>
            </div>
        </div> 
<div class="row">
  <div class="col-lg-12"><h4 class="text-muted text-center">Bulan ini : <?= $all ?></h4></div>
</div> -->