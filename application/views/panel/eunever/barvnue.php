
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/pengguna"); ?>"> Sales Revenue</a></li>
            <li class="active"> Baru</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading"> Isi Revenue Baru</div>
                <div class="panel-body">
                    <form action="" enctype="multipart/form-data" method="POST" onsubmit="FormSubmit(this);">
                        <div class="form-group"> 
                            <label for="">No. WO </label>
                                <input type="hidden" name="idr" class="form-control" />
                                <select name="idr_ddl" id="idr_ddl" onchange="DropDownChanged(this);" class="form-control pilidr" />

                                    <?php foreach($dafwor as $dw):?>
                                    <option value="<?php echo $dw->id_worde; ?>"><?php echo $dw->id_worde." - ".$dw->nm_custo; ?></option>
                                    <?php endforeach;?>
                                    <option value="">Lainnya..</option>
                                </select> <input type="text" name="idr_txt" style="display: none;" class="form-control" placeholder="Ketik lainnya disini" />

                        </div>
                        <div class="form-group">
                            <label for="">segment</label>
                            <select name="seg" class="form-control segment">
                                <?php foreach($daftar_segment as $ds):?>
                                    <option value="<?php echo $ds->id_seg; ?>"> <?php echo $ds->ds_seg;?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Receipt</label>
                            <input type="date" name="tgl" id="tgl" class="form-control">   
                        </div>
                        <div class="form-group">
                            <label for="">Start Bill.</label>
                            <input type="date" name="tgm" id="tgm" class="form-control">   
                        </div>
                        <div class="form-group">
                            <label for="">End Bill.</label>
                            <input type="date" name="tgs" id="tgs" class="form-control">   
                        </div>
                        <div class="form-group">
                            <label for="">Nilai (Rp.)</label>
                            <input type="text" name="nil" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Service Initial</label>
                            <input type="text" name="ser" id="ser" class="form-control">   
                        </div>
                        <div class="form-group">
                            <label for="">Vendor</label>
                            <input type="text" name="ven" id="ven" class="form-control">   
                        </div>
                        <br><br />
                        <center>
                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
                        <!-- <input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

<script>

    $(document).ready(function(){


            $(".pilidr").select2();
    })
</script>

</div>	<!--/.main-->

<script type="text/javascript">

function DropDownChanged(oDDL) {
    var oTextbox = oDDL.form.elements["idr_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}

function FormSubmit(oForm) {
    var oHidden = oForm.elements["idr"];
    var oDDL = oForm.elements["idr_ddl"];
    var oTextbox = oForm.elements["idr_txt"];
    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;
}

</script>