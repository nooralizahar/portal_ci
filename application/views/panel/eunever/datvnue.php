<?php
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;

?>

<?php if ($nmjab=='Sales GM'){?>
&nbsp;<a href="<?php echo base_url("salvenue/ropke"); ?>" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg"></i> <br><small><sup>EXCEL</sup></small></a>
<?php }elseif ($nmjab=='Sales Admin'){?>
&nbsp;<a href="<?php echo base_url("salvenue/vurab"); ?>" class="btn btn-primary"><i class="fa fa-file fa-lg"></i> <br><small><sup>&nbsp;BARU&nbsp;</sup></small></a>

<br>
<?php }else{}?>
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-3"><br>
        <?php 
           // echo "<small style='color:white; background-color:black;'>&nbsp;Last update : ". date("d-M-y H:i:s", strtotime($updidwor))."&nbsp;</small>";
         ?></div>
        </div>
        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true">No. WO </th>
                        <th data-field="s"  data-sortable="true">Sales</th>
                        <th data-field="b"  data-sortable="true">Pelanggan</th>
                        <th data-field="c"  data-sortable="true">Alamat</th>
                        <th data-field="d"  data-sortable="true">Segment</th>
                        <th data-field="z"  data-sortable="true">S. Initial</th>
                        <th data-field="e"  data-sortable="true">Receipt </th>
                        <th data-field="f"  data-sortable="true">Start Bill. </th>
                        <th data-field="g"  data-sortable="true">End Bill. </th>
                        <th data-field="h"  data-sortable="true"><center>Nilai <br>(Rp.)</center></th>
                        <th data-field="v"  data-sortable="true"><center>Vendor</center></th>
                        <th data-field="i"  data-sortable="true"><center>Dibuat<br>oleh</center></th>
                        <th data-field="j"  data-sortable="true">Opsi</th>
                        

                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach($revdat as $rd): ?>
                    <tr>
                        <td><?php echo $rd->idr; ?></td>
                        <td><?php echo $rd->nm_sales; ?></td>
                        <td><?php echo $rd->nm_custo; ?></td>
                        <td><?php echo $rd->al_custo; ?></td>
                        <td><?php echo $rd->seg; ?></td>
                        <td><?php echo $rd->sv_custo; ?></td>
                        <td><?php echo tgl_indome($rd->tgl); ?></td>
                        <td><?php if($rd->tgm=='0000-00-00'){echo '-';}else{echo tgl_indome($rd->tgm);}; ?></td>
                        <td><?php if($rd->tgs=='0000-00-00'){echo '-';}else{echo tgl_indome($rd->tgs);}; ?></td>
                        <td><?php echo number_format($rd->nil); ?></td>
                        <td><?php echo $rd->ven; ?></td>
                        <td><?php echo $rd->pbuat; ?></td>
                        <td>
                            <a href="<?php echo base_url("salvenue/supah/".$rd->idr); ?>" data-toggle='tooltip' title='Hapus' ><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;
                            <a href="<?php echo base_url("salvenue/vhabu".$rd->idr); ?>" data-toggle='tooltip' title='Edit' ><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp;
                        </td>
                        
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
            </table>