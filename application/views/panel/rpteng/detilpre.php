<?php 


?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("focmen/odpin/"); ?>"> Pre Sales</a></li>
            <li class="active"> Detil Penentuan Harga Presales</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <?php //$delay = 0; header("Refresh: $delay;");?>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h4 class="text-primary"> <?php echo  str_replace('%20',' ',$n);?> Bulan <?php echo $b."/".date('Y');?></h4>
                </div>
        <div class="panel-body">


<!-- <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">
                    <a href="<?php echo base_url("focmen/odpcs/"); ?>">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"> <?php echo number_format($this->db->where("idodp",1)->get("t_odp_rel")->num_rows()); ?>
                        </div>
                        <div class="text-muted"> Belum di Setting ODP</div>
                    </div>
                    </a>
                </div>
            </div>
        </div>


        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-teal panel-widget ">
                <div class="row no-padding">
                    <a href="<?php echo base_url("focmen/odpcs/1"); ?>">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"> <?php echo number_format($this->db->where("idodp !=",1)->get("t_odp_rel")->num_rows()); ?>
                        </div>
                        <div class="text-muted"> Sudah di Setting ODP</div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
</div> -->


            <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-page-size="50" data-pagination="true" data-sort-name="a" data-sort-order="asc">
                    <thead>
                    <tr>
                        <th data-field="d"  data-sortable="true">Tgl. Jawab</th>
                        <th data-field="e"  data-sortable="true">Jumlah</th>


                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($datil as $d):?>  
                        <tr>
                            <td><?php echo $d->tgljwb; ?></td>
                            <td><?php echo $d->jml; ?></td>

                        </tr>
                        <?php endforeach; ?>
                                  
 
                    </tbody>
            </table>



        </div>
      </div>
    </div>

    </div><!--/.row end-->






</div>	<!--/.main-->

