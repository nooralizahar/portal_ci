
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
        <li class="active">Kelola Pengguna</li>
    </ol>
</div><!--/.row--><br />

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <?php if(isset($_GET["succ"]) && $_GET["succ"] == 1): ?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Pengguna berhasil diblokir! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
            <?php elseif(isset($_GET["succ"]) && $_GET["succ"] == 2): ?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Pengguna berhasil dibuka! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
            <?php endif;?>
            <div class="panel-heading">Daftar Pengguna</div>
            <div class="panel-body">
                <a href="<?php echo base_url("panel/penggunatamb"); ?>" class="btn btn-success"><i class="fa fa-plus-circle fa-lg"></i> <br><small><sup>BARU</sup></small></a>
                <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="nik"  data-sortable="true">NIK</th>
                        <th data-field="nama_lengkap"  data-sortable="true">Nama Lengkap</th>
                        <th data-field="jabatan"  data-sortable="true">Jabatan</th>
                        <th data-field="dinas" data-sortable="true">Divisi</th>
                        <th>Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($daftar_pengguna as $pengguna): ?>
                        <tr>
                            <td><?php echo $pengguna->nip; ?></td>
                            <td><?php echo $pengguna->nama_lengkap; ?></td>
                            <td><?php echo $pengguna->nm_jab; ?></td>
                            <td><?php echo $pengguna->nm_div; ?></td>
                            <td>
                                <a href="<?php echo base_url("panoc/penggunaedit/".$pengguna->id_pengguna); ?>" class="btn btn-primary"><i class="fa fa-pencil fa-lg"></i><br><small><sup>EDIT</sup></small></a>&nbsp; <!-- Edit Pengguna -->
                                <?php if($pengguna->blokir == 0): ?>
                                    <a href="<?php echo base_url("panoc/penggunablokir/".$pengguna->id_pengguna); ?>" class="btn btn-warning"><i class="fa fa-ban fa-lg fa-fw"></i><br><small><sup>BLOK</sup></small></a> <!-- Blokir Pengguna -->
                                <?php else: ?>
                                    <a href="<?php echo base_url("panoc/penggunabuka/".$pengguna->id_pengguna); ?>" class="btn btn-success"><i class="fa fa-external-link fa-lg"></i><br><small><sup>BUKA</sup></small></a> <!-- Buka Blokir -->
                                <?php endif;?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!--/.row-->

</div><!--/.main-->
