
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panoc/"); ?>"> Panel</a></li>
            <li class="active"> Profil </li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default" >

                <div class="panel-heading">Profil Ku</div>
                <div class="panel-body">
                    
                    <table class="table table-hover">
                        <tr><td>Nama Lengkap</td><td>:</td><td><?php echo $pengguna->nama_lengkap; ?></td></tr>
                        <tr><td>Divisi </td><td>:</td><td><?php echo $pengguna->id_div; ?></td></tr>
                        <tr><td>Jabatan </td><td>:</td><td><?php echo $pengguna->id_jabatan; ?></td></tr>
                        <tr><td>Atasan </td><td>:</td><td><?php echo $pengguna->id_spv; ?></td></tr>
                        <tr><td>NIK </td><td>:</td><td><?php echo $pengguna->nip; ?></td></tr>
                        <tr><td>No. HP </td><td>:</td><td><?php echo $pengguna->notifikasi; ?></td></tr>
                    </table>
                       
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->


