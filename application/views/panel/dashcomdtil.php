﻿
<?php
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
$divi= $this->db->where("id_div",$this->session->userdata("id_div"))->get("ts_isivid")->result();

foreach($divi as $dvs): 
    $nmdv=$dvs->nm_div; 
    $iddv=$dvs->id_div;
endforeach;

$mharga= $this->db->where("penerima",$nmjab)->get("ts_agrah")->num_rows();
$nharga= $this->db->where("sales",$this->session->userdata("username"))->get("ts_agrah")->num_rows();
$dissuk=0;//$this->db->where("ke_user",$this->session->userdata("id_pengguna"))->get("relasi_disposisi")->num_rows();
$markaz = 0;//$this->db->where("id_dinas",$kantr)->get("dinas")->result();
?>
<?php if($idjab==1):?>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $this->db->get("ts_agrah")->num_rows(); ?></div>
                        <div class="text-muted">Req. Harga</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-red panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $this->db->get("pengguna")->num_rows(); ?></div>
                        <div class="text-muted">Pengguna</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-blue panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $this->db->get("ts_natabaj")->num_rows(); ?></div>
                        <div class="text-muted">Jabatan</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked app-window-with-content"><use xlink:href="#stroked-app-window-with-content"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $this->db->get("ts_isivid")->num_rows(); ?></div>
                        <div class="text-muted">Divisi</div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/.row-->
    
    <?php else: ?>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> Dashboard</h1>
        </div>
    </div><!--/.row-->

    <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-blue panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php if($nmjab=='Presales Manager' || $nmjab=='Presales' || $nmjab=='Sales Admin'){echo $mharga;}else{echo $nharga;} ?></div>
                        <div class="text-muted">Req. Harga</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-red panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $dissuk;?></div>
                        <div class="text-muted">Diskusi</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div><?php echo $nmjab; ?></div>
                        <div class="text-muted">Akses</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked app-window-with-content"><use xlink:href="#stroked-app-window-with-content"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div ><?php echo $nmdv;  ?></div>
                        <div class="text-muted">Divisi</div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/.row-->
<?php endif; ?>

    <hr>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal tambah harga! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Tambah harga berhasil disimpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["usuc"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Ubah permintaan berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["esucc"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Edit harga berhasil disimpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                               <?php elseif(isset($_GET["hsuc"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["eerr"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal edit harga! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["herr"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal di hapus <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>

 <!--      <div class="panel-heading"><i class="fa fa-money fa-lg"></i> Permintaan Harga</div>-->
           <div class="panel-body">
                <table class="table table-striped" id="tarminga" data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-page-size="25" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="tglkirim" data-sortable="true">Tgl. Kirim</th>
                        <th data-field="sales" data-sortable="true">Sales</th>
                        <th data-field="pelanggan" data-sortable="true">Pelanggan</th>
                        <th data-field="site" data-sortable="true">Site</th>
                        <th data-field="uraian" data-sortable="true">Uraian</th>
                        <th data-field="rpinstall" data-sortable="true">Instalasi (Rp.)</th>
                        <th data-field="rpbulanan" data-sortable="true">Bulanan (Rp.)</th>
                        <?php if($nmjab=='Presales Manager' || $nmjab=='Presales' || $nmjab=='Sales' || $nmjab=='Sales Supervisor' || $nmjab=="CRO Supervisor" || $nmjab=="CRO" || $nmjab=="Sales Jr. Manager"):?>
                        <th data-field="quo" data-sortable="true">BoQ. Ref.</th>   
                        <?php endif;?>
                        <th data-field="cathrga" data-sortable="true">Catatan</th>
                        <th data-field="assignby" data-sortable="true">Assign by</th>
                        <th data-field="tgljawab" data-sortable="true">Tgl. Jawab</th>
                        <th data-field="kpi" data-sortable="true">KPI (hari)</th>
<!--                        <th>Pilihan</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                    if($nmjab=="Administrator"){$daf=$dafmua;}elseif($nmjab=="CRO Supervisor"){$daf=$dafcropv;}elseif($nmjab=="CRO"){$daf=$dafcro;}elseif($nmjab=="Sales"){$daf=$dafrsa;}elseif($nmjab=="Sales Supervisor"){$daf=$dafrsapv;}elseif($nmjab=="Sales Jr. Manager"){$daf=$dafrsamg;}elseif($nmdv=="Manage Service"){$daf=$dafmserv;}else{$daf=$dafreq;}
                    foreach($daf as $harga): ?>
                        <tr >
                            <?php $idr=strval($harga->id_rha); 
                            $date1 = new DateTime($harga->tg_buat);
                            $date2 = new DateTime($harga->tg_jwb);

                            $diff = $date2->diff($date1);

                            $hours = $diff->h;
                            $hours = $hours + ($diff->days*24);
                            ?>
                            <td><?php echo date("d-M-y H:i:s",strtotime($harga->tg_buat)); ?> 
                            
                            </td>
                            <td>
                                <?php 
                                
                                echo $harga->nama_lengkap." <br>"; 
                                
                                if($harga->stapv1==1 && $harga->stapv2==1 && $harga->stapv3==1){
                                	if($harga->sales==$this->session->userdata("username")){
                                        echo "<center><a href=".base_url('panel/takquo/'.$harga->id_rha)." data-toggle='tooltip' data-placement='top' title='Cetak Quotation'><i class='fa fa-print fa-lg' style='color:green'></i><sup><small><br>QUO</small></sup></a></center>";     
                                	}
                                    }else{
                                        echo "";
                                    }

                                ?>
                            </td>
                            <td>

                            <?php if($harga->hg_instalasi==0 && $harga->hg_bulanan==0) :
                                if($harga->sales==$this->session->userdata("username")){ ?>

                                &nbsp;<a href="<?php echo base_url("panel/hargapus/".$harga->id_rha);  ?>"><i class="fa fa-trash"></i></a>&nbsp;<a href="<?php echo base_url("panel/hargadit/".$harga->id_rha);  ?>"><i class="fa fa-pencil"></i></a>
                            <?php } endif; ?>

                                <?php  
                                    echo $harga->id_rha." - ".$harga->nm_custo."<br><br>";

                                    if($harga->stapv1==null){
                                        echo ""; 
                                    }else{

                                        if($harga->stapv1==1){$st1="<a href='#' data-toggle='tooltip' data-placement='top' title='Approved $harga->tgapv1'><i class='fa fa-check fa-lg' style='color:green'></i></a>&nbsp;";
                                        }elseif(($harga->stapv1==2)){$st1="<a href='#' data-toggle='tooltip' data-placement='top' title='Rejected $harga->tgapv1'><i class='fa fa-times fa-lg' style='color:red'></i></a>&nbsp;";
                                        } 

                                        if($harga->stapv2==1){$st2="<a href='#' data-toggle='tooltip' data-placement='top' title='Approved $harga->tgapv1'><i class='fa fa-check fa-lg' style='color:green'></i></a>&nbsp;";
                                        }elseif(($harga->stapv2==2)){$st2="<a href='#' data-toggle='tooltip' data-placement='top' title='Rejected $harga->tgapv1'><i class='fa fa-times fa-lg' style='color:red'></i></a>&nbsp;";
                                        } 

                                        if($harga->stapv3==1){$st3="<a href='#' data-toggle='tooltip' data-placement='top' title='Approved $harga->tgapv1'><i class='fa fa-check fa-lg' style='color:green'></i></a>&nbsp;";
                                        }elseif(($harga->stapv3==2)){$st3="<a href='#' data-toggle='tooltip' data-placement='top' title='Rejected $harga->tgapv1'><i class='fa fa-times fa-lg' style='color:red'></i></a>&nbsp;";
                                        } 
                                        echo "<sup><small>S : ".$st1."<br> B : ".$st2."<br> D : ".$st3."</small></sup><br>";
                                    }

                            ?> 
                            </td>
                            <td><?php  echo $harga->al_custo; ?></td>
                            <td><?php if($harga->hg_bulanan==""):?><img src="<?php echo base_url('/assets/images/baru.gif') ?>" height="20px"><?php endif;?>&nbsp;<?php  echo $harga->request.", media : ".$harga->media.", service : ".$harga->service.", ";

                                if($harga->ix==0){}else{echo "IX : ".$harga->ix." Mbps";} 
                                if($harga->iix==0){}else{echo "IIX : ".$harga->iix." Mbps";}
                                if($harga->mix==0){}else{echo "MIX : ".$harga->mix." Mbps";}
                                if($harga->llo==0){}else{echo "LOCALLOOP : ".$harga->llo." Mbps";}
                                if($harga->colo==''){}else{echo ", Colocation : ".$harga->colo;}
                                if($harga->tele==0){}else{echo ", Telephony : ".$harga->tele." Line(s)";}
                                if($harga->ket==''){}else{echo "<br> <b>Remark :</b> ". strip_tags(substr($harga->ket,0,50))."<a href='"; echo base_url("/panel/hargadetil/").$harga->id_rha."' class='btn btn-primary btn-sm'><small> Detil ...</small></a><br> ";}
                                if($harga->lampiran==''){ 

                                }else{ 

                            if($harga->lampiran != null):
                            $lampiran = json_decode($harga->lampiran);
                            ?>
                                <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <a href="<?php echo base_url("assets/uploads/lampiran/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>"><?php echo $item->judul; ?></a>
                                <?php endforeach; ?> 
                            
                             <?php endif;?>



                             <?php  }

                              
                            ?></td>
                            <td class="text-right"><?php  echo number_format($harga->hg_instalasi); ?></td>
                            <td class="text-right"><?php  echo number_format($harga->hg_bulanan); ?> </td>
                            <td class="text-center"> 

                            <?php if($nmjab=='Presales'):?>
                                                                       
                                   <?php if($harga->msfile==''): ?> 

                                    <!--<a href="<?php //echo base_url($this->uriku."/msdquo/".$harga->id_rha); ?>"class="btn btn-info" data-toggle="tooltip" title="Upload Quotation"><i class="fa fa-upload fa-lg"></i></a>-->
                                   

                                   <?php else: 

                                        $lampiran = json_decode($harga->msfile); ?>

                                        <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                        
                                        <?php foreach($lampiran as $item):
                                        $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                        ?>
                                        <a href="<?php echo base_url("assets/uploads/quo/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>"><?php echo $item->judul; ?></a>
                                        <?php endforeach; ?> 

                                        <small><br><?php echo $harga->msnama; ?></small>
                            
                                    <?php endif;?>

                            <?php elseif($nmjab=='Sales'):?>

                                <?php if($harga->msfile!=''){echo "<a href='#' data-toggle='tooltip' title='Quotation MS Completed'><i class='fa fa-check-circle fa-lg text-success'></i></a>";}else{echo "-";} ?>
                                <?php endif;?></td>

                            <td><small><b><?php echo $harga->hg_catatan;?></b><br>
                            <?php if($harga->lampirana==''){ 

                                }else{ 

                            if($harga->lampirana != null):
                            $lampiran = json_decode($harga->lampirana);
                            ?>
                                <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <a href="<?php echo base_url("assets/uploads/lampiran/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>"><?php echo $item->judul; ?></a>
                                <?php endforeach; ?> 
                            
                             <?php endif; }?></small>

                            </td>
                            <td class="text-center">
                                <?php if($nmjab=='Presales Manager' || $nmjab=='Presales'):?> 

                                    <?php if($this->session->userdata('username')=='ratih' || $this->session->userdata('username')=='awisnun' || $this->session->userdata('username')=='maris' ): ?>
                                        <button data-id="<?php echo $harga->id_rha; ?>"  onclick="$('#dataid').text($(this).data('id')); document.getElementById('dataidv').value=$(this).data('id'); $('#hargaedit').modal('show');">&nbsp;<?php echo $harga->hg_assignby;?>&nbsp;</button>

                                    <?php elseif($harga->hg_assignby=='rizkip' || $this->session->userdata('username')=='maris'): ?>
                                        <button data-id="<?php echo $harga->id_rha; ?>"  onclick="$('#dataid').text($(this).data('id')); document.getElementById('dataidv').value=$(this).data('id'); $('#hargaedit').modal('show');">&nbsp;<?php echo $harga->hg_assignby;?>&nbsp;</button>
                                    <?php endif; ?> 
                                    <br><br><a href="<?php if(!empty($harga->request)){$p=1; echo base_url("panel/vbpl/".$harga->id_rha);}else{$p=0; echo base_url("panel/vbpb/".$harga->id_rha);}  ?>" class="btn btn-success"><small style="color: white"><?php if(empty($harga->hg_assignby)){}else{echo "<sup><small>PRA </small></sup>BP <br>";} ?></small></a>

                                    <?php if($harga->hg_bulanan!=0 ): ?>

                                        &nbsp;<a class="catrum" type="button" data-id="<?php echo $harga->id_rha ?>" data-toggle='modal'><i class="fa fa-comment-o fa-lg"></i></a>&nbsp;

                                    <?php if(lascat($harga->id_rha,$this->usrku)!=$this->usrku):?><img src="<?php echo base_url('/assets/images/baru.gif') ?>" height="20px"><?php endif;
                                    
                                    ?>

                                    &nbsp;<a href="#" data-toggle='tooltip' data-html='true' title="<?php $dc= tilcat($harga->id_rha); if(empty($dc)){echo "No Chat";}else{foreach($dc as $t){ echo '<b>'.substr($t->nama,0,8).'</b>...<br>'.$t->chat.'<br><small>'.date('d M y H:i',strtotime($t->tgchat)).'</small><br><br>';}}?>">...</a>

                                    <?php endif; ?> 

                                <?php else: ?> 
                                    <label> <?php echo $harga->hg_assignby ?></label> 

                                    &nbsp;<a class="catrum" type="button" data-id="<?php echo $harga->id_rha ?>" data-toggle='modal'><i class="fa fa-comment fa-lg"></i></a>
                                    <?php if(lascat($harga->id_rha,$this->usrku)!=$this->usrku):?><img src="<?php echo base_url('/assets/images/baru.gif') ?>" height="20px"><?php endif;
                                    
                                    ?>
                                    &nbsp;<a href="#" data-toggle='tooltip' data-html='true' title="<?php $dc= tilcat($harga->id_rha); if(empty($dc)){echo "No Chat";}else{foreach($dc as $t){ echo '<b>'.substr($t->nama,0,8).'</b>...<br>'.$t->chat.'<br><small>'.date('d M y H:i',strtotime($t->tgchat)).'</small><br><br>';}}?>">...</a>
                                <?php endif; ?>
                                
                                <?php //if($nmjab=='Presales'):?> 
                                <!--<a href="<?php //if(!empty($harga->request)){echo base_url("panel/hargabpl/".$harga->id_rha);}else{echo base_url("panel/hargabpb/".$harga->id_rha);} ?>" class="btn btn-info btn-sm"><small><?php //if(!empty($harga->request)){echo "BP LINK";}else{echo "BP BARG";} ?></small></a>-->
                                <?php //endif; ?>
                                
                            </td>
                            <td><center><?php if($harga->tg_jwb==''):?><?php if($nmjab=='Presales Manager' || $nmjab=='Presales'):?><a href="<?php if(!empty($harga->request)){$p=1;}else{$p=0;} echo base_url("panel/hargatamb/".$p.'/'.$harga->id_rha); ?>" class="btn btn-info"><i class="fa fa-reply fa-lg"></i><?php else :?><?php endif; ?></a><?php else :?><?php  echo date("d-M-y H:i:s",strtotime($harga->tg_jwb)); ?><?php endif; ?>

                            </center></td>
                            <td><?php $sisa=$hours%24; $hari=($hours-$sisa)/24; echo "<small>".$hari." hari ".$sisa." jam </small>"; if($harga->hg_edit>0) {?>&nbsp;<sup><span class="badge"><small><?php echo $harga->hg_edit;?></small></span></sup><?php }else{} ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                   </table>
                  </div>
                </div>
            </div>
        </div> 

<!-- Modal -->
<div class="modal" id="catrum" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Chat Zone <?= "<b id='idno'></b>"; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
<?php 



?>

  <form action="/chat/kirim" method="POST">
      <div class="modal-body">

        <input type="hidden" name="id_rha" class="id">
        <input type="text" name="chat" class="form-control" placeholder="Ketik chat disini">
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Kirim</button>
        <button type="submit" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
    </div>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    $(document).on('click', '.catrum', function() {
        var idx = $(this).data("id");
        $('.id').val(idx);
        document.getElementById("idno").innerHTML = idx;
        $('#catrum').modal('show');
    });
</script>
 
