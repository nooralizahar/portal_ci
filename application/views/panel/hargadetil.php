<?php $sales=$this->session->userdata("username")?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/hargatamb/"); ?>">Lihat Detil</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Detil Permintaan Harga</div>
                <div class="panel-body">




           
                        <div class="col-md-12">
                            <table>
                            <tr><td >Pelanggan </td><td>&nbsp;:&nbsp;</td> <td style="color:blue"><?php echo $dafrid->nm_custo; ?></td></tr>
                            <tr><td>Site      </td><td>&nbsp;:&nbsp;</td> <td style="color:blue"><?php echo $dafrid->al_custo; ?></td></tr>
                            </table>
                            <hr/>
                            <table>
                            <tr><td><label>Kepada </td><td>&nbsp;:&nbsp;</td> <td></label><?php echo $dafrid->penerima; ?></td></tr>
                            <tr><td><label>Sales </td><td>&nbsp;:&nbsp;</td> <td></label><?php echo $dafrid->nama_lengkap; ?></td></tr>
                            <tr><td><label>Request </td><td>&nbsp;:&nbsp;</td> <td></label><?php echo $dafrid->request; ?></td></tr>
                            <tr><td><label>Media </td><td>&nbsp;:&nbsp;</td> <td></label><?php echo $dafrid->media; ?></td></tr>
                            <tr><td><label>Service </td><td>&nbsp;:&nbsp;</td> <td></label>
                            <?php echo $dafrid->service." &rarr; "; 
                                if($dafrid->ix==0){}else{echo "IX : ".$dafrid->ix." Mbps";} 
                                if($dafrid->iix==0){}else{echo "IIX : ".$dafrid->iix." Mbps";}
                                if($dafrid->mix==0){}else{echo "MIX : ".$dafrid->mix." Mbps";}
                                if($dafrid->llo==0){}else{echo "LOCALLOOP : ".$dafrid->llo." Mbps";}
                                if($dafrid->colo==''){}else{echo ", Colocation : ".$dafrid->colo;}
                                if($dafrid->tele==0){}else{echo ", Telephony : ".$dafrid->tele." Line(s)";}
                                

                            ?></td></tr>
                            <tr><td><label>NMC </td><td>&nbsp;:&nbsp;</td> <td></label><?php if($dafrid->nmc==''){}else{echo $dafrid->nmc;} ?></td></tr>
                            <tr><td><label>Remark </td><td>&nbsp;:&nbsp;</td> <td></label><?php if($dafrid->ket==''){}else{echo "<br>".$dafrid->ket;} ?></td></tr>

                            <tr><td><label>Lampiran</label></td><td>:</td>
                                <td>
                            <?php if($dafrid->lampiran==''){ 

                                }else{ 

                            if($dafrid->lampiran != null):
                            $lampiran = json_decode($dafrid->lampiran);
                            ?>
                                <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b>
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <a href="<?php echo base_url("assets/uploads/lampiran/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>"><?php echo $item->judul; ?></a>
                                <?php endforeach; ?> 
                            
                             <?php endif;?>



                             <?php  } ?>

                                </td>
                            </tr>
                            </table>
                        </div>

                        <button onclick="kembali()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>

                
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script type="text/javascript">
    $(document).ready(function(){
    
    $(".attach-img").magnificPopup({
            type: "image"
        });

    $("#upload_md").fileinput({'showUpload':false, 'previewFileType':'any'});

        $(function(){
            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                return false;
            });
        })


        setTimeout(function(){
            $('[data-toggle="tooltip"]').tooltip();
        },1000);

        $(".star.fa-star").on("click",function(){
            var curStar = $(this);
            update_star(curStar);
            curStar.removeClass("fa-star").addClass("fa-spinner fa-pulse");
        });

    });



</script>

