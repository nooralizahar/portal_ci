
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
        <li class="active">Kirim DiskuC</li>
    </ol>
</div><!--/.row--><br />

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Kirim DiskuC</div>
            <div class="panel-body">
                
                <table data-toggle="table" data-url="<?php //echo base_url("panel/json_inbox");?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="waktu_kirim" data-sortable="true">Waktu Kirim</th>
                        <th data-field="instruksi_disposisi"  data-sortable="true">Perihal</th>
                        <th data-field="penerima">Partisipan</th>
                        

                        <th>Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($diskusix as $d): ?>
                        <tr>
                            <td><?php echo date("d-M-y H:m",strtotime($d->wk_kirm)); ?></td>
                            <td><?php echo $d->perihal; ?></td>
                            <td>
                                <?php
                                    foreach($d->penerima as $key=>$p):
                                        $key += 1;
                                        if($p->dibaca == 1)
                                            echo "<p data-toggle='tooltip' data-placement='left' title='Dibaca' style='padding: 5px;background-color: rgba(52, 152, 219, 0.76); color: white;'><b>" . $key . " . $p->nama_lengkap <i class='fa fa-eye fa-lg fa-fw'></i></b></p> ";
                                        else
                                            echo "<p data-toggle='tooltip' data-placement='left' title='Terkirim' style='padding: 5px;background-color: rgba(241, 196, 15,.76); color: white;'><b>" . $key . " . $p->nama_lengkap </b></p>";
                                    endforeach;
                                ?>
                            </td>
                            

                            <td>
                                <a href="<?php echo base_url("panel/disxcarim/" . $d->id_disx . "/" . $d->ko_disx); ?>" class="btn btn-success"><i class="fa fa-envelope-open fa-lg"></i><br><small><sup>BACA</sup></small></a>
<!--                                <a href="#" class="btn btn-warning">Hapus</a>-->
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div><!--/.row-->

</div><!--/.main-->
