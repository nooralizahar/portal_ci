﻿<?php
    $thnini= date('y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    $thnblu= date('ym',strtotime("-1 month"));
    $blnini= date('Y-m'); //2020-01
    $blnlal= date('Y-m',strtotime("-1 month")); 

    $x=$this->db->order_by("custno","desc")->limit(1)->get("radcheck")->result();
        foreach($x as $dat): 
        $dupd=$dat->registerdate;   
        endforeach; 
    $rhrg=$this->db->order_by("tg_buat","desc")->limit(1)->get("ts_agrah")->result();
        foreach($rhrg as $dat): 
        $updrhrg=$dat->tg_buat;   
        endforeach; 
    $idwo=$this->db->order_by("id_worde","desc")->limit(1)->get("tp_owatad")->result();
        foreach($idwo as $dat): 
        $updidwo=$dat->tg_issue;   
        endforeach; 

?>
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"> Rekapitulasi Permintaan Harga</a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-3">
        <?php 
            echo "<small>Data Terakhir : ". date("d-m-Y", strtotime($updrhrg))."</small>";
         ?></div>
        </div>
        <hr/>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar blank"><use xlink:href="#stroked-calendar-blank"></use></svg>
                        <small><?php echo $blnini; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/rhrg/$blnini"); ?>"><?php echo $this->db->where("tg_buat like ",$blnini."%")->get("ts_agrah")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan ini </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar blank"><use xlink:href="#stroked-calendar-blank"></use></svg>
                        <small><?php echo $blnlal; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/rhrg/$blnlal"); ?>"><?php echo $this->db->where("tg_buat like ",$blnlal."%")->get("ts_agrah")->num_rows(); ?></a></div>
                        <div class="text-muted">Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-blue panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>
                        <small><?php echo "20".$thnini; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/rhrg/20$thnlal"); ?>"><?php echo $this->db->where("tg_buat like ","20".$thnini."%")->get("ts_agrah")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-blue-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>
                        <small><?php echo "20".$thnlal; ?></small>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/rhrg/20$thnlal"); ?>"><?php echo $this->db->where("tg_buat like ","20".$thnlal."%")->get("ts_agrah")->num_rows(); ?></a></div>
                        <div class="text-muted">Tahun Lalu</div>
                    </div>
                </div>
            </div>
        </div> 
<hr/>

<div style="padding: 70px 30px;" class="container col-xs-12">

  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Total per Sales</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body panel-primary">
            <!--<h3>Data Belum Tersedia</h3>-->
            <div class="panel-body">
            <div class="form-group"> 

                    <div class="col-md-5">      
                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="3"><center><h3>BULAN INI</h3></center></th></tr>
                            <tr>

                                <th>No.</th>
                                <th>Nama Sales</th>
                                <th><center>Jumlah Req.</center></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $daf=$persal;
                            $no=1;
                            foreach($daf as $harga): ?>
                                <tr >
                                    <td align="right"><?php echo $no++; ?> </td>
                                    <td><?php echo $harga->nama_lengkap; ?> </td>
                                    <td align="center"><?php echo $harga->jmlreq; ?> </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>
                    </div>

            
                    <div class="col-md-2">
                    </div>

                    <div class="col-md-5">      
                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="3"><center><h3>BULAN LALU</h3></center></th></tr>
                            <tr>
                                <th align="center">No.</th>
                                <th>Nama Sales</th>
                                <th><center>Jumlah Req.</center></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $daf=$persall;
                            $no=1;
                            foreach($daf as $harga): ?>
                                <tr >
                                    <td align="right"><?php echo $no++; ?> </td>
                                    <td><?php echo $harga->nama_lengkap; ?> </td>
                                    <td align="center"><?php echo $harga->jmlreq; ?> </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>
                    </div>
            </div>
            </div>

        </div>
      </div>
    </div>
    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title" >
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Total per Segment</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body"><!--<h3>Data Belum Tersedia</h3>-->
                        <div class="form-group"> 

                    <div class="col-md-5">      
                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="3"><center><h3>BULAN INI</h3></center></th></tr>
                            <tr>

                                <th>No.</th>
                                <th>Segment</th>
                                <th><center>Jumlah Req.</center></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $daf=$persegi;
                            $no=1;
                            foreach($daf as $harga): ?>
                                <tr >
                                    <td align="right"><?php echo $no++; ?> </td>
                                    <td><?php echo $harga->segment; ?> </td>
                                    <td align="center"><?php echo $harga->jmlreq; ?> </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>
                    </div>

            
                    <div class="col-md-2">
                    </div>

                    <div class="col-md-5">      
                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="3"><center><h3>BULAN LALU</h3></center></th></tr>
                            <tr>
                                <th align="center">No.</th>
                                <th>Segment</th>
                                <th><center>Jumlah Req.</center></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $daf=$persegl;
                            $no=1;
                            foreach($daf as $harga): ?>
                                <tr >
                                    <td align="right"><?php echo $no++; ?> </td>
                                    <td><?php echo $harga->segment; ?> </td>
                                    <td align="center"><?php echo $harga->jmlreq; ?> </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>
                    </div>
            </div>


        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title" >
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Summary per Sales</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body"><!--<h3>Data Belum Tersedia</h3>-->

            <center><h2><u>RANGKUMAN PERMINTAAN HARGA</u></h2></center>
            <?php $this->load->view("/panel/rptcom/sumreq")?>

        </div>
      </div>
    </div>

    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title" >
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Summary Presales</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
        <div class="panel-body"><!--<h3>Data Belum Tersedia</h3>-->



            <center><h2><u>RANGKUMAN PENENTUAN HARGA</u></h2></center>

                <div class="row">
                <form class='add-item' method=POST action=''>
                        <div class='form-group row'>
                                <label for='lbltgl' class='col-sm-1 form-control-label'>&nbsp;&nbsp;&nbsp;&nbsp;Tahun</label>
                                <div class='col-sm-2'>
                                <select name="tgl" id="tgl" class="form-control" >
                                                <?php foreach($pilhun as $z):?>
                                                <option value="<?php echo $z->thn; ?>" <?php echo ($tgl == $z->thn) ? "selected" : ""; ?>><?php echo $z->thn;?></option>
                                                <?php endforeach;?>
                                </select>

                                </div>
                                <div class='col-sm-4'>
                                    <button type="submit" name="btnGen" class="btn btn-success"><i class="fa fa-eye fa-lg"></i> LIHAT
                                    </button>   
                                </div>
                        </div>       
                </form>
   
                </div>

            <?php $this->load->view("/panel/rptcom/sumpre")?> 


           
        </div>
      </div>
    </div>



    <div class="panel panel-primary">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">Lainnya</a>
        </h4>
      </div>
      <div id="collapse9" class="panel-collapse collapse">
        <div class="panel-body">

            
            <?php //$this->load->view("/panel/ompunem/infowobl")?> 
            <br><br>
            <center><h2><u>RANGKUMAN LAINNYA</u></h2></center>
            <?php //$this->load->view("/panel/rpteng/sumreq")?>

        </div>
      </div>
    </div>


  </div> 
</div>

</div> <!--/.row -->
   
