
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("khusus/"); ?>"> Inventory</a></li>
            <li class="active"> Goods In</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succsp"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data supplier berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Barang Masuk</div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                    	<div class="form-group">
                            <div class="col-md-12">
                            
                            <div class="alert alert-warning text-center"> DATA BARANG MASUK </div>
                            
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8">
                            <label for="">Nama Supplier </label>
                            <select name="supp" class="form-control vendor">

                                <?php foreach($dsup as $d):?>
                                    <option value="<?php echo $d->sid; ?>"> <?php echo $d->snama;?></option>
                                <?php endforeach;?>
                            </select>
                            </div>
                            <div class="col-md-2">
                            <label><i class="fa fa-plus-square-o"></i></label>
                            
                            <a href="spbaru/" class="btn btn-success form-control" ><i class="fa fa-plus-square-o"></i> Supplier</a> 
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">
                            <label>Jenis Barang</label>
                            <select name="jbrg" class="form-control status">
                                <option value="Adapter"> Adapter</option>
                                <option value="Attenuator"> Attenuator</option>
                                <option value="Kabel"> Kabel</option>
                                <option value="Konektor"> Konektor</option>
                            </select>
                            </div>
                            <div class="col-md-9">
                            <label for="">Deskripsi</label>
                            <input type="text" name="desk" class="form-control" required/>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                            <label for="">Jumlah</label>
                            <input type="text" name="juml" class="form-control">
                            </div>
                            <div class="col-md-3">
                            <label>Satuan</label>
                            <select name="unit" class="form-control status">
                                <option value="pcs"> pcs</option>
                                <option value="meter"> meter</option>
                            </select>
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                        <center>
                        <button type="submit" name="btnSimpan" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button></center>
                        <!-- <input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script>


    $(document).ready(function(){

        $(".segment").select2();
        $(".vendor").select2();
        //$(".orderby").select2();
        //$(".status").select2();

    })
</script>