<?php
    $thnini= date('Y');
    $thnlal= $thnini-1;
    $thnbli= date('ym');
    if(substr($thnbli, 2)==01){$bln=12;}else{$bln='0'.substr($thnbli, 2)-1;}
    $thnblu= ((substr($thnbli,0, 2))-1).$bln;
    $blnini=date('Y/m'); //2020-01
    $blnlal=date('Y-m',strtotime("-1 month")); 

        //$tik=$this->db->order_by("RowID","desc")->limit(1)->get("t_datame")->result();
        //foreach($tik as $d): 
        //$lastik=$d->Created_Time;   
        //endforeach;
?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("inventory/"); ?>"> Inventory</a></li>
            <li class="active"> Goods In</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5> Persediaan Masuk</h5>
                </div>
        <div class="panel-body">
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menambah data <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#requester" aria-controls="requester" role="tab" data-toggle="tab">Goods In</a></li>
                            <!--<li role="presentation"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Problem Side</a></li>-->
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="requester">
                                <div class="panel-body">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
            <a href="<?php echo base_url("inventory/ingood"); ?>" class="btn btn-success"><i class="fa fa-plus-circle fa-lg"></i> <br><small><sup>BARU</sup></small></a>
            <a href="<?php echo base_url("inventory/igexp"); ?>" class="btn btn-success"><i class="fa fa-file-excel-o fa-lg"></i> <br><small><sup>EXCEL</sup></small></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="problem">
                                <div class="panel-body">
                                    <div class="panel panel-primary">
                                        <div class="panel-body">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>          

<div class="row">
       <!-- <ol class="breadcrumb">
          <li><a href="#"><?php 
            //echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></a></li>

        </ol> 
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-4">  </div>
        </div>
        <hr/> -->
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_ksmngarab")->num_rows()); ?></a></div>
                        <div class="text-muted"> Total Items</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnlal"); ?>"><?php //echo number_format($this->db->where('stat','0')->get("t_ksmngarab")->num_rows()); ?></a></div>
                        <div class="text-muted"> A</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php //echo number_format($this->db->where('stat','Inactive')->get("t_nanaggnal")->num_rows()); ?> </a></div>
                        <div class="text-muted"> B</div>
                    </div>
                </div>
            </div>
        </div>

</div> 


    <br>



        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true">Jenis Barang</th>
                        <th data-field="b"  data-sortable="true">Uraian</th>
                        <th data-field="c"  data-sortable="true">Jumlah</th>
                        <th data-field="d"  data-sortable="true">Unit</th> 
                        <th data-field="e"  data-sortable="true">Supplier</th>            
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($vdsem as $dat): ?>
                        <tr>
                            <td><?php echo $dat->jbrg; ?></td>
                            <td><?php echo $dat->desk; ?></td>
                            <td><?php echo $dat->juml; ?></td>
                            <td><?php echo $dat->unit; ?></td>
                            <td><?php echo $dat->supp; ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>




        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

