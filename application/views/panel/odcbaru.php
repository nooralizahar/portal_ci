
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/dashit/"); ?>"> Akses IT</a></li>
            <li class="active"> ODC</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal tambah ODC !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> ODC berhasil disimpan ke tujuan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h5> ODC Baru</h5>
                </div>
        <div class="panel-body">
        <form class='add-item' method=POST action=''>
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Nama / Kode ODC</label>
                <div class='col-sm-10'>
                    <input type='text' name='nama' class='form-control' id='nama' required>
                </div>
          </div>  
         <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Alamat </label>
                <div class='col-sm-10'>
                    <input type='text' name='alamat' class='form-control' id='alamat' required>
                </div>
          </div> 
        <div class='form-group row'>
                <label for='lblkordinat' class='col-sm-2 form-control-label'>Kordinat</label>
                <div class='col-sm-10'>
                      <input type='text' name='kordinat' class='form-control' id='kordinat' required>
                </div>
                
        </div>    
        <div class='form-group row'>
                <label for='lblkapasitas' class='col-sm-2 form-control-label'>Kapasitas</label>
                <div class='col-sm-10'>
                       <input type='text' name='kapasitas' class='form-control' id='kapasitas' required>
                </div>
        </div>
            
                  
                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('/panel/dashit') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>

<div class="row">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo $this->db->get("t_odc")->num_rows(); ?></div>
                        <div class="text-muted">Jumlah</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-red panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                    <svg class="glyph stroked database"><use xlink:href="#stroked-database"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><?php $this->db->select_sum('kapasitas'); $hasil= $this->db->get("t_odc")->row(); $y=$hasil->kapasitas; echo $y; ?></div>
                        <div class="text-muted">Kapasitas</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-blue panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $this->db->select_sum('sisa'); $hasil= $this->db->get("v_t_odc")->row(); $y=$hasil->sisa; echo $y;?></div>
                        <div class="text-muted">Tersedia</div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <br>



        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="odc"  data-sortable="true">Nama ODC</th>
                        <th data-field="Alamat"  data-sortable="true">Alamat</th>
                        <th data-field="kordinat"  data-sortable="true">Kordinat</th>
                        <th data-field="kapasitas"  data-sortable="true">Kapasitas</th> 
                        <th data-field="sisa"  data-sortable="true">Sisa</th>            
                        <th>Pilihan</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($datait_odc as $datodc): ?>
                        <tr>
                            <td><?php echo $datodc->nama; ?></td>
                            <td><?php echo $datodc->alamat; ?></td>
                            <td><a href="<?php echo base_url("panel/petalokasi/".$datodc->idodc); ?>" target='_blank'><?php echo $datodc->kordinat; ?></a></td>
                            <?php //echo base_url("panel/disxcarim/" . $d->id_disx . "/" . $d->ko_disx); ?>
                            <td align="right"><?php echo $datodc->kapasitas; ?></td>
                            <td align="right"><?php echo $datodc->sisa; ?></td>
                            <td>
                                <a href="<?php echo base_url("panel/odcedit/".$datodc->idodc); ?>" ><i class="fa fa-pencil fa-lg"></i></a>&nbsp; <!-- Edit Pengguna -->
                                <a href="<?php echo base_url("panel/odcedit/".$datodc->idodc); ?>" ><i class="fa fa-plus fa-lg"></i></a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>

        </div>
      </div>
    </div>

    </div><!--/.row end-->






    

    <form action="" method="post" style="display: none;">
        <input type="hidden" name="password" id="confirmPassword">
        <input type="submit" name="selesaiBtnSubmit" id="selesaiBtnSubmit">
    </form>

</div>	<!--/.main-->

<script>
    $(document).ready(function(){
        $(".attach-img").magnificPopup({
            type: "image"
        });


        $("#isi_disposisi").shorten({
            moreText: "Selengkapnya",
            lessText: "Kecilkan",
            showChars: 300
        });

        $("#attach").fileinput({'showUpload':false});

        $(".attach").on("click",function(ev){
            var data_file = $(this).data().file;
            var content = "<ol>";

            $.each(data_file,function(key,item){
                var cls = item.file.split(".").length;
                var tipe_gambar = ["jpg","png","jpeg","bmp","gif"];
                cls = ($.inArray(item.file.split(".")[cls-1],tipe_gambar) == -1) ? "attach-doc" : "attach-img";
                content += "" +
                    "<li><a class='"+cls+"' href='"+BASE_URL+"assets/uploads/lampiran/follow_up_disposisi/"+item.file+"'>"+item.judul+"</a></li>";
            });

            content += "</ol>";

            bootbox.alert({
                size: "normal",
                title: "Lampiran Tanggapan",
                message: content
            });


            $(".attach-img").magnificPopup({
                type: "image"
            });


            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                $("#myModal").css("z-index",2000);
                return false;
            });


            ev.preventDefault();
        });


        $("#selesai").on("click",function(ev){

            swal({
                title: "Konfirmasi Penyelesaian",
                text: "Masukkan password anda untuk mengkonfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword").val(input);
                $("#selesaiBtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });


        $(function(){
            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                return false;
            });
        })
    });
</script>