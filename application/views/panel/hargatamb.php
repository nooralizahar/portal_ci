<?php $sales=$this->session->userdata("username")?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/hargatamb/"); ?>">Tambah Harga</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Form Tambah Harga</div>
                <div class="panel-body">
				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal tambah harga! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Tambah harga berhasil disimpan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                  



                <form action="" method="POST" enctype="multipart/form-data" onsubmit="FormSubmit(this);">
                        <div class="col-md-12">
                            <label>Untuk Presales : &nbsp;</label><?php echo $dafrid->penerima; ?><br>
                            <label>Dari Sales : &nbsp;</label><?php echo $dafrid->sales; ?><br>
                            <label>Request : &nbsp;</label><?php echo $dafrid->request; ?><br>  
                            <label>Media : &nbsp;</label><?php echo $dafrid->media; ?><br>
                            <label>Service : &nbsp;</label>
                            <?php echo $dafrid->service." &rarr; "; 
                                if($dafrid->ix==0){}else{echo "IX : ".$dafrid->ix." Mbps";} 
                                if($dafrid->iix==0){}else{echo "IIX : ".$dafrid->iix." Mbps";}
                                if($dafrid->mix==0){}else{echo "MIX : ".$dafrid->mix." Mbps";}
                                if($dafrid->llo==0){}else{echo "LOCALLOOP : ".$dafrid->llo." Mbps";}
                                if($dafrid->colo==''){}else{echo ", Colocation : ".$dafrid->colo;}
                                if($dafrid->tele==0){}else{echo ", Telephony : ".$dafrid->tele." Line(s)";}
                                

                            ?><br>
                            <label style="color: red"><b>NMC : </b></label><?php if($dafrid->nmc==''){}else{echo "<label style='color: red'> &nbsp;".$dafrid->nmc."</label>";} ?><br>
                            <label><b>Remark : </b></label><?php if($dafrid->ket==''){}else{echo "<br>".$dafrid->ket;} ?><br>
                        </div>
                        <div class="col-md-6">
                            <label>Biaya Instalasi (Rp.)</label>
                            <input type="hidden" name="id_rha" class="form-control" value="<?php  echo $dafrid->id_rha; ?>" >
                            <input type="text" name="hg_instalasi" placeholder="Ketik disini dalam Rupiah" class="form-control" value="<?= $dafrid->hg_instalasi  ?>" >
                        </div>
                        <div class="col-md-6">
                            <label>Biaya Bulanan (Rp.) </label>
                            <input type="text" name="hg_bulanan" placeholder=" Ketik disini dalam Rupiah" class="form-control" value="<?= $dafrid->hg_bulanan  ?>" >
                            <input type="hidden" name="hg_assignby" class="form-control" value="<?php  echo $this->session->userdata("username"); ?>" >
                            <input type="hidden" name="tg_jwb" class="form-control" value="<?php  $tgljam= date('Y-m-d H:i:s'); echo $tgljam; ?>" >
                        </div>
                        <div class="col-sm-12">
                        <!--<label>Catatan </label>
                        <input type="text" class="form-control" name="hg_catatan" placeholder="Ketik Catatan Disini Jika Ada" value=""> -->
                        <label>Catatan  </label>
                                
                                <input type="hidden" name="hg_catatan" class="form-control" />
                                <select name="hg_catatan_ddl" onchange="DropDownChanged(this);" class="form-control" />

                                    
                                    <option value="Harga perangkat / barang belum termasuk PPN">Harga perangkat / barang belum termasuk PPN</option>
                                    <option value="Garansi 12 bulan (diluar force majour)">Garansi 12 bulan (diluar force majour)</option>
                                    <option value="Delivery Jabodetabek">Delivery Jabodetabek</option>
                                    <option value="Penawaran berlaku 14 hari">Penawaran berlaku 14 hari</option>
                                    <option value="RFS maksimal 14 hari setelah PO diterima">RFS maksimal 14 hari setelah PO diterima</option>
                                    <option value="DP 50% dan pelunasan 50% setelah perangkat / barang diterima">DP 50% dan pelunasan 50% setelah perangkat / barang diterima</option>
                                    <option value="Tidak ada mitra yang cover, semua terhalang gedung tinggi">Tidak ada mitra yang cover, semua terhalang gedung tinggi</option>
                                    <option value="RFS 7 hari setelah survey dan PO ; Di luar biaya perijinan">RFS 7 hari setelah survey dan PO ; Di luar biaya perijinan</option>
                                    
                                    
                                    <option value="">Lainnya..</option>
                                </select> <input type="text" name="hg_catatan_txt" style="display: none;" class="form-control" placeholder="Ketik catatan lainnya disini" />
                        </div>
                        <div>                            <input type="file" multiple id="attach" name="attach[]" class="form-control">
                            <small>format file : .xls, .xlsx. (maks. 2 MB)</small><br><br /></div>
                        <div class="col-sm-12">
                        <p></p>
                        <br><br/>
                        </div>
                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-paper-plane-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>

                        <button onclick="kembali()" class='btn btn-success'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>BATAL</small></sup></button>

                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('ket',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>


    $(document).ready(function(){
        $("#msg").froalaEditor({
            height: 300
        });

        $(".penerima").select2();
        $(".media").select2();
        $(".request").select2();
        $(".service").select2();

        $("#tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>

<script type="text/javascript">

function DropDownChanged(oDDL) {
    var oTextbox = oDDL.form.elements["hg_catatan_txt"];
    if (oTextbox) {
        oTextbox.style.display = (oDDL.value == "") ? "" : "none";
        if (oDDL.value == "")
            oTextbox.focus();
    }
}

function FormSubmit(oForm) {
    var oHidden = oForm.elements["hg_catatan"];
    var oDDL = oForm.elements["hg_catatan_ddl"];
    var oTextbox = oForm.elements["hg_catatan_txt"];
    if (oHidden && oDDL && oTextbox)
        oHidden.value = (oDDL.value == "") ? oTextbox.value : oDDL.value;
}

</script>

