
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/disxbaru/"); ?>">DiskuC Baru</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Buka DiskuC</div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">

                        <div class="col-md-12">
                            <label>Undang Partisipan</label>
                            <select name="penerima[]" class="form-control penerima" multiple required>
                                <?php foreach($daftar_pengguna as $divisi  => $group_pengguna): ?>
                                    <optgroup label="<?php echo $divisi ?>">
                                        <?php foreach($group_pengguna as $pengguna):?>
                                            <option value="<?php echo $pengguna->id_pengguna; ?>"><?php echo $pengguna->nama_lengkap; ?></option> 
                                        <?php endforeach;?>
                                    </optgroup>
                                <?php endforeach; ?>
                            </select>
                         <!-- . " &rarr; " . $pengguna->nm_jab

                           <?php echo (termasuk_penerima($pengguna->id_pengguna,$penerima_otomatis) || isset($_GET["pn"]) && $_GET["pn"] == $pengguna->id_pengguna) ? "selected" : ""; ?> 
                         -->

                        </div>
                        <div class="col-md-12">
                            <label>Perihal </label>
                            <input type="text" name="perihal" class="form-control" value="" required/><br />
                        </div>
                        <div class="col-md-12">
                            <label>Prolog DiskuC </label>
                            <textarea name="isi" id="ckeditor" ></textarea><br />
                            <label>Lampiran</label>
                            <input type="file" multiple id="attach" name="attach[]" class="form-control"><br />
                            <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-paper-plane-o fa-lg"></i><br><small><sup>KIRIM</sup></small></button>
                            <!--<input type="submit" name="btnSubmit" class="btn btn-success" value="Kirim"> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->


<!-- <div id="modal_manage" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <form action="<?php echo base_url("panel/tambah_penerima_otomatis/"); ?>" method="POST">

             Modal content
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Manage Pengguna Otomatis</h4>
                </div>
                <div class="modal-body">
                    <p><i>Pengguna yang dipilih di bawah ini akan otomatis tertera dalam form penerima surat saat membuat surat</i></p>
                    <p><b>Penerima</b></p>
                    <select style="width: 100%;" name="penerima[]" class="form-control penerima" multiple>
                        <?php foreach(array_merge($daftar_pengguna,$daftar_pengguna_sedinas) as $dinas => $group_pengguna): ?>
                            <optgroup label="<?php echo $dinas ?>">
                                <?php foreach($group_pengguna as $pengguna):?>
                                    <option value="<?php echo $pengguna->id_pengguna; ?>" <?php echo (termasuk_penerima($pengguna->id_pengguna,$penerima_otomatis)) ? "selected" : ""; ?>><?php echo $pengguna->nama_lengkap . ", " . $pengguna->nama_jabatan . " - " . $dinas; ?></option>
                                <?php endforeach;?>
                            </optgroup>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </div>
            </div>

        </form>

    </div>
</div> -->
<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('isi',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>
<script>
    $(document).ready(function(){
        $("#msg").froalaEditor({
            height: 300
        });
        $(".penerima").select2();
        $("#instruksi").select2({
            placeholder: "Pilih item",
            allowClear: true
        });
        $("#keamanan").select2({
            placeholder: "Pilih item",
            allowClear: true
        });
        $("#kecepatan").select2({
            placeholder: "Pilih item",
            allowClear: true
        })
        $("#tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>

<script src="<?php echo base_url("assets/js/compose.js"); ?>"></script>