
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/disxkirm/"); ?>">DiskuC Out</a></li>
            <li class="active">Detil Diskusi</li>
        </ol>
    </div><!--/.row-->

    <hr/>
<!-- <div class="row">
        <div class="col-lg-12" id="surat">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5>Awal Surat</h5>
                </div>
                <div class="panel-body">
                    <b><?php echo $disposisi->lampiran_surat->subjek; ?></b><br />
                    <i>Waktu kirim : <?php echo $disposisi->lampiran_surat->waktu_kirim; ?></i>
                    <p><?php echo $disposisi->lampiran_surat->isi_pesan; ?></p>

                    <?php
                    $lampiran = $disposisi->lampiran_surat->lampiran;
                    if($lampiran != null):
                        $lampiran = json_decode($lampiran);
                        ?>
                        <p>Lampiran:</p>
                        <ol>
                            <?php foreach($lampiran as $l):
                                $cls = (is_doc($l->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <a class="<?php echo $cls; ?>" href="<?php echo base_url("assets/uploads/lampiran/".$l->file); ?>" data-judul="<?php echo $l->judul; ?>"><li><?php echo $l->judul; ?></li></a>
                            <?php endforeach;?>
                        </ol>
                    <?php endif; ?>
                </div>
                <div class="panel-footer">
                    <button onclick="$.print('#disposisi')" class="btn btn-primary"><i class="fa fa-print fa-lg fa-fw"></i> Print Lembar Disposisi</button>
                    <button onclick="$.print('#surat')" class="btn btn-success"><i class="fa fa-print fa-lg fa-fw"></i> Print Lampiran Surat</button>
                </div>
            </div>
        </div>    

</div> -->


    <div class="row">
        <div class="col-lg-12" id="diskusi">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal mengirim respon! Coba lagi nanti <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Respon berhasil dikirim ke tujuan! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h5>Detail diskusi</h5>
                </div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                    <table class="table table-responsive">
                        <tr>
                            <td colspan="2"><b>Undangan</b><br>
                                        <div class="chat_list">
                                          <div class="chat_people">
                                            <div > <img src="<?php echo base_url("assets/images/")."usrapp.png"; ?>" alt="avatar" class="chat_img"> </div>
                                            <div class="chat_ib">
                                              <h5><?php echo $disposisi->perihal; ?> <span class="chat_date"><?php  echo $disposisi->wk_kirm; ?></span></h5>
                                              <p style="text-align: justify;"><?php echo $disposisi->isi; ?></p>
                                              <p style="text-align: justify;">Partisipan : <?php
                                foreach($disposisi->penerima as $key=>$p) {
                                    $key += 1;
                                    if($p->dibaca == 1)
                                        echo "&nbsp;<i class='fa fa-eye fa-lg fa-fw'></i>&nbsp;".$p->nama_lengkap;

                                    else
                                        echo "&nbsp;&nbsp;&nbsp;".$p->nama_lengkap;

                                }
                                ?></p>
                                            </div>
                                          </div>
                                       </div>
                            </td>
                            <td>
                                <?php
                                $lampiran = $disposisi->lampiran;
                                if($lampiran != null):
                                    $lampiran = json_decode($lampiran);
                                    ?>
                                    <tr>
                                        <td><b>Lampiran</b></td>
                                        <td>
                                            <ol>
                                                <?php foreach($lampiran as $l):
                                                    $ext = explode(".",$l->file);
                                                    $cls = (is_doc(end($ext))) ? "attach-doc" : "attach-img";
                                                    ?>
                                                    <a class="<?php echo $cls; ?>" href="<?php echo base_url("assets/uploads/lampiran/".$l->file); ?>" data-judul="<?php echo $l->judul; ?>"><li><?php echo $l->judul; ?></li></a>
                                                <?php endforeach;?>
                                            </ol>
                                        </td>
                                    </tr>
                                <?php endif;?>
                            </td>
                        </tr>



                        <tr>
                                <td colspan="2"><b>Tanggapan</b><br>
                                    <div id="followUp" style="max-height: 200px;overflow-x: scroll;">
                                        <?php
                                        if(empty($follow_up)):
                                            ?>
                                        <div class="chat_list">
                                          <div class="chat_people">
                                            <div > <img src="<?php echo base_url("assets/images/")."usrapp.png"; ?>" alt="avatar" class="chat_img"> </div>
                                            <div class="chat_ib">
                                              <h5>Sistem <span class="chat_date"><?php  $tgljam= date('Y-m-d H:i:s'); echo date("d-M-y H:m",strtotime($tgljam)); ?></span></h5>
                                              <p>Belum ada tanggapan untuk diskusi ini</p>
                                            </div>
                                          </div>
                                        </div>
                                            
                                        <?php else:?>


                                            <!-- <ul style="list-style-position: inside; margin: 0; padding: 0; list-style-type: square"> -->
                                            <?php foreach($follow_up as $fu):?>
                                                <?php if($fu->lampiran_follow_up == null): ?>
                                                       <!-- <li data-placement="left" data-toggle="tooltip" title="<?php echo $fu->waktu_kirim; ?>"> -->
                                        <div class="chat_list">
                                          <div class="chat_people">
                                            <div > <img src="<?php echo base_url("assets/images/")."usrapp.png"; ?>" alt="avatar" class="chat_img"> </div>
                                                <div class="chat_ib">
                                              <h5><?php echo ($fu->id_pengguna == $this->session->userdata("id_pengguna")) ? "Anda" : $fu->nama_lengkap; ?><span class="chat_date"><?php echo $fu->waktu_kirim; ?></span></h5>
                                                            <p style="text-align: justify;"><?php echo $fu->isi_follow_up; ?></p>
                                            </div>
                                          </div>
                                        </div>
                                                        <!-- </li> -->
                                                    <?php else:?>
                                        <div class="chat_list">
                                          <div class="chat_people">
                                            <div > <img src="<?php echo base_url("assets/images/")."usrapp.png"; ?>" alt="avatar" class="chat_img"> </div>
                                                <div class="chat_ib">
                                              <h5><?php echo ($fu->id_pengguna == $this->session->userdata("id_pengguna")) ? "Anda" : $fu->nama_lengkap; ?>
                                                    <a href="#" class="attach" data-file='<?php echo $fu->lampiran_follow_up; ?>'><b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b></a>

                                                    <span class="chat_date"><?php echo $fu->waktu_kirim; ?></span></h5>
                                                            <p style="text-align: justify;"><?php echo $fu->isi_follow_up; ?></p>
                                            </div>
                                          </div>
                                        </div>

                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            </ul>
                                        <?php endif;?>
                                    </div>
                                    <hr />
                                </td>
                            </tr>
                        
                        
                            <tr>
                                
                                <td colspan="2">
                                    <textarea name="isi_follow_up" class="form-control" placeholder="Ketik Tanggapan disini" required></textarea>
                                    <input type="file" name="lampiran_follow_up[]" id="attach" multiple>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><br><center><button class="btn btn-success" name="btnSubmit" type="submit"><i class="fa fa-send fa-lg fa-fw"></i> Kirim</button></center><br></td>
                            </tr>
                            <tr style="text-align: center">
                                <td colspan="2"><a id="selesai" class="btn btn-block btn-success" href="#"><i class="fa fa-check fa-lg"></i> Selesai</a></td>
                            </tr>
                        
                     </table>
                    </form>
                </div>
            </div>
        </div>




    </div><!--/.row end-->

    <form action="" method="post" style="display: none;">
        <input type="hidden" name="password" id="confirmPassword">
        <input type="submit" name="selesaiBtnSubmit" id="selesaiBtnSubmit">
    </form>

</div>	<!--/.main-->

<script>
    $(document).ready(function(){
        $(".attach-img").magnificPopup({
            type: "image"
        });


        $("#isi_disposisi").shorten({
            moreText: "Selengkapnya",
            lessText: "Kecilkan",
            showChars: 300
        });

        $("#attach").fileinput({'showUpload':false});

        $(".attach").on("click",function(ev){
            var data_file = $(this).data().file;
            var content = "<ol>";

            $.each(data_file,function(key,item){
                var cls = item.file.split(".").length;
                var tipe_gambar = ["jpg","png","jpeg","bmp","gif"];
                cls = ($.inArray(item.file.split(".")[cls-1],tipe_gambar) == -1) ? "attach-doc" : "attach-img";
                content += "" +
                    "<li><a class='"+cls+"' href='"+BASE_URL+"assets/uploads/lampiran/follow_up_disposisi/"+item.file+"'>"+item.judul+"</a></li>";
            });

            content += "</ol>";

            bootbox.alert({
                size: "normal",
                title: "Lampiran Tanggapan",
                message: content
            });


            $(".attach-img").magnificPopup({
                type: "image"
            });


            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                $("#myModal").css("z-index",2000);
                return false;
            });


            ev.preventDefault();
        });


        $("#selesai").on("click",function(ev){

            swal({
                title: "Konfirmasi Penyelesaian",
                text: "Masukkan password anda untuk mengkonfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword").val(input);
                $("#selesaiBtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });


        $(function(){
            $('.attach-doc').on('click',function(){
                var pdf_link = $(this).attr('href');
                var title = $(this).data().judul;
                var iframe = '<div class="iframe-container"><iframe src="http://docs.google.com/gview?url='+pdf_link+'&embedded=true"></iframe></div>'
                $.createModal({
                    title: title,
                    message: iframe,
                    closeButton:true,
                    scrollable:false,
                    link: pdf_link
                });
                return false;
            });
        })
    });
</script>