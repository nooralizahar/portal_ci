<?php
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
$blmbaca = $this->db->where("dibaca",0)->where("ke_user",$this->session->userdata("id_pengguna"))->get("ts_isuksidrel")->num_rows();
$blmbaca2 = $this->db->where("dibaca",0)->where("id_pengguna !=",$this->session->userdata("id_pengguna"))->where("id_disx",$this->uri->segment(3))->get("ts_isuksidres")->num_rows();
$allblmbaca=$blmbaca2+$blmbaca;

$divi= $this->db->where("id_div",$this->session->userdata("id_div"))->get("ts_isivid")->result();

foreach($divi as $dvs): 
    $nmdv=$dvs->nm_div; 
    $iddv=$dvs->id_div;
endforeach;

$idku=$this->session->userdata("id_pengguna");
$levku=$this->session->userdata("level");
$aksku=$this->session->userdata("mn_akses");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (isset($judul)) ? $judul : ""; ?></title>

    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-table.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/datepicker3.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-timepicker.min.css");?> type="text/css" />
    <link href="<?php echo base_url("assets/css/styles.css");?>" rel="stylesheet">

    <link rel="icon" href="<?php echo base_url("favicon.png"); ?>" type="image/png" sizes="14x5">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/css/fileinput.min.css">

    <!-- Include Editor style. -->
    <!--<link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_style.min.css' rel='stylesheet' type='text/css' />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <!--Icons-->
    <script src="<?php echo base_url("assets/js/lumino.glyphs.js");?>"></script>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url("assets/js/html5shiv.js");?>"></script>
    <script src="<?php echo base_url("assets/js/respond.min.js");?>"></script>
    <![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/js/fileinput.min.js"></script>


    <link rel="stylesheet" href="<?php echo base_url("assets/css/magnific-popup.css"); ?>">
    <script src="<?php echo base_url("assets/js/bootstrap-datetimepicker.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.magnific-popup.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jQuery.print.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/bootbox.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.shorten.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
    <!-- Include JS file. -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/js/froala_editor.min.js'></script>

    <script>
        BASE_URL = "<?php echo base_url(); ?>";
	    $.FroalaEditor.DEFAULTS.key = "bvA-21sD-16A-13ojmweC8ahD6f1n==";
    </script>

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>JLM</span>PORTAL</a>

            <ul class="user-menu">

                <li class="dropdown pull-right">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <?php echo $this->session->userdata("nama_lengkap"); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <!-- <li><a href="<?php echo base_url("panoc/profilku/").$idku; ?>"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profil</a></li> 
                        <li><a href="#" id="pengaturan"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Ganti Sandi</a></li>--> 
                        <li><a href="<?php echo base_url("panel/logout/"); ?>"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
<br>

    <ul class="nav menu">
<?php if($levku=="user" && $aksku=="admin" ):?>
        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>ADMIN</b>
        <li class="<?php echo $menu["dashboard"] ?>"><a href="<?php echo base_url("panoc/"); ?>"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
        <li class="<?php echo $menu["rokelol"] ?>"><a href="<?php echo base_url("panoc/rokelol/"); ?>"><svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>Kelola Role</a></li>

        <li class="<?php echo $menu["uskelol"] ?>"><a href="<?php echo base_url("panoc/uskelol/"); ?>"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Kelola User</a></li>
        <li class="<?php echo $menu["tikelol"] ?>"><a href="<?php echo base_url("panoc/tikelol/"); ?>"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Kelola Tiket</a></li>
        <li class="<?php echo $menu["timanua"] ?>"><a href="<?php echo base_url("panoc/tikelol/"); ?>"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Tiket Manual</a></li>
<?php endif; ?>

       <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>USER</b>
        <li class="<?php echo $menu["profil"] ?>"><a href="<?php echo base_url("panoc/profilvi/").$idku; ?>"><svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>Profil</a></li>

        <li class="<?php echo $menu["uprofil"] ?>"><a href="<?php echo base_url("panoc/profilku/").$idku; ?>"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Ubah Profil</a></li>
        <!-- <li class="<?php echo $menu["gasandi"] ?>"><a href="<?php echo base_url("panoc/profil/"); ?>"><svg class="glyph stroked lock"><use xlink:href="#stroked-lock"></use></svg> Ganti Sandi</a></li> -->
        
        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>TIKET</b>
        <li class="<?php echo $menu["tisemua"] ?>"><a href="<?php echo base_url("panoc/tisemua/"); ?>"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Semua Tiket</a></li>

        <li class="<?php echo $menu["titunda"] ?>"><a href="<?php echo base_url("panoc/titunda/"); ?>"><svg class="glyph stroked pen tip"><use xlink:href="#stroked-pen-tip"></use></svg> Tiket Tertunda</a></li>

        <li class="<?php echo $menu["tiseles"] ?>"><a href="<?php echo base_url("panoc/titunda/"); ?>"><svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg> Tiket Selesai</a></li>

        <li class="<?php echo $menu["tikelol"] ?>"><a href="<?php echo base_url("panoc/tikelol/"); ?>"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg> Kelola Item Tiket</a></li>
<?php if($levku=="user" && $aksku=="super" ):?>
        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>SUPERUSER</b>
        <li class="<?php echo $menu["uskelol"] ?>"><a href="<?php echo base_url("panoc/uskelol/"); ?>"><svg class="glyph stroked chain"><use xlink:href="#stroked-chain"></use></svg> Kelola User</a></li>

        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>LAPORAN</b>
        <li class="<?php echo $menu["ladload"] ?>"><a href="<?php echo base_url("panoc/ladload/"); ?>"><svg class="glyph stroked printer"><use xlink:href="#stroked-printer"></use></svg> Cetak</a></li>
<?php endif; ?>

    </ul>

</div><!--/.sidebar-->



