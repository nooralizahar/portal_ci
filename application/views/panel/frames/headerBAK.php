<?php
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
$blmbaca = $this->db->where("dibaca",0)->where("ke_user",$this->session->userdata("id_pengguna"))->get("ts_isuksidrel")->num_rows();
$blmbaca2 = $this->db->where("dibaca",0)->where("id_pengguna !=",$this->session->userdata("id_pengguna"))->where("id_disx",$this->uri->segment(3))->get("ts_isuksidres")->num_rows();
$allblmbaca=$blmbaca2+$blmbaca;

$divi= $this->db->where("id_div",$this->session->userdata("id_div"))->get("ts_isivid")->result();

foreach($divi as $dvs): 
    $nmdv=$dvs->nm_div; 
    $iddv=$dvs->id_div;
endforeach;

$idku=$this->session->userdata("id_pengguna");

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (isset($judul)) ? $judul : ""; ?></title>

    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-table.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/datepicker3.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-timepicker.min.css");?> type="text/css" />
    <link href="<?php echo base_url("assets/css/styles.css");?>" rel="stylesheet">

    <link rel="icon" href="<?php echo base_url("favicon.png"); ?>" type="image/png" sizes="14x5">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/css/fileinput.min.css">

    <!-- Include Editor style. -->
    <!--<link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_style.min.css' rel='stylesheet' type='text/css' />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <!--Icons-->
    <script src="<?php echo base_url("assets/js/lumino.glyphs.js");?>"></script>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url("assets/js/html5shiv.js");?>"></script>
    <script src="<?php echo base_url("assets/js/respond.min.js");?>"></script>
    <![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/js/fileinput.min.js"></script>


    <link rel="stylesheet" href="<?php echo base_url("assets/css/magnific-popup.css"); ?>">
    <script src="<?php echo base_url("assets/js/bootstrap-datetimepicker.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.magnific-popup.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jQuery.print.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/bootbox.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.shorten.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
    <!-- Include JS file. -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/js/froala_editor.min.js'></script>

    <script>
        BASE_URL = "<?php echo base_url(); ?>";
	    $.FroalaEditor.DEFAULTS.key = "bvA-21sD-16A-13ojmweC8ahD6f1n==";
    </script>

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>JLM</span>PORTAL</a>

            <ul class="user-menu">

                <li class="dropdown pull-right">
                    <!--<a data-toggle="modal" data-target="#notifiku" href="#"><?php //echo $unread; ?>&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-envelope"></span>&nbsp;&nbsp;&nbsp;<?php //echo $unread2; ?></a>&nbsp;&nbsp;&nbsp;
                    <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown pull-right"> class="dropdown-toggle" data-toggle="dropdown" --> 
                    <!-- <a href="#" ><span class="label label-pill label-danger count" style="border-radius:10px;"></span> <span class="glyphicon glyphicon-bell" style="font-size:18px;"></span></a>&nbsp;&nbsp; -->
                    <!-- <ul class="dropdown-menu" role="menu"></ul>
                    </li>
                    </ul> -->

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <?php echo $this->session->userdata("nama_lengkap"); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo base_url("panel/profilku/").$idku; ?>"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profilku</a></li>
                        <!--<li><a href="#" id="pengaturan"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Ganti Sandi</a></li> -->
                        <li><a href="<?php echo base_url("panel/logout/"); ?>"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
<br>
<!--    <form role="search">-->
<!--        <div class="form-group">-->
<!--            <input type="text" class="form-control" placeholder="Search">-->
<!--        </div>-->
<!--    </form>-->
    <ul class="nav menu">
        <li class="<?php echo $menu["dashboard"] ?>"><a href="<?php echo base_url("panel/"); ?>"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>

    <?php if($nmdv=="Marketing" || $nmdv=="Wholesales" ):?>

        <?php if($idjab != 6 && $idjab != 5 && $idjab != 4): ?>
        <li class="<?php echo $menu["harga_baru"] ?>"><a href="<?php echo base_url("panel/hargabaru/"); ?>"><svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>Req. Harga</a></li>
        <?php endif;?>

        <li class="<?php echo $menu["survey"] ?>"><a href="<?php echo base_url("survey/"); ?>"><svg class="glyph stroked camera"><use xlink:href="#stroked-camera"></use></svg>Req. Survey</a></li>

        <?php if($nmjab!="PKL"):?>
        <li class="<?php echo $menu["purc_baru"] ?>"><a href="<?php echo base_url("panel/purcbaru/"); ?>"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>Req. Purchase</a></li> 
        <?php endif;?>
        
        <li class="<?php echo $menu["womonito"] ?>"><a href="<?php echo base_url("panel/womonito/"); ?>"><svg class="glyph stroked desktop"><use xlink:href="#stroked-desktop"></use></svg> Monitor WO</a></li>
            <?php if($idjab == 1 || $idjab==2 || $idjab==11 || $idjab==29 || $idjab==30): ?>        
                    <li class="<?php echo $menu["selfservice"] ?>"><a href="<?php echo base_url("selser/"); ?>"><svg class="glyph stroked download"><use xlink:href="#stroked-download"></use></svg> Self Service</a></li>
            <?php endif;?>  
            <?php if($idjab == 1 || $idjab==4 || $idjab==6): ?>        
                    <li class="<?php echo $menu["khusus"] ?>"><a href="<?php echo base_url("khusus/"); ?>"><svg class="glyph stroked flag"><use xlink:href="#stroked-flag"></use></svg> Khusus</a></li>
            <?php endif;?> 
            <?php if($idjab == 4 || $idjab == 9  ): ?>        
                    <li class="<?php echo $menu["salesrevenue"] ?>"><a href="<?php echo base_url("salvenue/"); ?>"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Sales Revenue</a></li>
            <?php endif;?> 
            
            <?php if($idjab == 1): ?>        
                    <li class="<?php echo $menu["inventory"] ?>"><a href="<?php echo base_url("inventory/"); ?>"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Inventory</a></li>
            <?php endif;?>  

    


    <?php elseif($nmdv=="IT"):?>
        <li class="<?php echo $menu["menu_it"] ?>"><a href="<?php echo base_url("panel/menuit/"); ?>"><svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>Menu TI</a></li>
        <li class="<?php echo $menu["purc_baru"] ?>"><a href="<?php echo base_url("panel/purcbaru/"); ?>"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>Req. Purchase</a></li> 
        
         

    <?php elseif($nmdv=="Engineering"):?>
        <li class="<?php echo $menu["menu_eng"] ?>"><a href="<?php echo base_url("panel/menueng/"); ?>"><svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>Menu Eng.</a></li>
   
    <?php elseif($nmdv=="Operation"):?>
        
    <?php elseif($nmdv=="HRD & GA"):?>

    <?php elseif($nmdv=="BOD"):?>
        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>DASHBOARD</b>
        <li class="<?php echo $menu["dashboard"] ?>"><a href="<?php echo base_url("panel/"); ?>"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Commercial</a></li>

        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>PERMINTAAN</b>
        <li class="<?php echo $menu["harga_baru"] ?>"><a href="<?php echo base_url("panel/hargabaru/"); ?>"><svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg> Harga</a></li>
        <li class="<?php echo $menu["survey"] ?>"><a href="<?php echo base_url("survey/"); ?>"><svg class="glyph stroked camera"><use xlink:href="#stroked-camera"></use></svg> Survei</a></li>
        <li class="<?php echo $menu["purc_baru"] ?>"><a href="<?php echo base_url("panel/purcbaru/"); ?>"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg> Pembelian</a></li>

        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>DAFTAR</b>
        <li class="<?php echo $menu["menol"] ?>"><a href="<?php echo base_url("datset/"); ?>"><svg class="glyph stroked laptop computer and mobile"><use xlink:href="#stroked-laptop-computer-and-mobile"></use></svg> Asset</a></li>
        <li class="<?php echo $menu["mesat"] ?>"><a href="<?php echo base_url("datbar/logbar/"); ?>"><svg class="glyph stroked wireless router"><use xlink:href="#stroked-wireless-router"></use></svg> Perangkat</a></li>
        <li class="<?php echo $menu["menuotb"] ?>"><a href="<?php echo base_url("otbmen/otbli/"); ?>"><svg class="glyph stroked external hard drive"><use xlink:href="#stroked-external-hard-drive"></use></svg> OTB</a></li>
        <li class="<?php echo $menu["lokaotb"] ?>"><a href="<?php echo base_url("otbmen/otblo/"); ?>"><svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"></use></svg> Lokasi OTB</a></li>
        <li class="<?php echo $menu["daftaodp"] ?>"><a href="<?php echo base_url("focmen/odpin/"); ?>"><svg class="glyph stroked external hard drive"><use xlink:href="#stroked-external-hard-drive"></use></svg> ODP</a></li>
        <li class="<?php echo $menu["daftaodc"] ?>"><a href="<?php echo base_url("focmen/odcin/"); ?>"><svg class="glyph stroked internal hard drive"><use xlink:href="#stroked-internal-hard-drive"></use></svg> ODC</a></li>
        <li class="<?php echo $menu["rekapodp"] ?>"><a href="<?php echo base_url("focmen/odprek/"); ?>"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Rekap ODC & ODP</a></li>

        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>LAINNYA</b> 
        <li class="<?php echo $menu["inventory"] ?>"><a href="<?php echo base_url("inventory/"); ?>"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Inventory</a></li>
    <?php else :?>


    <?php endif;?>

    <?php if($nmjab=="Purchasing"):?>
        <li class="<?php echo $menu["purc_baru"] ?>"><a href="<?php echo base_url("panel/purcbaru/"); ?>"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>Req. Purchase</a></li> 
         <li class="<?php echo $menu["inventory"] ?>"><a href="<?php echo base_url("inventory/"); ?>"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Inventory</a></li>
    <?php endif;?>

    <?php if($nmjab=="FTTH" || $nmjab=="CS FTTH" ):?>

        <!-- <li class="<?php echo $menu["menufoc"] ?>"><a href="<?php echo base_url("focmen/"); ?>"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Menu FTTH</a></li> -->
        <li class="<?php echo $menu["daftaodp"] ?>"><a href="<?php echo base_url("focmen/odpin/"); ?>"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Daftar ODP</a></li>
        <li class="<?php echo $menu["daftaodc"] ?>"><a href="<?php echo base_url("focmen/odcin/"); ?>"><svg class="glyph stroked chain"><use xlink:href="#stroked-chain"></use></svg> Daftar ODC</a></li>
        <li class="<?php echo $menu["rekapodp"] ?>"><a href="<?php echo base_url("focmen/odprek/"); ?>"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Rekapitulasi</a></li>

    <?php endif;?>

    <?php if($nmjab=="FOC Admin"):?>
            <li class="<?php echo $menu["survey"] ?>"><a href="<?php echo base_url("survey/"); ?>"><svg class="glyph stroked camera"><use xlink:href="#stroked-camera"></use></svg>Req. Survey</a></li>
            <li class="<?php echo $menu["womonito"] ?>"><a href="<?php echo base_url("panel/womonito/"); ?>"><svg class="glyph stroked desktop"><use xlink:href="#stroked-desktop"></use></svg> Monitor WO</a></li>
    <?php endif;?> 

    <?php if($nmjab=="FOC"):?>
        <li class="<?php echo $menu["menuotb"] ?>"><a href="<?php echo base_url("otbmen/"); ?>"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Menu OTB</a></li>
    <?php endif;?>

        <?php if($nmjab!="FTTH" || $nmjab!="PKL" ):?>
        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>Personal</b>
        <li class="<?php echo $menu["aktifitas"] ?>"><a href="<?php echo base_url("aktifitas/"); ?>"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Aktifitas</a></li>
        <?php endif;?>


        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>DiskuC</b>
        <li class="<?php echo $menu["chat_baru"] ?>"><a href="<?php echo base_url("panel/disxbaru/"); ?>"><svg class="glyph stroked empty message"><use xlink:href="#stroked-empty-message"></use></svg> Baru</a></li>
        <li class="<?php echo $menu["disx_trma"] ?>"><a href="<?php echo base_url("panel/disxtrma"); ?>"><svg class="glyph stroked folder"><use xlink:href="#stroked-folder"></use></svg> <?php echo ($blmbaca > 0) ? "Masuk <sup><span class='badge badge-danger blinking'>$blmbaca</span></sup>": "Masuk"; ?></a></li>
        <li class="<?php echo $menu["disx_kirm"] ?>"><a href="<?php echo base_url("panel/disxkirm"); ?>"><svg class="glyph stroked open folder"><use xlink:href="#stroked-open-folder"></use></svg>  <?php echo ($blmbaca2 > 0) ? "Keluar <!--<sup><span class='badge badge-danger blinking'>$blmbaca</span></sup>-->": "Keluar"; ?></a></li>

        <?php if($idjab == 1 || $idku==28 ): ?>
        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>Kelola</b>
        <li class="<?php echo $menu["pengguna"]; ?>"><a href="<?php echo base_url("panel/pengguna"); ?>"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"/></svg>Pengguna</a></li> 
        <li class="<?php echo $menu["divisi"]; ?>"><a href="<?php echo base_url("panel/divisi"); ?>"><svg class="glyph stroked chain"><use xlink:href="#stroked-chain"/></svg>Bagian</a></li>      
        <li class="<?php echo $menu["jabatan"]; ?>"><a href="<?php echo base_url("panel/jabatan"); ?>"><svg class="glyph stroked paperclip"><use xlink:href="#stroked-paperclip"/></svg>Jabatan</a></li>
        
        <?php endif;?>
        <li role="presentation" class="divider"></li>

    </ul>

</div><!--/.sidebar-->



