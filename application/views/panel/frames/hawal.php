<?php
$idjab=$this->session->userdata("id_jabatan");
$iddiv=$this->session->userdata("id_div");
$idku=$this->session->userdata("id_pengguna");
$levku=$this->session->userdata("level");
$aksku=$this->session->userdata("mn_akses");
$segku=$this->uri->segment(1);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (isset($judul)) ? $judul : ""; ?></title>

    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-table.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/datepicker3.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-timepicker.min.css");?> type="text/css" />
    <link href="<?php echo base_url("assets/css/styles.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/stybotn.css");?>" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url("favicon.png"); ?>" type="image/png" sizes="14x5">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/css/fileinput.min.css">

    <!-- Include Editor style. -->
    <!--<link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_style.min.css' rel='stylesheet' type='text/css' />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <!--Icons-->
    <script src="<?php echo base_url("assets/js/lumino.glyphs.js");?>"></script>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url("assets/js/html5shiv.js");?>"></script>
    <script src="<?php echo base_url("assets/js/respond.min.js");?>"></script>
    <![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/js/fileinput.min.js"></script>


    <link rel="stylesheet" href="<?php echo base_url("assets/css/magnific-popup.css"); ?>">
    <script src="<?php echo base_url("assets/js/bootstrap-datetimepicker.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.magnific-popup.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jQuery.print.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/bootbox.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.shorten.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
    <!-- Include JS file. -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/js/froala_editor.min.js'></script>

    <script>
        BASE_URL = "<?php echo base_url(); ?>";
	    $.FroalaEditor.DEFAULTS.key = "bvA-21sD-16A-13ojmweC8ahD6f1n==";
    </script>

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>JLM</span>PORTAL</a>

            <ul class="user-menu">

                <li class="dropdown pull-right">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>&nbsp;&nbsp; <?php echo $this->session->userdata("nama_lengkap"); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo base_url($segku."/logout/"); ?>"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Keluar</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
<br>
     &nbsp;&nbsp;<b>DASHBOARD</b>
    <ul class="nav menu">
        
       <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>PENGGUNA</b>
        <li class="<?php echo $menu["profil"] ?>"><a href="<?php echo base_url($segku."/profilvi/").$idku; ?>"><i class="fa fa-user"></i>&nbsp;&nbsp;Profilku</a></li>

        <li class="<?php echo $menu["uprofil"] ?>"><a href="<?php echo base_url($segku."/profilku/").$idku; ?>"><i class="fa fa-cog"></i>&nbsp;&nbsp;Ubah Sandi</a></li>
        <li class="<?php echo $menu["aprofil"] ?>"><a href="<?php echo base_url($segku."/profilak/"); ?>"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Aktifitas</a></li>
        <li role="presentation" class="divider"></li>


         &nbsp;&nbsp;<b>MENU AKSES</b>
        <li class="<?php echo $menu["menol"] ?>"><a href="<?php echo base_url($segku."/dafset/"); ?>"><i class="fa fa-list"></i>&nbsp;&nbsp;Daftar Asset</a></li>


<?php if($levku=="user" && $aksku=="super" ):?>
        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>ADMIN</b>
        <li class="<?php echo $menu["uskelol"] ?>"><a href="<?php echo base_url($segku."/uskelol/"); ?>"><svg class="glyph stroked chain"><use xlink:href="#stroked-chain"></use></svg> Kelola User</a></li>

<?php endif; ?>

    </ul>

</div><!--/.sidebar-->



