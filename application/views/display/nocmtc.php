<?php 

?>
<!DOCTYPE html>
<html class="no-js" lang="en"> 
    
<head>
        <title><?= $judul ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="display data">
		<meta name="keywords" content="display data">
        <meta name="author" content="toby fittivaldy">
		<meta http-equiv="refresh" content="300;url=<?php echo base_url("display/");?>">
        <meta charset="UTF-8">

        <!-- CSS Bootstrap & Custom -->
     
		<link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet" media="screen">
		<link href="<?php echo base_url("assets/css/stybotd.css");?>" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
       
        <!-- Favicons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/logo-kkami.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/logo-kkami.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/logo-kkami.png">
        <link rel="apple-touch-icon-precomposed" href="images/ico/logo-kkami.png">
        <link rel="shortcut icon" href="images/ico/logo-kkami.png">
    
        <!-- JavaScripts -->
		
	   

		        <script type="text/javascript">

				function jamOn() {
					var today = new Date();
					var h = today.getHours();
					var m = today.getMinutes();
					var s = today.getSeconds();
						m = tamNol(m);
						s = tamNol(s);
					document.getElementById('jamDigi').innerHTML =
					h + ":" + m + ":" + s;
					var t = setTimeout(jamOn, 500);
				}
				function tamNol(i) {
					if (i < 10) {i = "0" + i};  // tambah nol utk angka < 10
					return i;
				}
				
		</script>
		
		<script type="text/javascript">
			 if (document.addEventListener) {
				 document.addEventListener('contextmenu', function(e) {
					//alert("Anda tidak bisa melihat source code website ini");
					 e.preventDefault();
				 }, false);
			 } else{
				document.attachEvent('oncontextmenu', function() {
					//alert("Anda tidak bisa melihat source code website ini");
					 window.event.returnValue = false;
				 });
			 }
		</script>
		
		
			
    </head>
	
    <body onload="jamOn()">

    <table width="100%">	
	<?php $tgljam= date('Y-m-d H:i:s'); $ambln= substr($tgljam,5,2); $blnindo=bulan($ambln);?>
	<center><div style="color: cyan;"><h3><strong>DASHBOARD INFO <br> <?php $blnaja=strtoupper($blnindo); echo"$blnaja";?></strong></h3></center></div>
	
	<tr style="background-color:#3c3c3c;opacity: .6;color: white;"><td><h3><b><center><div><?php  $hrini=tgl_indo(date('Y-m-d')); echo "$hrini"; ?></div><div id="jamDigi"></div></center></h3></b></td></tr>

	<tr valign="top">
	<td>
		<div class='form-group row'> <!-- ST form row group 1-->

					   
			<div class='col-sm-1'></div> 

							
	    	<div class='col-sm-10'>
	    		<center><label for='mtcinfo' class='form-control-label'><h4 style="color: white"><?php //echo $barisj; ?> <i class='fa fa-info-circle'></i><strong> MAINTENANCE</strong></h4></label></center>
				<table class='table table-bordered'>
					<thead width='90%' >

					
						<tr style="background-color:#000278;opacity: .6;color: white;">
							<th class='text-center' width='10%' valign="middle">TANGGAL</th>
							<th class='text-center' width='5%' valign="middle">WAKTU</th>
							<th class='text-center' width='10%'>DOWNTIME (MENIT)</th>
							<th class='text-center' width='25%' valign="middle">DESKRIPSI</th>
							<th class='text-center' width='25%' valign="middle">IMPACT</th>
							<th class='text-center' width='5%' valign="middle">VENDOR</th>
							<th class='text-center' width='10%' valign="middle">SIGN BY</th>
						</tr>
					</thead>
					
					<tbody>
						<?php if($barisj>0): ?>	
						<?php 
						$no=1;
						foreach($maintj as $dpro): 

							if($no%2){$color="#aecbff";}else{$color="#deeaff";}
								$tgmtn = new DateTime($dpro->tgmtc);
								$today = new DateTime('today');	
								$day = $today->diff($tgmtn)->days;

								if($tgmtn<$today){
									$hat=$day*(-1);
								}else{
									$hat=$day;
								}

						?>
                        <tr style="background-color:<?= $color ?>;opacity: .8;">

                       
                            <td align="center">

                            <?php echo tgl_indosm($dpro->tgmtc)."<br>"; 

                            	if ($hat<-1){
                            		$updata= array(
									        'pbsta' => 'N',
									        'tg_off' => $tgljam,
									        'us_off' => 'timer'
									);

                            		//$q=$this->db->set('pbsta','N')->where('idmtc',$dpro->idmtc)->update('no_ctmofni');
                            		$q=$this->db->where('idmtc',$dpro->idmtc)->update('no_ctmofni',$updata);
                            		return $q;

								}else{
							
	                            	if ($hat==0){ 
									  echo "<span class='blinkred'><b>hari ini</b></span>";
									}elseif($hat<0) {
									  echo $hat*(-1)." hari lalu";
									}else{
									  echo "$hat hari lagi";
									}
								}

                            ?>
                            	
                            </td>

                            <td><?php echo "<center>".$dpro->jmmtc." s/d ".$dpro->jsmtc."</center>"; ?></td>
                            <td><?php echo "<center>".$dpro->dntime."</center>"; ?></td>
                            <td><?php echo $dpro->demtc; ?></td>
                            <td><?php echo substr(strip_tags($dpro->immtc),0,50)."<b>...</b>"; ?></td>
                            <td align="center"><?php echo $dpro->nmven; ?></td>
                            <td align="center"><?php echo $dpro->us_cre; ?></td>



                        </tr>

                        <?php $no++; endforeach; ?>

                           <?php else: ?>
                           	<tr bgcolor="#e4e5ff">
                            <td align="center" colspan="7"><b>TIDAK ADA DATA</b></td>
                            </tr>	
                            <?php endif;?>	
					</tbody>

	    	</div>


	    	<div class='col-sm-1'></div>


	    </div> <!-- EO form row group 1-->

	</td>
	</tr> 

	</table> 
		
	    <script src="js/bootstrap.min.js"></script>
        <script src="js/min/plugins.min.js"></script>
        <script src="js/min/custom.min.js"></script>



    </body>
</html>

<?php 
/*
			           <div class='form-group row'> <!-- ST form row group 1-->
					   
					        <div class='col-sm-1'>				
				            </div> 
							
			                <div class='col-sm-10'>
								
			            
								 <?php

																		
									$infomtc=mysqli_query($conn,"SELECT * FROM no_ctmofni WHERE pbsta='Y' ORDER BY tgmtc ASC");

									$adamtc=mysqli_num_rows($infomtc);
									echo "<table class='table table-bordered'>
											<thead width='98%' >
											<tr><th colspan='7' class='text-center'><label for='mtcinfo' class='form-control-label'><h3> <i class='fa fa-info-circle'></i> MAINTENANCE</h3></label></th></tr>
											<tr class='btn-primary'>
											<th>TANGGAL</th>
											<th>WAKTU</th>
											<th>DOWNTIME (MENIT)</th>
											<th>DESKRIPSI</th>
											<th>IMPACT</th>
											<th>VENDOR</th>
											<th>SIGN BY</th>
											</tr>
											</thead>
											<tbody>"; 
									
									if ($adamtc>0){
									

									
									$no=1;
									
									while($t=mysqli_fetch_array($infomtc)){
									
									 if($no%2){$color="#aecbff";}else{$color="#deeaff";}
										    $tgmtn = new DateTime($t[tgmtc]);
											$today = new DateTime('today');	
											$day = $today->diff($tgmtn)->days;
											if($tgmtn<$today){
													$hat=$day*(-1);
											}else{
													$hat=$day;
											}	

										//$ha1=date_format($tgmtn,'d');
										//$ha2=date_format($today,'d');
										//$hat=$ha1-$ha2; 
										
										
										//if ($day==0){
										if ($hat<-1){
											    mysqli_query($conn, "UPDATE no_ctmofni SET pbsta='N' ,
																						   tg_off  = '$tgljam',
																						   us_off  = 'timer'											  
																				 WHERE idmtc   = '$t[idmtc]'");
										//	   }
										}else{  
												$jmul=substr($t[jmmtc],0,5);
												$jsel=substr($t[jsmtc],0,5);
												$tgin=tgl_indop($t[tgmtc]);
												echo "
														<tr bgcolor = $color>
														<td width='10%'>$tgin<br> ";
														if ($hat==0){ 
															echo "<font style='color:red'>hari ini</font>";
														}elseif($hat<0) {
															echo $hat*(-1)." hr lalu";
														}else{
															echo "$hat hr lagi";
														}

												echo "</td>
														<td width='3%'> $jmul s/d $jsel</td>
														<td width='7%'>$t[dntime]</td>
														<td width='20%'>$t[demtc]</td>
														<td width='25%'>$t[immtc]</td>
														<td width='5%'>$t[nmven]</td>
														<td width='7%'>$t[us_cre]</td>
														</tr>

												";
												
										}
										$no++;
										
					
									} echo "<tr><center><!--<img src='images/oth/garis.png' class='img-responsive'>--></center></tr></tbody></table>"; }else{
									$color="#deeaff";
									echo "<tbody>
									<tr bgcolor = $color><td colspan=7>
									<center><h3>Tidak Ada Data</h3><br></center>
									<center><!--<img src='images/oth/garis.png' class='img-responsive'>--></center></td>
									</tr>
									</tbody></table>
									";	
									}
								  ?>

			                </div> 

					        <div class='col-sm-1'>				
				            </div> 
						
					</div> <!-- EO form row group 1--> */

?>