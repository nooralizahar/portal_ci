

            <div class="form-group"> 
                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="15"><center><h4>REKAPITULASI ROLLOUT BERDASARKAN TAHUN <?//= date('Y') ?></h4></center></th></tr>
                            <tr>

                                <th>&nbsp;No.</th>
                                <th>&nbsp;TAHUN</th>
                                <th><center>JAN</center></th>
                                <th><center>FEB</center></th>
                                <th><center>MAR</center></th>
                                <th><center>APR</center></th>
                                <th><center>MEI</center></th>
                                <th><center>JUN</center></th>
                                <th><center>JUL</center></th>
                                <th><center>AGU</center></th>
                                <th><center>SEP</center></th>
                                <th><center>OKT</center></th>
                                <th><center>NOP</center></th>
                                <th><center>DES</center></th>
                                <th><center>TOTAL</center></th>
                            </tr>
                            </thead>

                            <tbody>
                                <?php
                                    $no = 1;
                                    foreach ($roll_rekap_tahun->result() as $rt):
                                    $bulan = $this->db->query("SELECT MONTH(tgpas) as bulan, SUM(takab) as jumlah FROM app_latrop_lebak WHERE YEAR(tgpas) = ".$rt->tahun." GROUP BY MONTH(tgpas)");
                                    $jan = 0;$feb = 0;$mar = 0;$apr = 0;$mei = 0;$jun = 0;$jul = 0;$agu = 0;$sep = 0;$okt = 0;$nov = 0;$des = 0;
                                        foreach ($bulan->result() as $rb){
                                            switch ($rb->bulan) {
                                                case 1:
                                                    $jan = $rb->jumlah;
                                                    break;
                                                case 2:
                                                    $feb = $rb->jumlah;
                                                    break;
                                                case 3:
                                                    $mar = $rb->jumlah;
                                                    break;
                                                case 4:
                                                    $apr = $rb->jumlah;
                                                    break;
                                                case 5:
                                                    $mei = $rb->jumlah;
                                                    break;
                                                case 6:
                                                    $jun = $rb->jumlah;
                                                    break;
                                                case 7:
                                                    $jul = $rb->jumlah;
                                                    break;
                                                case 8:
                                                    $agu = $rb->jumlah;
                                                    break;
                                                case 9:
                                                    $sep = $rb->jumlah;
                                                    break;
                                                case 10:
                                                    $okt = $rb->jumlah;
                                                    break;
                                                case 11:
                                                    $nov = $rb->jumlah;
                                                    break;
                                                case 12:
                                                    $des = $rb->jumlah;
                                                    break;
                                                default:
                                                    echo 0;
                                            }
                                    }
                                ?>
                                <tr>
                                    <td><?= $no++  ?></td>
                                    <td><?= $rt->tahun ?></td>
                                    <td><?= $jan ?></td>
                                    <td><?= $feb ?></td>
                                    <td><?= $mar ?></td>
                                    <td><?= $apr ?></td>
                                    <td><?= $mei ?></td>
                                    <td><?= $jun ?></td>
                                    <td><?= $jul ?></td>
                                    <td><?= $agu ?></td>
                                    <td><?= $sep ?></td>
                                    <td><?= $okt ?></td>
                                    <td><?= $nov ?></td>
                                    <td><?= $des ?></td>
                                    <td><?= $rt->total ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                           </table>

            </div>
