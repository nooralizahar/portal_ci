﻿
<?php
    $nkap=$this->db->select('SUM(kapasitas) as kapasitas, SUM(sisa) as sisa')->get("v_t_odc")->result();
      foreach ($nkap as $n) {
           $kap=$n->kapasitas;
           $sis=$n->sisa;
      }


      foreach ($paktif as $a) {
           $jmlakt=$a->jml;
      }
      foreach ($pbloki as $b) {
           $jmlblo=$b->jml;
      }
      foreach ($ptermi as $t) {
           $jmlter=$t->jml;
      }

        $thnini= date('Y');
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));
        $harini= date('Y-m-d');
        $harlal= date('Y-m-d',strtotime("-1 day")); 
?>
<div class="row">
    <center>
    <h2> DASHBOARD FTTH/X</h2>
    </center>
</div>
    <ol class="breadcrumb">
          <li>DATA PELANGGAN FTTH <?php echo strtoupper(date('d M Y H:i:s')); 
            //echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></li>

    </ol>
<br/>
<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php //echo number_format($capodp->cap); ?><span class="hitlari"><?php echo $capodp->cap; ?></span></div>
                        <div class="text-muted"> Kapasitas (port ODP)</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda-l2 panel-widget">
                <div class="row no-padding">
                    <!--<a href="<?php echo base_url("focmen/odpcs/"); ?>" >-->
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php //echo number_format($jmlakt+$jmlblo); ?><span class="hitlari"><?php echo $jmlakt+$jmlblo; ?></span></div>
                        <div class="text-muted"> Pelanggan (iBoss)</div>
                    </div><!--</a>-->
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal-l2 panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><span class="hitlari"><?php echo $capodp->cap-($jmlakt+$jmlblo);?> </span></div>
                        <div class="text-muted"> Tersedia (port ODP)</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
    <ol class="breadcrumb">
          <li><center> RINCIAN PELANGGAN </center></li>

    </ol>
<br/>
<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($jmlakt); ?></div>
                        <div class="text-muted"> Active</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($jmlblo); ?></div>
                        <div class="text-muted"> Blocked</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><?php echo number_format($jmlter);?></div>
                        <div class="text-muted"> Terminate</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 


<div class="row">
    <div class="col-lg-12">
 <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><span class="hitlari"><?= $akttod ?></span></div>
                        <div class="text-muted"> Aktivasi Hari Ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clock"><use xlink:href="#stroked-clock"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><span class="hitlari"><?= $aktlas ?></span></div>
                        <div class="text-muted"> Aktivasi Kemarin</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
 <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><span class="hitlari"><?= $aktbni ?></span></div>
                        <div class="text-muted"> Aktivasi Bulan Ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clock"><use xlink:href="#stroked-clock"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><span class="hitlari"><?= $aktbla ?></span></div>
                        <div class="text-muted"> Aktivasi Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
            <div class="panel-body" style="max-width: 98%;overflow-x: scroll;">
                <?php $this->load->view("/display/dat/pelsu")?>
            </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
          <li>DATA ODP </li>

    </ol>
    <br/>    
       <!-- <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $dver." (".number_format(($dver/($dver+$dunv))*100)." %)" ?></div>
                        <div class="text-muted"> ODP Verified</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-merah panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clock"><use xlink:href="#stroked-clock"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $dunv." (".number_format(($dunv/($dver+$dunv))*100)." %)" ?></div>
                        <div class="text-muted"> ODP Unverified</div>
                    </div>
                </div>
            </div>
        </div>-->

    </div>
</div> 



<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($totodp); ?></div>
                        <div class="text-muted"> ODP</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <!--<a href="<?php echo base_url("focmen/odpcs/"); ?>" >-->
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($totodp); ?></div>
                        <div class="text-muted"> Active</div>
                    </div><!--</a>-->
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php echo number_format(0); ?> </a></div>
                        <div class="text-muted"> InActive</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 

<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked open folder"><use xlink:href="#stroked-open-folder"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($odpemp); ?></div>
                        <div class="text-muted"> 100% Kosong</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($odpbal); ?></div>
                        <div class="text-muted"> Sisa >= 50%</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($odpcil); ?></div>
                        <div class="text-muted"> Sisa < 50%</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked folder"><use xlink:href="#stroked-folder"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php echo number_format($odpful->jml);?> </a></div>
                        <div class="text-muted"> Sudah Penuh</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 

<div class="row">
    <div class="col-lg-12">
 <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked folder"><use xlink:href="#stroked-folder"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $odpbni ?></div>
                        <div class="text-muted"> ODP Bulan Ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked folder"><use xlink:href="#stroked-folder"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $odpbla ?></div>
                        <div class="text-muted"> ODP Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
            <div class="panel-body" style="max-width: 98%;overflow-x: scroll;">
                <?php $this->load->view("/display/dat/odpsu")?>
            </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
    	    <ol class="breadcrumb">
          <li>DATA ODC </li>

    </ol>
    <br/>   

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_odc")->num_rows()); ?></a></div>
                        <div class="text-muted" > ODC</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked upload"><use xlink:href="#stroked-upload"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($kap); ?></div>
                        <div class="text-muted"> Kapasitas</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked download"><use xlink:href="#stroked-download"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($sis); ?></div>
                        <div class="text-muted"> Sisa</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
            <div class="panel-body" style="max-width: 98%;overflow-x: scroll;">
                <?php $this->load->view("/display/dat/odcsu")?>
            </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
          <li><center> REKAPITULASI REALISASI ROLLOUT <sup><small>( Sejak Sep 2021 )</small></sup>&nbsp;&nbsp;&nbsp;<?php $lu=$this->db->order_by("tgbua","desc")->limit(1)->get("app_latrop_lebak")->row(); echo "Terakhir Input : ".date("d-M-Y H:i:s",strtotime($lu->tgbua)); ?></center></li>

    </ol>
<br/>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-blue-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-calendar-check-o fa-3x"></i>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $u=$this->db->select("sum(jml) as tjml")->where("bln like",$thnini."%")->get("v_app_latrop_lebak_bln")->row(); echo $u->tjml."<small> m</small>";?></div>
                        <div class="text-muted"> Jumlah Keseluruhan</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-calendar-o fa-3x"></i>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("bln",$blnini)->get("v_app_latrop_lebak_bln")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> Bulan ini</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa fa-calendar fa-3x"></i>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $s=$this->db->where("bln",$blnlal)->get("v_app_latrop_lebak_bln")->row(); if(empty($s)){echo "0";}else{echo $s->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> Bulan lalu</div>
                    </div>
                </div>
            </div>
        </div>



    </div>
</div> 

<div class="row">
    <div class="col-lg-12">
            <div class="panel-body" style="max-width: 98%;overflow-x: scroll;">
                <?php $this->load->view("/display/dat/rollsu")?>
            </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
          <li><center> REKAPITULASI BERDASARKAN FUNGSI KABEL</center></li>

    </ol>
<br/>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-merah panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                    <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                D    
                            </strong>
                    </span>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $d=$this->db->where("sekab","Distribusi")->get("v_app_latrop_lebak_seg")->row(); if(empty($d)){echo "0";}else{echo $d->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> Distribusi</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-merah-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                    <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                B    
                            </strong>
                    </span>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $b=$this->db->where("sekab","Backbone")->get("v_app_latrop_lebak_seg")->row(); if(empty($b)){echo "0";}else{echo $b->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> Backbone</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                    <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                F    
                            </strong>
                    </span>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $f=$this->db->where("sekab","Feeder")->get("v_app_latrop_lebak_seg")->row(); if(empty($f)){echo "0";}else{echo $f->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> Feeder</div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>



<div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
          <li><center> REKAPITULASI BERDASARKAN JENIS KABEL</center></li>

    </ol>
<br/>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                    <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                12    
                            </strong>
                    </span>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("jekab","12 Core")->get("v_app_latrop_lebak_jka")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> 12 Core</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                     <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                24    
                            </strong>
                    </span>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("jekab","24 Core")->get("v_app_latrop_lebak_jka")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> 24 Core</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                    <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                48    
                            </strong>
                    </span>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("jekab","48 Core")->get("v_app_latrop_lebak_jka")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> 48 Core</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                    <span class="fa-stack fa-2x">
                        <i class="fa fa-square-o fa-stack-2x"></i>
                            <strong class="fa-stack-1x">
                                96    
                            </strong>
                    </span>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $t=$this->db->where("jekab","96 Core")->get("v_app_latrop_lebak_jka")->row(); if(empty($t)){echo "0";}else{echo $t->jml."<small> m</small>";}?></div>
                        <div class="text-muted"> 96 Core</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<div class="row">
    <div class="col-lg-12">
            <ol class="breadcrumb">
          <li>DATA RACK</li>

    </ol>
    <br/>   

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_odc")->num_rows()); ?></a></div>
                        <div class="text-muted" > RACK</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked upload"><use xlink:href="#stroked-upload"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($kap); ?></div>
                        <div class="text-muted"> Kapasitas</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked download"><use xlink:href="#stroked-download"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($sis); ?></div>
                        <div class="text-muted"> Sisa</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
            <div class="panel-body" style="max-width: 98%;overflow-x: scroll;">
                <?php $this->load->view("/display/dat/odcsu")?>
            </div>
    </div>
</div>


<div class="row">
<p></p>

</div>