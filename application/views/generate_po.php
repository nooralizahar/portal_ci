<!DOCTYPE html>
<html>
<head>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="utf-8">
    <title>PO BNETFIT</title>
</head>
<style>
    * {
      box-sizing: border-box;
  }

  /* Create two equal columns that floats next to each other */
  .column {
      float: left;
      width: 50%;
      height: 150px; /* Should be removed. Only for demonstration */
  }

  /* Clear floats after the columns */
  .row:after {
      content: "";
      display: table;
      clear: both;
  }
  /* Create three equal columns that floats next to each other */
  .column1 {
      float: left;
      width: 33.33%;
  }

  /* Clear floats after the columns */
  .row1:after {
      content: "";
      display: table;
      clear: both;
  }
</style>
<body>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <h2 class="text-center">PURCHASE ORDER</h2>
    <div class="row" style="margin-top: 30px; ">
        <div class="column">
            <div class="">
                <b>Kepada :</b>
            </div>
            <div class="">
                <b><?= $get_po->penerima ?></b>
                <br><?= $get_po->alamat_penerima ?>
                <br><?= $get_po->notelp_penerima ?>
                <br><b>ATTN: <?= $get_po->atas_nama ?></b>
            </div>
        </div>
        <div class="column">
            <div class="">
                <div class="">
                    <b>Tanggal :</b>
                </div>
                <div class="">
                    <?= date('d-M-Y', strtotime($get_po->tanggal_po)) ?>
                </div>
            </div>
            <div class="">
                <div class="">
                    <b>PO No. :</b>
                </div>
                <div class="">
                    <?= $get_po->po_no ?>
                </div>
            </div>
            <div class="">
                <div class="">
                    <b>Quotation No. :</b>
                </div>
                <div class="">
                    <?= $get_po->quatation_no ?>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama barang</th>
                <th>Quantity</th>
                <th>Harga Satuan</th>
                <th>Jumlah</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $no = 1;
            $total = 0;
            $subtotal = 0;
            ?>
            <?php foreach ($get_nailat_rel as $value): ?>
                <?php 
                $jumlah = $value->juba*$value->harga;
                ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $value->jeba ?> <?= $value->keba ?></td>
                    <td><?= $value->juba ?> <?= $value->saba ?></td>
                    <td><?= number_format($value->harga,2,',','.') ?></td>
                    <td><?= number_format(($jumlah),2,',','.') ?></td>
                </tr>
                <?php 
                $subtotal += $jumlah;
                ?>
            <?php endforeach; ?>
            <tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" rowspan="7">
                            <span class="text-danger">* Peruntukan</span>
                            <br> <?= $get_po->term_payment ?? '-' ?><br><br>
                            <span class="text-danger">* Alamat Pengirim</span>
                            <br> <?= $get_po->alamat_pengirim ?>
                        </td>
                        <td colspan="2">
                            <?php
                                if ($get_po->ppn == 0) {
                                    $ppn = 0;
                                } else {
                                    $ppn = ($subtotal-$get_po->diskon) * ($get_po->ppn/100);
                                }
                                $grand_total = ($subtotal -$get_po->diskon) + $ppn;
                            ?>
                                <tr>
                                    <td><b>Sub Total<b></td>
                                    <td><b><?= number_format(($subtotal),2,',','.'); ?></b></td>
                                </tr>
                                <tr>
                                    <td><b>Diskon<b></td>
                                    <td><b><?= number_format(($get_po->diskon),2,',','.'); ?></b></td>
                                </tr>
                                <tr>
                                    <td><b>PPN 10%<b></td>
                                    <td><b><?= number_format(($ppn),2,',','.'); ?></b></td>
                                </tr>
                                <tr>
                                    <td><b>Grand Total<b></td>
                                    <td><b><?= number_format(($grand_total),2,',','.'); ?></b></td>
                                </tr>
                                <tr>
                                    <td><b>Biaya Pengiriman<b></td>
                                    <td><b><?= number_format(($get_po->biaya_pengiriman),2,',','.'); ?></b></td>
                                </tr> 
                                <tr>
                                    <td><b>Total<b></td>
                                    <td><b><?= number_format(($grand_total+$get_po->biaya_pengiriman),2,',','.'); ?></b></td>
                                </tr>
                        </td>
                    </tr>
                </tfoot>
        </table>
                        <div class="row1" style="text-align: center; margin-top: 30px">
                            <div class="column1">
                                <b>Pemohon,</b>
                                <br>Purchasing
                                <br><br><br><br>
                                <br>(Olivia)
                            </div>
                            <div class="column1">
                                <b>Mengetahui,</b>
                                <br>Head Dept
                                <br><br><br><br>
                                <br>(Henry)
                            </div>
                            <div class="column1">
                                <b>Menyetujui,</b>
                                <br>Direktur
                                <br><br><br><br>
                                <br>(Victor)
                            </div>
                        </div>   
                    </body>
                    </html>