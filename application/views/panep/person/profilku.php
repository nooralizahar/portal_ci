
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <?php if($this->session->userdata('id_jabatan')==1): ?>
            <!--<li>&nbsp;<a href="<?php echo base_url("panel/pengguna"); ?>">Pengguna</a></li>-->
            <?php endif; ?>
            <li>&nbsp;<a href="<?php echo base_url("panep/"); ?>">Dashboard</a></li>
            <li class="active"> Ubah Sandi</li>
        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal mengedit pengguna! Coba lagi nanti <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data pengguna berhasil diedit di sistem! <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading">Ubah Sandi</div>
                <div class="panel-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <input type="hidden" value="<?php echo $pengguna->idp; ?>" name="id_pengguna">
                        <div class="form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" class="form-control" value="<?php echo $pengguna->nama ?>" readonly/>
                        </div>

                        <div class="form-group">
                            <label for="">Sandi (<label for="bPassword">Ubah</label> <input type="checkbox" id="bPassword"/>)</label>
                            <input type="text" id="iPassword" name="password" class="form-control" disabled="true" required/>
                        </div>
                        <div class="form-group">
                            <!--<label for="">Nomor Induk Karyawan</label>-->
                            <input type="hidden" name="nip" class="form-control" value="<?php echo $pengguna->nik ?>" readonly/>
                        </div>
 
                        <div class="form-group">
                            <!--<label for="">No. HP</label>-->
                            <input type="hidden" name="notifikasi" class="form-control" value="<?php echo $pengguna->hp ?>" required/>
                        </div>  


                        <button type="submit" name="btnSubmit" class="btn btn-success"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                        
                    </form>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script>
    $(document).ready(function(){

        $("#bPassword").on("change",function(){
            if(this.checked){
                $("#iPassword").prop("disabled",false).val("");
            } else {
                $("#iPassword").prop("disabled",true).val("");
            }
        })

    })
</script>
