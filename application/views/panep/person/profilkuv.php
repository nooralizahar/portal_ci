
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url("panep/"); ?>">Dashboard</a></li>
            <li class="active"> ProfilKu</li>
        </ol>
    </div><!--/.row 1-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default" >

                <div class="panel-heading">Profil Ku</div>
                <div class="panel-body">
                    
                    <table class="table table-hover">
                        <tr><td>NIK </td><td>:</td><td><?php echo $pengguna->nik; ?></td></tr>
                        <tr><td>Nama Lengkap</td><td>:</td><td><?php echo $pengguna->nama; ?></td></tr>
                        <tr><td>Departemen </td><td>:</td><td><?php echo $pengguna->divisi; ?></td></tr>
                        <tr><td>Jabatan </td><td>:</td><td><?php echo $pengguna->jabatan; ?></td></tr>
                        <tr><td>Atasan </td><td>:</td><td><?php echo $pengguna->atasan; ?></td></tr>
                        <tr><td>No. HP </td><td>:</td><td><?php echo $pengguna->hp; ?></td></tr>
                    </table>
                       
                </div>
            </div>
        </div>
    </div><!--/.row 2-->

</div>	<!--/.main-->


