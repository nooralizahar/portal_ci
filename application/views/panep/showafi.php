﻿
<?php
    $idjab=$this->session->userdata("id_jabatan");

?>


<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-list fa-3x"></i>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= number_format(0) ?></div>
                        <div class="text-muted"> Proyek Bulan ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-teal-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-list fa-3x"></i>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= number_format(0) ?></div>
                        <div class="text-muted"> Proyek Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div> 



<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-users fa-3x"></i>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format(0); ?></div>
                        <div class="text-muted"> Total Peserta</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">
                    <!--<a href="<?php echo base_url("focmen/odpcs/"); ?>" >-->
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <i class="fa fa-bar-chart fa-3x"></i>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format(0); ?></div>
                        <div class="text-muted"> Total Proyek</div>
                    </div><!--</a>-->
                </div>
            </div>
        </div>


    </div>
</div> 




