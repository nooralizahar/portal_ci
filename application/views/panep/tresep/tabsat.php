        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">


        <i class="fa fa-times-circle" style="color: darkblue;"></i> : Belum diverifikasi, <i class="fa fa-check-circle" style="color: green;"></i> : Sesuai, <i class="fa fa-minus-circle" style="color: red;"></i> : Tidak sesuai
        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true">ID#</th>
                        <th data-field="z"  data-sortable="true">Status</th>
                        <th data-field="b"  data-sortable="true">Nama</th>
                        <th data-field="c"  data-sortable="true">Alamat</th>
                        <th data-field="e"  data-sortable="true">NO. HP</th>
                        <th data-field="f"  data-sortable="true" class="text-center">Email</th>
                        <th data-field="g"  data-sortable="true">KTP</th>
                        <th data-field="d"  data-sortable="true">NPWP</th>
                        <th data-field="s"  data-sortable="true">SIUP</th>                        
                        <th data-field="h"  data-sortable="true">TDP</th>
                        <th data-field="i"  data-sortable="true">NIB</th>
                        <th data-field="j"  data-sortable="true">Akta Pi</th>
                        <th data-field="k"  data-sortable="true">SK Kemenkumham Pi</th>
                        <th data-field="l"  data-sortable="true">Akta Pu</th>
                        <th data-field="m"  data-sortable="true">SK Kemenkumham Pu</th>
                        <th data-field="n"  data-sortable="true">SPT 3 Tahir</th>
                        <th data-field="o"  data-sortable="true">Pemilik</th>
                        <th data-field="p"  data-sortable="true">Managemen</th>
                        <th data-field="q"  data-sortable="true">Pengalaman</th>
                        <th data-field="r"  data-sortable="true">Produk</th>                        
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($sepfad as $s): ?>
                        <tr>
                            <td><?php echo $s->id; ?></td>
                            <td class="text-center">
                                <a href="" data-toggle='tooltip' title="<?php if($s->stadia==0){echo "Perorangan";}else{echo "Badan Usaha";} ?>">
                                <?php if($s->stadia==0): ?>
                                <i class="fa fa-user-o"></i>
                                <?php else : ?>
                                <i class="fa fa-building-o"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td><?php echo $s->nama; ?></td>
                            <td><?php echo $s->alamat; ?></td>
                            
                            <td><?php //echo tgl_indome($l->tglumum); ?></td>
                            <td class="text-center"><?php if($s->status==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <small><sup><?php echo $s->email; ?></sup></small>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php if($s->verid==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php if($s->vernpwp==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>                            
                            <td class="text-center"><?php if($s->versiup==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php if($s->vertdp==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php if($s->vernib==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php if($s->veraktapi==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php if($s->verskmpi==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php if($s->veraktapu==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php if($s->verskmpu==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php if($s->verspt3ti==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td class="text-center"><?php if($s->verpemilik==0): ?>
                                <i class="fa fa-times-circle" style="color: darkblue;"></i>
                                <?php else : ?>
                                <i class="fa fa-check-circle" style="color: green;"></i>
                                <?php endif; ?>
                                </a>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <!--<td><center>
                            <?php if($l->kontrak==0):  $judpdf='Generate PDF';?>
                                <a href="<?php echo base_url("panep/ubtiarba/".$l->idten); ?>" data-toggle='tooltip' title='Isi Keterangan'><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;&nbsp; 

                            <?php else : $judpdf='View PDF';?>
                            <?php endif;?>
                                
                                <a href="<?php echo base_url("panep/pdfgen/".$l->idten); ?>" data-toggle='tooltip' title='<?=$judpdf?>' target="_blank"><i class="fa fa-file-pdf-o fa-lg"></i></a>
 
                            </center></td> -->

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>