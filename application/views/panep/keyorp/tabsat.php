        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">



        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="i"  data-sortable="true">Kode</th>
                        <th data-field="q"  data-sortable="true">Jenis</th>
                        <th data-field="v"  data-sortable="true">Judul</th>
                        <th data-field="a"  data-sortable="true"><center>Batas <br>Penawaran</center></th>
                        <th data-field="j"  data-sortable="true">Pengumuman</th>
                        <th data-field="x"  data-sortable="true"><center>Lampiran<br>Dok.</center></th>
                        <th data-field="d"  data-sortable="true">HPS (Rp.)</th>                        
                        <th data-field="h"  data-sortable="true">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($orptad as $l): ?>
                        <tr>
                            <td><?php echo $l->kodpro; ?></td>
                            <td><?php echo $l->jenpro; ?></td>
                            <td><?php echo $l->judpro; ?></td>
                            <td><?php echo tgl_indome($l->tglbatas); ?></td>
                            <td><?php echo tgl_indome($l->tglumum); ?></td>
                            <td>
                                <?php //echo $l->lampro; ?>
                                <?php if($l->lampro==''){ 

                                }else{ 

                            if($l->lampro != null):
                            $lampiran = json_decode($l->lampro);
                            ?>
                                
                                <?php foreach($lampiran as $item):
                                $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                ?>
                                <!-- <b style="color:green"><i class="fa fa-paperclip fa-lg"></i> </b> -->
                                <a href="<?php echo base_url("assets/uploads/lampro/".$item->file); ?>" class="<?php echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><?php echo $item->judul; ?></a><br>
                                <?php endforeach;  ?> 
                            
                             <?php endif; }?>
                                
                            </td>
                            <td class="text-right"><a href="#" data-toggle='tooltip' title='<?php echo terbilang($l->hpspro); ?>'><?php echo number_format($l->hpspro); ?></a></td>
                            <td><center>
                            <?php if($l->kontrak==0):  $judpdf='Generate PDF';?>
                                <a href="<?php echo base_url("panep/ubtiarba/".$l->idten); ?>" data-toggle='tooltip' title='Isi Keterangan'><i class="fa fa-pencil-square-o fa-lg"></i></a>&nbsp;&nbsp; 

                            <?php else : $judpdf='View PDF';?>
                            <?php endif;?>
                                
                                <a href="<?php echo base_url("panep/pdfgen/".$l->idten); ?>" data-toggle='tooltip' title='<?=$judpdf?>' target="_blank"><i class="fa fa-file-pdf-o fa-lg"></i></a>
 
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>