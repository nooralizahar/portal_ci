
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main"> <!-- awal main col-sm-9 -->
    <div class="row"> <!-- awal row 1-->
        <ol class="breadcrumb">
            <li>&nbsp;<a href=""><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url("panep/"); ?>">Dashboard</a></li>
            <li>&nbsp;<a href="<?php echo base_url("panep/keyorp/"); ?>">Procurement</a></li>
            <li class="active"> Proyek</li>
        </ol>
    </div><!-- akhir row 1-->

    <hr/>

    <div class="row"> <!-- awal row 2-->
      <div class="col-lg-12">
        <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                <div class="alert bg-danger" role="alert">
                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php elseif(isset($_GET["succ"])):?>
                <div class="alert bg-success" role="alert">
                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <?php endif; ?>
                <div class="panel-heading"> Form Proyek</div>
            
            <div class="panel-body">

                <div class="row">  <!-- awal row inner -->
                 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-info">

                        <div class="panel"> <!-- awal panel a -->
                           <div class="tabbable-panel">
                            <div class="tabbable-line">

                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation"><a href="#tabno" aria-controls="tabno" role="tab" data-toggle="tab">  Baru</a></li>
                                    <li role="presentation" class="active"><a href="#tabtu" aria-controls="tabtu" role="tab" data-toggle="tab"> Proyek</a></li>
                                </ul>



                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane" id="tabno">
                                        <?php $this->load->view("/panep/keyorp/tabnol")?>  
                                    </div>


                                    <div role="tabpanel" class="tab-pane active" id="tabtu">
                                        <?php $this->load->view("/panep/keyorp/tabsat")?> 
                                    </div>

                                </div>


                          </div>
                         </div>
                        </div> <!-- akhir panel a -->
                  </div>
                 </div>
                </div>  <!-- akhir row inner -->

            </div> <!-- akhir panel body -->

        </div> <!-- akhir panel default -->
      </div><!-- akhir col-lg-12 -->
    </div><!-- akhir row 2-->

</div>	<!-- akhir main col-sm-9-->


