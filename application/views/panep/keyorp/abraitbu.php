
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li>&nbsp;<a href="#"><i class="fa fa-home fa-lg"></i></a></li>
            <li>&nbsp;<a href="<?php echo base_url("panaf/"); ?>"> Dashboard</a></li>
            <li class="active"> Isi Keterangan</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data !<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsuc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data id# <?php echo $this->uri->segment(4); ?> berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menghapus data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h5> Keterangan Bukti Pengeluaran Bank</h5>
                </div>
        <div class="panel-body">

             <?php foreach($pitiarba as $b):?>
        <div class='form-group row'>
                <label for='lbldik' class='col-sm-2 form-control-label'>Dibayar kepada</label>
                <div class='col-sm-6'>
                    <input type='text' name='nama' class='form-control' id='nama' value="<?php echo $b->dik; ?>" readonly/>
                    <input type="hidden" name="idt" class="form-control" value="<?php $nid=$b->idt; echo $nid;  ?>" style="color:blue" readonly/>
                </div>

        </div>  
   
        <div class='form-group row'>
                <label for='lblsui' class='col-sm-2 form-control-label'>Supplier</label>
                <div class='col-sm-6'>
                       <input type='text' name='sui' class='form-control' id='sui' value="<?php echo $b->sui ?>" readonly/>

                </div>
        </div>



            
        <?php endforeach; ?>   

        <form class='add-item' method=POST action=''>
        	    <div class='form-group row'>
                <label for='lblket' class='col-sm-2 form-control-label'>Keterangan</label>
                <div class='col-sm-5'>
                	   <input type='hidden' name='idt' class='form-control' id='idt' value="<?php echo $nid;?>">	
                       <input type='text' name='ket' class='form-control' id='ket'>

                </div>
                <label for='lblnit' class='col-sm-1 form-control-label'>Nilai Rp.</label>
                <div class='col-sm-4'>
                       <input type='text' name='nit' class='form-control' id='nit'>

                </div>
        		</div>

                <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-10'>
                      <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('panaf/tiarbank/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                </div>
                
        </form>


	       <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="i"  data-sortable="true">No.</th>
                        <th data-field="q"  data-sortable="true">Keterangan</th>
                        <th data-field="v"  data-sortable="true">Nominal Rp.</th>
                        <th data-field="o"  data-sortable="true">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $n=1; foreach($pilketil as $l): ?>
                        <tr>
                            <td><?php echo $n++; ?></td>
                            <td><?php echo $l->ket; ?></td>
                            <td class="text-right"><?php echo number_format($l->nit); ?></td>


                            <td><center>
                                <a href="<?php echo base_url("panaf/haket/".$l->idk."/".$l->idt); ?>" data-toggle='tooltip' title='hapus'><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;  <!-- Edit ODP -->
    
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>
            <div class="pull-right" style="font-size: 18px">
            <?php if($this->db->where("idt",$nid)->get('fat_knabraul_ket')->num_rows()>0): ?>

            <table>
            <tr><td>Subtotal </td><td>&nbsp;Rp.&nbsp;</td><td align="right"><?php echo number_format($totalket->rp); ?></td></tr>
            <tr><td>PPN 10%  </td><td>&nbsp;Rp.&nbsp;</td><td align="right"><?php echo number_format($totalket->rp*0.1); ?></td></tr>
            <tr><td>PPH 23  </td><td>&nbsp;Rp.&nbsp;</td><td align="right"><?php echo number_format(-($totalket->rp*0.02)); ?></td></tr></h4>
            </table>
            <?php else:?>
            <?php endif;?>	
            </div>

        </div> <!-- akhir panel body-->
      </div>
    </div>

    </div>



</div>	<!--/.main-->

