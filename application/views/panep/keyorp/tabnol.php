
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">
    <form class='add-item' method=POST action='' enctype="multipart/form-data">
        <div class='form-group row'>
                <label for='lblkode' class='col-sm-2 form-control-label'>Kode Proyek </label>
                <div class='col-sm-4'>
                    <input type='text' name='kodpro' class='form-control' id='kodpro' required>
                    
                </div>
                <label for='lbljenis' class='col-sm-2 form-control-label'>Jenis Proyek </label>
                <div class='col-sm-4'>
                    <select name="jenpro" class="form-control" id="jenpro">
                        <option value="Barang"> Barang</option>
                        <option value="Jasa"> Jasa</option>        
                    </select>   
                </div>

        </div>

        <div class="form-group row">
                <label for='lbljudul' class='col-sm-2 form-control-label'>Judul Proyek </label>
                <div class='col-sm-10'>
                    <input type='text' name='judpro' class='form-control' id='judpro' required>    
                </div>
        </div>

        <div class='form-group row'>
                <label for='lblspek' class='col-sm-2 form-control-label'>Detil Spesifikasi</label>
                <div class='col-sm-10'>
                    <!-- <input type='text' name='spepro' class='form-control' id='spepro' required>-->
                    <textarea name="spepro" id="ckeditor" ></textarea>
                </div>

        </div>

        <div class='form-group row'>
                <label for='lblbatas' class='col-sm-2 form-control-label'>Batas Penawaran</label>
                <div class='col-sm-4'>
                    <input type='date' name='tglbatas' class='form-control' id='tglbatas'>
                </div>
                <label for='lblumum' class='col-sm-2 form-control-label'>Pengumuman</label>
                <div class='col-sm-4'>
                    <input type='date' name='tglumum' class='form-control' id='tglumum'>
                </div>
        </div> 

        <div class="form-group row">
                <label for='lblhps' class='col-sm-2 form-control-label'>HPS Proyek (Rp.)</label>
                <div class='col-sm-4'>
                    <input type='text' name='hpspro' class='form-control' id='hpspro' required>
                    
                </div>
                <label for='lbllam' class='col-sm-2 form-control-label'>Lampiran (.pdf)</label>
                <div class='col-sm-4'>
                    <input type="file" multiple id="lampro" name="lampro[]" class="form-control">
                    
                </div>            
        </div>
  


        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                <button type='button' onclick="window.location.href = '<?php echo base_url('panep/keyorp/') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
     </div>
  </div>
</div>
<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('spepro',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>
<script>
    $(document).ready(function(){
      
        $("#lampro").fileinput({'showUpload':false, 'previewFileType':'any'});

    })
</script>

