
<?php
$token = 'eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjE0NjQ5NTAxMywidWlkIjoyODE1NjIzOCwiaWFkIjoiMjAyMi0wMi0xN1QwOTo0OTozNi4wMDBaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6MTEyNjIxNjUsInJnbiI6InVzZTEifQ.5SkDj1CSbUyG2i1k66bnw-RLdfK_iZGhLllZPAha3pc';
$apiUrl = 'https://api.monday.com/v2';
$headers = ['Content-Type: application/json', 'Authorization: ' . $token];

//$query = '{ boards { id name } }';
$query = '{ boards (ids: 2306832861) { activity_logs { id event data }}}';
$query = 'mutation{ create_item (board_id:2306832861, item_name:"Tes Input dari portal") { id } }';
$data = @file_get_contents($apiUrl, false, stream_context_create([
 'http' => [
  'method' => 'POST',
  'header' => $headers,
  'content' => json_encode(['query' => $query]),
 ]
]));
$responseContent = json_decode($data, true);

echo json_encode($responseContent);
?>


