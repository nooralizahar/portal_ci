
<?php
$token = 'U1RSa2SPTciCELsd27JfUw';
//$apiUrl = 'https://api.zoom.us/v2/';
$apiUrl = 'https://api.zoom.us/v2/metrics/meetings/82562107675';
$headers = ['Content-Type: application/json', 'Authorization: ' . $token];

$query = '{ "host","topic" }';

$data = @file_get_contents($apiUrl, false, stream_context_create([
 'http' => [
  'method' => 'GET',
  'header' => $headers,
  'content' => json_encode(['query' => $query]),
 ]
]));
$responseContent = json_decode($data, true);

echo json_encode($responseContent);
?>


/* 

{
  "host": "cool host",
  "duration": "30:00",
  "email": "example@example.com",
  "end_time": "2007-06-16T16:59:42.078Z",
  "has_3rd_party_audio": false,
  "has_archiving": false,
  "has_pstn": false,
  "has_recording": false,
  "has_screen_share": false,
  "has_sip": false,
  "has_video": false,
  "has_voip": false,
  "id": 33281536,
  "in_room_participants": 3,
  "participants": 4874645,
  "start_time": "2007-06-16T16:55:42.078Z",
  "topic": "My meeting",
  "user_type": "Pro|Webinar1000",
  "uuid": "carreter@-2c9b447f3"
}
*/