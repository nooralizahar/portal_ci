<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $judul; ?></title>

    <!-- Custom fonts for this template-->
    <link href="<?= base_url('assets-nl/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('assets-nl/'); ?>css/sb-admin-2.min.css" rel="stylesheet">
    <!-- <link href="<?= base_url('assets-nl/'); ?>css/bootstrap-datepicker.css" rel="stylesheet"> -->
	<!-- Custom styles for this page -->
    <link href="<?= base_url('assets-nl/'); ?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="<?= base_url('assets-nl/'); ?>chained/select2.min.css" rel="stylesheet">
    <!-- Page level plugins -->
    <script src="<?= base_url('assets-nl/'); ?>vendor/chart.js/Chart.min.js"></script>
    <!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets-nl/'); ?>vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets-nl/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url('assets-nl/'); ?>chained/select2.min.js"></script>
    
</head>

<body id="page-top">
<!-- Page Wrapper -->
<div id="wrapper">

<!-- menu  -->
<ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url(); ?>">
        <div class="sidebar-brand-icon">
           <i class="fas fa-wifi"></i>
        </div>
        <div class="sidebar-brand-text mx-2">JLM PORTAL</div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <div id='showmenu'></div>

        
    <hr class="sidebar-divider d-none d-md-block">
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>

<script>
   $(document).ready(function() {
       $('#showmenu').load('<?php echo base_url()."menu" ?>');
        // $('select').select2();
   }); 
</script>