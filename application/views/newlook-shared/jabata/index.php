<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
    <div class="row" style="margin-bottom: 10px">       
        <div class="col-md-4 text-center">
            <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            </div>
        </div>
        
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> <?= strtoupper($judul); ?> </h6>
        </div>
        <div class="card-body">
            <div class="form-group">
                <div class="col-lg-8" >
                    <a href="#javascript.void(0)" class="btn btn-primary btn-icon-split add" data-toggle="modal" data-target="#EditModal">
                        <span class="icon text-white-50">
                            <i class="fas fa-user-plus"></i>
                        </span>
                        <span class="text">Baru</span>
                    </a>
                    <!--<a href="users/excel" class="btn btn-success btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-check"></i>
                        </span>
                        <span class="text">Export to excel</span>
                    </a>-->
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-sm table-hover" id="tabel"  cellspacing="0">
                    <thead>
                        <tr class="bg-primary text-light">
                            <th style="text-align: center;">ID#</th>
                            <th style="text-align: center;">Nama Jabatan</th>
                            <th style="width: 15%; text-align: center;">Pilihan</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($daftar_jabatan as $jabatan): ?>
                        <tr>
                            <td><?php echo $jabatan->id_jab; ?></td>
                            <td><?php echo $jabatan->nm_jab; ?></td>

                            <td>
                                <a href="<?php echo base_url($this->uriku."/jabatanedit/".$jabatan->id_jab); ?>" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                                <a href="<?php echo base_url("hirarki/detil/".$jabatan->id_jab); //.$jabatan->id_jab?>" class="btn btn-primary"><i class="fas fa-sitemap"></i></a>
                            </td>
                        </tr>
                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#tabel').DataTable();
} );
</script>