<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
    <div class="row" style="margin-bottom: 10px">       
        <div class="col-md-4 text-center">
            <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
            </div>
        </div>
        
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> <?= strtoupper($judul); ?> </h6>
        </div>
        <div class="card-body">
            <div class="form-group">
                <div class="col-lg-8" >
                    <a href="#javascript.void(0)" class="btn btn-primary btn-icon-split add" data-toggle="modal" data-target="#EditModal">
                        <span class="icon text-white-50">
                            <i class="fas fa-user-plus"></i>
                        </span>
                        <span class="text">Baru</span>
                    </a>
                    <!--<a href="users/excel" class="btn btn-success btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-check"></i>
                        </span>
                        <span class="text">Export to excel</span>
                    </a>-->
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-sm table-hover" id="tabel"  cellspacing="0">
                    <thead>
                        <tr class="bg-primary text-light">
                            <th class="text-center">ID#</th>
                            <th class="text-center">Parent ID</th>
                            <th class="text-center">Title</th>
                            <th class="text-center">URL</th>
                            <th class="text-center">View</th>
                            <th class="text-center">Is Active</th>
                            <th class="text-center">Icon</th>
                            <th class="text-center">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($daftar_akses as $d): ?>
                        <tr>
                            <td class="text-center"><?php echo $d->id; ?></td>
                            <td><?php echo $d->parent_id; ?></td>
                            <td><?php echo $d->title; ?></td>
                            <td><?php echo $d->url; ?></td>
                            <td><?php echo $d->view; ?></td>
                            <td><?php echo $d->is_active; ?></td>
                            <td><?php echo $d->icon; ?></td>
                            <td class="text-center">
                                <a href="<?php //echo base_url($this->uriku."/edit/".$d->id); ?>" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#tabel').DataTable();
} );
</script>