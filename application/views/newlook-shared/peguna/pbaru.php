
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
    <div class="row" style="margin-bottom: 10px">       
        <div class="col-md-4 text-center">
            <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('pesan') <> '' ? $this->session->userdata('pesan') : ''; ?>
            </div>
        </div>
        
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> FORM <?= strtoupper($judul); ?> </h6>
        </div>
        <div class="card-body">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" name="username" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Nomor Induk Karyawan</label>
                            <input type="text" name="nip" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label for="">Departemen</label>
                            <select name="id_div" id="pildivisi" class="form-control" required>
                                <option value=""></option>
                                <?php foreach($daftar_divisi as $divisi): ?>
                                    <option value="<?php echo $divisi->id_div; ?>"><?php echo $divisi->nm_div; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Jabatan</label>
                            <select name="id_jabatan" id="piljabatan" class="form-control" required>
                                <option value=""></option>
                                <?php foreach($daftar_jabatan as $jabatan):?>
                                    <option value="<?php echo $jabatan->id_jab; ?>"><?php echo $jabatan->nm_jab; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Atasan</label>
                            <select name="id_spv" id="pilatasan" class="form-control ">
                                <option value=""></option>
                                <?php foreach($daftas as $atasan):?>
                                    <option value="<?php echo $atasan->id_pengguna; ?>"><?php echo $atasan->nama_lengkap." &rarr; ".$atasan->nm_jab; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Dept. Head</label>
                            <select name="id_spv" id="pildephed" class="form-control ">
                                <option value=""></option>
                                <?php foreach($dafhed as $dh):?>
                                    <option value="<?php echo $dh->id_pengguna; ?>"><?php echo $dh->nama_lengkap." &rarr; ".$dh->nm_jab; ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">No. HP</label>
                            <input type="text" name="notifikasi" class="form-control">
                        </div>                        
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="text" name="email" class="form-control">
                        </div>  
              <!--          <div class="form-group">
                        <p>Paraf</p>
                            <input type="file" multiple id="attachtp" name="attachtp[]" class="form-control">
                        </div> -->
                        <button type="submit" name="btnSubmit" class="btn btn-info btn-icon-split add">
                        <span class="icon text-white-50">
                            <i class="far fa-save"></i>
                        </span>
                        <span class="text">Simpan</span>
                         </button>
                         <!--<button type="submit" name="btnSubmit" class="btn btn-info"><i class="far fa-save"></i></i><br><small><sup>SIMPAN</sup></small></button>
                         -->
                    </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#msg").froalaEditor({
            height: 300
        });
        $("#pilatasan").select2({
            placeholder: "Pilih atasan",
            allowClear: true
        });
        $("#penerima").select2();
        $("#piljabatan").select2({
            placeholder: "Pilih jabatan",
            allowClear: true
        });
        $("#pildivisi").select2({
            placeholder: "Pilih divisi",
            allowClear: true
        });

        $("#attachtp").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>
