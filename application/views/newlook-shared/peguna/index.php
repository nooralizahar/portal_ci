<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800"><?= $judul; ?></h1>
    <div class="row" style="margin-bottom: 10px">       
        <div class="col-md-4 text-center">
            <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('pesan') <> '' ? $this->session->userdata('pesan') : ''; ?>
            </div>
        </div>
        
    </div>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> <?= strtoupper($judul); ?> </h6>
        </div>
        <div class="card-body">

            <?php $this->load->view('newlook-shared/peguna/batas'); ?>
            <div class="form-group">
                <div class="col-lg-8" >

                    <a href="pengguna/baru/" class="btn btn-primary btn-icon-split add">
                        <span class="icon text-white-50">
                            <i class="fas fa-user-plus"></i>
                        </span>
                        <span class="text">Baru</span>
                    </a>
                    
                    <!--<button class='btn text-primary  border border-primary' onclick="tambah_pengguna()"><i class='fa fa-plus'></i> Baru</button>

                     <a href="users/excel" class="btn btn-success btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-check"></i>
                        </span>
                        <span class="text">Export to excel</span>
                    </a>-->
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-sm table-hover" id="tabel"  cellspacing="0">
                    <thead>
                        <tr class="bg-primary text-light">
                            <th style="text-align: center;">NIK</th>
                            <th style="text-align: center;">Nama Lengkap</th>
                            <th style="text-align: center;">Jabatan</th>
                            <th style="text-align: center;">Bagian</th>
                            <th style="text-align: center;">Leader</th>
                            <th style="width: 15%; text-align: center;">Pilihan</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($daftar_pengguna as $pengguna): ?>
                        <tr>
                            <td><?php echo $pengguna->nip; ?></td>
                            <td><?php echo $pengguna->nama_lengkap; ?></td>
                            <td><?php echo $pengguna->nm_jab; ?></td>
                            <td><?php echo $pengguna->nm_div; ?></td>
                            <td><?php echo $pengguna->nama_spv; ?></td>
                            <td class="text-center">
                                <a href="<?php echo base_url("pengguna/ubah/".$pengguna->id_pengguna); ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-title="Ubah Data"><i class="fas fa-user-edit"></i></a>&nbsp; <!-- Edit Pengguna -->
                                <?php if($pengguna->blokir == 0): ?>
                                    <a href="<?php echo base_url("pengguna/blokir/".$pengguna->id_pengguna); ?>" class="btn btn-warning btn-xs" data-toggle="tooltip" data-title="Blokir Pengguna"><i class="fas fa-user-lock"></i></a> <!-- Blokir Pengguna -->
                                <?php else: ?>
                                    <a href="<?php echo base_url("/pengguna/buka/".$pengguna->id_pengguna); ?>" class="btn btn-success btn-xs" data-toggle="tooltip" data-title="Buka Blokir"><i class="fas fa-key"></i></a> <!-- Buka Blokir -->
                                <?php endif;?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
  <div class="modal" id="Modal">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
        <p id="tampil_error"></p>
        <!-- Modal Header -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body" id="isi_modal">
        
        </div> 
      </div>
    </div> 
    </div> 
   <!-- The Modal -->



<script type="text/javascript">
$(document).ready(function() {
    $('#tabel').DataTable();
} );

        function tambah_pengguna(){ 
        var xhr = new XMLHttpRequest();
        xhr.open("POST","<?php echo base_url('pengguna/tambah_pengguna')?>", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function () {
            if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200){
                 document.getElementById("isi_modal").innerHTML = xhr.responseText;
                 setTimeout(function(){
                    new SlimSelect({
                      select: '#mitra_id'
                    })
                    new SlimSelect({
                      select: '#group'
                    })
                  }, 300)
                $("#Modal").modal("show");
            }
        }
        xhr.send(); 
    }



</script>