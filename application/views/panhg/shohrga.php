﻿
<?php
    $idjab=$this->session->userdata("id_jabatan");

?>


<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-list fa-3x"></i>
                        
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= number_format(0) ?></div>
                        <div class="text-muted"> Tahun ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-teal-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">

                        <i class="fa fa-database fa-3x"></i>

                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= number_format(0) ?></div>
                        <div class="text-muted"> Tahun lalu</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div> 








