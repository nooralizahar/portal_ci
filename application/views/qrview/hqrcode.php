<div class="container" style="margin-top:11px;">
    <div class="row">
      	<div class="col-md-6">

			<h4>Untuk Cek QRCode</h4>
			<br><br>
			  <?php if(isset($_GET["r"])): ?>
                <div class="alert bg-success" role="alert">
                	<center><img src='<?php echo base_url('/assets/qrfiles/').$_GET["r"];?>'></center>
                </div>
                <?php else:?>
                <div class="alert bg-danger" role="alert">

                </div>
                <?php endif; ?>
			<form action="<?php echo base_url('QrController/qrcodeGenerator');?>" method="post">
				<div class="form-group">
				    <label for="text">Masukan Teks Disini:</label>
				    <input type="text" class="form-control" name="qrcode_text">
			  	</div>
			  	<div class="form-group">
				   <button class="btn btn-default">Simpan</button>
			  	</div>
			</form>
		</div>
	</div>
</div>				

