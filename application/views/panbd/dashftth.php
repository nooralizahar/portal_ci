﻿
<?php
    $idjab=$this->session->userdata("id_jabatan");
    //$dver=$this->db->where("ver",1)->get("t_odp")->num_rows();
    //$dunv=$this->db->where("ver",0)->get("t_odp")->num_rows();

    $nkap=$this->db->select('SUM(kapasitas) as kapasitas, SUM(sisa) as sisa')->get("v_t_odc")->result();
      foreach ($nkap as $n) {
           $kap=$n->kapasitas;
           $sis=$n->sisa;
      }
    /*$tik=$this->dbz->order_by("t_net_odp_id","desc")->limit(1)->get("t_net_odp")->result();
        foreach($tik as $d): 
        $lastik=$d->tg_cre;   
        endforeach;*/

      foreach ($paktif as $a) {
           $jmlakt=$a->jml;
      }
      foreach ($pbloki as $b) {
           $jmlblo=$b->jml;
      }
      foreach ($ptermi as $t) {
           $jmlter=$t->jml;
      }
?>

    <ol class="breadcrumb">
          <li>DATA PELANGGAN FTTH <?php echo strtoupper(date('d M Y H:i:s')); 
            //echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></li>

    </ol>
<br/>
<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($capodp->cap); ?></div>
                        <div class="text-muted"> Kapasitas (port ODP)</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda-l2 panel-widget">
                <div class="row no-padding">
                    <!--<a href="<?php echo base_url("focmen/odpcs/"); ?>" >-->
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($jmlakt+$jmlblo); ?></div>
                        <div class="text-muted"> Pelanggan (iBoss)</div>
                    </div><!--</a>-->
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal-l2 panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php echo number_format($capodp->cap-($jmlakt+$jmlblo));?> </a></div>
                        <div class="text-muted"> Tersedia (port ODP)</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
    <ol class="breadcrumb">
          <li> RINCIAN PELANGGAN </li>

    </ol>
<br/>
<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                	<a href="<?php echo base_url("focmen/detpel/1"); ?>">

                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($jmlakt); ?></div>
                        <div class="text-muted"> Active</div>
                    </div></a>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    <a href="<?php echo base_url("focmen/detpel/2"); ?>">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($jmlblo); ?></div>
                        <div class="text-muted"> Blocked</div>
                    </div></a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                	<a href="<?php echo base_url("focmen/detpel/3"); ?>">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><?php echo number_format($jmlter);?></div>
                        <div class="text-muted"> Terminate</div>
                    </div></a>
                </div>
            </div>
        </div>
    </div>
</div> 


<div class="row">
    <div class="col-lg-12">
 <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $akttod ?></div>
                        <div class="text-muted"> Aktivasi Hari Ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-ungu-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clock"><use xlink:href="#stroked-clock"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $aktlas ?></div>
                        <div class="text-muted"> Aktivasi Kemarin</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
 <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $aktbni ?></div>
                        <div class="text-muted"> Aktivasi Bulan Ini</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clock"><use xlink:href="#stroked-clock"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $aktbla ?></div>
                        <div class="text-muted"> Aktivasi Bulan Lalu</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-lg-12">
    <ol class="breadcrumb">
          <li>DATA ODP </li>

    </ol>
    <br/>    
       <!-- <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $dver." (".number_format(($dver/($dver+$dunv))*100)." %)" ?></div>
                        <div class="text-muted"> ODP Verified</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-merah panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clock"><use xlink:href="#stroked-clock"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $dunv." (".number_format(($dunv/($dver+$dunv))*100)." %)" ?></div>
                        <div class="text-muted"> ODP Unverified</div>
                    </div>
                </div>
            </div>
        </div>-->

    </div>
</div> 



<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <a href="<?php echo base_url("focmen/odpin/"); ?>" >
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($totodp); ?></div>
                        <div class="text-muted"> ODP</div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <!--<a href="<?php echo base_url("focmen/odpcs/"); ?>" >-->
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($totodp); ?></div>
                        <div class="text-muted"> Active</div>
                    </div><!--</a>-->
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php echo number_format(0); ?> </a></div>
                        <div class="text-muted"> InActive</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 

<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked open folder"><use xlink:href="#stroked-open-folder"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($odpemp); ?></div>
                        <div class="text-muted"> 100% Kosong</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($odpbal); ?></div>
                        <div class="text-muted"> Sisa >= 50%</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-jingga-l2 panel-widget">
                <div class="row no-padding">
                    
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($odpcil); ?></div>
                        <div class="text-muted"> Sisa < 50%</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-3">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked stroked folder"><use xlink:href="#stroked-folder"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php echo number_format($odpful->jml);?> </a></div>
                        <div class="text-muted"> Sudah Penuh</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 



<div class="row">
    <div class="col-lg-12">
    	    <ol class="breadcrumb">
          <li>DATA ODC </li>

    </ol>
    <br/>   

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><a href="<?php //echo base_url("panel/word/$blnini"); ?>"> <?php echo number_format($this->db->get("t_odc")->num_rows()); ?></a></div>
                        <div class="text-muted" > ODC</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked upload"><use xlink:href="#stroked-upload"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($kap); ?></div>
                        <div class="text-muted"> Kapasitas</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked download"><use xlink:href="#stroked-download"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($sis); ?></div>
                        <div class="text-muted"> Sisa</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- AWAL SEMUA MODAL DISINI   --> 
<center></center>

<div class="modal fade" id="locvie0" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel">Location View
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 

                    $a=1;
                    foreach ($datlok as $lp) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; ".$lp->area_name." - ".$lp->name."</td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="locvie1" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel">Location View
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $a=1;
                    foreach ($lokper as $lp) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; ".$lp->area_name." - ".$lp->name."</td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="locvie2" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel">Location View
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $a=1;
                    foreach ($loklin as $lp) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; ".$lp->area_name." - ".$lp->name."</td></tr>";
                        $a++;
                    }
                    ?></table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>


<!-- AKHIR SEMUA MODAL DISINI   --> 