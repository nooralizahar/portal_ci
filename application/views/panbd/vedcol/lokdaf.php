<?php 
$ij=$this->session->userdata('id_jabatan');
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("panbd/"); ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panbd/locdev/"); ?>">Lokasi</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Lokasi <?php if($ij=='80'){echo "Proyek";}else{echo "Kompensasi";}?> </div>
                <div class="panel-body">
				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data Berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

<!-- isi awal -->

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">

                    <a href="#" data-toggle="modal" data-target="#locvie0">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($tlok-1); ?></div>
                        <div class="text-muted"> Lokasi</div>
                    </div></a>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    
                    <a href="#" data-toggle="modal" data-target="#locvie1">

                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($plok); ?></div>
                        <div class="text-muted"> Perumahan</div>
                    </div></a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-blue panel-widget ">
                <div class="row no-padding">

                    <a href="#" data-toggle="modal" data-target="#locvie2">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked landscape"><use xlink:href="#stroked-landscape"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php  $l=$tlok-$plok; echo number_format($l-1);?> </a></div>
                        <div class="text-muted">Lingkungan</div>
                    </div></a>
                </div>
            </div>
        </div>

<center><h5 style="color: red"><b>Untuk menambah Data Lokasi, silahkan gunakan <a href="https://app.bnetfit.com" target="_blank">Aplikasi iBoss</a></b></h5></center>    

       <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="a" data-sort-order="asc">
                    <thead>
                    <?php if($ij=='67' || $ij=='80' ):?>
                    <tr>
                        <th data-field="a"  data-sortable="true">Lokasi</th>
                        <th data-field="b"  data-sortable="true">Diverifikasi</th>
                        <th data-field="c"  data-sortable="true">Tgl. Verifikasi</th>                      
                        <th data-field="d"  data-sortable="true">Disetujui</th> 
                        <th data-field="e"  data-sortable="true">Tgl. Persetujuan</th>  
                        <th>Status</th>     
                        <th>Opsi</th>
                    </tr>
                    <?php else:?>
                    <tr>
                        <th data-field="a"  data-sortable="true">Lokasi</th>
                        <th data-field="b"  data-sortable="true">Alamat</th>
                        <th data-field="c"  data-sortable="true">Area</th>                       
                        <th data-field="d"  data-sortable="true">Kota</th> 
                        <th data-field="e"  data-sortable="true">Provinsi</th>  
                        <th>Status</th>     
                        <th>Opsi</th>
                    </tr>
                    <?php endif; ?>
                    </thead>
                    <tbody>
                        <?php
                        if($ij=='67' || $ij=='80'){$dt=$dlokas;}else{$dt=$datlok;}

                         foreach($dt as $dl): ?>
                        <?php if($ij=='67' || $ij=='80'):?>
                          <tr>
                            <td><?php echo lokcek($dl->idlok); ?></td>
                            <td><?php echo naleng($dl->usver); ?></td>
                            <td><?php echo $dl->tgver; ?></td>
                            <td><?php echo naleng($dl->usapv); ?></td>
                            <td><?php echo $dl->tgapv; ?></td>
                            <td></td>
                            <td><?php if($ij=='80'){
                                echo "<a href=".base_url('panbd/kelbrg/'.$dl->idlok)." data-toggle='tooltip' data-placement='top' title='Barang Keluar'><i class='fa fa-shopping-cart fa-lg' style='color:green'></i></a>";
                                }else {} ?></td>
                          </tr>
                        <?php else:?>    
                        <tr>
                            <td><?php echo $dl->name; ?></td>
                            <td><?php echo $dl->address; ?></td>
                            <td><?php echo $dl->area_name; ?></td>
                            <td><?php echo $dl->city_name; ?></td>
                            <td><?php echo $dl->province_name; ?></td>
                            <td><?php echo stacek($dl->t_location_id); ?></td>
                            <td>
                              <?php echo "<center>";
                                if($ij=='63' || $ij=='64'){
                                echo"<a href=".base_url('panbd/ubalok/'.$dl->t_location_id)." data-toggle='tooltip' data-placement='top' title='Ubah Status'><i class='fa fa-pencil-square-o fa-lg' style='color:green'></i></a>&nbsp;&nbsp;";
                                }elseif($ij=='65'){
                                echo "<a href=".base_url('panbd/tamkom/'.$dl->t_location_id)." data-toggle='tooltip' data-placement='top' title='Tambah Kompensasi'><i class='fa fa-plus fa-lg' style='color:green'></i></a>";
                                }elseif($ij=='61' || $ij=='62' || $ij=='66' || $ij=='60'){
                                echo "<a href=".base_url('panbd/tamkom/'.$dl->t_location_id)." data-toggle='tooltip' data-placement='top' title='Lihat Kompensasi'><i class='fa fa-eye fa-lg' style='color:green'></i></a>";	
                                }

                              echo "</center>";
                              ?>
                              
                            </td>
                        </tr>
                        <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
            </table>



<!-- isi akhir -->

                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<div class="modal fade" id="locvie0" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel">Location View
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 

                    $a=1;
                    foreach ($datlok as $lp) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; ".$lp->area_name." - ".$lp->name."</td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>



<div class="modal fade" id="locvie1" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel">Location View
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $a=1;
                    foreach ($lokper as $lp) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; ".$lp->area_name." - ".$lp->name."</td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="locvie2" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel">Location View
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $a=1;
                    foreach ($loklin as $lp) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; ".$lp->area_name." - ".$lp->name."</td></tr>";
                        $a++;
                    }
                    ?></table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>



<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('immtc',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>


    $(document).ready(function(){


        $(".req").select2();


        $(".tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>