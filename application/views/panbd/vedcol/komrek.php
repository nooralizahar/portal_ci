<?php

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("panbd/"); ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panbd/"); ?>"> Penerima</a></li>
            <li class="active"> Data</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="import">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4> DATA PENERIMA</h4>
                </div>
        <div class="panel-body">
    
    <div class="row"> 
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-info">

                <?php
                if ($this->session->userdata('logged_member_level') == 1) {
                    $class = "panel-collapse collapse";
                    ?>
                    <?php
                } else {
                    $class = "panel-collapse collapse in";
                }
                ?>
                <div id="collapseOne" class="<?php echo $class; ?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="#requester" aria-controls="requester" role="tab" data-toggle="tab"> Baru</a></li>
                            <li role="presentation" class="active"><a href="#problem" aria-controls="problem" role="tab" data-toggle="tab">Daftar Penerima</a></li>
                        </ul>
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal menyimpan data.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsucc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal dihapus.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>


<div class="tab-content">
  <div role="tabpanel" class="tab-pane" id="requester">
    <div class="panel-body">
        <div class="panel panel-primary">
           <div class="panel-body">
        <form class='add-item' method=POST action=''>

        <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Lokasi </label>
                <div class='col-sm-6'>
                    <select name="idlok" id="idlok" class="form-control lokasi" required>
                                <option value=""></option>
                                <?php foreach($datlok as $l):?>
                                    <option value="<?php echo $l->t_location_id; ?>"><?php echo $l->area_name." - ".$l->name; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
        </div> 
        <div class='form-group row'>
                <label for='lblkordinat' class='col-sm-2 form-control-label'>Bank</label>
                <div class='col-sm-7'>
                    <select name="bank" id="bank" class="form-control bank">
                                <option value=""></option>
                                <?php foreach($dabank as $b):?>
                                    <option value="<?php echo $b->kode; ?>"><?php echo $b->kode." - ".$b->nama; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
                
        </div>     
        <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>No. Rek</label>
                <div class='col-sm-6'>
                    <input type='text' name='norek' class='form-control' id='norek' placeholder="Ketik no. rekening" required>
                </div>
        </div> 
 
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>Nama Penerima</label>
                <div class='col-sm-6'>
                    <input type='text' name='penerima' class='form-control' id='penerima' placeholder="Ketik nama sesuai no. rekening"required>
                    
                </div>

        </div>
 



   
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                <button type='button' onclick="kembali()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
             </div>
         </div>
     </div>
</div>


<div role="tabpanel" class="tab-pane active" id="problem">
        <div class="panel-body">
            <div class="panel panel-primary">
               <div class="panel-body">
<div class="row">
        <ol class="breadcrumb">
          <li><a href="#"><?php 
            //echo "<small>Data Terakhir : ". date("d-m-Y H:i:s", strtotime($lastik))."</small>";
         ?></a></li>

        </ol>
        <div class="row no-padding">
            <div class="col-xs-12 col-md-6 col-lg-2">  </div>
        </div>
        <hr/>




        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->get('v_odp_rek_kom')->num_rows()); ?></div>
                        <div class="text-muted"> Rekening</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <a href="<?php echo base_url("focmen/odpcs/"); ?>" >
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->get('v_odp_rek_kom')->num_rows()); ?></div>
                        <div class="text-muted"> Perumahan</div>
                    </div></a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked line graph"><use xlink:href="#stroked-line-graph"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php  $l=$tlok-$plok; echo number_format($l);?> </a></div>
                        <div class="text-muted"><small>Lingkungan</small></div>
                    </div>
                </div>
            </div>
        </div>


</div> <br>

    <br>

    <br>

        <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="b" data-sort-order="asc">
                    <thead>
                    <tr>
                        <th data-field="a"  data-sortable="true">#</th>
                        <th data-field="b"  data-sortable="true">Lokasi</th>
                        <th data-field="c"  data-sortable="true">Bank</th>
                        <th data-field="d"  data-sortable="true">No. Rekening</th>
                        <th data-field="e"  data-sortable="true">Penerima</th>

                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach($datrek as $dat): ?>
                        <tr>
                            <td><?php echo $dat->id; ?></td>
                            <td><?php echo lokcek($dat->idlok); ?></td>
                            <td><?php echo $dat->sink; ?></td>
                            <td><?php echo $dat->norek; ?></td>
                            <td><?php echo $dat->penerima; ?></td>
                            

                        </tr>
                        <?php endforeach; ?>
                    </tbody>
            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>








                </div>
            </div>
        </div>
    </div>          








        </div>
      </div>
    </div>

    </div><!--/.row end-->


</div>	<!--/.main-->

<script>
function Kembali() {
  window.history.back();
}
</script>
<script>


    $(document).ready(function(){

        $(".lokasix").select();
        $(".bankx").select2();


    })
</script>