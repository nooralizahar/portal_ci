<?php

?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("panbd/"); ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panbd/locdev/"); ?>">Lokasi</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Ubah Status Lokasi</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data Berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

<!-- isi awal -->
            <h3 >&nbsp;&nbsp;<span class="label label-warning"><?php echo " ID # ".$idm;?></span></h3>
            
           <div class="panel-body">
   
        <?php 

        //echo $lokpil->t_location_id."<br>";
        echo "<h4>".$lokpil->area_name." ".$lokpil->name."</h4>";
        
        ?>
 
        <form class='add-item' method=POST action=''>
            <?php if($this->db->where('idlok',$idm)->get('t_odp_lok')->num_rows()>0){
                $tgs=$sta->tgsur;
                $uss=$sta->ussur;
                $tgi=$sta->tgins;
                $usi=$sta->usins; 
                $tgr=$sta->tgrfs;
                $usr=$sta->usrfs;               
            }else{
                $tgs=null;
                $uss=null;
                $tgi=null;
                $usi=null;
                $tgr=null;
                $usr=null;

            }; ?>
         <div class='form-group row'>
                <label for='lblitem' class='col-sm-2 form-control-label'>Tgl. Survey</label>
                <div class='col-sm-4'>
                    <input type='hidden' name='idlok' class='form-control' value="<?php echo $idm; ?>">
                    <input type='date' name='tgsur' class='form-control' value="<?php echo $tgs; ?>" required/>
                    <input type='hidden' name='ussur' class='form-control' value="<?php echo $uss; ?>" >
                    
                </div>

        </div> 
        <div class='form-group row'>
                <label for='lblqty' class='col-sm-2 form-control-label'>Tgl. Instalasi</label>
                <div class='col-sm-4'>
                    <input type='date' name='tgins' class='form-control' value="<?= $tgi ?>" >
                </div>
        </div>   
        <div class='form-group row'>
                <label for='lblqty' class='col-sm-2 form-control-label'>Tgl. RFS </label>
                <div class='col-sm-4'>
                    <input type='date' name='tgrfs' class='form-control' value="<?= $tgr ?>" >
                </div>
        </div>        

        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>
                <button type='button' onclick="goBack()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
            </div>
        </div>
                
        </form>
    </div>



<!-- isi akhir -->

                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>  <!--/.main-->




<script>

    function goBack() {
  		window.history.back();
	}

    function hitmenit() {

    var wak1 = document.getElementById("jmmtc").value;
    var wak2 = document.getElementById("jsmtc").value;
    var wa = wak1.split(":");
    var wb = wak2.split(":");
    var t1= new Date(2020,10,21,wa[0],wa[1]);
    var t2= new Date(2020,10,21,wb[0],wb[1]);
    var di=t2.getTime() - t1.getTime();
    var menit = Math.floor(di / 1000 / 60);

   
        document.getElementById("dntime").value=menit;
    }
</script>