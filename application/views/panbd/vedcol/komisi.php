<?php
$idjab=$this->session->userdata("id_jabatan");
$usrnm=$this->session->userdata("username");


?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("panbd/"); ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/panbd/"); ?>">Lokasi</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Kompensasi Baru</div>
                <div class="panel-body">
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Verifikasi data gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Verifikasi berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

<p><?php if($this->session->userdata('id_jabatan')=='65'): ?></p>
    
        <form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lblitem' class='col-sm-2 form-control-label'>Tgl. BAK</label>
                <div class='col-sm-4'>
                    <input type='hidden' name='idlok_kom' class='form-control' value="<?php echo $id; ?>">
                    <input type='date' name='tgbak' class='form-control' required/>
                    
                </div>

                <label for='lblqty' class='col-sm-2 form-control-label'>Tgl. Pencairan </label>
                <div class='col-sm-4'>
                    <input type='date' name='tgcai' class='form-control' required/>
                </div>
         </div>   
         <div class='form-group row'>
                <label for='lblhgven' class='col-sm-2 form-control-label'>Perihal </label>
                <div class='col-sm-8'>
                    <input type='text' name='perihal' class='form-control' placeholder="Ketik perihal disini" required/>
                </div>

         </div>      
         <div class='form-group row'>
                <label for='lblhgven' class='col-sm-2 form-control-label'>Nilai (Rp.)</label>
                <div class='col-sm-4'>
                    <input type='text' name='nikom' class='form-control' placeholder="Ketik nilai disini" required/>
                </div>

        </div>  
        <div class='form-group row'>
                <label for='lblket' class='col-sm-2 form-control-label'>Keterangan </label>
                <div class='col-sm-8'>
                    <input type='text' name='cakom' class='form-control' placeholder="Ketik keterangan disini maks. 250 karakter" required/>
                </div>
        </div>        
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="kembali()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button> <!-- "window.location.href = '<?php //echo base_url('focmen/') ?>';"-->
            </div>
        </div>
                
        </form>
<?php endif; ?>
        
            <center> 
                <h4>----- KOMPENSASI -----</h4> 
                <h4><?php echo $datlok->area_name." - ".$datlok->name; ?></h4>

            </center>
       
        </div> <!-- end 1st row -->

    <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                    <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="a" data-sort-order="asc">
                                <thead>
                                
                                <tr>
                                    <th data-field="a"  data-sortable="true" class="text-center">#</th>
                                    <th data-field="f"  data-sortable="true" class="text-center">Perihal</th>
                                    <th data-field="b"  data-sortable="true" class="text-center">Tgl. BAK</th>
                                    <th data-field="c"  data-sortable="true" class="text-center">Tgl. Pencairan</th>
                                    <th data-field="d"  data-sortable="true" class="text-center">Nilai Kompensasi (Rp.)</th>
                                    <th data-field="e"  data-sortable="true" class="text-center">Keterangan</th>                                   

                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($datkom as $k): ?>
                                    <tr>
                                        <td class="text-center"><?php echo $k->id; ?></td>
                                        <td class="text-left"><?php echo $k->perihal; ?></td>
                                        <td class="text-center"><?php echo $k->tgbak; ?></td>
                                        <td class="text-center"><?php echo $k->tgcai; ?></td>
                                        
                                        <td class="text-right"><?php echo number_format($k->nikom); ?></td>
                                        <td class="text-left"><?php echo $k->cakom; ?></td>

                                    </tr>
                                     
                                    <?php endforeach; ?>

                                </tbody>
                        </table>
<br><br>
            <center> 
                <h4>No. Rek. Pencairan :</h4> 
                <?php if(isset($datrek->norek)):?>
                
                <h4><?php echo $datrek->norek." - a/n ".$datrek->penerima."<br>".$datrek->sink;?></h4>
                <p></p>  
                <?php else :?>
                <h4 style="color: red">Data tidak tersedia</h4>
                <?php endif;?>              
 
                <br>
               
                <h4>Daftar ODP  :</h4> 
                <?php if($datodp==0){
                    echo "<h4 style='color: red'>Data tidak tersedia</h4>";
                }else{ ?>
                <h5 style="color: blue"><?php 
                foreach ($datodp as $o) {
                    echo $o->nama.", ";

                }}
                ?></h5>
                <p><br></p>  
               

            </center>

              <center>
                <h4> Hasil IoM : </h4> 
              </center>
              <table width="100%">
                <?php if(isset($datiom->fmjlm)):?>
                <tr><td width="15%">Dari JLM  </td><td><?php echo "&nbsp;:&nbsp;".$datiom->fmjlm; ?> </td></tr>
                <tr><td width="15%">Dari Perum./Lingk. </td><td><?php echo "&nbsp;:&nbsp;".$datiom->fmpeli; ?> </td></tr>
                <tr><td width="15%">Perihal </td><td><?php echo "&nbsp;:&nbsp;".$datiom->perihal; ?> </td></tr>                
                <tr><td width="15%">Tgl./Waktu </td><td><?php echo "&nbsp;:&nbsp;".$datiom->tgmom." / ".$datiom->jmmom." - ".$datiom->jsmom; ?> </td></tr>
                <tr><td>Hasil Rapat </td><td>&nbsp;:&nbsp;</td></tr>
                <tr><td colspan="2"><?php echo "<br>".$datiom->uraian; ?> </td></tr>
                <tr><td></td><td></td></tr>
                <?php else :?>
                <tr><td colspan="2" class="text-center"><h4 style="color: red">Data tidak tersedia</h4></td></tr>
                <?php endif;?>
              </table>

                        <p><br></p>
                        <table width="100%">

                        <?php if($hitsta>0):?>

                        <?php if($datver->usver!=null):?>

                            <tr style="text-align: center">
                                <td colspan="3"><i class="fa fa-check-circle-o fa-lg fa-4x" style="color:green"></i><h5> Sudah di verifikasi oleh : <?= naleng($datver->usver) ?> pada <?= $datver->tgver ?></h5></td>
                            </tr>
                        <?php endif; ?>

                        <?php if($datver->usapv!=null):?>

                            <tr style="text-align: center">
                                <td colspan="3"><i class="fa fa-check-circle-o fa-lg fa-4x" style="color:green"></i><h5> Pencairan sudah disetujui oleh : <?= naleng($datver->usapv) ?> pada <?= $datver->tgapv ?></h5></td>
                            </tr>
  
                        <?php endif; ?>


                        <?php else: ?>
                        	<tr style="text-align: center">
                                <td colspan="3"><i class="fa fa-circle-o fa-lg fa-4x" style="color:red"></i><h4><span class='blinkred'> Data belum diverifikasi oleh verifikator</span></h4></td>
                            </tr>
                        <?php endif; ?>

                        <?php if($usrnm=="rama.r"):?>
                        	<?php if(isset($datver->usver)):?>

                        	<?php else: ?>
                                <tr style="text-align: center">
                                    <td colspan="3"><a id="selesai2" class="btn btn-block btn-success" href=""><i class="fa fa-check fa-lg"></i> Verified</a></td>
                                </tr>
                            <?php endif;?>
                        <?php endif;?>

                            <?php if($usrnm=="aldhin"):?>
	                        	<?php if($datver->usver!=null):?>

	                        	<?php else: ?>                            	
	                            <tr style="text-align: center">
	                                    <td colspan="3"><a id="selesai3" class="btn btn-block btn-success" href=""><i class="fa fa-check fa-lg"></i> Setujui Pencairan</a></td>
	                            </tr>
	                            <?php endif;?>
                            <?php endif;?>
                       




                             </table>  
                             <p><br><br></p> 


<?php if($usrnm=="rama.r"):?>
    <form action="" method="post" style="display: none;">
        <input type="hidden" name="usr" id="usr" value="<?php echo $usrnm; ?>">
        <input type="hidden" name="password" id="confirmPassword2">
        <input type="submit" name="selesai2BtnSubmit" id="selesai2BtnSubmit">
    </form>
<?php elseif($usrnm=="aldhin"):?>
    <form action="" method="post" style="display: none;">
        <input type="hidden" name="usr" id="usr" value="<?php echo $usrnm; ?>">
        <input type="hidden" name="password" id="confirmPassword3">
        <input type="submit" name="selesai3BtnSubmit" id="selesai3BtnSubmit">
    </form>
<?php else: ?>
    <form action="" method="post" style="display: none;">
        <input type="hidden" name="password" id="confirmPassword">
        <input type="submit" name="selesaiBtnSubmit" id="selesaiBtnSubmit">
    </form>

<?php endif;?>
</div>  <!--/.main-->

<script>
    $(document).ready(function(){

        $("#selesai").on("click",function(ev){

            swal({
                title: "Konfirmasi Pesetujuan",
                text: "Masukkan sandi untuk konfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword").val(input);
                $("#selesaiBtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });

        $("#selesai2").on("click",function(ev){

            swal({
                title:"Konfirmasi Verifikasi",
                text: "Masukkan sandi untuk konfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword2").val(input);
                $("#selesai2BtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });

        $("#selesai3").on("click",function(ev){

            swal({
                title:"Konfirmasi Pesetujuan",
                text: "Masukkan sandi untuk konfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword3").val(input);
                $("#selesai3BtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });        

        /*$("#tolak").on("click",function(ev){

            swal({
                title:"Konfirmasi Penolakan",
                text: "Masukkan sandi untuk konfirmasi akun",
                type: "prompt",
                showCancelButton: true,
                closeOnConfirm: false
            },function(input){
                if(input === false) return false;

                if(input == "") {
                    swal.showInputError("Masukkan password anda!");
                    return false;
                }
                $("#confirmPassword").val(input);
                $("#tolakBtnSubmit").trigger("click");
            });

            $(".sweet-alert fieldset input").prop("type","password");

            ev.preventDefault();
        });*/

    });
</script>



<script>
function kembali() {
  window.history.back();
}
</script>