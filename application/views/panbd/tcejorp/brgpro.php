<?php
$idjab=$this->session->userdata("id_jabatan");
$usrnm=$this->session->userdata("username");


?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panel/panbd/"); ?>">Lokasi</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Pengeluaran Barang</div>
                <div class="panel-body">
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg>Data gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>


    
        <form class='add-item' method=POST action=''>
 
        <div class='form-group row'>
                <label for='lblitem' class='col-sm-2 form-control-label'>Tgl. Permintaan</label>
                <div class='col-sm-4'>
                    <input type='hidden' name='idlok' class='form-control' value="<?php echo $id; ?>">
                    <input type='date' name='tgmin' class='form-control' required/>
                    
                </div>

                <label for='lblqty' class='col-sm-2 form-control-label'>Tgl. Keluar </label>
                <div class='col-sm-4'>
                    <input type='date' name='tgkel' class='form-control' required/>
                </div>
         </div>   
         <div class='form-group row'>
                <label for='lblhgven' class='col-sm-2 form-control-label'>Nama Barang </label>
                <div class='col-sm-8'>
                    <input type='text' name='nmbrg' class='form-control' placeholder="Ketik Barang" required/>
                </div>

         </div>      
         <div class='form-group row'>
                <label for='lblhgven' class='col-sm-2 form-control-label'>Jumlah Keluar</label>
                <div class='col-sm-2'>
                    <input type='text' name='jlkel' class='form-control' placeholder="Ketik jumlah disini" required/>
                </div>
                <div class='col-sm-1'>
                    <select name="satuan" class="form-control sat" required="/">
                                <option value="m"> meter</option>
                                <option value="r"> roll</option>
                                <option value="p"> pieces</option>
                                <option value="u"> unit</option>
                                <option value="b"> batang</option>
                    </select>
                </div>

        </div>  
        <div class='form-group row'>
                <label for='lblket' class='col-sm-2 form-control-label'>Keterangan </label>
                <div class='col-sm-8'>
                    <input type='text' name='cabrg' class='form-control' placeholder="Ketik keterangan disini maks. 250 karakter" required/>
                </div>
        </div>        
        <div class='form-group row'>
            <label for='tombol' class='col-sm-2 form-control-label'></label>
            <div class='col-sm-10'>
                <button type="submit" name="btnSimpan" class="btn btn-primary"><i class="fa fa-plus-circle fa-lg"></i><br><small><sup>TAMBAH</sup></small></button>
                <button type='button' onclick="kembali()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button> <!-- "window.location.href = '<?php //echo base_url('focmen/') ?>';"-->
            </div>
        </div>
                
        </form>

        
        </div> <!-- end 1st row -->
<center><h4> PROYEK <?php echo strtoupper(lokcek($id)); ?> <br>BARANG KELUAR</h4></center>
    <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                    <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="toolbar1" data-pagination="false" data-sort-name="a" data-sort-order="asc">
                                <thead>
                                
                                <tr>
                                    <th data-field="a"  data-sortable="true" class="text-center">#</th>
                                    <th data-field="b"  data-sortable="true" class="text-center">Tgl. Permintaan</th>
                                    <th data-field="c"  data-sortable="true" class="text-center">Tgl. Keluar</th>
                                    <th data-field="d"  data-sortable="true" class="text-center">Barang</th>
                                    <th data-field="e"  data-sortable="true" class="text-center">Jumlah</th>
                                    <th data-field="f"  data-sortable="true" class="text-center">Satuan</th>
                                    <th data-field="g"  data-sortable="true" class="text-center">Keterangan</th>                                   

                                </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($datbrg as $k): ?>
                                    <tr>
                                        <td class="text-center"><?php echo $k->id; ?></td>
                                        <td class="text-center"><?php echo tgl_indome($k->tgmin); ?></td>
                                        <td class="text-center"><?php echo tgl_indome($k->tgkel); ?></td>
                                        <td class="text-left"><?php echo $k->nmbrg; ?></td>
                                        
                                        <td class="text-right"><?php echo number_format($k->jlkel); ?></td>
                                        <td class="text-left"><?php echo satuan($k->satuan); ?></td>
                                        <td class="text-left"><?php echo $k->cabrg; ?></td>

                                    </tr>
                                     
                                    <?php endforeach; ?>

                                </tbody>
                        </table>
<br><br>




 



</div>  <!--/.main-->
<script>


    $(document).ready(function(){


        $(".sat").select2();


    })
</script>

<script>
function kembali() {
  window.history.back();
}
</script>