
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panbd/"); ?>"> Laporan</a></li>
            <li class="active"> Bulanan</li>
        </ol>
    </div><!--/.row-->

    <hr/>
<div class="row"></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default" >

                <div class="panel-heading">LAPORAN BULANAN</div>
                <div class="panel-body">
        <form class='add-item' method=POST action=''>
        <div class='form-group row'>
                <label for='lbltgl' class='col-sm-1 form-control-label'>Bulan</label>
                <div class='col-sm-2'>
                <select name="tgl" id="tgl" class="form-control" >
                                <option value="<?php echo date('Y')."-01-01"; ?>"> Jan <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-02-01"; ?>"> Feb <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-03-01"; ?>"> Mar <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-04-01"; ?>"> Apr <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-05-01"; ?>"> Mei <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-06-01"; ?>"> Jun <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-07-01"; ?>"> Jul <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-08-01"; ?>"> Agu <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-09-01"; ?>"> Sep <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-10-01"; ?>"> Okt <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-11-01"; ?>"> Nop <?php echo "&nbsp;".date('Y'); ?></option>
                                <option value="<?php echo date('Y')."-12-01"; ?>"> Des <?php echo "&nbsp;".date('Y'); ?></option>
                </select>

                </div>
                <div class='col-sm-4'>
                    <button type="submit" name="btnGen" class="btn btn-success"><i class="fa fa-eye fa-lg"></i> LIHAT
                    </button>   
                </div>

        </div>
        </form>

           <div class="col-lg-12" id="lbrcetakbl">
            <table border="0" width="100%">
                <tbody>
                    <tr><td align="center" colspan="7">PT. JALA LINTAS MEDIA 
                    </td></tr>
                   <hr/>
                    <tr>
                        <td align="center" colspan="4"> <h4>LAPORAN PENCAIRAN BULANAN </h4><?//= tgl_indopj($tgl);?></td>
                    </tr>
                    <tr>
                       <td align="right" colspan="4"><small><sup>&nbsp;Dicetak tanggal&nbsp;:&nbsp; <?= date('d-M-Y H:s');?>&nbsp;</sup></small></td> 
                    </tr>

                    <tr>
                       <td align="center" colspan="4">

                        <?php if(!empty($datlap)):?>
                           <table border="1" width="100%">
                            <thead><th width="5%"><center>No.</center></th><th width="25%">&nbsp;<center>Perum./Lingk.</center></th><th width="5%">&nbsp;<center>Status</center></th><th width="10%"><center>Tgl. BAK</center></th><th width="10%"><center>Tgl. Pencairan</center></th><th width="15%">&nbsp;<center>Nilai Kompensasi (Rp.)</center></th><th width="10%"><center>Tgl. RFS</center></th><th width="10%"><center>ODP</center></th><th width="10%"><center>Keterangan</center></th></thead>
                            <tbody>
                                <?php $n=1; foreach($datlap as $d):?>
                                    <tr>
                                    <td align="center"><?= $n; ?></td>
                                    <td><?= "&nbsp;".lokcek($d->idlok); ?></td>
                                    <td class="text-center"><?= "&nbsp;".stalap($d->idlok); ?></td>
                                    <td class="text-center"><?= tgl_indosm($d->tgbak); ?></td>
                                    <td class="text-center"><?= tgl_indosm($d->tgcai); ?></td>
                                    <td class="text-right"><?= number_format($d->nikom).",-"; ?></td>
                                    <td align="center"><?= $d->tgrfs; ?></td>
                                    <td align="center"><?= $d->odp; ?></td>
                                    <td align="center"><?= $d->cakom; ?></td>
                                    
                                    </tr>
                                <?php $n++; endforeach;?>    
                            </tbody>       
                           </table>

                           <?php else :?>
                           <h4 style="color: red">Data tidak tersedia</h4>
                           <?php endif;?> 

                       </td> 
                    </tr>

</tbody></table>


    </div> 
                    

                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->


