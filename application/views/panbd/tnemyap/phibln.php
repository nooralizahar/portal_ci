<?php 
$dat=$this->db->order_by('myid','desc')->limit(1)->get('rad_pay')->row();
$ld=$dat->inputdate;

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panbd/"); ?>"> Dashboard</a></li>
            <li class="active"> Data Pembayaran</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-primary"> <a href="#" id="tklik">Data Pembayaran</a></h4>

                </div>
        <div class="panel-body">
    

    <form class='add-item' method=POST action=''>
        <div class='form-group row'>

                <label for='lbliddat' class='col-sm-2 form-control-label'>Laporan </label>
                <div class='col-sm-2'>
                    <select name="crbln" id="crbln" class="form-control nmpil">
                                <option value="202103">MAR 2021</option>
   								<option value="202102">FEB 2021</option>
   								<option value="202101">JAN 2021</option>
                    </select>
                </div>
                <div class='col-sm-4'>              
                    <button type="submit" name="btnCari" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;<small>GENERATE</small></button>
                    <?php if(empty($pil)){}else{ ?>
                    &nbsp;<a href="<?php echo base_url("panbd/lapblnan/".$pil); ?>" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg"></i>&nbsp;<small>TO XLS</small></a><?php } ?>
                </div>

        </div> 

    </form>
          	        <small style='color:white; background-color:green;'>&nbsp;Data terakhir : <?php echo $ld; ?>&nbsp;</small>


            <div class="form-group"> 
                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="17"><center><h3>LAPORAN BULAN </h3></center></th></tr>
                            <tr>

                                <th>&nbsp;No.</th>
                                <th><center>USER ID</center></th>
                                <th><center>NAMA</center></th>
                                <th><center>PAKET</center></th>
                                <th><center>TGL. BAYAR</center></th>
                                <th><center>PERIODE</center></th>
                                <th><center>BULANAN</center></th>
                                <th><center>ADMIN</center></th>
                                <th><center>PPN</center></th>
                                <th><center>BAYAR</center></th>
                                <th><center>KASIR</center></th>

                            </tr>
                            </thead>

                            <tbody>
                            <?php 
                            
                            $no=1;
                            foreach($detpaid as $i): ?>
                                <tr >
                                    <td align="right"><?php echo $no++; ?> </td>
                                    <td align="center"><?php echo $i->username; ?> </td>
                                    <td><?php echo $i->name; ?> </td>
                                    <td align="center"><?php echo $i->packet; ?> </td>
                                    <td><?php echo $i->pay_date; ?> </td>
                                    <td align="center"><?php echo $i->bln; ?> </td>
                                    <td align="right"><?php if(substr($i->name,0,2)=='R-'){$p=$i->price;}else{$p=$i->price-5000;} echo number_format($p); ?> </td>
                                    <td align="right"><?php if(substr($i->name,0,2)=='R-'){$a=0;}else{$a=5000;}echo number_format($a); ?> </td>
                                    <td align="right"><?php echo number_format($i->ppn); ?> </td>
                                    <td align="right"><?php echo $i->vendor; ?> </td>
                                    <td align="right"><?php echo $i->kasir; ?> </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>

            </div>


        </div>
      </div>
    </div>

    </div><!--/.row end-->






</div>	<!--/.main-->

<script>

    $(document).ready(function(){
        $(".nmpil").select2();

        document.getElementById("tklik").style.animation = "spin 1.5s alternate 1";
    })



    function kembali() {
        window.history.back();
    }

</script>