
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<div class="row">
    <ol class="breadcrumb">
        <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
        <li class="active">Pengaturan Email Pengirim</li>
    </ol>
</div><!--/.row--><br />

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">Data Email Pengirim</div>
            <div class="panel-body">
    <form action="" method="POST" enctype="multipart/form-data">
          <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Pengirim </label>
                <div class='col-sm-8'>
                    <input type='text' name='nama' class='form-control' id='nama'value="<?php echo $datnot->nama; ?>">
                </div>
          </div>        
         <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>SMTP </label>
                <div class='col-sm-8'>
                    <input type='text' name='smtp' class='form-control' id='smtp'value="<?php echo $datnot->smtp; ?>">
                </div>
          </div> 
           <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Email Pengirim </label>
                <div class='col-sm-8'>
                    <input type='email' name='email' class='form-control' id='email'value="<?php echo $datnot->email; ?>">
                </div>
          </div>                     
           <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Email Password </label>
                <div class='col-sm-8'>
                    <input type='password' name='password' class='form-control' id='password'value="<?php echo $datnot->password; ?>">
                </div>
          </div> 
           <div class='form-group row'>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Secure </label>
                <div class='col-sm-3'>
                    <input type='text' name='secure' class='form-control' id='secure'value="<?php echo $datnot->secure; ?>">
                </div>
                <label for='lblalamat' class='col-sm-2 form-control-label'>Port </label>
                <div class='col-sm-3'>
                    <input type='text' name='port' class='form-control' id='port'value="<?php echo $datnot->port; ?>">
                </div>
                
          </div> 
          <div class='form-group row'>
          	<div class='col-sm-2'></div>&nbsp;&nbsp;&nbsp;
          	<button type="submit" name="btnSubmit" class="btn btn-primary"><i class="fa fa-floppy-o fa-lg"></i><br><small><sup>SIMPAN</sup></small></button>&nbsp;&nbsp;&nbsp;<button type='button' onclick="window.location.href = '<?php echo base_url('/panbd/blasin') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
          </div>


        </form>
            </div>
        </div>
    </div>
</div><!--/.row-->

</div><!--/.main-->
