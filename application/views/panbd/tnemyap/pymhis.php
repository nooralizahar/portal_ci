<?php 
$dat=$this->db->order_by('myid','desc')->limit(1)->get('rad_pay')->row();
$ld=$dat->inputdate;

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panbd/"); ?>"> Dashboard</a></li>
            <li class="active"> Data Pembayaran</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-primary"> <a href="#" id="tklik">Data Pembayaran</a></h4>

                </div>
        <div class="panel-body">
    
    <form class='add-item' method=POST action=''>
        <div class='form-group row'>

                <label for='lbliddat' class='col-sm-2 form-control-label'>Nama Pelanggan </label>
                <div class='col-sm-5'>
                    <select name="nmpel" id="nmpel" class="form-control nmpil">
                                <option value=""></option>
                                <?php foreach($datpela as $p):?>
                                    <option value="<?php echo $p->username; ?>"><?php echo $p->username." - ".$p->name; ?></option>
                                <?php endforeach;?>
                    </select>
                </div>
                <div class='col-sm-4'>              
                    <button type="submit" name="btnCari" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;<small>CARI</small></button>
                    <?php if(empty($pil)){}else{ ?>
                    &nbsp;<a href="<?php echo base_url("panbd/toexcel/".$pil); ?>" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg"></i>&nbsp;<small>TO XLS</small></a><?php } ?>
                </div>

        </div> 
    </form>

    <form class='add-item' method=POST action='<?php echo base_url("panbd/phibln"); ?>'>
        <div class='form-group row'>

                <label for='lbliddat' class='col-sm-2 form-control-label'>Laporan Bulan</label>
                <div class='col-sm-2'>
                    <select name="crbln" id="crbln" class="form-control nmpil">
                                <option value="202103">MAR 2021</option>
   								<option value="202102">FEB 2021</option>
   								<option value="202101">JAN 2021</option>
                    </select>
                </div>
                <div class='col-sm-4'>              
                    <button type="submit" name="btnCari" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;<small>GENERATE</small></button>
                    <?php if(empty($pil)){}else{ ?>
                    &nbsp;<a href="<?php echo base_url("panbd/toexcel/".$pil); ?>" class="btn btn-primary"><i class="fa fa-file-excel-o fa-lg"></i>&nbsp;<small>TO XLS</small></a><?php } ?>
                </div>

        </div> 

    </form>

            <div class="form-group"> 
            	        <small style='color:white; background-color:green;'>&nbsp;Data terakhir : <?php echo $ld; ?>&nbsp;</small>
                    <!--<div class="loader"> 
                        <div class="loading_1"></div>        
                    </div> -->
                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="17"><center><h4>DATA PEMBAYARAN</h4></center></th></tr>
                            <tr>

                                <th>&nbsp;No.</th>
                                <th class="text-center">&nbsp;ID#</th>
                                <th>&nbsp;NAMA</th>
                                <th>&nbsp;TAHUN</th>
                                <th><center>JAN</center></th>
                                <th><center>FEB</center></th>
                                <th><center>MAR</center></th>
                                <th><center>APR</center></th>
                                <th><center>MEI</center></th>
                                <th><center>JUN</center></th>
                                <th><center>JUL</center></th>
                                <th><center>AGU</center></th>
                                <th><center>SEP</center></th>
                                <th><center>OKT</center></th>
                                <th><center>NOP</center></th>
                                <th><center>DES</center></th>
                                <th><center>TOTAL</center></th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php 
                            
                            $no=1;
                            foreach($datpaid as $h): ?>
                                <tr >
                                    <td align="right"><?php echo $no++; ?> </td>
                                    <td><?php echo $h->username; ?> </td>
                                    <td><?php echo substr($h->nama,0,6); ?> </td>
                                     <td><?php echo $h->thn; ?> </td>
                                    <td align="center"><a href="<?php //echo base_url("panel/wodetps/".$h->nama."/".date('Y')."-01"); ?>" data-toggle='tooltip' title='<?php echo " ".$h->nama.",&#013; Detil bulan : Januari ".date('Y'); ?>'><?php echo number_format($h->JAN); ?></a> </td>
                                    <td align="center"><?php echo number_format($h->FEB); ?> </td>
                                    <td align="center"><?php echo number_format($h->MAR); ?> </td>
                                    <td align="center"><?php echo number_format($h->APR); ?> </td>
                                    <td align="center"><?php echo number_format($h->MEI); ?> </td>
                                    <td align="center"><?php echo number_format($h->JUN); ?> </td>
                                    <td align="center"><?php echo number_format($h->JUL); ?> </td>
                                    <td align="center"><?php echo number_format($h->AGU); ?> </td>
                                    <td align="center"><?php echo number_format($h->SEP); ?> </td>
                                    <td align="center"><?php echo number_format($h->OKT); ?> </td>
                                    <td align="center"><?php echo number_format($h->NOP); ?> </td>
                                    <td align="center"><?php echo number_format($h->DES); ?> </td>
                                    <td align="center"><?php echo number_format($h->TOTAL); ?> </td>

                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>

            </div>

<br><br>

            <div class="form-group"> 
                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="17"><center><h4>RINCIAN TRANSAKSI</h4></center></th></tr>
                            <tr>

                                <th>&nbsp;No.</th>
                                <th><center>TAHUN</center></th>
                                <th><center>INVCODE</center></th>
                                <th class="text-center">&nbsp;ID#</th>
                                <th>&nbsp;NAMA</th>
                                <th><center>PAKET</center></th>
                                <th>&nbsp;TGL. BAYAR</th>
                                <th><center>PERIODE</center></th>
                                <th><center>HARGA</center></th>
                                <th><center>PPN</center></th>
                                <th><center>DISC.</center></th>
                                <th><center>METODA</center></th>
                                <th><center>KASIR</center></th>
                                

                            </tr>
                            </thead>

                            <tbody>
                            <?php 
                            
                            $no=1;
                            foreach($detpaid as $i): ?>
                                <tr >
                                    <td align="right"><?php echo $no++; ?> </td>
                                    <td><?php echo $i->thn; ?> </td>
                                    <td><?php echo $i->invcode; ?> </td>
                                    <td><?php echo $i->username; ?> </td>
                                    <td><?php echo $i->name; ?> </td>
                                    <td><?php echo $i->packet; ?> </td>
                                    <td><?php echo $i->pay_date; ?> </td>
                                    <td><?php echo $i->bln; ?> </td>
                                    <td><?php echo number_format($i->price); ?> </td>
                                    <td><?php echo number_format($i->ppn); ?> </td>
                                    <td><?php echo number_format($i->discount+$i->discount_tagihan); ?> </td>
                                    <td><?php echo $i->vendor; ?> </td>
                                    <td><?php echo $i->kasir; ?> </td>

                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>

            </div>

<br><br>

            <div class="form-group"> 
                        <table class="table table-bordered">
                            <thead>
                            <tr> <th colspan="17"><center><h4>DETIL SESSION</h4></center></th></tr>
                            <tr>

                                <th>&nbsp;No.</th>
                                <th><center>USERNAME</center></th>
                                <th><center>SESSION_ID</center></th>
                                <th><center>NAS IP ADDR.</center></th>
                                <th><center>NAS PORT</center></th>
                                <th><center>NAS PORT TYPE</center></th>
                                <th><center>START</center></th>
                                <th><center>UPDATE</center></th>
                                <th><center>STOP</center></th>

                            </tr>
                            </thead>

                            <tbody>
                            <?php 
                            
                            $no=1;
                            foreach($detsesi as $j): ?>
                                <tr >
                                    <td align="right"><?php echo $no++; ?> </td>
                                    <td align="center"><?php echo $j->username; ?> </td>
                                    <td align="center"><?php echo $j->acctsessionid; ?> </td>
                                    <td align="center"><?php echo $j->nasipaddress; ?> </td>
                                    <td align="center"><?php echo $j->nasportid; ?> </td>
                                    <td align="center"><?php echo $j->nasporttype; ?> </td>
                                    <td align="center"><?php echo tgl_indome(substr($j->acctstarttime,0,10)).' '.substr($j->acctstarttime,11); ?> </td>
                                    <td align="center"><?php echo tgl_indome(substr($j->acctupdatetime,0,10)).' '.substr($j->acctupdatetime,11);; ?> </td>
                                    <td align="center"><?php echo tgl_indome(substr($j->acctstoptime,0,10)).' '.substr($j->acctstoptime,11);; ?> </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>

            </div>




        </div>
      </div>
    </div>

    </div><!--/.row end-->






</div>	<!--/.main-->

<script>

    $(document).ready(function(){
        $(".nmpil").select2();

        document.getElementById("tklik").style.animation = "spin 1.5s alternate 1";
    })



    function kembali() {
        window.history.back();
    }

</script>