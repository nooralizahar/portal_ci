<?php 


?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panbd/"); ?>"> Dashboard</a></li>
            <li class="active"> Blasting Info</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="blasting">
            <div class="panel panel-default">
                <?php if(isset($_GET["err"])): ?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Gagal upload file.<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["succ"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Upload file berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["hsuc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data <?=$this->uri->segment(3); ?> berhasil dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["herr"])):?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data tidak bisa dihapus. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["bsuc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Blasting Whatsapp ID# <?=$this->uri->segment(3); ?> berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["berr"])):?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Blasting Whatsapp ID# <?=$this->uri->segment(3); ?> gagal. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["ebsu"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Blasting Email ID# <?=$this->uri->segment(3); ?> berhasil. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["eber"])):?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Blasting Email ID# <?=$this->uri->segment(3); ?> gagal. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["esuc"])):?>
                    <div class="alert bg-success" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Perubahan email pengirim berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php elseif(isset($_GET["eerr"])):?>
                    <div class="alert bg-danger" role="alert">
                        <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Perubahan email pengirim gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    </div>
                <?php endif; ?>
                <div class="panel-heading">
                    <h4 class="text-primary"> Data Blasting</h4>
                </div>

        <div class="panel-body">
    <?php echo  $this->session->flashdata('pesan');?>
    <form action="" method="POST" enctype="multipart/form-data">
        <div class='form-group row'>
                <label for='lblnama' class='col-sm-2 form-control-label'>File Excel</label>
                <div class='col-sm-10'>
                <input type="file" multiple id="nafile" name="nafile[]" class="form-control"> <small>format file : *.xls,*.xlsx. <br></small>
  

                </div>
        </div>  
        <div class='form-group row'>
                    <label for='tombol' class='col-sm-2 form-control-label'></label>
                    <div class='col-sm-9'>
                      <button type="submit" name="btnImport" class="btn btn-primary"><i class="fa fa-upload fa-lg"></i><br><small><sup>UNGGAH</sup></small></button>
                      <button type='button' onclick="window.location.href = '<?php echo base_url('/panbd') ?>';" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button>
                    </div>
                    <div class='col-sm-1'>  
                      <a href="<?php echo base_url("panbd/setemail/"); ?>" data-toggle='tooltip' title='Atur Email Pengirim'><i class="fa fa-cog fa-lg fa-2x"></i></a>

                    </div>
        </div>
                
    </form>

            <div class="form-group"> 

                       <!-- <table class="table table-bordered"> data-sort-name="name" data-sort-order="desc"-->
                       	<center><h3>BLASTING UPLOAD DATA</h3></center>
                               <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true">
                            <thead>
                            
                            <tr>

                            
                                <th class="text-center">&nbsp;ID#</th>
                                <th>&nbsp;File</th>
                                <th>&nbsp;Tgl.</th>
                                <th><center>Jumlah</center></th>
                                <th><center>Uploader</center></th>
                                <th><center>Opsi</center></th>

                            </tr>
                            </thead>

                            <tbody>
                            <?php 
                            
                            
                            foreach($daffil as $h): ?>
                                <tr >
                                   
                                    <td><?php echo $h->id; ?> </td>

                                    <td><?php if($h->nafi==''){ 

                                    }else{ 

                                if($h->nafi != null){
                                $fibla= json_decode($h->nafi);
                                ?>
                                    
                                    <?php foreach($fibla as $item):
                                    $cls = (is_doc($item->file)) ? "attach-doc" : "attach-img";
                                    ?>
                                     
                                    <a href="<?php echo base_url("assets/uploads/tsalb/".$item->file); ?>" class="<?php  echo $cls; ?>" data-judul="<?php echo $item->judul; ?>" target="_blank"><small><?php echo $item->judul;//$ij=explode(' ',$item->judul); echo $ij[0].'...'; ?></a></small><br>
                                    <?php endforeach;  ?> 
                                
                                <?php } }?> </td>

                                    <td><?php echo $h->tgup; ?> </td>
                                    <td class="text-right"><?php echo $h->data; ?> </td>
                                    <td><?php echo $h->nama; ?> </td>
                                    <td><center>
                                    <?php if (!empty($h->tgeb)):?>
                                        <a href="#" data-toggle='tooltip' title='Email berhasil di kirim' style='color:green'><i class="fa fa-check-circle fa-lg" ></i></a>
                                    <?php else:?>   
                                        <a href="<?php echo base_url("panbd/kirnotem/".$h->id); ?>" data-toggle='tooltip' title='Blasting Email'><i class="fa fa-envelope-o fa-lg"></i></a>
                                    <?php endif;?>
                                    <?php if (!empty($h->tgbl)):?>
                                        <a href="#" data-toggle='tooltip' title='Whatsapp berhasil di kirim' style='color:green'><i class="fa fa-check-circle fa-lg" ></i></a>
                                    <?php else:?>
                                        &nbsp;&nbsp;<a href="<?php echo base_url("panbd/kirnotwa/".$h->id); ?>" data-toggle='tooltip' title='Blasting Whatsapp'><i class="fa fa-whatsapp fa-lg"></i></a>
                                    <?php endif;?>
                                    <?php if (!empty($h->tgeb) || !empty($h->tgbl) ):?>
                                    <?php else:?>
                                        &nbsp;&nbsp;<a href="<?php echo base_url("panbd/dropblas/".$h->id); ?>" data-toggle='tooltip' title='Drop Blasting'><i class="fa fa-trash-o fa-lg"></i></a></center> </td>
                                    <?php endif;?>

                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                           </table>

            </div>

<br><br>



        </div> <!-- panel body end -->
      </div><!-- panel-default end-->
    </div> <!--/.col-12 end-->

    </div><!--/.row end-->

</div>	<!--/.main-->

<script>

    $(document).ready(function(){
        $(".nmpil").select2();
    })

    function kembali() {
        window.history.back();
    }

</script>