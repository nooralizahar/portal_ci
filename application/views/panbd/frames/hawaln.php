<?php
$idjab=$this->session->userdata("id_jabatan");
$iddiv=$this->session->userdata("id_div");
$idku=$this->session->userdata("id_pengguna");
$levku=$this->session->userdata("level");
$aksku=$this->session->userdata("mn_akses");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo (isset($judul)) ? $judul : ""; ?></title>

    <link href="<?php echo base_url("assets/css/bootstrap.min.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-table.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/datepicker3.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/bootstrap-timepicker.min.css");?> type="text/css" />
    <link href="<?php echo base_url("assets/css/styles.css");?>" rel="stylesheet">
    <link href="<?php echo base_url("assets/css/stybotn.css");?>" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url("favicon.png"); ?>" type="image/png" sizes="14x5">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/css/fileinput.min.css">

    <!-- Include Editor style. -->
    <!--<link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/css/froala_style.min.css' rel='stylesheet' type='text/css' />-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <!--Icons-->
    <script src="<?php echo base_url("assets/js/lumino.glyphs.js");?>"></script>

    <!--[if lt IE 9]>
    <script src="<?php echo base_url("assets/js/html5shiv.js");?>"></script>
    <script src="<?php echo base_url("assets/js/respond.min.js");?>"></script>
    <![endif]-->

    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.6/js/fileinput.min.js"></script>


    <link rel="stylesheet" href="<?php echo base_url("assets/css/magnific-popup.css"); ?>">
    <script src="<?php echo base_url("assets/js/bootstrap-datetimepicker.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.magnific-popup.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jQuery.print.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/bootbox.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/jquery.shorten.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
    <!-- Include JS file. -->
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.4.0/js/froala_editor.min.js'></script>

    <script>
        BASE_URL = "<?php echo base_url(); ?>";
	    $.FroalaEditor.DEFAULTS.key = "bvA-21sD-16A-13ojmweC8ahD6f1n==";
    </script>

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span>JLM</span>PORTAL</a>

            <ul class="user-menu">

                <li class="dropdown pull-right">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> <?php echo $this->session->userdata("nama_lengkap"); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <!-- <li><a href="<?php echo base_url("panbd/profilku/").$idku; ?>"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profil</a></li> 
                        <li><a href="#" id="pengaturan"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Ganti Sandi</a></li>--> 
                        <li><a href="<?php echo base_url("panel/logout/"); ?>"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
<br>

    <ul class="nav menu">
<?php if($levku=="user" && $aksku=="admin" ):?>
        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>ADMIN</b>
        <li class="<?php echo $menu["dashboard"] ?>"><a href="<?php echo base_url("panbd/"); ?>"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Dashboard</a></li>
        <li class="<?php echo $menu["rokelol"] ?>"><a href="<?php echo base_url("panbd/rokelol/"); ?>"><svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>Kelola Role</a></li>

        <li class="<?php echo $menu["uskelol"] ?>"><a href="<?php echo base_url("panbd/uskelol/"); ?>"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Kelola User</a></li>
        <li class="<?php echo $menu["tikelol"] ?>"><a href="<?php echo base_url("panbd/tikelol/"); ?>"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Kelola Tiket</a></li>
        <li class="<?php echo $menu["timanua"] ?>"><a href="<?php echo base_url("panbd/tikelol/"); ?>"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Tiket Manual</a></li>
<?php endif; ?>

       <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>PENGGUNA</b>
        <li class="<?php echo $menu["profil"] ?>"><a href="<?php echo base_url("panbd/profilvi/").$idku; ?>"><svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>Profil</a></li>

        <li class="<?php echo $menu["uprofil"] ?>"><a href="<?php echo base_url("panbd/profilku/").$idku; ?>"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Ubah Profil</a></li>
        <!-- <li class="<?php echo $menu["gasandi"] ?>"><a href="<?php echo base_url("panbd/profil/"); ?>"><svg class="glyph stroked lock"><use xlink:href="#stroked-lock"></use></svg> Ganti 
        Sandi</a></li> -->
        <li role="presentation" class="divider"></li>
        <?php if($idjab=="68"):?>
        &nbsp;&nbsp;<b>CUSTOMER</b>
        <li class="<?php echo $menu["pymhis"] ?>"><a href="<?php echo base_url("panbd/pymhis/"); ?>"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>Payment History</a></li>       
        <li class="<?php echo $menu["blainf"] ?>"><a href="<?php echo base_url("panbd/blasin/"); ?>"><svg class="glyph stroked empty message"><use xlink:href="#stroked-empty-message"></use></svg>Blasting Info</a></li>
        <li class="<?php echo $menu["blapro"] ?>"><a href="<?php echo base_url("panbd/blaspro/"); ?>"><svg class="glyph stroked two messages"><use xlink:href="#stroked-two-messages"></use></svg>Blasting Promo</a></li>
        <?php elseif($idjab=="70" || $idjab=="71" || $idjab=="72"):?>
        &nbsp;&nbsp;<b>SALES</b>
        <li class="<?php echo $menu["salopp"] ?>"><a href="<?php echo base_url("panbd/salopp/"); ?>"><svg class="glyph stroked female user"><use xlink:href="#stroked-female-user"></use></svg>Opportunity</a></li>
        <li class="<?php echo $menu["salinc"] ?>"><a href="<?php echo base_url("panbd/salinc/"); ?>"><svg class="glyph stroked heart"><use xlink:href="#stroked-heart"></use></svg><?php if($idjab==70){echo "My Incentive";}else{echo "Incentive";}?></a></li>    
        <?php else:?>
         &nbsp;&nbsp;<b>BUSDEV</b>
        <li class="<?php echo $menu["locdev"] ?>"><a href="<?php echo base_url("panbd/locdev/"); ?>"><svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"></use></svg>Lokasi</a></li>

        <?php endif;?>

        <?php if($idjab=="65"):?>
        <li class="<?php echo $menu["spkbru"] ?>"><a href="<?php echo base_url("panbd/spkbru/"); ?>"><svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>SPK</a></li>
        <li class="<?php echo $menu["komrek"] ?>"><a href="<?php echo base_url("panbd/komrek/"); ?>"><svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>Penerima</a></li>
        <?php endif;?>

        <?php if($idjab=="63" || $idjab=="64" ):?>
        <li class="<?php echo $menu["mombaru"] ?>"><a href="<?php echo base_url("panbd/mombaru/"); ?>"><svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>IoM</a></li>   
        <?php endif;?>
        
        <?php if($idjab=="60" || $idjab=="66" ):?>
       <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>LAPORAN</b>
        <li class="<?php echo $menu["laphri"] ?>"><a href="<?php echo base_url("panbd/laphri/"); ?>"><svg class="glyph stroked calendar blank"><use xlink:href="#stroked-calendar-blank"></use></svg>Harian</a></li>
        <li class="<?php echo $menu["lapbln"] ?>"><a href="<?php echo base_url("panbd/lapbln/"); ?>"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg>Bulanan</a></li>
        <?php endif;?>


<?php if($levku=="user" && $aksku=="super" ):?>
        <li role="presentation" class="divider"></li>
        &nbsp;&nbsp;<b>SUPERUSER</b>
        <li class="<?php echo $menu["uskelol"] ?>"><a href="<?php echo base_url("panbd/uskelol/"); ?>"><svg class="glyph stroked chain"><use xlink:href="#stroked-chain"></use></svg> Kelola User</a></li>

<?php endif; ?>

    </ul>

</div><!--/.sidebar-->



