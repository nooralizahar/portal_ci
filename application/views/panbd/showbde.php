﻿
<?php
    $idjab=$this->session->userdata("id_jabatan");
    $dver=$this->db->where("ver",1)->get("t_odp")->num_rows();
    $dunv=$this->db->where("ver",0)->get("t_odp")->num_rows();
?>


<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $dver." (".number_format(($dver/($dver+$dunv))*100)." %)" ?></div>
                        <div class="text-muted"> ODP Verified</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="panel panel-merah panel-widget">
                <div class="row no-padding">
                    <!--<a href="<?php echo base_url("focmen/odpcs/"); ?>" >-->
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clock"><use xlink:href="#stroked-clock"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?= $dunv." (".number_format(($dunv/($dver+$dunv))*100)." %)" ?></div>
                        <div class="text-muted"> ODP Unverified</div>
                    </div><!--</a>-->
                </div>
            </div>
        </div>

    </div>
</div> 



<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->get("t_odp")->num_rows()); ?></div>
                        <div class="text-muted"> ODP</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal panel-widget">
                <div class="row no-padding">
                    <!--<a href="<?php echo base_url("focmen/odpcs/"); ?>" >-->
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->where("status",1)->get("t_odp")->num_rows()); ?></div>
                        <div class="text-muted"> Active</div>
                    </div><!--</a>-->
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php echo number_format($this->db->where("status",0)->get("t_odp")->num_rows()); ?> </a></div>
                        <div class="text-muted"> InActive</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 





<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-jingga panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $this->db->select_sum('kapasitas'); $hasil= $this->db->get("t_odp")->row(); $y=$hasil->kapasitas; echo number_format($y); ?></div>
                        <div class="text-muted"> Kapasitas</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-jingga-l2 panel-widget">
                <div class="row no-padding">
                    <!--<a href="<?php echo base_url("focmen/odpcs/"); ?>" >-->
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked male user"><use xlink:href="#stroked-male-user"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($this->db->get("t_odp_rel")->num_rows()); ?></div>
                        <div class="text-muted"> Pelanggan</div>
                    </div><!--</a>-->
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-teal-l2 panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked blank document"><use xlink:href="#stroked-blank-document"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><a href="<?php //echo base_url("panel/word/20$thnini"); ?>"><?php $u=$this->db->get("t_odp_rel")->num_rows(); $t=$y-$u; echo number_format($t);?> </a></div>
                        <div class="text-muted"> Tersedia</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 


<div class="row">
    <div class="col-lg-12">
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-blue panel-widget">
                <div class="row no-padding">

                    <a href="#" data-toggle="modal" data-target="#locvie0">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($tlok-1); ?></div>
                        <div class="text-muted"> Lokasi</div>
                    </div></a>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido-l2 panel-widget">
                <div class="row no-padding">

                   <a href="#" data-toggle="modal" data-target="#locvie1">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php echo number_format($plok); ?></div>
                        <div class="text-muted"> Perumahan</div>
                    </div></a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget ">
                <div class="row no-padding">

                    <a href="#" data-toggle="modal" data-target="#locvie2">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked landscape"><use xlink:href="#stroked-landscape"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><?php  $l=$tlok-$plok; echo number_format($l-1);?> </div>
                        <div class="text-muted">Lingkungan</div>
                    </div></a>
                </div>
            </div>
        </div>
    </div>
</div> 



<!-- AWAL SEMUA MODAL DISINI   --> 
<center></center>

<div class="modal fade" id="locvie0" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel">Location View
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 

                    $a=1;
                    foreach ($datlok as $lp) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; ".$lp->area_name." - ".$lp->name."</td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>



<div class="modal fade" id="locvie1" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel">Location View
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $a=1;
                    foreach ($lokper as $lp) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; ".$lp->area_name." - ".$lp->name."</td></tr>";
                        $a++;
                    }
                    ?>
                    </table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="locvie2" tabindex="-1" role="dialog" aria-labelledby="locvieLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="locvieLabel">Location View
                <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button></h4>
            </div>
            
                <div class="modal-body">
                    <table>
                    <?php 
                    $a=1;
                    foreach ($loklin as $lp) {
                        echo "<tr><td class='text-right'>".$a.". </td><td>&nbsp; ".$lp->area_name." - ".$lp->name."</td></tr>";
                        $a++;
                    }
                    ?></table>
                </div>

                <div class="modal-footer">
                    <center>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
                    <!--<button type="submit" class="btn btn-primary">Simpan</button> -->
                    </center>
                </div>
            
        </div>
    </div>
</div>


<!-- AKHIR SEMUA MODAL DISINI   --> 