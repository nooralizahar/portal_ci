<?php 
$level= $this->db->where("id_jab",$this->session->userdata("id_jabatan"))->get("ts_natabaj")->result();
foreach($level as $lvl): 
    $nmjab=$lvl->nm_jab; 
    $idjab=$lvl->id_jab;

endforeach;
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url("panbd/"); ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("panbd/salopp/"); ?>">Sales</a></li>

        </ol>
    </div><!--/.row-->

    <hr/>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"> Sales Incentive <?php //echo $idibo; ?></div>
                <div class="panel-body">
				    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <?php if(isset($_GET["err"])): ?>
                                <div class="alert bg-danger" role="alert">
                                    <svg class="glyph stroked cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-cancel"></use></svg> Data gagal disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php elseif(isset($_GET["succ"])):?>
                                <div class="alert bg-success" role="alert">
                                    <svg class="glyph stroked checkmark"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#stroked-checkmark"></use></svg> Data Berhasil disimpan. <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

<!-- isi awal -->

 <!--       <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-ungu panel-widget">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $tot=$this->db->get("v_sales_opp")->num_rows(); echo $tot; ?></div>
                        <div class="text-muted"> Alamat ODP</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-bido panel-widget">
                <div class="row no-padding">
                    <a href="<?php echo base_url("focmen/odpcs/"); ?>" >
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">
                        <div class="large"><?php $per=$this->db->where('alamat like','%perum%')->get("v_sales_opp")->num_rows(); echo $per;?></div>
                        <div class="text-muted"> Perumahan</div>
                    </div></a>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel panel-meda panel-widget ">
                <div class="row no-padding">
                    <div class="col-sm-3 col-lg-5 widget-left">
                        <svg class="glyph stroked landscape"><use xlink:href="#stroked-landscape"></use></svg>
                    </div>
                    <div class="col-sm-9 col-lg-7 widget-right">

                        <div class="large"><a href="#"><?php   echo $tot-$per;?> </a></div>
                        <div class="text-muted">Lingkungan</div>
                    </div>
                </div>
            </div>
        </div> -->
<center><h4 style="color: green">Data sales dari <b><a href="https://app.bnetfit.com" target="_blank">Aplikasi iBoss</a></b></h4></center>
       <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-page-size="15" data-sort-name="c" data-sort-order="desc">
                    <thead>

                    <tr>
                        
                        <th data-field="a"  data-sortable="true">ID#</th>
                        <th data-field="d"  data-sortable="true"><center>Bulan</center></th>
                        <th data-field="b"  data-sortable="true">Nama</th>
                        <th data-field="c"  data-sortable="true"><center>Jumlah Pelanggan</center></th>
                       
                    </thead>
                    <tbody>

                        <?php if ($idjab=='70'){$di=$salesi;}else{$di=$datinc;}
                        foreach($di as $do): ?>

                          <tr>
                            
                            <td ><?php echo $do->sales_id; ?></td>
                            <td class="text-center"><?php echo $do->bln; ?></td>
                            
                            <td><?php echo $do->sales_name; ?></td>
                            <td class="text-center"><?php echo $do->jml; ?></td>
                            
                          </tr>
 

                        <?php  endforeach; ?>
                    </tbody>
            </table> 



<!-- isi akhir -->

                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->

<script type="text/javascript">
    var ckeditor = CKEDITOR.replace('immtc',{});
    CKEDITOR.disableAutoInline=true;
    CKEDITOR.inline('editable');
</script>

<script>


    $(document).ready(function(){


        $(".req").select2();


        $(".tanggal").datepicker({
            format: "yyyy-mm-dd"
        });
        $("#attach").fileinput({'showUpload':false, 'previewFileType':'any'});
    })
</script>