<?php 
        $dkap=$this->db->where("idodp",$id)->get("t_odp")->result();
        $dpel=$this->db->where("idodp",$id)->get("t_odp_rel")->num_rows();
        foreach($dkap as $d): 
        $kap=$d->kapasitas;  
        $nodp=$d->nama;
        $kodp=$d->kordinat; 
        endforeach;
        $sta=$kap-$dpel;

function codexworldGetDistanceOpt($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
{
    $rad = M_PI / 180;
    //Calculate distance from latitude and longitude
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin($latitudeFrom * $rad) 
        * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
        * cos($latitudeTo * $rad) * cos($theta * $rad);

    return acos($dist) / $rad * 60 *  1.853;
}

?>


<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo base_url("focmen/odpin/"); ?>"> Menu ODP</a></li>
            <li class="active"> Daftar ODP</li>
        </ol>
    </div><!--/.row-->

    <hr/>


    <div class="row">
        <div class="col-lg-12" id="odcbaru">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="text-primary"> Daftar Pelanggan ODP No. <?php echo $id;?> </h4>
                </div>
        <div class="panel-body">

        <h3 class="text-success text-center"><a href="<?php echo base_url("focmen/lopel/".$id); ?>" target='_blank'><i class='fa fa-map-marker fa-lg'></i>&nbsp;<?php echo $nodp; ?></a><br></h3>
        <h3 class="text-success text-center"><?php echo "Tersisa : ".$sta." port"; ?><br></h3>
         <p><br><button type='button' onclick="kembali()" class='btn btn-primary'><i class='fa fa-arrow-left fa-lg'></i><br> <sup><small>KEMBALI</small></sup></button><br></p>

            <table class="table table-striped table-responsive" data-toggle="table" data-url="<?php ?>"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                    <thead>
                    <tr>
                        <th data-field="i"  data-sortable="true">No. Port</th>
                        <th data-field="a"  data-sortable="true">Pelanggan</th>
                        <th data-field="b"  data-sortable="true">Alamat</th>
                        <th data-field="c"  data-sortable="true">Koordinat</th>
                        <!--<th data-field="d"  data-sortable="true">Tarikan</th>-->
                        <th data-field="e"  data-sortable="true">Hit. Jarak (m)</th>
                        <th data-field="f"  data-sortable="true">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $fkor=explode(",",$kodp);

                        foreach($dafpel as $d):
                            $tkor=explode(",",$d->kordinat);
                        ?>  
                        <tr>
                            <td><?php echo $d->noport; ?></td>
                            <td><?php echo $d->pelanggan; ?></td>
                            <td><?php echo $d->alamat; ?></td>

                            <td><a href="<?php echo base_url("focmen/petap/".$d->idodpr); ?>" target='_blank' data-toggle='tooltip' title='<?php echo "IKR : ".$d->ikr.",&#013; Tarikan : ".$d->tarikan; ?>'><?php echo $d->kordinat; ?></a></td>
                            <!-- <td><?php echo $d->tarikan; ?></td>-->
                            <td><?php if($d->kordinat==null or $d->kordinat==0 ){ echo "tikor kosong";}else{$j=codexworldGetDistanceOpt($fkor[0],$fkor[1],$tkor[0],$tkor[1]); echo number_format($j*1000,2);} ?></td>
                           <td><center>
                               <!-- <a href="<?php //echo base_url("focmen/peled/".$d->idodpr); ?>" data-toggle='tooltip' title='Edit' ><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp; 
                                <a href="<?php //echo base_url("focmen/pelha/".$d->idodpr); ?>" data-toggle='tooltip' title='Hapus' ><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp; 
                                <a href="<?php //echo base_url("focmen/odpli/".$dat->idodp); ?>" ><i class="fa fa-plus fa-lg"></i></a>  -->
                            </center></td>

                        </tr>
                        <?php endforeach; ?>
                                  
 
                    </tbody>
            </table>



        </div>
      </div>
    </div>

    </div><!--/.row end-->






</div>	<!--/.main-->

<script>
        function kembali() {
        window.history.back();
    }
</script>