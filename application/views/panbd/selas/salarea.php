<?php
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">

  <title>ODP <?php echo $param; ?></title>
  <link rel="shortcut icon" href="images/favicon.png">
    <style>
      #mapCanvas {
        width: 100%;
        height: 640px;
      }
    </style>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAArK11nKE_IVBCHbXbM52illBzDCAqeQs&callback=initMap"></script>
<script>
function initMap() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the web page
    map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);
    map.setTilt(50);
        
    // Multiple markers location, latitude, and longitude
    var markers = [
        <?php //if($datpel->num_rows > 0){
            foreach ($datodp as $k) {
                $kor=explode(',',$k->kordinat);        
                echo '["'.$k->nama.'", '.$kor[0].', '.$kor[1].'],';
            //}
        }
        ?>
    ];

    var staODP = [
        <?php //if($datpel->num_rows > 0){
            foreach ($datodp as $s) {    
                echo $s->status.',';
            //}
        }
        ?>
    ];
                        
    // Info window content
    var infoWindowContent = [
        <?php if($datodp != 0){
            foreach($datodp as $l){ ?>
                ['<div class="info_content">' +
                '<h3><?php echo $l->nama." <sup> Sisa : ".$l->sisa." port</sup>"; ?></h3>' +
                '<p><?php echo $l->alamat; ?></p>' + '</div>'],
        <?php 

            }
        }
        ?>
    ];
        
    // Add multiple markers to map
    
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Place each marker on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        if(staODP[i] == 0){
            pinma='../../assets/images/bnetfit_fu.png';
        }else{

            pinma='../../assets/images/bnetfit_ms.png';

        }


        marker = new google.maps.Marker({
            position: position,
            map: map,
            //label:''+sisaPort[i],
            icon: pinma,
            title: markers[i][0]
        });
        
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
    }

    // Set zoom level
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });
    
}

// Load initialize function
google.maps.event.addDomListener(window, 'load', initMap);
</script>

</head>
  <body>
            <?php            /*
            foreach ($datodp as $k) {
                $kor=explode(',',$k->kordinat);        
                echo '["'.$k->nama.'", '.$kor[0].', '.$kor[1].'],';
            
        }*/
        ?>
    <div id="mapCanvas"></div>
  </body>
</html>