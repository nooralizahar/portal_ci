﻿ <?php //echo $id; ?>

<!DOCTYPE html>
<html>
  <head>
    <title>GAMBAR JALUR DI PETA</title>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <style type="text/css">
        
#map {
  height: 100%;
}

/* Optional: Makes the sample page fill the window. */
html,
body {
  height: 100%;
  margin: 0;
  padding: 0;
}

    </style>
<script>
function initMap() {

  <?php 
  $k=explode(',',$potjc->tiko);        
  $pusat='{ lat: '.$k[0].', lng: '.$k[1].'}'; ?>
  

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 16,
    center: <?=$pusat?>, //{ lat: -6.500931, lng: 106.794434},
    mapTypeId: "terrain",
  });

  const tikorJcl = [

        <?php foreach ($dotjc as $x) {
                $kor=explode(',',$x->tiko);        
                echo '{ lat: '.$kor[0].', lng: '.$kor[1].'},';   
        }?>

  ];

  /*const sBox = {
    path: "M -2,0 0,-2 2,0 0,2 z",
    strokeColor: "#F00",
    fillColor: "#F00",
    fillOpacity: 1,
  };*/


  const jalurOtb = new google.maps.Polyline({
    path: tikorJcl,
    geodesic: true,
    strokeColor: "#00FF00",
    strokeOpacity: 1.0,
    strokeWeight: 5,
   // icons:[{icon: sBox,offset: "0%",},{icon: sBox,offset: "30%",},{icon: sBox,offset: "50%",},],
   // map: map,
  });




// 0. { lat: -6.500931, lng: 106.794434},1. { lat: -6.499696, lng: 106.793334},2. { lat: -6.499470, lng: 106.791542},3. { lat: -6.499087, lng: 106.789806},4. { lat: -6.498457, lng: 106.789070},5. { lat: -6.498949, lng: 106.788073},6. { lat: -6.499338, lng: 106.787041}, 

  jalurOtb.setMap(map);

  setMarkers(map);

}

  const posjc = [

  <?php foreach ($dotjc as $m) {
                $kor=explode(',',$m->tiko);        
                echo '["'.$m->nama.'", '.$kor[0].','.$kor[1].', '.$m->idjc.'],';   
  }?>

  ];


function setMarkers(map) {

  const image = {
    url: "../../assets/images/bnetfit_fu.png",
    // This marker is 20 pixels wide by 32 pixels high.
    size: new google.maps.Size(35, 49),
    // The origin for this image is (0, 0).
    origin: new google.maps.Point(0, 0),
    // The anchor for this image is the base of the flagpole at (0, 32).
    anchor: new google.maps.Point(5, 22),
  };
  // Shapes define the clickable region of the icon. The type defines an HTML
  // <area> element 'poly' which traces out a polygon as a series of X,Y points.
  // The final coordinate closes the poly by connecting to the first coordinate.
  const shape = {
    coords: [1, 1, 1, 35, 22, 35, 22, 1],
    type: "poly",
  };

  for (let i = 0; i < posjc.length; i++) {
    const datjc = posjc[i];
    new google.maps.Marker({
      position: { lat: datjc[1], lng: datjc[2] },
      map,
      icon: image,
      shape: shape,
      title: datjc[0],
      label: datjc[0],
      zIndex: datjc[3],
    });
  }
}

    
</script>
  </head>
  <body>
    <?php
        /*foreach ($dotjc as $x) {
                $kor=explode(',',$x->tiko);        
                echo $x->idjc.'. { lat: '.$kor[0].', lng: '.$kor[1].'},';   
        }*/

        foreach ($dotjc as $z) {       
                echo $z->nama.',';   
        }


     ?>
    <div id="map"></div>

    <!-- Async script executes immediately and must be after any DOM elements used in callback. -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAArK11nKE_IVBCHbXbM52illBzDCAqeQs&callback=initMap"></script>  


  </body>
</html>


