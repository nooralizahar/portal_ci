<?php


defined("BASEPATH") OR exit("Akses ditolak!");

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Panbd extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->dbz = $this->load->database('newdb', TRUE); // Koneksi ke Postgres iBoss
        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("general");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }
        $this->load->model("excel_import_model");
        //$this->load->library('excel');
        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Busdev_model","busdev");
        $this->load->model("Bayar_model","bayar");
        $this->load->model("Blaspro_model","blaspro");
        $this->load->model("Odpdata_model","odpdata");
        $this->load->model("Lokasi_model","lokasi");
    }

    public function index() {
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));

        $judul = "Dashboard - Business Development";
        $menu = $this->set_menu("dashboard");


        $datlok = $this->lokasi->semlok();
        $lokper = $this->lokasi->pilare('Perumahan');
        $loklin = $this->lokasi->pilare('Lingkungan');
        $tlok  = $this->lokasi->totlok();
        $plok  = $this->lokasi->perlok();
        
        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/index",compact("datlok","lokper","loklin","tlok","plok"));
        $this->load->view("panbd/frames/footer");
    }


    public function uskelol() {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != super )
            redirect(base_url("panbd/"));

        $daftar_pengguna = $this->pengguna->nocsem();

        $judul = "Kelola Business Dev.";
        $menu = $this->set_menu("uskelol");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/tiunem/uskelol",compact("daftar_pengguna"));
        $this->load->view("panbd/frames/footer");
    }

    public function profilku($id = null) {
        //if($this->session->userdata("id_jabatan") != 1)
            //redirect(base_url("panel/"));
$post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->editpro();
            if($edt != false) {
                redirect(base_url("panbd/profilku/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url("panbd/profilku/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        //if($id == null) redirect(base_url("panbd/uskelol/"));
        $judul = "Edit Profil";
        $menu = $this->set_menu("uprofil");

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
            $this->load->view("panbd/person/profilku",compact("pengguna"));
            $this->load->view("panbd/frames/footer");
        } else redirect(base_url("panbd/pengguna/"));
    }

    public function profilvi($id = null) {
        if($this->session->userdata("id_pengguna") != $id)
            redirect(base_url("panbd/"));

        $judul = "Profil Pengguna";
        $menu = $this->set_menu("profil");

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
            $this->load->view("panbd/person/profilkuv",compact("pengguna"));
            $this->load->view("panbd/frames/footer");
        } else redirect(base_url("panbd/pengguna/"));
    }

    public function ustamba() {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != super )
            redirect(base_url("panbd/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $tbh = $this->pengguna->tambah();
            if($tbh != false) {
                redirect(base_url("panbd/ustamba/?succ"));
            } else {
                redirect(base_url("panbd/ustamba/?err"));
            }
            exit();
        }

        $judul = "Tambah NOC";
        $menu = $this->set_menu("uskelol");
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/tiunem/ustamba",compact("daftas","daftar_jabatan","daftar_divisi"));
        $this->load->view("panbd/frames/footer");
    }


    public function penggunaedit($id = null) {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panbd/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->edit();
            if($edt != false) {
                redirect(base_url("panbd/penggunaedit/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url("panbd/penggunaedit/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url("panbd/uskelol/"));
        $judul = "Edit Pengguna";
        $menu = $this->set_menu("uskelol");
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
            $this->load->view("panbd/penggunaedit",compact("pengguna","daftar_jabatan","daftar_divisi","daftas"));
            $this->load->view("panbd/frames/footer");
        } else redirect(base_url("panbd/pengguna/"));
    }

    public function penggunablokir($id = null) {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panbd/"));
        if($id == null) redirect(base_url("panbd/pengguna"));
        $this->pengguna->blokir($id);
        redirect(base_url("panbd/pengguna/?succ=1"));
    }

    public function penggunabuka($id = null) {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panbd/"));
        if($id == null) redirect(base_url("panel/pengguna"));
        $this->pengguna->buka($id);
        redirect(base_url("panbd/pengguna/?succ=2"));
    }



    public function mombaru() {
        if($this->session->userdata("id_div") != 10)
          redirect(base_url("panel/"));

        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $bar = $this->busdev->isimom();
            if($bar != false) {
                redirect(base_url("panbd/mombaru/?succ"));
            } else {
                redirect(base_url("panbd/mombaru/?err"));
            }
            exit();
        }

        $judul = "IoM baru";
        $menu = $this->set_menu("mombaru");


        $datiom = $this->lokasi->semiom();
        $datlok = $this->lokasi->semlok();

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/dbunem/mombaru",compact("datlok","datiom"));
        $this->load->view("panbd/frames/footer");
    }

    public function ubalok($idm=null) {
       if($this->session->userdata("id_div") != 10)
          redirect(base_url("panel/"));   
        
        $lokpil = $this->lokasi->pillok($idm);

        $post = $this->input->post();


        if(isset($post["btnSimpan"])) {
            $sim = $this->lokasi->simlok($idm);
            if($sim != false) {
                redirect(base_url("panbd/locdev/?succ"));
            } else {
                redirect(base_url("panbd/ubalok/".$idm."?err"));
            }
            exit();
        }

        $sta=$this->lokasi->pilsta($idm);

        $judul = "Ubah Status Lokasi";
        $menu = $this->set_menu("locdev");
        

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/vedcol/lokuba",compact("lokpil","idm","sta"));
        $this->load->view("panbd/frames/footer");
    }

    public function tamkom($id=null){
        if($this->session->userdata("id_div") != 10)
          redirect(base_url("panel/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
            $kom = $this->lokasi->kombar();
            if($kom != false) {
                redirect(base_url("panbd/tamkom/".$id."?succ"));
            } else {
                redirect(base_url("panbd/tamkom/".$id."?err"));
            }
            exit();
        }

        $post = $this->input->post();
        
        $datcek  = $this->lokasi->cekiom($id);
        
        $idno=$id;



        if(isset($post["selesai2BtnSubmit"])) {
            if(md5($post["password"]) == $this->session->userdata("password") && $datcek>0) { //
                $this->lokasi->verkom($idno);
                redirect(base_url("panbd/tamkom/".$idno."?succ"));
            } else {
                redirect(base_url("panbd/tamkom/".$idno."?err"));
            }
        }

        if(isset($post["selesai3BtnSubmit"])) {
            if(md5($post["password"]) == $this->session->userdata("password") && $datcek>0) { //
                $this->lokasi->apvkom($idno);
                redirect(base_url("panbd/tamkom/".$idno."?succ"));
            } else {
                redirect(base_url("panbd/tamkom/".$idno."?err"));
            }
        }
        $hitsta = $this->lokasi->hitlok($id);
        $datver = $this->lokasi->pilsta($id);
		$datkom = $this->lokasi->semkom($id);
        $datlok = $this->lokasi->pillok($id);
        $datrek = $this->lokasi->pilrek($id);
        $datodp = $this->lokasi->pilodp($id);
        $datiom = $this->lokasi->piliom($id);

        $judul = "Tambah Kompensasi";
        $menu = $this->set_menu("locdev");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/vedcol/komisi",compact("id","datkom","datlok","datrek","datodp","datiom","datver","hitsta"));
        $this->load->view("panbd/frames/footer");
    }
public function kelbrg($id=null){
        if($this->session->userdata("id_div") != 10)
          redirect(base_url("panel/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
            $brg = $this->lokasi->brgkel();
            if($brg != false) {
                redirect(base_url("panbd/kelbrg/".$id."?succ"));
            } else {
                redirect(base_url("panbd/kelbrg/".$id."?err"));
            }
            exit();
        }


        
        $idno=$id;




        $hitsta = $this->lokasi->hitlok($id);
        $datver = $this->lokasi->pilsta($id);
        $datkom = $this->lokasi->semkom($id);
        $datlok = $this->lokasi->pillok($id);
        $datbrg = $this->lokasi->pilbrg($id);



        $judul = "Barang Keluar";
        $menu = $this->set_menu("locdev");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/tcejorp/brgpro",compact("id","datkom","datbrg","datlok","datver","hitsta"));
        $this->load->view("panbd/frames/footer");
    }
    public function komrek($id=null){
        if($this->session->userdata("id_div") != 10)
          redirect(base_url("panel/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
            $rek = $this->lokasi->rekbar();
            if($rek != false) {
                redirect(base_url("panbd/komrek/".$id."?succ"));
            } else {
                redirect(base_url("panbd/komrek/".$id."?err"));
            }
            exit();
        }

        $datrek  = $this->lokasi->semrek();
        $datlok  = $this->lokasi->semlok();
        $dabank  = $this->lokasi->datbank();
        $tlok  = $this->lokasi->totlok();
        $plok  = $this->lokasi->perlok();


        $judul = "Tambah Penerima";
        $menu = $this->set_menu("komrek");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/vedcol/komrek",compact("id","datlok","dabank","datrek","tlok","plok"));
        $this->load->view("panbd/frames/footer");
    }
    public function spkbru($id=null){
        if($this->session->userdata("id_div") != 10)
          redirect(base_url("panel/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
            $rek = $this->lokasi->rekbar();
            if($rek != false) {
                redirect(base_url("panbd/komrek/".$id."?succ"));
            } else {
                redirect(base_url("panbd/komrek/".$id."?err"));
            }
            exit();
        }

        $datrek  = $this->lokasi->semrek();
        $datlok  = $this->lokasi->semlok();
        $dabank  = $this->lokasi->datbank();
        $tlok  = $this->lokasi->totlok();
        $plok  = $this->lokasi->perlok();


        $judul = "SPK Baru";
        $menu = $this->set_menu("spkbru");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/vedcol/spkbar",compact("id","datlok","dabank","datrek","tlok","plok"));
        $this->load->view("panbd/frames/footer");
    }
    public function laphri() {
        if($this->session->userdata("id_div") != 10)
          redirect(base_url("panel/"));

         $post = $this->input->post();
         $tgl=$this->input->post("tgl");
        
        if(!empty($tgl)){
            $datlap = $this->lokasi->semlap($tgl);
            
        }else{
            $tgl=date('Y-m-d');
            $datlap = $this->lokasi->semlap($tgl);
        }

        //$datlap = $this->lokasi->semlap();


        $judul = "Laporan Harian Pencairan - FTTH";
        $menu = $this->set_menu("laphri");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/report/laphri",compact("datlap"));
        $this->load->view("panbd/frames/footer");
    }

    public function lapbln() {
        if($this->session->userdata("id_div") != 10)
          redirect(base_url("panel/"));
      
         $post = $this->input->post();
         $tgl=$this->input->post("tgl");
        
        if(!empty($tgl)){
            $tgl=substr($this->input->post("tgl"), 0,4)."-".substr($this->input->post("tgl"), 5,2);
            $datlap = $this->lokasi->semlap($tgl);
            
        }else{
            $tgl=date('Y-m');
            $datlap = $this->lokasi->semlap($tgl);
        }

        //$datlap = $this->lokasi->semlap();


        $judul = "Laporan Bulanan Pencairan - FTTH";
        $menu = $this->set_menu("lapbln");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/report/lapbln",compact("datlap"));
        $this->load->view("panbd/frames/footer");
    }

    public function locdev() {
        if($this->session->userdata("id_div") != 10)
          redirect(base_url("panel/"));

        $datsta = $this->lokasi->semsta();
        $datlok = $this->lokasi->semlok();
        $lokper = $this->lokasi->pilare('Perumahan');
        $loklin = $this->lokasi->pilare('Lingkungan');
        $dlokas = $this->lokasi->lokkas();
        $tlok   = $this->lokasi->totlok();
        $plok   = $this->lokasi->perlok();

        $judul = "Data Lokasi Kompensasi - FTTH";
        $menu = $this->set_menu("locdev");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/vedcol/lokdaf",compact("loklin","lokper","datlok","tlok","plok","datsta","dlokas"));
        $this->load->view("panbd/frames/footer");
    }

    public function salopp($i=null) {       
        if($this->session->userdata("id_div") != 12)
          redirect(base_url("panel/"));

        if($i!=null)
          redirect(base_url("panbd/salodp/").$i);

        $datopp = $this->lokasi->datopp();

        $judul = "Sales Opportunity - FTTH";
        $menu = $this->set_menu("salopp");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/selas/salopp",compact("datopp"));
        $this->load->view("panbd/frames/footer");
    }

    public function salinc() {
       if($this->session->userdata("id_div") != 12)
          redirect(base_url("panel/"));
        $idibo=$this->session->userdata('id_iboss');
        $datinc = $this->lokasi->seminc();
        $salesi = $this->lokasi->incsal($idibo);

        $judul = "Sales Incentive - FTTH";
        $menu = $this->set_menu("salinc");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/selas/salinc",compact("datinc","salesi","idibo"));
        $this->load->view("panbd/frames/footer");
    }
    public function logout() {
        $akt=$this->pengguna->tamakt('logout berhasil',$this->session->userdata('username'));
        $this->session->sess_destroy();
        redirect(base_url("login/"));
    }

    public function areasale($key) {
        $pilih = 'alamat';
        $param = str_replace("%20"," ",$key);
        $datodp = $this->odpdata->pilodp($pilih,$param);
        
        $judul = "Peta ODP";
        $menu = $this->set_menu("salopp");
        
        $this->load->view("panbd/selas/salarea",compact("datodp","param"));
        
    }

    public function salodp($id) {


        $dafpel = $this->odpdata->pelanggan($id);
       // $pelsemua = $this->laporan->pelanggan();
        
        $judul = "ODP - Daftar Pelanggan";
        $menu = $this->set_menu("salopp");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/selas/salodp",compact("dafpel","id"));
        $this->load->view("panbd/frames/footer");
    }

    public function pymhis($pil=null) {

        $post = $this->input->post();
        $pil=$this->input->post('nmpel');
        
        if(isset($post["btnCari"])) {
        
        $datpaid = $this->bayar->cekdata($pil);
        $detpaid = $this->bayar->paymdet($pil);
        $detsesi = $this->bayar->sesidet($pil);

        }else{
        $datpaid = $this->bayar->cekdata('');
        $detpaid = $this->bayar->paymdet('');
        }

        
        $datpela = $this->bayar->namapel();
        

        $judul = "Data Pembayaran";
        $menu = $this->set_menu("pymhis");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/tnemyap/pymhis",compact("detsesi","pil","detpaid","datpaid","datpela"));
        $this->load->view("panbd/frames/footer");
    }


    public function phibln($pil=null) {

        $post = $this->input->post();
        $pil=$this->input->post('crbln');
        
        if(isset($post["btnCari"])) {
        
        
        $detpaid = $this->bayar->paybln($pil);
        

        }else{
        
        $detpaid = $this->bayar->paybln('');
        }

        
        $datpela = $this->bayar->namapel();
        

        $judul = "Data Pembayaran";
        $menu = $this->set_menu("pymhis");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/tnemyap/phibln",compact("pil","detpaid"));
        $this->load->view("panbd/frames/footer");
    }
    public function setemail() {
        //if($this->session->userdata("id_jabatan") != 1)
        //    redirect(base_url("panbd/"));

        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $edi = $this->excel_import_model->simemail();
            if($edi != false) {
                redirect(base_url("panbd/blasin/?esuc"));
            } else {
                redirect(base_url("panbd/blasin/?eerr"));
            }
            exit();
        }



        $judul = "Pengaturan Email Pengirim";
        $menu = $this->set_menu("blainf");
        $datnot=$this->excel_import_model->setemail();

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/tnemyap/setema",compact("datnot"));
        $this->load->view("panbd/frames/footer");
    }
    
    public function kirnotem($id){

            $run= $this->excel_import_model->pilebl($id);
            if($run != false) {
                redirect(base_url("panbd/blasin/".$id."/?ebsu"));
                exit();
            }else{
                redirect(base_url("panbd/blasin/".$id."/?eber"));
                exit();
            }
            

    }


    public function kirnotwa($id){

            $run= $this->excel_import_model->pilbla($id);
            if($run != false) {
                redirect(base_url("panbd/blasin/".$id."/?bsuc"));
                exit();
            }else{
                redirect(base_url("panbd/blasin/".$id."/?berr"));
                exit();
            }
            

    }

    public function dropblas($id) {


            $run= $this->excel_import_model->hapbla($id);
            if($run != false) {
                redirect(base_url("panbd/blasin/".$id."/?hsuc"));
            } else {
                redirect(base_url("panbd/blasin/".$id."/?herr"));
            }
            exit();
    }

    public function blasin() { // $pil=null

     
        $post = $this->input->post();

        if(isset($post["btnImport"])) {
             
            $bla = $this->excel_import_model->ulobla();
            $this->imp2db();
            if($bla != false) {
                redirect(base_url("panbd/blasin/?succ"));
            } else {
                redirect(base_url("panbd/blasin/?err"));
            }
            exit();
        }
        $daffil=$this->excel_import_model->dfile();

        $judul = "Blasting Info";
        $menu = $this->set_menu("blainf");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/tnemyap/blainf",compact("daffil"));
        $this->load->view("panbd/frames/footer");
    }

    function imp2db()
    {
        $this->load->library('excel');

        $lf=$this->excel_import_model->lfile();

        foreach ($lf as $v) {
        	$idf=$v->id;
        	$naf=json_decode($v->nafi);
        }
        foreach($naf as $item){
        	$rfile=$item->file;
        }

        if(isset($rfile)){
            $path = "assets/uploads/tsalb/".$rfile; //$_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $id = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $us = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $na = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $pr = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $al = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $em = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $hp = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $re = $worksheet->getCellByColumnAndRow(7, $row)->getValue();


                    $data[] = array(
                        'id'        =>  $id,
                        'idfile'    =>  $idf,
                        'username'  =>  $us,
                        'nama'      =>  $na,
                        'produk'    =>  $pr,
                        'alamat'    =>  $al,
                        'email'     =>  $em,
                        'hp'        =>  $hp,
                        'registrasi'=>  $re
                    );
                }
            }
            $this->excel_import_model->insertblast($data);
            $jum=$highestRow-1;
            echo $jum.' data berhasil di import';
        }   
    }

    public function toexcel($pil=null) {
    $nafile=$pil;
    //$spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    //$spreadsheet->getDefaultStyle()->getFont()->setSize(8);
    $dafphi = $this->bayar->cekdata($pil);
    $dafrin = $this->bayar->paymdet($pil);
    /*$spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'Coba Lagi !');

    $writer = new Xlsx($spreadsheet);
    $writer->save('coba.xlsx');*/
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_phi.xlsx');
    $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(11);
    $worksheet = $spreadsheet->getActiveSheet();

    //$worksheet->getCell('B2')->setValue($naleng);
    //$worksheet->getCell('B3')->setValue($this->session->userdata('id_div'));
    //$worksheet->getCell('B4')->setValue($this->session->userdata('id_jabatan'));
    $no=1;
    $n=3;
    foreach ($dafphi as $h) {
    $worksheet->getCell('A'.$n)->setValue($no++);
    $worksheet->getCell('B'.$n)->setValue($h->username);
    $worksheet->getCell('C'.$n)->setValue(substr($h->nama,0,6));
    $worksheet->getCell('D'.$n)->setValue($h->thn);
    $worksheet->getCell('E'.$n)->setValue($h->JAN);  
    $worksheet->getCell('F'.$n)->setValue($h->FEB);
    $worksheet->getCell('G'.$n)->setValue($h->MAR);
    $worksheet->getCell('H'.$n)->setValue($h->APR);
    $worksheet->getCell('I'.$n)->setValue($h->MEI);
    $worksheet->getCell('J'.$n)->setValue($h->JUN);
    $worksheet->getCell('K'.$n)->setValue($h->JUL);
    $worksheet->getCell('L'.$n)->setValue($h->AGU);  
    $worksheet->getCell('M'.$n)->setValue($h->SEP);
    $worksheet->getCell('N'.$n)->setValue($h->OKT);
    $worksheet->getCell('O'.$n)->setValue($h->NOP);
    $worksheet->getCell('P'.$n)->setValue($h->DES);
    $worksheet->getCell('Q'.$n)->setValue($h->TOTAL);
    $n++;   
    }
    $nr=$n+2;
    $nr1=$nr+1;
    $worksheet->mergeCells('A'.$nr.':P'.$nr);
    $worksheet->getCell('A'.$nr)->setValue('RINCIAN TRANSAKSI');
    $worksheet->getStyle('A'.$nr)->getAlignment()->setHorizontal('center');
    $worksheet->getStyle('A'.$nr)->getFont()->setSize(14);

    $worksheet->getCell('A'.$nr1)->setValue('No.');
    $worksheet->getCell('B'.$nr1)->setValue('Tahun');

    $worksheet->mergeCells('C'.$nr1.':D'.$nr1);
    $worksheet->getCell('C'.$nr1)->setValue('ID#');
    $worksheet->getStyle('A'.$nr1.':P'.$nr1)->getAlignment()->setHorizontal('center');

    $worksheet->mergeCells('E'.$nr1.':G'.$nr1);
    $worksheet->getCell('E'.$nr1)->setValue('Nama');
    $worksheet->getStyle('E'.$nr1)->getAlignment()->setHorizontal('center');

    $worksheet->getCell('H'.$nr1)->setValue('Tgl. Bayar');
    $worksheet->getCell('I'.$nr1)->setValue('Periode');
    $worksheet->getCell('J'.$nr1)->setValue('Harga');
    $worksheet->getCell('K'.$nr1)->setValue('PPN');
    $worksheet->getCell('L'.$nr1)->setValue('Diskon');
    $worksheet->getCell('M'.$nr1)->setValue('Metoda');

    $worksheet->mergeCells('N'.$nr1.':P'.$nr1);
    $worksheet->getCell('N'.$nr1)->setValue('Kasir');
    $no2=1;
    $nr2=$nr+2;
    foreach ($dafrin as $h) {
    $worksheet->getCell('A'.$nr2)->setValue($no2++);
    $worksheet->getCell('B'.$nr2)->setValue($h->thn);
    $worksheet->mergeCells('C'.$nr2.':D'.$nr2);
    $worksheet->getCell('C'.$nr2)->setValue($h->username);
    $worksheet->getStyle('C'.$nr2)->getAlignment()->setHorizontal('center');

    $worksheet->mergeCells('E'.$nr2.':G'.$nr2);
    $worksheet->getCell('E'.$nr2)->setValue($h->name);
    $worksheet->getCell('H'.$nr2)->setValue($h->pay_date);
    $worksheet->getCell('I'.$nr2)->setValue($h->bln);   
    $worksheet->getCell('J'.$nr2)->setValue($h->price);
    $worksheet->getCell('K'.$nr2)->setValue($h->ppn);
    $worksheet->getCell('L'.$nr2)->setValue($h->discount);
    $worksheet->getCell('M'.$nr2)->setValue($h->vendor);
    $worksheet->mergeCells('N'.$nr2.':P'.$nr2);
    $worksheet->getCell('N'.$nr2)->setValue($h->kasir);
    $nr2++;
    }

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //header('Content-Disposition: attachment; filename='.date('Ymd').'_'.$nafile.'Payment_History.xlsx');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename='.date('YmdHis').'-'.$nafile.'-Payment_History.xls');    
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



    redirect(base_url("panbd/pymhis/"));

    }

    public function lapblnan($pil=null) {
    $nafile=$pil;

    $dafbyr = $this->bayar->paybln($pil);

    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_mre.xlsx');
    $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(11);
    $worksheet = $spreadsheet->getActiveSheet();

    $worksheet->mergeCells('A3:G3');
    $worksheet->getCell('A3')->setValue('LAPORAN PEMBAYARAN BULAN '.$nafile.'.');
    $worksheet->getStyle('A3')->getAlignment()->setHorizontal('center');
    $worksheet->getStyle('A3')->getFont()->setSize(14);


    $no=1;
    $n=6;
    foreach ($dafbyr as $h) {

    if(substr($h->name,0,2)=='R-'){$p=$h->price; $a=0;}else{$p=$h->price-5000;$a=5000;}

    $worksheet->getCell('A'.$n)->setValue($no++);
    $worksheet->getCell('B'.$n)->setValue($h->username);
    $worksheet->getCell('C'.$n)->setValue($h->name);
    $worksheet->getCell('D'.$n)->setValue($h->packet);
    $worksheet->getCell('E'.$n)->setValue($h->pay_date);  
    $worksheet->getCell('F'.$n)->setValue($h->bln);
    $worksheet->getCell('G'.$n)->setValue($p);
    $worksheet->getCell('H'.$n)->setValue($a);
    $worksheet->getCell('I'.$n)->setValue($h->ppn);
    $worksheet->getCell('J'.$n)->setValue($h->vendor);
    $worksheet->getCell('K'.$n)->setValue($h->kasir);
    $n++;   
    }


    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //header('Content-Disposition: attachment; filename='.date('Ymd').'_'.$nafile.'Payment_History.xlsx');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename='.date('YmdHis').'-LapBulan_'.$nafile.'.xls');    
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



    redirect(base_url("panbd/pymhis/"));

    }

    public function blaspro() { // $pil=null

     
        $post = $this->input->post();

        if(isset($post["btnImport"])) {
             
            $blap = $this->excel_import_model->iblapro();
            //$this->imp2db();
            if($blap != false) {
                redirect(base_url("panbd/blaspro/?succ"));
            } else {
                redirect(base_url("panbd/blaspro/?err"));
            }
            exit();
        }
        $daffil=$this->excel_import_model->dfpro();
        $dafpak=$this->excel_import_model->pakpro();

        $judul = "Blasting Promo";
        $menu = $this->set_menu("blapro");

        $this->load->view("panbd/frames/hawaln",compact("judul","menu"));
        $this->load->view("panbd/omorp/blapro",compact("daffil","dafpak"));
        $this->load->view("panbd/frames/footer");
    }

    
    private function set_menu($active) {
        $menu = [
            "dashboard"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "mombaru"=>"",
            "locdev"=>"",
            "salopp"=>"",
            "pymhis"=>"",
            "blainf"=>"",
            "blapro"=>"",
            "salinc"=>"",
            "komrek"=>"",
            "spkbru"=>"",
            "tikelol"=>"",
            "uskelol"=>"",
            "rokelol"=>"",
            "timanua"=>"",
            "tisemua"=>"",
            "laphri"=>"",
            "lapbln"=>"",
            "gasandi"=>"",
            "ladload"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }

    public function mailpro($id){

            $run= $this->blaspro->eblaspro($id);
            if($run != false) {
                redirect(base_url("panbd/blaspro/".$id."/?ebsu"));
                exit();
            }else{
                redirect(base_url("panbd/blaspro/".$id."/?eber"));
                exit();
            }
            

    }

    public function wapppro($id){

            $run= $this->blaspro->wblaspro($id);
            if($run != false) {
                redirect(base_url("panbd/blaspro/".$id."/?ebsu"));
                exit();
            }else{
                redirect(base_url("panbd/blaspro/".$id."/?eber"));
                exit();
            }
            

    }   


}