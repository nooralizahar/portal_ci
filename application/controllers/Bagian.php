<?php


defined("BASEPATH") OR exit("Akses ditolak!");

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Bagian extends CI_Controller {

    var $uriku;

    public function __construct() {
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("general");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->library('pdf');
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }

        $this->load->library('phpqrcode/qrlib');
        $this->load->helper('url');        
        $this->load->model("excel_import_model");
        $this->load->model("Divisi_model","divisi");
   

    }

    public function index() {
        if($this->jabku != 1 && $this->jabku != 11 )
            redirect(base_url($this->uriku."/"));
        
        $daftar_divisi = $this->divisi->divisi_semua();        

        $judul = "Daftar Bagian";
       
        $this->load->view("newlook-shared/rangka/nl-hawal",compact("judul"));
        $this->load->view("newlook-shared/rangka/nl-hatas");
        $this->load->view("newlook-shared/bagian/index",compact("daftar_divisi"));
        $this->load->view("newlook-shared/rangka/nl-hakhi");
    }


}