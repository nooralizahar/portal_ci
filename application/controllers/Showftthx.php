<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Showftthx extends CI_Controller {

        public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->model("Iboss_model","iboss");

    }


    public function index() {

        $judul = "DASHBOARD FTTH/X (realtime)";
        $this->load->view("display/hal/hawal",compact("judul"));
        $this->load->view("display/limiter");
        $this->load->view("display/hal/hakhi");
    }

    public function rshow() {

        // variable yang digunakan
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));
        $harini= date('Y-m-d');
        $harlal= date('Y-m-d',strtotime("-1 day"));

        // ambil data realtime iBoss
        $paktif=$this->iboss->jmlpel('ACCST-01');
        $pbloki=$this->iboss->jmlpel('ACCST-02');
        $ptermi=$this->iboss->jmlpel('ACCST-03');
        $totodp=$this->iboss->jmlodp();
        $capodp=$this->iboss->kapodp();
        $akttod=$this->iboss->pelakt($harini,'h');
        $aktlas=$this->iboss->pelakt($harlal,'h');
        $aktbni=$this->iboss->pelakt($blnini,'b');
        $aktbla=$this->iboss->pelakt($blnlal,'b');

        $odpemp=$this->iboss->odpkos();
        $odpful=$this->iboss->odpnuh();
        $odpbal=$this->iboss->odpsis('>=');
        $odpcil=$this->iboss->odpsis('<');
        $odpbni=$this->iboss->odpakt($blnini);
        $odpbla=$this->iboss->odpakt($blnlal);

        $pelthn=$this->iboss->detpelthn();
        $odpthn=$this->iboss->detodpthn();

        $roll_rekap_tahun = $this->iboss->roll_rekap_tahun();
        $odc_rekap_tahun = $this->iboss->odc_rekap_tahun();

        $dat = $this->input->post();


        if(isset($dat["btnKirim"])) {
           
            if($this->input->post('sandi') == 'B0Daccess') {
               
            $judul = "DASHBOARD FTTH/X (realtime)";
            $this->load->view("display/hal/hawal",compact("judul"));
            $this->load->view("display/dasftth",compact("odpthn","pelthn","odpbla","odpbni","odpcil","odpbal","odpful","odpemp","aktbni","aktbla","aktlas","akttod","capodp","totodp","ptermi","pbloki","paktif", "roll_rekap_tahun", "odc_rekap_tahun"));
            $this->load->view("display/hal/hakhi");

            } else {
                //redirect(base_url("showftthx/"));
                echo "<center><h1 style='color:red'><br><br>Kata sandi salah</h1></center>";
            }
           // exit();
        }


    }



}