<?php


defined("BASEPATH") OR exit("Akses ditolak!");

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Pengguna extends CI_Controller {

    var $uriku;

    public function __construct() {
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("general");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->library('pdf');
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }

        $this->load->library('phpqrcode/qrlib');
        $this->load->helper('url');        
        $this->load->model("excel_import_model");
        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Genral_model","genral");

    }

    public function index() {
        if($this->jabku != 1 && $this->jabku != 11 )
            redirect(base_url($this->uriku."/"));

        $daftar_pengguna = $this->pengguna->nsemua();        

        $judul = "Daftar Pengguna";
       
        $this->load->view("newlook-shared/rangka/nl-hawal",compact("judul"));
        $this->load->view("newlook-shared/rangka/nl-hatas");
        $this->load->view("newlook-shared/peguna/index",compact("daftar_pengguna"));
        $this->load->view("newlook-shared/rangka/nl-hakhi");
    }

    public function baru() {

    if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10 )
            redirect(base_url("newlook/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $tbh = $this->pengguna->tambah();
            if($tbh != false) {
                $this->genral->rimsan(1);
                redirect(base_url("pengguna/"));
            } else {
                $this->genral->rimsan(2);
                redirect(base_url("pengguna/baru/"));
            }
            exit();
        }

        $judul = "Pengguna Baru";
        
        $daftas = $this->pengguna->data_atasan();
        $dafhed = $this->pengguna->data_head();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $this->load->view("newlook-shared/rangka/nl-hawal",compact("judul"));
        $this->load->view("newlook-shared/rangka/nl-hatas");
        $this->load->view("newlook-shared/peguna/pbaru",compact("dafhed","daftas","daftar_jabatan","daftar_divisi"));
        $this->load->view("newlook-shared/rangka/nl-hakhi");
        
     
    }

    public function blokir($id = null) {
        if($this->jabku != 1 && $this->jabku != 11 )
            redirect(base_url($this->uriku."/"));
        if($id == null) redirect(base_url("/pengguna"));
        $this->pengguna->blokir($id);
        redirect(base_url("/pengguna/?succ=1"));
    }

    public function buka($id = null) {
        if($this->jabku != 1 && $this->jabku != 11 )
            redirect(base_url($this->uriku."/"));
        if($id == null) redirect(base_url("/pengguna"));
        $this->pengguna->buka($id);
        redirect(base_url("/pengguna/?succ=2"));
    }

    public function ubah($id = null) {

        $this->load->model("Lokasi_model","lokasi");

        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url("/newlook/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->edit();
            if($edt != false) {
                $this->genral->rimsan(1);
                redirect(base_url("/pengguna/"));
            } else {
                $this->genral->rimsan(2);
                redirect(base_url("/pengguna/ubah/".$post["id_pengguna"]));
            }
            exit();
        }

        if($id == null) redirect(base_url("/pengguna/"));
        $judul = "Ubah Pengguna";
       
        $datibo = $this->lokasi->usribo();
        $daftas = $this->pengguna->data_atasan();
        $dafhed = $this->pengguna->data_head();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("newlook-shared/rangka/nl-hawal",compact("judul"));
            $this->load->view("newlook-shared/rangka/nl-hatas");
            $this->load->view("newlook-shared/peguna/pubah",compact("dafhed","pengguna","datibo","daftar_jabatan","daftar_divisi","daftas"));
            $this->load->view("newlook-shared/rangka/nl-hakhi");
        } else redirect(base_url("/pengguna/"));
    }    

}