<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Hirarki extends CI_Controller {

    public function __construct() {
        parent::__construct();

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->helper("tgl_indo");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }

        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");


    }

    public function index($id=null) {
    

        $judul = "Hirarki Jabatan";
        $menu = $this->set_amenu("jabatan");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ikrarih/dashhir",compact("id"));
        $this->load->view("panel/frames/footer");
    }

    public function detil($id=null) {
    
        $dathir = $this->jabatan->hirpil($id);
        $nmid = $this->jabatan->jabpil($id);
        $judul = "Hirarki Jabatan";
        $menu = $this->set_amenu("jabatan");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ikrarih/arihdtil",compact("nmid","id","dathir"));
        $this->load->view("panel/frames/footer");
    }

    private function set_amenu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "harga_baru"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "purc_baru"=>"",
            "tesaja"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "aktifitas"=>"",
            "surat_terkirim"=>"",
            "disposisi_keluar"=>"",
            "disposisi_masuk"=>"",
            "pengguna"=>"",
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "jabatan"=>"",
            "inventory"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }


}