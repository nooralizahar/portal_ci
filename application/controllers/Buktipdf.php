<?php
Class Buktipdf extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->helper('general');
    }
    
    function index(){
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->SetMargins(12,1,1);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(30,4,' ',0,0,'C');
        $pdf->Cell(125,4,' ',0,0,'C');
        $pdf->Cell(30,4,' INTERNAL ',1,1,'C');

        $pdf->SetFont('Arial','B',13);
        // mencetak judul
        $pdf->Cell(190,6,'BUKTI PENGELUARAN BANK',0,1,'C');
        $pdf->SetFont('Arial','B',11);
        $pdf->Cell(190,3,'PT. JALA LINTAS MEDIA',0,1,'C');

        
        $pdf->Cell(10,2,'',0,1); // Jarak spasi kosong


        $pdf->SetFont('Arial','',10);

        $datu = $this->db->where('idt',2)->get('v_fat_knabraul')->result();
        foreach ($datu as $d){
            $pdf->Cell(30,5,'Dibayar kepada',0,0);
            $pdf->Cell(5,5,' : ',0,0);
            $pdf->Cell(40,5,$d->dik,0,1);
            $pdf->Cell(30,5,'Nominal',0,0);
            $pdf->Cell(5,5,' : ',0,0);
            $pdf->Cell(40,5,number_format($d->nit).',-',0,1); 
            $pdf->Cell(30,5,'Terbilang',0,0);
            $pdf->Cell(5,5,' : ',0,0);
            $pdf->SetFont('Arial','B',11);
            $pdf->setFillColor(230,230,230);
            $pdf->Cell(100,5,terbilang($d->nit),0,1,'L',1); 
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(30,5,'Supplier invoice',0,0);
            $pdf->Cell(5,5,' : ',0,0);
            $pdf->Cell(40,5,$d->sui,0,1); 
            $ppn=$d->p10;
            $pph=$d->p23;

        }
        $n=1;
        $dadu = $this->db->where('idt',2)->get('fat_knabraul_ket')->result();
        foreach ($dadu as $d){
            if($n==1){ $k='Keterangan'; $t=' : ';}else{$k='';$t='   ';}

            $pdf->Cell(30,5,$k,0,0);
            $pdf->Cell(5,5,$t,0,0);
            $pdf->Cell(115,5,$d->ket,0,0);
            $pdf->Cell(10,5,'Rp. ',0,0);
            $pdf->Cell(25,5,number_format($d->nit).',-',0,1,'R'); 
            $n++;
        }
        $cekdat=$this->db->where('idt',2)->get('fat_knabraul_ket')->num_rows();


        if($cekdat==1){$x=5;}elseif($cekdat==1){$x=4;}elseif($cekdat==2){$x=3;}elseif($cekdat==3){$x=3;}elseif($cekdat==4){$x=2;}else{$x=1;}
        for ($i=0; $i < $x; $i++) { 
            $pdf->Cell(30,5,'',0,0);
            $pdf->Cell(130,5,' ',0,0);
            $pdf->Cell(25,3,'-',0,1,'R'); 
        }

        $suto = $this->db->select("sum(nit) as subtot")->where('idt',2)->get('fat_knabraul_ket')->row();
            $pdf->Cell(30,3,'',0,0);
            $pdf->Cell(130,3,'',0,0,'R');
            $pdf->Cell(25,3,"------------------------------ +",0,1,'R'); 

            $pdf->Cell(30,5,'',0,0);
            $pdf->Cell(130,5,'Subtotal',0,0,'R');
            $pdf->Cell(25,5,number_format($suto->subtot).",-",0,1,'R'); 

            $pdf->Cell(30,5,'',0,0);
            $pdf->Cell(130,5,'PPN',0,0,'R');
            $pdf->Cell(25,5,number_format($ppn).",-",0,1,'R'); 

            $pdf->Cell(30,5,'',0,0);
            $pdf->Cell(130,5,'PPH 23',0,0,'R');
            $pdf->Cell(25,5,number_format(-1*($pph)).",-",0,1,'R'); 


			$pdf->Cell(185,1,' ','T',1); // jarak spasi kosong
			//$pdf->Cell(185,1,'',0,1); // jarak spasi kosong

            $pdf->SetFont('Arial','I',10);
            $pdf->Cell(125,5,'                     Creditor A/C  :','T',0);
            $pdf->Cell(60,5,' PO No. : ','T',1);
            $pdf->Cell(125,5,'                   Bank Transfer :','T',0);
            $pdf->Cell(60,5,'SPK No. : ','T',1);  
            $pdf->Cell(185,1,'',0,1);
            $pdf->Cell(185,1,' ','T',1); // jarak spasi kosong

            $pdf->SetFont('Arial','',10);
     		$pdf->Cell(30,5,'Disiapkan ',1,0,'C');
        	$pdf->Cell(50,5,'Verifikasi',1,0,'C');
        	$pdf->Cell(75,5,'Menyetujui',1,0,'C');
        	$pdf->Cell(30,5,'Penerima',1,1,'C');

			$pdf->Cell(30,18,' ',1,0);
			$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(30,18,' ',1,1);           

        	$pdf->SetFont('Arial','',9);
			$pdf->Cell(30,4,'Finance A/P',1,0,'C');
			$pdf->Cell(25,4,'Presales',1,0,'C');
        	$pdf->Cell(25,4,'FAM',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(30,4,' ',1,1,'C');
        	$pdf->SetFont('Arial','',8);
        	$pdf->Cell(30,4,'(                     )',1,0,'C');
			$pdf->Cell(25,4,'(                     )',1,0,'C');
        	$pdf->Cell(25,4,'( Henry )',1,0,'C');
        	$pdf->Cell(25,4,'( Viktor Irianto )',1,0,'C');
        	$pdf->Cell(25,4,'( Yulius Purnama )',1,0,'C');
        	$pdf->Cell(25,4,'( Edwind J.B. )',1,0,'C');
        	$pdf->Cell(30,4,'(                     )',1,1,'C');

        $pdf->Output();
    }
}