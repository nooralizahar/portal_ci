<?php


defined("BASEPATH") OR exit("Akses ditolak!");
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Talian extends CI_Controller {

    public function __construct() {
        parent::__construct();

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }
        $this->uriku=$this->uri->segment(1);
        $this->load->library('phpqrcode/qrlib');
        $this->load->helper('url');
        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Talian_model","talian");

    }

    public function index() {

        $post = $this->input->post();
        $idta=$this->input->post('idta');

        if(isset($post["btnSimpan"])) {
            $sim = $this->talian->simtal();
                if ($this->session->userdata('id_spv') == 38) {
                    $data = array(
                        "status_approve" => 1,
                        "tgl_approve" => date('Y-m-d H:i:s'),
                    );
                    $this->talian->autoApprove($sim, $data);
                }
            if($sim != false) {
                $akt=$this->pengguna->tamakt('Pembelian Baru No. PR '.$idta,$this->session->userdata('username'));
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data berhasil disimpan </div>'); 
                redirect(base_url("talian/sirang/".$idta));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data gagal disimpan </div>');               
                redirect(base_url("talian/sirang/".$idta));
            }
            exit();
        }

        $dapr=$this->talian->pureku(); // user request
        $sepr=$this->talian->dapure(); // semua request
        $dapr_prv=$this->talian->pureprove(); // approve request

        $judul = "Permintaan Pembelian";
        $menu = $this->set_amenu("talian");


        if($this->db->get('app_latrop_nailat')->num_rows()==0){
        $nopr=1;    
        }else{
        $np=$this->db->order_by('idta','desc')->limit(1)->get('app_latrop_nailat')->row();
        $nopr=$np->idta+1;
        }

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("share/nailat/taliru",compact("nopr","dapr","sepr", "dapr_prv"));
        $this->load->view("panel/frames/footer");
    }

    public function sirang($id) {

        $post = $this->input->post();
        
        if(isset($post["btnSimpan"])) {
            $sim = $this->talian->tambar();
            if($sim != false) {
                
                $akt=$this->pengguna->tamakt('Tambah Barang'.$this->input->post('jeba').' '.$this->input->post('jeba').' No. PR '.$id,$this->session->userdata('username'));

                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data berhasil ditambahkan </div>'); 
                redirect(base_url($this->uriku."/sirang/".$id));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data gagal ditambahkan </div>');
                redirect(base_url($this->uriku."/sirang/".$id));
            }
            exit();
        }

        $jeba=$this->talian->cbjeba();
        $daba=$this->talian->darang($id);
        $depr=$this->talian->depure($id);
        $dapr_prv=$this->talian->pureprove(); // approve request

        $judul = "Detil Pembelian";
        $menu = $this->set_amenu("talian");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("share/nailat/sirang",compact("id","jeba","daba","depr","dapr_prv"));
        $this->load->view("panel/frames/footer");
    }

    public function approval($id)
    {
        $data = array(
            "status_approve" => 1,
            "tgl_approve" => date('Y-m-d H:i:s'),
        );

        $update = $this->talian->approval($id, $data);

        $nailat = $this->talian->get_nailat($id);
        $pengguna = $this->talian->get_pengguna($nailat->usbu);
        $spv = $this->talian->get_pengguna($pengguna->id_spv);

        if ($update) {

            $n=sprintf("%04d",$id);

            $this->load->library('phpmailer_lib');
            $mail = $this->phpmailer_lib->load();

            $email_admin = 'nooralizah@jlm.net.id';
            $nama_admin = 'Aplikasi Portal - PR';
            $password_admin = 'jlm2021-@#$';

            $mail->isSMTP();
            $mail->SMTPKeepAlive = true;
            $mail->Charset  = 'UTF-8';
            $mail->IsHTML(true);
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = true;
            $mail->Host = "mail.jlm.net.id";
            $mail->SMTPSecure = "tls";
            $mail->Port = 587;
            $mail->Username = $email_admin;
            $mail->Password = $password_admin;
            $mail->WordWrap = 500;

            $mail->setFrom($email_admin, $nama_admin);
            $mail->addAddress('olivia@jlm.net.id');
            $mail->addAddress('pamella@jlm.net.id');
            // $mail->addAddress('noor.afifah.r25@gmail.com');

            $mail->Subject          = "Purchase Request - Pemohon ".$pengguna->nama_lengkap.", Dept. ".$pengguna->nm_div.", No. PR. ".$n;
            $mail_data['subject']   = $n.'_'.$pengguna->nama_lengkap;
            $mail_data['induk']     = 'Permintaan - '.$n.'_'.$pengguna->nama_lengkap;
            // pesan

            $mail->Body = "
                <br>Kepada Bpk/Ibu Yth
                <br><br>
                <p>Berikut Reminder Purchase Request yang telah di approve, atas nama:</p>
            <br>Nama Lengkap: ". $pengguna->nama_lengkap."<br>No. PR: ".$n."<br>Department: ".$pengguna->nm_div."<br>Tanggal dibutuhkan: ".date('d M Y', strtotime($nailat->tgta))."<br><br>Sekian dan Terimah Kasih. <br> url: <a target='_blank' href='".base_url($this->uriku."/talian")."'>Portal Bnetfit</a>";

            if ($mail->send()) {
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Reminder berhasil dikirimkan kepada penanggung jawab PO </div>');
                redirect(base_url($this->uriku));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Reminder gagal dikirimkan kepada penanggung jawab PO </div>');
                redirect(base_url($this->uriku));
            }
        }
        $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Permintaan pembelian barang gagal di approve </div>');
        redirect(base_url($this->uriku));

    }

    
    public function sirang_detail($id) {

        $jeba=$this->talian->cbjeba();
        $daba=$this->talian->darang($id);
        $depr=$this->talian->depure($id);

        $judul = "Detil Pembelian";
        $menu = $this->set_amenu("talian");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("share/nailat/detail",compact("id","jeba","daba","depr"));
        $this->load->view("panel/frames/footer");
    }

    public function harang($id) {
        $ref=$this->db->where("idre",$id)->get("app_latrop_nailat_rel")->row();

        $h = $this->talian->pusbar($id);
            if($h){
            $akt=$this->pengguna->tamakt('Hapus barang id# '.$id,$this->session->userdata('username'));
            $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data berhasil dihapus </div>');
            
            }else{
            $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data gagal dihapus </div>');
            }
               redirect(base_url("talian/sirang/".$ref->idta));  
    }

    public function send_email_permintaan($id)
    {
        $nailat = $this->talian->get_nailat($id);
        $pengguna = $this->talian->get_pengguna($nailat->usbu);
        $spv = $this->talian->get_pengguna($pengguna->id_spv);

        if (isset($spv->email)) {

            $email_penerima = $spv->email;
            $n=sprintf("%04d",$id);

            $this->load->library('phpmailer_lib');
            $mail = $this->phpmailer_lib->load();

            $email_admin = 'nooralizah@jlm.net.id';
            $nama_admin = 'Aplikasi Portal - PR';
            $password_admin = 'jlm2021-@#$';

            $mail->isSMTP();
            $mail->SMTPKeepAlive = true;
            $mail->Charset  = 'UTF-8';
            $mail->IsHTML(true);
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = true;
            $mail->Host = "mail.jlm.net.id";
            $mail->SMTPSecure = "tls";
            $mail->Port = 587;
            $mail->Username = $email_admin;
            $mail->Password = $password_admin;
            $mail->WordWrap = 500;

            $mail->setFrom($email_admin, $nama_admin);
            $mail->addAddress($email_penerima);
            // $mail->addAddress('noor.afifah.r25@gmail.com');

            $mail->Subject          = "Approval Request - Pemohon ".$pengguna->nama_lengkap.", Dept. ".$pengguna->nm_div.", No. PR. ".$n;
            $mail_data['subject']   = $n.'_'.$pengguna->nama_lengkap;
            $mail_data['induk']     = 'Permintaan - '.$n.'_'.$pengguna->nama_lengkap;
            // pesan

            $mail->Body = "
                <br>Kepada Bpk/Ibu Yth
                <br><br>
                <p>Berikut Reminder Approval Permintaan Pembelian Barang Atas Nama:</p>
            <br>Nama Lengkap: ". $pengguna->nama_lengkap."<br>No. PR: ".$n."<br>Department: ".$pengguna->nm_div."<br>Tanggal dibutuhkan: ".date('d M Y', strtotime($nailat->tgta))."<br><br>Sekian dan Terimah Kasih. <br> url: <a target='_blank' href='".base_url($this->uriku."/talian")."'>Portal Bnetfit</a>";

            if ($mail->send()) {
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Reminder berhasil dikirimkan kepada supervisor </div>');
                redirect(base_url($this->uriku."/talian?succ"));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Reminder gagal dikirimkan kepada supervisor </div>');
                redirect(base_url($this->uriku."/talian?err"));
            }
        }
        $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Email Supervisor kosong, tidak bisa mengirimkan reminder </div>');
        redirect(base_url($this->uriku."/talian?err"));
    }

    private function set_amenu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "harga_baru"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "survey"=>"",
            "salesrevenue"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "talian"=>"",
            "menuotb"=>"",
            "lokaotb"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "aktifitas"=>"",
            "surat_terkirim"=>"",
            "menu_eng"=>"",
            "disposisi_masuk"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "inventory"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",         
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "meiga"=>"",
            "meemp"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }




}