<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kodeqr extends CI_Controller {

	function __construct ()
	{	
		parent::__construct();
		$this->load->library('phpqrcode/qrlib');
		$this->load->helper('url');
	}

	public function index()
	{	
		$this->load->view('layout/hawal.php');
		$this->load->view('qrview/hqrcode.php');
		$this->load->view('layout/hakhir.php');
	}

	public function qrcodeGen($param)
	{

		$qrtext = str_replace(' ','_', $param);
		
		if(isset($qrtext))
		{

			//file path for store images
		    $SERVERFILEPATH = $_SERVER['DOCUMENT_ROOT'].'/siratpa/assets/qrfiles/';
		   
			$text = $qrtext;
			$text1= substr($text, 0,9);
			
			$folder = $SERVERFILEPATH;
			$file_name1 = $text1."-qr" . rand(2,200) . ".png";
			$file_name = $folder.$file_name1;
			QRcode::png($text,$file_name);


			//echo"<center><img src=".'http://150.107.140.204/siratpa/assets/qrfiles/'.$file_name1."></center";
			
		}
		else
		{
			//echo 'No Text Entered';
		}	
	}
}
