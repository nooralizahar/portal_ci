<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Display extends CI_Controller {

        public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->model("Nocmainte_model","mainte");

    }


    public function index() {
   
        $judul = "MAINTENANCE INFO";
        $maintj  = $this->mainte->jadtam();
        $barisj  = $this->mainte->jadhit();
        $this->load->view("display/nocmtc",compact("judul","maintj","barisj"));
    }

    



}