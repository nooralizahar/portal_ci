<?php


defined("BASEPATH") OR exit("Akses ditolak!");
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;




class Otbmen extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->usrku=$this->session->userdata("username");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }
        
        $this->load->library('phpqrcode/qrlib');
        $this->load->helper('url');
        $this->load->model("Otbdata_model","otbdat");
        $this->load->model("Genral_model","genral");


    }

    public function index() {
   

        $judul = "Menu Optical Termination Box (OTB)";
        $menu = $this->set_amenu("menuotb");


        if($this->divku==18 || $this->divku==20){
            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
            $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("panel/tounem/menuotb");
        $this->load->view("share/akgnar/hakhi");

    }

    public function potjc($id) {
        $dotjc = $this->otbdat->petapil($id);
        $potjc = $this->otbdat->pilcent($id);
        $otb = $this->otbdat->otbpil($id);
        $cap=$otb->jm_por;



        $judul = "PETA JALUR OTB - JCL";
        $menu = $this->set_amenu("menufoc");
        
        $this->load->view("panel/tounem/petaotb",compact("dotjc","id","potjc","cap"));
        
    }

    public function jclha($i=null){

        $hap = $this->otbdat->jclhap($i);

            if($hap){
                $akt=$this->genral->tamakt('Hapus JC '.$id.'',$this->usrku);
                redirect(base_url("otbmen/?hsucc"));
            }else{
                redirect(base_url("otbmen/?herr"));
            }
    
    }

    public function ganma($pi=null) {

        $otb = $this->otbdat->otbpil($pi);

        $old = $this->input->post('nm_otb_old');
        $new = $this->input->post('nm_otb');

        $dat = $this->input->post();


        if(isset($dat["btnSimpan"])) {
            $s = $this->otbdat->updnam();
            if($s != false) {
                $akt=$this->genral->tamakt('Ganti Nama OTB dari '.$old.' menjadi '.$new,$this->usrku);
                redirect(base_url("otbmen/otbli/?succ"));
            } else {
                redirect(base_url("otbmen/otbli/?err"));
            }
            exit();
        }
   

        $judul = "Edit OTB";
        $menu = $this->set_amenu("menuotb");
        
        if($this->divku==18 || $this->divku==20){
            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
            $this->load->view("panel/frames/header",compact("judul","menu"));
        }

        $this->load->view("panel/tounem/named",compact("otb"));
        $this->load->view("share/akgnar/hakhi");

    }



    public function otbjc($ido=null){
        

        $otpil= $this->otbdat->otbpil($ido);
        $jcsem= $this->otbdat->jclsem();
        $jcpil= $this->otbdat->jcllih($ido);


        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $sim = $this->otbdat->jclsim();
            if($sim != false) {
                $akt=$this->genral->tamakt('Tambah JC ',$this->usrku);
                redirect(base_url("otbmen/otbjc/".$ido."?succ"));
            } else {
                redirect(base_url("otbmen/otbjc/".$ido."?err"));
            }
            exit();
        }

        $judul = "Tambah JC";
        $menu = $this->set_amenu("menuotb");
        
        if($this->divku==18 || $this->divku==20){
            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
            $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        
        $this->load->view("panel/tounem/otbjc",compact("otpil","jcsem","jcpil"));
        $this->load->view("share/akgnar/hakhi");       


    }

    public function otbli() {

        

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $tbh = $this->otbdat->ottam();
            if($tbh != false) {
                $akt=$this->genral->tamakt('tambah OTB ',$this->usrku);
                redirect(base_url("otbmen/otbli/?succ"));
            } else {
                redirect(base_url("otbmen/otbli/?err"));
            }
            exit();
        }


        $otbsem = $this->otbdat->otsem();
        $daflok = $this->otbdat->losem();
       
        
        $judul = "FOC - Optical Termination Box";
        $menu = $this->set_amenu("menuotb");

        if($this->divku==18 || $this->divku==20){
            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
            $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("panel/tounem/otbli",compact("otbsem","daflok"));
        $this->load->view("share/akgnar/hakhi");

    }

    public function otblo() {

      

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $tbh = $this->otbdat->lotam();
            if($tbh != false) {
                $akt=$this->genral->tamakt('Tambah lokasi ',$this->usrku);
                redirect(base_url("otbmen/otblo/?succ"));
            } else {
                redirect(base_url("otbmen/otblo/?err"));
            }
            exit();
        }


        $otblosem = $this->otbdat->losem();
       
        
        $judul = "FOC - Optical Termination Box";
        $menu = $this->set_amenu("menuotb");
        $menu = $this->set_amenu("lokaotb");

        if($this->divku==18 || $this->divku==20){
            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
            $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("panel/tounem/otblo",compact("otblosem"));
        $this->load->view("share/akgnar/hakhi");
    }





    public function otbti($pi=null) {
   
        $otbde = $this->otbdat->otlih($pi);
        $otbpi = $this->otbdat->otpil($pi);
        foreach ($otbpi as $a) {
           $onm=$a->nm_otb;
        }

        $judul = "Detil OTB";
        $menu = $this->set_amenu("menuotb");
        

        if($this->divku==18 || $this->divku==20){
            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
            $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("panel/tounem/otbti",compact("otbde","pi","onm"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function otbed($ido=null,$idp=null) {
   
        $otbde = $this->otbdat->otlih($idp);
        $corpi = $this->otbdat->copil($idp);

        $otbco = $this->otbdat->otlink($ido);
        $otbcx = $this->otbdat->otlinx($ido);
        $corst = $this->otbdat->otcor($idp);
        // foreach ($otbpi as $a) {
        //   $onm=$a->nm_otb;
        // }

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $sim = $this->otbdat->cosim($idp);
            if($sim != false) {
                $akt=$this->genral->tamakt('edit OTB '.$ido.'port '.$idp.'',$this->usrku);
                redirect(base_url("otbmen/otbti/".$ido."?succ"));
            } else {
                redirect(base_url("otbmen/otbti/".$ido."/".$idp."?err"));
            }
            exit();
        }

        $judul = "Edit OTB";
        $menu = $this->set_amenu("menuotb");
        

        if($this->divku==18 || $this->divku==20){
            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
            $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("panel/tounem/otbed",compact("corpi","otbco","corst","otbcx"));//,"pi","onm"
        $this->load->view("share/akgnar/hakhi");
    }



    public function otbha($id=null) {
   
        $hap = $this->otbdat->otpus($id);

            if($hap){
                $akt=$this->genral->tamakt('Hapus OTB '.$id.'',$this->usrku);
                redirect(base_url("otbmen/?hsucc"));
            }else{
                redirect(base_url("otbmen/?herr"));
            }
    }

    public function loked($idl=null) {
   
        
        $lokpi = $this->otbdat->pillo($idl);

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $sim = $this->otbdat->simlo($idl);
            if($sim != false) {
                $akt=$this->genral->tamakt('edit lokasi '.$idl.'',$this->usrku);
                redirect(base_url("otbmen/otblo/".$idl."?succ"));
            } else {
                redirect(base_url("otbmen/loked/".$idl."?err"));
            }
            exit();
        }

        $judul = "Edit Lokasi";
        $menu = $this->set_amenu("menuotb");
        
        if($this->divku==18 || $this->divku==20){
            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
            $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("panel/tounem/loked",compact("lokpi"));
        $this->load->view("share/akgnar/hakhi");


    }

    public function eksporxls($pi=null) {
    $nafile=str_replace(' ', '', $this->session->userdata('nama_lengkap'));
    $naleng=$this->session->userdata('nama_lengkap');
    
    $otbde = $this->otbdat->otlih($pi);
    $otbpi = $this->otbdat->otpil($pi);
    foreach ($otbpi as $a) {
           $onm=$a->nm_otb;
    }
    /*$spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'Coba Lagi !');

    $writer = new Xlsx($spreadsheet);
    $writer->save('coba.xlsx');*/
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_otb.xlsx');

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getCell('A2')->setValue($onm);
    //$worksheet->getCell('B3')->setValue($this->session->userdata('id_div'));
    //$worksheet->getCell('B4')->setValue($this->session->userdata('id_jabatan'));

    $n=6;
    foreach ($otbde as $dat) {
    $worksheet->getCell('A'.$n)->setValue($dat->ou_des);
    $worksheet->getCell('B'.$n)->setValue($dat->id_cor);
    $worksheet->getCell('D'.$n)->setValue($dat->nm_gab);
    $worksheet->getCell('E'.$n)->setValue($dat->tg_upd." oleh ".$dat->us_upd);

    $n++;   
    }


    //$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');

    //$writer->save('assets/temp/'.date('Ymd').'_'.$nafile.'DailyReport.xls');

    
    /*header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename='.date('Ymd').'_'.$nafile.'DailyReport.xls');
    header('Cache-Control: max-age=0');
    $writer = new Xlsx($spreadsheet);
    $writer->save("php://output");*/
    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename=Data_'.str_replace(' ', '-',$onm).'_ExpBy_'.$nafile.'-'.date('Ymd').'.xls');
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");


    $akt=$this->genral->tamakt('eksport to XLS '.$pi.'',$this->usrku);
    redirect(base_url("otbmen/otbli/"));

    }




    private function set_amenu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "menu_eng"=>"",
            "harga_baru"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "talian"=>"",
            "menuotb"=>"",
            "lokaotb"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "aktifitas"=>"",
            "surat_terkirim"=>"",
            "disposisi_keluar"=>"",
            "disposisi_masuk"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "inventory"=>"",
            "menol"=>"",
            "mesat"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",
            "pengguna"=>"",
            "divisi"=>"", 
            "jabatan"=>"",
            "kaarea"=>"",
            "talian"=>"",           
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "meena"=>"",
            "ladload"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }

    public function logout() {
        $akt=$this->genral->tamakt('Logout berhasil',$this->usrku);
        redirect(base_url("nuber/logout/"));
    }
    
    public function peta($id) {
        $datodp = $this->otbdat->pilih($id);
        $judul = "Lokasi ODP";
        $menu = $this->set_amenu("menuotb");
        
        $this->load->view("panel/tounem/odplo",compact("datodp"));
        
    }

    public function petap($id) {
        $datpel = $this->otbdat->pilihp($id);
        $judul = "Lokasi Pelanggan";
        $menu = $this->set_amenu("menuotb");
        
        $this->load->view("panel/tounem/pello",compact("datpel"));
        
    }

    public function lopel($id) {
        $datpel = $this->otbdat->pelanggan($id);
        $odp = $this->otbdat->selodp($id);
        foreach($odp as $d): 
        $nodp=$d->nama; 
        $kodp=$d->kordinat;
        $aodp=$d->alamat; 
        $sodp=$d->sisa;  
        endforeach;
        $judul = "Lokasi Pelanggan";
        $menu = $this->set_amenu("menuotb");
        
        $this->load->view("panel/tounem/lopel",compact("datpel","nodp","kodp","aodp","sodp"));
        
    }

    public function jaodp() {
        $pilih = $this->input->post('pilih');
        $param = $this->input->post('keyword');
        $datodp = $this->otbdat->pilodp($pilih,$param);
        
        $judul = "Peta ODP";
        $menu = $this->set_amenu("menuotb");
        
        $this->load->view("panel/tounem/jaodp",compact("datodp","param"));
        
    }



}