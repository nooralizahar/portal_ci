<?php


defined("BASEPATH") OR exit("Akses ditolak!");

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Panel extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->helper("tgl_indo");
        $this->load->helper("chat");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }
        $this->load->model('excel_import_model');

        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Harga_model","harga");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Diskusi_model","diskusi");
        $this->load->model("Surat_model","surat");
        $this->load->model("Notadinas_model","notadinas");
        $this->load->model("Datait_model","datait");
        $this->load->model("Wordata_model","wordata");
        $this->load->model("Kodrat_model","kodrat");
        $this->load->model("Nmcfee_model","nmcfee");
        $this->load->model("Rolout_model","rolout");
        $this->load->model("Genral_model","genral");
        //$this->load->model("Lokasi_model","lokasi");
        $this->load->model("Iboss_model","iboss");
        $this->load->model("Disposisi_model","disposisi");
        $this->load->model("Pemberitahuan_model","pemberitahuan");
    }

    public function index() {
        //if($this->session->userdata("id_jabatan") >31)
          //  redirect(base_url("panbd/"));
        $thnini= date('Y');
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));
        $harini= date('Y-m-d');
        $harlal= date('Y-m-d',strtotime("-1 day"));

        $judul = "Portal Dashboard";
        $menu = $this->set_menu("dashboard");
        $dafcro=$this->harga->gcro();
        $dafcropv=$this->harga->gcropv();
        $dafreq=$this->harga->gpsaleso();
        $dafrsamg=$this->harga->gsalesmg();
        $dafrsa=$this->harga->gsales();
        $dafrsapv=$this->harga->gsalespv();
        $dafmua=$this->harga->semua();
        $dafnmc=$this->harga->semnmc();
        $persal=$this->harga->pilpersal($blnini);
        $persall=$this->harga->pilpersal($blnlal);
        $persegi=$this->harga->pilperseg($blnini);
        $persegl=$this->harga->pilperseg($blnlal);
        $woblni=$this->harga->pilwo($blnini);
        $woblnl=$this->harga->pilwo($blnlal);

        $pilhun=$this->harga->lihun();
        
        $post = $this->input->post();
        $tgl=$this->input->post("tgl");

        if(isset($post["btnGen"])) {    
            if(!empty($tgl)){
                $tgl=$this->input->post("tgl");
                
            }else{
                $tgl=date('Y');
               
            }
        }

        $summary=$this->harga->summary();
        $summarypre=$this->harga->summarypre($tgl);
        $totalpre=$this->harga->summaryprethn($tgl);
        $summarydur=$this->harga->summarydur();
        $wosum=$this->harga->wosummary();
        $wosal=$this->harga->wosales();
        $woseg=$this->harga->wosegme();
        $wosta=$this->harga->wostatu();
        $dafaktif = $this->datait->laporanpilih();
        $dafmserv=$this->harga->gmanser(); // manage service

        //iboss data linked
        $paktif=$this->iboss->jmlpel('ACCST-01');
        $pbloki=$this->iboss->jmlpel('ACCST-02');
        $ptermi=$this->iboss->jmlpel('ACCST-03');
        $totodp=$this->iboss->jmlodp();
        $capodp=$this->iboss->kapodp();
        $akttod=$this->iboss->pelakt($harini,'h');
        $aktlas=$this->iboss->pelakt($harlal,'h');
        $aktbni=$this->iboss->pelakt($blnini,'b');
        $aktbla=$this->iboss->pelakt($blnlal,'b');
        $odpemp=$this->iboss->odpkos();
        $odpful=$this->iboss->odpnuh();
        $odpbal=$this->iboss->odpsis('>=');
        $odpcil=$this->iboss->odpsis('<');
        //iboss rekapitulasi
        $odpthn=$this->iboss->detodpthn();
        $odplok=$this->iboss->detodplok($thnini);

        if($this->session->userdata("id_jabatan") != 17){
        $this->load->view("panel/frames/header",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/hawaln",compact("judul","menu"));
        }
        $this->load->view("panel/index",compact("odplok","odpthn","aktbni","aktbla","odpcil","odpbal","odpful","odpemp","aktlas","akttod","capodp","totodp","ptermi","pbloki","paktif","wosta","woseg","wosal","dafcro","dafcropv","wosum","woblni","woblnl","dafnmc","dafaktif","dafreq","dafmua","dafrsa","dafrsapv","dafrsamg","persal","persall","persegi","persegl","summary","summarypre","summarydur","tgl","pilhun","totalpre"));
        $this->load->view("panel/frames/footer");
    }

    public function kabbar() {
        if($this->divku != 20 && $this->divku != 4 )
            redirect(base_url($this->uriku."/logout/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {

            $kab = $this->rolout->simkab();
            if($kab != false) {
            $akt=$this->genral->tamakt('Kabel Baru # '.$al."Tiang #",$this->usrku);
                $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/kabbar/"));
            } else {
                $this->genral->rimsan(2);
                redirect(base_url($this->uriku."/kabbar/"));
            }
            exit();
        }

        $dafkab=$this->rolout->tamkab();
        $dafseg=$this->rolout->cbkaseg();
        $dafjka=$this->rolout->cbkajen();

        $judul = "Data Kabel";
        $menu = $this->set_menu("medua");

        $this->load->view($this->uriku."/frames/header",compact("judul","menu"));
        $this->load->view("share/tuolor/lebtup",compact("dafkab","dafseg","dafjka"));
        $this->load->view($this->uriku."/frames/footer");

    }


    public function puskab($a=null) {
        if($this->divku != 20 && $this->divku != 4 )
            redirect(base_url($this->uriku."/logout/"));

        $h = $this->rolout->puskab($a);
            if($h){
            $akt=$this->genral->tamakt('Hapus Kabel ID #'.$a,$this->usrku);
                 $this->genral->rimsan(3);
                redirect(base_url($this->uriku."/kabbar/"));
            }else{
                $this->genral->rimsan(4);
                redirect(base_url($this->uriku."/kabbar/")); 
            }
    }




    public function profilku($id = null) {
        //if($this->session->userdata("id_jabatan") != 1)
            //redirect(base_url("panel/"));
	 $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->editpro();
            if($edt != false) {
                redirect(base_url("panel/profilku/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url("panel/profilku/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        //if($id == null) redirect(base_url("panel/uskelol/"));
        $judul = "Edit Profil";
        $menu = $this->set_menu("uskelol");
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panel/frames/header",compact("judul","menu"));
            $this->load->view("panel/person/profilku",compact("pengguna","daftar_jabatan","daftar_divisi","daftas"));
            $this->load->view("panel/frames/footer");
        } else redirect(base_url("panel/pengguna/"));
    }

    public function pengguna() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10 )
            redirect(base_url("panel/"));

        $daftar_pengguna = $this->pengguna->semua();

        $judul = "Tata Kelola Pengguna";
        $menu = $this->set_menu("pengguna");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/pengguna",compact("daftar_pengguna"));
        $this->load->view("panel/frames/footer");
    }

    public function penggunatamb() {
    if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10 )
            redirect(base_url("panel/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $tbh = $this->pengguna->tambah();
            if($tbh != false) {
                redirect(base_url("panel/penggunatamb/?succ"));
            } else {
                redirect(base_url("panel/penggunatamb/?err"));
            }
            exit();
        }

        $judul = "Tambah Pengguna";
        $menu = $this->set_menu("pengguna");
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/penggunatamb",compact("daftas","daftar_jabatan","daftar_divisi"));
        $this->load->view("panel/frames/footer");
    }

    public function penggunaedit($id = null) {

        $this->load->model("Lokasi_model","lokasi");

        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10)
            redirect(base_url("panel/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->edit();
            if($edt != false) {
                redirect(base_url("panel/penggunaedit/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url("panel/penggunaedit/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url("panel/pengguna/"));
        $judul = "Edit Pengguna";
        $menu = $this->set_menu("pengguna");
        $datibo = $this->lokasi->usribo();
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panel/frames/header",compact("judul","menu"));
            $this->load->view("panel/penggunaedit",compact("pengguna","datibo","daftar_jabatan","daftar_divisi","daftas"));
            $this->load->view("panel/frames/footer");
        } else redirect(base_url("panel/pengguna/"));
    }

    public function penggunablokir($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10)
            redirect(base_url("panel/"));
        if($id == null) redirect(base_url("panel/pengguna"));
        $this->pengguna->blokir($id);
        redirect(base_url("panel/pengguna/?succ=1"));
    }

    public function penggunabuka($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10)
            redirect(base_url("panel/"));
        if($id == null) redirect(base_url("panel/pengguna"));
        $this->pengguna->buka($id);
        redirect(base_url("panel/pengguna/?succ=2"));
    }

    public function divisi() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10) 
            redirect(base_url("panel/"));


        $judul = "Kelola Divisi";
        $menu = $this->set_menu("divisi");

        //if($this->session->userdata("id_divisi") == 37){
        $daftar_divisi = $this->divisi->divisi_semua();
        //}else{
        //$daftar_divisi = $this->divisi->divisi_sesama($this->session->userdata("id_divisi"));    
        //}

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/divisi",compact("daftar_divisi"));
        $this->load->view("panel/frames/footer");
    }
    public function tambdiv() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10)
            redirect(base_url("panel/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
            $tbh = $this->divisi->divtamb();
            if($tbh != false) {
                redirect(base_url("panel/divisi/?succ"));
            } else {
                redirect(base_url("panel/divisi/?err"));
            }
            exit();
        }

   //     $judul = "Tambah Kode Surat";
   //     $menu = $this->set_menu("kode_surat");
    }

    public function divisiedit($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10)
            redirect(base_url("panel/"));

        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $edt = $this->divisi->divedit();
            if($edt != false) {
                redirect(base_url("panel/divisiedit/".$post["id_dinas"]."?succ"));
            } else {
                redirect(base_url("panel/divisiedit/".$post["id_dinas"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url("panel/divisi"));

        $judul = "Edit Divisi";
        $menu = $this->set_menu("divisi");
        $divisi = $this->divisi->ambil_berdasarkan_id($id);

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/divisiedit",compact("divisi"));
        $this->load->view("panel/frames/footer");
    }


    public function jabatan() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10)
            redirect(base_url("panel/"));
        $judul = "Kelola Jabatan";
        $menu = $this->set_menu("jabatan");
        $daftar_jabatan = $this->jabatan->ambil_semua();

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/jabatan",compact("daftar_jabatan"));
        $this->load->view("panel/frames/footer");
    }

    public function tambahjabatan() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 10)
            redirect(base_url("panel/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $tbh = $this->jabatan->tambah();
            if($tbh != false) {
                redirect(base_url("panel/tambahjabatan/?succ"));
            } else {
                redirect(base_url("panel/tambahjabatan/?err"));
            }
            exit();
        }

        $judul = "Tambah Jabatan";
        $menu = $this->set_menu("jabatan");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/tambahjabatan");
        $this->load->view("panel/frames/footer");
    }

    public function jabatanedit($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11  && $this->session->userdata("id_jabatan") != 10)
            redirect(base_url("panel/"));
        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $edt = $this->jabatan->edit();
            if($edt != false) {
                redirect(base_url("panel/jabatanedit/".$post["id_jabatan"]."?succ"));
            } else {
                redirect(base_url("panel/jabatanedit/".$post["id_jabatan"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url("panel/jabatan"));

        $judul = "Edit Jabatan";
        $menu = $this->set_menu("jabatan");
        $jabatan = $this->jabatan->ambil_berdasarkan_id($id);

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/jabatanedit",compact("jabatan"));
        $this->load->view("panel/frames/footer");
    }


 
    public function baca_disposisi_keluar($id_disposisi,$kode_disposisi) {
        if($this->session->userdata("disposisi") == 0)
            redirect(base_url("panel/"));

        $post = $this->input->post();

        $disposisi = $this->disposisi->ambil_satu_disposisi($id_disposisi,$kode_disposisi);
        if($disposisi == null) redirect(base_url("panel/disposisi_keluar"));

        if(isset($post["btnSubmit"])) {
            unset($post["btnSubmit"]);
            if($_FILES["lampiran_follow_up"]["name"][0] != "") {
                $gambar = $this->upload_files($_FILES["lampiran_follow_up"],"assets/uploads/lampiran/follow_up_disposisi/");
                if($gambar == false) {
                    redirect(base_url("panel/baca_disposisi_masuk/".$id_disposisi."/".$kode_disposisi."?err"));
                    exit();
                }
                $post["lampiran_follow_up"] = json_encode($gambar);
            }
            $post["id_pengguna"] = $this->session->userdata("id_pengguna");
            $post["id_disposisi"] = $id_disposisi;
            $in = $this->disposisi->follow_up($post);
            if($in)
                redirect(base_url("panel/baca_disposisi_keluar/".$id_disposisi."/".$kode_disposisi."?succ"));
            else
                redirect(base_url("panel/baca_disposisi_keluar/".$id_disposisi."/".$kode_disposisi."?err"));
            exit();
        }

        if(isset($post["selesaiBtnSubmit"])) {
            if(md5($post["password"]) == $this->session->userdata("password") && $disposisi->penerima[0]->dari_user == $this->session->userdata("id_pengguna")) {
                $this->disposisi->selesai($id_disposisi);
                redirect(base_url("panel/baca_disposisi_keluar/".$id_disposisi."/".$kode_disposisi."?succ"));
            } else {
                redirect(base_url("panel/baca_disposisi_keluar/".$id_disposisi."/".$kode_disposisi."?err"));
            }
        }


        $follow_up = $this->disposisi->ambil_follow_up($id_disposisi);

        $judul = "Baca Disposisi";
        $menu = $this->set_menu("disposisi_keluar");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/baca_disposisi_keluar",compact("disposisi","follow_up"));
        $this->load->view("panel/frames/footer");
    }

    public function disposisi_masuk() {
        $judul = "Disposisi Masuk";
        $menu = $this->set_menu("disposisi_masuk");

        $disposisi = $this->disposisi->ambil_disposisi_masuk();

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/disposisi_masuk",compact("disposisi"));
        $this->load->view("panel/frames/footer");
    }

    public function baca_disposisi_masuk($id_disposisi,$kode_disposisi) {
        $post = $this->input->post();

        $disposisi = $this->disposisi->ambil_satu_disposisi_masuk($id_disposisi,$kode_disposisi);
        if($disposisi == null) redirect(base_url("panel/disposisi_masuk"));

        $this->disposisi->baca_disposisi($id_disposisi,$kode_disposisi);

        if(isset($post["btnSubmit"])) {
            if($disposisi->selesai == 1) redirect(base_url("panel/baca_disposisi_masuk/".$id_disposisi."/".$kode_disposisi));
            unset($post["btnSubmit"]);
            if($_FILES["lampiran_follow_up"]["name"][0] != "") {
                $gambar = $this->upload_files($_FILES["lampiran_follow_up"],"assets/uploads/lampiran/follow_up_disposisi/");
                if($gambar == false) {
                    redirect(base_url("panel/baca_disposisi_masuk/".$id_disposisi."/".$kode_disposisi."?err"));
                    exit();
                }
                $post["lampiran_follow_up"] = json_encode($gambar);
            }
            $post["id_pengguna"] = $this->session->userdata("id_pengguna");
            $post["id_disposisi"] = $id_disposisi;
            $in = $this->disposisi->follow_up($post);
            if($in)
                redirect(base_url("panel/baca_disposisi_masuk/".$id_disposisi."/".$kode_disposisi."?succ"));
            else
                redirect(base_url("panel/baca_disposisi_masuk/".$id_disposisi."/".$kode_disposisi."?err"));
            exit();
        }

        $judul = "Baca Disposisi";
        $menu = $this->set_menu("disposisi_masuk");

        $follow_up = $this->disposisi->ambil_follow_up($id_disposisi);


        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/baca_disposisi_masuk",compact("disposisi","follow_up"));
        $this->load->view("panel/frames/footer");
    }


    public function cetak_dpsisi_masuk($id_disposisi,$kode_disposisi) {
        $post = $this->input->post();

        $disposisi = $this->disposisi->ambil_satu_disposisi_masuk($id_disposisi,$kode_disposisi);
        if($disposisi == null) redirect(base_url("panel/cetak_dpsisi_masuk"));

        $this->disposisi->baca_disposisi($id_disposisi,$kode_disposisi);

        if(isset($post["btnSubmit"])) {
            if($disposisi->selesai == 1) redirect(base_url("panel/cetak_dpsisi_masuk/".$id_disposisi."/".$kode_disposisi));
            unset($post["btnSubmit"]);
            if($_FILES["lampiran_follow_up"]["name"][0] != "") {
                $gambar = $this->upload_files($_FILES["lampiran_follow_up"],"assets/uploads/lampiran/follow_up_disposisi/");
                if($gambar == false) {
                    redirect(base_url("panel/cetak_dpsisi_masuk/".$id_disposisi."/".$kode_disposisi."?err"));
                    exit();
                }
                $post["lampiran_follow_up"] = json_encode($gambar);
            }
            $post["id_pengguna"] = $this->session->userdata("id_pengguna");
            $post["id_disposisi"] = $id_disposisi;
            $in = $this->disposisi->follow_up($post);
            if($in)
                redirect(base_url("panel/cetak_dpsisi_masuk/".$id_disposisi."/".$kode_disposisi."?succ"));
            else
                redirect(base_url("panel/cetak_dpsisi_masuk/".$id_disposisi."/".$kode_disposisi."?err"));
            exit();
        }

        $judul = "Cetak Disposisi";
        $menu = $this->set_menu("disposisi_masuk");
        $daftar_dinas = $this->dinas->ambil_sedinas($this->session->userdata("id_dinas")); 
        $follow_up = $this->disposisi->ambil_follow_up($id_disposisi);


        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/cetak_dpsisi_masuk",compact("disposisi","follow_up","daftar_dinas"));
        $this->load->view("panel/frames/footer");
    }

    public function baca_notif() {
        $get = $this->input->get();
        if(isset($get["rel_link"]) && isset($get["id"])) {
            $this->pemberitahuan->baca($get["id"]);
            redirect(base_url("panel/".$get["rel_link"]));
        } else {
            redirect(base_url("panel/"));
        }
    }

    public function logout() {
        $akt=$this->pengguna->tamakt('logout berhasil',$this->session->userdata('username'));
        $this->session->sess_destroy();
        redirect(base_url("login/"));
    }


    private function set_menu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",
            "menu_eng"=>"",
            "harga_baru"=>"",
            "salesrevenue"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "talian"=>"",
            "menufoc"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>"",
            "menuotb"=>"",
            "lokaotb"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "aktifitas"=>"",
            "surat_terkirim"=>"",
            "disposisi_keluar"=>"",
            "disposisi_masuk"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "inventory"=>"",
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "meena"=>"",
            "metuj"=>"",
            "medel"=>"",
            "mascidsid"=>""
            
        ];
        $menu[$active] = "active";
        return $menu;
    }



    // Latihan

    public function latihan() {

        curl_post("https://eoffice.jlm.net.id/notif_pesan_baru",array("id_pesan"=>138));

    }

    public function menuit() {

        $judul = "DIVISI TEKNOLOGI INFORMASI";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/itunem/menuit");
        $this->load->view("panel/frames/footer");
    }

    public function menueng() {

        $judul = "DEPARTEMEN ENGINEERING";
        $menu = $this->set_menu("menu_eng");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/itunem/menueng");
        $this->load->view("panel/frames/footer");
    }

    function import()
    {
                $this->load->library('excel');
        if(isset($_FILES["file"]["name"]))
        {
            $path = $_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $rid = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $cti = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $sub = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $rte = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $psi = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $rca = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $dti = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $uti = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
                    $tel = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $res = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $rty = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $tec = $worksheet->getCellByColumnAndRow(11, $row)->getValue();

                    $data[] = array(
                        'Request_ID'      =>  $rid,
                        'Created_Time'    =>  $cti,
                        'Subject'         =>  $sub,
                        'Requester'       =>  $rte,
                        'Problem_Side'    =>  $psi,
                        'Root_Cause'      =>  $rca,
                        'Down_Time'       =>  $dti,
                        'Uptime'          =>  $uti,
                        'Time_Elapsed'    =>  $tel,
                        'Resolution'      =>  $res,
                        'Request_Type'    =>  $rty,
                        'Technician'      =>  $tec
                    );
                }
            }
            $this->excel_import_model->insert($data);
            $jum=$highestRow-1;
            echo $jum.' data berhasil di import';
        }   
    }

    public function excel() {


      /*  if ($this->input->post('btnSubmit')) {
            $file = explode('.', $_FILES['import']['name']);
            $length = count($file);
            if ($file[$length - 1] == 'xlsx' || $file[$length - 1] == 'xls') {
                $tmp = $_FILES['import']['tmp_name'];
                $this->load->library('excel');
                $read = PHPExcel_IOFactory::createReaderForFile($tmp);
                $read->setReadDataOnly(true);
                $excel = $read->load($tmp);
                $sheets = $read->listWorksheetNames($tmp);
                foreach ($sheets as $sheet) {
                    if ($this->db->table_exists($sheet)) {
                        $_sheet = $excel->setActiveSheetIndexByName($sheet);
                        $maxRow = $_sheet->getHighestRow();
                        $maxCol = $_sheet->getHighestColumn();
                        $field = array();
                        $sql = array();
                        $maxCol = range('A', $maxCol);
                        foreach ($maxCol as $key => $coloumn) {
                            $field[$key] = $_sheet->getCell($coloumn . '1')->getCalculatedValue();
                        }
                        for ($i = 2; $i <= $maxRow; $i++) {
                            foreach ($maxCol as $k => $coloumn) {
                                $sql[$field[$k]] = $_sheet->getCell($coloumn . $i)->getCalculatedValue();
                            }
                            $this->db->insert($sheet, $sql);
                        }
                    }
                }
            } else {
                $this->session->set_flashdata('pesan', 'Format Data Harus File Excel');
                //redirect(base_url());
            }
        }
        $this->session->set_flashdata('pesan', 'Import Data Berhasil');
        //redirect(base_url()); */



        $judul = "Menu TI - Import Data Tiket";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/itunem/import");
        $this->load->view("panel/frames/footer");
    }

    public function disxbaru() {
        //if($this->session->userdata("disposisi") == 0)
        //    redirect(base_url("panel/"));

        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_files($_FILES["attach"]);
                if($gambar == false) {
                    redirect(base_url("panel/disxbaru/?err"));
                    exit();
                }
                $post["lampiran"] = json_encode($gambar);
            }
            $kirim = $this->diskusi->kirim($post);
           if($kirim)
                redirect(base_url("panel/disxbaru/?succ"));
            else
                redirect(base_url("panel/disxbaru/?err"));
            exit();
        }

        $judul = " Diskusi Baru";       
        $menu = $this->set_menu("chat_baru");

        $daftar_pengguna=$this->pengguna->ambil_per_grup();
        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/disxbaru",compact("daftar_pengguna","menu"));
        $this->load->view("panel/frames/footer");
    }

        public function disxkirm() {
        //if($this->session->userdata("disposisi") == 0)
        //    redirect(base_url("panel/"));
        $judul = "Diskusi Kirim";
        $menu = $this->set_menu("disx_kirm");

        $diskusix = $this->diskusi->disxkirm();

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/disxkirm",compact("diskusix"));
        $this->load->view("panel/frames/footer");
    }

        public function disxtrma() {
        $judul = "Diskusi Masuk";
        $menu = $this->set_menu("disx_trma");

        $diskusix = $this->diskusi->disxtrma();

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/disxtrma",compact("diskusix"));
        $this->load->view("panel/frames/footer");
    }

        public function disxcarim($id_disx,$ko_disx) {
        //if($this->session->userdata("disposisi") == 0)
        //    redirect(base_url("panel/"));

        $post = $this->input->post();

        $disposisi = $this->diskusi->disxsatuid($id_disx,$ko_disx);
        if($disposisi == null) redirect(base_url("panel/disxkirm"));

        if(isset($post["btnSubmit"])) {
            unset($post["btnSubmit"]);
            if($_FILES["lampiran_follow_up"]["name"][0] != "") {
                $gambar = $this->upload_files($_FILES["lampiran_follow_up"],"assets/uploads/lampiran/follow_up_disposisi/");
                if($gambar == false) {
                    redirect(base_url("panel/disxcarim/".$id_disx."/".$ko_disx."?err"));
                    exit();
                }
                $post["lampiran_follow_up"] = json_encode($gambar);
            }
            $post["id_pengguna"] = $this->session->userdata("id_pengguna");
            $post["id_disx"] = $id_disx;
            $in = $this->diskusi->follow_up($post);
            if($in)
                redirect(base_url("panel/disxcarim/".$id_disx."/".$ko_disx."?succ"));
            else
                redirect(base_url("panel/disxcarim/".$id_disx."/".$ko_disx."?err"));
            exit();
        }

        if(isset($post["selesaiBtnSubmit"])) {
            if(md5($post["password"]) == $this->session->userdata("password") && $disposisi->penerima[0]->dari_user == $this->session->userdata("id_pengguna")) {
                $this->diskusi->selesai($id_disx);
                redirect(base_url("panel/disxcarim/".$id_disx."/".$ko_disx."?succ"));
            } else {
                redirect(base_url("panel/disxcarim/".$id_disx."/".$ko_disx."?err"));
            }
        }

        $disc=$id_disx;
        $follow_up = $this->diskusi->ambil_follow_up($disc);
        
        $judul = "Baca Diskusi";
        $menu = $this->set_menu("disx_kirm");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/disxcarim",compact("disposisi","follow_up","disc")); //
        $this->load->view("panel/frames/footer");
    }


        public function disxcasuk($id_disx,$ko_disx) {
        //if($this->session->userdata("disposisi") == 0)
        //    redirect(base_url("panel/"));

        $post = $this->input->post();

        $disposisi = $this->diskusi->disxsatuid($id_disx,$ko_disx);
        if($disposisi == null) redirect(base_url("panel/disxtrma"));
        $this->diskusi->konfirmbaca($id_disx,$ko_disx);
        if(isset($post["btnSubmit"])) {
            unset($post["btnSubmit"]);
            if($_FILES["lampiran_follow_up"]["name"][0] != "") {
                $gambar = $this->upload_files($_FILES["lampiran_follow_up"],"assets/uploads/lampiran/follow_up_disposisi/");
                if($gambar == false) {
                    redirect(base_url("panel/disxcasuk/".$id_disx."/".$ko_disx."?err"));
                    exit();
                }
                $post["lampiran_follow_up"] = json_encode($gambar);
            }
            $post["id_pengguna"] = $this->session->userdata("id_pengguna");
            $post["id_disx"] = $id_disx;
            $in = $this->diskusi->follow_up($post);
            if($in)
                redirect(base_url("panel/disxcasuk/".$id_disx."/".$ko_disx."?succ"));
            else
                redirect(base_url("panel/disxcasuk/".$id_disx."/".$ko_disx."?err"));
            exit();
        }

        if(isset($post["selesaiBtnSubmit"])) {
            if(md5($post["password"]) == $this->session->userdata("password") && $disposisi->penerima[0]->dari_user == $this->session->userdata("id_pengguna")) {
                $this->diskusi->selesai($id_disx);
                redirect(base_url("panel/disxcasuk/".$id_disx."/".$ko_disx."?succ"));
            } else {
                redirect(base_url("panel/disxcasuk/".$id_disx."/".$ko_disx."?err"));
            }
        }


        $follow_up = $this->diskusi->ambil_follow_up($id_disx);

        $judul = "Baca Diskusi";
        $menu = $this->set_menu("disx_trma");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/disxcasuk",compact("disposisi","follow_up")); //
        $this->load->view("panel/frames/footer");
    }

    public function hargapus($id=null) {
            $h = $this->harga->haprha($id);
            if($h != false) {
                redirect(base_url("panel/?hsuc"));
            } else {
                redirect(base_url("panel/?herr"));
            }
            exit();   
    }

	public function hargabaru() {
		//if($this->session->userdata("id_jabatan") != 1)
        //    redirect(base_url("panel/"));
        
        $post = $this->input->post();

        $silog=$this->input->post('nm_custo');

        if(isset($post["btnSubmit"])) {
           /* if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_files($_FILES["attach"]);
                if($gambar == false) {
                    redirect(base_url("panel/hargabaru/?err"));
                    exit();
                }
                $post["lampiran"] = json_encode($gambar);
            }*/

            $eksetamb = $this->harga->minharga();
            if($eksetamb != false) {
                $akt=$this->pengguna->tamakt('Permintaan harga untuk '.$silog,$this->session->userdata('username'));
                redirect(base_url("panel/hargabaru/?succ"));
            } else {
                redirect(base_url("panel/hargabaru/?err"));
            }
            exit();
        }

        $judul = "Permintaan Harga";       
        $menu = $this->set_menu("harga_baru");
        //$daftar_pengguna = $this->pengguna->semua_psales();
        $daftar_jabatan = $this->jabatan->jpresales(); // Jabatan Presales saja
        $daftar_segment = $this->harga->segment_semua(0);
        $daftar_media = $this->harga->media_semua();
        $daftar_request = $this->harga->request_semua();
        $daftar_service = $this->harga->service_semua();
        $datdiv = $this->divisi->pildiv(19); // Manage Service Dept.

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/hargabaru",compact("datdiv","daftar_jabatan","daftar_segment","daftar_media","daftar_request","daftar_service"));
        $this->load->view("panel/frames/footer");
    }

    public function hargadit($id) {
        
        $post = $this->input->post();

        $silog=$this->input->post('nm_custo');

        if(isset($post["btnSubmit"])) {
           /* if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_files($_FILES["attach"]);
                if($gambar == false) {
                    redirect(base_url("panel/hargabaru/?err"));
                    exit();
                }
                $post["lampiran"] = json_encode($gambar);
            }*/

            $ekseubah = $this->harga->ubaharga($id);
            if($ekseubah != false) {
                $akt=$this->pengguna->tamakt('Ubah permintaan harga untuk '.$silog,$this->session->userdata('username'));
                redirect(base_url("panel/?usuc"));
            } else {
                redirect(base_url("panel/hargadit/?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url("panel/"));
        $judul = "Edit Permintaan Harga";
        $menu = $this->set_menu("dashboard");
        $dafrid=$this->harga->harga_id($id);
        $daftar_segment = $this->harga->segment_semua(0);
        $daftar_media = $this->harga->media_semua();
        $daftar_request = $this->harga->request_semua();
        $daftar_service = $this->harga->service_semua();
        $daftar_jabatan = $this->jabatan->jpresales(); // Jabatan Presales saja

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/hargaubah",compact("dafrid","daftar_jabatan","daftar_segment","daftar_media","daftar_request","daftar_service"));
        $this->load->view("panel/frames/footer");
    }



    public function hargatamb($req=null,$id = null) {

        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 6)
            redirect(base_url("panel/"));
        $post = $this->input->post();

        $silog=$this->input->post('id_rha').'_'.$this->input->post('nm_custo');

        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->harga->isiharga();
            if($edt != false) {
                $akt=$this->pengguna->tamakt('Beri harga untuk '.$silog,$this->session->userdata('username'));
                redirect(base_url("panel/?succ"));
            } else {
                redirect(base_url("panel/?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url("panel/"));
        $judul = "Tambah Harga";
        $menu = $this->set_menu("dashboard");
        $dafrid=$this->harga->harga_id($id);
        $dafibp=$this->harga->pilitembp($id);
        $dafmua=$this->harga->semua();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

                
            $this->load->view("panel/frames/header",compact("judul","menu"));
            if($req==1){
            $this->load->view("panel/bpharga/hargatlink",compact("dafibp","id","req","dafrid","daftar_jabatan","daftar_divisi"));    
            }
            else{
                $this->load->view("panel/bpharga/hargatbara",compact("dafibp","id","req","dafrid","daftar_jabatan","daftar_divisi"));    
            }
            
            
            $this->load->view("panel/frames/footer");
        
    }
    public function hargabpb($id=null) {

        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 6 )
            redirect(base_url("panel/"));
        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {

        $post["tgbua"] = date("Y-m-d H:i:s");
        $post["usbua"] = $this->session->userdata("username");
        $post["idrha"] = $id;
        $post["toven"] = $this->input->post('qty')*$this->input->post('hgven');
        $post["tojlm"] = $this->input->post('qty')*$this->input->post('hgjlm');

           $simp = $this->harga->isiitembp($post);
            if($simp != false) {
                redirect(base_url("panel/hargabpb/".$id."/?succ"));
            } else {
                redirect(base_url("panel/hargabpb/".$id."/?err"));
            }
            exit();
        }


        if($id == null) redirect(base_url("panel/"));

        $judul = "BP BRG";
        $menu = $this->set_menu("dashboard");
        $dafrid=$this->harga->harga_id($id);
        $dafibp=$this->harga->pilitembp($id);
        $bptot=$this->harga->jmlbp($id);

        if($this->harga->cekbp($id)>0){
            $hid=$bptot->idrha;
            $hjl=$bptot->totjlm;
            $hve=$bptot->totven;
            $isihgi = $this->db->where("id_rha",$id)->set('hg_instalasi',$hjl)->update("ts_agrah");  
        }else{
            $hid=$id;
            $hjl=0;
            $hve=0;            
        }

        $dafmua=$this->harga->semua();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();
                
            $this->load->view("panel/frames/header",compact("judul","menu"));
            
            $this->load->view("panel/bpharga/hargabpb",compact("hid","hjl","hve","bptot","dafibp","id","dafrid","daftar_jabatan","daftar_divisi"));    
                      
            $this->load->view("panel/frames/footer");
       

    }

    public function vbpb($id=null) {
        $judul = "BP BRG";
        $menu = $this->set_menu("dashboard");
        $dafrid=$this->harga->harga_id($id);
        $dafibp=$this->harga->pilitembp($id);
        $bptot=$this->harga->jmlbp($id);

        if($this->harga->cekbp($id)>0){
            $hid=$bptot->idrha;
            $hjl=$bptot->totjlm;
            $hve=$bptot->totven;
            $isihgi = $this->db->where("id_rha",$id)->set('hg_instalasi',$hjl)->update("ts_agrah");  
        }else{
            $hid=$id;
            $hjl=0;
            $hve=0;            
        }

        $dafmua=$this->harga->semua();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();
                
            $this->load->view("panel/frames/header",compact("judul","menu"));
            
            $this->load->view("panel/bpharga/hargabpb",compact("hid","hjl","hve","bptot","dafibp","id","dafrid","daftar_jabatan","daftar_divisi"));    
                      
            $this->load->view("panel/frames/footer");
    }

    public function appvnmc($id=null){
    
            $post = $this->input->post();

        if(isset($post["btnSimpanSGM"])) {

            $simpan = $this->nmcfee->appvsales($id,$post);
            if($simpan)
                redirect(base_url("panel/?bsucc"));
            else
                redirect(base_url("panel/?berr"));
                  
        }

        
        if(isset($post["btnSimpanBDM"])) {

            $simpan = $this->nmcfee->appvbudev($id,$post);
            if($simpan)
                redirect(base_url("panel/?bsucc"));
            else
                redirect(base_url("panel/?berr"));
                  
        }

        if(isset($post["btnSimpanDIR"])) {

            $simpan = $this->nmcfee->appvdirut($id,$post);
            if($simpan)
                redirect(base_url("panel/?bsucc"));
            else
                redirect(base_url("panel/?berr"));
                  
        }
    }

    public function takquo($id=null){
        


        $judul = "Cetak Quotation";
        $menu = $this->set_menu("dashboard");
        
        $dafrid=$this->harga->harga_id($id);
        $dafibp=$this->harga->pilitembp($id);
        $bptot=$this->harga->jmlbp($id);

        if($this->harga->cekbp($id)>0){
            $hid=$bptot->idrha;
            $hjl=$bptot->totjlm;
            $hve=$bptot->totven;
            $isihgi = $this->db->where("id_rha",$id)->set('hg_instalasi',$hjl)->update("ts_agrah");  
        }else{
            $hid=$id;
            $hjl=0;
            $hve=0;            
        }

        $dafmua=$this->harga->semua();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();
        
        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/bpharga/cetakquo",compact("hid","hjl","hve","bptot","dafibp","id","dafrid","daftar_jabatan","daftar_divisi"));
        $this->load->view("panel/frames/footer");
    }
    public function hargabpl($id=null) {

        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 6)
            redirect(base_url("panel/"));
        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {

        $post["tgbua"] = date("Y-m-d H:i:s");
        $post["usbua"] = $this->session->userdata("username");
        $post["idrha"] = $id;
        $post["toven"] = $this->input->post('qty')*$this->input->post('hgven');
        $post["tojlm"] = $this->input->post('qty')*$this->input->post('hgjlm');

           $simp = $this->harga->isiitembp($post);
            if($simp != false) {
                redirect(base_url("panel/hargabpl/".$id."/?succ"));
            } else {
                redirect(base_url("panel/hargabpl/".$id."/?err"));
            }
            exit();
        }


        if($id == null) redirect(base_url("panel/"));

        $judul = "BP LINK";
        $menu = $this->set_menu("dashboard");
        $dafrid=$this->harga->harga_id($id);
        $dafibp=$this->harga->pilitembp($id);
        $bptot=$this->harga->jmlbp($id);
        $dafrve=$this->harga->pilbprve($id);
        
        if($this->harga->cekbp($id)>0){
            $hid=$bptot->idrha;
            $hjl=$bptot->totjlm;
            $hve=$bptot->totven;
            $isihgi = $this->db->where("id_rha",$id)->set('hg_instalasi',$hjl)->update("ts_agrah");  
        }else{
            $hid=$id;
            $hjl=0;
            $hve=0;            
        }


        $dafmua=$this->harga->semua();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();
                
            $this->load->view("panel/frames/header",compact("judul","menu"));
            
            $this->load->view("panel/bpharga/hargabpl",compact("dafrve","hid","hjl","hve","bptot","dafibp","id","dafrid","daftar_jabatan","daftar_divisi"));    
                      
            $this->load->view("panel/frames/footer");
       

    }

    public function vbpl($id=null) {
        $judul = "BP LINK";
        $menu = $this->set_menu("dashboard");
        $dafrid=$this->harga->harga_id($id);
        $dafibp=$this->harga->pilitembp($id);
        $bptot=$this->harga->jmlbp($id);
        $nilrve=$this->harga->cekrve($id);
        $dafrve=$this->harga->pilbprve($id);
        
        if($this->harga->cekbp($id)>0){
            $hid=$bptot->idrha;
            $hjl=$bptot->totjlm;
            $hve=$bptot->totven;
            $isihgi = $this->db->where("id_rha",$id)->set('hg_instalasi',$hjl)->update("ts_agrah");  
        }else{
            $hid=$id;
            $hjl=0;
            $hve=0;            
        }


        $dafmua=$this->harga->semua();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();
                
            $this->load->view("panel/frames/header",compact("judul","menu"));
            
            $this->load->view("panel/bpharga/hargabpl",compact("nilrve","dafrve","hid","hjl","hve","bptot","dafibp","id","dafrid","daftar_jabatan","daftar_divisi"));    
                      
            $this->load->view("panel/frames/footer");
       

    }

    public function revenue($id=null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 6)
            redirect(base_url("panel/"));
        $post = $this->input->post();
    if(isset($post["btnSimpan"])) {

        $post["tgbua"] = date("Y-m-d H:i:s");
        $post["usbua"] = $this->session->userdata("username");
        $post["idrha"] = $id;


           $simp = $this->harga->isirevenue($post);
            if($simp != false) {
                $akt=$this->pengguna->tamakt('isi revenue '.$id,$this->session->userdata('username'));
                redirect(base_url("panel/hargabpl/".$id."/?succ"));
            } else {
                redirect(base_url("panel/hargabpl/".$id."/?err"));
            }
            exit();
        }       

    }
    public function isimser() {
        $p = $this->input->post();
        $this->db->where("id_rha",$p["id_rha"]);
        $isi = $this->db->update("ts_agrah",$p);
        redirect(base_url("panel/hargabpb/".$p["id_rha"]));
    }
    public function hargadetil($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") > 9 && $this->session->userdata("id_jabatan") < 29 && $this->session->userdata("id_jabatan") > 30)
            redirect(base_url("panel/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->harga->isiharga();
            if($edt != false) {
                redirect(base_url("panel/?succ"));
            } else {
                redirect(base_url("panel/?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url("panel/"));
        $judul = "Detil Harga";
        $menu = $this->set_menu("dashboard");
        $dafrid=$this->harga->harga_bid($id);
        $dafmua=$this->harga->semua();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

                
            $this->load->view("panel/frames/header",compact("judul","menu"));
            $this->load->view("panel/hargadetil",compact("dafrid","daftar_jabatan","daftar_divisi"));
            $this->load->view("panel/frames/footer");
        
    }

   

    public function odcbaru() {

        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $tbh = $this->datait->odctamb();
            if($tbh != false) {
                redirect(base_url("panel/odcbaru/?succ"));
            } else {
                redirect(base_url("panel/odcbaru/?err"));
            }
            exit();
        }


        $datait_odc = $this->datait->vodcsemua();
        
        $judul = "Data ODC";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/odcbaru",compact("datait_odc"));
        $this->load->view("panel/frames/footer");
    }

        public function odpbaru() {

        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $tbh = $this->datait->odptamb();
            if($tbh != false) {
                redirect(base_url("panel/odpbaru/?succ"));
            } else {
                redirect(base_url("panel/odpbaru/?err"));
            }
            exit();
        }


        $datait_odc = $this->datait->vodpsemua();
        
        $judul = "Data ODP";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/odpbaru",compact("datait_odc"));
        $this->load->view("panel/frames/footer");
    }

    public function netflix($usernm=null,$nama=null) {
        if(isset($usernm)){
        $d_nftam = $this->datait->nflixtamba($usernm,$nama);    
        }
        
        $d_nflix = $this->datait->nflixsemua();
        
        $judul = "Data Netflix";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/netflix",compact("d_nflix"));
        $this->load->view("panel/frames/footer");
    }

    public function mtainmon() {
        
        $d_mtain = $this->datait->mtainsemua();
        
        $judul = "Data Maintenance";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/mtainmon",compact("d_mtain"));
        $this->load->view("panel/frames/footer");
    }

    public function rhrg($pilih=null) {
        
        
        $pil=$pilih;
        $d_rhrg = $this->datait->rhrgpilih($pil);
        $judul = "Data Req. Harga";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/rhrg",compact("d_rhrg","pil"));
        $this->load->view("panel/frames/footer");
    }

    public function word($pilih=null) {
        
        
        $pil=$pilih;
        $d_word = $this->datait->wordpilih($pil);
        $judul = "Data Work Order";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/word",compact("d_word","pil"));
        $this->load->view("panel/frames/footer");
    }

    public function ftth($pilih=null) {
        
        
        $pil=$pilih;
        $d_ftth = $this->datait->ftthpilih($pil);
        $judul = "Data FTTH";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ftth",compact("d_ftth","pil"));
        $this->load->view("panel/frames/footer");
    }


    public function imtc($pilih=null) {
        
        
        $pil=$pilih;
        $d_imtc = $this->datait->imtcpilih($pil);
        $judul = "Data Maintenance";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/imtc",compact("d_imtc","pil"));
        $this->load->view("panel/frames/footer");
    }

    public function afoc($pilih=null) {
        
        
        $pil=$pilih;
        $d_afoc = $this->datait->afocpilih($pil);
        $judul = "Data Aktifitas FOC";
        $menu = $this->set_menu("menu_it");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/afoc",compact("d_afoc","pil"));
        $this->load->view("panel/frames/footer");
    }


    public function petalokasi($id) {
        $datait_byid = $this->datait->odcbyid($id);
        $judul = "Lokasi ODC";
        $menu = $this->set_menu("Data ODC");

        
        $this->load->view("panel/odclokasi",compact("datait_byid"));
        
    }




    public function hargaedit($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 6 && $this->session->userdata("id_jabatan") != 35)
            redirect(base_url("panel/"));
        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
  
            $edt = $this->harga->ediharga();
            if($edt != false) {
                $akt=$this->pengguna->tamakt('ubah harga '.$id,$this->session->userdata('username'));
                redirect(base_url("panel/?esucc"));
            } else {
                redirect(base_url("panel/?eerr"));
            }
            exit();
        }

        if($id == null) redirect(base_url("panel/"));
        $judul = "Edit Harga";
        $menu = $this->set_menu("dashboard");
        //$dafrid=$this->harga->harga_id($id); "dafrid",
        $dafmua=$this->harga->semua();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

                
            $this->load->view("panel/frames/header",compact("judul","menu"));
            $this->load->view("panel/hargatamb",compact("daftar_jabatan","daftar_divisi"));
            $this->load->view("panel/frames/footer");
        
    }



    public function purcbaru() {
        //if($this->session->userdata("id_jabatan") != 1)
        //    redirect(base_url("panel/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
           /* if($_FILES["attach"]["name"][0] != "") {
                $gambar = $this->upload_files($_FILES["attach"]);
                if($gambar == false) {
                    redirect(base_url("panel/hargabaru/?err"));
                    exit();
                }
                $post["lampiran"] = json_encode($gambar);
            }*/
            $eksetamb = $this->datait->purctamb();
            if($eksetamb != false) {
                redirect(base_url("panel/purcbaru/?succ"));
            } else {
                redirect(base_url("panel/purcbaru/?err"));
            }
            exit();
        }

        $judul = "Permintaan Pembelian";       
        $menu = $this->set_menu("purc_baru");
        //$daftar_pengguna = $this->pengguna->semua_psales();
        $daftar_jabatan = $this->jabatan->jhrga();
        $d_purc = $this->datait->purcsemua();
        $d_prja = $this->datait->purcusrja();
        $d_prus = $this->datait->purcusrnm();
        $d_prap = $this->datait->purcusrap();
        $d_apus = $this->datait->purcapvus();
        $d_apfi = $this->datait->purcapvfi();

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/purcbaru",compact("daftar_jabatan","d_purc","d_prja","d_prus","d_prap","d_apus","d_apfi"));
        $this->load->view("panel/frames/footer");
    }

        public function purcedit($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 27)
            redirect(base_url("panel/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
  
            $simpan = $this->datait->purcharga($id);
            if($simpan != false) {
                redirect(base_url("panel/purctamb/".$this->input->post('id_rpu')."?succ"));
            } else {
                redirect(base_url("panel/purctamb/".$this->input->post('id_rpu')."?err"));
            }
            exit();
        }

        /*if($id == null) redirect(base_url("panel/"));*/
        $judul = "Edit Permintaan Pembelian";
        $menu = $this->set_menu("purc_baru");
        $d_basm = $this->datait->barghrg($id);
        $idno=$id;

            $this->load->view("panel/frames/header",compact("judul","menu"));
            $this->load->view("panel/purcedit",compact("d_basm","idno"));
            $this->load->view("panel/frames/footer");
        
    }

    public function purckirm($id=null) {

            $eksekir = $this->datait->purckirm($id);
            if($eksekir != false) {
                redirect(base_url("panel/purcbaru/?ksucc"));
            } else {
                redirect(base_url("panel/purcbaru/?kerr"));
            }
            exit();
        

        $judul = "Permintaan Pembelian";       
        $menu = $this->set_menu("purc_baru");
        //$daftar_pengguna = $this->pengguna->semua_psales();
        $daftar_jabatan = $this->jabatan->jhrga();
        $d_purc = $this->datait->purcsemua();
        $d_prus = $this->datait->purcusrnm();

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/purcbaru",compact("daftar_jabatan","d_purc","d_prus"));
        $this->load->view("panel/frames/footer");
    }
    public function purcappv($id = null) {
        $post = $this->input->post();
        $d_pusm = $this->datait->purcpilih($id);
        $d_basm = $this->datait->bargpilih($id);
        $idno=$id;



        if(isset($post["selesaiBtnSubmit"])) {
            if(md5($post["password"]) == $this->session->userdata("password") && $d_pusm->penerima == $this->session->userdata("id_pengguna")) { //
                $this->datait->selesai($idno);
                redirect(base_url("panel/purcappv/".$idno."?succ"));
            } else {
                redirect(base_url("panel/purcappv/".$idno."?err"));
            }
        }
        if(isset($post["selesai2BtnSubmit"])) {
            if(md5($post["password"]) == $this->session->userdata("password") && $post["usr"] == $this->session->userdata("username")) { //
                $this->datait->selesai2($idno);
                redirect(base_url("panel/purcappv/".$idno."?succ"));
            } else {
                redirect(base_url("panel/purcappv/".$idno."?err"));
            }
        }

        if(isset($post["selesai3BtnSubmit"])) {
            if(md5($post["password"]) == $this->session->userdata("password") && $post["usr"] == $this->session->userdata("username")) { //
                $this->datait->selesai3($idno);
                redirect(base_url("panel/purcappv/".$idno."?succ"));
            } else {
                redirect(base_url("panel/purcappv/".$idno."?err"));
            }
        }

        if(isset($post["tolakBtnSubmit"])) {
            if(md5($post["password"]) == $this->session->userdata("password")) { //&& $disposisi->penerima[0]->dari_user == $this->session->userdata("id_pengguna")
                $this->datait->tolak($idno);
                redirect(base_url("panel/purcappv/".$idno."?succ"));
            } else {
                redirect(base_url("panel/purcappv/".$idno."?err"));
            }
        }
        
        $judul = "Detil Purc. Req.";
        $menu = $this->set_menu("purc_baru");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/purcappv",compact("d_pusm","d_basm","idno"));
        $this->load->view("panel/frames/footer");
    }

    public function purcctak($id = null) {
        $post = $this->input->post();
        $d_pusm = $this->datait->purcpilih($id);
        $d_basm = $this->datait->bargpilih($id);
        $idno=$id;
            $judul = "Cetak PO";
        $menu = $this->set_menu("purc_baru");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/purcctak",compact("d_pusm","d_basm","idno"));
        $this->load->view("panel/frames/footer");
    }


        public function purctamb($id = null) {
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {

            $eksetamb = $this->datait->bargtamb();
            if($eksetamb != false) {
                if($id !=null){
                   redirect(base_url("panel/purctamb/".$id."/?succ")); 
                }else{
                   redirect(base_url("panel/purctamb/?succ")); 
                }

            } else {
                if($id !=null){
                   redirect(base_url("panel/purctamb/".$id."/?err")); 
                }else{
                   redirect(base_url("panel/purctamb/?err")); 
                }
            }
            exit();
        }

        $judul = "Isi Data Barang";       
        $menu = $this->set_menu("purc_baru");
        //$daftar_pengguna = $this->pengguna->semua_psales();
        $daftar_jabatan = $this->jabatan->jhrga();
        $d_barg = $this->datait->bargid($id);
        $idrpu=$id;


        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/purctamb",compact("daftar_jabatan","d_barg","idrpu"));
        $this->load->view("panel/frames/footer");
    }

        public function purchapu($idbrg) {


            $eksehapu= $this->datait->barghapu($idbrg);
            if($eksehapu != false) {
                redirect(base_url("panel/purctamb/".$idbrg."/?hsucc"));
            } else {
                redirect(base_url("panel/purctamb/".$idbrg."/?herr"));
            }
            exit();
        

        $judul = "Isi Data Barang";       
        $menu = $this->set_menu("purc_baru");
        //$daftar_pengguna = $this->pengguna->semua_psales();
        $daftar_jabatan = $this->jabatan->jhrga();
        $d_barg = $this->datait->bargsemua();


        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/purctamb",compact("daftar_jabatan","d_barg"));
        $this->load->view("panel/frames/footer");
    }

        public function wobaru() {

        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $inject = $this->wordata->wo_before_sap();
            if($inject != false) {
                $akt=$this->pengguna->tamakt('input no. wo sebelum SAP '.$id,$this->session->userdata('username'));
                redirect(base_url("panel/womonito/".$post["id_worde"]."?succ"));
            } else {
                redirect(base_url("panel/womonito/".$post["id_worde"]."?err"));
            }
            exit();
        }
        
        $dafsv_custo=$this->genral->semua('v_sap_service');
        $dafnm_svsta=$this->genral->semua('v_sap_service_status');
        $dafca_custo=$this->genral->semua('v_sap_capacity');
        $dafmd_custo=$this->genral->semua('v_sap_media');
        $dafif_custo=$this->genral->semua('v_sap_interface');
        $dafnm_sales=$this->genral->semua('v_sap_sales');

        $judul = "WO Baru";
        $menu = $this->set_menu("womonito");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ompunem/wobaru",compact("dafsv_custo","dafnm_svsta","dafca_custo","dafmd_custo","dafif_custo","dafnm_sales"));
        $this->load->view("panel/frames/footer");        

        }
        
        public function womonito() {
        
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));

        $pillan = $this->wordata->blnwo();
        $woblni=$this->harga->pilwo($blnini);
        $woblnl=$this->harga->pilwo($blnlal);
        $wosum=$this->harga->wosummary();
        $wosal=$this->harga->wosales();
        $woseg=$this->harga->wosegme();
        $wosta=$this->harga->wostatu();
        if($this->session->userdata('id_jabatan')==22){
        $datait_wo = $this->wordata->wo4baa();
        }else{
            
            $post = $this->input->post();

            if(isset($post["btnFilter"])) {

               if($this->input->post('filter')==''){
                $datait_wo = $this->wordata->wosemua(); 
               }else{
                $datait_wo = $this->wordata->wosemua($this->input->post('filter'));
               }
               
            }else{
               $datait_wo = $this->wordata->wosemua();
            }
        
        }
        $data_wopil = $this->wordata->wopilih();

        $judul = "Monitoring WO";
        $menu = $this->set_menu("womonito");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ompunem/womonito",compact("pillan","wosta","woseg","wosal","woblnl","woblni","wosum","datait_wo","data_wopil"));
        $this->load->view("panel/frames/footer");
    }
        public function womorevi($id = null) {
            $rev = $this->wordata->worev($id);

            if($rev != false) {
                $akt=$this->pengguna->tamakt('revisi no. wo '.$id,$this->session->userdata('username'));
                redirect(base_url("panel/womonito/?revok"));
            } else {
                redirect(base_url("panel/womonito/?revor"));
            }

        }

        public function wosofblo($id = null) {
            $sblok = $this->wordata->woblok($id);

            if($sblok != false) {
                redirect(base_url("panel/womonito/?sbok"));
            } else {
                redirect(base_url("panel/womonito/?sber"));
            }

        }


        public function womoedit($id = null) {

        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $tbh = $this->wordata->protamb();
            if($tbh != false) {
                $akt=$this->pengguna->tamakt('input progress no. wo '.$id,$this->session->userdata('username'));
                redirect(base_url("panel/womoedit/".$post["id_worde"]."?succ"));
            } else {
                redirect(base_url("panel/womoedit/".$post["id_worde"]."?err"));
            }
            exit();
        }

        $afrev = $this->wordata->dawoaf($id);
        $berev = $this->wordata->dawobe($id);
        $datsit = $this->wordata->dasite($id);
        $dattop = $this->wordata->topopil($id);
        $d_wopro = $this->wordata->prosemua($id);
        $idno=$id;

        
        $judul = "Progress WO";
        $menu = $this->set_menu("womonito");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ompunem/womoedit",compact("afrev","berev","datsit","dattop","d_wopro","idno"));
        $this->load->view("panel/frames/footer");
    }


        public function womoalok($id = null) {

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $asignto = $this->datait->wodispatch();
            if($asignto != false) {
                redirect(base_url("panel/womoalok/".$post["id_worde"]."?succ"));
            } else {
                redirect(base_url("panel/womoalok/".$post["id_worde"]."?err"));
            }
            exit();
        }


        $daftar_saladm = $this->pengguna->semua_saladm();
        
        $d_wopro = $this->wordata->prosemua($id);
        $idno=$id;
        
        $judul = "WO Assign to";
        $menu = $this->set_menu("womonito");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/womoalok",compact("d_wopro","idno","daftar_saladm"));
        $this->load->view("panel/frames/footer");
    }

    public function topotamb($id = null) {


        $post = $this->input->post();
        


        if(isset($post["btnSubmit"])) {
            $tbh = $this->wordata->toptamb();
            if($this->input->post('id_cir')=='89'){
                $upload_image = $_FILES['image']['name'][0];

                if ($upload_image) {
                    $config['upload_path'] = 'assets/uploads/topo/';
                    $config['allowed_types'] = 'gif|jpg|png|pdf';
                    $config['max_size']     = '5000';

                    $this->load->library('upload', $config);

                    if ($this->upload->do_upload('image')) {
                        $new_image = $this->upload->data('file_name');
                        $this->db->where("wono",$id)->set('filetopo', $new_image)->update("tp_owatad_assto");
                        redirect(base_url("panel/topotamb/".$post["id_worde"]."?succ"));
                    } else {
                        echo $this->upload->display_errors();
                        redirect(base_url("panel/topotamb/".$post["id_worde"]."?err"));
                    }
                }
            }

            if($tbh != false) {
                $akt=$this->pengguna->tamakt('unggah topology '.$id,$this->session->userdata('username'));
                redirect(base_url("panel/topotamb/".$post["id_worde"]."?succ"));
            } else {
                redirect(base_url("panel/topotamb/".$post["id_worde"]."?err"));
            }
            exit();
        }      

        $afrev = $this->wordata->dawoaf($id);
        $berev = $this->wordata->dawobe($id);
        $datdes = $this->wordata->desper();
        $datgam = $this->wordata->dafper();
        $dattop = $this->wordata->topopil($id);
        $datlod = $this->wordata->asstopi($id);
        $d_wopro = $this->wordata->prosemua($id);
        $idno=$id;
        
        $judul = "Topology WO";
        $menu = $this->set_menu("womonito");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ompunem/topoedit",compact("afrev","berev","datlod","dattop","datdes","datgam","d_wopro","idno"));
        $this->load->view("panel/frames/footer");
    }


  public function baatamb($id = null) {


        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
             
            $baa = $this->wordata->uplobaa($id);
            if($baa != false) {
                $akt=$this->pengguna->tamakt('unggah baa no. wo '.$id,$this->session->userdata('username'));    
                redirect(base_url("panel/baatamb/".$id."?succ"));
            } else {
                redirect(base_url("panel/baatamb/".$id."?err"));
            }
            exit();
        }

        $datdes = $this->wordata->desper();
        $datgam = $this->wordata->dafper();
        $dattop = $this->wordata->topopil($id);
        $datlod = $this->wordata->asstopi($id);
        $d_wopro = $this->wordata->prosemua($id);
        $idno=$id;
        
        $judul = "BAA WO ";
        $menu = $this->set_menu("womonito");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ompunem/baauploa",compact("datlod","dattop","datdes","datgam","d_wopro","idno"));
        $this->load->view("panel/frames/footer");
    }

  public function baptamb($id = null) {


        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
             
            $bap = $this->wordata->uplobap($id);
            if($bap != false) {
            $akt=$this->pengguna->tamakt('unggah bap no. wo '.$id,$this->session->userdata('username'));
                redirect(base_url("panel/baptamb/".$id."?succ"));
            } else {
                redirect(base_url("panel/baptamb/".$id."?err"));
            }
            exit();
        }

        $datdes = $this->wordata->desper();
        $datgam = $this->wordata->dafper();
        $dattop = $this->wordata->topopil($id);
        $datlod = $this->wordata->asstopi($id);
        $d_wopro = $this->wordata->prosemua($id);
        $idno=$id;
        
        $judul = "BAP WO ";
        $menu = $this->set_menu("womonito");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ompunem/bapuploa",compact("datlod","dattop","datdes","datgam","d_wopro","idno"));
        $this->load->view("panel/frames/footer");
    }

    public function perhap($id,$idp) {


            $run= $this->wordata->perhap($idp);
            if($run != false) {
                redirect(base_url("panel/topotamb/".$id."/?hsuc"));
            } else {
                redirect(base_url("panel/topotamb/".$id."/?herr"));
            }
            exit();
    }

    public function prohap($id,$idp) {


            $run= $this->wordata->prohap($idp);
            if($run != false) {
                $akt=$this->pengguna->tamakt('hapus progress No. WO '.$id.'_progress'.$idp,$this->session->userdata('username'));
                redirect(base_url("panel/womoedit/".$id."/?hsuc"));
            } else {
                redirect(base_url("panel/womoedit/".$id."/?herr"));
            }
            exit();
    }

    public function womoliat($id = null) {
        $afrev = $this->wordata->dawoaf($id);
        $berev = $this->wordata->dawobe($id);
		$dattop = $this->wordata->topopil($id);
        $d_wopro = $this->wordata->prosemua($id);
        $datlod = $this->wordata->asstopi($id);
        $idno=$id;
        
        $judul = "Detil Progress WO";
        $menu = $this->set_menu("womonito");
        if($this->divku==23){
        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/header",compact("judul","menu"));    
        }
        
        $this->load->view("panel/ompunem/womoliat",compact("afrev","berev","datlod","dattop","d_wopro","idno"));
        $this->load->view("panel/frames/footer");
    }

    public function woeksport() {

    $nafile=str_replace(' ', '', $this->session->userdata('nama_lengkap'));
    $naleng=$this->session->userdata('nama_lengkap');

    $dafaktif = $this->wordata->wodateks();

    /*$spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'Coba Lagi !');

    $writer = new Xlsx($spreadsheet);
    $writer->save('coba.xlsx');*/
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_woe.xlsx');

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getCell('A2')->setValue(date('Y'));
    //$worksheet->getCell('B3')->setValue($this->session->userdata('id_div'));
    //$worksheet->getCell('B4')->setValue($this->session->userdata('id_jabatan'));

    $n=4;
    $angka=1;
    foreach ($dafaktif as $a) {
    if($a->sta==0){$st="open";}else{ if($a->dan==1){$st="closed";}else{$st="on progress";} }


    $worksheet->getCell('A'.$n)->setValue($angka);
    $worksheet->getCell('B'.$n)->setValue($a->tg_issue);
    $worksheet->getCell('C'.$n)->setValue('');
    $worksheet->getCell('D'.$n)->setValue($a->id_worde);
    $worksheet->getCell('E'.$n)->setValue(''); 
    $worksheet->getCell('F'.$n)->setValue($a->no_baa); 
    $worksheet->getCell('G'.$n)->setValue($a->nm_custo);
    $worksheet->getCell('H'.$n)->setValue($a->nm_svsta); 
    $worksheet->getCell('I'.$n)->setValue($a->ca_custo); 
    $worksheet->getCell('J'.$n)->setValue($a->md_custo); 
    $worksheet->getCell('K'.$n)->setValue($a->sv_custo);     
    $worksheet->getCell('L'.$n)->setValue($a->al_custo); 
    $worksheet->getCell('M'.$n)->setValue($a->nm_sales);     
    $worksheet->getCell('N'.$n)->setValue($a->tg_erefs); 
    $worksheet->getCell('O'.$n)->setValue($a->nm_ven); 
    $worksheet->getCell('P'.$n)->setValue(''); 
    $worksheet->getCell('Q'.$n)->setValue($st); 
    $worksheet->getCell('R'.$n)->setValue(''); 
    $worksheet->getCell('S'.$n)->setValue('');     
    $worksheet->getCell('T'.$n)->setValue(''); 
    $worksheet->getCell('U'.$n)->setValue($a->upd);
    $worksheet->getCell('V'.$n)->setValue($a->exe_sel);
    $worksheet->getCell('W'.$n)->setValue($a->de_custo);
    $worksheet->getCell('X'.$n)->setValue($a->tr_sta);
    $n++;   
    $angka++;
    }


    //$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');

    //$writer->save('assets/temp/'.date('Ymd').'_'.$nafile.'DailyReport.xls');

    
    /*header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename='.date('Ymd').'_'.$nafile.'DailyReport.xls');
    header('Cache-Control: max-age=0');
    $writer = new Xlsx($spreadsheet);
    $writer->save("php://output");*/
    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename='.date('Ymd').'_'.$nafile.'-WOExport.xls');
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



    redirect(base_url("panel/womonito/"));

    }

    public function womocomp($id) {


            $run= $this->datait->procomp($id);
            if($run != false) {
                redirect(base_url("panel/womonito/?csuc"));
            } else {
                redirect(base_url("panel/womonito/?cerr"));
            }
            exit();
    }

    public function woimport() {

       
        $judul = "SAP B1 IMPORT";
        $menu = $this->set_menu("womonito");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/woimport");
        $this->load->view("panel/frames/footer");
    }
    

    public function datimp() {

       
        $judul = "SAP B1 Interfacing";
        $menu = $this->set_menu("womonito");

        $akt=$this->pengguna->tamakt('Import WO dari SAP',$this->session->userdata('username'));

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ibpas/datimp");
        $this->load->view("panel/frames/footer");
    }   

    public function tarsap() {

        $judul = "TARIK CID DARI SAP";
        $menu = $this->set_menu("womonito");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ibpas/cidtar");
        $this->load->view("panel/frames/footer");
    }

    public function tarcid() {
  
        $judul = "CID BERHASIL DITARIK";
        $menu = $this->set_menu("womonito");

        $akt=$this->pengguna->tamakt('Tarik CID dari SAP',$this->session->userdata('username'));
        
        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ibpas/cidsap");
        $this->load->view("panel/frames/footer");
    } 

        private function upload_files($files,$path = "assets/uploads/lampiran/")
    {
        $config = array(
            'upload_path'   => $path,
            'allowed_types' => 'jpg|png|pdf|jpeg|bmp|gif|doc|docx|xls|xlsx|ppt|pptx',
            'overwrite'     => 1,
        );

        $this->load->library('upload', $config);

        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $split = explode(".",$image);
            $ext = end($split);

            $fileName = uniqid() . "." . $ext; //.'_'. md5($image) 

            $obj = new stdClass();
            $obj->file = $fileName;
            $obj->judul = $image;
            $images[] = $obj;

            $config['file_name'] = $fileName;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
                exit();
            }
        }

        return $images;
    }


    public function lfocwktu(){
        $post = $this->input->post();
        if(isset($post["btnSelesai"])) {
             
            $lfocsls = $this->datait->lfocselesai($post);
            if($lfocsls != false) {
                redirect(base_url("panel/?succ"));
            } else {
                redirect(base_url("panel/lfocwktu/?err"));
            }
            exit();
        }

        $judul = "Aktifitas FOC";
        $menu = $this->set_menu("dashboard");
        $daftugas = $this->datait->tugas();
        $dafaktif = $this->datait->laporanpilih();
        $hirdat = $this->datait->dakhir();

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/lfocwktu",compact("hirdat","daftugas","dafaktif"));
        $this->load->view("panel/frames/footer");
    }


    public function lfoctamb() {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url("panel/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
             
            $lfocspn = $this->datait->lfoctamb();
            if($lfocspn != false) {
                redirect(base_url("panel/lfocwktu/?succ"));
            } else {
                redirect(base_url("panel/lfoctamb/?err"));
            }
            exit();
        }

        $judul = "Tambah Aktifitas FOC";
        $menu = $this->set_menu("dashboard");
        $daftugas = $this->datait->tugas();
        $dafaktif = $this->datait->laporan();

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/lfoctamb",compact("daftugas","dafaktif"));
        $this->load->view("panel/frames/footer");
    }

    public function wodetps($n=null,$b=null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 6 && $this->session->userdata("id_jabatan") != 9 )
            redirect(base_url("panel/"));

        $datps = $this->harga->pildatwops($n,$b);

        $judul = "WO Detil per Sales";
        $menu = $this->set_menu("dashboard");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/ompunem/wotilps",compact("datps","n","b"));
        $this->load->view("panel/frames/footer");
    }

    public function rantal($n=null,$b=null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 && $this->session->userdata("id_jabatan") != 6 && $this->session->userdata("id_jabatan") != 9 )
            redirect(base_url("panel/"));

        $datil = $this->harga->pildatpre($n,$b);

        $judul = "Detil Presales";
        $menu = $this->set_menu("dashboard");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/rptcom/detilpre",compact("datil","n","b"));
        $this->load->view("panel/frames/footer");
    }


public function konsaptes() {

        $judul = "Koneksi SAP B1";
        $menu = $this->set_menu("womonito");

        $akt=$this->pengguna->tamakt('cek koneksi ke SAP B1',$this->session->userdata('username'));
            
            $this->load->view("panel/frames/header",compact("judul","menu"));
            $this->load->view("panel/ibpas/konsaptes");
            $this->load->view("panel/frames/footer");
        
    }

public function salvenue() {

        $judul = "Sales Revenue";
        $menu = $this->set_menu("salesrevenue");
            
            $this->load->view("panel/frames/header",compact("judul","menu"));
            $this->load->view("panel/ibpas/konsaptes");
            $this->load->view("panel/frames/footer");
        
    }


public function tesaja($id = null) {

        $judul = "Tes Aja";
        $menu = $this->set_menu("tesaja");
            
            $this->load->view("panel/frames/header",compact("judul","menu"));
            $this->load->view("panel/tesaja");
            $this->load->view("panel/frames/footer");
        
    }
    
    public function smehrb() {
//        if($this->jabku != 1 && $this->jabku != 11 )
//            redirect(base_url($this->uriku."/"));

               
        $datsme =$this->iboss->smehrb();
        
        $judul = "Data SME & HRB";
        $menu = $this->set_menu("menol");
       
        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/smehrb",compact("datsme"));
        $this->load->view("panel/frames/footer");
    }



}