<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Chat extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("chat");

        $this->load->model("Khusus_model","khusus");
        $this->load->library('excel');

    }

    public function index() {
   

        $judul = "DiskuC Zone";


        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("share/chat/index",compact("dafcsi","dafwon"));
        $this->load->view("panel/frames/footer");
    }

    public function daftar() {

        if($this->najab=='Presales' || $this->najab=='Presales Manager'){
            $dafcat = $this->db->get('v_app_portal_chat_daftar')->result();
        }else{
            $dafcat = $this->db->where('uswal',$this->usrku)->order_by('akhir','DESC')->get('v_app_portal_chat_daftar')->result();
        }
        

        $judul = "Daftar DiskuC";
        $menu = $this->set_menu("chat_baru");

        $this->load->view("share/akgnar/header",compact("judul","menu"));
        $this->load->view("share/chat/daftar",compact("dafcat"));
        $this->load->view("share/akgnar/footer");
    }

    public function detail($id) {

        $detcat = $this->db->where('id_rha',$id)->get('v_app_portal_chat')->result();


        $judul = "Detail DiskuC";
        $menu = $this->set_menu("chat_baru");

        $this->load->view("share/akgnar/header",compact("judul","menu"));
        $this->load->view("share/chat/detail",compact("detcat","id"));
        $this->load->view("share/akgnar/footer");
    }

    public function kirim() {
        $id = $this->input->post('id_rha');
        
        $data = array(
            "id_rha" => $id,
            "chat" => $this->input->post('chat'),
            "uschat" => $this->session->userdata('username'),
            "tgchat" => date('Y-m-d H:i:s'),
        );

        $this->db->insert('app_portal_chat', $data);

    $post=$this->input->post();
    if(isset($post["btnChat"])) { 
        redirect(base_url("chat/detail/".$id));
    }else{
        redirect(base_url("panel/"));        
    }
    
    }


     private function set_menu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",
            "menu_eng"=>"",
            "harga_baru"=>"",
            "salesrevenue"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "talian"=>"",
            "menufoc"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>"",
            "menuotb"=>"",
            "lokaotb"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "aktifitas"=>"",
            "surat_terkirim"=>"",
            "disposisi_keluar"=>"",
            "disposisi_masuk"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "inventory"=>"",
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "mascidsid"=>""
            
        ];
        $menu[$active] = "active";
        return $menu;
    }
}