<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporsla extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
		//$this->load->model('SiswaModel');
		$this->load->model("Laporan_model","laporan");
		$this->load->library('excel');
	}
	
	public function index(){
		//$data['siswa'] = $this->SiswaModel->view();
		$data['siswa'] = $this->laporan->view();
		//$this->load->view('view', $data);
	}
	
	public function export(){
		// Load plugin PHPExcel nya
		//include APPPATH.'libraries/PHPExcel.php';

		
		// Panggil class PHPExcel nya
		$excel = new PHPExcel();



		// Settingan awal fil excel
		$excel->getProperties()->setCreator('Portal')
							   ->setLastModifiedBy('My Portal')
							   ->setTitle("Laporan SLA")
							   ->setSubject("Laporan")
							   ->setDescription("Laporan SLA Pelanggan")
							   ->setKeywords("Data SLA");

		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
				'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);

				$objDrawing = new PHPExcel_Worksheet_Drawing();
                $objDrawing->setPath('./assets/images/logo.jpg');
                $objDrawing->setCoordinates('K2');
                $objDrawing->setHeight(70);
                $objDrawing->setWidth(140);
                $objDrawing->setWorksheet($excel->getActiveSheet());



		$lnapel=$this->input->post('napel');
		$jmldat=$this->laporan->hitung();
		$lakhi=date('d-M-Y', strtotime($this->input->post('tgl_akhir')));
		$lawal=date('d-M-Y', strtotime($this->input->post('tgl_awal')));
		$tahun = date('Y', strtotime($this->input->post('tgl_awal')));
        $bulan=date('m', strtotime($this->input->post('tgl_awal')));
		$Bln= $bulan[1];
		$tdown='=H11+H12+H13+H14+H15+H16+H17+H18+H19+H20+H21+H22+H23+H24+H25+H26+H27+H28+H29+H30+H31+H32+H33+H34+H35+H36+H37+H38+H39+H40';
		$datava=$this->laporan->hitava();

		foreach ($datava as $d) {
			$ava=$d->tot;
			$req=$d->Requester;
		}

	if($ava!=0 or $ava!=null){

		 $tanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

				$jmava= $ava;
				$jkonvert = explode(":", $jmava);
				$dava = $jkonvert[0]*3600 + $jkonvert[1] * 60 + $jkonvert[2];
					
                $detik_sebulan = $tanggal * 86400;
                $hitava = $detik_sebulan - $dava;
                $akhira = $hitava / $detik_sebulan * 100;
                $hasila = round($akhira, 2);
        //echo $bulan;exit;
        
		
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "PERFORMANCE REPORT"); // Set kolom A1 dengan tulisan "JUDUL"
		$excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		//$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Nomor");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "");
		$excel->setActiveSheetIndex(0)->setCellValue('A4', "Period");
		$excel->setActiveSheetIndex(0)->setCellValue('C4', $lawal." s/d ". $lakhi);	
		$excel->getActiveSheet()->getStyle('C4')->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	
		$excel->setActiveSheetIndex(0)->setCellValue('A5', "Requester");
		$excel->setActiveSheetIndex(0)->setCellValue('C5', $lnapel);
				$excel->getActiveSheet()->getStyle('C5')->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$excel->setActiveSheetIndex(0)->setCellValue('A6', "Total Trouble Ticket");
		$excel->setActiveSheetIndex(0)->setCellValue('C6', $jmldat);
		$excel->setActiveSheetIndex(0)->setCellValue('A7', "Availablity (%)");
		$excel->setActiveSheetIndex(0)->setCellValue('C7', $hasila);
		//$excel->setActiveSheetIndex(0)->setCellValue('D7', $hasila);
		$excel->setActiveSheetIndex(0)->setCellValue('A8', "Total Downtime");
		$excel->setActiveSheetIndex(0)->setCellValue('C8', $tdown);

		//$excel->setActiveSheetIndex(0)->setCellValue('D8', $ava);
		$excel->getActiveSheet()->getStyle('A3:B8')->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('argb' => '39b3d7')
                        )
                );
		$excel->getActiveSheet()->getStyle("A3:C8")->applyFromArray(
	    array(
	        'borders' => array(
	            'allborders' => array(
	                'style' => PHPExcel_Style_Border::BORDER_THIN,
	                'color' => array('argb' => '000000')
	            )
	       	  )
	    	)
		);
		
		$excel->getActiveSheet()->getStyle('C8')->getNumberFormat()->setFormatCode('[hh]:mm');
		
		// Header Tabel di baris ke 10
		$excel->setActiveSheetIndex(0)->setCellValue('A10', "No."); 
		$excel->setActiveSheetIndex(0)->setCellValue('B10', "Created_Time");
		$excel->setActiveSheetIndex(0)->setCellValue('C10', "Service Description"); 
		$excel->setActiveSheetIndex(0)->setCellValue('D10', "Request_ID"); 
		$excel->setActiveSheetIndex(0)->setCellValue('E10', "Request_Type"); 
		$excel->setActiveSheetIndex(0)->setCellValue('F10', "Down_Time"); 
		$excel->setActiveSheetIndex(0)->setCellValue('G10', "Uptime"); 
		$excel->setActiveSheetIndex(0)->setCellValue('H10', "Duration"); 
		$excel->setActiveSheetIndex(0)->setCellValue('I10', "Caused Code"); 
		$excel->setActiveSheetIndex(0)->setCellValue('J10', "Resolution"); 
		$excel->setActiveSheetIndex(0)->setCellValue('K10', "SLA (PKS %)"); 
		$excel->setActiveSheetIndex(0)->setCellValue('L10', "Availablity (%)"); 

		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L10')->applyFromArray($style_col);		
		
		// Panggil function view yang ada di Laporan Model
		$datsla = $this->laporan->view();

		$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 11; // Set baris pertama untuk isi tabel adalah baris ke 4
		foreach($datsla as $data){ // Lakukan looping pada variabel
			    
			    $jam = $data->Durasi;
				$jamkonvert = explode(":", $jam);
				$detik = $jamkonvert[0]*3600 + $jamkonvert[1] * 60 + $jamkonvert[2];
					
                $detik_sebulan = $tanggal * 86400;
                $hitung = $detik_sebulan - $detik;
                $akhir = $hitung / $detik_sebulan * 100;
                $hasil = round($akhir, 2);




			$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
			$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->Created_Time);
			$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->Subject);
			$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->Request_ID);
			$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->Request_Type);
			$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->Down_Time);
			$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->Uptime);
			$excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->Durasi);
			$excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->Root_Cause);
			$excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->Resolution);
			$excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, '99.50');
			$excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $hasil);
			
			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('K'.$numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('L'.$numrow)->applyFromArray($style_row);
			
			$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
			$nrow=$numrow;
			
			//$excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow+7, 'Manager Sales');
			//$excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow+7, 'Manager IT');
		}
			$excel->setActiveSheetIndex(0)->setCellValue('K'.($nrow+2), 'Jakarta, '.date('d-M-Y'));
			$excel->setActiveSheetIndex(0)->setCellValue('K'.($nrow+7), 'Manager Sales');
			$excel->setActiveSheetIndex(0)->setCellValue('L'.($nrow+7), 'Manager IT');
			
			

}else{
		$excel->setActiveSheetIndex(0)->setCellValue('A2', "PERFORMANCE REPORT"); // Set kolom A1 dengan tulisan "JUDUL"
		$excel->getActiveSheet()->mergeCells('A2:E2'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		//$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "Nomor");
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "");
		$excel->setActiveSheetIndex(0)->setCellValue('A4', "Period");
		$excel->setActiveSheetIndex(0)->setCellValue('C4', $lawal." s/d ". $lakhi);	
		$excel->getActiveSheet()->getStyle('C4')->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);	
		$excel->setActiveSheetIndex(0)->setCellValue('A5', "Requester");
		$excel->setActiveSheetIndex(0)->setCellValue('C5', $lnapel);
				$excel->getActiveSheet()->getStyle('C5')->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$excel->setActiveSheetIndex(0)->setCellValue('A6', "Total Trouble Ticket");
		$excel->setActiveSheetIndex(0)->setCellValue('C6', 0);
		$excel->setActiveSheetIndex(0)->setCellValue('A7', "Availablity (%)");
		$excel->setActiveSheetIndex(0)->setCellValue('C7', 100);
		//$excel->setActiveSheetIndex(0)->setCellValue('D7', $hasila);
		$excel->setActiveSheetIndex(0)->setCellValue('A8', "Total Downtime");
		$excel->setActiveSheetIndex(0)->setCellValue('C8', 0);
		//$excel->setActiveSheetIndex(0)->setCellValue('D8', $ava);

		$excel->getActiveSheet()->getStyle('C8')->getNumberFormat()->setFormatCode('[hh]:mm');

		$excel->setActiveSheetIndex(0)->setCellValue('A10', "No."); 
		$excel->setActiveSheetIndex(0)->setCellValue('B10', "Created_Time");
		$excel->setActiveSheetIndex(0)->setCellValue('C10', "Service Description"); 
		$excel->setActiveSheetIndex(0)->setCellValue('D10', "Request_ID"); 
		$excel->setActiveSheetIndex(0)->setCellValue('E10', "Request_Type"); 
		$excel->setActiveSheetIndex(0)->setCellValue('F10', "Down_Time"); 
		$excel->setActiveSheetIndex(0)->setCellValue('G10', "Uptime"); 
		$excel->setActiveSheetIndex(0)->setCellValue('H10', "Duration"); 
		$excel->setActiveSheetIndex(0)->setCellValue('I10', "Caused Code"); 
		$excel->setActiveSheetIndex(0)->setCellValue('J10', "Resolution"); 
		$excel->setActiveSheetIndex(0)->setCellValue('K10', "SLA (PKS %)"); 
		$excel->setActiveSheetIndex(0)->setCellValue('L10', "Availablity (%)"); 

		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('I10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('J10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('K10')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('L10')->applyFromArray($style_col);	

		$excel->getActiveSheet()->mergeCells('A11:L11');
		$excel->setActiveSheetIndex(0)->setCellValue('A11', " Tidak Ada Data Tiket "); 
		$excel->getActiveSheet()->getStyle('A11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$excel->getActiveSheet()->getStyle('A11')->getFont()->setBold(TRUE); 
		$excel->getActiveSheet()->getStyle('A11')->getFont()->setSize(13);

		$excel->setActiveSheetIndex(0)->setCellValue('K'.(13), 'Jakarta, '.date('d-M-Y'));
		$excel->setActiveSheetIndex(0)->setCellValue('K'.(18), 'Manager Sales');
		$excel->setActiveSheetIndex(0)->setCellValue('L'.(18), 'Manager IT');
}
		$excel->getActiveSheet()->getStyle('A10:L10')->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('argb' => '39b3d7')
                        )
                );
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(17); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(40); // Set width kolom C
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(12); // Set width kolom D
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(20); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(16); // Set width kolom F
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(16); // Set width kolom G
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(8); // Set width kolom H
		$excel->getActiveSheet()->getColumnDimension('I')->setWidth(15); // Set width kolom I
		$excel->getActiveSheet()->getColumnDimension('J')->setWidth(75); // Set width kolom J
		$excel->getActiveSheet()->getColumnDimension('K')->setWidth(13); // Set width kolom K
		$excel->getActiveSheet()->getColumnDimension('L')->setWidth(13); // Set width kolom L

		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle(substr($lnapel,0,25));
		$excel->setActiveSheetIndex(0);

		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="RPT_SLA_'.$lnapel.'.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');

		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	}
}
