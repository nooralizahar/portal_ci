<?php


defined("BASEPATH") OR exit("Akses ditolak!");
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Datbar extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }
        
        $this->load->library('phpqrcode/qrlib');
        $this->load->helper('url');
        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Datbar_model","datbar");
        $this->load->model("Otbdata_model","otbdat");

    }

    public function index() {
   

        $judul = "Menu Log Book";
        $menu = $this->set_amenu("menuotb");


        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/tounem/menuotb");
        $this->load->view("panel/frames/footer");
    }

    public function logboo() {
            $harini= date('Y-m-d');
            $thnini= date('y');
            $thnlal= $thnini-1;
            $blnini= date('Y-m'); //2020-01
            $blnlal= date('Y-m',strtotime("-1 month")); 

        $post = $this->input->post();
        $silog= $this->input->post('nmtoo').'_'.$this->input->post('nmper').'_'.$this->input->post('keter');

        if(isset($post["btnSimpan"])) {
            $tbh = $this->datbar->logtam();
            if($tbh != false) {
                $akt=$this->pengguna->tamakt('Tambah log '.$silog,$this->session->userdata('username'));
                redirect(base_url("datbar/logboo/?succ"));
            } else {
                redirect(base_url("datbar/logboo/?err"));
            }
            exit();
        }

        $pmakai = $this->datbar->focsem();
        $logboo = $this->datbar->logsem();
        $lhrini = $this->datbar->loghit($harini);
        $lblini = $this->datbar->loghit($blnini);
        
        $judul = "FOC - Log Book";
        $menu = $this->set_amenu("menuotb");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/adunem/lboli",compact("logboo","focsem","pmakai","lhrini","lblini"));
        $this->load->view("panel/frames/footer");
    }



    public function loged($idlog=null) {
   
        $logpil = $this->datbar->logpil($idlog);
        $pmakai = $this->datbar->focsem();
        $silog=$idlog;
        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $sim = $this->datbar->logsim($idlog);
            if($sim != false) {
                $akt=$this->pengguna->tamakt('Ubah log '.$silog,$this->session->userdata('username'));
                redirect(base_url("datbar/logboo/".$idlog."?succ"));
            } else {
                redirect(base_url("datbar/loged/".$idlog."?err"));
            }
            exit();
        }

        $judul = "Edit Buku Log";
        $menu = $this->set_amenu("menuotb");      

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/adunem/lboed",compact("logpil","pmakai"));//,"pi","onm"
        $this->load->view("panel/frames/footer");
    }

    public function brged($idbrg=null) {
   
        $pb = $this->datbar->pilbar($idbrg);
        $pmakai = $this->datbar->focsem();
        $nisbar = $this->datbar->jebar();
         $silog=$idbrg;
        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $sim = $this->datbar->brgsim($idbrg);
            if($sim != false) {
                $akt=$this->pengguna->tamakt('Ubah barang '.$silog,$this->session->userdata('username'));
                redirect(base_url("datbar/logbar/".$idbrg."?succ"));
            } else {
                redirect(base_url("datbar/brged/".$idbrg."?err"));
            }
            exit();
        }

        $judul = "Edit Log Barang";
        $menu = $this->set_amenu("menuotb");      

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/adunem/lbaed",compact("pb","pmakai","nisbar"));//,"pi","onm"
        $this->load->view("panel/frames/footer");
    }

    public function brgha($id=null) {
        $pb = $this->datbar->pilbar($id);
        $silog=$pb->id.'_'.$pb->tybrg.'_'.$pb->tybrg;
        $hap = $this->datbar->brgpus($id);

            if($hap){
                $akt=$this->pengguna->tamakt('Hapus barang '.$silog,$this->session->userdata('username'));
                redirect(base_url("datbar/logbar/?hsucc"));
            }else{
                redirect(base_url("datbar/logbar/?herr"));
            }
    }

    public function logha($id=null) {
        $pl = $this->datbar->pillog($id);
        $silog=$pl->id.'_'.$pl->nmtoo.'_'.$pl->nmper.'_'.$pl->keter;

        $h= $this->datbar->logpus($id);

            if($h){
                 $akt=$this->pengguna->tamakt('Hapus log '.$silog,$this->session->userdata('username'));
                redirect(base_url("datbar/logboo/?hsucc"));
            }else{
                redirect(base_url("datbar/logboo/?herr"));
            }
    }


    public function logbar() {
            $harini= date('Y-m-d');
            $thnini= date('y');
            $thnlal= $thnini-1;
            $blnini= date('Y-m'); //2020-01
            $blnlal= date('Y-m',strtotime("-1 month")); 

        $post = $this->input->post();
        $silog= $this->input->post('tybrg').'_'.$this->input->post('snbrg').'_'.$this->input->post('untuk');

        if(isset($post["btnSimpan"])) {
            $tbh = $this->datbar->brgtam();
            if($tbh != false) {
                $akt=$this->pengguna->tamakt('Tambah barang '.$silog,$this->session->userdata('username'));
                redirect(base_url("datbar/logbar/?succ"));
            } else {
                redirect(base_url("datbar/logbar/?err"));
            }
            exit();
        }

        $pmakai = $this->datbar->focsem();
        $logbrg = $this->datbar->brgsem();
        $logsfp = $this->datbar->brgpil(1);
        $logpat = $this->datbar->brgpil(2);
        $logper = $this->datbar->brgpil(3);
        $lhrini = $this->datbar->loghit($harini);
        $lblini = $this->datbar->loghit($blnini);
        
        $judul = "FOC - Log Barang";
        $menu = $this->set_amenu("menuotb");
        $menu = $this->set_amenu("mesat");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/adunem/barli",compact("logbrg","logsfp","logpat","logper","focsem","pmakai","lhrini","lblini"));
        $this->load->view("panel/frames/footer");
    }

    public function eksporxls($pi=null) {
    $nafile=str_replace(' ', '', $this->session->userdata('nama_lengkap'));
    $naleng=$this->session->userdata('nama_lengkap');
    
    $otbde = $this->otbdat->otlih($pi);
    $otbpi = $this->otbdat->otpil($pi);
    foreach ($otbpi as $a) {
           $onm=$a->nm_otb;
    }
    /*$spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'Coba Lagi !');

    $writer = new Xlsx($spreadsheet);
    $writer->save('coba.xlsx');*/
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_otb.xlsx');

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getCell('A2')->setValue($onm);
    //$worksheet->getCell('B3')->setValue($this->session->userdata('id_div'));
    //$worksheet->getCell('B4')->setValue($this->session->userdata('id_jabatan'));

    $n=6;
    foreach ($otbde as $dat) {
    $worksheet->getCell('A'.$n)->setValue($dat->ou_des);
    $worksheet->getCell('B'.$n)->setValue($dat->id_cor);
    $worksheet->getCell('D'.$n)->setValue($dat->nm_gab);
    $worksheet->getCell('E'.$n)->setValue($dat->tg_upd." oleh ".$dat->us_upd);

    $n++;   
    }


    //$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');

    //$writer->save('assets/temp/'.date('Ymd').'_'.$nafile.'DailyReport.xls');

    
    /*header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename='.date('Ymd').'_'.$nafile.'DailyReport.xls');
    header('Cache-Control: max-age=0');
    $writer = new Xlsx($spreadsheet);
    $writer->save("php://output");*/
    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename=Data_'.str_replace(' ', '-',$onm).'_ExpBy_'.$nafile.'-'.date('Ymd').'.xls');
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



    redirect(base_url("datbar/logboo/"));

    }


    private function set_amenu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "harga_baru"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "talian"=>"",
            "menuotb"=>"",
            "lokaotb"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "aktifitas"=>"",
            "surat_terkirim"=>"",
            "disposisi_keluar"=>"",
            "disposisi_masuk"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "inventory"=>"",
            "menu_eng"=>"",
            "menol"=>"",
            "mesat"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }




}