<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Datpel extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->usrku=$this->session->userdata("username");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("general");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->library('pdf');

        $this->load->model("Khusus_model","khusus");
        $this->load->library('excel');
        //$this->load->model("Laporan_model","laporan");
        $this->load->model("Harga_model","harga");
        $this->load->model("Wordata_model","wordata");
        $this->load->model("Genral_model","genral");
    }


// awal CS Dedicated Controller

    public function index() {

        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));


        $woblni=$this->harga->pilwo($blnini);
        $woblnl=$this->harga->pilwo($blnlal);
        $wosum=$this->harga->wosummary();
        $wosal=$this->harga->wosales();
        $woseg=$this->harga->wosegme();
        $wosta=$this->harga->wostatu();
        if($this->session->userdata('id_jabatan')==22){
        $datait_wo = $this->wordata->wo4baa();
        }else{
        $datait_wo = $this->wordata->wosemua();
        }
        $data_wopil = $this->wordata->wopilih();



        $judul = "Data Pelanggan";
        $menu = $this->set_menu("menol");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/csdedi/dpela",compact("wosta","woseg","wosal","woblnl","woblni","wosum","datait_wo","data_wopil"));
        $this->load->view("share/akgnar/hakhi");

    }

    public function dascsd() {
        $judul = "Dashboard Customer Support";
        $menu = $this->set_menu("metuj");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("nuber/csdedi/shocsde");
        $this->load->view("share/akgnar/hakhi");

    }

    public function aktif() {
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));


        $woblni=$this->harga->pilwo($blnini);
        $woblnl=$this->harga->pilwo($blnlal);
        $wosum=$this->harga->wosummary();
        $wosal=$this->harga->wosales();
        $woseg=$this->harga->wosegme();
        $wosta=$this->harga->wostatu();
        if($this->session->userdata('id_jabatan')==22){
        $datait_wo = $this->wordata->wo4baa();
        }else{
        $datait_wo = $this->wordata->wosemua();
        }
        $data_wopil = $this->wordata->wopilih();
        $datait_pe = $this->wordata->woaktif();

        $judul = "Data Pelanggan Aktif";
        $menu = $this->set_menu("mesat");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/csdedi/dptif",compact("wosta","woseg","wosal","woblnl","woblni","wosum","datait_wo","data_wopil","datait_pe"));
        $this->load->view("share/akgnar/hakhi");

    }

// akhir CS Dedicated Controller


    public function logout() {
        $akt=$this->genral->tamakt('logout berhasil',$this->usrku);
        $this->session->sess_destroy();
        redirect(base_url("login/"));
    }

    private function set_menu($active) {
        $menu = [
            "dashboard"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",
            "pengguna"=>"",
            "divisi"=>"", 
            "jabatan"=>"",
            "kaarea"=>"",
            "talian"=>"",
            "survey"=>"",           
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "meena"=>"",
            "ladload"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }


}