<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Selser extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");        

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->library('zip');
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }

        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Datait_model","datait");
        $this->load->model("Laporan_model","laporan");

    }

    public function index() {
   

        $judul = "Layanan Mandiri";
        $menu = $this->set_amenu("selfservice");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/rcunem/menucr");
        $this->load->view("panel/frames/footer");
    }

    public function repsla() {

        $list_file=array();

if(empty($this->uri->segment(3))){
    $dir = "assets/uploads/alsper/";
}elseif(!empty($this->uri->segment(3))) {
    if(empty($this->uri->segment(4))){
        $dir = "assets/uploads/alsper/".$this->uri->segment(3)."/";
    }elseif(!empty($this->uri->segment(4))){
        $dir = "assets/uploads/alsper/".$this->uri->segment(3)."/".$this->uri->segment(4)."/";
    }
}

        // buka directory, dan baca isinya
        if (is_dir($dir)){
          if ($dh = opendir($dir)){
            while (($file = readdir($dh)) !== false){
                $list_file[]=$file;
            }
            closedir($dh);
          }
        }

        $data['daftar_file']=$list_file;
        $data['tiksemua']=$this->laporan->semua();
        $data['pelsemua']=$this->laporan->pelanggan();
        $data['Requester']=$this->laporan->dafreq();
        //$this->load->view('home',$data);

        //$tiksemua = $this->laporan->semua();
        //$pelsemua = $this->laporan->pelanggan();


        
        $judul = "Self Service - Laporan SLA Pelanggan";
        $menu = $this->set_amenu("selfservice");

        if($this->divku==23 ){
        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("panel/rcunem/repsla",$data); //compact("tiksemua","pelsemua")
        $this->load->view("panel/frames/footer");
    }


    private function set_amenu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "harga_baru"=>"",
            "salesrevenue"=>"",
            "chat_baru"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "menu_eng"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "talian"=>"",
            "menufoc"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "aktifitas"=>"",
            "daftaodp"=>"",
            "surat_terkirim"=>"",
            "disposisi_keluar"=>"",
            "disposisi_masuk"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "mascidsid"=>"",
            "inventory"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }


    public function petalokasi($id) {
        $datait_byid = $this->datait->odcbyid($id);
        $judul = "Lokasi Survey";
        $menu = $this->set_menu("Data Survey");

        
        $this->load->view("panel/survlokasi",compact("datait_byid"));
        
    }

    public function zipfol($par=null,$fol=null) {


        
        /*$alam = $this->uri->segment(3);
        $nafi = $fol.'.zip';
        $path = '/var/www/portal/assets/uploads/alsper/'.$alam.'/'.$fol.'/';
        $this->zip->read_dir($path);
        $this->zip->archive(FCPATH.'/assets/tmp/'.$nafi);
        $this->zip->download($nafi); 
        redirect(base_url("selser/repsla/".$fol)); */

     //if($fol != NULL){
        // File name
        $filename = $par.'-'.$fol."_semua.zip";
        // Directory path (uploads directory stored in project root)
        $path = '/var/www/portal/assets/uploads/alsper/'.$par.'/'.$fol.'/';

        // Add directory to zip
        $this->zip->read_dir($path);

        // Save the zip file to archivefiles directory
        $this->zip->archive('/var/www/portal/assets/uploads/tmp/'.$filename);

        // Download
        $this->zip->download($filename);
     //}
    }



}