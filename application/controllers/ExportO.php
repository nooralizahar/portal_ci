<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export extends CI_Controller {

    function __construct() {
        parent::__construct();
         //$this->load->model('Export_model');
         $this->load->model("Export_model","ekspor");

    }

    public function index() {
        if ($this->input->post('pdf')) {
            $this->load->library('mpdf');

            $requester = $this->input->post('nama');
            $tgl_awal = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
            $tahun = date('Y');
            $bulan=Explode('-', $tgl_awal);
			$Bln= $bulan[1];
            //echo $bulan;exit;
            $tanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
            $pdf = $this->ekspor->get_Requester($requester, $tgl_awal, $tgl_akhir);
            $nama_dokumen = 'PERFORMANCE REPORT'; //Beri nama file PDF hasil.
            $mpdf = new mPDF('L', // mode - default ''
                    'A4', // format - A4, for example, default ''
                    8, // font size - default 0
                    '', // default font family
                    10, // margin_left
                    10, // margin right
                    16, // margin top
                    16, // margin bottom
                    9, // margin header
                    9, // margin footer
                    'L');  //
            ob_start();
            $count = $this->ekspor->get_countRequester($requester, $tgl_awal, $tgl_akhir);
            foreach ($count as $row) {
                $total = $row['total'];
                $ava = 0;
                foreach ($pdf as $a) {
                    $jam2 = $a['DURASI'];
					$jamkonvert2 = explode(":", $jam2);
					$detik2 = $jamkonvert2[0]*3600 + $jamkonvert2[1] * 60 + $jamkonvert2[2];
					$detik_sebulan2 = $tanggal * 86400;
					$hitung2 = $detik_sebulan2 - $detik2;
					$akhir2 = $hitung2 / $detik_sebulan2 * 100;
					$hasil2 = round($akhir2, 2);

                    $ava += $hasil2 / $total;
                    $ava = round($ava, 2);
                }
                $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

                $tahun_print = substr($tgl_awal, 0, 4);
                $bulan_print = substr($tgl_awal, 5, 2);
                $tgl_print = substr($tgl_awal, 8, 2);
                $tahun_print2 = substr($tgl_akhir, 0, 4);
                $bulan_print2 = substr($tgl_akhir, 5, 2);
                $tgl_print2 = substr($tgl_akhir, 8, 2);

                $tgl_awalx = $tgl_print . " " . $BulanIndo[(int) $bulan_print - 1] . " " . $tahun_print;
                $tgl_akhirx = $tgl_print2 . " " . $BulanIndo[(int) $bulan_print2 - 1] . " " . $tahun_print2;
                $html = '
                <label> No</label><label></label><br>
                <label> Period</label><label> : <b>' . $tgl_awalx . '</b> -  <b>' . $tgl_akhirx . '</b></label><br>
                <label> Requester</label><label> : <b>' . $requester . '</b></label><br>
                <label> Total Trouble Ticket</label><label> : <b>' . $total . '</b> Trouble Tickets</label><br>
                <label> Availability</label><label> : <b>' . $ava . '</b> %</label><br>
                <div align="center"><h4>PERFORMANCE REPORT</h4></div>
                 <table style="width:100%;border-collapse:collapse;" border="1" > 
                <thead>
                <tr>
                  <th style="text-align:center; padding:5px 5px; background-color:#4C9ED9; color: #fff;">No</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Created Time</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Service Description</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Request ID</th>  
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Request Type </th>   
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Downtime </th> 
                  <th style="text-align:center; padding:5px 5px; background-color:#4C9ED9; color: #fff;">Uptime</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Duration</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Cause Code</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Resolution</th>  
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">SLA (PKS)</th>  
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Availability</th>
                  </tr>
                </thead>
                ';
            }
            $i = 1;
            foreach ($pdf as $s):
                $jam = $s['DURASI'];
				$jamkonvert = explode(":", $jam);
				$detik = $jamkonvert[0]*3600 + $jamkonvert[1] * 60 + $jamkonvert[2];
					
                $detik_sebulan = $tanggal * 86400;
                $hitung = $detik_sebulan - $detik;
                $akhir = $hitung / $detik_sebulan * 100;
                $hasil = round($akhir, 2);
                $html .='<tr><td style="text-align:center;">' . $i++ . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Created_Time'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Subject'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['request_id'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['request_type'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Down_Time'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Uptime'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['DURASI'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $detik . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Root_Cause'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Resolution'] . '</td>
                <td style="text-align:center; padding:3px 3px;"> 99,50%</td>
                <td style="text-align:center; padding:3px 3px;">' . $hasil . ' %</td>
                </tr>';
            endforeach;
            $html .='</table>';
            $mpdf->AddPage($i);
            $mpdf->WriteHTML($html);
            $mpdf->Output($nama_dokumen . ".pdf", 'I');
        } else {
            if ($this->input->post('excel')) {

                $nama = $this->input->post('nama');
                $tgl_awal = $this->input->post('tgl_awal');
                $tgl_akhir = $this->input->post('tgl_akhir');

                //Mendefinisikan Jumlah Hari Dalam Sebulan
                $tahun = date('Y');
                $bulan = substr($tgl_awal, 3, 2);
                //echo $bulan;exit;
                $tanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
                //echo $hasil;exit;
                //Time Zone
                date_default_timezone_set('Asia/Jakarta');

                //Memanggil Library Excel
                $this->load->library('excel');

                //Posisi Awal Sheet
                $this->excel->setActiveSheetIndex(0);

                //Setting Kolom Statis
                $this->excel->getActiveSheet()->setCellValue('A2', 'PERFORMANCE REPORT');
                $this->excel->getActiveSheet()->mergeCells('A2:D2');
                $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(16);
                $this->excel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('#333');
                $this->excel->getActiveSheet()->getStyle('A3:B7')->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('rgb' => '39b3d7')
                        )
                );
                $this->excel->getActiveSheet()->getStyle('C7')->getNumberFormat()->setFormatCode('0.00');
                $this->excel->getActiveSheet()->getStyle('L11:L100')
                        ->getNumberFormat()->setFormatCode('0.00');
                $this->excel->getActiveSheet()->getStyle('C7')->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->excel->getActiveSheet()->getStyle('L11:L100')->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //Kolom Statis
                $this->excel->getActiveSheet()->setCellValue('A3', 'Nomor');
                $this->excel->getActiveSheet()->mergeCells('A3:B3');
                $this->excel->getActiveSheet()->setCellValue('A4', 'Period');
                $this->excel->getActiveSheet()->mergeCells('A4:B4');
                $this->excel->getActiveSheet()->setCellValue('A5', 'Requester');
                $this->excel->getActiveSheet()->mergeCells('A5:B5');
                $this->excel->getActiveSheet()->setCellValue('A6', 'Total Trouble Ticket');
                $this->excel->getActiveSheet()->mergeCells('A6:B6');
                $this->excel->getActiveSheet()->setCellValue('A7', 'Availability (%)');
                $this->excel->getActiveSheet()->mergeCells('A7:B7');

                $objDrawing = new PHPExcel_Worksheet_Drawing();
                $objDrawing->setPath('./assets/image/logo.jpg');
                $objDrawing->setCoordinates('K2');
                $objDrawing->setHeight(70);
                $objDrawing->setWidth(140);
                $objDrawing->setWorksheet($this->excel->getActiveSheet());

                //Isi Kolom Statis
                $requester_count = $this->ekspor->get_countRequester($nama, $tgl_awal, $tgl_akhir);
                foreach ($requester_count as $rows) {
                    $total = $rows['total'];
                }

                $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

                $tahun_print = substr($tgl_awal, 0, 4);
                $bulan_print = substr($tgl_awal, 5, 2);
                $tgl_print = substr($tgl_awal, 8, 2);
                $tahun_print2 = substr($tgl_akhir, 0, 4);
                $bulan_print2 = substr($tgl_akhir, 5, 2);
                $tgl_print2 = substr($tgl_akhir, 8, 2);

                $tgl_awalx = $tgl_print . " " . $BulanIndo[(int) $bulan_print - 1] . " " . $tahun_print;
                $tgl_akhirx = $tgl_print2 . " " . $BulanIndo[(int) $bulan_print2 - 1] . " " . $tahun_print2;
                //echo $nm_requester;exit;
                $this->excel->getActiveSheet()->setTitle('' . $nama);
                $this->excel->getActiveSheet()->mergeCells('C3:D3');
                $this->excel->getActiveSheet()->setCellValue('C4', '' . $tgl_awalx . ' - ' . $tgl_akhirx);
                $this->excel->getActiveSheet()->mergeCells('C4:D4');
                $this->excel->getActiveSheet()->setCellValue('C5', '' . $nama);
                $this->excel->getActiveSheet()->mergeCells('C5:D5');
                $this->excel->getActiveSheet()->setCellValue('C6', '' . $total . ' Trouble Tickets');
                $this->excel->getActiveSheet()->mergeCells('C6:D6');
                $this->excel->getActiveSheet()->setCellValue('C7', '=AVERAGE(L11:L100)');
                $this->excel->getActiveSheet()->mergeCells('C7:D7');

                /* script menentukan tanggal */
                $tgls = date('j');

                /* script menentukan bulan */
                $array_bln = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                $bln = $array_bln[date('n')];

                /* script menentukan tahun */
                $thn = date('Y');
                /* script perintah keluaran */
                $sekarang = $tgls . " " . $bln . " " . $thn;
                //Footer
                $mark = $total + 17;
                $tgl = $total + 13;
                $this->excel->getActiveSheet()->getStyle('J' . $mark . ':K' . $tgl)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->setCellValue('K' . $mark, 'Manager Sales');
                $this->excel->getActiveSheet()->setCellValue('L' . $mark, 'Manager IT');

                $this->excel->getActiveSheet()->setCellValue('K' . $tgl, 'Jakarta, ' . $sekarang);
                $this->excel->getActiveSheet()->mergeCells('K' . $tgl . ':L' . $tgl);

                //Judul Kolom Looping
                $this->excel->getActiveSheet()->setCellValue('A9', 'No');
                $this->excel->getActiveSheet()->mergeCells('A9:A10');
                $this->excel->getActiveSheet()->setCellValue('B9', 'Create Time');
                $this->excel->getActiveSheet()->mergeCells('B9:B10');
                $this->excel->getActiveSheet()->setCellValue('C9', 'Service Description');
                $this->excel->getActiveSheet()->mergeCells('C9:C10');
                $this->excel->getActiveSheet()->setCellValue('D9', 'TROUBLE  TICKET');
                $this->excel->getActiveSheet()->mergeCells('D9:G9');
                $this->excel->getActiveSheet()->setCellValue('D10', 'Request_ID');
                $this->excel->getActiveSheet()->setCellValue('E10', 'Request Type');
                $this->excel->getActiveSheet()->setCellValue('F10', 'Downtime');
                $this->excel->getActiveSheet()->setCellValue('G10', 'Uptime');
                $this->excel->getActiveSheet()->setCellValue('H10', 'Duration');
                $this->excel->getActiveSheet()->setCellValue('I9', 'Caused Code');
                $this->excel->getActiveSheet()->mergeCells('I9:I10');
                $this->excel->getActiveSheet()->setCellValue('J9', 'Resolution');
                $this->excel->getActiveSheet()->mergeCells('J9:J10');
                $this->excel->getActiveSheet()->setCellValue('K9', 'SLA (PKS %)');
                $this->excel->getActiveSheet()->mergeCells('K9:K10');
                $this->excel->getActiveSheet()->setCellValue('L9', 'Availability (%)');
                $this->excel->getActiveSheet()->mergeCells('L9:L10');
                $this->excel->getActiveSheet()->getStyle('A9:L10')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('A9:L10')->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('rgb' => '39b3d7')
                        )
                );
                $this->excel->getActiveSheet()->getStyle('A3:D7')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('A9:L10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                for ($col = ord('A'); $col <= ord('L'); $col++) {
                    $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
                    $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                    $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setWrapText(true);
                }

                //Query Menampilkan Data Looping
                $requester = $this->ekspor->get_Requester($nama, $tgl_awal, $tgl_akhir);

                //Looping Isi Table
                $i = 11;
                $no = 0;
                foreach ($requester as $row) {
                    $jam = $row['DURASI'];
					$jamkonvert = explode(":", $jam);
					$detik = $jamkonvert[0]*3600 + $jamkonvert[1] * 60 + $jamkonvert[2];
					
                    $detik_sebulan = $tanggal * 86400;
                    $hitung = $detik_sebulan - $detik;
                    $akhir = $hitung / $detik_sebulan * 100;
                    $hasil = round($akhir, 2);
                    
                    //$availability = str_replace(".", ",", $hasil);
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->getStyle('A' . $i . ':' . 'L' . $i)->getFont()->setSize(8);
                    $this->excel->getActiveSheet()->getStyle('A' . $i . ':' . 'L' . $i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                    $this->excel->getActiveSheet()->setCellValue('A' . $i, $no + 1);
                    $this->excel->getActiveSheet()->setCellValue('B' . $i, $row['Created_Time']);
                    $this->excel->getActiveSheet()->setCellValue('C' . $i, $row['Subject']);
                    $this->excel->getActiveSheet()->setCellValue('D' . $i, $row['request_id']);
                    $this->excel->getActiveSheet()->setCellValue('E' . $i, $row['request_type']);
                    $this->excel->getActiveSheet()->setCellValue('F' . $i, $row['Down_Time']);
                    $this->excel->getActiveSheet()->setCellValue('G' . $i, $row['Uptime']);
                    $this->excel->getActiveSheet()->setCellValue('H' . $i, $row['DURASI']);
                    $this->excel->getActiveSheet()->setCellValue('I' . $i, $row['Root_Cause']);
                    $this->excel->getActiveSheet()->setCellValue('J' . $i, $row['Resolution']);
                    $this->excel->getActiveSheet()->setCellValue('K' . $i, '99,50');
                    $this->excel->getActiveSheet()->setCellValue('L' . $i, $hasil);
                    $i = $i + 1;
                    $no = $no + 1;
                }

                //Setting Output Excel
                $filename = '' . $nama . '_' . date('d/m/y') . '.xlsx'; //Nama File Berdasarkan Requester
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //Format Untuk .xlsx
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');

                ob_end_clean();
                //Output Untuk Excel 2007
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

                $objWriter->save('php://output');
            }
        }
    }

    public function problem() {
        if ($this->input->post('p_pdf')) {
            $this->load->library('mpdf');

            $problem = $this->input->post('p_nama');
            $tgl_awal = $this->input->post('p_tgl_awal');
            $tgl_akhir = $this->input->post('p_tgl_akhir');
            $tahun = date('Y');
            $bulan = substr($tgl_awal, 3, 2);
            //echo $bulan;exit;
            $tanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
            $pdf = $this->ekspor->get_Problem($problem, $tgl_awal, $tgl_akhir);
            $nama_dokumen = 'PERFORMANCE REPORT'; //Beri nama file PDF hasil.
            $mpdf = new mPDF('L', // mode - default ''
                    'A4', // format - A4, for example, default ''
                    8, // font size - default 0
                    '', // default font family
                    10, // margin_left
                    10, // margin right
                    16, // margin top
                    16, // margin bottom
                    9, // margin header
                    9, // margin footer
                    'L');  //
            ob_start();
            $count = $this->ekspor->get_countProblem($problem, $tgl_awal, $tgl_akhir);
            foreach ($count as $row) {
                $total = $row['total'];
                $ava = 0;
                foreach ($pdf as $a) {
                    $jam2 = $a['DURASI'];
					$jamkonvert2 = explode(":", $jam2);
					$detik2 = $jamkonvert2[0]*3600 + $jamkonvert2[1] * 60 + $jamkonvert2[2];
					$detik_sebulan2 = $tanggal * 86400;
					$hitung2 = $detik_sebulan2 - $detik2;
					$akhir2 = $hitung2 / $detik_sebulan2 * 100;
					$hasil2 = round($akhir2, 2);

                    $ava += $hasil2 / $total;
                    $ava = round($ava, 2);
                }
                $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

                $tahun_print = substr($tgl_awal, 0, 4);
                $bulan_print = substr($tgl_awal, 5, 2);
                $tgl_print = substr($tgl_awal, 8, 2);
                $tahun_print2 = substr($tgl_akhir, 0, 4);
                $bulan_print2 = substr($tgl_akhir, 5, 2);
                $tgl_print2 = substr($tgl_akhir, 8, 2);

                $tgl_awalx = $tgl_print . " " . $BulanIndo[(int) $bulan_print - 1] . " " . $tahun_print;
                $tgl_akhirx = $tgl_print2 . " " . $BulanIndo[(int) $bulan_print2 - 1] . " " . $tahun_print2;
                $html = '
                <label> No</label><label></label><br>
                <label> Period</label><label> : <b>' . $tgl_awalx . '</b> -  <b>' . $tgl_akhirx . '</b></label><br>
                <label> Problem Side</label><label> : <b>' . $problem . '</b></label><br>
                <label> Total Trouble Ticket</label><label> : <b>' . $total . '</b> Trouble Tickets</label><br>
                <label> Availability</label><label> : <b>' . $ava . '</b> %</label><br>
                <div align="center"><h4>PERFORMANCE REPORT</h4></div>
                 <table style="width:100%;border-collapse:collapse;" border="1" > 
                <thead>
                <tr>
                  <th style="text-align:center; padding:5px 5px; background-color:#4C9ED9; color: #fff;">No</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Requester</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Created Time</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Service Description</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Request ID</th>  
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Request Type </th>   
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Downtime </th> 
                  <th style="text-align:center; padding:5px 5px; background-color:#4C9ED9; color: #fff;">Uptime</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Duration</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Cause Code</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Resolution</th>  
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">SLA (PKS)</th>  
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Availability</th>
                  </tr>
                </thead>
                ';
            }
            $i = 1;
            foreach ($pdf as $s):
                $jam = $s['DURASI'];
				$jamkonvert = explode(":", $jam);
				$detik = $jamkonvert[0]*3600 + $jamkonvert[1] * 60 + $jamkonvert[2];
                $detik_sebulan = $tanggal * 86400;
                $hitung = $detik_sebulan - $detik;
                $akhir = $hitung / $detik_sebulan * 100;
                $hasil = round($akhir, 2);

                $html .='<tr><td style="text-align:center;">' . $i++ . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Requester'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Created_Time'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Subject'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['request_id'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['request_type'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Down_Time'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Uptime'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['DURASI'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Root_Cause'] . '</td>
                <td style="text-align:center; padding:3px 3px;">' . $s['Resolution'] . '</td>
                <td style="text-align:center; padding:3px 3px;"> 99,50%</td>
                <td style="text-align:center; padding:3px 3px;">' . $hasil . '%</td>
                </tr>';
            endforeach;
            $html .='</table>';
            $mpdf->AddPage($i);
            $mpdf->WriteHTML($html);
            $mpdf->Output($nama_dokumen . ".pdf", 'I');
        } else {
            if ($this->input->post('p_excel')) {

                $problem = $this->input->post('p_nama');
                $tgl_awal = $this->input->post('p_tgl_awal');
                $tgl_akhir = $this->input->post('p_tgl_akhir');

                //Mendefinisikan Jumlah Hari Dalam Sebulan
                $tahun = date('Y');
                $bulan = substr($tgl_awal, 3, 2);
                //echo $problem;exit;
                $tanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
                //echo $hasil;exit;
                //Time Zone
                date_default_timezone_set('Asia/calcutta');

                //Memanggil Library Excel
                $this->load->library('excel');

                //Posisi Awal Sheet
                $this->excel->setActiveSheetIndex(0);

                //Setting Kolom Statis
                $this->excel->getActiveSheet()->setCellValue('A2', 'PERFORMANCE REPORT');
                $this->excel->getActiveSheet()->mergeCells('A2:D2');
                $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(16);
                $this->excel->getActiveSheet()->getStyle('A2')->getFill()->getStartColor()->setARGB('#333');
                $this->excel->getActiveSheet()->getStyle('A3:B7')->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('rgb' => '39b3d7')
                        )
                );
                $this->excel->getActiveSheet()->getStyle('C7')->getNumberFormat()->setFormatCode('0.00');
                $this->excel->getActiveSheet()->getStyle('M11:M100')
                        ->getNumberFormat()->setFormatCode('0.00');
                $this->excel->getActiveSheet()->getStyle('C7')->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $this->excel->getActiveSheet()->getStyle('M11:M100')->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                //Kolom Statis
                $this->excel->getActiveSheet()->setCellValue('A3', 'Nomor');
                $this->excel->getActiveSheet()->mergeCells('A3:B3');
                $this->excel->getActiveSheet()->setCellValue('A4', 'Period');
                $this->excel->getActiveSheet()->mergeCells('A4:B4');
                $this->excel->getActiveSheet()->setCellValue('A5', 'Problem Side');
                $this->excel->getActiveSheet()->mergeCells('A5:B5');
                $this->excel->getActiveSheet()->setCellValue('A6', 'Total Trouble Ticket');
                $this->excel->getActiveSheet()->mergeCells('A6:B6');
                $this->excel->getActiveSheet()->setCellValue('A7', 'Availability (%)');
                $this->excel->getActiveSheet()->mergeCells('A7:B7');

                $objDrawing = new PHPExcel_Worksheet_Drawing();
                $objDrawing->setPath('./assets/image/logo.jpg');
                $objDrawing->setCoordinates('L2');
                $objDrawing->setHeight(70);
                $objDrawing->setWidth(140);
                $objDrawing->setWorksheet($this->excel->getActiveSheet());

                //Isi Kolom Statis
                $problem_count = $this->ekspor->get_countProblem($problem, $tgl_awal, $tgl_akhir);
                foreach ($problem_count as $rows) {
                    $total = $rows['total'];
                }
                $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

                $tahun_print = substr($tgl_awal, 0, 4);
                $bulan_print = substr($tgl_awal, 5, 2);
                $tgl_print = substr($tgl_awal, 8, 2);
                $tahun_print2 = substr($tgl_akhir, 0, 4);
                $bulan_print2 = substr($tgl_akhir, 5, 2);
                $tgl_print2 = substr($tgl_akhir, 8, 2);

                $tgl_awalx = $tgl_print . " " . $BulanIndo[(int) $bulan_print - 1] . " " . $tahun_print;
                $tgl_akhirx = $tgl_print2 . " " . $BulanIndo[(int) $bulan_print2 - 1] . " " . $tahun_print2;
                //echo $nm_problem;exit;
                $this->excel->getActiveSheet()->setTitle('' . $problem);
                $this->excel->getActiveSheet()->mergeCells('C3:D3');
                $this->excel->getActiveSheet()->setCellValue('C4', '' . $tgl_awalx . ' - ' . $tgl_akhirx);
                $this->excel->getActiveSheet()->mergeCells('C4:D4');
                $this->excel->getActiveSheet()->setCellValue('C5', '' . $problem);
                $this->excel->getActiveSheet()->mergeCells('C5:D5');
                $this->excel->getActiveSheet()->setCellValue('C6', '' . $total . ' Trouble Tickets');
                $this->excel->getActiveSheet()->mergeCells('C6:D6');
                $this->excel->getActiveSheet()->setCellValue('C7', '=AVERAGE(M11:M100)');
                $this->excel->getActiveSheet()->mergeCells('C7:D7');

                /* script menentukan tanggal */
                $tgls = date('j');

                /* script menentukan bulan */
                $array_bln = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                $bln = $array_bln[date('n')];

                /* script menentukan tahun */
                $thn = date('Y');
                /* script perintah keluaran */
                $sekarang = $tgls . " " . $bln . " " . $thn;
                //Footer
                $mark = $total + 17;
                $tgl = $total + 13;
                $this->excel->getActiveSheet()->getStyle('K' . $mark . ':L' . $tgl)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->setCellValue('L' . $mark, 'Manager Sales');
                $this->excel->getActiveSheet()->setCellValue('M' . $mark, 'Manager IT');

                $this->excel->getActiveSheet()->setCellValue('L' . $tgl, 'Jakarta, ' . $sekarang);
                $this->excel->getActiveSheet()->mergeCells('L' . $tgl . ':M' . $tgl);

                //Judul Kolom Looping
                $this->excel->getActiveSheet()->setCellValue('A9', 'No');
                $this->excel->getActiveSheet()->mergeCells('A9:A10');
                $this->excel->getActiveSheet()->setCellValue('B9', 'Requester');
                $this->excel->getActiveSheet()->mergeCells('B9:B10');
                $this->excel->getActiveSheet()->setCellValue('C9', 'Create Time');
                $this->excel->getActiveSheet()->mergeCells('C9:C10');
                $this->excel->getActiveSheet()->setCellValue('D9', 'Service Description');
                $this->excel->getActiveSheet()->mergeCells('D9:D10');
                $this->excel->getActiveSheet()->setCellValue('E9', 'TROUBLE  TICKET');
                $this->excel->getActiveSheet()->mergeCells('E9:H9');
                $this->excel->getActiveSheet()->setCellValue('E10', 'Request_ID');
                $this->excel->getActiveSheet()->setCellValue('F10', 'Request Type');
                $this->excel->getActiveSheet()->setCellValue('G10', 'Downtime');
                $this->excel->getActiveSheet()->setCellValue('H10', 'Uptime');
                $this->excel->getActiveSheet()->setCellValue('I10', 'Duration');
                $this->excel->getActiveSheet()->setCellValue('J9', 'Caused Code');
                $this->excel->getActiveSheet()->mergeCells('J9:J10');
                $this->excel->getActiveSheet()->setCellValue('K9', 'Resolution');
                $this->excel->getActiveSheet()->mergeCells('K9:K10');
                $this->excel->getActiveSheet()->setCellValue('L9', 'SLA (PKS %)');
                $this->excel->getActiveSheet()->mergeCells('L9:L10');
                $this->excel->getActiveSheet()->setCellValue('M9', 'Availability (%)');
                $this->excel->getActiveSheet()->mergeCells('M9:M10');
                $this->excel->getActiveSheet()->getStyle('A9:M10')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('A9:M10')->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('rgb' => '39b3d7')
                        )
                );
                $this->excel->getActiveSheet()->getStyle('A3:D7')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                $this->excel->getActiveSheet()->getStyle('A9:M10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                for ($col = ord('A'); $col <= ord('M'); $col++) {
                    $this->excel->getActiveSheet()->getColumnDimension(chr($col))->setAutoSize(true);
                    $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                    $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setWrapText(true);
                }

                //Query Menampilkan Data Looping
                $problems = $this->ekspor->get_Problem($problem, $tgl_awal, $tgl_akhir);

                //Looping Isi Table
                $i = 11;
                $no = 0;
                foreach ($problems as $row) {
                    $jam = $row['DURASI'];
					$jamkonvert = explode(":", $jam);
					$detik = $jamkonvert[0]*3600 + $jamkonvert[1] * 60 + $jamkonvert[2];
                    $detik_sebulan = $tanggal * 86400;
                    $hitung = $detik_sebulan - $detik;
                    $akhir = $hitung / $detik_sebulan * 100;
                    $hasil = round($akhir, 2);
                    
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->getStyle('A' . $i . ':' . 'M' . $i)->getFont()->setSize(8);
                    $this->excel->getActiveSheet()->getStyle('A' . $i . ':' . 'M' . $i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                    $this->excel->getActiveSheet()->setCellValue('A' . $i, $no + 1);
                    $this->excel->getActiveSheet()->setCellValue('B' . $i, $row['Requester']);
                    $this->excel->getActiveSheet()->setCellValue('C' . $i, $row['Created_Time']);
                    $this->excel->getActiveSheet()->setCellValue('D' . $i, $row['Subject']);
                    $this->excel->getActiveSheet()->setCellValue('E' . $i, $row['request_id']);
                    $this->excel->getActiveSheet()->setCellValue('F' . $i, $row['request_type']);
                    $this->excel->getActiveSheet()->setCellValue('G' . $i, $row['Down_Time']);
                    $this->excel->getActiveSheet()->setCellValue('H' . $i, $row['Uptime']);
                    $this->excel->getActiveSheet()->setCellValue('I' . $i, $row['DURASI']);
                    $this->excel->getActiveSheet()->setCellValue('J' . $i, $row['Root_Cause']);
                    $this->excel->getActiveSheet()->setCellValue('K' . $i, $row['Resolution']);
                    $this->excel->getActiveSheet()->setCellValue('L' . $i, '99,50');
                    $this->excel->getActiveSheet()->setCellValue('M' . $i, $hasil);
                    $i = $i + 1;
                    $no = $no + 1;
                }

                //Setting Output Excel
                $filename = '' . $problem . '_' . date('d/m/y') . '.xlsx'; //Nama File Berdasarkan Requester
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //Format Untuk .xlsx
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');

                ob_end_clean();
                //Output Untuk Excel 2007
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

                $objWriter->save('php://output');
            }
        }
    }

}

?>
