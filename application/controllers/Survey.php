<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Survey extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");



        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->helper("tgl_indo");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }

        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Datait_model","datait");
        $this->load->model("Survey_model","survey");
        $this->load->model("Genral_model","genral");
    }

    public function index() {
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));
        

        $judul = "Survey";
        $menu = $this->set_amenu("survey");
        
        $daftugas = $this->survey->tugas();
        $dafaktif = $this->survey->laporanpilih();
        $hirdat = $this->survey->dakhir();
        $dafstaff = $this->survey->staff();
        if($this->jabku==22 || $this->jabku==6 || $this->jabku==35){
            $dafrsv = $this->survey->semua();
        }elseif($this->jabku==2 || $this->jabku==29 || $this->jabku==96){
            $dafrsv = $this->survey->pilih($this->session->userdata('id_pengguna'));
        }elseif($this->jabku==7 || $this->jabku==30){
            $dafrsv = $this->survey->spvpil($this->session->userdata('id_pengguna'));
        
        }else{
            
            switch ($this->divku) {
              case 2:
                $dafrsv = $this->survey->semua();
                break;
              case 18:
                $a = array(6,35);
                $dafrsv = $this->survey->pilihin($a);
                break;
            }
        }
        
       
        
        if(isset($_GET["staff"])){

        $dafpilih = $this->survey->laporanstaff($_GET["staff"]);
        }


        if($this->divku == 18){
        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/header",compact("judul","menu"));
        }

        $this->load->view("share/survey/dashsurv",compact("dafrsv","hirdat","daftugas","dafaktif","dafstaff","dafpilih"));
        $this->load->view("panel/frames/footer");
    }


    private function set_amenu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "menu_eng"=>"",
            "harga_baru"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "talian"=>"",
            "menuotb"=>"",
            "lokaotb"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "aktifitas"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "inventory"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",
            "kaarea"=>"",
            "talian"=>"",           
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "meena"=>"",
            "metuj"=>"",
            "medel"=>"",
            "mesem"=>"",
            "mesep"=>"",
            "meseb"=>"",
            "ladload"=>""

        ];
        $menu[$active] = "active";
        return $menu;
    }


    public function petalokasi($id) {
        $datait_byid = $this->datait->odcbyid($id);
        $judul = "Lokasi Survey";
        $menu = $this->set_menu("Data Survey");

        
        $this->load->view("panel/survlokasi",compact("datait_byid"));
        
    }

    

    public function survwktu(){
        $post = $this->input->post();
        if(isset($post["btnSelesai"])) {
             
            $atifsls = $this->survey->atifselesai($post);
            if($atifsls != false) {
                redirect(base_url("survey/?succ"));
            } else {
                redirect(base_url("survey/survwktu/?err"));
            }
            exit();
        }

        $judul = "Survey Harian";
        $menu = $this->set_amenu("survey");
        $daftugas = $this->survey->tugas();
        $dafaktif = $this->survey->laporanpilih();
        $hirdat = $this->survey->dakhir();

        if($this->divku == 18){
        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("share/survey/survwktu",compact("hirdat","daftugas","dafaktif"));
        $this->load->view("panel/frames/footer");
    }


    public function survtamb() {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url("panel/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
             
            $svy = $this->survey->tambsvy();
            if($svy != false) {
                redirect(base_url("survey/?succ"));
            } else {
                redirect(base_url("survey/survtamb/?err"));
            }
            exit();
        }

        $judul = "Survey Baru";
        $menu = $this->set_amenu("survey");
        $dafrsv = $this->survey->semua();
        $daftugas = $this->survey->tugas();
        $dafcus = $this->survey->cusrhg();
        $dafaktif = $this->survey->laporan();
        
        if($this->divku == 18){
        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("share/survey/survtamb",compact("dafrsv","dafaktif","dafcus","daftugas"));
        $this->load->view("panel/frames/footer");
    }
    public function focalok($id) {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url("panel/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
             
            $svy = $this->survey->picsvy();
            if($svy != false) {
                redirect(base_url("survey/?succ"));
            } else {
                redirect(base_url("survey/focalok/?err"));
            }
            exit();
        }

        $judul = "Survey Baru";
        $menu = $this->set_amenu("survey");
        $pilsvy = $this->survey->survalok($id);
        $alopic = $this->survey->picfoc();

        if($this->divku == 18){
        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("share/survey/survalok",compact("pilsvy","alopic"));
        $this->load->view("panel/frames/footer");
    }

    public function hasur($id){
        $hap = $this->survey->pusvei($id);

            if($hap){
               $akt=$this->genral->tamakt('Hapus Survey ID #'.$id,$this->usrku);
               redirect(base_url("survey/?hsuc"));
            }else{
               redirect(base_url("survey/?herr"));
            }
   
    }

    public function survuplo($id) {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url("panel/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
             
            $svy = $this->survey->hassvy();
            if($svy != false) {
                redirect(base_url("survey/?succ"));
            } else {
                redirect(base_url("survey/survuplo/?err"));
            }
            exit();
        }

        $judul = "Survey Baru";
        $menu = $this->set_amenu("survey");
        $pilsvy = $this->survey->survalok($id);
        $alopic = $this->survey->picfoc();

        if($this->divku == 18){
        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("share/survey/survuplo",compact("pilsvy","alopic"));
        $this->load->view("panel/frames/footer");
    }

    public function survliat($id){

        $judul = "Survey Preview";
        $menu = $this->set_amenu("survey");
        $pilsvy = $this->survey->survalok($id);
        $alopic = $this->survey->picfoc();

        if($this->divku == 18){
        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("share/survey/survliat",compact("pilsvy","alopic"));
        $this->load->view("panel/frames/footer");
    }
}