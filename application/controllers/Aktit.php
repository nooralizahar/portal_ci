<?php


defined("BASEPATH") OR exit("Akses ditolak!");
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;




class Aktit extends CI_Controller {

    public function __construct() {
        parent::__construct();

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }

        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Datait_model","datait");
        $this->load->model("Aktif_model","aktif");
    }

    public function index() {
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));

        $post = $this->input->post();
        $thn=$this->input->post("thn");
        
        if(!empty($thn)){
            
            $datakt = $this->aktif->rekap($thn);
            
        }else{
            $thn=date('Y');
            $datakt = $this->aktif->rekap($thn);
        }
    

        $judul = "Aktifitas";
        $menu = $this->set_amenu("aprofil");
        $daftugas = $this->aktif->tugas();
        $dafaktif = $this->aktif->laporanpilih();
        $hirdat = $this->aktif->dakhir();
        $dafstaff = $this->aktif->staff();
        $datakt4m=$this->aktif->rekap4m($thn,0);    
        
        if(isset($_GET["staff"])){
        $kid=explode('-', $_GET["staff"]);
        $dafpilih = $this->aktif->laporanstaff($_GET["staff"]);
        $datakt4m = $this->aktif->rekap4m($thn,$kid[0]);
        }


        $this->load->view("panit/frames/hawal",compact("judul","menu"));     
        $this->load->view("panit/aktifi/dashatif",compact("thn","datakt4m","datakt","hirdat","daftugas","dafaktif","dafstaff","dafpilih"));
        $this->load->view("panit/frames/hakhi",compact("judul","menu"));

    }

    public function loadexcel() {
    $nafile=str_replace(' ', '', $this->session->userdata('nama_lengkap'));
    $naleng=$this->session->userdata('nama_lengkap');
    $dafaktif = $this->aktif->laporanpilih();
	/*$spreadsheet = new Spreadsheet();
	$sheet = $spreadsheet->getActiveSheet();
	$sheet->setCellValue('A1', 'Coba Lagi !');

	$writer = new Xlsx($spreadsheet);
	$writer->save('coba.xlsx');*/
	$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_akt.xlsx');

	$worksheet = $spreadsheet->getActiveSheet();
	$worksheet->getCell('B2')->setValue($naleng);
	$worksheet->getCell('B3')->setValue($this->session->userdata('id_div'));
	$worksheet->getCell('B4')->setValue($this->session->userdata('id_jabatan'));

	$n=6;
	foreach ($dafaktif as $a) {
	$worksheet->getCell('A'.$n)->setValue($a->date);
	$worksheet->getCell('B'.$n)->setValue($a->customer);
	$worksheet->getCell('C'.$n)->setValue($a->jobs);
	$worksheet->getCell('D'.$n)->setValue($a->case);
	$worksheet->getCell('E'.$n)->setValue($a->issues);	
	$worksheet->getCell('F'.$n)->setValue($a->action);
	$n++;	
	}


	//$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');

	//$writer->save('assets/temp/'.date('Ymd').'_'.$nafile.'DailyReport.xls');

	
	/*header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename='.date('Ymd').'_'.$nafile.'DailyReport.xls');
	header('Cache-Control: max-age=0');
	$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");*/
    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename='.date('Ymd').'_'.$nafile.'DailyReport.xls');
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



	redirect(base_url("aktit/"));

    }


    private function set_amenu($active) {
       
        $menu = [
            "dashboard"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "meiga"=>"",
            "ladload"=>""
        ];
        $menu[$active] = "active";
        return $menu;


    }


    public function petalokasi($id) {
        $datait_byid = $this->datait->odcbyid($id);
        $judul = "Lokasi Kerja";
        $menu = $this->set_menu("aprofil");

        
        $this->load->view("panit/lokasi",compact("datait_byid"));
        
    }

    


    public function atifwktu(){
        $post = $this->input->post();
        if(isset($post["btnSelesai"])) {
             
            $atifsls = $this->aktif->atifselesai($post);
            if($atifsls != false) {
                redirect(base_url("aktit/?succ"));
            } else {
                redirect(base_url("aktifitas/atifwktu/?err"));
            }
            exit();
        }

        $judul = "Aktifitas Harian";
        $menu = $this->set_amenu("aprofil");
        $daftugas = $this->aktif->tugas();
        $dafaktif = $this->aktif->laporanpilih();
        $hirdat = $this->aktif->dakhir();

        $this->load->view("panit/frames/header",compact("judul","menu"));
        $this->load->view("panit/aktifitas/atifwktu",compact("hirdat","daftugas","dafaktif"));
        $this->load->view("panit/frames/footer");
    }


    public function atiftamb() {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url("panit/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
             
            $atifspn = $this->aktif->atiftamb();
            if($atifspn != false) {
                redirect(base_url("aktifitas/atifwktu/?succ"));
            } else {
                redirect(base_url("aktifitas/atiftamb/?err"));
            }
            exit();
        }

        $judul = "Aktifitas Baru";
        $menu = $this->set_amenu("aktifitas");
        $daftugas = $this->aktif->tugas();
        $dafaktif = $this->aktif->laporan();
        

        $this->load->view("panit/frames/hawal",compact("judul","menu"));
        $this->load->view("panit/aktifi/atiftamb",compact("daftugas","dafaktif"));
        $this->load->view("panit/frames/hakhi");
    }

    public function atifubah($id = null) {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url("panit/"));
        
        $post = $this->input->post();
        if(isset($post["btnUbah"])) {
             
            $atifuspn = $this->aktif->atifubah();
            if($atifuspn != false) {
                redirect(base_url("aktifitas/?succ"));
            } else {
                redirect(base_url("aktifitas/atifubah/?err"));
            }
            exit();
        }

        $judul = "Aktifitas Ubah";
        $menu = $this->set_amenu("aktifitas");
        $dafakpil = $this->aktif->atifpil($id);
       
        

        $this->load->view("panit/frames/header",compact("judul","menu"));
        $this->load->view("panit/aktifitas/atifubah.php",compact("dafakpil"));
        $this->load->view("panit/frames/footer");
    }


}