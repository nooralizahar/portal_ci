<?php


defined("BASEPATH") OR exit("Akses ditolak!");
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;




class Salvenue extends CI_Controller {

    public function __construct() {
        parent::__construct();

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->helper("tgl_indo");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }

        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Datait_model","datait");
        $this->load->model("Harga_model","harga");
        $this->load->model("Wordata_model","wordata");
        $this->load->model("Laporan_model","laporan");
        $this->load->model("Eunvlas_model","eunvlas");
    }

    public function index($i=null) {

        $thnini= date('Y');
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));
        $harini= date('Y-m-d');
        $harlal= date('Y-m-d',strtotime("-1 day"));


        /*$dafcro=$this->harga->gcro();
        $dafcropv=$this->harga->gcropv();
        $dafreq=$this->harga->gpsaleso();
        $dafrsamg=$this->harga->gsalesmg();
        $dafrsa=$this->harga->gsales();
        $dafrsapv=$this->harga->gsalespv();
        $dafmua=$this->harga->semua();
        $dafnmc=$this->harga->semnmc();
        $persal=$this->harga->pilpersal($blnini);
        $persall=$this->harga->pilpersal($blnlal);
        $persegi=$this->harga->pilperseg($blnini);
        $persegl=$this->harga->pilperseg($blnlal);
        $woblni=$this->harga->pilwo($blnini);
        $woblnl=$this->harga->pilwo($blnlal);
        $summary=$this->harga->summary();
        $summarypre=$this->harga->summarypre();
        $summarydur=$this->harga->summarydur();
        $wosum=$this->harga->wosummary();
        $wosal=$this->harga->wosales();
        $woseg=$this->harga->wosegme();
        $wosta=$this->harga->wostatu();
        $dafaktif = $this->datait->laporanpilih();*/

        $revdat=$this->eunvlas->vhilip();
        $dafimp=$this->eunvlas->dafile();


        $judul = "Sales Revenue";
        $menu = $this->set_amenu("salesrevenue");


        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/eunever/eunever",compact("i","revdat","dafimp")); //,"wosta","woseg","wosal","dafcro","dafcropv","wosum","woblni","woblnl","dafnmc","dafaktif","dafreq","dafmua","dafrsa","dafrsapv","dafrsamg","persal","persall","persegi","persegl","summary","summarypre","summarydur"
        $this->load->view("panel/frames/footer");

    }

    public function supah($i=null) {
            $pus = $this->eunvlas->vsupah($i);

            if($pus)
                redirect(base_url("salvenue/?hsu"));
            else
                redirect(base_url("salvenue/?her"));
    }

    public function vurab() {
        $thnini= date('Y');

        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $salv = $this->eunvlas->napmis();
            if($salv != false) {
                redirect(base_url("salvenue/?suc"));
            } else {
                redirect(base_url("salvenue/vurab/?err"));
            }
            exit();
        }
        $daftar_segment =$this->harga->segment_semua(1);
        $dafwor=$this->wordata->woplist();
        
        $judul = "Sales Revenue - Baru";
        $menu = $this->set_amenu("salesrevenue");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/eunever/barvnue",compact("dafwor","daftar_segment"));
        $this->load->view("panel/frames/footer");
    }

    public function ropmi() {
        $post = $this->input->post();

        if(isset($post["btnImport"])) {
             
            $pmi = $this->eunvlas->ulosuke();
            $this->imxls2tsk();
            if($pmi != false) {
                redirect(base_url("salvenue/?isu"));
            } else {
                redirect(base_url("salvenue/?ier"));
            }
            exit();
        }
    }

    function imxls2tsk()
    {
        $this->load->library('excel');
        $hrini=date('Y-m-d H:i:s');

        $lf=$this->eunvlas->lfile();

        foreach ($lf as $v) {
            $idf=$v->idupl;
            $naf=json_decode($v->nafi);
        }
        foreach($naf as $item){
            $rfile=$item->file;
        }

        if(isset($rfile)){
            $path = "assets/uploads/fileim/".$rfile; //$_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $wo = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $se = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $tt = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $tm = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $ts = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $sv = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $ve = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $ni = $worksheet->getCellByColumnAndRow(7, $row)->getValue();

                    $data[] = array(
                        'idr'    =>  $wo,
                        'seg'    =>  $se,
                        'tgl'    =>  $tt,
                        'tgm'    =>  $tm,
                        'tgs'    =>  $ts,
                        'ser'    =>  $sv,
                        'ven'    =>  $ve,
                        'nil'    =>  $ni,
                        'tgi'    =>  $hrini,
                        'usi'    =>  $this->session->userdata('id_pengguna'),
                        'idi'    =>  $idf

                    );
                }
            }
            $this->eunvlas->insertdatxls($data);
            $jum=$highestRow-1;
            echo $jum.' data berhasil di import';
        }   
    }







    private function set_amenu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "menu_eng"=>"",
            "harga_baru"=>"",
            "salesrevenue"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "talian"=>"",
            "menufoc"=>"",
            "menuotb"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "aktifitas"=>"",
            "surat_terkirim"=>"",
            "disposisi_keluar"=>"",
            "disposisi_masuk"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "mascidsid"=>"",
            "inventory"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }



}