<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Focmen extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        $this->dbz = $this->load->database('newdb', TRUE); // Koneksi ke Postgres iBoss
        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("general");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }
        
        $this->load->library('phpqrcode/qrlib');
        $this->load->helper('url');
        $this->load->model("Odpdata_model","odpdata");
        $this->load->model("Odcdata_model","odcdata");
        $this->load->model("Iboss_model","iboss");
        $this->load->model("Lokasi_model","lokasi");
        $this->load->model("Genral_model","genral");

    }

    public function index() {
   

        $judul = "Menu FOC";
        $menu = $this->set_amenu("menufoc");


        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/menufo");
        $this->load->view("panel/frames/footer");
    }

    public function odcst($st,$id) {
        if($st==1){
            $stupd = array(
            'st_cek' => 1,
            'us_cek' => $this->session->userdata('username'),
            'tg_cek' =>date('Y-m-d H:i:s')
          ); 
        $akt=$this->genral->tamakt('Ubah status ODC ID '.$id.' jadi valid.',$this->usrku);            
        $this->genral->rimsan(1);
        }else{
            $stupd = array(
            'st_cek' => 0,
            'us_cek' => $this->usrku,
            'tg_cek' =>date('Y-m-d H:i:s')
          );            
        $akt=$this->genral->tamakt('Ubah status ODC ID '.$id.' jadi not checked.',$this->usrku);
        $this->genral->rimsan(1);
        }
        $this->db->where('idodc',$id)->update('t_odc',$stupd);    
        redirect(base_url('focmen/odcin/'));
        
    }

    public function odpdlo() {


        $repodp = $this->iboss->odpdolo();
        
            if($repodp != false) {
                redirect(base_url("focmen/odcin/?dsuc"));
            } else {
                redirect(base_url("focmen/odcin/?derr"));
            }
            exit();
    }

    public function oltdlo() {


        $repolt = $this->iboss->oltdolo();
        
            if($repolt != false) {
                redirect(base_url("focmen/odcin/?osuc"));
            } else {
                redirect(base_url("focmen/odcin/?oerr"));
            }
            exit();
    }


    public function odpdet($id) {


        $dafpel = $this->iboss->odpdetil($id);
        
        $judul = "ODP".$id." - Detil Pelanggan";
        $menu = $this->set_amenu("daftarodp");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odpli",compact("dafpel","id"));
        $this->load->view("panel/frames/footer");
    }

    public function detpel($id) {

     if($id==3){
         $tilpel = $this->iboss->detpel('ACCST-03'); 
         $ket='Terminate';  
     }elseif($id==2){
         $tilpel = $this->iboss->detpel('ACCST-02');
         $ket='Blocked'; 
     }elseif($id==1){
         $tilpel = $this->iboss->detpel('ACCST-01');
         $ket='Active';    
     }
        
        
        $judul = "Detil Pelanggan ".$ket;
        $menu = $this->set_amenu("dashboard");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/peldet",compact("tilpel","id","ket"));
        $this->load->view("panel/frames/footer");
    }

    public function odprek() {
        $thnini= date('Y');
        //iboss rekapitulasi
        $odpthn=$this->iboss->detodpthn();
        $odplok=$this->iboss->detodplok($thnini);

        $judul = "Rekapitulasi";
        $menu = $this->set_amenu("rekapodp");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odpre",compact("odpthn","odplok"));
        $this->load->view("panel/frames/footer");
    }


    public function odced($id) {

        

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $ubh = $this->odcdata->ubah();
            if($ubh != false) {
                redirect(base_url("focmen/odcin/?succ"));
            } else {
                redirect(base_url("focmen/odced/?err"));
            }
            exit();
        }


        $dafodc = $this->odcdata->pilih($id);
        //$dafolt = $this->odpdata->oltsem();
        $daflok = $this->lokasi->semlok();

        $judul = "FTTH - Edit ODC";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odced",compact("daflok","dafodc"));
        $this->load->view("panel/frames/footer");
    }

    public function odcin() {    

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $tbh = $this->odcdata->tambah();
            if($tbh != false) {
                redirect(base_url("focmen/odcin/?succ"));
            } else {
                redirect(base_url("focmen/odcin/?err"));
            }
            exit();
        }


        $odcsem = $this->odcdata->semua();
        $dafolt = $this->lokasi->semolt();
        $daflok = $this->lokasi->semlok();

        $judul = "FTTH - Optical Distribution Cabinet";
        $menu = $this->set_amenu("daftaodc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odcin",compact("dafolt","odcsem","daflok"));
        $this->load->view("panel/frames/footer");
    }

    public function odcli($id) {

        

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $ubh = $this->odcdata->tamodc();
            if($ubh != false) {
                redirect(base_url("focmen/odcli/".$post["idodc"]."?succ"));
            } else {
                redirect(base_url("focmen/odcli/".$post["idodc"]."?err"));
            }
            exit();
        }

        $dafodp = $this->odcdata->odpdet($id);
        $dafpel = $this->odcdata->pilih($id);
        $semodp = $this->odpdata->odpcbx();

        
        $judul = "ODC - Detil Data ODP";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odcli",compact("semodp","dafodp","dafpel","id"));
        $this->load->view("panel/frames/footer");
    }

    public function odcol($id) {

        

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $ubh = $this->odcdata->tamolt();
            if($ubh != false) {
                redirect(base_url("focmen/odcol/".$post["idodc"]."?succ"));
            } else {
                redirect(base_url("focmen/odcol/".$post["idodc"]."?err"));
            }
            exit();
        }

        $detolt = $this->odcdata->oltdet($id);
        $dafpel = $this->odcdata->pilih($id);
        $semodp = $this->odpdata->odpcbx();
        $dafolt = $this->lokasi->semoltc();

        $judul = "ODC - Detil Data OLT";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odcol",compact("dafolt","semodp","detolt","dafpel","id"));
        $this->load->view("panel/frames/footer");
    }

    public function oltha($idc=null,$idt=null){
        $hap = $this->odcdata->rmolt($idt);

            if($hap)
                redirect(base_url("focmen/odcol/".$idc."?hsuc"));
            else
                redirect(base_url("focmen/odcol/".$idc."?herr"));
   
    }


    public function odcha($id){
        $hap = $this->odcdata->hapus($id);

            if($hap)
                redirect(base_url("focmen/odcin/?hsucc"));
            else
                redirect(base_url("focmen/odcin/?herr"));
   
    }

    public function rm_odp($id,$idc){

        $hap = $this->odcdata->to_odp($id);

            if($hap)
                redirect(base_url("focmen/odcli/".$idc."?hsucc"));
            else
                redirect(base_url("focmen/odcli/".$idc."?herr"));
   
    }


    public function odpin() {

        

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $tbh = $this->odpdata->tambah();
            if($tbh != false) {
                redirect(base_url("focmen/odpin/?succ"));
            } else {
                redirect(base_url("focmen/odpin/?err"));
            }
            exit();
        }

        $odpsem = $this->iboss->datodp();
        //$odpsem = $this->odpdata->semua();
        //$dafolt = $this->odpdata->oltsem();
        //$daflok = $this->lokasi->semlok();

        $judul = "FTTH - Optical Distribution Point";
        $menu = $this->set_amenu("daftaodp");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        //$this->load->view("panel/founem/odpin",compact("odpsem","dafolt","daflok"));
        $this->load->view("panel/founem/odpib",compact("odpsem"));
        $this->load->view("panel/frames/footer");
    }

    public function odpinew() {      

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $tbh = $this->odpdata->tambah();
            if($tbh != false) {
                redirect(base_url("focmen/odpinew/?succ"));
            } else {
                redirect(base_url("focmen/odpinew/?err"));
            }
            exit();
        }

        $nama = $this->input->post('search');
        if(empty($nama)){
        $odpcar = 0;
        }else{
        $odpcar = $this->odpdata->cari($nama);
        }
        $dafolt = $this->odpdata->oltsem();
        $daflok = $this->lokasi->semlok();

        $judul = "FTTH - Optical Distribution Point";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odpinew",compact("odpcar","dafolt","daflok"));
        $this->load->view("panel/frames/footer");
    }

        public function odpha($id){
        $hap = $this->odpdata->odphapus($id);

            if($hap)
                redirect(base_url("focmen/odpin/?hsucc"));
            else
                redirect(base_url("focmen/odpin/?herr"));
   
    }

    public function oltin() {

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $tbh = $this->odpdata->olttam();
            if($tbh != false) {
                redirect(base_url("focmen/oltin/?succ"));
            } else {
                redirect(base_url("focmen/oltin/?err"));
            }
            exit();
        }


        $oltsem = $this->odpdata->oltsem();
       // $pelsemua = $this->laporan->pelanggan();
        
        $judul = "OLT - Optical Line Termination";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/oltin",compact("oltsem"));
        $this->load->view("panel/frames/footer");
    }


    public function odcmap($ref=null) {


        $oltdat = $this->odpdata->odcref($ref);
        
        $judul = "Aplikasi Portal - Detil ODC ";

        $menu = $this->set_amenu("daftaodc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odcma",compact("oltdat"));
        $this->load->view("panel/frames/footer");
    }

    public function oltmap($ref=null) {


        $oltdat = $this->odpdata->oltref($ref);
        
        $judul = "Aplikasi Portal - Jalur OLT ";

        $menu = $this->set_amenu("daftaodc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/oltma",compact("oltdat"));
        $this->load->view("panel/frames/footer");
    }


    public function odped($id) {

        

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $ubh = $this->odpdata->ubah();
            if($ubh != false) {
                redirect(base_url("focmen/odpin/?succ"));
            } else {
                redirect(base_url("focmen/odped/?err"));
            }
            exit();
        }


        $dafodp = $this->odpdata->pilih($id);
        $dafolt = $this->odpdata->oltsem();
        $daflok = $this->lokasi->semlok();

        $judul = "FOC - Edit ODP";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odped",compact("daflok","dafodp","dafolt"));
        $this->load->view("panel/frames/footer");
    }

    public function odpli($id) {

        

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $ubh = $this->odpdata->tampel();
            if($ubh != false) {
                redirect(base_url("focmen/odpli/".$post["idodp"]."?succ"));
            } else {
                redirect(base_url("focmen/odpli/".$post["idodp"]."?err"));
            }
            exit();
        }


        $dafpel = $this->odpdata->pelanggan($id);
       // $pelsemua = $this->laporan->pelanggan();
        
        $judul = "ODP - Daftar Pelanggan";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odpli",compact("dafpel","id"));
        $this->load->view("panel/frames/footer");
    }

    public function odpqr($id) {

        $godp = $this->odpdata->pilih($id);
        foreach($godp as $d): 
        $teks=$d->nama;   
        endforeach;

        //$teks = $this->input->post('qrcode_text');
        $qrtext = str_replace(' ','', $teks);
        $qrodp = md5($qrtext);
        
        if(isset($qrtext))
        {

            //file path for store images
            $SERVERFILEPATH = $_SERVER['DOCUMENT_ROOT'].'/assets/qrfiles/';
           
            $text = $qrtext;
            //$text1= substr($text, 4,7);
            $text1= $id;
            
            $folder = $SERVERFILEPATH;
            $file_name1 = "qr".$text1."e".rand(2,999). ".png";
            $file_name = $folder.$file_name1;
            QRcode::png($qrodp,$file_name); //$text

            if($file_name1!=''){
                $this->odpdata->updqr($id,$qrodp,$file_name1);
                redirect(base_url("focmen/odpin/"));

            }else{
                //echo"<center><img src=".'http://150.107.140.204/siratpa/assets/qrfiles/'.$file_name1."></center";
            }
            //redirect(base_url("panel/ratsuk/?succ"));

            
        }
        else
        {
            echo 'No Text Entered';
        }   


    }

    public function odpce($id) {

        $dafodp = $this->odpdata->pilih($id);
       
        
        $judul = "FOC - Cetak ODP";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odpce",compact("dafodp"));
        $this->load->view("panel/frames/footer");
    }

    public function odpcs($p=null) {


        if($p==1){
        $dafpel = $this->odpdata->pelodpset();
        }else{
        $dafpel = $this->odpdata->pelsemua();  
        }

        
       // $pelsemua = $this->laporan->pelanggan();
        
        $judul = "ODP - Daftar Pelanggan";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odpcs",compact("dafpel"));
        $this->load->view("panel/frames/footer");
    }

    public function odpcsn($p=null) {


        if($p==1){
        $dafpel = $this->odpdata->pelodpset();
        }elseif($p==2){
        $dafpel = $this->odpdata->pelsemua(); 
        }elseif($p==3){
        $pilih = $this->input->post('pilih');
        $param = $this->input->post('keyword');
        $dafpel = $this->odpdata->pilpel($pilih,$param);
        }else{
        $dafpel = $this->odpdata->pilpel('pelanggan','*');

        }


        
       

        
        $judul = "ODP - Daftar Pelanggan";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odpcsn",compact("dafpel","p"));
        $this->load->view("panel/frames/footer");
    }



    public function peled($id) {

        

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $ubh = $this->odpdata->ubahp();
            if($ubh != false) {
                redirect(base_url("focmen/odpcsn/?succ"));//.$post["idodp"]."?succ"));
            } else {
                redirect(base_url("focmen/peled/?err"));
            }
            exit();
        }


        $dafpel = $this->odpdata->pilihp($id);
        $lodp = $this->odpdata->semua();
       // $pelsemua = $this->laporan->pelanggan();
        
        $judul = "ODP - Edit Pelanggan";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/peled",compact("dafpel","lodp"));
        $this->load->view("panel/frames/footer");
    }




    public function pelha($idp) {
        $godp = $this->odpdata->pilihp($idp);
        foreach($godp as $d): 
        $id=$d->idodp;   
        endforeach;
        
        $hap= $this->odpdata->hapusp($idp);
            if($hap != false) {
                redirect(base_url("focmen/odpli/".$id."/?hsucc"));
            } else {
                redirect(base_url("focmen/odpli/".$id."?err"));
            }
            exit();

        $dafpel = $this->odpdata->pelanggan($id);
       // $pelsemua = $this->laporan->pelanggan();
        
        $judul = "ODP - Daftar Pelanggan";
        $menu = $this->set_amenu("menufoc");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/founem/odpli",compact("dafpel","id"));
        $this->load->view("panel/frames/footer");
  

    }


    private function set_amenu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "menu_eng"=>"",
            "harga_baru"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "talian"=>"",
            "menufoc"=>"",
            "menuotb"=>"",
            "lokaotb"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "aktifitas"=>"",
            "surat_terkirim"=>"",
            "disposisi_keluar"=>"",
            "disposisi_masuk"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "inventory"=>"",
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }


    public function peta($id) {
        $datodp = $this->iboss->piltikor($id);
        $judul = "Lokasi ODP";
        $menu = $this->set_amenu("menufoc");
        
        $this->load->view("panel/founem/odplo",compact("datodp"));
        
    }

    public function petap($id) {
        $datpel = $this->odpdata->pilihp($id);
        $judul = "Lokasi Pelanggan";
        $menu = $this->set_amenu("menufoc");
        
        $this->load->view("panel/founem/pello",compact("datpel"));
        
    }

    public function lopel($id) {
        $datpel = $this->odpdata->pelanggan($id);
        $odp = $this->odpdata->selodp($id);
        foreach($odp as $d): 
        $nodp=$d->nama; 
        $kodp=$d->kordinat;
        $aodp=$d->alamat; 
        $sodp=$d->sisa;  
        endforeach;
        $judul = "Lokasi Pelanggan";
        $menu = $this->set_amenu("menufoc");
        
        $this->load->view("panel/founem/lopel",compact("datpel","nodp","kodp","aodp","sodp"));
        
    }

    public function jaodp() {
        $pilih = $this->input->post('pilih');
        $param = $this->input->post('keyword');
        $datodp = $this->iboss->pilodp($pilih,$param);
        $pusat= $this->iboss->pilsat($pilih,$param);
        $judul = "Peta ODP";
        $menu = $this->set_amenu("menufoc");
        
        $this->load->view("panel/founem/jaodp",compact("datodp","param","pusat"));
        //$this->load->view("panel/founem/cekjaodp",compact("datodp","param"));
        
    }


    public function jaodc() {
        $pilih = $this->input->post('pilih');
        $param = $this->input->post('keyword');
        $datodc = $this->odcdata->pilodc($pilih,$param);
        $pusat= $this->odcdata->pilsat($pilih,$param);
        $judul = "Peta ODC";
        $menu = $this->set_amenu("menufoc");
        
        $this->load->view("panel/founem/jaodc",compact("datodc","param","pusat","pilih"));
        //$this->load->view("panel/founem/cekjaodp",compact("datodp","param"));
        
    }


}