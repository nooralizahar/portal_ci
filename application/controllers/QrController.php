<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class QrController extends CI_Controller {

	function __construct ()
	{	
		parent::__construct();
		$this->load->library('phpqrcode/qrlib');
		$this->load->helper('url');
	}

	public function index()
	{	
		$this->load->view('layout/hawal.php');
		$this->load->view('qrview/hqrcode.php');
		$this->load->view('layout/hakhir.php');
	}

	public function qrcodeGenerator ( )
	{

		$qrtext = str_replace(' ','_', $this->input->post('qrcode_text'));
		
		if(isset($qrtext))
		{

			//file path for store images
		    $SERVERFILEPATH = $_SERVER['DOCUMENT_ROOT'].'/assets/qrfiles/';
		   
			$text = $qrtext;
			$text1= substr($text, 0,9);
			
			$folder = $SERVERFILEPATH;
			$file_name1 = $text1."-qr" . rand(2,200) . ".png";
			$file_name = $folder.$file_name1;
			QRcode::png($text,$file_name);

			if($file_name1!=''){
				redirect(base_url("QrController/?r=".$file_name1.""));

			}else{
				echo"<center><img src=".base_url('assets/qrfiles/').$file_name1."></center>";
			}
			//redirect(base_url("panel/ratsuk/?succ"));

			
		}
		else
		{
			echo 'No Text Entered';
		}	
	}
}
