<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scriptest extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
        $this->load->model("Otbdata_model","otbdat");
	}
	
	public function index(){

        $judul = "Script Test";
        $this->load->view("script/hal/hawal",compact("judul"));
        $this->load->view("script/htama");
        $this->load->view("script/hal/hakhi");

	}

	public function scpeta(){

		$dotjc = $this->otbdat->petapil(21);
		$potjc = $this->otbdat->pilcent(21);

        $this->load->view("script/hpeta",compact("dotjc","potjc"));


	}
	

}
