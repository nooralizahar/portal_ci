<?php


defined("BASEPATH") OR exit("Akses ditolak!");

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Panit extends CI_Controller {

    var $uriku;

    public function __construct() {
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
        $this->dbe = $this->load->database('dbbaru', TRUE);
        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("general");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->library('pdf');
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }
        $this->load->model("excel_import_model");
        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Hris_model","hris");
        $this->load->model("Huroga_model","huroga");
        $this->load->model("Aktif_model","aktif");
        $this->load->model("Talian_model","talian");

    }

    public function index() {
        
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));

        $judul = "Dashboard - Information Technology";
        $menu = $this->set_menu("dashboard");

       
        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/index",compact("blnini"));
        $this->load->view($this->uriku."/frames/hakhi");
    }

    public function talian() {

        $post = $this->input->post();
        $idta=$this->input->post('idta');

        if(isset($post["btnSimpan"])) {
            $sim = $this->talian->simtal();
            if($sim != false) {
                $akt=$this->pengguna->tamakt('Permintaan Baru No. PR '.$idta,$this->session->userdata('username'));
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data berhasil disimpan </div>'); 
                redirect(base_url($this->uriku."/sirang/".$idta));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data gagal disimpan </div>');  
                redirect(base_url($this->uriku."/sirang/".$idta."?err"));
            }
            exit();
        }

        $dapr=$this->talian->pureku(); // user request
        $sepr=$this->talian->dapure(); // semua request
   
        $judul = "Permintaan Pembelian";
        $menu = $this->set_menu("talian");


        if($this->db->get('app_latrop_nailat')->num_rows()==0){
        $nopr=1;    
        }else{
        $np=$this->db->order_by('idta','desc')->limit(1)->get('app_latrop_nailat')->row();
        $nopr=$np->idta+1;
        }

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view("share/nailat/taliru",compact("nopr","dapr","sepr"));
        $this->load->view($this->uriku."/frames/hakhi");
    }

    
    public function sirang($id) {

        $post = $this->input->post();
        
        if(isset($post["btnSimpan"])) {
            $sim = $this->talian->tambar();
            if($sim != false) {
                
                $akt=$this->pengguna->tamakt('Tambah Barang'.$this->input->post('jeba').' '.$this->input->post('jeba').' No. PR '.$id,$this->session->userdata('username'));

                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data berhasil ditambahkan </div>'); 
                redirect(base_url($this->uriku."/sirang/".$id));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data gagal ditambahkan </div>');
                redirect(base_url($this->uriku."/sirang/".$id));
            }
            exit();
        }

        $jeba=$this->talian->cbjeba();
        $daba=$this->talian->darang($id);
        $depr=$this->talian->depure($id);

        $judul = "Detil Pembelian";
        $menu = $this->set_menu("talian");

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view("share/nailat/sirang",compact("id","jeba","daba","depr"));
        $this->load->view($this->uriku."/frames/hakhi");
    }

    public function harang($id) {

        $h = $this->talian->pusbar($id);
            if($h){
            $akt=$this->pengguna->tamakt('Hapus barang id# '.$idta,$this->session->userdata('username'));
            $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data berhasil dihapus </div>');
            }else{
            $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data gagal dihapus </div>');
            }  

            redirect(base_url($this->uriku."/sirang/".$id));
    }

    public function dafset($id = null) {

        $post = $this->input->post();


        if(isset($post["btnSimpan"])) {
            $sim = $this->huroga->isidafset();
            if($sim != false) {
                $silog= $this->input->post('jeasse').'_'.$this->input->post('measse');
                $akt=$this->pengguna->tamakt('simpan asset '.$silog,$this->session->userdata('username'));
                redirect(base_url($this->uriku."/dafset/?suc"));
            } else {
                redirect(base_url($this->uriku."/dafset/?err"));
            }
            exit();
        }

        $pemasok=$this->huroga->cbpemasok();
        $lokasi=$this->huroga->cblokasi();
        $jenis=$this->huroga->cbjenis();
        $merk=$this->huroga->cbmerk();
        $user=$this->hris->cbuser();
        $dfasset=$this->huroga->daftar();
        
        $judul = "Aplikasi Portal - Daftar Asset";
        $menu = $this->set_menu("menol");

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/dasset/tessad",compact("dfasset","pemasok","lokasi","jenis","merk","user"));
        $this->load->view($this->uriku."/frames/hakhi");

    }

    public function pusset($i=null){
        $h = $this->huroga->pusdafset($i);
            if($h)
                redirect(base_url($this->uriku."/dafset/?hsuc"));
            else
                redirect(base_url($this->uriku."/dafset/?herr"));       
    }

    public function profilku($id = null) {
        //if($this->session->userdata("id_jabatan") != 1)
            //redirect(base_url($this->uriku."/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->editpro();
            if($edt != false) {
                $akt=$this->pengguna->tamakt('ubah sandi ',$this->session->userdata('username'));
                redirect(base_url($this->uriku."/profilku/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url($this->uriku."/profilku/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        $judul = "Aplikasi Portal - Ubah Sandi";
        $menu = $this->set_menu("uprofil");

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
            $this->load->view($this->uriku."/person/profilku",compact("pengguna"));
            $this->load->view($this->uriku."/frames/hakhi");
        } else redirect(base_url($this->uriku."/pengguna/"));
    }

    public function profilvi($id = null) {
        if($this->session->userdata("id_pengguna") != $id)
            redirect(base_url($this->uriku."/"));

        $judul = "Aplikasi Portal - Profil Pengguna";
        $menu = $this->set_menu("profil");
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
            $this->load->view($this->uriku."/person/profilkv",compact("pengguna","daftar_jabatan","daftar_divisi","daftas"));
            $this->load->view($this->uriku."/frames/hakhi");
        } else redirect(base_url($this->uriku."/pengguna/"));
    }


    public function profilak() {
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));

        $post = $this->input->post();
        $thn=$this->input->post("thn");
        
        if(!empty($thn)){
            
            $datakt = $this->aktif->rekap($thn);
            
        }else{
            $thn=date('Y');
            $datakt = $this->aktif->rekap($thn);
        }
    

        $judul = $this->session->userdata('nm_div')." Department - Aktifitas";
        $menu = $this->set_menu("aprofil");
        $daftugas = $this->aktif->tugas();
        $dafaktif = $this->aktif->laporanpilih();
        $hirdat = $this->aktif->dakhir();
        $dafstaff = $this->aktif->staff();
        $datakt4m=$this->aktif->rekap4m($thn,0);    
        
        if(isset($_GET["staff"])){
        $kid=explode('-', $_GET["staff"]);
        $dafpilih = $this->aktif->laporanstaff($_GET["staff"]);
        $datakt4m = $this->aktif->rekap4m($thn,$kid[0]);
        }


        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/aktifi/dashatif",compact("thn","datakt4m","datakt","hirdat","daftugas","dafaktif","dafstaff","dafpilih"));
        $this->load->view($this->uriku."/frames/hakhi",compact("judul","menu"));

    }

    public function atiftamb() {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url($this->uriku."/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
             
            $atifspn = $this->aktif->atiftamb();
            if($atifspn != false) {
                $akt=$this->pengguna->tamakt('simpan aktifitas baru ',$this->session->userdata('username'));
                redirect(base_url($this->uriku."/atifwktu/?succ"));
            } else {
                redirect(base_url($this->uriku."/atiftamb/?err"));
            }
            exit();
        }

        $judul = "Aplikasi Portal - Aktifitas Baru";
        $menu = $this->set_menu("aprofil");
        $daftugas = $this->aktif->tugas();
        $dafaktif = $this->aktif->laporan();
        

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/aktifi/atiftamb",compact("daftugas","dafaktif"));
        $this->load->view($this->uriku."/frames/hakhi");
    }

    public function atifwktu(){
        $post = $this->input->post();
        if(isset($post["btnSelesai"])) {
             
            $atifsls = $this->aktif->atifselesai($post);
            if($atifsls != false) {
                redirect(base_url($this->uriku."/profilak/?succ"));
            } else {
                redirect(base_url($this->uriku."/atifwktu/?err"));
            }
            exit();
        }

        $judul = "Aplikasi Portal - Aktifitas Harian";
        $menu = $this->set_menu("aprofil");
        $daftugas = $this->aktif->tugas();
        $dafaktif = $this->aktif->laporanpilih();
        $hirdat = $this->aktif->dakhir();

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/aktifi/atifwktu",compact("hirdat","daftugas","dafaktif"));
        $this->load->view($this->uriku."/frames/hakhi");
    }

    public function atifubah($id = null) {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url($this->uriku."/"));
        
        $post = $this->input->post();
        if(isset($post["btnUbah"])) {
             
            $atifuspn = $this->aktif->atifubah();
            if($atifuspn != false) {
                $akt=$this->pengguna->tamakt('ubah aktifitas ',$this->session->userdata('username'));
                redirect(base_url($this->uriku."/profilak/?succ"));
            } else {
                redirect(base_url($this->uriku."/atifubah/?err"));
            }
            exit();
        }

        $judul = "Aktifitas Ubah";
        $menu = $this->set_menu("aprofil");
        $dafakpil = $this->aktif->atifpil($id);
       
        

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/aktifi/atifubah",compact("dafakpil"));
        $this->load->view($this->uriku."/frames/hakhi");
    }


    public function logout() {
        $akt=$this->pengguna->tamakt('logout berhasil',$this->session->userdata('username'));
        $this->session->sess_destroy();
        redirect(base_url("login/"));
    }


    function imp2db()
    {
        $this->load->library('excel');

        $lf=$this->excel_import_model->lfile();

        foreach ($lf as $v) {
        	$idf=$v->id;
        	$naf=json_decode($v->nafi);
        }
        foreach($naf as $item){
        	$rfile=$item->file;
        }

        if(isset($rfile)){
            $path = "assets/uploads/tsalb/".$rfile; //$_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $id = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $us = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $na = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $pr = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $al = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $em = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $hp = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $re = $worksheet->getCellByColumnAndRow(7, $row)->getValue();


                    $data[] = array(
                        'id'        =>  $id,
                        'idfile'    =>  $idf,
                        'username'  =>  $us,
                        'nama'      =>  $na,
                        'produk'    =>  $pr,
                        'alamat'    =>  $al,
                        'email'     =>  $em,
                        'hp'        =>  $hp,
                        'registrasi'=>  $re
                    );
                }
            }
            $this->excel_import_model->insertblast($data);
            $jum=$highestRow-1;
            echo $jum.' data berhasil di import';
        }   
    }

    public function toexcel($pil=null) {
    $nafile=$pil;
    //$spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    //$spreadsheet->getDefaultStyle()->getFont()->setSize(8);
    $dafphi = $this->bayar->cekdata($pil);
    $dafrin = $this->bayar->paymdet($pil);
    /*$spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'Coba Lagi !');

    $writer = new Xlsx($spreadsheet);
    $writer->save('coba.xlsx');*/
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_phi.xlsx');
    $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(11);
    $worksheet = $spreadsheet->getActiveSheet();

    //$worksheet->getCell('B2')->setValue($naleng);
    //$worksheet->getCell('B3')->setValue($this->session->userdata('id_div'));
    //$worksheet->getCell('B4')->setValue($this->session->userdata('id_jabatan'));
    $no=1;
    $n=3;
    foreach ($dafphi as $h) {
    $worksheet->getCell('A'.$n)->setValue($no++);
    $worksheet->getCell('B'.$n)->setValue($h->username);
    $worksheet->getCell('C'.$n)->setValue(substr($h->nama,0,6));
    $worksheet->getCell('D'.$n)->setValue($h->thn);
    $worksheet->getCell('E'.$n)->setValue($h->JAN);  
    $worksheet->getCell('F'.$n)->setValue($h->FEB);
    $worksheet->getCell('G'.$n)->setValue($h->MAR);
    $worksheet->getCell('H'.$n)->setValue($h->APR);
    $worksheet->getCell('I'.$n)->setValue($h->MEI);
    $worksheet->getCell('J'.$n)->setValue($h->JUN);
    $worksheet->getCell('K'.$n)->setValue($h->JUL);
    $worksheet->getCell('L'.$n)->setValue($h->AGU);  
    $worksheet->getCell('M'.$n)->setValue($h->SEP);
    $worksheet->getCell('N'.$n)->setValue($h->OKT);
    $worksheet->getCell('O'.$n)->setValue($h->NOP);
    $worksheet->getCell('P'.$n)->setValue($h->DES);
    $worksheet->getCell('Q'.$n)->setValue($h->TOTAL);
    $n++;   
    }
    $nr=$n+2;
    $nr1=$nr+1;
    $worksheet->mergeCells('A'.$nr.':P'.$nr);
    $worksheet->getCell('A'.$nr)->setValue('RINCIAN TRANSAKSI');
    $worksheet->getStyle('A'.$nr)->getAlignment()->setHorizontal('center');
    $worksheet->getStyle('A'.$nr)->getFont()->setSize(14);

    $worksheet->getCell('A'.$nr1)->setValue('No.');
    $worksheet->getCell('B'.$nr1)->setValue('Tahun');

    $worksheet->mergeCells('C'.$nr1.':D'.$nr1);
    $worksheet->getCell('C'.$nr1)->setValue('ID#');
    $worksheet->getStyle('A'.$nr1.':P'.$nr1)->getAlignment()->setHorizontal('center');

    $worksheet->mergeCells('E'.$nr1.':G'.$nr1);
    $worksheet->getCell('E'.$nr1)->setValue('Nama');
    $worksheet->getStyle('E'.$nr1)->getAlignment()->setHorizontal('center');

    $worksheet->getCell('H'.$nr1)->setValue('Tgl. Bayar');
    $worksheet->getCell('I'.$nr1)->setValue('Periode');
    $worksheet->getCell('J'.$nr1)->setValue('Harga');
    $worksheet->getCell('K'.$nr1)->setValue('PPN');
    $worksheet->getCell('L'.$nr1)->setValue('Diskon');
    $worksheet->getCell('M'.$nr1)->setValue('Metoda');

    $worksheet->mergeCells('N'.$nr1.':P'.$nr1);
    $worksheet->getCell('N'.$nr1)->setValue('Kasir');
    $no2=1;
    $nr2=$nr+2;
    foreach ($dafrin as $h) {
    $worksheet->getCell('A'.$nr2)->setValue($no2++);
    $worksheet->getCell('B'.$nr2)->setValue($h->thn);
    $worksheet->mergeCells('C'.$nr2.':D'.$nr2);
    $worksheet->getCell('C'.$nr2)->setValue($h->username);
    $worksheet->getStyle('C'.$nr2)->getAlignment()->setHorizontal('center');

    $worksheet->mergeCells('E'.$nr2.':G'.$nr2);
    $worksheet->getCell('E'.$nr2)->setValue($h->name);
    $worksheet->getCell('H'.$nr2)->setValue($h->pay_date);
    $worksheet->getCell('I'.$nr2)->setValue($h->bln);   
    $worksheet->getCell('J'.$nr2)->setValue($h->price);
    $worksheet->getCell('K'.$nr2)->setValue($h->ppn);
    $worksheet->getCell('L'.$nr2)->setValue($h->discount);
    $worksheet->getCell('M'.$nr2)->setValue($h->vendor);
    $worksheet->mergeCells('N'.$nr2.':P'.$nr2);
    $worksheet->getCell('N'.$nr2)->setValue($h->kasir);
    $nr2++;
    }

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //header('Content-Disposition: attachment; filename='.date('Ymd').'_'.$nafile.'Payment_History.xlsx');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename='.date('YmdHis').'-'.$nafile.'-Payment_History.xls');    
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



    redirect(base_url($this->uriku."/pymhis/"));

    }


    Public function pdfgen($id=null){
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->SetMargins(12,1,1);
        $pdf->Image(base_url('/assets/images/jlmlogo.png'),12,10,-200);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(30,4,' ',0,0,'C');
        $pdf->Cell(125,4,' ',0,0,'C');
        $pdf->Cell(30,4,' INTERNAL ',1,1,'C');


        $datu = $this->db->where('idt',$id)->get('v_fat_knabraul')->result();
        foreach ($datu as $d){

        	$pdf->Cell(40,4,'',0,0,'C');
        	$pdf->Cell(105,4,'',0,0,'C');
        	$pdf->SetFont('Arial','',9);
        	$pdf->Cell(20,4,'No.',0,0,'L');
        	$pdf->Cell(40,4,': '.$d->urt,0,1,'L');

        	$pdf->Cell(40,4,'',0,0,'C');
        	$pdf->SetFont('Arial','B',13);
        	$pdf->Cell(105,4,'BUKTI PENGELUARAN BANK',0,0,'C');
        	$pdf->SetFont('Arial','',9);
        	$pdf->Cell(20,4,'Tanggal ',0,0,'L');
        	$pdf->Cell(20,4,': '.tgl_indome($d->tgt),0,1,'L');

        	$pdf->Cell(40,4,'',0,0,'C');
        	$pdf->Cell(105,4,'',0,0,'C');
        	$pdf->Cell(20,4,'Tgl. Invoice',0,0,'L');
        	$pdf->Cell(20,4,': '.tgl_indome($d->tgi),0,1,'L');

        	$pdf->Cell(10,2,'',0,1); // Jarak spasi kosong

        	$pdf->SetFont('Arial','',10);
            $pdf->Cell(30,4,'Dibayar kepada',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->Cell(40,4,$d->dik,0,1);
            $pdf->Cell(30,4,'Nominal',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->Cell(40,4,number_format($d->nit).',-',0,1); 
            $pdf->Cell(30,4,'Terbilang',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->SetFont('Arial','B',10);
            $pdf->setFillColor(230,230,230);
            $pdf->Cell(150,4,terbilang($d->nit),0,1,'L',1); 
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(30,4,'Supplier invoice',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->Cell(40,4,$d->sui,0,1); 
            $ppn=$d->p10;
            $pph=$d->p23;
            $naf=$d->dik;

        }
        $n=1;
        $dadu = $this->db->where('idt',$id)->get('fat_knabraul_ket')->result();
        foreach ($dadu as $d){
            if($n==1){ $k='Keterangan'; $t=' : ';}else{$k='';$t='   ';}

            $pdf->Cell(30,4,$k,0,0);
            $pdf->Cell(5,4,$t,0,0);
            $pdf->Cell(115,4,$d->ket,0,0);
            $pdf->Cell(10,4,'Rp. ',0,0);
            $pdf->Cell(25,4,number_format($d->nit).',-',0,1,'R'); 
            $n++;
        }
        $cekdat=$this->db->where('idt',$id)->get('fat_knabraul_ket')->num_rows();


        if($cekdat==1){$x=4;}elseif($cekdat==2){$x=3;}elseif($cekdat==3){$x=2;}else{$x=1;}
        for ($i=0; $i < $x; $i++) { 
            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(130,4,' ',0,0);
            $pdf->Cell(25,4,'-',0,1,'R'); 
        }

        $suto = $this->db->select("sum(nit) as subtot")->where('idt',$id)->get('fat_knabraul_ket')->row();
            $pdf->Cell(30,3,'',0,0);
            $pdf->Cell(130,3,'',0,0,'R');
            $pdf->Cell(25,3,"-------------------------- +",0,1,'R'); 

            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(127,4,'Subtotal Rp.',0,0,'R');
            $pdf->Cell(28,4,number_format($suto->subtot).",-",0,1,'R'); 

            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(127,4,'PPN Rp.',0,0,'R');
            $pdf->Cell(28,4,number_format($ppn).",-",0,1,'R'); 

            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(127,4,'PPH 23 Rp.',0,0,'R');
            $pdf->Cell(28,4,number_format(-1*($pph)).",-",0,1,'R'); 


			$pdf->Cell(185,1,' ','T',1); // jarak spasi kosong
			//$pdf->Cell(185,1,'',0,1); // jarak spasi kosong

            $pdf->SetFont('Arial','I',10);
            $pdf->Cell(125,4,'                     Creditor A/C  :','T',0);
            $pdf->Cell(60,4,' PO No. : ','T',1);
            $pdf->Cell(125,4,'                   Bank Transfer :','T',0);
            $pdf->Cell(60,4,'SPK No. : ','T',1);  
            $pdf->Cell(185,1,'',0,1);
            $pdf->Cell(185,1,' ','T',1); // jarak spasi kosong

            $pdf->SetFont('Arial','',10);
     		$pdf->Cell(30,5,'Disiapkan ',1,0,'C');
        	$pdf->Cell(50,5,'Verifikasi',1,0,'C');
        	$pdf->Cell(75,5,'Menyetujui',1,0,'C');
        	$pdf->Cell(30,5,'Penerima',1,1,'C');

			$pdf->Cell(30,18,' ',1,0);
			$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(30,18,' ',1,1);           

        	$pdf->SetFont('Arial','',9);
			$pdf->Cell(30,4,'Finance A/P',1,0,'C');
			$pdf->Cell(25,4,'Presales',1,0,'C');
        	$pdf->Cell(25,4,'FAM',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(30,4,' ',1,1,'C');
        	$pdf->SetFont('Arial','',8);
        	$pdf->Cell(30,4,'(                     )',1,0,'C');
			$pdf->Cell(25,4,'(                     )',1,0,'C');
        	$pdf->Cell(25,4,'( Henry )',1,0,'C');
        	$pdf->Cell(25,4,'( Victor Irianto )',1,0,'C');
        	$pdf->Cell(25,4,'( Yulius Purnama )',1,0,'C');
        	$pdf->Cell(25,4,'( Edwind J.B. )',1,0,'C');
        	$pdf->Cell(30,4,'(                     )',1,1,'C');
        // ob_start();
        $namafile=date('ymdHi').'-'.$naf;
        $pdf->Output('D','BUK'.$namafile.'.pdf');
        // $pdf->Output();
        $this->huroga->hittak($id);

    }
    
    private function set_menu($active) {
        $menu = [
            "dashboard"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",
            "pengguna"=>"",
            "divisi"=>"", 
            "jabatan"=>"",
            "talian"=>"",           
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "meiga"=>"",
            "ladload"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }

    public function pengguna() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url($this->uriku."/"));

        $daftar_pengguna = $this->pengguna->semua();

        $judul = "Tata Kelola Pengguna";
        $menu = $this->set_menu("pengguna");

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/pengguna",compact("daftar_pengguna"));
        $this->load->view($this->uriku."/frames/hakhi");
    }

    public function penggunatamb() {
    if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url($this->uriku."/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $tbh = $this->pengguna->tambah();
            if($tbh != false) {
                redirect(base_url($this->uriku."/penggunatamb/?succ"));
            } else {
                redirect(base_url($this->uriku."/penggunatamb/?err"));
            }
            exit();
        }

        $judul = "Tambah Pengguna";
        $menu = $this->set_menu("pengguna");
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/penggunatamb",compact("daftas","daftar_jabatan","daftar_divisi"));
        $this->load->view($this->uriku."/frames/hakhi");
    }

    public function penggunaedit($id = null) {

        $this->load->model("Lokasi_model","lokasi");

        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url($this->uriku."/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->edit();
            if($edt != false) {
                redirect(base_url($this->uriku."/penggunaedit/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url($this->uriku."/penggunaedit/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url($this->uriku."/pengguna/"));
        $judul = "Edit Pengguna";
        $menu = $this->set_menu("pengguna");
        $datibo = $this->lokasi->usribo();
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
            $this->load->view($this->uriku."/peguna/penggunaedit",compact("pengguna","datibo","daftar_jabatan","daftar_divisi","daftas"));
            $this->load->view($this->uriku."/frames/hakhi");
        } else redirect(base_url($this->uriku."/pengguna/"));
    }

    public function penggunablokir($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url($this->uriku."/"));
        if($id == null) redirect(base_url($this->uriku."/pengguna"));
        $this->pengguna->blokir($id);
        redirect(base_url($this->uriku."/pengguna/?succ=1"));
    }

    public function penggunabuka($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11)
            redirect(base_url($this->uriku."/"));
        if($id == null) redirect(base_url($this->uriku."/pengguna"));
        $this->pengguna->buka($id);
        redirect(base_url($this->uriku."/pengguna/?succ=2"));
    }

    public function divisi() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11)
            redirect(base_url($this->uriku."/"));


        $judul = "Kelola Divisi";
        $menu = $this->set_menu("divisi");

        //if($this->session->userdata("id_divisi") == 37){
        $daftar_divisi = $this->divisi->divisi_semua();
        //}else{
        //$daftar_divisi = $this->divisi->divisi_sesama($this->session->userdata("id_divisi"));    
        //}

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/divisi",compact("daftar_divisi"));
        $this->load->view($this->uriku."/frames/hakhi");
    }

    public function tambdiv() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11)
            redirect(base_url($this->uriku."/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
            $tbh = $this->divisi->divtamb();
            if($tbh != false) {
                redirect(base_url($this->uriku."/divisi/?succ"));
            } else {
                redirect(base_url($this->uriku."/divisi/?err"));
            }
            exit();
        }


    }

    public function divisiedit($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11)
            redirect(base_url($this->uriku."/"));

        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $edt = $this->divisi->divedit();
            if($edt != false) {
                redirect(base_url($this->uriku."/divisiedit/".$post["id_dinas"]."?succ"));
            } else {
                redirect(base_url($this->uriku."/divisiedit/".$post["id_dinas"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url($this->uriku."/divisi"));

        $judul = "Edit Divisi";
        $menu = $this->set_menu("divisi");
        $divisi = $this->divisi->ambil_berdasarkan_id($id);

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/divisiedit",compact("divisi"));
        $this->load->view($this->uriku."/frames/hakhi");
    }


    public function jabatan() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url($this->uriku."/"));
        $judul = "Kelola Jabatan";
        $menu = $this->set_menu("jabatan");
        $daftar_jabatan = $this->jabatan->ambil_semua();

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/jabatan",compact("daftar_jabatan"));
        $this->load->view($this->uriku."/frames/hakhi");
    }

    public function tambahjabatan() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url($this->uriku."/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $tbh = $this->jabatan->tambah();
            if($tbh != false) {
                redirect(base_url($this->uriku."/tambahjabatan/?succ"));
            } else {
                redirect(base_url($this->uriku."/tambahjabatan/?err"));
            }
            exit();
        }

        $judul = "Tambah Jabatan";
        $menu = $this->set_menu("jabatan");

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/tambahjabatan");
        $this->load->view($this->uriku."/frames/hakhi");
    }

    public function jabatanedit($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url($this->uriku."/"));
        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $edt = $this->jabatan->edit();
            if($edt != false) {
                redirect(base_url($this->uriku."/jabatanedit/".$post["id_jabatan"]."?succ"));
            } else {
                redirect(base_url($this->uriku."/jabatanedit/".$post["id_jabatan"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url($this->uriku."/jabatan"));

        $judul = "Edit Jabatan";
        $menu = $this->set_menu("jabatan");
        $jabatan = $this->jabatan->ambil_berdasarkan_id($id);

        $this->load->view($this->uriku."/frames/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/jabatanedit",compact("jabatan"));
        $this->load->view($this->uriku."/frames/hakhi");
    }


}