<?php

defined("BASEPATH") OR exit("Akses ditolak!");

class Login extends CI_Controller {

    public function __construct(){
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
       

        $this->load->helper("pengalih");
        
        proteksi_logout($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }

        $this->load->model("Pengguna_model","pengguna");
    }


    public function index() {
        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $user = $this->cek_login($post);
            

            if($user != false) {

                if($user["username"] == "maintenanceusr") {
                    $this->konfig->ubah_status_maintenance(1);
                    redirect(base_url());
                    exit();
                }

                $this->session->set_userdata($user);

                $this->divku=$this->session->userdata("id_div");
                
                        // Operation Department  
                        if($this->divku ==4){
                            
                            if($this->session->userdata("id_jabatan") == 17 || $this->session->userdata("id_jabatan") == 24 ){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                            redirect(base_url("panoc/"));
                            }else{
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("panel/"));  
                            }                            



                        // IT Department  
                        }elseif($this->divku ==2){
                            if($this->session->userdata("id_jabatan") ==10 || $this->session->userdata("id_jabatan") ==11){
                                $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("nuber/"));
                                //redirect(base_url("newlook/"));
                            }else{
                                redirect(base_url("login/"));   
                            }

                        // HR, GA Department  
                        }elseif($this->divku ==3){
                            if($this->session->userdata("id_jabatan") ==21 || $this->session->userdata("id_jabatan") ==15){
                                $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("panhg/"));
                            }else{
                                redirect(base_url("login/"));   
                            }

                        // Finance Accounting Tax Department
                        }elseif($this->divku ==5){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panaf/"));

                        // Engineering Department
                        }elseif($this->divku ==6){

                            if ($this->session->userdata("id_jabatan") == 13 || $this->session->userdata("id_jabatan") == 92 ) {
                                $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("nuber/")); 
                            } else {
                                $akt=$this->pengguna->tamakt('login berhasil', $this->session->userdata('username'));
                                redirect(base_url("panel/"));
                            }

                        // Commercial OMG
                        }elseif($this->divku ==12){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("nuber/"));

                        // Network Development Department
                        }elseif($this->divku ==18){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("nuber/"));                       

                        // Manage Service Department
                        }elseif($this->divku ==19){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("nuber/")); 

                        }elseif($this->divku ==20){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("nuber/")); 
                        
                        // Bussiness Development Department
                        }elseif($this->divku ==10){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panbd/")); 


                        // Business Solution Dept
                        }elseif($this->divku ==21){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));
                        
                        // Asset Management Dept
                        }elseif($this->divku ==22){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));

                         // Customer Support Dept
                        }elseif($this->divku ==23){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("nuber/"));

                        // Site Acquisition Dept
                        }elseif($this->divku ==24){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));                                               
                        //}elseif($this->divku==10){
                            
                        //    if($this->session->userdata("id_jabatan") >59){
                        //    $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        //    redirect(base_url("panbd/"));
                        //    }

                        // Khusus e-proc  Procurement, Legal
                        }elseif($this->divku ==16 || $this->divku ==11 ){
                            if ($this->session->userdata("id_jabatan") == 27) {
                                $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("nuber/")); 
                            } else {
                                $akt=$this->pengguna->tamakt('login berhasil', $this->session->userdata('username'));
                                redirect(base_url("panep/"));
                            }

                        // Marketing, Commercial JLM, Wholesales Department 
                        }elseif($this->divku==1 || $this->divku==9 || $this->divku==15){
                        $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));
                        }                                               

                /*       }else{
                        $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));
                        } */
                


            } else {
                $akt=$this->pengguna->tamakt('login gagal',$this->input->post('username'));
                redirect(base_url("login/?err"));
            }
        }
        
        $get = $this->input->get();

        if (isset($get['username'])) {
        	$user_intra = $this->cek_login_intra($get);

        	if($user_intra != false) {

                if($user_intra["username"] == "maintenanceusr") {
                    $this->konfig->ubah_status_maintenance(1);
                    redirect(base_url());
                    exit();
                }

                $this->session->set_userdata($user_intra);

                $this->divku=$this->session->userdata("id_div");
                
                        // Operation Department  
                        if($this->divku ==4){
                            
                            if($this->session->userdata("id_jabatan") == 17 || $this->session->userdata("id_jabatan") == 24 ){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                            redirect(base_url("panoc/"));
                            }else{
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("panel/"));  
                            }                            



                        // IT Department  
                        }elseif($this->divku ==2){
                            if($this->session->userdata("id_jabatan") ==10 || $this->session->userdata("id_jabatan") ==11){
                                $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("nuber/"));
                            }else{
                                redirect(base_url("login/"));   
                            }

                        // HR, GA Department  
                        }elseif($this->divku ==3){
                            if($this->session->userdata("id_jabatan") ==21 || $this->session->userdata("id_jabatan") ==15){
                                $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("panhg/"));
                            }else{
                                redirect(base_url("login/"));   
                            }

                        // Finance Accounting Tax Department
                        }elseif($this->divku ==5){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panaf/"));

                        // Engineering Department
                        }elseif($this->divku ==6){

                            if ($this->session->userdata("id_jabatan") == 13 || $this->session->userdata("id_jabatan") == 92 ) {
                                $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("nuber/")); 
                            } else {
                                $akt=$this->pengguna->tamakt('login berhasil', $this->session->userdata('username'));
                                redirect(base_url("panel/"));
                            }

                        // Commercial OMG
                        }elseif($this->divku ==12){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("nuber/"));

                        // Network Development Department
                        }elseif($this->divku ==18){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("nuber/"));                       

                        // Manage Service Department
                        }elseif($this->divku ==19){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("nuber/")); 

                        }elseif($this->divku ==20){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("nuber/")); 
                        
                        // Bussiness Development Department
                        }elseif($this->divku ==10){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panbd/")); 


                        // Business Solution Dept
                        }elseif($this->divku ==21){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));
                        
                        // Asset Management Dept
                        }elseif($this->divku ==22){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));

                         // Customer Support Dept
                        }elseif($this->divku ==23){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("nuber/"));
                                               
                        //}elseif($this->divku==10){
                            
                        //    if($this->session->userdata("id_jabatan") >59){
                        //    $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        //    redirect(base_url("panbd/"));
                        //    }

                        // Khusus e-proc  Procurement, Legal
                        }elseif($this->divku ==16 || $this->divku ==11 ){
                            if ($this->session->userdata("id_jabatan") == 27) {
                                $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("nuber/")); 
                            } else {
                                $akt=$this->pengguna->tamakt('login berhasil', $this->session->userdata('username'));
                                redirect(base_url("panep/"));
                            }

                        // Marketing, Commercial JLM, Wholesales Department 
                        }elseif($this->divku==1 || $this->divku==9 || $this->divku==15){
                        $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));
                        }                                               

                /*       }else{
                        $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));
                        } */
                


            } else {
                $akt=$this->pengguna->tamakt('login gagal', $this->input->get('username'));
                redirect(base_url("login/?err"));
            }
        }

        $this->load->view("login/login");
    }

    private function cek_login($post) {
        if(isset($post["username"]) && isset($post["password"])) {
            $data = $this->pengguna->login($post["username"],$post["password"]);
            return $data;
        }
    }

    private function cek_login_intra($get) {
        if(isset($get["username"])) {
	        $data = $this->pengguna->login_intra($get["username"]);
	        return $data;
        }
    }


}