<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Khusus extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");

        $this->load->model("Khusus_model","khusus");
        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Genral_model","genral");
        $this->load->library('excel');
        //$this->load->model("Laporan_model","laporan");
    }

    public function index() {
   

        $judul = "Menu Khusus";
        if($this->nadiv=='Business Solution'){
            $menu = $this->set_amenu("mesat");
        }else{
            $menu = $this->set_amenu("khusus");
        }

        if($this->divku==23 ){
        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("panel/vdunem/menuvd");
        $this->load->view("panel/frames/footer");
    }

    public function vendat() {



        $vdsem = $this->khusus->dafsem();
        $vdrek = $this->genral->semua("v_t_nanaggnal_rekap");
        //$pelsemua = $this->laporan->pelanggan();
        
        $judul = "Data Vendor";
        if($this->nadiv=='Business Solution'){
            $menu = $this->set_amenu("mesat");
        }else{
            $menu = $this->set_amenu("khusus");
        }

        if($this->divku==23 ){
        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        }else{
        $this->load->view("panel/frames/header",compact("judul","menu"));
        }
        $this->load->view("panel/vdunem/vendat",compact("vdrek","vdsem","pelsemua"));
        $this->load->view("panel/frames/footer");
    }

    public function ubah_data($id) {

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
             
            $ubsim = $this->khusus->simubah();
            if($ubsim != false) {
                redirect(base_url("khusus/vendat/?succ"));
            } else {
                redirect(base_url("khusus/vdubah/?err"));
            }
            exit();
        }

        $vdsem = $this->khusus->dafsem();
        $dven = $this->khusus->dafven();
        $dseg = $this->khusus->dafseg();
        $gdat = $this->genral->pildat("t_nanaggnal","idtr",$id);
        $judul = "Ubah Vendor";
        $menu = $this->set_amenu("mesat");



        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/vdunem/vdubah",compact("vdsem","dven","dseg","gdat"));
        $this->load->view("panel/frames/footer");
    }


   public function hapus_data($id=null) {
 
        $h = $this->db->where("idtr",$id)->delete("t_nanaggnal");
            if($h){
            $akt=$this->pengguna->tamakt('Hapus Data Vendor ID '.$id,$this->usrku);
                redirect(base_url("khusus/vendat/?hsuc"));
            }else{
                redirect(base_url("khusus/vendat/?herr")); 
            }
    }


   public function terminated_data($id=null) {
 
        $d = array('stat' => 'InActive', );
        $t = $this->db->where("idtr",$id)->update("t_nanaggnal",$d);
            if($t){
            $akt=$this->pengguna->tamakt('Terminate/InActive data Vendor ID '.$id,$this->usrku);
                redirect(base_url("khusus/vendat/?tsuc"));
            }else{
                redirect(base_url("khusus/vendat/?terr")); 
            }
    }

    public function site_data($v,$s,$k) {

        $site = $this->db->where("vnam",$v)->where("stat",$s)->where("kota",$k)->get("v_t_nanaggnal")->result();
        $tots = $this->db->where("vnam",$v)->where("stat",$s)->where("kota",$k)->get("v_t_nanaggnal")->num_rows();
        $nama = $this->db->select("vnama,stat,kota")->where("vnam",$v)->where("stat",$s)->where("kota",$k)->limit(1)->get("v_t_nanaggnal")->row();

        $judul = "Data Site";
        if($this->nadiv=='Business Solution'){
            $menu = $this->set_amenu("mesat");
        }else{
            $menu = $this->set_amenu("khusus");
        }

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/vdunem/vdsite",compact("site","tots","nama"));
        $this->load->view("panel/frames/footer");
    }



    public function venexp() {
        $thn=date("Y");
        $judul="DATA BERLANGGANAN VENDOR";


        
        
        
        $excel = new PHPExcel();



        // Settingan awal fil excel
        $excel->getProperties()->setCreator('Portal')
                               ->setLastModifiedBy('System Portal')
                               ->setTitle($judul)
                               ->setSubject("Laporan".$thn)
                               ->setDescription($judul)
                               ->setKeywords("LAPORAN");

        // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
        $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );

        // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
        );


                $objDrawing = new PHPExcel_Worksheet_Drawing();
                $objDrawing->setPath('./assets/images/logo.jpg');
                $objDrawing->setCoordinates('A1');
                $objDrawing->setHeight(40);
                $objDrawing->setWidth(80);
                $objDrawing->setWorksheet($excel->getActiveSheet());

        
        
        $excel->setActiveSheetIndex(0)->setCellValue('A2', $judul." ".$thn); // Set kolom A2 dengan tulisan "JUDUL"
        $excel->getActiveSheet()->mergeCells('A2:W2'); // Set Merge Cell pada kolom A2 sampai W2
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(TRUE); // Set bold kolom A2
        $excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(17); // Set font size 15 untuk kolom A2
        $excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        /*
        $excel->setActiveSheetIndex(0)->setCellValue('A3', "Nomor");
        $excel->setActiveSheetIndex(0)->setCellValue('C3', "");
        $excel->setActiveSheetIndex(0)->setCellValue('A4', "Period");
        $excel->setActiveSheetIndex(0)->setCellValue('C4', 0); 
        $excel->getActiveSheet()->getStyle('C4')->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);    
        $excel->setActiveSheetIndex(0)->setCellValue('A5', "Requester");
        $excel->setActiveSheetIndex(0)->setCellValue('C5', 0);
                $excel->getActiveSheet()->getStyle('C5')->getAlignment()
                        ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
        $excel->setActiveSheetIndex(0)->setCellValue('A6', "Total Trouble Ticket");
        $excel->setActiveSheetIndex(0)->setCellValue('C6', 0);
        $excel->setActiveSheetIndex(0)->setCellValue('A7', "Availablity (%)");
        $excel->setActiveSheetIndex(0)->setCellValue('C7', 0);
        //$excel->setActiveSheetIndex(0)->setCellValue('D7', $hasila);
        $excel->setActiveSheetIndex(0)->setCellValue('A8', "Total Downtime");
        $excel->setActiveSheetIndex(0)->setCellValue('C8', 0);

        //$excel->setActiveSheetIndex(0)->setCellValue('D8', $ava);
        $excel->getActiveSheet()->getStyle('A3:B8')->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('argb' => '39b3d7')
                        )
                );
        $excel->getActiveSheet()->getStyle("A3:C8")->applyFromArray(
        array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000')
                )
              )
            )
        );
        
        $excel->getActiveSheet()->getStyle('C8')->getNumberFormat()->setFormatCode('[hh]:mm');*/
        
        // Header Tabel di baris ke 10
        $excel->setActiveSheetIndex(0)->setCellValue('A4', "No."); 
        $excel->getActiveSheet()->mergeCells('A4:A5');
        $excel->setActiveSheetIndex(0)->setCellValue('B4', "Nama Vendor");
        $excel->getActiveSheet()->mergeCells('B4:B5');
        $excel->setActiveSheetIndex(0)->setCellValue('C4', "Status");
        $excel->getActiveSheet()->mergeCells('C4:C5'); 
        $excel->setActiveSheetIndex(0)->setCellValue('D4', "No. PO");
        $excel->getActiveSheet()->mergeCells('D4:D5');  
        $excel->setActiveSheetIndex(0)->setCellValue('E4', "Tgl. PO");
        $excel->getActiveSheet()->mergeCells('E4:E5');  
        $excel->setActiveSheetIndex(0)->setCellValue('F4', "Aktif");
        $excel->getActiveSheet()->mergeCells('F4:F5'); 
        $excel->setActiveSheetIndex(0)->setCellValue('G4', "Kontrak (bln)");
        $excel->getActiveSheet()->mergeCells('G4:G5'); 
        $excel->getActiveSheet()->getStyle('G4:G5')->getAlignment()->setWrapText(true);
        $excel->setActiveSheetIndex(0)->setCellValue('H4', "Akhir Kontrak (1 thn)"); 
        $excel->getActiveSheet()->mergeCells('H4:H5');
        $excel->getActiveSheet()->getStyle('H4:H5')->getAlignment()->setWrapText(true);
        $excel->setActiveSheetIndex(0)->setCellValue('I4', "Durasi Kontrak (bln)");
        $excel->getActiveSheet()->mergeCells('I4:I5'); 
        $excel->getActiveSheet()->getStyle('I4:I5')->getAlignment()->setWrapText(true);
        $excel->setActiveSheetIndex(0)->setCellValue('J4', "CID / SID");
        $excel->getActiveSheet()->mergeCells('J4:J5');
        $excel->setActiveSheetIndex(0)->setCellValue('K4', "Kota");
        $excel->getActiveSheet()->mergeCells('K4:K5'); 
        $excel->setActiveSheetIndex(0)->setCellValue('L4', "Site");
        $excel->getActiveSheet()->mergeCells('L4:L5'); 
        $excel->setActiveSheetIndex(0)->setCellValue('M4', "Alamat Instalasi");
        $excel->getActiveSheet()->mergeCells('M4:M5');
        $excel->setActiveSheetIndex(0)->setCellValue('N4', "Segment");
        $excel->getActiveSheet()->mergeCells('N4:N5');

        //--Vendor
        $excel->setActiveSheetIndex(0)->setCellValue('O4', "Vendor");
        $excel->getActiveSheet()->mergeCells('O4:R4');
        $excel->getActiveSheet()->getStyle('O4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->setActiveSheetIndex(0)->setCellValue('O5', "Instalasi Charge"); 
        $excel->setActiveSheetIndex(0)->setCellValue('P5', "Monthly Charge");
        $excel->setActiveSheetIndex(0)->setCellValue('Q5', "Bandwidth"); 
        $excel->setActiveSheetIndex(0)->setCellValue('R5', "Activation Date");
        //-- Customer
        $excel->setActiveSheetIndex(0)->setCellValue('S4', "Customer");
        $excel->getActiveSheet()->mergeCells('S4:V4');
        $excel->getActiveSheet()->getStyle('S4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $excel->setActiveSheetIndex(0)->setCellValue('S5', "Instalasi Charge"); 
        $excel->setActiveSheetIndex(0)->setCellValue('T5', "Monthly Charge");
        $excel->setActiveSheetIndex(0)->setCellValue('U5', "Bandwidth"); 
        $excel->setActiveSheetIndex(0)->setCellValue('V5', "Activation Date");

        $excel->setActiveSheetIndex(0)->setCellValue('W4', "Keterangan");
        $excel->getActiveSheet()->mergeCells('W4:W5'); 
         

        // Apply style header yang telah kita buat tadi ke masing-masing kolom header
        $excel->getActiveSheet()->getStyle('A4:W5')->applyFromArray($style_col);
 /*       $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_col);
 */     
        $excel->getActiveSheet()->getStyle("A4:W5")->applyFromArray(
        array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000')
                )
              )
            )
        );


        $excel->getActiveSheet()->getStyle('A4:W5')->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('argb' => 'ffbbac')
                        )
                );
        $excel->getActiveSheet()->getStyle('O4:R5')->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('argb' => 'acffb0')
                        )
                );
        $excel->getActiveSheet()->getStyle('S4:V5')->getFill()->applyFromArray(
                        array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array('argb' => 'acb0ff')
                        )
                );
        $datsem = $this->khusus->dafsem();
        $no = 1; // Untuk penomoran tabel, di awal set dengan 1
        $numrow = 6; // Set baris pertama untuk isi tabel adalah baris ke 4
        foreach($datsem as $b){ // Lakukan looping pada variabel
                
            $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
            $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $b->vnama);
            $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $b->stat);
            $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $b->nopo);
            $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $b->tgpo);
            $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $b->tgac);
            $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $b->lcon);
            $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $b->acon);
            $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $b->dcon);
            $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $b->csid);
            $excel->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $b->kota);
            $excel->setActiveSheetIndex(0)->setCellValue('L'.$numrow, $b->site);
            $excel->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $b->alam);
            $excel->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $b->csegm);
            $excel->setActiveSheetIndex(0)->setCellValue('O'.$numrow, $b->vich);
            $excel->setActiveSheetIndex(0)->setCellValue('P'.$numrow, $b->vmch);
            $excel->getActiveSheet()->getStyle('O'.$numrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $excel->getActiveSheet()->getStyle('P'.$numrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $excel->setActiveSheetIndex(0)->setCellValue('Q'.$numrow, $b->vbwi);
            $excel->setActiveSheetIndex(0)->setCellValue('R'.$numrow, $b->vtga);
            $excel->setActiveSheetIndex(0)->setCellValue('S'.$numrow, $b->cich);
            $excel->setActiveSheetIndex(0)->setCellValue('T'.$numrow, $b->cmch);
            $excel->getActiveSheet()->getStyle('S'.$numrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $excel->getActiveSheet()->getStyle('T'.$numrow)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $excel->setActiveSheetIndex(0)->setCellValue('U'.$numrow, $b->cbwi);
            $excel->setActiveSheetIndex(0)->setCellValue('V'.$numrow, $b->ctga);
            $excel->setActiveSheetIndex(0)->setCellValue('W'.$numrow, $b->kete);


        $no++; // Tambah 1 setiap kali looping
        $numrow++; // Tambah 1 setiap kali looping

        }

        // Set width kolom
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); // Set width kolom B
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(11); // Set width kolom C
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(13); // Set width kolom D
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(13); // Set width kolom E
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(13); // Set width kolom F
        $excel->getActiveSheet()->getColumnDimension('G')->setWidth(11); // Set width kolom G
        $excel->getActiveSheet()->getColumnDimension('H')->setWidth(13); // Set width kolom H
        $excel->getActiveSheet()->getColumnDimension('I')->setWidth(14); // Set width kolom I
        $excel->getActiveSheet()->getColumnDimension('J')->setWidth(15); // Set width kolom J
        $excel->getActiveSheet()->getColumnDimension('K')->setWidth(13); // Set width kolom K
        $excel->getActiveSheet()->getColumnDimension('L')->setWidth(13); // Set width kolom L
        $excel->getActiveSheet()->getColumnDimension('M')->setWidth(51); // Set width kolom K
        $excel->getActiveSheet()->getColumnDimension('N')->setWidth(11); // Set width kolom L
        $excel->getActiveSheet()->getColumnDimension('O')->setWidth(15); // Set width kolom K
        $excel->getActiveSheet()->getColumnDimension('P')->setWidth(15); // Set width kolom L
        $excel->getActiveSheet()->getColumnDimension('Q')->setWidth(13); // Set width kolom K
        $excel->getActiveSheet()->getColumnDimension('R')->setWidth(15); // Set width kolom L
        $excel->getActiveSheet()->getColumnDimension('S')->setWidth(15); // Set width kolom K
        $excel->getActiveSheet()->getColumnDimension('T')->setWidth(15); // Set width kolom L
        $excel->getActiveSheet()->getColumnDimension('U')->setWidth(13); // Set width kolom K
        $excel->getActiveSheet()->getColumnDimension('V')->setWidth(15); // Set width kolom L
        $excel->getActiveSheet()->getColumnDimension('W')->setWidth(51); // Set width kolom K

        // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        // Set orientasi kertas jadi LANDSCAPE
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        // Set judul file excel nya
        $excel->getActiveSheet(0)->setTitle($thn);
        $excel->setActiveSheetIndex(0);

        // Proses file excel
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$judul." ".$thn.'.xlsx"'); // Set nama file excel nya
        header('Cache-Control: max-age=0');

        $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $write->save('php://output');
   
       /* $judul = "Data Vendor";
        $menu = $this->set_amenu("khusus");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/vdunem/vendat",compact("vdsem"));
        $this->load->view("panel/frames/footer");*/

    }


    public function venbar() {

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
             
            $vdsim = $this->khusus->vensim();
            if($vdsim != false) {
                redirect(base_url("khusus/vdtamb/?succ"));
            } else {
                redirect(base_url("khusus/ventamb/?err"));
            }
            exit();
        }
       
        $judul = "Tambah Vendor";
        if($this->nadiv=='Business Solution'){
            $menu = $this->set_amenu("mesat");
        }else{
            $menu = $this->set_amenu("khusus");
        }

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/vdunem/ventamb");
        $this->load->view("panel/frames/footer");
    }


        public function vdtamb() {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url("panel/"));
        
        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
             
            $dtsim = $this->khusus->datsim();
            if($dtsim != false) {
                redirect(base_url("khusus/vendat/?succ"));
            } else {
                redirect(base_url("khusus/vdtamb/?err"));
            }
            exit();
        }

        $judul = "Vendor Baru";
        
        if($this->nadiv=='Business Solution'){
            $menu = $this->set_amenu("mesat");
        }else{
            $menu = $this->set_amenu("khusus");
        }
        
        
        $dven = $this->khusus->dafven();
        $dseg = $this->khusus->dafseg();

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/vdunem/vdtamb",compact("dven","dseg"));
        $this->load->view("panel/frames/footer");
    }


    private function set_amenu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "harga_baru"=>"",
            "salesrevenue"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "talian"=>"",
            "menufoc"=>"",
            "womonito"=>"",
            "daftaodp"=>"",
            "divisi"=>"",
            "aktifitas"=>"",
            "aprofil"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "mascidsid"=>"",
            "inventory"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }


}