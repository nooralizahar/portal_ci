<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Odpscan extends CI_Controller 
{
	//public $data;

	//public $student;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Pidai_model', 'pidai');
		$this->load->model('Odpdata_model', 'odpdata');

	}

	public function index()
	{
		$this->data = array(
			'title' => "PIDAI KODE QR"
		);
		$this->load->view('panel/kode/hbacaqr', $this->data);
	}

	public function periksa(){

       $post = $this->input->post();
        //if(isset($post["cariqr"])) {
        	$r=$this->input->post('cariqr');

		        if($r==''){
		        	redirect(base_url("odpscan"));
		        }else{
		        	$cekdat = $this->odpdata->cekpidai($r);	
		        }


        if($cekdat > 0) {

        		$rdat = $this->odpdata->odppidai($r);

        		foreach ($rdat as $d):
            	$cid=$d->idodp;
            	endforeach;

				$this->data = array(
				'title' => "ODP Terdaftar",
				'idku'=>$cid,
				'dpel'=>$this->odpdata->odppel($cid),
				'pkai'=>$this->odpdata->odpuse($cid), 
				'data' => $rdat
				);
				$this->load->view('panel/kode/hasilbc', $this->data);

            } else {

		        $this->data = array(
					'title' => "ODP Tidak Terdaftar",
					'nodok' => $r
				);
		        $this->load->view('panel/kode/gagalbc', $this->data);
		        

     		}

        //}
	}

}

/* End of file Haldaftar.php */
/* Location: ./application/modules/infoeskpn/controllers/Haldaftar.php */