<?php


defined("BASEPATH") OR exit("Akses ditolak!");

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Newlook extends CI_Controller {

    var $uriku;

    public function __construct() {
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");
        $this->dbz = $this->load->database('newdb', TRUE); // Koneksi ke Postgres iBoss
        $this->dbe = $this->load->database('dbbaru', TRUE);

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("general");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->library('pdf');
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }

        $this->load->library('phpqrcode/qrlib');
        $this->load->helper('url');        
        $this->load->model("excel_import_model");
        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Hris_model","hris");
        $this->load->model("Huroga_model","huroga");
        $this->load->model("Aktif_model","aktif");
        $this->load->model("Talian_model","talian");
        $this->load->model("Harga_model","harga");
        $this->load->model("Netdev_model","netdev");
        $this->load->model("Otbdata_model","otbdat");
        $this->load->model("Wordata_model","wordata");
        $this->load->model("Rolout_model","rolout");
        $this->load->model("Odpdata_model","odpdata");
        $this->load->model("Odcdata_model","odcdata");
        $this->load->model("Iboss_model","iboss");
        $this->load->model("Lokasi_model","lokasi");
        $this->load->model("Genral_model","genral");

    }

    public function index() {
        
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));
        $dafmserv=$this->harga->gmanser(); // manage service
        if($this->divku==18){
            $dafnedev=$this->netdev->semua(); // network development
            $marea= $this->db->get("v_app_latrop_aeraak")->num_rows();    
        }elseif($this->divku==12){

            if($this->idaku==154 ){
                $dafneapv=$this->genral->gentam("v_app_latrop_aeraak_apv");
            }elseif($this->idaku==5 ){
                $dafneapv=$this->genral->pildat("v_app_latrop_aeraak_apv","apsa",154);
            }
                $dafnedev=$this->netdev->reahon($this->idaku); // per sales / pemohon
                $marea= $this->db->where("phrea",$this->idaku)->get("v_app_latrop_aeraak")->num_rows();                
           

        }
        

        $judul = "Dashboard";
       
        $this->load->view("newlook-shared/rangka/nl-hawal",compact("judul"));
        $this->load->view("newlook-shared/rangka/nl-hatas");
        $this->load->view("newlook/index");
        $this->load->view("newlook-shared/rangka/nl-hakhi");
    }


    public function profil_tampil() {
        $id=$this->idaku;
//        if($this->session->userdata("id_pengguna") != $id)
//            redirect(base_url($this->uriku."/"));

        $judul = "Data Profil Pengguna";
        
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
        $this->load->view("newlook-shared/rangka/nl-hawal",compact("judul"));
        $this->load->view("newlook-shared/rangka/nl-hatas");
        $this->load->view("newlook-shared/person/index",compact("pengguna","daftar_jabatan","daftar_divisi","daftas"));
        $this->load->view("newlook-shared/rangka/nl-hakhi");

        } else redirect(base_url($this->uriku."/pengguna/"));
    }

    public function logout() {
        $akt=$this->pengguna->tamakt('logout-nl berhasil',$this->usrku);
        $this->session->sess_destroy();
        redirect(base_url("login/"));
    }




// awal Rollout Controller
    public function dasrol() {
        $judul = "Dashboard Rollout";
        $menu = $this->set_menu("metuj");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("nuber/tuolor/shorout");
        $this->load->view("share/akgnar/hakhi");

    }

    public function tiabar() {
        if($this->divku != 20 and $this->divku!=18)
            redirect(base_url($this->uriku."/logout/"));

        $post = $this->input->post();
        $alam = $this->input->post("altia");
        

        if(isset($post["btnSimpan"])) {
             
            $pol = $this->rolout->simala();
            $tid = $this->rolout->akhala();
            if($pol != false) {
            $akt=$this->genral->tamakt('Alamat Pole ID Baru '.$alam,$this->usrku);
                $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/tiltia/".$tid->idala));
            } else {
                $this->genral->rimsan(2);
                redirect(base_url($this->uriku."/tiabar/".$tid->idala));
            }
            exit();
        }

        $daftia=$this->rolout->datala();
        $judul = "Data Tiang";
        $menu = $this->set_menu("mesat");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/tuolor/fadait",compact("daftia","alapol"));
        $this->load->view("share/akgnar/hakhi");

    }

    public function pusala($i=null) {
        if($this->divku != 20)
            redirect(base_url($this->uriku."/logout/"));

        $cek=$this->genral->pilhit("app_latrop_gnait_lit","idala",$i);

        if($cek>0){
                $this->genral->rimsan(5);
                redirect(base_url($this->uriku."/tiabar/"));           
        }

        $h = $this->genral->pilpus("app_latrop_gnait","idala",$i);
            if($h){
                $akt=$this->pengguna->tamakt('Hapus Alamat Pole ID '.$i,$this->usrku);
                 $this->genral->rimsan(3);
                redirect(base_url($this->uriku."/tiabar/"));
            }else{
                $this->genral->rimsan(4);
                redirect(base_url($this->uriku."/tiabar/")); 
            }
    }

    public function tiltia($al=null) {
        if($this->divku != 20 and $this->divku!=18)
            redirect(base_url($this->uriku."/logout/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {

            $pol = $this->rolout->simtia();
            if($pol != false) {
            $akt=$this->pengguna->tamakt('Tiang Baru # '.$al."Tiang #",$this->usrku);
                $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/tiltia/".$al));
            } else {
                $this->genral->rimsan(2);
                redirect(base_url($this->uriku."/tiltia/".$al));
            }
            exit();
        }

        //$daftia=$this->rolout->dattia();
        $daftil=$this->rolout->tilala($al);
        $dafseg=$this->rolout->cbtiseg();
        $dafket=$this->rolout->cbtiket();
        $dafdcp=$this->rolout->cbodcdp();

        $judul = "Data Tiang";
        $menu = $this->set_menu("mesat");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/tuolor/littup",compact("daftil","dafseg","dafket","dafdcp","daftia","al"));
        $this->load->view("share/akgnar/hakhi");

    }

    public function pustia($pa=null,$al=null) {
        if($this->divku != 20)
            redirect(base_url($this->uriku."/logout/"));

        $h = $this->rolout->pustia($al);
            if($h){
            $akt=$this->pengguna->tamakt('Hapus Tiang ID '.$alam,$this->usrku);
                 $this->genral->rimsan(3);
                redirect(base_url($this->uriku."/tiltia/".$pa));
            }else{
                $this->genral->rimsan(4);
                redirect(base_url($this->uriku."/tiltia/".$pa)); 
            }
    }


    public function kabbar() {
        if($this->divku != 20 and $this->divku!=18)
            redirect(base_url($this->uriku."/logout/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {

            $kab = $this->rolout->simkab();
            if($kab != false) {
            $akt=$this->pengguna->tamakt('Kabel Baru # '.$al."Tiang #",$this->usrku);
                $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/kabbar/"));
            } else {
                $this->genral->rimsan(2);
                redirect(base_url($this->uriku."/kabbar/"));
            }
            exit();
        }

        $dafkab=$this->rolout->tamkab();
        $dafseg=$this->rolout->cbkaseg();
        $dafjka=$this->rolout->cbkajen();

        $judul = "Data Kabel";
        $menu = $this->set_menu("medua");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/tuolor/lebtup",compact("dafkab","dafseg","dafjka"));
        $this->load->view("share/akgnar/hakhi");

    }


    public function puskab($a=null) {
        if($this->divku != 20)
            redirect(base_url($this->uriku."/logout/"));

        $h = $this->rolout->puskab($a);
            if($h){
            $akt=$this->pengguna->tamakt('Hapus Kabel ID #'.$a,$this->usrku);
                 $this->genral->rimsan(3);
                redirect(base_url($this->uriku."/kabbar/".$pa));
            }else{
                $this->genral->rimsan(4);
                redirect(base_url($this->uriku."/kabbar/".$pa)); 
            }
    }

    public function otblis() {

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $tbh = $this->otbdat->ottam();
            if($tbh != false) {
                $akt=$this->pengguna->tamakt('tambah OTB ',$this->usrku);
                $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/otblis/"));
            } else {
                $this->genral->rimsan(2);
                redirect(base_url($this->uriku."/otblis/"));
            }
            exit();
        }


        $otbsem = $this->otbdat->otsem();
        $daflok = $this->otbdat->losem();

        $judul = "Daftar OTB";
        $menu = $this->set_menu("menol");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/tuolor/silbto",compact("daflok","otbsem"));
        $this->load->view("share/akgnar/hakhi");

    }

    public function otbdit($pi=null) {
   
        $otbde = $this->otbdat->otlih($pi);
        $otbpi = $this->otbdat->otpil($pi);
        foreach ($otbpi as $a) {
           $onm=$a->nm_otb;
        }

        $judul = "Detil OTB";
        $menu = $this->set_menu("menol");
        

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/tuolor/tidbto",compact("otbde","pi","onm"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function otblo() {     

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $tbh = $this->otbdat->lotam();
            if($tbh != false) {
                $akt=$this->pengguna->tamakt('Tambah lokasi ',$this->usrku);
                redirect(base_url($this->uriku."/otblo/?succ"));
            } else {
                redirect(base_url($this->uriku."/otblo/?err"));
            }
            exit();
        }


        $otblosem = $this->otbdat->losem();
       
        
        $judul = "FOC - Optical Termination Box";
        $menu = $this->set_menu("menol");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/tuolor/kolbto",compact("otblosem"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function loked($idl=null) {
   
        
        $lokpi = $this->otbdat->pillo($idl);

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $sim = $this->otbdat->simlo($idl);
            if($sim != false) {
                $akt=$this->pengguna->tamakt('edit lokasi '.$idl.'',$this->usrku);
                redirect(base_url($this->uriku."/otblo/".$idl."?succ"));
            } else {
                redirect(base_url($this->uriku."/loked/".$idl."?err"));
            }
            exit();
        }

        $judul = "Edit Lokasi";
        $menu = $this->set_menu("menol");
        

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/tuolor/tidkol",compact("lokpi"));
        $this->load->view("share/akgnar/hakhi");


    }

    public function cordit($ido=null,$idp=null) {
   
        $otbde = $this->otbdat->otlih($idp);
        $corpi = $this->otbdat->copil($idp);

        $otbco = $this->otbdat->otlink($ido);
        $otbcx = $this->otbdat->otlinx($ido);
        $corst = $this->otbdat->otcor($idp);

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $sim = $this->otbdat->cosim($idp);
            if($sim != false) {
                $akt=$this->pengguna->tamakt('edit core OTB '.$ido.'port '.$idp.'',$this->usrku);
                $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/otbdit/".$ido));
            } else {
                $this->genral->rimsan(2);
                redirect(base_url($this->uriku."/cordit/".$ido."/".$idp));
            }
            exit();
        }

        $judul = "Edit Core OTB";
        $menu = $this->set_menu("menol");
        

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/tuolor/tidroc",compact("corpi","otbco","corst","otbcx"));//,"pi","onm"
        $this->load->view("share/akgnar/hakhi");
    }

    public function odpin() {

        

       /* $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $tbh = $this->odpdata->tambah();
            if($tbh != false) {
                redirect(base_url("focmen/odpin/?succ"));
            } else {
                redirect(base_url("focmen/odpin/?err"));
            }
            exit();
        }*/

        $odpsem = $this->iboss->datodp();
        //$odpsem = $this->odpdata->semua();
        //$dafolt = $this->odpdata->oltsem();
        //$daflok = $this->lokasi->semlok();

        $judul = "FTTH - Optical Distribution Point";
        $menu = $this->set_menu("metig");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        //$this->load->view("panel/founem/odpin",compact("odpsem","dafolt","daflok"));
        $this->load->view("share/tuolor/odpib",compact("odpsem"));
        $this->load->view("share/akgnar/hakhi");
    }
    public function odcin() {    

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $tbh = $this->odcdata->tambah();
            if($tbh != false) {
                redirect(base_url($this->uriku."/odcin/"));
            } else {
                redirect(base_url($this->uriku."/odcin/"));
            }
            exit();
        }


        $odcsem = $this->odcdata->semua();
        $dafolt = $this->lokasi->semolt();
        $daflok = $this->lokasi->semlok();

        $judul = "FTTH - Optical Distribution Cabinet";
        $menu = $this->set_menu("meemp");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/tuolor/odcin",compact("dafolt","odcsem","daflok"));
        $this->load->view("share/akgnar/hakhi");
    }


    public function odprek() {
        $thnini= date('Y');
        //iboss rekapitulasi
        $odpthn=$this->iboss->detodpthn();
        $odplok=$this->iboss->detodplok($thnini);

        $judul = "Rekapitulasi";
        $menu = $this->set_menu("melim");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/tuolor/odpre",compact("odpthn","odplok"));
        $this->load->view("share/akgnar/hakhi");
    }


// akhir  Rollout Controller


// awal Manage Service Controller

    public function msdquo($id = null) {
        if($this->divku != 19)
            redirect(base_url($this->uriku."/logout/"));

        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
             
            $quo = $this->harga->uploquo($id);
            if($quo != false) {
            $akt=$this->genral->tamakt('unggah  quotation id# '.$id,$this->usrku);
                 $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/"));
            } else {
                $this->genral->rimsan(2);
                redirect(base_url($this->uriku."/"));
            }
            exit();
        }


        $datlod = $this->harga->quotil($id);

        $judul = "MSD Quotation Upload";
        $menu = $this->set_menu("dashboard");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/litnim/quoloa",compact("id","datlod"));
        $this->load->view("share/akgnar/hakhi");

    }

    public function pusboq($a=null) {
        if($this->divku != 19)
            redirect(base_url($this->uriku."/logout/"));

        $h = $this->genral->genrui("ts_agrah","msfile","","id_rha",$a);
            if($h){
            $akt=$this->genral->tamakt('Hapus BoQ #'.$a,$this->usrku);
                 $this->genral->rimsan(3);
                redirect(base_url($this->uriku."/"));
            }else{
                $this->genral->rimsan(4);
                redirect(base_url($this->uriku."/")); 
            }
    }

    public function mintil($id = null) {
        if($this->divku != 19)
            redirect(base_url($this->uriku."/logout/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->harga->isiharga();
            if($edt != false) {
                 $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/"));
            } else {
                 $this->genral->rimsan(2);
                redirect(base_url($this->uriku."/"));
            }
            exit();
        }

        if($id == null) redirect(base_url($this->uriku."/logout/"));
        $judul = "Detil Harga";
        $menu = $this->set_menu("dashboard");
        $dafrid=$this->harga->harga_bid($id);
        $dafmua=$this->harga->semua();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();


                
            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
            $this->load->view("share/litnim/mintil",compact("dafrid","daftar_jabatan","daftar_divisi","id"));
            $this->load->view("share/akgnar/hakhi");
        
    }

// akhir Manage Service 


// awal Network Development 
    public function kaarea() {
        //if($this->divku != 18 || $this->divku != 12)
           // redirect(base_url($this->uriku."/logout/"));

        $post = $this->input->post();
        $nalok=$this->input->post("nalok");

        if(isset($post["btnSimpan"])) {
            $sim = $this->netdev->simrea();
            if($sim != false) {
                $akt=$this->pengguna->tamakt('Permintaan Buka Area '.$nalok,$this->usrku);
                 $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/kaarea/"));
            } else {
                 $this->genral->rimsan(2); 
                redirect(base_url($this->uriku."/kaarea/"));
            }
            exit();
        }


        $judul = "Permintaan Buka Area";
        $menu = $this->set_menu("kaarea");

        $datdiv = $this->divisi->pildiv(18); // Network Dev. Dept.
        $drilok = $this->genral->gencbo("app_latrop_aeraak","rilok");
        $dsalok = $this->genral->gencbo("app_latrop_aeraak","salok");
        $dtalok = $this->genral->gencbo("app_latrop_aeraak","talok");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/aeraak/kareru",compact("datdiv","dapr","sepr","drilok","dsalok","dtalok"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function hatran($id=null) {

        $area=$this->netdev->pilrea($id); 
        $file=json_decode($area->piran);
        foreach($file as $f){
           $naf=$f->file;
           $juf=$f->judul;
        }
    
        $judul = "Detil Lampiran";
        $menu = $this->set_menu("dashboard");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/lilam",compact("id","naf","juf"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function dafcpo($id) {
        //if($this->divku != 18 || $this->divku != 12)
           // redirect(base_url($this->uriku."/logout/"));

        $post = $this->input->post();
        $ncpo = $this->input->post("nakli");

        if(isset($post["btnSimpan"])) {
            $sim = $this->netdev->simcpo();
            if($sim != false) {
                $akt=$this->pengguna->tamakt('Isi Daftar Customer PO a/n '.$ncpo,$this->usrku);
                 $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/dafcpo/".$id));
            } else {
                 $this->genral->rimsan(2); 
                redirect(base_url($this->uriku."/dafcpo/".$id));
            }
            exit();
        }

        $judul = "Isi Daftar Customer PO";
        $menu = $this->set_menu("dashboard");

        $dafcpo=$this->genral->pildat("app_latrop_aeraak_cpo","idrea",$id);

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/remmoc/sipepo",compact("id","dafcpo"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function puscpo($id,$pi) {

        $h = $this->genral->pilpus("app_latrop_aeraak_cpo","idkli",$id);
            if($h){
            $akt=$this->pengguna->tamakt('Hapus klien id # '.$id,$this->usrku);
             $this->genral->rimsan(3);
            }else{
             $this->genral->rimsan(4);
            }  
            redirect(base_url($this->uriku."/dafcpo/".$pi));
    }

    public function dafodp($id) {
        //if($this->divku != 18 || $this->divku != 12)
           // redirect(base_url($this->uriku."/logout/"));

        $post = $this->input->post();
        $ncpo = $this->input->post("korba");

        if(isset($post["btnSimpan"])) {
            $sim = $this->genral->gensim("app_latrop_aeraak_odp");
            if($sim != false) {
                $akt=$this->pengguna->tamakt('Isi ODP '.$ncpo,$this->usrku);
                 $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/dafodp/".$id));
            } else {
                 $this->genral->rimsan(2); 
                redirect(base_url($this->uriku."/dafodp/".$id));
            }
            exit();
        }

        $judul = "Isi Data ODP Baru";
        $menu = $this->set_menu("dashboard");

        $dafodp=$this->genral->pildat("app_latrop_aeraak_odp","idrea",$id);

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/remmoc/sidaod",compact("id","dafodp"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function pusodp($id,$pi) {

        $h = $this->genral->pilpus("app_latrop_aeraak_odp","idodp",$id);
            if($h){
            $akt=$this->pengguna->tamakt('Hapus ODP id # '.$id,$this->usrku);
             $this->genral->rimsan(3);
            }else{
             $this->genral->rimsan(4);
            }  
            redirect(base_url($this->uriku."/dafodp/".$pi));
    }

    public function dafkom($id) {
        //if($this->divku != 18 || $this->divku != 12)
           // redirect(base_url($this->uriku."/logout/"));

        $post = $this->input->post();
        $ncpo = $this->input->post("korba");

        if(isset($post["btnSimpan"])) {
            $sim = $this->genral->gensim("app_latrop_aeraak_kom");
            if($sim != false) {
                $akt=$this->pengguna->tamakt('Isi ODP '.$ncpo,$this->usrku);
                 $this->genral->rimsan(1);
                redirect(base_url($this->uriku."/dafkom/".$id));
            } else {
                 $this->genral->rimsan(2); 
                redirect(base_url($this->uriku."/dafkom/".$id));
            }
            exit();
        }

        $judul = "Isi Kompetitor";
        $menu = $this->set_menu("dashboard");

        $dafkom=$this->genral->pildat("app_latrop_aeraak_kom","idrea",$id);

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/remmoc/sidako",compact("id","dafkom"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function puskom($id,$pi) {

        $h = $this->genral->pilpus("app_latrop_aeraak_kom","idkom",$id);
            if($h){
            $akt=$this->pengguna->tamakt('Hapus kompetitor id # '.$id,$this->usrku);
             $this->genral->rimsan(3);
            }else{
             $this->genral->rimsan(4);
            }  
            redirect(base_url($this->uriku."/dafkom/".$pi));
    }

    public function dafapv($id) {
        //if($this->divku != 18 || $this->divku != 12)
           // redirect(base_url($this->uriku."/logout/"));

        $post = $this->input->post();

        if(isset($post["btnSimpan"])) {
            $sim = $this->genral->gensim("app_latrop_aeraak_apv");
            if($sim != false) {

                    if($this->idaku==154) {
                        $akt=$this->pengguna->tamakt('Approval Satu IoM oleh '.$this->idaku,$this->usrku);
                    }elseif($this->idaku==5){
                        $akt=$this->pengguna->tamakt('Approval Dua IoM oleh '.$this->idaku,$this->usrku);    
                    }
                 $this->genral->rimsan(1);
                
                redirect(base_url($this->uriku."/dafapv/".$id));
            } else {
                 $this->genral->rimsan(2); 
                redirect(base_url($this->uriku."/dafapv/".$id));
            }
            exit();
        }



        $judul = "Persetujuan IoM";
        $menu = $this->set_menu("dashboard");

        $dafiom=$this->genral->pildat("v_app_latrop_aeraak","idrea",$id);
        $dafcpo=$this->genral->pildat("app_latrop_aeraak_cpo","idrea",$id);
        $dafodp=$this->genral->pildat("app_latrop_aeraak_odp","idrea",$id);
        $dafkom=$this->genral->pildat("app_latrop_aeraak_kom","idrea",$id);

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/remmoc/hatiom",compact("id","dafiom","dafcpo","dafodp","dafkom"));
        $this->load->view("share/akgnar/hakhi");
    }


// akhir Network Development

// awal Pembelian

    public function talian() {

        $post = $this->input->post();
        $idta=$this->input->post('idta');

        if(isset($post["btnSimpan"])) {
            $sim = $this->talian->simtal();
                if ($this->session->userdata('id_spv') == 38) {
                    $data = array(
                        "status_approve" => 1
                    );
                    $this->talian->autoApprove($sim, $data);
                }
            if($sim != false) {
                $akt=$this->pengguna->tamakt('Permintaan Baru No. PR '.$idta,$this->usrku);
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> Data berhasil disimpan </div>'); 
                redirect(base_url($this->uriku."/sirang/".$idta));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data gagal disimpan </div>');  
                redirect(base_url($this->uriku."/sirang/".$idta."?err"));
            }
            exit();
        }

        $dapr=$this->talian->pureku(); // user request
        $dapr_prv=$this->talian->pureprove(); // approve request
        $sepr=$this->talian->dapure(); // semua request

        $alamat=$this->talian->alamat(); // semua request
   
        $judul = "Permintaan Pembelian";
        $menu = $this->set_menu("talian");


        if($this->db->get('app_latrop_nailat')->num_rows()==0){
        $nopr=1;    
        }else{
        $np=$this->db->order_by('idta','desc')->limit(1)->get('app_latrop_nailat')->row();
        $nopr=$np->idta+1;
        }

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/nailat/taliru",compact("nopr","dapr","sepr","dapr_prv", "alamat"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function approval($id)
    {
        $data = array(
            "status_approve" => 1,
            "tgl_approve" => date('Y-m-d H:i:s'),
        );

        $update = $this->talian->approval($id, $data);

        $nailat = $this->talian->get_nailat($id);
        $pengguna = $this->talian->get_pengguna($nailat->usbu);
        $spv = $this->talian->get_pengguna($pengguna->id_spv);

        if ($update) {

            $n=sprintf("%04d",$id);

            $this->load->library('phpmailer_lib');
            $mail = $this->phpmailer_lib->load();

            $email_admin = 'nooralizah@jlm.net.id';
            $nama_admin = 'Aplikasi Portal - PR';
            $password_admin = 'jlm2021-@#$';

            $mail->isSMTP();
            $mail->SMTPKeepAlive = true;
            $mail->Charset  = 'UTF-8';
            $mail->IsHTML(true);
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = true;
            $mail->Host = "mail.jlm.net.id";
            $mail->SMTPSecure = "tls";
            $mail->Port = 587;
            $mail->Username = $email_admin;
            $mail->Password = $password_admin;
            $mail->WordWrap = 500;

            $mail->setFrom($email_admin, $nama_admin);
            $mail->addAddress('olivia@jlm.net.id');
            // $mail->addAddress('noor.afifah.r25@gmail.com');

            $mail->Subject          = "Purchase Request - Pemohon ".$pengguna->nama_lengkap.", Dept. ".$pengguna->nm_div.", No. PR. ".$n;
            $mail_data['subject']   = $n.'_'.$pengguna->nama_lengkap;
            $mail_data['induk']     = 'Permintaan - '.$n.'_'.$pengguna->nama_lengkap;
            // pesan

            $mail->Body = "
                <br>Kepada Bpk/Ibu Yth
                <br><br>
                <p>Berikut Reminder Purchase Request yang telah di approve, atas nama:</p>
            <br>Nama Lengkap: ". $pengguna->nama_lengkap."<br>No. PR: ".$n."<br>Department: ".$pengguna->nm_div."<br>Tanggal dibutuhkan: ".date('d M Y', strtotime($nailat->tgta))."<br><br>Sekian dan Terimah Kasih. <br> url: <a target='_blank' href='".base_url($this->uriku."/talian")."'>Portal Bnetfit</a>";

            if ($mail->send()) {
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Reminder berhasil dikirimkan kepada penanggung jawab PO </div>');
                redirect(base_url($this->uriku."/talian?succ"));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Reminder gagal dikirimkan kepada penanggung jawab PO </div>');
                redirect(base_url($this->uriku."/talian?err"));
            }
        }
        $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Permintaan pembelian barang gagal di approve </div>');
        redirect(base_url($this->uriku."/talian?err"));

    }

    
    public function sirang_detail($id) {

        $jeba=$this->talian->cbjeba();
        $daba=$this->talian->darang($id);
        $depr=$this->talian->depure($id);

        $judul = "Detil Pembelian";
        $menu = $this->set_menu("talian");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/nailat/detail",compact("id","jeba","daba","depr"));
        $this->load->view("share/akgnar/hakhi");
    }

    
    public function sirang_detail_harga($id) {

        $jeba=$this->talian->cbjeba();
        $daba=$this->talian->darang($id);
        $depr=$this->talian->depure($id);

        $judul = "Detil Pembelian";
        $menu = $this->set_menu("talian");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/nailat/detail_harga",compact("id","jeba","daba","depr"));
        $this->load->view("share/akgnar/hakhi");
    }

    
    public function sirang_detail_harga_form() {
        $id = $this->input->post('idre');
    	$ta = $this->input->post('idta');
        $data = array(
            "harga" => $this->input->post('harga'),
        );

        $this->talian->update_harga($id, $data);
        redirect(base_url($this->uriku."/sirang_detail_harga/".$ta));
    }

    
    public function sirang_detail_juba_form() {
        $id = $this->input->post('idre');
        $ta = $this->input->post('idta');
        $data = array(
            "juba" => $this->input->post('juba'),
        );

        $this->talian->update_juba($id, $data);
        redirect(base_url($this->uriku."/sirang_detail_harga/".$ta));
    }

    
    public function sirang($id) {

        $post = $this->input->post();
        
        if(isset($post["btnSimpan"])) {
            $sim = $this->talian->tambar();
            if($sim != false) {
                
                $akt=$this->pengguna->tamakt('Tambah Barang'.$this->input->post('jeba').' '.$this->input->post('jeba').' No. PR '.$id,$this->usrku);

                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data berhasil ditambahkan </div>'); 
                redirect(base_url($this->uriku."/sirang/".$id));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Data gagal ditambahkan </div>');
                redirect(base_url($this->uriku."/sirang/".$id));
            }
            exit();
        }

        $jeba=$this->talian->cbjeba();
        $daba=$this->talian->darang($id);
        $depr=$this->talian->depure($id);

        $judul = "Detil Pembelian";
        $menu = $this->set_menu("talian");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/nailat/sirang",compact("id","jeba","daba","depr"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function harang($id) {

        $h = $this->talian->pusbar($id);
            if($h){
            $akt=$this->pengguna->tamakt('Hapus barang id# '.$idta,$this->usrku);
             $this->genral->rimsan(3);
            }else{
             $this->genral->rimsan(4);
            }  

            redirect(base_url($this->uriku."/sirang/".$idta));
    }

    public function dafset($id = null) {

        $post = $this->input->post();


        if(isset($post["btnSimpan"])) {
            $sim = $this->huroga->isidafset();
            if($sim != false) {
                $silog= $this->input->post('jeasse').'_'.$this->input->post('measse');
                $akt=$this->pengguna->tamakt('simpan asset '.$silog,$this->usrku);
                redirect(base_url($this->uriku."/dafset/?suc"));
            } else {
                redirect(base_url($this->uriku."/dafset/?err"));
            }
            exit();
        }

        $pemasok=$this->huroga->cbpemasok();
        $lokasi=$this->huroga->cblokasi();
        $jenis=$this->huroga->cbjenis();
        $merk=$this->huroga->cbmerk();
        $user=$this->hris->cbuser();
        $dfasset=$this->huroga->daftar();
        
        $judul = "Aplikasi Portal - Daftar Asset";
        $menu = $this->set_menu("menol");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/dasset/tessad",compact("dfasset","pemasok","lokasi","jenis","merk","user"));
        $this->load->view("share/akgnar/hakhi");

    }

    public function pusset($i=null){
        $h = $this->huroga->pusdafset($i);
            if($h)
                redirect(base_url($this->uriku."/dafset/?hsuc"));
            else
                redirect(base_url($this->uriku."/dafset/?herr"));       
    }

// akhir Pembelian




    public function profilku($id = null) {
        //if($this->session->userdata("id_jabatan") != 1)
            //redirect(base_url($this->uriku."/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->editpro();
            if($edt != false) {
                $akt=$this->pengguna->tamakt('ubah sandi ',$this->usrku);
                redirect(base_url($this->uriku."/profilku/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url($this->uriku."/profilku/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        $judul = "Aplikasi Portal - Ubah Sandi";
        $menu = $this->set_menu("uprofil");

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {

            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
            $this->load->view($this->uriku."/person/profilku",compact("pengguna"));
            $this->load->view("share/akgnar/hakhi");
        } else redirect(base_url($this->uriku."/pengguna/"));
    }




    public function profilak() {
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));

        $post = $this->input->post();
        $thn=$this->input->post("thn");
        
        if(!empty($thn)){
            
            $datakt = $this->aktif->rekap($thn);
            
        }else{
            $thn=date('Y');
            $datakt = $this->aktif->rekap($thn);
        }
    

        $judul = $this->session->userdata('nm_div')." Department - Aktifitas";
        $menu = $this->set_menu("aprofil");
        $daftugas = $this->aktif->tugas();
        $dafaktif = $this->aktif->laporanpilih();
        $hirdat = $this->aktif->dakhir();
        $dafstaff = $this->aktif->staff();
        $datakt4m=$this->aktif->rekap4m($thn,0);    
        
        if(isset($_GET["staff"])){
        $kid=explode('-', $_GET["staff"]);
        $dafpilih = $this->aktif->laporanstaff($_GET["staff"]);
        $datakt4m = $this->aktif->rekap4m($thn,$kid[0]);
        }


        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/aktifi/dashatif",compact("thn","datakt4m","datakt","hirdat","daftugas","dafaktif","dafstaff","dafpilih"));
        $this->load->view("share/akgnar/hakhi",compact("judul","menu"));

    }

    public function atiftamb() {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url($this->uriku."/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
             
            $atifspn = $this->aktif->atiftamb();
            if($atifspn != false) {
                $akt=$this->pengguna->tamakt('simpan aktifitas baru ',$this->usrku);
                redirect(base_url($this->uriku."/atifwktu/?succ"));
            } else {
                redirect(base_url($this->uriku."/atiftamb/?err"));
            }
            exit();
        }

        $judul = "Aplikasi Portal - Aktifitas Baru";
        $menu = $this->set_menu("aprofil");
        $daftugas = $this->aktif->tugas();
        $dafaktif = $this->aktif->laporan();
        

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/aktifi/atiftamb",compact("daftugas","dafaktif"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function atifwktu(){
        $post = $this->input->post();
        if(isset($post["btnSelesai"])) {
             
            $atifsls = $this->aktif->atifselesai($post);
            if($atifsls != false) {
                redirect(base_url($this->uriku."/profilak/?succ"));
            } else {
                redirect(base_url($this->uriku."/atifwktu/?err"));
            }
            exit();
        }

        $judul = "Aplikasi Portal - Aktifitas Harian";
        $menu = $this->set_menu("aprofil");
        $daftugas = $this->aktif->tugas();
        $dafaktif = $this->aktif->laporanpilih();
        $hirdat = $this->aktif->dakhir();

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/aktifi/atifwktu",compact("hirdat","daftugas","dafaktif"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function atifubah($id = null) {
    //if($this->session->userdata("id_jabatan") != 1)
           // redirect(base_url($this->uriku."/"));
        
        $post = $this->input->post();
        if(isset($post["btnUbah"])) {
             
            $atifuspn = $this->aktif->atifubah();
            if($atifuspn != false) {
                $akt=$this->pengguna->tamakt('ubah aktifitas ',$this->usrku);
                redirect(base_url($this->uriku."/profilak/?succ"));
            } else {
                redirect(base_url($this->uriku."/atifubah/?err"));
            }
            exit();
        }

        $judul = "Aktifitas Ubah";
        $menu = $this->set_menu("aprofil");
        $dafakpil = $this->aktif->atifpil($id);
       
        

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/aktifi/atifubah",compact("dafakpil"));
        $this->load->view("share/akgnar/hakhi");
    }





    function imp2db()
    {
        $this->load->library('excel');

        $lf=$this->excel_import_model->lfile();

        foreach ($lf as $v) {
        	$idf=$v->id;
        	$naf=json_decode($v->nafi);
        }
        foreach($naf as $item){
        	$rfile=$item->file;
        }

        if(isset($rfile)){
            $path = "assets/uploads/tsalb/".$rfile; //$_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $id = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $us = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $na = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $pr = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $al = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $em = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $hp = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $re = $worksheet->getCellByColumnAndRow(7, $row)->getValue();


                    $data[] = array(
                        'id'        =>  $id,
                        'idfile'    =>  $idf,
                        'username'  =>  $us,
                        'nama'      =>  $na,
                        'produk'    =>  $pr,
                        'alamat'    =>  $al,
                        'email'     =>  $em,
                        'hp'        =>  $hp,
                        'registrasi'=>  $re
                    );
                }
            }
            $this->excel_import_model->insertblast($data);
            $jum=$highestRow-1;
            echo $jum.' data berhasil di import';
        }   
    }

    public function toexcel($pil=null) {
    $nafile=$pil;
    //$spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    //$spreadsheet->getDefaultStyle()->getFont()->setSize(8);
    $dafphi = $this->bayar->cekdata($pil);
    $dafrin = $this->bayar->paymdet($pil);
    /*$spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'Coba Lagi !');

    $writer = new Xlsx($spreadsheet);
    $writer->save('coba.xlsx');*/
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_phi.xlsx');
    $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(11);
    $worksheet = $spreadsheet->getActiveSheet();

    //$worksheet->getCell('B2')->setValue($naleng);
    //$worksheet->getCell('B3')->setValue($this->session->userdata('id_div'));
    //$worksheet->getCell('B4')->setValue($this->session->userdata('id_jabatan'));
    $no=1;
    $n=3;
    foreach ($dafphi as $h) {
    $worksheet->getCell('A'.$n)->setValue($no++);
    $worksheet->getCell('B'.$n)->setValue($h->username);
    $worksheet->getCell('C'.$n)->setValue(substr($h->nama,0,6));
    $worksheet->getCell('D'.$n)->setValue($h->thn);
    $worksheet->getCell('E'.$n)->setValue($h->JAN);  
    $worksheet->getCell('F'.$n)->setValue($h->FEB);
    $worksheet->getCell('G'.$n)->setValue($h->MAR);
    $worksheet->getCell('H'.$n)->setValue($h->APR);
    $worksheet->getCell('I'.$n)->setValue($h->MEI);
    $worksheet->getCell('J'.$n)->setValue($h->JUN);
    $worksheet->getCell('K'.$n)->setValue($h->JUL);
    $worksheet->getCell('L'.$n)->setValue($h->AGU);  
    $worksheet->getCell('M'.$n)->setValue($h->SEP);
    $worksheet->getCell('N'.$n)->setValue($h->OKT);
    $worksheet->getCell('O'.$n)->setValue($h->NOP);
    $worksheet->getCell('P'.$n)->setValue($h->DES);
    $worksheet->getCell('Q'.$n)->setValue($h->TOTAL);
    $n++;   
    }
    $nr=$n+2;
    $nr1=$nr+1;
    $worksheet->mergeCells('A'.$nr.':P'.$nr);
    $worksheet->getCell('A'.$nr)->setValue('RINCIAN TRANSAKSI');
    $worksheet->getStyle('A'.$nr)->getAlignment()->setHorizontal('center');
    $worksheet->getStyle('A'.$nr)->getFont()->setSize(14);

    $worksheet->getCell('A'.$nr1)->setValue('No.');
    $worksheet->getCell('B'.$nr1)->setValue('Tahun');

    $worksheet->mergeCells('C'.$nr1.':D'.$nr1);
    $worksheet->getCell('C'.$nr1)->setValue('ID#');
    $worksheet->getStyle('A'.$nr1.':P'.$nr1)->getAlignment()->setHorizontal('center');

    $worksheet->mergeCells('E'.$nr1.':G'.$nr1);
    $worksheet->getCell('E'.$nr1)->setValue('Nama');
    $worksheet->getStyle('E'.$nr1)->getAlignment()->setHorizontal('center');

    $worksheet->getCell('H'.$nr1)->setValue('Tgl. Bayar');
    $worksheet->getCell('I'.$nr1)->setValue('Periode');
    $worksheet->getCell('J'.$nr1)->setValue('Harga');
    $worksheet->getCell('K'.$nr1)->setValue('PPN');
    $worksheet->getCell('L'.$nr1)->setValue('Diskon');
    $worksheet->getCell('M'.$nr1)->setValue('Metoda');

    $worksheet->mergeCells('N'.$nr1.':P'.$nr1);
    $worksheet->getCell('N'.$nr1)->setValue('Kasir');
    $no2=1;
    $nr2=$nr+2;
    foreach ($dafrin as $h) {
    $worksheet->getCell('A'.$nr2)->setValue($no2++);
    $worksheet->getCell('B'.$nr2)->setValue($h->thn);
    $worksheet->mergeCells('C'.$nr2.':D'.$nr2);
    $worksheet->getCell('C'.$nr2)->setValue($h->username);
    $worksheet->getStyle('C'.$nr2)->getAlignment()->setHorizontal('center');

    $worksheet->mergeCells('E'.$nr2.':G'.$nr2);
    $worksheet->getCell('E'.$nr2)->setValue($h->name);
    $worksheet->getCell('H'.$nr2)->setValue($h->pay_date);
    $worksheet->getCell('I'.$nr2)->setValue($h->bln);   
    $worksheet->getCell('J'.$nr2)->setValue($h->price);
    $worksheet->getCell('K'.$nr2)->setValue($h->ppn);
    $worksheet->getCell('L'.$nr2)->setValue($h->discount);
    $worksheet->getCell('M'.$nr2)->setValue($h->vendor);
    $worksheet->mergeCells('N'.$nr2.':P'.$nr2);
    $worksheet->getCell('N'.$nr2)->setValue($h->kasir);
    $nr2++;
    }

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //header('Content-Disposition: attachment; filename='.date('Ymd').'_'.$nafile.'Payment_History.xlsx');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename='.date('YmdHis').'-'.$nafile.'-Payment_History.xls');    
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



    redirect(base_url($this->uriku."/pymhis/"));

    }


    Public function pdfgen($id=null){
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->SetMargins(12,1,1);
        $pdf->Image(base_url('/assets/images/jlmlogo.png'),12,10,-200);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(30,4,' ',0,0,'C');
        $pdf->Cell(125,4,' ',0,0,'C');
        $pdf->Cell(30,4,' INTERNAL ',1,1,'C');


        $datu = $this->db->where('idt',$id)->get('v_fat_knabraul')->result();
        foreach ($datu as $d){

        	$pdf->Cell(40,4,'',0,0,'C');
        	$pdf->Cell(105,4,'',0,0,'C');
        	$pdf->SetFont('Arial','',9);
        	$pdf->Cell(20,4,'No.',0,0,'L');
        	$pdf->Cell(40,4,': '.$d->urt,0,1,'L');

        	$pdf->Cell(40,4,'',0,0,'C');
        	$pdf->SetFont('Arial','B',13);
        	$pdf->Cell(105,4,'BUKTI PENGELUARAN BANK',0,0,'C');
        	$pdf->SetFont('Arial','',9);
        	$pdf->Cell(20,4,'Tanggal ',0,0,'L');
        	$pdf->Cell(20,4,': '.tgl_indome($d->tgt),0,1,'L');

        	$pdf->Cell(40,4,'',0,0,'C');
        	$pdf->Cell(105,4,'',0,0,'C');
        	$pdf->Cell(20,4,'Tgl. Invoice',0,0,'L');
        	$pdf->Cell(20,4,': '.tgl_indome($d->tgi),0,1,'L');

        	$pdf->Cell(10,2,'',0,1); // Jarak spasi kosong

        	$pdf->SetFont('Arial','',10);
            $pdf->Cell(30,4,'Dibayar kepada',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->Cell(40,4,$d->dik,0,1);
            $pdf->Cell(30,4,'Nominal',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->Cell(40,4,number_format($d->nit).',-',0,1); 
            $pdf->Cell(30,4,'Terbilang',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->SetFont('Arial','B',10);
            $pdf->setFillColor(230,230,230);
            $pdf->Cell(150,4,terbilang($d->nit),0,1,'L',1); 
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(30,4,'Supplier invoice',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->Cell(40,4,$d->sui,0,1); 
            $ppn=$d->p10;
            $pph=$d->p23;
            $naf=$d->dik;

        }
        $n=1;
        $dadu = $this->db->where('idt',$id)->get('fat_knabraul_ket')->result();
        foreach ($dadu as $d){
            if($n==1){ $k='Keterangan'; $t=' : ';}else{$k='';$t='   ';}

            $pdf->Cell(30,4,$k,0,0);
            $pdf->Cell(5,4,$t,0,0);
            $pdf->Cell(115,4,$d->ket,0,0);
            $pdf->Cell(10,4,'Rp. ',0,0);
            $pdf->Cell(25,4,number_format($d->nit).',-',0,1,'R'); 
            $n++;
        }
        $cekdat=$this->db->where('idt',$id)->get('fat_knabraul_ket')->num_rows();


        if($cekdat==1){$x=4;}elseif($cekdat==2){$x=3;}elseif($cekdat==3){$x=2;}else{$x=1;}
        for ($i=0; $i < $x; $i++) { 
            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(130,4,' ',0,0);
            $pdf->Cell(25,4,'-',0,1,'R'); 
        }

        $suto = $this->db->select("sum(nit) as subtot")->where('idt',$id)->get('fat_knabraul_ket')->row();
            $pdf->Cell(30,3,'',0,0);
            $pdf->Cell(130,3,'',0,0,'R');
            $pdf->Cell(25,3,"-------------------------- +",0,1,'R'); 

            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(127,4,'Subtotal Rp.',0,0,'R');
            $pdf->Cell(28,4,number_format($suto->subtot).",-",0,1,'R'); 

            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(127,4,'PPN Rp.',0,0,'R');
            $pdf->Cell(28,4,number_format($ppn).",-",0,1,'R'); 

            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(127,4,'PPH 23 Rp.',0,0,'R');
            $pdf->Cell(28,4,number_format(-1*($pph)).",-",0,1,'R'); 


			$pdf->Cell(185,1,' ','T',1); // jarak spasi kosong
			//$pdf->Cell(185,1,'',0,1); // jarak spasi kosong

            $pdf->SetFont('Arial','I',10);
            $pdf->Cell(125,4,'                     Creditor A/C  :','T',0);
            $pdf->Cell(60,4,' PO No. : ','T',1);
            $pdf->Cell(125,4,'                   Bank Transfer :','T',0);
            $pdf->Cell(60,4,'SPK No. : ','T',1);  
            $pdf->Cell(185,1,'',0,1);
            $pdf->Cell(185,1,' ','T',1); // jarak spasi kosong

            $pdf->SetFont('Arial','',10);
     		$pdf->Cell(30,5,'Disiapkan ',1,0,'C');
        	$pdf->Cell(50,5,'Verifikasi',1,0,'C');
        	$pdf->Cell(75,5,'Menyetujui',1,0,'C');
        	$pdf->Cell(30,5,'Penerima',1,1,'C');

			$pdf->Cell(30,18,' ',1,0);
			$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(30,18,' ',1,1);           

        	$pdf->SetFont('Arial','',9);
			$pdf->Cell(30,4,'Finance A/P',1,0,'C');
			$pdf->Cell(25,4,'Presales',1,0,'C');
        	$pdf->Cell(25,4,'FAM',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(30,4,' ',1,1,'C');
        	$pdf->SetFont('Arial','',8);
        	$pdf->Cell(30,4,'(                     )',1,0,'C');
			$pdf->Cell(25,4,'(                     )',1,0,'C');
        	$pdf->Cell(25,4,'( Henry )',1,0,'C');
        	$pdf->Cell(25,4,'( Victor Irianto )',1,0,'C');
        	$pdf->Cell(25,4,'( Yulius Purnama )',1,0,'C');
        	$pdf->Cell(25,4,'( Edwind J.B. )',1,0,'C');
        	$pdf->Cell(30,4,'(                     )',1,1,'C');
        // ob_start();
        $namafile=date('ymdHi').'-'.$naf;
        $pdf->Output('D','BUK'.$namafile.'.pdf');
        // $pdf->Output();
        $this->huroga->hittak($id);

    }
    
    private function set_menu($active) {
        $menu = [
            "dashboard"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",
            "pengguna"=>"",
            "survey"=>"",
            "divisi"=>"", 
            "jabatan"=>"",
            "kaarea"=>"",
            "talian"=>"",           
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "meena"=>"",
            "metuj"=>"",
            "medel"=>"",
            "mesem"=>"",
            "mesep"=>"",
            "meseb"=>"",
            "ladload"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }


    public function penggunatamb() {
    if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url($this->uriku."/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $tbh = $this->pengguna->tambah();
            if($tbh != false) {
                redirect(base_url("/nuber/penggunatamb/?succ"));
            } else {
                redirect(base_url("/nuber/penggunatamb/?err"));
            }
            exit();
        }

        $judul = "Tambah Pengguna";
        $menu = $this->set_menu("pengguna");
        $daftas = $this->pengguna->data_atasan();
        $dafhed = $this->pengguna->data_head();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/penggunatamb",compact("dafhed","daftas","daftar_jabatan","daftar_divisi"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function penggunaedit($id = null) {

        $this->load->model("Lokasi_model","lokasi");

        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url("/nuber/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->edit();
            if($edt != false) {
                redirect(base_url("/nuber/penggunaedit/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url("/nuber/penggunaedit/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url("/nuber/pengguna/"));
        $judul = "Edit Pengguna";
        $menu = $this->set_menu("pengguna");
        $datibo = $this->lokasi->usribo();
        $daftas = $this->pengguna->data_atasan();
        $dafhed = $this->pengguna->data_head();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("share/akgnar/hawal",compact("judul","menu"));
            $this->load->view("/nuber/peguna/penggunaedit",compact("dafhed","pengguna","datibo","daftar_jabatan","daftar_divisi","daftas"));
            $this->load->view("share/akgnar/hakhi");
        } else redirect(base_url("/nuber/pengguna/"));
    }



    public function divisi() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11)
            redirect(base_url($this->uriku."/"));


        $judul = "Kelola Divisi";
        $menu = $this->set_menu("divisi");

        //if($this->session->userdata("id_divisi") == 37){
        $daftar_divisi = $this->divisi->divisi_semua();
        //}else{
        //$daftar_divisi = $this->divisi->divisi_sesama($this->session->userdata("id_divisi"));    
        //}

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/divisi",compact("daftar_divisi"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function tambdiv() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11)
            redirect(base_url($this->uriku."/"));

        $post = $this->input->post();
        if(isset($post["btnSimpan"])) {
            $tbh = $this->divisi->divtamb();
            if($tbh != false) {
                redirect(base_url($this->uriku."/divisi/?succ"));
            } else {
                redirect(base_url($this->uriku."/divisi/?err"));
            }
            exit();
        }


    }

    public function divisiedit($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11)
            redirect(base_url($this->uriku."/"));

        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $edt = $this->divisi->divedit();
            if($edt != false) {
                redirect(base_url($this->uriku."/divisiedit/".$post["id_dinas"]."?succ"));
            } else {
                redirect(base_url($this->uriku."/divisiedit/".$post["id_dinas"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url($this->uriku."/divisi"));

        $judul = "Edit Divisi";
        $menu = $this->set_menu("divisi");
        $divisi = $this->divisi->ambil_berdasarkan_id($id);

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/divisiedit",compact("divisi"));
        $this->load->view("share/akgnar/hakhi");
    }



    public function tambahjabatan() {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url($this->uriku."/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $tbh = $this->jabatan->tambah();
            if($tbh != false) {
                redirect(base_url($this->uriku."/tambahjabatan/?succ"));
            } else {
                redirect(base_url($this->uriku."/tambahjabatan/?err"));
            }
            exit();
        }

        $judul = "Tambah Jabatan";
        $menu = $this->set_menu("jabatan");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/tambahjabatan");
        $this->load->view("share/akgnar/hakhi");
    }

    public function jabatanedit($id = null) {
        if($this->session->userdata("id_jabatan") != 1 && $this->session->userdata("id_jabatan") != 11 )
            redirect(base_url($this->uriku."/"));
        $post = $this->input->post();

        if(isset($post["btnSubmit"])) {
            $edt = $this->jabatan->edit();
            if($edt != false) {
                redirect(base_url($this->uriku."/jabatanedit/".$post["id_jabatan"]."?succ"));
            } else {
                redirect(base_url($this->uriku."/jabatanedit/".$post["id_jabatan"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url($this->uriku."/jabatan"));

        $judul = "Edit Jabatan";
        $menu = $this->set_menu("jabatan");
        $jabatan = $this->jabatan->ambil_berdasarkan_id($id);

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view($this->uriku."/peguna/jabatanedit",compact("jabatan"));
        $this->load->view("share/akgnar/hakhi");
    }



    public function loadexcel() {
    $nafile=str_replace(' ', '', $this->session->userdata('nama_lengkap'));
    $naleng=$this->session->userdata('nama_lengkap');
    $dafaktif = $this->aktif->laporanpilih();
    /*$spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'Coba Lagi !');

    $writer = new Xlsx($spreadsheet);
    $writer->save('coba.xlsx');*/
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_akt.xlsx');

    $worksheet = $spreadsheet->getActiveSheet();
    $worksheet->getCell('B2')->setValue($naleng);
    $worksheet->getCell('B3')->setValue($this->session->userdata('id_div'));
    $worksheet->getCell('B4')->setValue($this->session->userdata('id_jabatan'));

    $n=6;
    foreach ($dafaktif as $a) {
    $worksheet->getCell('A'.$n)->setValue($a->date);
    $worksheet->getCell('B'.$n)->setValue($a->customer);
    $worksheet->getCell('C'.$n)->setValue($a->jobs);
    $worksheet->getCell('D'.$n)->setValue($a->case);
    $worksheet->getCell('E'.$n)->setValue($a->issues);  
    $worksheet->getCell('F'.$n)->setValue($a->action);
    $n++;   
    }


    //$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');

    //$writer->save('assets/temp/'.date('Ymd').'_'.$nafile.'DailyReport.xls');

    
    /*header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename='.date('Ymd').'_'.$nafile.'DailyReport.xls');
    header('Cache-Control: max-age=0');
    $writer = new Xlsx($spreadsheet);
    $writer->save("php://output");*/
    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename='.date('Ymd').'_'.$nafile.'DailyReport.xls');
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



    redirect(base_url($this->uriku."/profilak/"));

    }

    public function sendEmail()
    {
        $id = $this->input->post('id_rha');
        $dafmserv=$this->harga->gmanser_id($id);

        $this->load->library('phpmailer_lib');
        $mail = $this->phpmailer_lib->load();

        $email_admin = 'nooralizah@jlm.net.id';
        $nama_admin = 'Aplikasi Portal - PR';
        $password_admin = 'jlm2021-@#$';

        $mail->isSMTP();
        $mail->SMTPKeepAlive = true;
        $mail->Charset  = 'UTF-8';
        $mail->IsHTML(true);
        $mail->SMTPDebug = 0;
        $mail->SMTPAuth = true;
        $mail->Host = "mail.jlm.net.id";
        $mail->SMTPSecure = "tls";
        $mail->Port = 587;
        $mail->Username = $email_admin;
        $mail->Password = $password_admin;
        $mail->WordWrap = 250;

        $mail->setFrom($email_admin, $nama_admin);
        foreach ($this->input->post('email') as $value) {
            $mail->addAddress($value);
        }

        $mail->Subject          = 'BOQ - '.$dafmserv->id_rha.' - '.$dafmserv->nm_custo;
        $mail_data['subject']   = $dafmserv->id_rha.'_'.$dafmserv->nm_custo;
        $mail_data['induk']     = 'Lampiran BOQ - '.$dafmserv->id_rha.' - '.$dafmserv->nm_custo;
        // pesan
        $mail->Body = 'Berikut Terlampir BOQ Permintaan Harga';
        
            foreach (json_decode($dafmserv->msfile) as $row) {
                $gambar = $row->file;
                $path=$_SERVER['DOCUMENT_ROOT'].'assets/uploads/quo/'.$gambar;
                $mail->addAttachment($path);
            }

        $params = array(
            "id_rha" => $this->input->post('id_rha'),
            "status" => 1,
            "created_at" => date('Y-m-d H:i:s')
        );

        $status = array(
            "id_status_email" => 1
        );

        if ($mail->send()) {
            $this->harga->insert_log_email($params);
            $this->harga->update_status_agrah($id, $status);
            redirect(base_url($this->uriku."?succ"));
        } else {
            redirect(base_url($this->uriku."?err"));
        }
    }

    public function export_kabel()
    {
        $data = $this->rolout->tamkab();

        $spreadsheet = new Spreadsheet();

        $spreadsheet->getActiveSheet()
                    ->setCellValue('A1', 'No')
                    ->setCellValue('B1', 'ID Kabel')
                    ->setCellValue('C1', 'Tanggal')
                    ->setCellValue('D1', 'Alamat')
                    ->setCellValue('E1', 'Segment')
                    ->setCellValue('F1', 'Jenis Kabel')
                    ->setCellValue('G1', 'Panjang tarikan')
                    ->getStyle('A1:G1')->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('F28A8C');
                    
        $no = 1;
        $x = 2;
        foreach($data as $row)
        {

            $spreadsheet->getActiveSheet()
                        ->setCellValue('A'.$x, $no)
                        ->setCellValue('B'.$x, $row->idkab)
                        ->setCellValue('C'.$x, $row->tgpas)
                        ->setCellValue('D'.$x, $row->jakab)
                        ->setCellValue('E'.$x, $row->sekab)
                        ->setCellValue('F'.$x, $row->jekab)
                        ->setCellValue('G'.$x, $row->takab);
            $no++;
            $x++;
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Data Realisasi Gelar Kabel.xlsx"');
        header('Cache-Control: max-age=0');
    
        $writer->save('php://output');
    }

    public function reject_po()
    {
        $data = array(
            "status_approve" => 3,
            "alasan" => $this->input->post('alasan'),
            "tgl_approve" => date('Y-m-d H:i:s'),
        );

        $update = $this->talian->ignore_po($this->input->post('idta'), $data);

        if ($update) {
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Purchasing Order Berhasil di Reject </div>');
                redirect(base_url($this->uriku."/talian?succ"));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Purchasing Order Gagal di Reject </div>');
                redirect(base_url($this->uriku."/talian?err"));
            }
    }

    public function ignore_po()
    {
        $update = array(
            "alasan" => $this->input->post('alasan'),
            "tgl_proses" => NULL,
        );
        $this->talian->ignore_po($this->input->post('idta'), $update);

        if ($update) {
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Purchasing Order Berhasil di Ignore </div>');
                redirect(base_url($this->uriku."/talian?succ"));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Purchasing Order Gagal di Ignore </div>');
                redirect(base_url($this->uriku."/talian?err"));
            }
    }

    public function save_purchasing_order()
    {

    	$data = array(
            "idta" => $this->input->post('idta'),
            "penerima" => $this->input->post('penerima'),
            "atas_nama" => $this->input->post('atas_nama'),
            "alamat_penerima" => $this->input->post('alamat_penerima'),
            "notelp_penerima" => $this->input->post('notelp_penerima'),
            "alamat_pengirim" => $this->input->post('alamat_pengirim'),
            "tanggal_po" => $this->input->post('tanggal_po'),
            "po_no" => $this->input->post('po_no'),
            "quatation_no" => $this->input->post('quatation_no'),
            "diskon" => $this->input->post('diskon'),
            "biaya_pengiriman" => $this->input->post('biaya_pengiriman'),
            "ppn" => $this->input->post('ppn'),
            "term_payment" => $this->input->post('term_payment'),
        );
        $id = $this->talian->save_po($data);
        $update = array(
            "tgl_proses" => date('Y-m-d H:i:s')
        );
        $this->talian->update_po($this->input->post('idta'), $update);

        if ($update) {
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Purchasing Order Berhasil di Proses </div>');
                redirect(base_url($this->uriku."/talian?succ"));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Purchasing Order Gagal di Proses </div>');
                redirect(base_url($this->uriku."/talian?err"));
            }
    }

    public function cancel_po($id)
    {
        $data = array('idta' => $id);
        $delete = $this->talian->delete_po($data);
        $update = array(
            "tgl_proses" => NULL
        );
        $this->talian->update_po($id, $update);
         if ($delete) {
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Purchasing Order Berhasil di Cancel </div>');
                redirect(base_url($this->uriku."/talian?succ"));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Purchasing Order Gagal di Cancel </div>');
                redirect(base_url($this->uriku."/talian?err"));
            }
    }

    public function print_po($id)
    {
        $data['get_po'] = $this->talian->get_po($id);
        $data['get_nailat'] = $this->talian->get_nailat($id);
        $data['get_nailat_rel'] = $this->talian->get_nailat_rel($id);

        $this->load->library('pdfgenerator');
        
        $file_pdf = 'PO MAHAVIRA';
        $paper = 'A4';
        $orientation = "portrait";
        
		$html = $this->load->view('generate_po', $data, true);	
        $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);
    }

    public function send_email_permintaan($id)
    {
        $nailat = $this->talian->get_nailat($id);
        $pengguna = $this->talian->get_pengguna($nailat->usbu);
        $spv = $this->talian->get_pengguna($pengguna->id_spv);

        if (isset($spv->email)) {

            $email_penerima = $spv->email;
            $n=sprintf("%04d",$id);

            $this->load->library('phpmailer_lib');
            $mail = $this->phpmailer_lib->load();

            $email_admin = 'nooralizah@jlm.net.id';
            $nama_admin = 'Aplikasi Portal - PR';
            $password_admin = 'jlm2021-@#$';

            $mail->isSMTP();
            $mail->SMTPKeepAlive = true;
            $mail->Charset  = 'UTF-8';
            $mail->IsHTML(true);
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = true;
            $mail->Host = "mail.jlm.net.id";
            $mail->SMTPSecure = "tls";
            $mail->Port = 587;
            $mail->Username = $email_admin;
            $mail->Password = $password_admin;
            $mail->WordWrap = 500;

            $mail->setFrom($email_admin, $nama_admin);
            $mail->addAddress($email_penerima);
            // $mail->addAddress('noor.afifah.r25@gmail.com');

            $mail->Subject          = "Approval Request - Pemohon ".$pengguna->nama_lengkap.", Dept. ".$pengguna->nm_div.", No. PR. ".$n;
            $mail_data['subject']   = $n.'_'.$pengguna->nama_lengkap;
            $mail_data['induk']     = 'Permintaan - '.$n.'_'.$pengguna->nama_lengkap;
            // pesan

            $mail->Body = "
                <br>Kepada Bpk/Ibu Yth
                <br><br>
                <p>Berikut Reminder Approval Permintaan Pembelian Barang Atas Nama:</p>
            <br>Nama Lengkap: ". $pengguna->nama_lengkap."<br>No. PR: ".$n."<br>Department: ".$pengguna->nm_div."<br>Tanggal dibutuhkan: ".date('d M Y', strtotime($nailat->tgta))."<br><br>Sekian dan Terimah Kasih. <br> url: <a target='_blank' href='".base_url($this->uriku."/talian")."'>Portal Bnetfit</a>";

            if ($mail->send()) {
                $this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Reminder berhasil dikirimkan kepada supervisor </div>');
                redirect(base_url($this->uriku."/talian?succ"));
            } else {
                $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Reminder gagal dikirimkan kepada supervisor </div>');
                redirect(base_url($this->uriku."/talian?err"));
            }
        }
        $this->session->set_flashdata('pesan','<div class="alert alert-danger alert-dismissible" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              Email Supervisor kosong, tidak bisa mengirimkan reminder </div>');
        redirect(base_url($this->uriku."/talian?err"));
    }


}