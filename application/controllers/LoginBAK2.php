<?php

defined("BASEPATH") OR exit("Akses ditolak!");

class Login extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper("pengalih");
        proteksi_logout($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }

        $this->load->model("Pengguna_model","pengguna");
    }


    public function index() {
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $user = $this->cek_login($post);
            if($user != false) {



                if($user["username"] == "maintenanceusr") {
                    $this->konfig->ubah_status_maintenance(1);
                    redirect(base_url());
                    exit();
                }

                $this->session->set_userdata($user);


                        if($this->session->userdata("id_div") ==4){
                            
                            if($this->session->userdata("id_jabatan") == 17 || $this->session->userdata("id_jabatan") == 24 ){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                            redirect(base_url("panoc/"));
                            }

                        // Finance Accounting Tax Department
                        }elseif($this->session->userdata("id_div") ==5){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panaf/"));


                        // Network Development Department
                        }elseif($this->session->userdata("id_div") ==18){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));                       

                        // Bussiness Development Department
                        }elseif($this->session->userdata("id_div") ==10){
                            
                            if($this->session->userdata("id_jabatan") >59){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                            redirect(base_url("panbd/"));
                            }
                        
                        }elseif($this->session->userdata("id_div") ==16 || $this->session->userdata("id_div") ==11 ){
                            $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panep/"));

                        // IT Department  
                        }elseif($this->session->userdata("id_div") ==2){
                            if($this->session->userdata("id_jabatan") ==10 || $this->session->userdata("id_jabatan") ==11){
                                $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("panit/"));
                            }else{
                                redirect(base_url("login/"));   
                            }


                        // HR&GA Department  
                        }elseif($this->session->userdata("id_div") ==3){
                            if($this->session->userdata("id_jabatan") ==21 || $this->session->userdata("id_jabatan") ==15){
                                $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                                redirect(base_url("panhg/"));
                            }else{
                                redirect(base_url("login/"));   
                            }
                                               

                        }else{
                        $akt=$this->pengguna->tamakt('login berhasil',$this->session->userdata('username'));
                        redirect(base_url("panel/"));
                        }
                


            } else {
                $akt=$this->pengguna->tamakt('login gagal',$this->input->post('username'));
                redirect(base_url("login/?err"));
            }
        }
        $this->load->view("login/login");
    }

    private function cek_login($post) {
        if(isset($post["username"]) && isset($post["password"])) {
            $data = $this->pengguna->login($post["username"],$post["password"]);
            return $data;
        }
    }


}