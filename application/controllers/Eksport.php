<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing;

class Eksport extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		
		$this->load->helper('tgl_indo');
		$this->load->model("Wordata_model","wordata");
		$this->load->library('excel');
	}
	
    public function baa($wo=null) {
    //$nafile=str_replace(' ', '', $this->session->userdata('nama_lengkap'));
    $nafile= $wo;
    //$naleng=$this->session->userdata('nama_lengkap');
    $dw = $this->wordata->pilwo($wo);

    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_baa.xlsx');

    $worksheet = $spreadsheet->getActiveSheet();
    //$worksheet->getCell('B2')->setValue($naleng);
    $worksheet->getCell('A6')->setValue('No.   /BAA/JLM/'.blnro(date('m')).'/'.date('Y'));
    $worksheet->getCell('A7')->setValue('         Pada Hari '.hari(date('l')). ', Tanggal '.angkahuruf(date('d')).', Bulan '.bulan(date('m')).' , Tahun '.angkahuruf(date('Y')).', bersama-sama memeriksa proses AKTIVASI atas link tersebut di bawah dari PT JALA LINTAS MEDIA untuk account dibawah ini dengan spesifikasi sebagai berikut:');
    $worksheet->getCell('A11')->setValue('2. '.$dw->nm_custo);

    $worksheet->getCell('C12')->setValue($dw->cp_custo);
    $worksheet->getCell('C13')->setValue($dw->ja_custo);

    $worksheet->getCell('C15')->setValue($dw->nm_svsta);
    $worksheet->getCell('C16')->setValue($dw->ca_custo);
    $worksheet->getCell('C17')->setValue($dw->md_custo);
    $worksheet->getCell('C18')->setValue($dw->if_custo); 

    $worksheet->getCell('C20')->setValue($dw->de_custo);

    $worksheet->getCell('C29')->setValue($dw->ph_ven);
    $worksheet->getCell('C30')->setValue($wo);

    $worksheet->getCell('F42')->setValue('Jakarta, '.date('d').' '.bulan(date('m')).' '.date('Y'));
    $worksheet->getCell('F43')->setValue(strtoupper($dw->nm_custo));

    $worksheet->getCell('G49')->setValue($dw->cp_custo);
    $worksheet->getCell('G50')->setValue($dw->ja_custo); 

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename=BAA_'.$nafile.'.xls');
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



    //redirect(base_url("panel/womonito/"));

    }
	
	
}
