<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Datewo extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->usrku=$this->session->userdata("username");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");

        $this->load->model("Khusus_model","khusus");
        $this->load->library('excel');
        //$this->load->model("Laporan_model","laporan");
        $this->load->model("Otbdata_model","otbdata");
        $this->load->model("Odcdata_model","odcdata");
        $this->load->model("Genral_model","genral");
    }

    public function index() {


        $post = $this->input->post();
        $alam = $this->input->post("area");

        if(isset($post["btnSimpan"])) {

            unset($post["podc"]);
             
            $s = $this->genral->gensim("app_latrop_owe");
            if($s != false) {
            $akt=$this->genral->tamakt('EWO baru id '.$alam,$this->usrku);
                $this->genral->rimsan(1);
                redirect(base_url($this->uriku));
            } else {
                $this->genral->rimsan(2);
                redirect(base_url($this->uriku));
            }
            exit();
        }
   

        $judul = "Engineering Work Order";
        $menu = $this->set_menu("meena");

        $jclsem = $this->otbdata->jcsem();
        $otbsem = $this->otbdata->otsem();
        $odcsem = $this->odcdata->semua();
        $dafewo = $this->genral->gentam("app_latrop_owe");

        $this->load->view("share/akgnar/hawal",compact("judul","menu"));
        $this->load->view("share/enigne/owetup",compact("odcsem","otbsem","jclsem","dafewo"));
        $this->load->view("share/akgnar/hakhi");
    }

    public function ewohap($id=null) {

            $h= $this->genral->pilpus("app_latrop_owe","ideo",$id);

            if($h){
                $akt=$this->genral->tamakt('Hapus EWO id '.$id,$this->usrku);
                $this->genral->rimsan(3);
                redirect(base_url($this->uriku));
            }else{
                $this->genral->rimsan(4);
                redirect(base_url($this->uriku));
            }
    }

    public function logout() {
        $akt=$this->genral->tamakt('logout berhasil',$this->usrku);
        $this->session->sess_destroy();
        redirect(base_url("login/"));
    }

    private function set_menu($active) {
        $menu = [
            "dashboard"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "aprofil"=>"",
            "pengguna"=>"",
            "divisi"=>"", 
            "jabatan"=>"",
            "kaarea"=>"",
            "talian"=>"",
            "survey"=>"",           
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "meena"=>"",
            "ladload"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }


}