<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Intrates extends CI_Controller {
	
	public function __construct(){
		parent::__construct();

		$this->dba = $this->load->database('dbintra', TRUE);
        $this->load->model("Intranet_model","intra");
	}
	
	public function index(){

        $judul = "Tes Koneksi Database Intranet";
        $this->load->view("script/hal/hawal",compact("judul"));
        $this->load->view("script/htama");
        $this->load->view("script/hal/hakhi");

	}

	public function sintra($d){

      $t=$this->intra->intrasync($d);
       echo "<pre>".$t."</pre>";

	}
	

}
