<?php


defined("BASEPATH") OR exit("Akses ditolak!");

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Panep extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->dbz = $this->load->database('newdb', TRUE); // Koneksi ke Postgres iBoss
        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("general");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->library('pdf');
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }
        $this->load->model("excel_import_model");
        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Finacc_model","finacc");
        $this->load->model("Ucorpe_model","ucorpe");

    }

    public function index() {
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));

        $judul = "Dashboard - eProcurment";
        $menu = $this->set_menu("dashboard");

       
        $this->load->view("panep/frames/hawal",compact("judul","menu"));
        $this->load->view("panep/index",compact("blnini"));
        $this->load->view("panep/frames/hakhi");
    }


    public function uskelol() {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != super )
            redirect(base_url("panep/"));

        $daftar_pengguna = $this->pengguna->nocsem();

        $judul = "Kelola Business Dev.";
        $menu = $this->set_menu("uskelol");

        $this->load->view("panep/frames/hawal",compact("judul","menu"));
        $this->load->view("panep/tiunem/uskelol",compact("daftar_pengguna"));
        $this->load->view("panep/frames/hakhi");
    }

    public function profilku($id = null) {
        //if($this->session->userdata("id_jabatan") != 1)
            //redirect(base_url("panel/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->editpro();
            if($edt != false) {
                redirect(base_url("panep/profilku/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url("panep/profilku/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        //if($id == null) redirect(base_url("panep/uskelol/"));
        $judul = "Edit Profil";
        $menu = $this->set_menu("uprofil");

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panep/frames/hawal",compact("judul","menu"));
            $this->load->view("panep/person/profilku",compact("pengguna"));
            $this->load->view("panep/frames/hakhi");
        } else redirect(base_url("panep/pengguna/"));
    }

    public function atesep($id = null) {

      /*  $post = $this->input->post();


        if(isset($post["btnSimpan"])) {
            $sim = $this->finacc->isibuarba();
            if($sim != false) {
                redirect(base_url("panep/tiarbank/?succ"));
            } else {
                redirect(base_url("panep/tiarbank/?err"));
            }
            exit();
        }

        $datiarba=$this->finacc->datbuarba();*/

        $sepfad=$this->ucorpe->sepmat();
        
        $judul = "Aplikasi e-Proc - Peserta";
        $menu = $this->set_menu("atesep");

        $this->load->view("panep/frames/hawal",compact("judul","menu"));
        $this->load->view("panep/tresep/mutama",compact("sepfad"));
        $this->load->view("panep/frames/hakhi");

    }

    public function ubtiarba($id = null,$p=null) {

        $post = $this->input->post();


        if(isset($post["btnSimpan"])) {
            $sim = $this->finacc->simket();
            //$nid=$this->input->post("idt");
            if($sim != false) {
                redirect(base_url("panep/ubtiarba/".$id."?succ"));
            } else {
                redirect(base_url("panep/ubtiarba/".$id."?err"));
            }
            exit();
        }

        $pitiarba=$this->finacc->pilbuarba($id);
        $pilketil=$this->finacc->pilket($id);
        $totalket=$this->finacc->sumket($id);
        
        $judul = "Detil Bukti Pengeluaran Bank";
        $menu = $this->set_menu("tiarba");

        $this->load->view("panep/frames/hawal",compact("judul","menu"));
        $this->load->view("panep/bukti/abraitbu",compact("totalket","pitiarba","pilketil"));
        $this->load->view("panep/frames/hakhi");

    }


    public function profilvi($id = null) {
        if($this->session->userdata("id_pengguna") != $id)
            redirect(base_url("panep/"));

        $judul = "Profil Pengguna";
        $menu = $this->set_menu("profil");
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panep/frames/hawal",compact("judul","menu"));
            $this->load->view("panep/person/profilkuv",compact("pengguna","daftar_jabatan","daftar_divisi","daftas"));
            $this->load->view("panep/frames/hakhi");
        } else redirect(base_url("panep/pengguna/"));
    }


    public function penggunaedit($id = null) {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panep/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->edit();
            if($edt != false) {
                redirect(base_url("panep/penggunaedit/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url("panep/penggunaedit/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url("panep/uskelol/"));
        $judul = "Edit Pengguna";
        $menu = $this->set_menu("uskelol");
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panep/frames/hawal",compact("judul","menu"));
            $this->load->view("panep/penggunaedit",compact("pengguna","daftar_jabatan","daftar_divisi","daftas"));
            $this->load->view("panep/frames/hakhi");
        } else redirect(base_url("panep/pengguna/"));
    }


    public function logout() {
        $akt=$this->pengguna->tamakt('logout berhasil',$this->session->userdata('username'));
        $this->session->sess_destroy();
        redirect(base_url("login/"));
    }


    function imp2db()
    {
        $this->load->library('excel');

        $lf=$this->excel_import_model->lfile();

        foreach ($lf as $v) {
        	$idf=$v->id;
        	$naf=json_decode($v->nafi);
        }
        foreach($naf as $item){
        	$rfile=$item->file;
        }

        if(isset($rfile)){
            $path = "assets/uploads/tsalb/".$rfile; //$_FILES["file"]["tmp_name"];
            $object = PHPExcel_IOFactory::load($path);
            foreach($object->getWorksheetIterator() as $worksheet)
            {
                $highestRow = $worksheet->getHighestRow();
                $highestColumn = $worksheet->getHighestColumn();
                for($row=2; $row<=$highestRow; $row++)
                {
                    $id = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $us = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $na = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $pr = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $al = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $em = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $hp = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $re = $worksheet->getCellByColumnAndRow(7, $row)->getValue();


                    $data[] = array(
                        'id'        =>  $id,
                        'idfile'    =>  $idf,
                        'username'  =>  $us,
                        'nama'      =>  $na,
                        'produk'    =>  $pr,
                        'alamat'    =>  $al,
                        'email'     =>  $em,
                        'hp'        =>  $hp,
                        'registrasi'=>  $re
                    );
                }
            }
            $this->excel_import_model->insertblast($data);
            $jum=$highestRow-1;
            echo $jum.' data berhasil di import';
        }   
    }

    public function toexcel($pil=null) {
    $nafile=$pil;
    //$spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    //$spreadsheet->getDefaultStyle()->getFont()->setSize(8);
    $dafphi = $this->bayar->cekdata($pil);
    $dafrin = $this->bayar->paymdet($pil);
    /*$spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    $sheet->setCellValue('A1', 'Coba Lagi !');

    $writer = new Xlsx($spreadsheet);
    $writer->save('coba.xlsx');*/
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_phi.xlsx');
    $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(11);
    $worksheet = $spreadsheet->getActiveSheet();

    //$worksheet->getCell('B2')->setValue($naleng);
    //$worksheet->getCell('B3')->setValue($this->session->userdata('id_div'));
    //$worksheet->getCell('B4')->setValue($this->session->userdata('id_jabatan'));
    $no=1;
    $n=3;
    foreach ($dafphi as $h) {
    $worksheet->getCell('A'.$n)->setValue($no++);
    $worksheet->getCell('B'.$n)->setValue($h->username);
    $worksheet->getCell('C'.$n)->setValue(substr($h->nama,0,6));
    $worksheet->getCell('D'.$n)->setValue($h->thn);
    $worksheet->getCell('E'.$n)->setValue($h->JAN);  
    $worksheet->getCell('F'.$n)->setValue($h->FEB);
    $worksheet->getCell('G'.$n)->setValue($h->MAR);
    $worksheet->getCell('H'.$n)->setValue($h->APR);
    $worksheet->getCell('I'.$n)->setValue($h->MEI);
    $worksheet->getCell('J'.$n)->setValue($h->JUN);
    $worksheet->getCell('K'.$n)->setValue($h->JUL);
    $worksheet->getCell('L'.$n)->setValue($h->AGU);  
    $worksheet->getCell('M'.$n)->setValue($h->SEP);
    $worksheet->getCell('N'.$n)->setValue($h->OKT);
    $worksheet->getCell('O'.$n)->setValue($h->NOP);
    $worksheet->getCell('P'.$n)->setValue($h->DES);
    $worksheet->getCell('Q'.$n)->setValue($h->TOTAL);
    $n++;   
    }
    $nr=$n+2;
    $nr1=$nr+1;
    $worksheet->mergeCells('A'.$nr.':P'.$nr);
    $worksheet->getCell('A'.$nr)->setValue('RINCIAN TRANSAKSI');
    $worksheet->getStyle('A'.$nr)->getAlignment()->setHorizontal('center');
    $worksheet->getStyle('A'.$nr)->getFont()->setSize(14);

    $worksheet->getCell('A'.$nr1)->setValue('No.');
    $worksheet->getCell('B'.$nr1)->setValue('Tahun');

    $worksheet->mergeCells('C'.$nr1.':D'.$nr1);
    $worksheet->getCell('C'.$nr1)->setValue('ID#');
    $worksheet->getStyle('A'.$nr1.':P'.$nr1)->getAlignment()->setHorizontal('center');

    $worksheet->mergeCells('E'.$nr1.':G'.$nr1);
    $worksheet->getCell('E'.$nr1)->setValue('Nama');
    $worksheet->getStyle('E'.$nr1)->getAlignment()->setHorizontal('center');

    $worksheet->getCell('H'.$nr1)->setValue('Tgl. Bayar');
    $worksheet->getCell('I'.$nr1)->setValue('Periode');
    $worksheet->getCell('J'.$nr1)->setValue('Harga');
    $worksheet->getCell('K'.$nr1)->setValue('PPN');
    $worksheet->getCell('L'.$nr1)->setValue('Diskon');
    $worksheet->getCell('M'.$nr1)->setValue('Metoda');

    $worksheet->mergeCells('N'.$nr1.':P'.$nr1);
    $worksheet->getCell('N'.$nr1)->setValue('Kasir');
    $no2=1;
    $nr2=$nr+2;
    foreach ($dafrin as $h) {
    $worksheet->getCell('A'.$nr2)->setValue($no2++);
    $worksheet->getCell('B'.$nr2)->setValue($h->thn);
    $worksheet->mergeCells('C'.$nr2.':D'.$nr2);
    $worksheet->getCell('C'.$nr2)->setValue($h->username);
    $worksheet->getStyle('C'.$nr2)->getAlignment()->setHorizontal('center');

    $worksheet->mergeCells('E'.$nr2.':G'.$nr2);
    $worksheet->getCell('E'.$nr2)->setValue($h->name);
    $worksheet->getCell('H'.$nr2)->setValue($h->pay_date);
    $worksheet->getCell('I'.$nr2)->setValue($h->bln);   
    $worksheet->getCell('J'.$nr2)->setValue($h->price);
    $worksheet->getCell('K'.$nr2)->setValue($h->ppn);
    $worksheet->getCell('L'.$nr2)->setValue($h->discount);
    $worksheet->getCell('M'.$nr2)->setValue($h->vendor);
    $worksheet->mergeCells('N'.$nr2.':P'.$nr2);
    $worksheet->getCell('N'.$nr2)->setValue($h->kasir);
    $nr2++;
    }

    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //header('Content-Disposition: attachment; filename='.date('Ymd').'_'.$nafile.'Payment_History.xlsx');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename='.date('YmdHis').'-'.$nafile.'-Payment_History.xls');    
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



    redirect(base_url("panep/pymhis/"));

    }

    public function lapblnan($pil=null) {
    $nafile=$pil;

    $dafbyr = $this->bayar->paybln($pil);

    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('assets/uploads/_tpl/tpl_mre.xlsx');
    $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(11);
    $worksheet = $spreadsheet->getActiveSheet();

    $worksheet->mergeCells('A3:G3');
    $worksheet->getCell('A3')->setValue('LAPORAN PEMBAYARAN BULAN '.$nafile.'.');
    $worksheet->getStyle('A3')->getAlignment()->setHorizontal('center');
    $worksheet->getStyle('A3')->getFont()->setSize(14);


    $no=1;
    $n=6;
    foreach ($dafbyr as $h) {
    $worksheet->getCell('A'.$n)->setValue($no++);
    $worksheet->getCell('B'.$n)->setValue($h->username);
    $worksheet->getCell('C'.$n)->setValue($h->name);
    $worksheet->getCell('D'.$n)->setValue($h->packet);
    $worksheet->getCell('E'.$n)->setValue($h->pay_date);  
    $worksheet->getCell('F'.$n)->setValue($h->bln);
    $worksheet->getCell('G'.$n)->setValue($h->price);
    $n++;   
    }


    $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
    //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //header('Content-Disposition: attachment; filename='.date('Ymd').'_'.$nafile.'Payment_History.xlsx');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename='.date('YmdHis').'-LapBulan_'.$nafile.'.xls');    
    
    //$writer = new Xlsx($spreadsheet);
    $writer->save("php://output");



    redirect(base_url("panep/pymhis/"));

    }

    public function haket($p,$id){
        $hap = $this->finacc->pusket($p);
        $u=$this->finacc->pusnom($id);
            if($hap)

                redirect(base_url("panep/ubtiarba/".$id."/".$p."?hsuc"));
            else
                redirect(base_url("panep/ubtiarba/".$id."?herr"));
   
    }


    Public function pdfgen($id=null){
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->SetMargins(12,1,1);
        $pdf->Image(base_url('/assets/images/jlmlogo.png'),12,10,-200);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(30,4,' ',0,0,'C');
        $pdf->Cell(125,4,' ',0,0,'C');
        $pdf->Cell(30,4,' INTERNAL ',1,1,'C');


        $datu = $this->db->where('idt',$id)->get('v_fat_knabraul')->result();
        foreach ($datu as $d){

        	$pdf->Cell(40,4,'',0,0,'C');
        	$pdf->Cell(105,4,'',0,0,'C');
        	$pdf->SetFont('Arial','',9);
        	$pdf->Cell(20,4,'No.',0,0,'L');
        	$pdf->Cell(40,4,': '.$d->urt,0,1,'L');

        	$pdf->Cell(40,4,'',0,0,'C');
        	$pdf->SetFont('Arial','B',13);
        	$pdf->Cell(105,4,'BUKTI PENGELUARAN BANK',0,0,'C');
        	$pdf->SetFont('Arial','',9);
        	$pdf->Cell(20,4,'Tanggal ',0,0,'L');
        	$pdf->Cell(20,4,': '.tgl_indome($d->tgt),0,1,'L');

        	$pdf->Cell(40,4,'',0,0,'C');
        	$pdf->Cell(105,4,'',0,0,'C');
        	$pdf->Cell(20,4,'Tgl. Invoice',0,0,'L');
        	$pdf->Cell(20,4,': '.tgl_indome($d->tgi),0,1,'L');

        	$pdf->Cell(10,2,'',0,1); // Jarak spasi kosong

        	$pdf->SetFont('Arial','',10);
            $pdf->Cell(30,4,'Dibayar kepada',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->Cell(40,4,$d->dik,0,1);
            $pdf->Cell(30,4,'Nominal',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->Cell(40,4,number_format($d->nit).',-',0,1); 
            $pdf->Cell(30,4,'Terbilang',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->SetFont('Arial','B',10);
            $pdf->setFillColor(230,230,230);
            $pdf->Cell(150,4,terbilang($d->nit),0,1,'L',1); 
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(30,4,'Supplier invoice',0,0);
            $pdf->Cell(5,4,' : ',0,0);
            $pdf->Cell(40,4,$d->sui,0,1); 
            $ppn=$d->p10;
            $pph=$d->p23;
            $naf=$d->dik;

        }
        $n=1;
        $dadu = $this->db->where('idt',$id)->get('fat_knabraul_ket')->result();
        foreach ($dadu as $d){
            if($n==1){ $k='Keterangan'; $t=' : ';}else{$k='';$t='   ';}

            $pdf->Cell(30,4,$k,0,0);
            $pdf->Cell(5,4,$t,0,0);
            $pdf->Cell(115,4,$d->ket,0,0);
            $pdf->Cell(10,4,'Rp. ',0,0);
            $pdf->Cell(25,4,number_format($d->nit).',-',0,1,'R'); 
            $n++;
        }
        $cekdat=$this->db->where('idt',$id)->get('fat_knabraul_ket')->num_rows();


        if($cekdat==1){$x=4;}elseif($cekdat==2){$x=3;}elseif($cekdat==3){$x=2;}else{$x=1;}
        for ($i=0; $i < $x; $i++) { 
            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(130,4,' ',0,0);
            $pdf->Cell(25,4,'-',0,1,'R'); 
        }

        $suto = $this->db->select("sum(nit) as subtot")->where('idt',$id)->get('fat_knabraul_ket')->row();
            $pdf->Cell(30,3,'',0,0);
            $pdf->Cell(130,3,'',0,0,'R');
            $pdf->Cell(25,3,"-------------------------- +",0,1,'R'); 

            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(127,4,'Subtotal Rp.',0,0,'R');
            $pdf->Cell(28,4,number_format($suto->subtot).",-",0,1,'R'); 

            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(127,4,'PPN Rp.',0,0,'R');
            $pdf->Cell(28,4,number_format($ppn).",-",0,1,'R'); 

            $pdf->Cell(30,4,'',0,0);
            $pdf->Cell(127,4,'PPH 23 Rp.',0,0,'R');
            $pdf->Cell(28,4,number_format(-1*($pph)).",-",0,1,'R'); 


			$pdf->Cell(185,1,' ','T',1); // jarak spasi kosong
			//$pdf->Cell(185,1,'',0,1); // jarak spasi kosong

            $pdf->SetFont('Arial','I',10);
            $pdf->Cell(125,4,'                     Creditor A/C  :','T',0);
            $pdf->Cell(60,4,' PO No. : ','T',1);
            $pdf->Cell(125,4,'                   Bank Transfer :','T',0);
            $pdf->Cell(60,4,'SPK No. : ','T',1);  
            $pdf->Cell(185,1,'',0,1);
            $pdf->Cell(185,1,' ','T',1); // jarak spasi kosong

            $pdf->SetFont('Arial','',10);
     		$pdf->Cell(30,5,'Disiapkan ',1,0,'C');
        	$pdf->Cell(50,5,'Verifikasi',1,0,'C');
        	$pdf->Cell(75,5,'Menyetujui',1,0,'C');
        	$pdf->Cell(30,5,'Penerima',1,1,'C');

			$pdf->Cell(30,18,' ',1,0);
			$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(25,18,' ',1,0);
        	$pdf->Cell(30,18,' ',1,1);           

        	$pdf->SetFont('Arial','',9);
			$pdf->Cell(30,4,'Finance A/P',1,0,'C');
			$pdf->Cell(25,4,'Presales',1,0,'C');
        	$pdf->Cell(25,4,'FAM',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(25,4,'Director',1,0,'C');
        	$pdf->Cell(30,4,' ',1,1,'C');
        	$pdf->SetFont('Arial','',8);
        	$pdf->Cell(30,4,'(                     )',1,0,'C');
			$pdf->Cell(25,4,'(                     )',1,0,'C');
        	$pdf->Cell(25,4,'( Henry )',1,0,'C');
        	$pdf->Cell(25,4,'( Victor Irianto )',1,0,'C');
        	$pdf->Cell(25,4,'( Yulius Purnama )',1,0,'C');
        	$pdf->Cell(25,4,'( Edwind J.B. )',1,0,'C');
        	$pdf->Cell(30,4,'(                     )',1,1,'C');
        // ob_start();
        $namafile=date('ymdHi').'-'.$naf;
        $pdf->Output('D','BUK'.$namafile.'.pdf');
        // $pdf->Output();
        $this->finacc->hittak($id);

    }



    public function keyorp($id = null) {

        $post = $this->input->post();


        if(isset($post["btnSimpan"])) {
            $pro = $this->ucorpe->orpmis();
            if($pro != false) {
                redirect(base_url("panep/keyorp/?suc"));
            } else {
                redirect(base_url("panep/keyorp/?err"));
            }
            exit();
        }

        $orptad=$this->ucorpe->orpmat();
        
        $judul = "Aplikasi e-Proc - Proyek";
        $menu = $this->set_menu("keyorp");

        $this->load->view("panep/frames/hawal",compact("judul","menu"));
        $this->load->view("panep/keyorp/mutama",compact("orptad"));
        $this->load->view("panep/frames/hakhi");

    }
    
    private function set_menu($active) {
        $menu = [
            "dashboard"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "mombaru"=>"",
            "atesep"=>"",
            "keyorp"=>"",
            "salopp"=>"",
            "pymhis"=>"",
            "blainf"=>"",
            "blapro"=>"",
            "salinc"=>"",
            "komrek"=>"",
            "spkbru"=>"",
            "tikelol"=>"",
            "uskelol"=>"",
            "rokelol"=>"",
            "timanua"=>"",
            "tisemua"=>"",
            "laphri"=>"",
            "lapbln"=>"",
            "gasandi"=>"",
            "ladload"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }


}