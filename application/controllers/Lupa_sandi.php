<?php

defined("BASEPATH") OR exit("Akses ditolak!");
class Lupa_sandi extends CI_Controller {

    public function __construct() {
        parent::__construct();

        date_default_timezone_set("Asia/Jakarta");

        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->helper('url');

        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Lupa_model","lupa");
    }

    public function index() {
    

        $judul = "Portal JLM - Lupa Sandi";
        
        $this->load->view("share/lupa_sandi",compact("judul"));
        
    }

    public function kirim() {

                $tmail=$this->input->post('alamat_email');
                $check_domain_ref=explode('@',$tmail,1);
        if($check_domain_ref!="jlm.net.id"){
                $this->session->set_flashdata('pesan', '<div class="alert alert-danger text-center">Alamat email yang digunakan harus berdomain jlm.net.id.</div>');

        }else{


                $query = $this->db->query("SELECT * FROM pengguna WHERE email = ?", array($tmail));

                    if($query->num_rows() == 1){

                    $this->lupa->kirimE($tmail);

                    //$error = "Error, Cannot insert new user details!";
                    $this->session->set_flashdata('pesan', '<div class="alert alert-success text-center">Permohonan pengaturan ulang sandi berhasil, silahkan periksa email anda.</div>');
                        //return $query->row();
                    }else{
                    $this->session->set_flashdata('pesan', '<div class="alert alert-danger text-center">Mohon maaf email yang anda masukan tidak terdaftar.</div>');
                    }                    
                
            }
    
        $this->load->view("share/lupa_sandi",compact("judul","check_domain_ref"));
        
    }

    function set_sandi($hashcode){

        if($this->lupa->vresandi($hashcode)){
            $data=$this->lupa->vresandi($hashcode);
            $this->session->set_flashdata('pesan', '<div class="alert alert-success text-center">link valid</div>');

            $this->load->view('share/set_sandi',compact("data"));

        }else{
            $this->session->set_flashdata('pesan', '<div class="alert alert-danger text-center">Alamat sudah kadaluarsa, silahkan dicoba lagi.</div>');
            redirect('lupa_sandi');
        }
    }

    public function simpan_sandi(){
        $this->form_validation->set_rules('txt_sandi','Kata Sandi', 'trim|required');
        $this->form_validation->set_rules('ula_sandi','Konfirmasi Sandi', 'trim|required|matches[txt_sandi]');
        if($this->form_validation->run() == false){

            $this->load->view('rsandi');

            
        }else{
            $this->session->set_flashdata('verify', '<div class="alert alert-success text-center">Kata sandi berhasil disimpan.</div>');
            $v1=$this->input->post('txt_id');
            $v2=$this->input->post('txt_sandi');
            $this->lupa->savsandi($v1,$v2);
            redirect('/login');

        }

    }



}