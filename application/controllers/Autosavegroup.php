<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Autosavegroup extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->user_logon = $this->session->userdata('logged_member_user');
        if ($this->user_logon == '') {
            redirect(base_url() . 'index.php/login');
        }
        $this->load->model('export_all_model');
        //$this->load->helper('url');
    }

    public function index() {

         /*   if ($this->input->post('export')) {
                $tgl_awal = $this->input->post('tgl_awal_all');
                $tgl_akhir = $this->input->post('tgl_akhir_all');*/

                $tgl_awal ='2021-08-01';
                $tgl_akhir = '2021-08-31';
                $kondisi ='Kemenko';

                //Mendefinisikan Jumlah Hari Dalam Sebulan
                $tahun = date('Y');
                $bulan = substr($tgl_awal, 5, 2);
                //echo $bulan;exit;
                $tanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
                //echo $hasil;exit;
                //Time Zone
                date_default_timezone_set('Asia/Jakarta');

                $this->load->library('excel');

                //Posisi Awal Sheet
                $this->excel->getActiveSheet();

                $n_group = $this->export_all_model->get_namePartial($tgl_awal, $tgl_akhir,$kondisi);

                $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

                $tahun_print = substr($tgl_awal, 0, 4);
				$bulan_print = substr($tgl_awal, 5, 2);
				$tgl_print = substr($tgl_awal, 8, 2);
				$tahun_print2 = substr($tgl_akhir, 0, 4);
				$bulan_print2 = substr($tgl_akhir, 5, 2);
				$tgl_print2 = substr($tgl_akhir, 8, 2);

				$tgl_awalx = $tgl_print . " " . $BulanIndo[(int) $bulan_print - 1] . " " . $tahun_print;
				$tgl_akhirx = $tgl_print2 . " " . $BulanIndo[(int) $bulan_print2 - 1] . " " . $tahun_print2;

                $loop = 0;
                foreach ($n_group as $row) {
                    $requester = $row['Requester'];
                    $total = $row['total'];
                    //echo $exceldata;
                    //exit;
                    //Tambah Sheet Berikutnya
                    // Tambah Sheet
                    $persen=$this->export_all_model->get_req_persen($tgl_awal,$requester);
                    if($total==0){$nilav=100;}elseif ($total==1) {$nilav='=L11';}else{$nilav=' '.number_format($persen->p,2);}
                    $objWorkSheet = $this->excel->createSheet($loop); //Setting index Ketika Dibuat
                    //Setting Kolom Statis
                    $objWorkSheet->setCellValue('A2', 'PERFORMANCE REPORT');
                    $objWorkSheet->mergeCells('A2:D2');
                    $objWorkSheet->getStyle('A2')->getFont()->setBold(true);
                    $objWorkSheet->getStyle('A2')->getFont()->setSize(16);
                    $objWorkSheet->getStyle('A2')->getFill()->getStartColor()->setARGB('#333');
                    $objWorkSheet->getStyle('A3:B7')->getFill()->applyFromArray(
                            array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => '39b3d7')
                            )
                    );
                    $objWorkSheet->getStyle('C7')->getNumberFormat()->setFormatCode('0.00');
                    $objWorkSheet->getStyle('L11:L100')
                            ->getNumberFormat()->setFormatCode('0.00');
                    $objWorkSheet->getStyle('C7')->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $objWorkSheet->getStyle('L11:L100')->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                    //Kolom Statis
                    $objWorkSheet->setCellValue('A3', 'Nomor');
                    $objWorkSheet->mergeCells('A3:B3');
                    $objWorkSheet->setCellValue('A4', 'Period');
                    $objWorkSheet->mergeCells('A4:B4');
                    $objWorkSheet->setCellValue('A5', 'Requester');
                    $objWorkSheet->mergeCells('A5:B5');
                    $objWorkSheet->setCellValue('A6', 'Total Trouble Ticket');
                    $objWorkSheet->mergeCells('A6:B6');
                    $objWorkSheet->setCellValue('A7', 'Availability (%)');
                    $objWorkSheet->mergeCells('A7:B7');

                    //echo $nm_requester;exit;
                    $objWorkSheet->mergeCells('C3:D3');
                    $objWorkSheet->setCellValue('C4', $tgl_awalx . ' - ' . $tgl_akhirx);
                    $objWorkSheet->mergeCells('C4:D4');
                    $objWorkSheet->setCellValue('C5', $requester);
                    $objWorkSheet->mergeCells('C5:D5');
                    $objWorkSheet->setCellValue('C6', $total . ' Trouble Tickets');
                    $objWorkSheet->mergeCells('C6:D6');
                    $objWorkSheet->setCellValue('C7', $nilav ); //'=AVERAGE(L11:L100)'
                    $objWorkSheet->mergeCells('C7:D7');

                    /* script menentukan tanggal */
                    $tgls = date('j');

                    /* script menentukan bulan */
                    $array_bln = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                    $bln = $array_bln[date('n',strtotime('+1 month', strtotime($tgl_awal)))];

                    /* script menentukan tahun */
                    $thn = date('Y');
                    /* script perintah keluaran */
                    $sekarang = $tgls . " " . $bln . " " . $thn;
                    //Footer
                    $mark = $total + 17;
                    $tgl = $total + 13;
                    $objWorkSheet->getStyle('K' . $mark . ':L' . $tgl)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objWorkSheet->setCellValue('K' . $mark, 'Manager Sales');
                    $objWorkSheet->setCellValue('L' . $mark, 'Manager IT');

                    $objWorkSheet->setCellValue('K' . $tgl, 'Jakarta, ' . $sekarang);
                    $objWorkSheet->mergeCells('K' . $tgl . ':L' . $tgl);


                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                    $objDrawing->setPath('./assets/image/logo.jpg');
                    $objDrawing->setCoordinates('K2');
                    $objDrawing->setHeight(70);
                    $objDrawing->setWidth(140);
                    $objDrawing->setWorksheet($this->excel->getActiveSheet());

                    //Judul Kolom Looping
                    $objWorkSheet->setCellValue('A9', 'No');
                    $objWorkSheet->mergeCells('A9:A10');
                    $objWorkSheet->setCellValue('B9', 'Create Time');
                    $objWorkSheet->mergeCells('B9:B10');
                    $objWorkSheet->setCellValue('C9', 'Service Description');
                    $objWorkSheet->mergeCells('C9:C10');
                    $objWorkSheet->setCellValue('D9', 'TROUBLE  TICKET');
                    $objWorkSheet->mergeCells('D9:G9');
                    $objWorkSheet->setCellValue('D10', 'Request_ID');
                    $objWorkSheet->setCellValue('E10', 'Request Type');
                    $objWorkSheet->setCellValue('F10', 'Downtime');
                    $objWorkSheet->setCellValue('G10', 'Uptime');
                    $objWorkSheet->setCellValue('H10', 'Duration');
                    $objWorkSheet->setCellValue('I9', 'Caused Code');
                    $objWorkSheet->mergeCells('I9:I10');
                    $objWorkSheet->setCellValue('J9', 'Resolution');
                    $objWorkSheet->mergeCells('J9:J10');
                    $objWorkSheet->setCellValue('K9', 'SLA (PKS %)');
                    $objWorkSheet->mergeCells('K9:K10');
                    $objWorkSheet->setCellValue('L9', 'Availability (%)');
                    $objWorkSheet->mergeCells('L9:L10');
                    $objWorkSheet->getStyle('A9:L10')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                    $objWorkSheet->getStyle('A9:L10')->getFill()->applyFromArray(
                            array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => '39b3d7')
                            )
                    );
                    $objWorkSheet->getStyle('A3:D7')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                    $objWorkSheet->getStyle('A9:L10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                    for ($col = ord('A'); $col <= ord('L'); $col++) {
                        $objWorkSheet->getColumnDimension(chr($col))->setAutoSize(true);
                        $objWorkSheet->getStyle(chr($col))->getFont()->setSize(12);
                        $objWorkSheet->getStyle(chr($col))->getAlignment()->setWrapText(true);
                    }
                    //Query Menampilkan Data Looping
                    $requester_detail = $this->export_all_model->get_Requester($requester, $tgl_awal, $tgl_akhir);

                    //Looping Isi Table
                    $i = 11;
                    $no = 0;
                    foreach ($requester_detail as $row) {
						$jam = $row['DURASI'];
						$jamkonvert = explode(":", $jam);
						$detik = $jamkonvert[0]*3600 + $jamkonvert[1] * 60 + $jamkonvert[2];
						$detik_sebulan = $tanggal * 86400;
						$hitung = $detik_sebulan - $detik;
						$akhir = $hitung / $detik_sebulan * 100;
						$hasil = round($akhir, 2);
                        $this->excel->setActiveSheetIndex(0);
                        $objWorkSheet->getStyle('A' . $i . ':' . 'L' . $i)->getFont()->setSize(8);
                        $objWorkSheet->getStyle('A' . $i . ':' . 'L' . $i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                        $objWorkSheet->setCellValue('A' . $i, $no + 1);
                        $objWorkSheet->setCellValue('B' . $i, $row['Created_Time']);
                        $objWorkSheet->setCellValue('C' . $i, $row['Subject']);
                        $objWorkSheet->setCellValue('D' . $i, $row['request_id']);
                        $objWorkSheet->setCellValue('E' . $i, $row['request_type']);
                        $objWorkSheet->setCellValue('F' . $i, $row['Down_Time']);
                        $objWorkSheet->setCellValue('G' . $i, $row['Uptime']);
                        $objWorkSheet->setCellValue('H' . $i, $row['DURASI']);
                        $objWorkSheet->setCellValue('I' . $i, $row['Root_Cause']);
                        $objWorkSheet->setCellValue('J' . $i, $row['Resolution']);
                        $objWorkSheet->setCellValue('K' . $i, '99,50');
                        $objWorkSheet->setCellValue('L' . $i, $hasil);
                        $i = $i + 1;
                        $no = $no + 1;
                    }

                    // Rename sheet
                    $objWorkSheet->setTitle($requester);

                    $loop++;
                }
                $filename = 'Rekap_gangguan_saja_'.strtoupper($kondisi).'_'.date('d/m/y',strtotime('+1 month', strtotime($tgl_awal))) . '.xlsx'; //Nama File Berdasarkan Requester
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //Format Untuk .xlsx
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');

                ob_end_clean();
                //Output Untuk Excel 2007
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

                $objWriter->save('php://output');
            //}
        
    }

    public function problem() {
        if ($this->input->post('p_export_pdf')) {
            $this->load->library('mpdf');
            $tgl_awal = $this->input->post('p_tgl_awal_all');
            $tgl_akhir = $this->input->post('p_tgl_akhir_all');
            $data['tgl_awal'] = $tgl_awal;
            $data['tgl_akhir'] = $tgl_akhir;
            $tahun = date('Y');
            $bulan = substr($tgl_awal, 3, 2);
            //echo $bulan;exit;
            $tanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
            $group = $this->export_all_model->get_nameProblem($tgl_awal, $tgl_akhir);
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

            $tahun_print = substr($tgl_awal, 0, 4);
            $bulan_print = substr($tgl_awal, 5, 2);
            $tgl_print = substr($tgl_awal, 8, 2);
            $tahun_print2 = substr($tgl_akhir, 0, 4);
            $bulan_print2 = substr($tgl_akhir, 5, 2);
            $tgl_print2 = substr($tgl_akhir, 8, 2);

            $tgl_awalx = $tgl_print . " " . $BulanIndo[(int) $bulan_print - 1] . " " . $tahun_print;
            $tgl_akhirx = $tgl_print2 . " " . $BulanIndo[(int) $bulan_print2 - 1] . " " . $tahun_print2;
            $loop = 0;
            foreach ($group as $row) {
                $problem = $row['Problem_Side'];
                $total = $row['total'];
                $ava = 0;
                $pdf = $this->export_all_model->get_Problem($problem, $tgl_awal, $tgl_akhir);
                foreach ($pdf as $a) {
                    $jam2 = $a['DURASI'];
					$jamkonvert2 = explode(":", $jam2);
					$detik2 = $jamkonvert2[0]*3600 + $jamkonvert2[1] * 60 + $jamkonvert2[2];
					$detik_sebulan2 = $tanggal * 86400;
					$hitung2 = $detik_sebulan2 - $detik2;
					$akhir2 = $hitung2 / $detik_sebulan2 * 100;
					$hasil2 = round($akhir2, 2);

                    $ava += $hasil2 / $total;
                    $ava = round($ava, 2);
                }
                $nama_dokumen = 'PROBLEM PERFORMANCE REPORT'; //Beri nama file PDF hasil.
                $mpdf = new mPDF('L', // mode - default ''
                        'A4', // format - A4, for example, default ''
                        8, // font size - default 0
                        '', // default font family
                        10, // margin_left
                        10, // margin right
                        16, // margin top
                        16, // margin bottom
                        9, // margin header
                        9); // margin footer
                $mpdf->AddPage($loop++);
                ob_start();

                $html .= '
                <label> No</label><label></label><br>
                <label> Period</label><label> : <b>' . $tgl_awalx . '</b> - <b>' . $tgl_akhirx . '</b></label><br>
                <label> Problem Side</label><label> : <b>' . $problem . '</b></label><br>
                <label> Total Trouble Ticket</label><label> : <b>' . $total . '</b> Trouble Tickets</label><br>
                <label> Availability</label><label> : <b>' . $ava . '</b> %</label><br>
                <div align="center"><h4>PERFORMANCE REPORT</h4></div>
                 <table style="width:100%;border-collapse:collapse;" border="1" > 
                <thead>
                <tr>
                  <th style="text-align:center; padding:5px 5px; background-color:#4C9ED9; color: #fff;">No</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Created Time</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Service Description</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Request ID</th>  
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Request Type </th>  
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Downtime </th> 
                  <th style="text-align:center; padding:5px 5px; background-color:#4C9ED9; color: #fff;">Uptime</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Duration</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Cause Code</th>
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Resolution</th>  
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">SLA (PKS)</th>  
                  <th style="text-align:center;padding:5px 5px;  background-color:#4C9ED9; color: #fff;">Availability</th>
                  </tr>
                </thead>
                ';
                $i = 1;
                foreach ($pdf as $s) {
                    $jam = $s['DURASI'];
					$jamkonvert = explode(":", $jam);
					$detik = $jamkonvert[0]*3600 + $jamkonvert[1] * 60 + $jamkonvert[2];
					$detik_sebulan = $tanggal * 86400;
					$hitung = $detik_sebulan - $detik;
					$akhir = $hitung / $detik_sebulan * 100;
					$hasil = round($akhir, 2);
                    $html .='<tr><td style="text-align:center;">' . $i++ . '</td>
                    <td style="text-align:center; padding:3px 3px;">' . $s['Created_Time'] . '</td>
                    <td style="text-align:center; padding:3px 3px;">' . $s['Subject'] . '</td>
                    <td style="text-align:center; padding:3px 3px;">' . $s['request_id'] . '</td>
                	<td style="text-align:center; padding:3px 3px;">' . $s['request_type'] . '</td>
                    <td style="text-align:center; padding:3px 3px;">' . $s['Down_Time'] . '</td>
                    <td style="text-align:center; padding:3px 3px;">' . $s['Uptime'] . '</td>
                    <td style="text-align:center; padding:3px 3px;">' . $s['DURASI'] . '</td>
                    <td style="text-align:center; padding:3px 3px;">' . $s['Root_Cause'] . '</td>
                    <td style="text-align:center; padding:3px 3px;">' . $s['Resolution'] . '</td>
                    <td style="text-align:center; padding:3px 3px;"> 99,50%</td>
                    <td style="text-align:center; padding:3px 3px;">' . $hasil . '%</td>
                    </tr>';
                }
                $html .='</table><br><br><br>';
            }
            ob_end_clean();
            $mpdf->WriteHTML($html);
            $mpdf->Output($nama_dokumen . ".pdf", 'I');
        } else {
            if ($this->input->post('p_export')) {
                $tgl_awal = $this->input->post('p_tgl_awal_all');
                $tgl_akhir = $this->input->post('p_tgl_akhir_all');

                //Mendefinisikan Jumlah Hari Dalam Sebulan
                $tahun = date('Y');
                $bulan = substr($tgl_awal, 5, 2);
                //echo $bulan;exit;
                $tanggal = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
                //echo $hasil;exit;
                //Time Zone
                date_default_timezone_set('Asia/Jakarta');

                $this->load->library('excel');

                //Posisi Awal Sheet
                $this->excel->getActiveSheet();

                $n_group = $this->export_all_model->get_nameProblem($tgl_awal, $tgl_akhir);

                $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

                $tahun_print = substr($tgl_awal, 0, 4);
				$bulan_print = substr($tgl_awal, 5, 2);
				$tgl_print = substr($tgl_awal, 8, 2);
				$tahun_print2 = substr($tgl_akhir, 0, 4);
				$bulan_print2 = substr($tgl_akhir, 5, 2);
				$tgl_print2 = substr($tgl_akhir, 8, 2);

				$tgl_awalx = $tgl_print . " " . $BulanIndo[(int) $bulan_print - 1] . " " . $tahun_print;
				$tgl_akhirx = $tgl_print2 . " " . $BulanIndo[(int) $bulan_print2 - 1] . " " . $tahun_print2;
                $loop = 0;
                foreach ($n_group as $row) {
                    $problem = $row['Problem_Side'];
                    $total = $row['total'];
                    //echo $exceldata;
                    //exit;
                    //Tambah Sheet Berikutnya
                    // Tambah Sheet
                    $objWorkSheet = $this->excel->createSheet($loop); //Setting index Ketika Dibuat
                    //Setting Kolom Statis
                    $objWorkSheet->setCellValue('A2', 'PERFORMANCE REPORT');
                    $objWorkSheet->mergeCells('A2:D2');
                    $objWorkSheet->getStyle('A2')->getFont()->setBold(true);
                    $objWorkSheet->getStyle('A2')->getFont()->setSize(16);
                    $objWorkSheet->getStyle('A2')->getFill()->getStartColor()->setARGB('#333');
                    $objWorkSheet->getStyle('A3:B7')->getFill()->applyFromArray(
                            array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => '39b3d7')
                            )
                    );
                    $objWorkSheet->getStyle('C7')->getNumberFormat()->setFormatCode('0.00');
                    $objWorkSheet->getStyle('M11:M100')
                            ->getNumberFormat()->setFormatCode('0.00');
                    $objWorkSheet->getStyle('C7')->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $objWorkSheet->getStyle('M11:M100')->getAlignment()
                            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                    $persen=$this->export_all_model->getpersen($tgl_awal,$problem);
                    //Kolom Statis
                    $objWorkSheet->setCellValue('A3', 'Nomor');
                    $objWorkSheet->mergeCells('A3:B3');
                    $objWorkSheet->setCellValue('A4', 'Period');
                    $objWorkSheet->mergeCells('A4:B4');
                    $objWorkSheet->setCellValue('A5', 'Problem Side');
                    $objWorkSheet->mergeCells('A5:B5');
                    $objWorkSheet->setCellValue('A6', 'Total Trouble Ticket');
                    $objWorkSheet->mergeCells('A6:B6');
                    $objWorkSheet->setCellValue('A7', 'Availability (%) in '.$persen->jhr.' days');
                    $objWorkSheet->mergeCells('A7:B7');

                    //echo $nm_requester;exit;
                    $objWorkSheet->mergeCells('C3:D3');
                    $objWorkSheet->setCellValue('C4', $tgl_awalx . ' - ' . $tgl_akhirx);
                    $objWorkSheet->mergeCells('C4:D4');
                    $objWorkSheet->setCellValue('C5', $problem);
                    $objWorkSheet->mergeCells('C5:D5');
                    $objWorkSheet->setCellValue('C6', $total . ' Trouble Tickets');
                    $objWorkSheet->mergeCells('C6:D6');
                    $objWorkSheet->setCellValue('C7', ' '.number_format($persen->p,2).' % (100-('.$persen->totnit.'/'.$persen->mbln.')x100)');
                    $objWorkSheet->mergeCells('C7:D7');

                    /* script menentukan tanggal */
                    $tgls = date('j');

                    /* script menentukan bulan */
                    $array_bln = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                    $bln = $array_bln[date('n')];

                    /* script menentukan tahun */
                    $thn = date('Y');
                    /* script perintah keluaran */
                    $sekarang = $tgls . " " . $bln . " " . $thn;
                    //Footer
                    $mark = $total + 17;
                    $tgl = $total + 13;
                    $objWorkSheet->getStyle('L' . $mark . ':M' . $tgl)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objWorkSheet->setCellValue('L' . $mark, 'Manager Sales');
                    $objWorkSheet->setCellValue('M' . $mark, 'Manager IT');

                    $objWorkSheet->setCellValue('L' . $tgl, 'Jakarta, ' . $sekarang);
                    $objWorkSheet->mergeCells('L' . $tgl . ':M' . $tgl);


                    $objDrawing = new PHPExcel_Worksheet_Drawing();
                    $objDrawing->setPath('./assets/image/logo.jpg');
                    $objDrawing->setCoordinates('L2');
                    $objDrawing->setHeight(70);
                    $objDrawing->setWidth(140);
                    $objDrawing->setWorksheet($this->excel->getActiveSheet());

                    //Judul Kolom Looping
                    $objWorkSheet->setCellValue('A9', 'No');
                    $objWorkSheet->mergeCells('A9:A10');
                    $objWorkSheet->setCellValue('B9', 'Requester');
                    $objWorkSheet->mergeCells('B9:B10');
                    $objWorkSheet->setCellValue('C9', 'Create Time');
                    $objWorkSheet->mergeCells('C9:C10');
                    $objWorkSheet->setCellValue('D9', 'Service Description');
                    $objWorkSheet->mergeCells('D9:D10');
                    $objWorkSheet->setCellValue('E9', 'TROUBLE  TICKET');
                    $objWorkSheet->mergeCells('E9:H9');
                    $objWorkSheet->setCellValue('E10', 'Request_ID');
                    $objWorkSheet->setCellValue('F10', 'Request Type');
                    $objWorkSheet->setCellValue('G10', 'Downtime');
                    $objWorkSheet->setCellValue('H10', 'Uptime');
                    $objWorkSheet->setCellValue('I10', 'Duration');
                    $objWorkSheet->setCellValue('J9', 'Caused Code');
                    $objWorkSheet->mergeCells('J9:J10');
                    $objWorkSheet->setCellValue('K9', 'Resolution');
                    $objWorkSheet->mergeCells('K9:K10');
                    $objWorkSheet->setCellValue('L9', 'SLA (PKS %)');
                    $objWorkSheet->mergeCells('L9:L10');
                    $objWorkSheet->setCellValue('M9', 'Availability (%)');
                    $objWorkSheet->mergeCells('M9:M10');
                    $objWorkSheet->getStyle('A9:M10')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                    $objWorkSheet->getStyle('A9:M10')->getFill()->applyFromArray(
                            array(
                                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => array('rgb' => '39b3d7')
                            )
                    );
                    $objWorkSheet->getStyle('A3:D7')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                    $objWorkSheet->getStyle('A9:M10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                    for ($col = ord('A'); $col <= ord('M'); $col++) {
                        $objWorkSheet->getColumnDimension(chr($col))->setAutoSize(true);
                        $objWorkSheet->getStyle(chr($col))->getFont()->setSize(12);
                    }
                    //Query Menampilkan Data Looping
                    $problem_detail = $this->export_all_model->get_Problem($problem, $tgl_awal, $tgl_akhir);

                    //Looping Isi Table
                    $i = 11;
                    $no = 0;
                    foreach ($problem_detail as $row) {
                        $jam = $row['DURASI'];
						$jamkonvert = explode(":", $jam);
						$detik = $jamkonvert[0]*3600 + $jamkonvert[1] * 60 + $jamkonvert[2];
						$detik_sebulan = $tanggal * 86400;
						$hitung = $detik_sebulan - $detik;
						$akhir = $hitung / $detik_sebulan * 100;
						$hasil = round($akhir, 2);
                        $this->excel->setActiveSheetIndex(0);
                        $objWorkSheet->getStyle('A' . $i . ':' . 'M' . $i)->getFont()->setSize(8);
                        $objWorkSheet->getStyle('A' . $i . ':' . 'M' . $i)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border :: BORDER_THIN);
                        $objWorkSheet->setCellValue('A' . $i, $no + 1);
                        $objWorkSheet->setCellValue('B' . $i, $row['Requester']);
                        $objWorkSheet->setCellValue('C' . $i, $row['Created_Time']);
                        $objWorkSheet->setCellValue('D' . $i, $row['Subject']);
                        $objWorkSheet->setCellValue('E' . $i, $row['request_id']);
                        $objWorkSheet->setCellValue('F' . $i, $row['request_type']);
                        $objWorkSheet->setCellValue('G' . $i, $row['Down_Time']);
                        $objWorkSheet->setCellValue('H' . $i, $row['Uptime']);
                        $objWorkSheet->setCellValue('I' . $i, $row['DURASI']);
                        $objWorkSheet->setCellValue('J' . $i, $row['Root_Cause']);
                        $objWorkSheet->setCellValue('K' . $i, $row['Resolution']);
                        $objWorkSheet->setCellValue('L' . $i, '99,50');
                        $objWorkSheet->setCellValue('M' . $i, $hasil);
                        $i = $i + 1;
                        $no = $no + 1;
                    }

                    // Rename sheet
                    $objWorkSheet->setTitle($problem);

                    $loop++;
                }
                $filename = 'PROBLEM PERFORMANCE REPORT_PS-' . date('YmdHis') . '.xlsx'; //Nama File Berdasarkan Requester
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //Format Untuk .xlsx
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');

                ob_end_clean();
                //Output Untuk Excel 2007
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');

                $objWriter->save('php://output');
            }
        }
    }

}


