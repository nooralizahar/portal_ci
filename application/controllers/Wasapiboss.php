<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Wasapiboss extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("chat");

        $this->load->model("Khusus_model","khusus");
        $this->load->library('excel');

    }

    public function index() {

        $data = array(
            "id" => "PORTALTES01",
            "number" => "6287882252582",
            "message" => "TEST NEBENG KE iBOSS"
        );
        
        $url ="http://10.0.47.11:28055/send";
        
            $options = array(
              'http' => array(
                'method'  => 'POST',
                'content' => json_encode( $data ),
                'header'=>  "Content-Type: application/json\r\n" .
                            "Accept: application/json\r\n"
                )
            );

            $context  = stream_context_create( $options );
            $result = file_get_contents( $url, false, $context );
            $response = json_decode( $result );

    }


    public function posversicurl1(){
                    // API URL
            $url = 'http://www.example.com/api';

            // Create a new cURL resource
            $ch = curl_init($url);

            // Setup request to send json via POST
            $data = array(
                'username' => 'codexworld',
                'password' => '123456'
            );
            $payload = json_encode(array("user" => $data));

            // Attach encoded JSON string to the POST fields
            curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

            // Set the content type to application/json
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

            // Return response instead of outputting
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Execute the POST request
            $result = curl_exec($ch);

            // Close cURL resource
            curl_close($ch);

           /* Catatan :

            Receive JSON POST Data using PHP 

            The following example shows how you can get or fetch the JSON POST data using PHP.

                Use json_decode() function to decoded JSON data in PHP.
                The file_get_contents() function is used to received data in a more readable format.

            $data = json_decode(file_get_contents('php://input'), true); 

            */


    }


    public function ruangwaid_post(){
        $token = "xxxx";
        $phone= "62812xxxxxx"; //untuk group pakai groupid contoh: 62812xxxxxx-xxxxx
        $message = "Testing by API ruangwa";

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://app.ruangwa.id/api/send_message',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => 'token='.$token.'&number='.$phone.'&message='.$message,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        echo $response;
    }


}