<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
    public function __construct(){
		parent::__construct();
        $this->load->model('Menu_model');				
	}

    public function index(){
        $menu = $this->Menu_model->get_menu();
        $data['menu'] = $this->fetch_menu($menu);
        $this->load->view('newlook-shared/rangka/nl-samp',$data);
    }

    public function fetch_menu($data){
        $menu1 = "";
        foreach($data as $menu){
        	if($menu->url<>"#"){
        		$menu1 .= '<li class="nav-item"><a class="nav-link collapsed"  href='.base_url().$menu->url.'  aria-expanded="false" aria-controls="collapseUtilities"><i class="'.$menu->icon.'"></i><span>'.$menu->title.'</span></a>';
        	}else{
            	$menu1 .= '<li class="nav-item"><a class="nav-link collapsed" href='.$menu->url.' data-toggle="collapse" data-target="#menu_'.$menu->id.'" aria-expanded="false" aria-controls="collapseUtilities"><i class="'.$menu->icon.'"></i><span>'.$menu->title.'</span></a>';
        	}
            if(!empty($menu->sub)){
                $menu1 .= '<div id="menu_'.$menu->id.'" class="collapse" data-parent="#accordionSidebar" style="">';
                $menu1 .= $this->fetch_sub_menu($menu->sub);
                $menu1 .= "</div>";
            }
            $menu1 .= '</li>';
        }
        return $menu1;
    }

    public function fetch_sub_menu($sub_menu){
		$sub = '<div class="bg-white py-2 collapse-inner rounded">';
		foreach($sub_menu as $menu){
			$sub .= '<a class="collapse-item" href="'.base_url().$menu->url.'">'.$menu->title.'</a>';	
		}
        $sub .= '</div>';
		return $sub;
	}

}