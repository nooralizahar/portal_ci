<?php


defined("BASEPATH") OR exit("Akses ditolak!");
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Datset extends CI_Controller {

    var $uriku;

    public function __construct() {
        parent::__construct();

        $this->uriku=$this->uri->segment(1);
        $this->dbe = $this->load->database('dbbaru', TRUE);
        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("tgl_indo");
        $this->load->helper("general");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("bool");
        $this->load->library('pdf');
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }
        $this->load->model("excel_import_model");
        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Hris_model","hris");
        $this->load->model("Huroga_model","huroga");
        $this->load->model("Aktif_model","aktif");

    }

    public function index() {
        $post = $this->input->post();


        if(isset($post["btnSimpan"])) {
            $sim = $this->huroga->isidafset();
            if($sim != false) {
                $silog= $this->input->post('jeasse').'_'.$this->input->post('measse');
                $akt=$this->pengguna->tamakt('simpan asset '.$silog,$this->session->userdata('username'));
                redirect(base_url("dafset/?suc"));
            } else {
                redirect(base_url("dafset/?err"));
            }
            exit();
        }

        $pemasok=$this->huroga->cbpemasok();
        $lokasi=$this->huroga->cblokasi();
        $jenis=$this->huroga->cbjenis();
        $merk=$this->huroga->cbmerk();
        $user=$this->hris->cbuser();
        $stat=$this->huroga->cbstat();
        $dfasset=$this->huroga->daftar();
        
        $judul = "Aplikasi Portal - Daftar Asset";
        $menu = $this->set_amenu("menol");

        $this->load->view("panel/frames/header",compact("judul","menu"));
        $this->load->view("panel/dasset/tessad",compact("stat","dfasset","pemasok","lokasi","jenis","merk","user"));
        $this->load->view("panel/frames/footer");
    }



    private function set_amenu($active) {
        $menu = [
            "dashboard"=>"",
            "menu_it"=>"",
            "menu_eng"=>"",
            "harga_baru"=>"",
            "salesrevenue"=>"",
            "chat_baru"=>"",
            "disx_kirm"=>"",
            "disx_trma"=>"",
            "talian"=>"",
            "menufoc"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>"",
            "menuotb"=>"",
            "lokaotb"=>"",
            "womonito"=>"",
            "divisi"=>"",
            "survey"=>"",
            "selfservice"=>"",
            "khusus"=>"",
            "aktifitas"=>"",
            "surat_terkirim"=>"",
            "disposisi_keluar"=>"",
            "disposisi_masuk"=>"",
            "pengguna"=>"",
            "jabatan"=>"",
            "inventory"=>"",
            "menol"=>"",
            "mesat"=>"",
            "daftaodp"=>"",
            "daftaodc"=>"",
            "rekapodp"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }




}