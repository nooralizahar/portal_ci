<?php


defined("BASEPATH") OR exit("Akses ditolak!");
class Panoc extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->uriku=$this->uri->segment(1);
        $this->idaku=$this->session->userdata("id_pengguna");
        $this->namku=$this->session->userdata("nama_lengkap");
        $this->usrku=$this->session->userdata("username");
        $this->levku=$this->session->userdata("level");
        $this->divku=$this->session->userdata("id_div");
        $this->nadiv=$this->session->userdata("nm_div");
        $this->jabku=$this->session->userdata("id_jabatan");
        $this->najab=$this->session->userdata("nm_jab");

        date_default_timezone_set("Asia/Jakarta");
        $this->load->helper("pengalih");
        $this->load->helper("cektipe");
        $this->load->helper("tgl_indo");
        $this->load->helper("bool");
        proteksi_login($this->session->userdata());

        $this->load->model("Konfig_web_model","konfig");

        if($this->konfig->status_maintenance()) {
            $this->load->view("maintenance");
        }
        $this->load->model('excel_import_model');
        $this->load->library('excel');
        $this->load->model("Pengguna_model","pengguna");
        $this->load->model("Harga_model","harga");
        $this->load->model("Divisi_model","divisi");
        $this->load->model("Jabatan_model","jabatan");
        $this->load->model("Diskusi_model","diskusi");
        $this->load->model("Datait_model","datait");
        $this->load->model("Noctiket_model","ntiket");
        $this->load->model("Laporan_model","laporan");
        $this->load->model("Wordata_model","wordata");
        $this->load->model("Nocmainte_model","mainte");
    }

    public function index() {
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));

        $judul = "Portal Dashboard";
        $menu = $this->set_menu("dashboard");

        
        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/index");
        $this->load->view("panoc/frames/footer");
    }

     public function tisemua($param=null) {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != super )
            redirect(base_url("panoc/"));


        $judul = "Daftar Tiket";
        if($param==1){
        $tiket = $this->ntiket->sem_ntik(1);
        $menu = $this->set_menu("titunda");
        $lbr="Tiket Tertunda";
        }else if($param==2){
        $menu = $this->set_menu("tiseles");
        $tiket = $this->ntiket->sem_ntik(2);
        $lbr="Tiket Selesai";
        }else{
        $menu = $this->set_menu("tisemua"); 
        $tiket = $this->ntiket->sem_ntik(0); 
        $lbr="Semua Tiket";   
        }
        

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/tiunem/tisemua",compact("tiket","lbr"));
        $this->load->view("panoc/frames/footer");
    }   

    public function tikelol() {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != super )
            redirect(base_url("panoc/"));

        //$daftar_pengguna = $this->pengguna->nocsem();

        $judul = "Kelola Tiket";
        $menu = $this->set_menu("tikelol");

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/tiunem/tikelol",compact("daftar_pengguna"));
        $this->load->view("panoc/frames/footer");
    }

    public function uskelol() {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != super )
            redirect(base_url("panoc/"));

        $daftar_pengguna = $this->pengguna->nocsem();

        $judul = "Kelola NOC";
        $menu = $this->set_menu("uskelol");

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/tiunem/uskelol",compact("daftar_pengguna"));
        $this->load->view("panoc/frames/footer");
    }

    public function profilku($id = null) {
        //if($this->session->userdata("id_jabatan") != 1)
            //redirect(base_url("panel/"));
$post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->editpro();
            if($edt != false) {
                redirect(base_url("panoc/profilku/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url("panoc/profilku/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        //if($id == null) redirect(base_url("panoc/uskelol/"));
        $judul = "Edit Profil";
        $menu = $this->set_menu("uskelol");


        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
            $this->load->view("panoc/person/profilku",compact("pengguna"));
            $this->load->view("panoc/frames/footer");
        } else redirect(base_url("panoc/pengguna/"));
    }

        public function profilvi($id = null) {
        if($this->session->userdata("id_pengguna") != $id)
            redirect(base_url("panoc/"));

        $judul = "Profil Pengguna";
        $menu = $this->set_menu("uskelol");


        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
            $this->load->view("panoc/person/profilkuv",compact("pengguna"));
            $this->load->view("panoc/frames/footer");
        } else redirect(base_url("panoc/pengguna/"));
    }

    public function ustamba() {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != super )
            redirect(base_url("panoc/"));
        
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $tbh = $this->pengguna->tambah();
            if($tbh != false) {
                redirect(base_url("panoc/ustamba/?succ"));
            } else {
                redirect(base_url("panoc/ustamba/?err"));
            }
            exit();
        }

        $judul = "Tambah NOC";
        $menu = $this->set_menu("uskelol");
        $daftas = $this->pengguna->data_atasan();
        $daftar_jabatan = $this->jabatan->ambil_semua();
        $daftar_divisi = $this->divisi->divisi_semua();

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/tiunem/ustamba",compact("daftas","daftar_jabatan","daftar_divisi"));
        $this->load->view("panoc/frames/footer");
    }

    public function tihapus($id){
        $hap = $this->ntiket->thapus($id);

            if($hap)
                redirect(base_url("panoc/tisemua/?hsucc"));
            else
                redirect(base_url("panoc/tisemua/?herr"));
   
    }

    public function tikbaru() {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panoc/"));

        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $bar = $this->ntiket->baru();
            if($bar != false) {
                redirect(base_url("panoc/tisemua/?succ"));
            } else {
                redirect(base_url("panoc/tikbaru/?err"));
            }
            exit();
        }



        $dreqter = $this->ntiket->semreq();
        $dstatus = $this->ntiket->semsta();
        $dreqtyp = $this->ntiket->semrty();
        $dvendor = $this->ntiket->semven();
        $dprosid = $this->ntiket->sempsi();
        $dlincat = $this->ntiket->semlca();
        $dcaugro = $this->ntiket->semcgr();
        $drotcau = $this->ntiket->semrca();

        $judul = "Tiket Baru";
        $menu = $this->set_menu("tisemua");

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/tiunem/tikbaru",compact("dreqter","dstatus","dreqtyp","dvendor","dprosid","dlincat","dcaugro","drotcau"));
        $this->load->view("panoc/frames/footer");
    }

    public function tiupdat($id) {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panoc/"));

        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $bar = $this->ntiket->baru();
            if($bar != false) {
                redirect(base_url("panoc/tisemua/?succ"));
            } else {
                redirect(base_url("panoc/tikbaru/?err"));
            }
            exit();
        }

        $dtiket = $this->ntiket->tpilih($id);

        $dreqter = $this->ntiket->semreq();
        $dstatus = $this->ntiket->semsta();
        $dreqtyp = $this->ntiket->semrty();
        $dvendor = $this->ntiket->semven();
        $dprosid = $this->ntiket->sempsi();
        $dlincat = $this->ntiket->semlca();
        $dcaugro = $this->ntiket->semcgr();
        $drotcau = $this->ntiket->semrca();
        $dcreaby = $this->ntiket->semcby();
        $dclosby = $this->ntiket->semlby();

        $judul = "Tiket Update";
        $menu = $this->set_menu("tisemua");

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/tiunem/tiupdat",compact("dtiket","dreqter","dstatus","dreqtyp","dvendor","dprosid","dlincat","dcaugro","drotcau","dcreaby","dclosby"));
        $this->load->view("panoc/frames/footer");
    }

    public function penggunaedit($id = null) {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panoc/"));
        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            if(isset($post["password"])){
                if($post["password"] == "")
                    unset($post["password"]);
                else
                    $post["password"] = md5($post["password"]);
            }
            $edt = $this->pengguna->edit();
            if($edt != false) {
                redirect(base_url("panoc/penggunaedit/".$post["id_pengguna"]."?succ"));
            } else {
                redirect(base_url("panoc/penggunaedit/".$post["id_pengguna"]."?err"));
            }
            exit();
        }

        if($id == null) redirect(base_url("panoc/uskelol/"));
        $judul = "Edit Pengguna";
        $menu = $this->set_menu("uskelol");


        $pengguna = $this->pengguna->ambil_berdasarkan_id($id);

        if($pengguna) {
            $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
            $this->load->view("panoc/penggunaedit",compact("pengguna"));
            $this->load->view("panoc/frames/footer");
        } else redirect(base_url("panoc/pengguna/"));
    }

    public function penggunablokir($id = null) {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panoc/"));
        if($id == null) redirect(base_url("panoc/pengguna"));
        $this->pengguna->blokir($id);
        redirect(base_url("panoc/pengguna/?succ=1"));
    }

    public function penggunabuka($id = null) {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panoc/"));
        if($id == null) redirect(base_url("panel/pengguna"));
        $this->pengguna->buka($id);
        redirect(base_url("panoc/pengguna/?succ=2"));
    }

    public function mtcbaru() {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panoc/"));

        $post = $this->input->post();
        if(isset($post["btnSubmit"])) {
            $bar = $this->mainte->baru();
            if($bar != false) {
                redirect(base_url("panoc/mtcsemu/?succ"));
            } else {
                redirect(base_url("panoc/mtcbaru/?err"));
            }
            exit();
        }




        $judul = "Maintenance Baru";
        $menu = $this->set_menu("mtcbaru");

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/mtunem/mtcbaru");
        $this->load->view("panoc/frames/footer");
    }

    public function mtcsemu() {
        if($this->session->userdata("level") != 'user' && $this->session->userdata("mn_akses") != 'super' )
            redirect(base_url("panoc/"));

        $d_mtain  = $this->mainte->semua();
        $judul = "Data Maintenance";
        $menu = $this->set_menu("mtcsemu");

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/mtunem/mtcsemu",compact("d_mtain"));
        $this->load->view("panoc/frames/footer");
    }

    public function mtcubah($idm=null) {
   
        
        $mtcpi = $this->mainte->pilih($idm);

        $post = $this->input->post();


        if(isset($post["btnSimpan"])) {
            $sim = $this->mainte->simpan($idm);
            if($sim != false) {
                redirect(base_url("panoc/mtcsemu/?succ"));
            } else {
                redirect(base_url("panoc/mtcubah/".$idm."?err"));
            }
            exit();
        }

        $judul = "Ubah Maintenance";
        $menu = $this->set_menu("mtcsemu");
        

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/mtunem/mtcubah",compact("mtcpi","idm"));//,"pi","onm"
        $this->load->view("panoc/frames/footer");
    }


    public function womonito() {
        
        $blnini= date('Y-m');
        $blnlal= date('Y-m',strtotime("-1 month"));


        $woblni=$this->harga->pilwo($blnini);
        $woblnl=$this->harga->pilwo($blnlal);
        $wosum=$this->harga->wosummary();
        $wosal=$this->harga->wosales();
        $woseg=$this->harga->wosegme();
        $wosta=$this->harga->wostatu();
        if($this->session->userdata('id_jabatan')==22){
        $datait_wo = $this->wordata->wo4baa();
        }else{
        $datait_wo = $this->wordata->wosemua();
        }
        $data_wopil = $this->wordata->wopilih();

        $judul = "Monitoring WO";
        $menu = $this->set_menu("menol");

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("panoc/owunem/womonito",compact("wosta","woseg","wosal","woblnl","woblni","wosum","datait_wo","data_wopil"));
        $this->load->view("panoc/frames/footer");
    }

    public function repsla() {
        $list_file=array();

if(empty($this->uri->segment(3))){
    $dir = "assets/uploads/alsper/";
}elseif(!empty($this->uri->segment(3))) {
    if(empty($this->uri->segment(4))){
        $dir = "assets/uploads/alsper/".$this->uri->segment(3)."/";
    }elseif(!empty($this->uri->segment(4))){
        $dir = "assets/uploads/alsper/".$this->uri->segment(3)."/".$this->uri->segment(4)."/";
    }
}

        // buka directory, dan baca isinya
        if (is_dir($dir)){
          if ($dh = opendir($dir)){
            while (($file = readdir($dh)) !== false){
                $list_file[]=$file;
            }
            closedir($dh);
          }
        }

        $data['daftar_file']=$list_file;
        $data['tiksemua']=$this->laporan->semua();
        $data['pelsemua']=$this->laporan->pelanggan();
        $data['Requester']=$this->laporan->dafreq();
        //$this->load->view('home',$data);

        //$tiksemua = $this->laporan->semua();
        //$pelsemua = $this->laporan->pelanggan();


        
        $judul = "Self Service - Laporan SLA Pelanggan";
        $menu = $this->set_menu("mesat");

        $this->load->view("panoc/frames/hawaln",compact("judul","menu"));
        $this->load->view("share/foeruc/repsla",$data); //compact("tiksemua","pelsemua")
        $this->load->view("panoc/frames/footer");
    }


    public function logout() {
        $akt=$this->pengguna->tamakt('logout berhasil',$this->session->userdata('username'));
        $this->session->sess_destroy();
        redirect(base_url("login/"));
    }


    private function set_menu($active) {
        $menu = [
            "dashboard"=>"",
            "profil"=>"",
            "uprofil"=>"",
            "tisemua"=>"",
            "titunda"=>"",
            "tiseles"=>"",
            "tikelol"=>"",
            "uskelol"=>"",
            "womonito"=>"",
            "rokelol"=>"",
            "timanua"=>"",
            "mtcbaru"=>"",
            "mtcsemu"=>"",
            "gasandi"=>"",
            "menol"=>"",
            "mesat"=>"",
            "medua"=>"",
            "metig"=>"",
            "meemp"=>"",
            "melim"=>"",
            "ladload"=>""
        ];
        $menu[$active] = "active";
        return $menu;
    }


   


}