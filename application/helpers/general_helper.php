<?php
if(!function_exists("usrasset")) {
    function usrasset($r) {
        $inj = get_instance();
        if(empty($r)){
        $hasil=null;    
        }else{
        $q = $inj->dbe->where('id', $r)->get('v_opsiasset_final')->row();
        if(!empty($q)){
        $hasil=$q->opsi;}else{$hasil=$r;}
        }
        return $hasil;
    }
}


if(!function_exists("odpdetil")) {
    function odpdetil($ref) {
        $ini = get_instance();
        if(empty($ref)){
        $hasil=null;    
        }else{
        $ini->dbz->select('cid_pelanggan,nama_pelanggan,poncard,ponsn')->where('t_net_odp_id', $ref);
        $hasil = $ini->dbz->get('v_jlm_detil_cus_revi')->result();
        
        }
        return $hasil;
    }
}

if(!function_exists("odptikor")) {
    function odptikor($ref) {
        $ini = get_instance();
        if(empty($ref)){
        $hasil=null;    
        }else{
        $ini->dbz->select('coordinate,capacity')->where('t_net_odp_id', $ref);
        $q = $ini->dbz->get('t_net_odp')->row();
        $hasil="<b><sup style='color:green';>[".$q->capacity."]</sup></b><br>".$q->coordinate;
        }
        return $hasil;
    }
}

if(!function_exists("lokcek")) {
    function lokcek($idloc) {
        $ini = get_instance();
        if(empty($idloc)){
        $hasil=null;    
        }else{
       	$ini->dbz->where('t_location_id', $idloc);
        $q = $ini->dbz->get('v_location')->row();
        $hasil=$q->area_name." - ".$q->name;
        }
        return $hasil;
    }
}

if(!function_exists("isiodp")) {
    function isiodp($ido) {
        $ini = get_instance();
        if(empty($ido)){
        $hasil=null;    
        }else{
        $ini->dbz->where('t_net_odp_id', $ido);
        $q = $ini->dbz->get('v_net_odp_used')->row();
            if(empty($q)){
            $hasil=0;
            }else{
            $hasil=$q->pakai;
            }
        }
        return $hasil;
    }
}

if(!function_exists("stacek")) {
    function stacek($id) {
        $ini = get_instance();
        $ini->db->where('idlok', $id);
        $r = $ini->db->get('t_odp_lok')->row();
        if(isset($r->tgsur)){
            if(is_null($r->tgsur)){$rfs="SVY : <i class='fa fa-circle-o text-danger'>";}else{$sur="SVY : <a href='#' data-toggle='tooltip' data-placement='left' title='$r->tgsur'><i class='fa fa-check'></i></a>";}
            if(is_null($r->tgins)){$rfs="INS : <i class='fa fa-circle-o text-danger'>";}else{$ins="INS : <a href='#' data-toggle='tooltip' data-placement='top' title='$r->tgins'><i class='fa fa-check'></i></a> ";}
            if(is_null($r->tgrfs)){$rfs="RFS : <i class='fa fa-circle-o text-danger'>";}else{$rfs="RFS : <a href='#' data-toggle='tooltip' data-placement='right' title='$r->tgrfs'><i class='fa fa-check'></i></a> ";}
        $s="<small>".$sur.", ".$ins.", ".$rfs."</small>";
        }else{ 
        $s=null;
        }
        return $s;
    }
    }

if(!function_exists("starfs")) {
    function starfs($id) {
        $ini = get_instance();
        $ini->db->where('idlok', $id);
        $q= $ini->db->get('t_odp_lok')->row();
            if(is_null($q->tgrfs)){$rfs="<i class='fa fa-circle-o text-danger'>";}else{$rfs="<a href='#' data-toggle='tooltip' data-placement='right' title='$q->tgrfs'><i class='fa fa-check'></i></a> ";}
        $rf="<small>".$rfs."</small>";
        return $rf;
    }
    }

if(!function_exists("stalap")) {
    function stalap($id) {
        $ini = get_instance();
        $ini->db->where('idlok', $id);
        $q= $ini->db->get('t_odp_lok')->row();
            if(is_null($q->tgsur)){$s="<i class='fa fa-circle-o text-danger'>";}else{$s="SVY";}
            if($s=='SVY' && is_null($q->tgins)){$s="SVY";}else{$s="INS";}
            if($s=='INS' && is_null($q->tgrfs)){$s="INS";}else{$s="RFS";}
        $rf="".$s."";
        return $rf;
        }
    }
    
if(!function_exists("naleng")) {
    function naleng($id) {
        $ini = get_instance();
        $ini->db->where('id_pengguna', $id);
        $q= $ini->db->get('pengguna')->row();           
        $nam=$q->nama_lengkap;
        return $nam;
        }
    }
    
if(!function_exists("odcnama")) {
    function odcnama($id) {
        if(empty($id)){
        $nam=null;    
        }else{
            
        $ini = get_instance();
        $ini->db->where('idodp', $id);
        $q= $ini->db->get('v_odc_odp_brig')->row();
        if(empty($q->nmodc)){
           $nam=""; 
         }else{
           $nam=$q->nmodc; 
         }           
        
        return $nam;
        }
      }
    }

if(!function_exists("satuan")) {
    function satuan($s) {
        switch ($s)
            {
                case "m":
                    return "meter";
                    break;
                case "p":
                    return "pieces";
                    break;
                case "u":
                    return "unit";
                    break;
                case "b":
                    return "batang";
                    break;
                case "r":
                    return "roll";
                    break;
                }
        }
    }

if(!function_exists("ht2pt")) {
function ht2pt($str){
    $str = str_replace('&nbsp;', ' ', $str);
    $str = str_replace('<h1>', '*', $str);
    $str = str_replace('</h1>', '*'.chr(10), $str);
    $str = str_replace('<br>', ''.chr(10), $str);
    $str = str_replace('<br/>', ''.chr(10), $str);
    $str = html_entity_decode($str, ENT_QUOTES | ENT_COMPAT , 'UTF-8');
    $str = html_entity_decode($str, ENT_HTML5, 'UTF-8');
    $str = html_entity_decode($str);
    $str = htmlspecialchars_decode($str);
    $str = strip_tags($str);

    return $str;
}
}    



if(!function_exists("penyebut")) {
    function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }
}

if(!function_exists("terbilang")) {
    function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim(penyebut($nilai));
        } else {
            $hasil = trim(penyebut($nilai));
        }           
        return $hasil." rupiah";
    }
}

if(!function_exists("hitkpi")) {
    function hitkpi($tga,$tgb){
                            //$idr=strval($tga); 
                            $date1= new DateTime($tga);
                            $date2= new DateTime($tgb);

                            $diff = $date2->diff($date1);

                            $hours = $diff->h;
                            $hours = $hours + ($diff->days*24);

                            $sisa=$hours%24; 
                            $hari=($hours-$sisa)/24; 
                            $hasil ="<small>".$hari." hari ".$sisa." jam </small>";
        return $hasil;
    }
}

/*    function codexworldGetDistanceOpt($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo){
    $rad = M_PI / 180;
    //Calculate distance from latitude and longitude
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin($latitudeFrom * $rad) 
        * sin($latitudeTo * $rad) +  cos($latitudeFrom * $rad)
        * cos($latitudeTo * $rad) * cos($theta * $rad);

    return acos($dist) / $rad * 60 *  1.853;
    } 
}

SELECT cid_pelanggan,nama_pelanggan,poncard,ponsn FROM public.v_jlm_detil_cus_revi where t_net_odp_id='171';


*/