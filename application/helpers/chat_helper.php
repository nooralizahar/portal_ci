<?php
if(!function_exists("tilcat")) {
    function tilcat($id) {
        $ini = get_instance();
        $ini->db->where('id_rha', $id);
        $hchat = $ini->db->get('v_app_portal_chat')->result();  
      
        return $hchat;
        }
    }

if(!function_exists("lascat")) {
    function lascat($id,$us) {
        $ini = get_instance();
        $ini->db->where('id_rha', $id)->order_by('id','DESC')->limit(1);
        $q= $ini->db->get('v_app_portal_chat')->row(); 
        if(empty($q->uschat)){
        $uchat=$us; 
        }else{
        $uchat=$q->uschat; 
        }
        return $uchat;
        }
    }