<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	 
	if ( ! function_exists('tgl_indo'))
	{
	    function tgl_indo($tgl)
	    {
	        $ubah = gmdate($tgl, time()+60*60*8);
	        $pecah = explode("-",$ubah);
	        $tanggal = $pecah[2];
	        $bulan = bulan($pecah[1]);
	        $tahun = $pecah[0];
	        return $tanggal.' '.$bulan.' '.$tahun;
	    }

	    	function tgl_indopj($tgl){
			$tanggal = substr($tgl,8,2);
			$bulan = bulan(substr($tgl,5,2));
			$tahun = substr($tgl,0,4);
			return $tanggal.' '.$bulan.' '.$tahun;		 
		}
	}
	 
	if ( ! function_exists('bulan'))
	{

	    function blnno($bln)
	    {
	        switch ($bln)
	        {
	            case JAN:
	                return "01";
	                break;
	            case FEB:
	                return "02";
	                break;
	            case MAR:
	                return "03";
	                break;
	            case 4:
	                return "IV";
	                break;
	            case 5:
	                return "V";
	                break;
	            case 6:
	                return "VI";
	                break;
	            case 7:
	                return "VII";
	                break;
	            case 8:
	                return "VIII";
	                break;
	            case 9:
	                return "IX";
	                break;
	            case 10:
	                return "X";
	                break;
	            case 11:
	                return "XI";
	                break;
	            case 12:
	                return "XII";
	                break;
	        }
	    }



	    function blnro($bln)
	    {
	        switch ($bln)
	        {
	            case 1:
	                return "I";
	                break;
	            case 2:
	                return "II";
	                break;
	            case 3:
	                return "III";
	                break;
	            case 4:
	                return "IV";
	                break;
	            case 5:
	                return "V";
	                break;
	            case 6:
	                return "VI";
	                break;
	            case 7:
	                return "VII";
	                break;
	            case 8:
	                return "VIII";
	                break;
	            case 9:
	                return "IX";
	                break;
	            case 10:
	                return "X";
	                break;
	            case 11:
	                return "XI";
	                break;
	            case 12:
	                return "XII";
	                break;
	        }
	    }

	  function bulan($bln)
	    {
	        switch ($bln)
	        {
	            case 1:
	                return "Januari";
	                break;
	            case 2:
	                return "Februari";
	                break;
	            case 3:
	                return "Maret";
	                break;
	            case 4:
	                return "April";
	                break;
	            case 5:
	                return "Mei";
	                break;
	            case 6:
	                return "Juni";
	                break;
	            case 7:
	                return "Juli";
	                break;
	            case 8:
	                return "Agustus";
	                break;
	            case 9:
	                return "September";
	                break;
	            case 10:
	                return "Oktober";
	                break;
	            case 11:
	                return "November";
	                break;
	            case 12:
	                return "Desember";
	                break;
	        }
	    }

	  function angkahuruf($angka)
	    {
	        switch ($angka)
	        {
	            case 1:
	                return "Satu";
	                break;
	            case 2:
	                return "Dua";
	                break;
	            case 3:
	                return "Tiga";
	                break;
	            case 4:
	                return "Empat";
	                break;
	            case 5:
	                return "Lima";
	                break;
	            case 6:
	                return "Enam";
	                break;
	            case 7:
	                return "Tujuh";
	                break;
	            case 8:
	                return "Delapan";
	                break;
	            case 9:
	                return "Sembilan";
	                break;
	            case 10:
	                return "Sepuluh";
	                break;
	            case 11:
	                return "Sebelas";
	                break;
	            case 12:
	                return "Dua Belas";
	                break;
	            case 13:
	                return "Tiga Belas";
	                break;
	            case 14:
	                return "Empat Belas";
	                break;
	            case 15:
	                return "Lima Belas";
	                break;
	            case 16:
	                return "Enam Belas";
	                break;
	            case 17:
	                return "Tujuh Belas";
	                break;
	            case 18:
	                return "Delapan Belas";
	                break;
	            case 19:
	                return "Sembilan Belas";
	                break;
	            case 20:
	                return "Dua Puluh";
	                break;
	            case 21:
	                return "Dua Puluh Satu";
	                break;
	            case 22:
	                return "Dua Puluh Dua";
	                break;
	            case 23:
	                return "Dua Puluh Tiga";
	                break;
	            case 24:
	                return "Dua Puluh Empat";
	                break;
	            case 25:
	                return "Dua Puluh Lima";
	                break;
	            case 26:
	                return "Dua Puluh Enam";
	                break;
	            case 27:
	                return "Dua Puluh Tujuh";
	                break;
	            case 28:
	                return "Dua Puluh Delapan";
	                break;
	            case 29:
	                return "Dua Puluh Sembilan";
	                break;
	            case 30:
	                return "Tiga Puluh";
	                break;
	            case 31:
	                return "Tiga Puluh Satu";
	                break;
	            case 2022:
	                return "Dua Ribu Dua Puluh Dua";
	                break;
	            case 2023:
	                return "Dua Ribu Dua Puluh Tiga";
	                break;

	        }
	    }	


	}

	//Format Shortdate
	if ( ! function_exists('shortdate_indo'))
	{
	    function shortdate_indo($tgl)
	    {
	        $ubah = gmdate($tgl, time()+60*60*8);
	        $pecah = explode("-",$ubah);
	        $tanggal = $pecah[2];
	        $bulan = short_bulan($pecah[1]);
	        $tahun = $pecah[0];
	        return $tanggal.'/'.$bulan.'/'.$tahun;
	    }
	}
	 
	if ( ! function_exists('short_bulan'))
	{
	    function short_bulan($bln)
	    {
	        switch ($bln)
	        {
	            case 1:
	                return "01";
	                break;
	            case 2:
	                return "02";
	                break;
	            case 3:
	                return "03";
	                break;
	            case 4:
	                return "04";
	                break;
	            case 5:
	                return "05";
	                break;
	            case 6:
	                return "06";
	                break;
	            case 7:
	                return "07";
	                break;
	            case 8:
	                return "08";
	                break;
	            case 9:
	                return "09";
	                break;
	            case 10:
	                return "10";
	                break;
	            case 11:
	                return "11";
	                break;
	            case 12:
	                return "12";
	                break;
	        }
	    }
	}

	//Format Medium date
	if ( ! function_exists('tgl_indome'))
	{
	    function tgl_indome($tgl)
	    {
		if(empty($tgl)){
        	$hasil='-';
        	return $hasil;    
        }else{
	        $ubah = gmdate($tgl, time()+60*60*8);
	        $pecah = explode("-",$ubah);
	        $tanggal = $pecah[2];
	        $bulan = medium_bulan($pecah[1]);
	        $tahun = substr($pecah[0], 2,2);
	        return $tanggal.'-'.$bulan.'-'.$tahun;}
	    }
	}

	if ( ! function_exists('tgl_indosm'))
	{
	    function tgl_indosm($tgl)
	    {
	      if(empty($tgl)){
        	$hasil='-';
        	return $hasil;    
          }else{
	        $ubah = gmdate($tgl, time()+60*60*8);
	        $pecah = explode("-",$ubah);
	        $tanggal = $pecah[2];
	        $bulan = medium_bulan($pecah[1]);
	        $tahun = substr($pecah[0], 0,2);
	        return $tanggal.'-'.$bulan.'-'.$tahun;}
	    }
	}
	 
	if ( ! function_exists('medium_bulan'))
	{
	    function medium_bulan($bln)
	    {
	        switch ($bln)
	        {
	            case 1:
	                return "Jan";
	                break;
	            case 2:
	                return "Feb";
	                break;
	            case 3:
	                return "Mar";
	                break;
	            case 4:
	                return "Apr";
	                break;
	            case 5:
	                return "Mei";
	                break;
	            case 6:
	                return "Jun";
	                break;
	            case 7:
	                return "Jul";
	                break;
	            case 8:
	                return "Agu";
	                break;
	            case 9:
	                return "Sep";
	                break;
	            case 10:
	                return "Okt";
	                break;
	            case 11:
	                return "Nov";
	                break;
	            case 12:
	                return "Des";
	                break;
	        }
	    }
	}
	
	//Long date indo Format
	if ( ! function_exists('longdate_indo'))
	{
	    function longdate_indo($tanggal)
	    {
	        $ubah = gmdate($tanggal, time()+60*60*8);
	        $pecah = explode("-",$ubah);
	        $tgl = $pecah[2];
	        $bln = $pecah[1];
	        $thn = $pecah[0];
	        $bulan = bulan($pecah[1]);
	 
	        $nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
	        $nama_hari = "";
	        if($nama=="Sunday") {$nama_hari="Minggu";}
	        else if($nama=="Monday") {$nama_hari="Senin";}
	        else if($nama=="Tuesday") {$nama_hari="Selasa";}
	        else if($nama=="Wednesday") {$nama_hari="Rabu";}
	        else if($nama=="Thursday") {$nama_hari="Kamis";}
	        else if($nama=="Friday") {$nama_hari="Jumat";}
	        else if($nama=="Saturday") {$nama_hari="Sabtu";}
	        return $nama_hari.','.$tgl.' '.$bulan.' '.$thn;
	    }
	}

		if ( ! function_exists('hari_indo'))
	{
	    function hari_indo($tanggal)
	    {
	        $ubah = gmdate($tanggal, time()+60*60*8);
	        $pecah = explode("-",$ubah);
	        $tgl = $pecah[2];
	        $bln = $pecah[1];
	        $thn = $pecah[0];
	        $bulan = bulan($pecah[1]);
	 
	        $nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
	        $nama_hari = "";
	        if($nama=="Sunday") {$nama_hari="Minggu";}
	        else if($nama=="Monday") {$nama_hari="Senin";}
	        else if($nama=="Tuesday") {$nama_hari="Selasa";}
	        else if($nama=="Wednesday") {$nama_hari="Rabu";}
	        else if($nama=="Thursday") {$nama_hari="Kamis";}
	        else if($nama=="Friday") {$nama_hari="Jumat";}
	        else if($nama=="Saturday") {$nama_hari="Sabtu";}
	        return $nama_hari;
	    }
	}

		if ( ! function_exists('namahr'))
	{
	    function namahr($tanggal)
	    {

	 
	        $nama = date("l", $tanggal);
	        $nama_hari = "";
	        if($nama=="Sunday") {$nama_hari="Minggu";}
	        else if($nama=="Monday") {$nama_hari="Senin";}
	        else if($nama=="Tuesday") {$nama_hari="Selasa";}
	        else if($nama=="Wednesday") {$nama_hari="Rabu";}
	        else if($nama=="Thursday") {$nama_hari="Kamis";}
	        else if($nama=="Friday") {$nama_hari="Jumat";}
	        else if($nama=="Saturday") {$nama_hari="Sabtu";}
	        return $nama_hari;
	    }
	}

	if ( ! function_exists('rumPMT'))
	{
		//* @param float $apr   Interest rate.
		//* @param integer $term  Loan length in years. 
		//* @param float $loan   The loan amount.
		
		function rumPMT($apr, $term, $loan)
		{
		  $term = $term * 12;
		  $apr = $apr / 1200;
		  $amount = $apr * -$loan * pow((1 + $apr), $term) / (1 - pow((1 + $apr), $term));
		  return round($amount);
		}

	}

	if ( ! function_exists('rumIRR'))
	{

		function rumIRR($investment, $flow, $precision = 0.000001) 
		{

	    if (array_sum($flow) < $investment):
	        return 0;
	    endif;
	    $maxIterations = 20;
	    $i =0;
	    if (is_array($flow)):
	        $min = 0;
	        $max = 1;
	        $net_present_value = 1;
	        while ((abs($net_present_value - $investment) > $precision)&& ($i < $maxIterations)) {
	            $net_present_value = 0;
	            $guess = ($min + $max) / 2;
	            foreach ($flow as $period => $cashflow) {
	                $net_present_value += $cashflow / (1 + $guess) ** ($period + 1);
	            }
	            if ($net_present_value - $investment > 0) {
	                $min = $guess;
	            } else {
	                $max = $guess;
	            }
	            $i++;
	        }
	        return $guess * 100;
	    else:
	        return 0;
	    endif;
		}
	}


	if ( ! function_exists('scan'))
	{

	function scan($dir){

    $files = array();

    // Is there actually such a folder/file?

    if(file_exists($dir)){
    
        foreach(scandir($dir) as $f) {
        
            if(!$f || $f[0] == '.') {
                continue; // Ignore hidden files
            }

            if(is_dir($dir . '/' . $f)) {

                // The path is a folder

                $files[] = array(
                    "name" => $f,
                    "type" => "folder",
                    "path" => $dir . '/' . $f,
                    "items" => scan($dir . '/' . $f) // Recursively get the contents of the folder
                );
            }
            
            else {

                // It is a file

                $files[] = array(
                    "name" => $f,
                    "type" => "file",
                    "path" => $dir . '/' . $f,
                    "size" => filesize($dir . '/' . $f) // Gets the size of this file
                );
            }
        }
    
    	}

    	return $files;
		}
	}


if ( ! function_exists('hari'))
	{
	    function hari($h)
	    {
	       switch ($h)
	        {
	            case "Sunday":
	                return "Minggu";
	                break;
	            case "Monday":
	                return "Senin";
	                break;
	            case "Tuesday":
	                return "Selasa";
	                break;
	            case "Wednesday":
	                return "Rabu";
	                break;
	            case "Thursday":
	                return "Kamis";
	                break;
	            case "Friday":
	                return "Jumat";
	                break;
	            case "Saturday":
	                return "Sabtu";
	                break;
	        }
	    }


	}	 
	